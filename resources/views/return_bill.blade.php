<!DOCTYPE html>
<html>
<head>
  <title>Return Bill</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  <style>
    body {
      width: 100%;
      padding: 20px;
      display: block;
    }

    h1 {
      font-size: 12px;
      line-height: 3px;
    }
    h1 {
      font-size: 12px;
      line-height: 3px;
    }

    p {
      font-size: 11px;
      line-height: 20px;
    }
    
    tr {
      font-size: 12px;
      line-height: 35px;
    }
    #second-table {
      text-align: center;
      border-collapse: collapse;
      width: 100%;
    }
    #table-a{
      text-align: left;
    }
    #table-b{
      text-align: left;
    }
    h6{
      font-size: 11px;
    }
    #table-c{
      text-align: center;
    }
    #table-d{
      text-align: left;
    }
    #second-table {
      border: 1px solid black;
      border-collapse: collapse;
    }
    #nd-rows, #nd-rows > th, #nd-rows > td{
      border: 1px solid black;
      text-align: center;
    }
    #table-a > p{
      font-size: 11px;
    }
    #table-ab{
      font-size: 12px;
    }


  </style>
</head>
<body>
  <center>
    <h3><b>{{$com_details->branch_name}}</b></h3>
    <p>{{$com_details->branch_address}}<br/> 
     {{$com_details->branch_country}}.<b>PH: +</b>{{$com_details->country_code}} {{$com_details->branch_contact}}</p>
     <p></p>
     <h4><b>PHARMACY RETURN BILL</b></h4>  <br>
   </center>
   <div class="row">
    <div class="col-md-12">
      <table style="width:100%" id="table-ab" class="col-md-12">
        <tr>
          <th >Customer name:</th>
          <td>{{$customer_details->customer_name}}</td>
          <th align="right">Invoice no:</th>
          <td>{{$invoice}}</td>
        </tr>
        <tr>
          <th><b>SalesReturn no: </b></th>
          <td>{{$return_details->sale_return_id}}</td>
          <th align="right">Date: </th>
          <td>{{$date}}</td>
        </tr>
      </table>
    </div>
  </div>
  <center>
    <table id = "second-table" class="col-md-12" style="width:100%">
      <tr id="nd-rows">
        <th>S.No</th>
        <th>Product Name</th> 
        <th>Batch.No</th>
        <th>Invoiced.Qty</th>
        <th>Return.Qty</th>
        <th>Rate</th>
        <th>Amount</th>
      </tr>
      @foreach($datas  as $key=> $return_lists)
      <tr id="nd-rows">
        <td>{{ ++$key }}</td>
        <td>{{  $return_lists['product_genericname'] }}</td>
        <td>{{  $return_lists['batch_no'] }}</td>
        <td>{{  $order_detail->product_qty }}</td>
        <td>{{  $return_lists['product_qty'] }}</td>
        <td>{{  $return_lists['product_netprice'] }}</td>
        <td>{{  $return_lists['product_grossprice'] }}</td>
      </tr>
      $key++;
      @endforeach
    </table>

    <div class="row">
      <div class="col-md-12">
        <table style="width:100%" id="table-ab" class="col-md-6">
          <div class="row">
            <div class="col-md-12">
              <table style="width:100%" id="table-ab" class="col-md-12">
                <tr>
                  <th >Billed By:</th>
                  <td>{{$customer_details->customer_name}}." ".{{$date}}</td>
                  <th align="right">Refunded Amount:</th>
                  <td>{{  $return_lists['product_tot_price'] }}</td>
                </tr>
      </table>
    </div>
  </div>
</div>
</div>
</center>
</body>
</html>