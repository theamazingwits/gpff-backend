<?php

namespace App\Http\Controllers;

use Closure;
use DB;
use Mail;
use Carbon\Carbon;
class Branch
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        return $next($request);
    }

    ////////////////////////////
//Branch Managment Api Calls//
////////////////////////////
    //Insert Branch Data
    public function storeBranch($branch) 
    {   

        $values = array(
            'branch_name' => $branch->branch_name, 
            'branch_description'  => $branch->branch_description,
            'branch_address'  => $branch->branch_address , 
            'branch_currency'     => $branch->branch_currency ,
            'branch_country'    => $branch->branch_country ,
            'branch_landline' => $branch->branch_landline,
            'branch_contact' =>$branch->branch_contact,
            'country_code'  => $branch->country_code,
            'country' => $branch->country,
            'created_at'=> date('Y-m-d H:i:s') , 
            'updated_at'=> date('Y-m-d H:i:s')
        );
    
        return  DB::table('gpff_branch')
                ->insert($values);
    }
    //Update Branch details
    public function updateBranch($branch) 
    {
        $values = array(
            'branch_name' => $branch->branch_name, 
            'branch_description'  => $branch->branch_description,
            'branch_address'  => $branch->branch_address , 
            'branch_currency'     => $branch->branch_currency ,
            'branch_country'    => $branch->branch_country ,
            'branch_landline' => $branch->branch_landline,
            'branch_contact' =>$branch->branch_contact,
            'country_code'  => $branch->country_code,
            'country' => $branch->country,
            'updated_at'=> date('Y-m-d H:i:s')
            );

        return  DB::table('gpff_branch')
                ->where('branch_id', $branch->branch_id)
                ->update($values);
    }

    //Delete Branch details
    public function deleteBranch($branch)
    {
        return  DB::table('gpff_branch')
                ->where('branch_id', $branch->branch_id)
                ->delete();
    }

    //Active Deactive Branch Details
    public function branchActiveOrDeactive($branch) 
    {
        $values = array(
                'branch_status'  => $branch->branch_status ,  
                'updated_at' => date('Y-m-d H:i:s')
             );

        return DB::table('gpff_branch')
        ->where('branch_id', $branch->branch_id)
        ->update($values);  
    }

    public function getAllBranch() 
    {
        $data =[];
        $data['totaldata']= DB::table('gpff_branch')
                            ->orderBy('branch_name','ASC')
                            ->get();
         return $data;   
    }

    //Get Indivitual Branch Details
    public function getIndBranch($branch)
    {   
        return  DB::table('gpff_branch')
                ->where('branch_id', $branch->branch_id)
                ->get();
    }

    //Get Manager Id based Branch Details
    public function getIdBasedBranch($branch)
    {   

        $admin = DB::table('gpff_users')
                    ->where('user_id', $branch->manager_id)
                    ->First();

            $tot = [];
                $myArray = explode(',', $admin->branch_id);
                $arr = DB::table('gpff_branch')
                    ->whereIn('branch_id', $myArray)
                    ->orderBy('branch_name','ASC')
                    ->get();
                array_push($tot, $arr);
            return  $arr;
    }

////////////////////////////
//Region Managment Api Calls//
////////////////////////////
    //Insert Region Data
    public function storeRegion($branch) 
    {   

        $values = array(
            'region_name' => $branch->region_name, 
            'region_address'  => $branch->region_address,
            'region_description'  => $branch->region_description , 
            'region_contact'     => $branch->region_contact ,
            'region_landline'    => $branch->region_landline ,
            'branch_id' => $branch->branch_id,
            'country_code'  => $branch->country_code,
            'country' => $branch->country,
            'created_at'=> date('Y-m-d H:i:s') , 
            'updated_at'=> date('Y-m-d H:i:s')
        );
    
        return  DB::table('gpff_region')
                ->insert($values);
    }
    //Update Region details
    public function updateRegion($branch) 
    {
        $values = array(
            'region_name' => $branch->region_name, 
            'region_address'  => $branch->region_address,
            'region_description'  => $branch->region_description , 
            'region_contact'     => $branch->region_contact ,
            'region_landline'    => $branch->region_landline ,
            'branch_id' => $branch->branch_id,
            'country_code'  => $branch->country_code,
            'country' => $branch->country,
            'updated_at'=> date('Y-m-d H:i:s')
            );

        return  DB::table('gpff_region')
                ->where('region_id', $branch->region_id)
                ->update($values);
    }
    
    //Delete Branch details
    public function deleteRegion($branch)
    {
        return  DB::table('gpff_region')
                ->where('region_id', $branch->region_id)
                ->delete();
    }
    //Active Deactive Branch Details
    public function regionActiveOrDeactive($branch) 
    {
        $values = array(
                'region_status'  => $branch->region_status ,  
                'updated_at' => date('Y-m-d H:i:s')
             );

        return DB::table('gpff_region')
        ->where('region_id', $branch->region_id)
        ->update($values);  
    }

    public function getAllRegion() 
    {
        $data =[];
        $data['totaldata']= DB::table('gpff_region as gpre')
                            ->join('gpff_branch as gpbr','gpre.branch_id', '=' , 'gpbr.branch_id')
                            ->orderBy('gpre.updated_at','DESC')
                            ->get();
         return $data;   
    }

    //Get Indivitual Region Details
    public function getIndRegion($branch)
    {   
        return  DB::table('gpff_region')
                ->where('region_id', $branch->region_id)
                ->get();
    }


    //Get Manager ID based Region Details
    public function getIdBasedRegion($branch)
    {   

        $admin = DB::table('gpff_users')
                    ->where('user_id', $branch->manager_id)
                    ->First();

            $tot = [];
                $myArray = explode(',', $admin->region_id);
                $arr = DB::table('gpff_region')
                    ->whereIn('region_id', $myArray)
                    ->orderBy('region_name','ASC')
                    ->get();
                array_push($tot, $arr);
            return  $arr;
    }

////////////////////////////
//Area Managment Api Calls//
////////////////////////////
    //Insert Area Data
    public function storeArea($branch) 
    {   

        $values = array(
            'area_name' => $branch->area_name, 
            'area_address'  => $branch->area_address,
            'area_description'  => $branch->area_description , 
            'area_contact'     => $branch->area_contact ,
            'area_landline'    => $branch->area_landline ,
            'branch_id' => $branch->branch_id,
            'region_id' => $branch->region_id,
            'country_code'  => $branch->country_code,
            'country' => $branch->country,
            'pin_code'=> $branch->pin_code,
            'created_at'=> date('Y-m-d H:i:s') , 
            'updated_at'=> date('Y-m-d H:i:s')
        );
    
        return  DB::table('gpff_area')
                ->insert($values);
    }
    //Update Area details
    public function updateArea($branch) 
    {
        $values = array(
            'area_name' => $branch->area_name, 
            'area_address'  => $branch->area_address,
            'area_description'  => $branch->area_description , 
            'area_contact'     => $branch->area_contact ,
            'area_landline'    => $branch->area_landline ,
            'branch_id' => $branch->branch_id,
            'region_id' => $branch->region_id,
            'country_code'  => $branch->country_code,
            'country' => $branch->country,
            'pin_code'=> $branch->pin_code,
            'updated_at'=> date('Y-m-d H:i:s')
        );

        return  DB::table('gpff_area')
                ->where('area_id', $branch->area_id)
                ->update($values);
    }
    
    //Delete Area details
    public function deleteArea($branch)
    {
        return  DB::table('gpff_area')
                ->where('area_id', $branch->area_id)
                ->delete();
    }

    //Active Deactive Area Details
    public function areaActiveOrDeactive($branch) 
    {
        $values = array(
                'area_status'  => $branch->area_status ,  
                'updated_at' => date('Y-m-d H:i:s')
             );

        return DB::table('gpff_area')
        ->where('area_id', $branch->area_id)
        ->update($values);  
    }

    public function getAllArea() 
    {
        $data =[];
        $data['totaldata']= DB::table('gpff_area as gpar')
                            ->join('gpff_branch as gpbr','gpar.branch_id', '=' , 'gpbr.branch_id')
                            ->join('gpff_region as gpre','gpar.region_id', '=' , 'gpre.region_id')
                            ->orderBy('gpar.updated_at','DESC')
                            ->get();
         return $data;   
    }

    //Get Indivitual Area Details
    public function getIndArea($branch)
    {   
        return  DB::table('gpff_area')
                ->where('area_id', $branch->area_id)
                ->get();
    }

      //Get Manager ID based Area Details
    public function getIdBasedArea($branch)
    {   
        $admin = DB::table('gpff_users')
                    ->where('user_id', $branch->manager_id)
                    ->First();

            $tot = [];
                $myArray = explode(',', $admin->area_id);
                $arr = DB::table('gpff_area')
                    ->whereIn('area_id', $myArray)
                    ->orderBy('updated_at','DESC')
                    ->get();
                array_push($tot, $arr);
            return  $arr;
            
/*        $admin = DB::table('gpff_users')
                ->where('user_id', $branch->manager_id)
                ->First();

        $area_ids = explode(', ', $admin->area_id);
        $data =[];
        
        $data['totaldata'] = DB::table('gpff_area')
                ->whereIn('area_id', $area_ids)
                ->get();
      
        return  $data;*/
    }

    //Area Based Field Details Fetch 
    public function getAreaBasedField($branch)
    {   
        $region = DB::table('gpff_area')
                    ->where('area_id', $branch->area_id)
                    ->First(['region_id']);

        $fouser = DB::table('gpff_users')
                ->where('region_id', $region->region_id)
                ->where('role', 5)
                ->get();

        $tot = [];
        foreach ($fouser as $value) {
            $myArray = explode(',', $value->area_id);
            if (in_array($branch->area_id, $myArray)) {
                array_push($tot, $value);
            }
            /*$myArray = explode(',', $value->area_id);
            foreach ($myArray as $value1) {
                if($branch->area_id == $value1){
                    array_push($tot, $value);
                }  
            }*/
        }
        return  $tot;
        /*return  DB::table('gpff_users')
                ->whereIn('area_id', [$branch->area_id])
                ->where('role', 5)
                ->get();*/

    }
    //Multi Area Based Field Details Fetch 
    public function getMultiAreaBasedField($branch)
    {   
        $region = DB::table('gpff_area')
                    ->whereIn('area_id', [$branch->area_id])
                    ->get(['region_id']);

        $reid = [];
        foreach ($region as $val) {
            $reid[] = $val->region_id;
        }

        $fouser = DB::table('gpff_users')
                ->whereIn('region_id', $reid)
                ->where('role', 5)
                ->orderBy('firstname','ASC')
                ->get();

        $tot = [];
        foreach ($fouser as $value) {
            $myArray = explode(',', $value->area_id);
            if (array_intersect($branch->area_id, $myArray)) {
                array_push($tot, $value);
            }
        }
        return  $tot;
    }
    //Area Based Field Details Fetch 
    public function getAreabasFO($branch)
    {   

        return  DB::table('gpff_users')
                ->where('area_manager_id', $branch->user_id)
                ->where('role', 5)
                ->orderBy('updated_at','DESC')
                ->get();

        /*$data = DB::table('gpff_users')
                    ->where('user_id', $branch->user_id)
                    ->First(['area_id']);

        $myArray = explode(',', $data->area_id);

        return  DB::table('gpff_users as guse')
                ->join('gpff_area as garea','guse.area_id','garea.area_id')
                ->whereIn('guse.area_id', $myArray)
                ->where('guse.role', 5)
                ->orderBy('guse.updated_at','DESC')
                ->get();*/
    }

////////////////////////////
//Warehouse Managment Api Calls//
////////////////////////////
    //Insert Warehouse Data
    public function storeWarehouse($branch) 
    {   
        $exit = DB::table('gpff_warehouse')
                ->where('stockist_id', $branch->stockist_id)
                ->get();

        if(count($exit) > 0){
            return 2;
        }
        else{
        $values = array(
            'warehouse_name' => $branch->warehouse_name, 
            'branch_id'  => $branch->branch_id,
            'region_id'  => $branch->region_id , 
            // 'area_id'     => json_encode($branch->area_id),
            'area_id'     => $branch->area_id,
            'warehouse_cr_id'    => $branch->warehouse_cr_id ,
            'stockist_id' => $branch->stockist_id,
            'warehouse_lan' => $branch->warehouse_lan,
            'warehouse_lat' => $branch->warehouse_lat,
            'warehouse_address' => $branch->warehouse_address,
            'warehouse_contact' => $branch->warehouse_contact,
            'country_code'  => $branch->country_code,
            'warehouse_type'  => $branch->warehouse_type,
            'main_warehouse_id'  => $branch->main_warehouse_id,
            'main_warehouse_name'  => $branch->main_warehouse_name,
            'warehouse_landline'  => $branch->warehouse_landline,
            'country' => $branch->country,
            'created_at'=> date('Y-m-d H:i:s') , 
            'updated_at'=> date('Y-m-d H:i:s')
        );
        // print_r($values);
    
        $wareid =  DB::table('gpff_warehouse')
                ->insertGetId($values);

        // print_r($wareid);
        
        $warevalues = array(
            'warehouse_id' => $wareid,
            'warehouse_type'  => $branch->warehouse_type,
            'updated_at'=> date('Y-m-d H:i:s')
        );


        DB::table('gpff_users')
        ->where('user_id', $branch->stockist_id)
        ->update($warevalues);

        return 1;
        }
    }
    //Update Warehouse details
    public function updateWarehouse($branch) 
    {   

        $warehousede = DB::table('gpff_warehouse')
                    ->where('warehouse_id', $branch->warehouse_id)
                    ->First();

        if($warehousede->stockist_id == $branch->stockist_id){
            $warevalues = array(
                'warehouse_id' => $branch->warehouse_id,
                'updated_at'=> date('Y-m-d H:i:s')
            );
            DB::table('gpff_users')
            ->where('user_id', $branch->stockist_id)
            ->update($warevalues);

        }else{
            $warevalues = array(
                'warehouse_id' => $branch->warehouse_id,
                'updated_at'=> date('Y-m-d H:i:s')
            );
            DB::table('gpff_users')
        ->where('user_id', $branch->stockist_id)
        ->update($warevalues);

        $oldwarevalues = array(
                'warehouse_id' => "",
                'updated_at'=> date('Y-m-d H:i:s')
            );
            DB::table('gpff_users')
        ->where('user_id', $warehousede->stockist_id)
        ->update($warevalues);

        }
        $values = array(
             'warehouse_name' => $branch->warehouse_name, 
            'branch_id'  => $branch->branch_id,
            'region_id'  => $branch->region_id , 
            'area_id'     => $branch->area_id ,
            'warehouse_cr_id'    => $branch->warehouse_cr_id ,
            'stockist_id' => $branch->stockist_id,
            'warehouse_lan' => $branch->warehouse_lan,
            'warehouse_lat' => $branch->warehouse_lat,
            'warehouse_address' => $branch->warehouse_address,
            'warehouse_contact' => $branch->warehouse_contact,
            'country_code'  => $branch->country_code,
            'warehouse_type'  => $branch->warehouse_type,
            'main_warehouse_id'  => $branch->main_warehouse_id,
            'main_warehouse_name'  => $branch->main_warehouse_name,
            'warehouse_landline'  => $branch->warehouse_landline,
            'country' => $branch->country,
            'updated_at'=> date('Y-m-d H:i:s')
            );
        
        return  DB::table('gpff_warehouse')
                ->where('warehouse_id', $branch->warehouse_id)
                ->update($values);
    }
    
    //Delete Warehouse details
    public function deleteWarehouse($branch)
    {   
        $exit = DB::table('gpff_warehouse')
                ->where('warehouse_id', $Products->warehouse_id)
                ->First();

        $warevalues = array(
            'warehouse_id' => "",
            'updated_at'=> date('Y-m-d H:i:s')
        );

        DB::table('gpff_users')
        ->where('user_id', $exit->stockist_id)
        ->update($warevalues);

        return  DB::table('gpff_warehouse')
                ->where('warehouse_id', $branch->warehouse_id)
                ->delete();
    }

    //Active Deactive Warehouse Details
    public function warehouseActiveOrDeactive($branch) 
    {
        $values = array(
                'warehouse_status'  => $branch->warehouse_status ,  
                'updated_at' => date('Y-m-d H:i:s')
             );

        return DB::table('gpff_warehouse')
        ->where('warehouse_id', $branch->warehouse_id)
        ->update($values);  
    }

    public function getAllWarehouse() 
    {
        $data =[];
        $data['totaldata']= DB::table('gpff_warehouse as gpwar')
                            ->leftjoin('gpff_branch as gpbr','gpwar.branch_id', '=' , 'gpbr.branch_id')
                            ->leftjoin('gpff_region as gpre','gpwar.region_id', '=' , 'gpre.region_id')
                            /*->leftjoin('gpff_area as gpar','gpwar.area_id', '=' , 'gpar.area_id')*/
                            ->leftjoin('gpff_users as gpusr','gpwar.stockist_id', '=' , 'gpusr.user_id')
                            ->orderBy('gpwar.updated_at','DESC')
                            ->get();
         return $data;   
    }

    //Get Indivitual Warehouse Details
    public function getIndWarehouse($branch)
    {   
        return  DB::table('gpff_warehouse as gw')
                ->where('gw.warehouse_id', $branch->warehouse_id)
                ->leftjoin('gpff_users as use','gw.stockist_id','use.user_id')
                ->get(['gw.warehouse_id','gw.warehouse_name','gw.branch_id','gw.region_id','gw.area_id','gw.warehouse_cr_id','gw.stockist_id','use.firstname','gw.warehouse_lan','gw.warehouse_lat','gw.warehouse_address','gw.warehouse_contact','gw.country_code','gw.country','gw.warehouse_type','gw.main_warehouse_id','gw.main_warehouse_name','gw.warehouse_status','gw.created_at','gw.updated_at']);
    }

    // Get Main Based Sub Warehouse Details
    public function getMainBaseSubWarehouse($branch)
    {   
        return  DB::table('gpff_warehouse')
                ->where('main_warehouse_id', $branch->warehouse_id)
                ->where('warehouse_type', 2)
                ->get();
    }

    //Get All Stockist Details
    public function getAllStockist(){

        $data =[];
        $data['totaldata']= DB::table('gpff_users')
                            ->where('role','7')
                            ->where('is_active','1')
                            ->get();
         return $data; 
    }


    //Get Region Based Warehouse Details
    public function getRegionBasWare($branch)
    {   
        return  DB::table('gpff_warehouse')
                ->where('region_id', $branch->region_id)
                ->get();
    }
    //Get Region Based Main Warehouse Details
    public function getRegionBasMainWare($branch)
    {   
        return  DB::table('gpff_warehouse')
                ->where('region_id', $branch->region_id)
                ->where('warehouse_type',1)
                ->get();
    }
    //Get Region Based Stock Request Warehouse Details
    public function getRegionBasStockRequestWare($branch)
    {   
        return  DB::table('gpff_warehouse')
                ->where('region_id', $branch->region_id)
                ->whereNotIn('warehouse_id',[$branch->warehouse_id])
                ->get();
    }
    
    //Get Branch Based Region Details
    public function getBranchBaseReg($branch)
    {   
        return  DB::table('gpff_region')
                ->where('branch_id', $branch->branch_id)
                ->orderBy('region_name','ASC')
                ->get();
    }
    //Get Region Based Are Details
    public function getRegionBasArea($branch)
    {   
        return  DB::table('gpff_area')
                ->where('region_id', $branch->region_id)
                ->orderBy('area_name','ASC')
                ->get();
    }

    //Get Region Based Stockist Details
    public function getRegionBasStockist($branch)
    {   
        return  DB::table('gpff_users')
                ->where('role','7')
                ->where('is_active','1')
                ->where('region_id', $branch->region_id)
                ->get();
    }

    //Get Region Based Stockist Details
    public function getRegionBasAvaiStockist($branch)
    {   
        return  DB::table('gpff_users')
                ->where('role','7')
                ->where('is_active','1')
                ->where('region_id', $branch->region_id)
                ->where('warehouse_id',"")
                ->get();
    }


    //New Calls For (May 27) 
    //1-Region List,2-Area List,3-Warehouse List,4-Region Manager List,5-Area Manager List,6-Field Officer List,7-Stockist List,8-Customer List,9-Product,10-Sample,11-Gift,12-Order,13-alltask,14-current date task
    //Branch AdminBased Details Fetch ,15-finance user,15-checker
    public function getBranchAdminBaseDetails($branch)
    {   
        $data = DB::table('gpff_users')
                    ->where('user_id', $branch->user_id)
                    ->First(['branch_id']);

        $myArray = explode(',', $data->branch_id);

        if($branch->type == 1){
            return  DB::table('gpff_region as gpreg')
                    ->join('gpff_branch as gpbr','gpreg.branch_id','gpbr.branch_id')
                    ->whereIn('gpreg.branch_id', $myArray)
                    ->orderBy('gpreg.updated_at','DESC')
                    ->get(['gpreg.region_id','gpreg.region_name','gpreg.region_address','gpreg.region_description','gpreg.region_status','gpreg.region_contact','gpreg.country_code','gpreg.country','gpreg.region_landline','gpreg.branch_id','gpreg.created_at','gpreg.updated_at','gpbr.branch_name']);

        } elseif($branch->type == 2){
            return  DB::table('gpff_area as gpare')
                    ->join('gpff_branch as gpbr','gpare.branch_id','gpbr.branch_id')
                    ->join('gpff_region as gpreg','gpare.region_id','gpreg.region_id')
                    ->whereIn('gpare.branch_id', $myArray)
                    ->orderBy('gpare.updated_at','DESC')
                    ->get(['gpare.area_id','gpare.area_name','gpare.area_address','gpare.area_description','gpare.area_status','gpare.area_contact','gpare.country_code','gpare.country','gpare.area_landline','gpare.pin_code','gpare.branch_id','gpare.region_id','gpare.created_at','gpare.updated_at','gpbr.branch_name','gpreg.region_name']);

        } elseif($branch->type == 3){
            return  DB::table('gpff_warehouse as gpwar')
                    ->join('gpff_branch as gpbr','gpwar.branch_id','gpbr.branch_id')
                    ->join('gpff_region as gpreg','gpwar.region_id','gpreg.region_id')
                    ->whereIn('gpwar.branch_id', $myArray)
                    ->orderBy('gpwar.updated_at','DESC')
                    ->get(['gpwar.warehouse_id','gpwar.warehouse_name','gpwar.branch_id','gpwar.region_id','gpwar.area_id','gpwar.warehouse_cr_id','gpwar.stockist_id','gpwar.warehouse_lan','gpwar.warehouse_lat','gpwar.warehouse_address','gpwar.warehouse_contact','gpwar.country_code','gpwar.country','gpwar.warehouse_status','gpwar.main_warehouse_id','gpwar.warehouse_type','gpwar.main_warehouse_name','gpwar.created_at','gpwar.updated_at','gpbr.branch_name','gpreg.region_name']);

        } elseif($branch->type == 4){
            return  DB::table('gpff_users as gpuse')
                    ->join('gpff_branch as gpbr','gpuse.branch_id','gpbr.branch_id')
                    ->whereIn('gpuse.branch_id', $myArray)
                    ->orderBy('gpuse.updated_at','DESC')
                    ->where('gpuse.role', 3)
                    ->get(['gpuse.user_id','gpuse.firstname','gpuse.lastname','gpuse.username','gpuse.email','gpuse.role','gpuse.is_active','gpuse.online_status','gpuse.city','gpuse.country','gpuse.language','gpuse.address','gpuse.country_code','gpuse.mobile','gpuse.avatar','gpuse.permission','gpuse.branch_id','gpuse.region_id','gpuse.cr_by_id','gpuse.created_at','gpuse.updated_at','gpbr.branch_name']);

        } elseif($branch->type == 5){
            return  DB::table('gpff_users as gpuse')
                    ->join('gpff_branch as gpbr','gpuse.branch_id','gpbr.branch_id')
                    ->join('gpff_region as gpreg','gpuse.region_id','gpreg.region_id')
                    ->whereIn('gpuse.branch_id', $myArray)
                    ->orderBy('gpuse.updated_at','DESC')
                    ->where('gpuse.role', 4)
                    ->get(['gpuse.user_id','gpuse.firstname','gpuse.lastname','gpuse.username','gpuse.email','gpuse.role','gpuse.is_active','gpuse.online_status','gpuse.city','gpuse.country','gpuse.language','gpuse.address','gpuse.country_code','gpuse.mobile','gpuse.avatar','gpuse.permission','gpuse.branch_id','gpuse.region_id','gpuse.area_id','gpuse.cr_by_id','gpuse.created_at','gpuse.updated_at','gpbr.branch_name','gpreg.region_name']);

        } elseif($branch->type == 6){
            return  DB::table('gpff_users as gpuse')
                    ->join('gpff_branch as gpbr','gpuse.branch_id','gpbr.branch_id')
                    ->join('gpff_region as gpreg','gpuse.region_id','gpreg.region_id')
                    //->join('gpff_area as gpare','gpuse.area_id','gpare.area_id')
                    ->whereIn('gpuse.branch_id', $myArray)
                    ->orderBy('gpuse.updated_at','DESC')
                    ->where('gpuse.role', 5)
                    ->get(['gpuse.user_id','gpuse.firstname','gpuse.lastname','gpuse.username','gpuse.email','gpuse.role','gpuse.is_active','gpuse.online_status','gpuse.city','gpuse.country','gpuse.language','gpuse.address','gpuse.country_code','gpuse.mobile','gpuse.avatar','gpuse.permission','gpuse.branch_id','gpuse.region_id','gpuse.area_id','gpuse.cr_by_id','gpuse.created_at','gpuse.updated_at','gpuse.user_attendance_status','gpbr.branch_name','gpreg.region_name']);

        } elseif($branch->type == 7){
            return  DB::table('gpff_users as gpuse')
                    ->join('gpff_branch as gpbr','gpuse.branch_id','gpbr.branch_id')
                    ->join('gpff_region as gpreg','gpuse.region_id','gpreg.region_id')
                    ->whereIn('gpuse.branch_id', $myArray)
                    ->orderBy('gpuse.updated_at','DESC')
                    ->where('gpuse.role', 7)
                    ->get(['gpuse.user_id','gpuse.firstname','gpuse.lastname','gpuse.username','gpuse.email','gpuse.role','gpuse.is_active','gpuse.online_status','gpuse.city','gpuse.country','gpuse.language','gpuse.address','gpuse.country_code','gpuse.mobile','gpuse.avatar','gpuse.permission','gpuse.branch_id','gpuse.region_id','gpuse.cr_by_id','gpuse.created_at','gpuse.updated_at','gpbr.branch_name','gpreg.region_name']);

        } elseif($branch->type == 8){
            return  DB::table('gpff_customer as gpcus')
                    ->join('gpff_branch as gpbr','gpcus.branch_id','gpbr.branch_id')
                    ->join('gpff_region as gpreg','gpcus.region_id','gpreg.region_id')
                    ->where('vendor', $branch->vendor)
                    ->whereIn('gpcus.branch_id', $myArray)
                    ->orderBy('gpcus.updated_at','DESC')
                ->get(['gpcus.customer_id','gpcus.customer_login_id','gpcus.customer_name','gpcus.customer_email','gpcus.country_code','gpcus.customer_mobile','gpcus.customer_city','gpcus.customer_country','gpcus.customer_address','gpcus.customer_location','gpcus.customer_lan','gpcus.customer_lat','gpcus.customer_cr_id','gpcus.credit_limit','gpcus.area_manager_id','gpcus.region_manager_id','gpcus.branch_id','gpcus.region_id','gpcus.area_id','gpcus.website','gpcus.avatar','gpcus.customer_type','gpcus.is_active','gpcus.customer_emp_id','gpcus.created_at','gpcus.updated_at','gpbr.branch_name','gpreg.region_name']);

        } elseif($branch->type == 9){
            return  DB::table('gpff_product')
                    ->whereIn('branch_id', $myArray)
                    ->orderBy('updated_at','DESC')
                ->get();

        } elseif($branch->type == 10){
            return  DB::table('gpff_samples as gpsam')
                    ->join('gpff_product as gppr','gpsam.product_id','gppr.product_id')
                    ->join('gpff_branch as gpbr','gpsam.branch_id','gpbr.branch_id')
                    ->join('gpff_region as gpreg','gpsam.region_id','gpreg.region_id')
                    ->join('gpff_users as gpuse','gpsam.stockist_id','gpuse.user_id')
                    ->join('gpff_warehouse as gpwar','gpsam.warehouse_id','gpwar.warehouse_id')
                    ->whereIn('gpsam.branch_id', $myArray)
                    ->orderBy('gpsam.updated_at','DESC')
                ->get(['gpsam.samples_id','gpsam.product_id','gpsam.category_id','gpsam.category_name','gpsam.warehouse_id','gpwar.warehouse_name','gpsam.samples_name','gpsam.samples_description','gpsam.samples_tot_qty','gpsam.samples_blc_qty','gpsam.samples_sales_qty','gpsam.samples_cr_id','gpsam.stockist_id','gpuse.firstname','gpuse.lastname','gpsam.branch_id','gpsam.region_id','gpsam.area_id','gpsam.samples_status','gpsam.created_at','gpsam.updated_at','gpbr.branch_name','gpreg.region_name','gppr.product_netprice','gppr.product_grossprice','gppr.product_discount']);

        } elseif($branch->type == 11){
            return  DB::table('gpff_gift as gpgif')
                    ->join('gpff_branch as gpbr','gpgif.branch_id','gpbr.branch_id')
                    ->join('gpff_region as gpreg','gpgif.region_id','gpreg.region_id')
                    ->whereIn('gpgif.branch_id', $myArray)
                    ->orderBy('gpgif.updated_at','DESC')
                    ->get(['gpgif.gift_id','gpgif.gift_name','gpgif.gift_description','gpgif.gift_cr_id','gpgif.branch_id','gpgif.region_id','gpgif.region_name','gpgif.area_id','gpgif.gift_status','gpgif.created_at','gpgif.updated_at','gpbr.branch_name','gpreg.region_name']);

        } elseif($branch->type == 12){
            return  DB::table('gpff_order as gpor')
                    ->join('gpff_branch as gpbr','gpor.branch_id','gpbr.branch_id')
                    ->join('gpff_region as gpreg','gpor.region_id','gpreg.region_id')
                    ->join('gpff_area as gpare','gpor.area_id','gpare.area_id')
                    ->join('gpff_warehouse as gpwar','gpor.warehouse_id','gpwar.warehouse_id')
                    ->whereIn('gpor.branch_id', $myArray)
                    ->orderBy('gpor.updated_at','DESC')
                ->get(['gpor.order_id','gpor.task_id','gpor.field_officer_id','gpor.field_officer_name','gpor.area_manager_id','gpor.customer_id','gpor.customer_name','gpor.order_date','gpor.or_tot_price','gpor.branch_id','gpor.region_id','gpor.area_id','gpor.warehouse_id','gpwar.warehouse_name','gpor.order_status','gpor.created_at','gpor.updated_at','gpbr.branch_name','gpreg.region_name','gpare.area_name','gpor.payment_type','gpor.or_type','gpor.invoice_type']);

        } elseif($branch->type == 13){
            return  DB::table('gpff_task as gta')
            ->join('gpff_customer as gcus','gta.customer_id', '=' , 'gcus.customer_id')
            ->join('gpff_users as guse','gta.field_officer_id', '=' , 'guse.user_id')
            ->whereIn('gta.branch_id', $myArray)
            ->orderBy('gta.created_at', 'DESC')
            ->get(['gta.task_id','gta.created_by','gta.field_officer_id','guse.firstname','gta.customer_id','gta.area_manager_id','gta.task_title','gta.task_desc','gta.task_status','gta.task_date_time','gta.task_reassign_status','gta.task_start_date_time','gta.task_end_date_time','gta.task_active_minu','gta.notes','gta.created_at','gta.updated_at','gcus.customer_name','gcus.customer_email','gcus.country_code','gcus.customer_mobile','gcus.customer_city','gcus.customer_country','gcus.customer_fax','gcus.customer_address','gcus.customer_location','gcus.customer_lan','gcus.customer_lat','gcus.customer_cr_id','gcus.website','gcus.avatar','gcus.is_active','gta.branch_id','gta.region_id','gta.area_id']);

        } elseif($branch->type == 14){

            $today = date('Y-m-d') . '%';

            $query = DB::table('gpff_task as gta')
                ->join('gpff_customer as gcus', 'gta.customer_id', '=', 'gcus.customer_id')
                ->join('gpff_users as guse', 'gta.field_officer_id', '=', 'guse.user_id')
                ->where('gta.task_date_time', 'like', $today)
                ->whereIn('gta.branch_id', $myArray)
                ->orderBy('gta.created_at', 'ASC')
                ->limit(10)
                ->select('gta.task_date_time', 'gta.created_at', 'gta.updated_at', 'gcus.customer_name', 'gcus.customer_location')
                ->get();
            
            return $query;
            
        }elseif($branch->type == 15){
            return  DB::table('gpff_users as gpuse')
                    ->join('gpff_branch as gpbr','gpuse.branch_id','gpbr.branch_id')
                    ->join('gpff_region as gpreg','gpuse.region_id','gpreg.region_id')
                    ->whereIn('gpuse.branch_id', $myArray)
                    ->orderBy('gpuse.updated_at','DESC')
                    ->where('gpuse.role', 9)
                ->get(['gpuse.user_id','gpuse.firstname','gpuse.lastname','gpuse.username','gpuse.email','gpuse.role','gpuse.is_active','gpuse.online_status','gpuse.city','gpuse.country','gpuse.language','gpuse.address','gpuse.country_code','gpuse.mobile','gpuse.permission','gpuse.avatar','gpuse.branch_id','gpuse.region_id','gpuse.cr_by_id','gpuse.created_at','gpuse.updated_at','gpbr.branch_name','gpreg.region_name']);
        }elseif($branch->type == 16){
            return  DB::table('gpff_users as gpuse')
                    ->join('gpff_branch as gpbr','gpuse.branch_id','gpbr.branch_id')
                    ->join('gpff_region as gpreg','gpuse.region_id','gpreg.region_id')
                    ->whereIn('gpuse.branch_id', $myArray)
                    ->orderBy('gpuse.updated_at','DESC')
                    ->where('gpuse.role', 10)
                ->get(['gpuse.user_id','gpuse.firstname','gpuse.lastname','gpuse.username','gpuse.email','gpuse.role','gpuse.is_active','gpuse.online_status','gpuse.city','gpuse.country','gpuse.language','gpuse.address','gpuse.country_code','gpuse.mobile','gpuse.permission','gpuse.avatar','gpuse.branch_id','gpuse.region_id','gpuse.cr_by_id','gpuse.created_at','gpuse.updated_at','gpbr.branch_name','gpreg.region_name']);
            }elseif($branch->type == 17){           //Auditor
            return  DB::table('gpff_users as gpuse')
                    ->join('gpff_branch as gpbr','gpuse.branch_id','gpbr.branch_id')
                    ->join('gpff_region as gpreg','gpuse.region_id','gpreg.region_id')
                    ->whereIn('gpuse.branch_id', $myArray)
                    ->orderBy('gpuse.updated_at','DESC')
                    ->where('gpuse.role', 11)
                ->get(['gpuse.user_id','gpuse.firstname','gpuse.lastname','gpuse.username','gpuse.email','gpuse.role','gpuse.is_active','gpuse.online_status','gpuse.city','gpuse.country','gpuse.language','gpuse.address','gpuse.country_code','gpuse.mobile','gpuse.permission','gpuse.avatar','gpuse.branch_id','gpuse.region_id','gpuse.cr_by_id','gpuse.created_at','gpuse.updated_at','gpbr.branch_name','gpreg.region_name']);
            }
    }
    //1-Area List,2-Warehouse List,3-Region Manager List,4-Area Manager List,5-Field Officer List,6-Stockist List,7-Customer List,8-Categories,9-Product,10-Sample,11-Gift,12-Order,13-alltask,14-current date task
    //Region Manager Based Details Fetch 
    // public function getRegionMangeBaseDetails($branch)
    // {   
    //     $data = DB::table('gpff_users')
    //                 ->where('user_id', $branch->user_id)
    //                 ->First(['region_id']);

    //     $myArray = explode(',', $data->region_id);

    //     if($branch->type == 1){
    //         return  DB::table('gpff_area as gpare')
    //                 ->join('gpff_branch as gpbr','gpare.branch_id','gpbr.branch_id')
    //                 ->join('gpff_region as gpreg','gpare.region_id','gpreg.region_id')
    //                 // ->where('vendor', $branch->vendor)
    //                 ->whereIn('gpare.region_id', $myArray)
    //                 ->orderBy('gpare.updated_at','DESC')
    //             ->get(['gpare.area_id','gpare.area_name','gpare.area_address','gpare.area_description','gpare.area_status','gpare.area_contact','gpare.country_code','gpare.country','gpare.area_landline','gpare.pin_code','gpare.branch_id','gpare.region_id','gpare.created_at','gpare.updated_at','gpbr.branch_name','gpreg.region_name']);

    //     } elseif($branch->type == 2){
    //         return  DB::table('gpff_warehouse as gpwar')
    //                 ->join('gpff_branch as gpbr','gpwar.branch_id','gpbr.branch_id')
    //                 ->join('gpff_region as gpreg','gpwar.region_id','gpreg.region_id')
    //                 ->join('gpff_users as gpuse','gpwar.stockist_id','gpuse.user_id')
    //                 // ->where('vendor', $branch->vendor)
    //             ->whereIn('gpwar.region_id', $myArray)
    //             ->orderBy('gpwar.updated_at','DESC')
    //             ->get(['gpwar.warehouse_id','gpwar.warehouse_name','gpwar.branch_id','gpwar.region_id','gpwar.area_id','gpwar.warehouse_cr_id','gpwar.stockist_id','gpuse.firstname','gpwar.warehouse_lan','gpwar.warehouse_lat','gpwar.warehouse_address','gpwar.warehouse_contact','gpwar.country_code','gpwar.country','gpwar.main_warehouse_id','gpwar.main_warehouse_name','gpwar.warehouse_type','gpwar.warehouse_status','gpwar.created_at','gpwar.updated_at','gpbr.branch_name','gpreg.region_name']);

    //     } elseif($branch->type == 3){
    //         return  DB::table('gpff_users as gpuse')
    //                 ->join('gpff_branch as gpbr','gpuse.branch_id','gpbr.branch_id')
    //                 // ->where('vendor', $branch->vendor)
    //             ->whereIn('gpuse.region_id', $myArray)
    //             ->orderBy('gpuse.updated_at','DESC')
    //             ->where('role', 3)
    //             ->get(['gpuse.user_id','gpuse.firstname','gpuse.lastname','gpuse.username','gpuse.email','gpuse.role','gpuse.is_active','gpuse.online_status','gpuse.city','gpuse.country','gpuse.language','gpuse.address','gpuse.country_code','gpuse.mobile','gpuse.permission','gpuse.avatar','gpuse.branch_id','gpuse.region_id','gpuse.cr_by_id','gpuse.created_at','gpuse.updated_at','gpbr.branch_name']);

    //     } elseif($branch->type == 4){
    //         return  DB::table('gpff_users as gpuse')
    //                 ->join('gpff_branch as gpbr','gpuse.branch_id','gpbr.branch_id')
    //                 ->join('gpff_region as gpreg','gpuse.region_id','gpreg.region_id')
    //                 // ->where('vendor', $branch->vendor)
    //                 ->whereIn('gpuse.region_id', $myArray)
    //                 ->orderBy('gpuse.updated_at','DESC')
    //                 ->where('gpuse.role', 4)
    //             ->get(['gpuse.user_id','gpuse.firstname','gpuse.lastname','gpuse.username','gpuse.email','gpuse.role','gpuse.is_active','gpuse.online_status','gpuse.city','gpuse.country','gpuse.language','gpuse.address','gpuse.country_code','gpuse.mobile','gpuse.permission','gpuse.avatar','gpuse.branch_id','gpuse.region_id','gpuse.area_id','gpuse.cr_by_id','gpuse.created_at','gpuse.updated_at','gpbr.branch_name','gpreg.region_name']);

    //     } elseif($branch->type == 5){
    //         return  DB::table('gpff_users as gpuse')
    //                 ->join('gpff_branch as gpbr','gpuse.branch_id','gpbr.branch_id')
    //                 ->join('gpff_region as gpreg','gpuse.region_id','gpreg.region_id')
    //                 //->join('gpff_area as gpare','gpuse.area_id','gpare.area_id')
    //                 // ->where('vendor', $branch->vendor)
    //                 ->whereIn('gpuse.region_id', $myArray)
    //                 ->orderBy('gpuse.updated_at','DESC')
    //                 ->where('gpuse.role', 5)
    //             ->get(['gpuse.user_id','gpuse.firstname','gpuse.lastname','gpuse.username','gpuse.email','gpuse.role','gpuse.is_active','gpuse.online_status','gpuse.city','gpuse.country','gpuse.language','gpuse.address','gpuse.country_code','gpuse.mobile','gpuse.permission','gpuse.avatar','gpuse.branch_id','gpuse.region_id','gpuse.area_id','gpuse.cr_by_id','gpuse.created_at','gpuse.updated_at','gpuse.user_attendance_status','gpbr.branch_name','gpreg.region_name']);

    //     } elseif($branch->type == 6){
    //         return  DB::table('gpff_users as gpuse')
    //                 ->join('gpff_branch as gpbr','gpuse.branch_id','gpbr.branch_id')
    //                 ->join('gpff_region as gpreg','gpuse.region_id','gpreg.region_id')
    //                 // ->where('vendor', $branch->vendor)
    //                 ->whereIn('gpuse.region_id', $myArray)
    //                 ->orderBy('gpuse.updated_at','DESC')
    //                 ->where('gpuse.role', 7)
    //             ->get(['gpuse.user_id','gpuse.firstname','gpuse.lastname','gpuse.username','gpuse.email','gpuse.role','gpuse.is_active','gpuse.online_status','gpuse.city','gpuse.country','gpuse.language','gpuse.address','gpuse.country_code','gpuse.mobile','gpuse.permission','gpuse.avatar','gpuse.branch_id','gpuse.region_id','gpuse.cr_by_id','gpuse.created_at','gpuse.updated_at','gpbr.branch_name','gpreg.region_name']);

    //     } elseif($branch->type == 7){
    //         return  DB::table('gpff_customer as gpcus')
    //                 ->join('gpff_branch as gpbr','gpcus.branch_id','gpbr.branch_id')
    //                 ->join('gpff_region as gpreg','gpcus.region_id','gpreg.region_id')
    //                 ->where('vendor', $branch->vendor)
    //             ->whereIn('gpcus.region_id', $myArray)
    //             ->orderBy('gpcus.updated_at','DESC')
    //             ->get(['gpcus.customer_id','gpcus.customer_login_id','gpcus.customer_name','gpcus.customer_email','gpcus.country_code','gpcus.customer_mobile','gpcus.customer_city','gpcus.customer_country','gpcus.customer_address','gpcus.customer_location','gpcus.customer_lan','gpcus.customer_lat','gpcus.customer_cr_id','gpcus.credit_limit','gpcus.area_manager_id','gpcus.region_manager_id','gpcus.branch_id','gpcus.region_id','gpcus.area_id','gpcus.website','gpcus.avatar','gpcus.customer_type','gpcus.is_active','gpcus.customer_emp_id','gpcus.created_at','gpcus.updated_at','gpbr.branch_name','gpreg.region_name']);

    //     } elseif($branch->type == 8){
    //         return  DB::table('gpff_category as gpcat')
    //                 ->join('gpff_branch as gpbr','gpcat.branch_id','gpbr.branch_id')
    //                 ->join('gpff_region as gpreg','gpcat.region_id','gpreg.region_id')
    //                 // ->where('vendor', $branch->vendor)
    //             ->whereIn('gpcat.region_id', $myArray)
    //             ->orderBy('gpcat.updated_at','DESC')
    //             ->get(['gpcat.category_id','gpcat.category_name','gpcat.branch_id','gpcat.region_id','gpcat.category_status','gpcat.created_at','gpcat.updated_at','gpcat.area_id','gpbr.branch_name','gpreg.region_name']);

    //     } elseif($branch->type == 9){
    //         return  DB::table('gpff_product')
    //             // ->where('vendor', $branch->vendor)
    //             ->whereIn('region_id', $myArray)
    //             ->orderBy('updated_at','DESC')
    //             ->get();

    //     } elseif($branch->type == 10){
    //         return  DB::table('gpff_samples as gpsam')
    //                 ->join('gpff_product as gppr','gpsam.product_id','gppr.product_id')
    //                 ->join('gpff_branch as gpbr','gpsam.branch_id','gpbr.branch_id')
    //                 ->join('gpff_region as gpreg','gpsam.region_id','gpreg.region_id')
    //                 ->join('gpff_users as gpuse','gpsam.stockist_id','gpuse.user_id')
    //                 ->join('gpff_warehouse as gpwar','gpsam.warehouse_id','gpwar.warehouse_id')
    //                 // ->where('vendor', $branch->vendor)
    //                 ->whereIn('gpsam.region_id', $myArray)
    //                 ->orderBy('gpsam.updated_at','DESC')
    //             ->get(['gpsam.samples_id','gpsam.product_id','gpsam.category_id','gpsam.category_name','gpsam.warehouse_id','gpwar.warehouse_name','gpsam.samples_name','gpsam.samples_description','gpsam.samples_tot_qty','gpsam.samples_blc_qty','gpsam.samples_sales_qty','gpsam.samples_cr_id','gpsam.stockist_id','gpuse.firstname','gpuse.lastname','gpsam.branch_id','gpsam.region_id','gpsam.area_id','gpsam.samples_status','gpsam.created_at','gpsam.updated_at','gpbr.branch_name','gpreg.region_name','gppr.product_netprice','gppr.product_grossprice','gppr.product_discount']);

    //     } elseif($branch->type == 11){
    //         return  DB::table('gpff_gift as gpgif')
    //                 ->join('gpff_branch as gpbr','gpgif.branch_id','gpbr.branch_id')
    //                 ->join('gpff_region as gpreg','gpgif.region_id','gpreg.region_id')
    //                 // ->where('vendor', $branch->vendor)
    //             ->whereIn('gpgif.region_id', $myArray)
    //             ->orderBy('gpgif.updated_at','DESC')
    //             ->get(['gpgif.gift_id','gpgif.gift_name','gpgif.gift_description','gpgif.gift_cr_id','gpgif.branch_id','gpgif.region_id','gpgif.region_name','gpgif.area_id','gpgif.gift_status','gpgif.created_at','gpgif.updated_at','gpbr.branch_name','gpreg.region_name']);

    //     } elseif($branch->type == 12){
    //         $details = DB::table('gpff_order as gpor')
    //                 ->join('gpff_branch as gpbr','gpor.branch_id','gpbr.branch_id')
    //                 ->join('gpff_region as gpreg','gpor.region_id','gpreg.region_id')
    //                 ->leftjoin('gpff_area as gpare','gpor.area_id','gpare.area_id')
    //                 ->join('gpff_warehouse as gpwar','gpor.warehouse_id','gpwar.warehouse_id')
    //                 // ->where('vendor', $branch->vendor)
    //                 ->whereIn('gpor.region_id', $myArray)
    //                 ->orderBy('gpor.updated_at','DESC');

    //         if($branch->start_date != ''){
    //             $details =$details->whereBetween(DB::RAW('date(gpor.created_at)'), [date($branch->start_date), date($branch->end_date)]);
    //         }
    //         if($branch->field_officer_id != ''){
    //             $details = $details->whereIn('gpor.field_officer_id', $branch->field_officer_id);
    //         }
            
    //         $details = $details->get(['gpor.order_id','gpor.task_id','gpor.field_officer_id','gpor.field_officer_name','gpor.area_manager_id','gpor.customer_id','gpor.customer_name','gpor.order_date','gpor.or_tot_price','gpor.branch_id','gpor.region_id','gpor.area_id','gpor.warehouse_id','gpwar.warehouse_name','gpor.order_status','gpor.created_at','gpor.updated_at','gpbr.branch_name','gpreg.region_name','gpare.area_name', 'gpor.payment_type','gpor.invoice_type','gpor.or_type']);

    //         return $details;

    //     } elseif($branch->type == 13){
    //         return  DB::table('gpff_task as gta')
    //         ->join('gpff_customer as gcus','gta.customer_id', '=' , 'gcus.customer_id')
    //         ->join('gpff_users as guse','gta.field_officer_id', '=' , 'guse.user_id')
    //         // ->where('vendor', $branch->vendor)
    //         ->whereIn('gta.region_id', $myArray)
    //         ->orderBy('gta.created_at', 'DESC')
    //         ->get(['gta.task_id','gta.created_by','gta.field_officer_id','guse.firstname','gta.customer_id','gta.area_manager_id','gta.task_title','gta.task_desc','gta.task_status','gta.task_date_time','gta.task_reassign_status','gta.task_start_date_time','gta.task_end_date_time','gta.task_active_minu','gta.notes','gta.created_at','gta.updated_at','gcus.customer_name','gcus.customer_email','gcus.country_code','gcus.customer_mobile','gcus.customer_city','gcus.customer_country','gcus.customer_fax','gcus.customer_address','gcus.customer_location','gcus.customer_lan','gcus.customer_lat','gcus.customer_cr_id','gcus.website','gcus.avatar','gcus.is_active','gta.branch_id','gta.region_id','gta.area_id']);

    //     } elseif($branch->type == 14){
    //     $today = date('Y-m-d') . '%';

    //     return DB::table('gpff_task as gta')
    //     ->join('gpff_customer as gcus', 'gta.customer_id', '=', 'gcus.customer_id')
    //     ->join('gpff_users as guse', 'gta.field_officer_id', '=', 'guse.user_id')
    //     ->where('gta.task_date_time', 'like', $today)
    //     ->whereIn('gta.region_id', $myArray)
    //     ->orderBy('gta.created_at', 'ASC')
    //     ->select(['gta.task_id','gta.created_by','gta.field_officer_id','guse.firstname','gta.customer_id','gta.area_manager_id','gta.task_title','gta.task_desc','gta.task_status','gta.task_date_time','gta.task_reassign_status','gta.task_start_date_time','gta.task_end_date_time','gta.task_active_minu','gta.notes','gta.created_at','gta.updated_at','gcus.customer_name','gcus.customer_email','gcus.country_code','gcus.customer_mobile','gcus.customer_city','gcus.customer_country','gcus.customer_fax','gcus.customer_address','gcus.customer_location','gcus.customer_lan','gcus.customer_lat','gcus.customer_cr_id','gcus.website','gcus.avatar','gcus.is_active','gta.branch_id','gta.region_id','gta.area_id',])
    //     ->limit(10)
    //     ->get();

    //     }elseif($branch->type == 15){
    //         return  DB::table('gpff_users as gpuse')
    //                 ->join('gpff_branch as gpbr','gpuse.branch_id','gpbr.branch_id')
    //                 ->join('gpff_region as gpreg','gpuse.region_id','gpreg.region_id')
    //                 // ->where('vendor', $branch->vendor)
    //                 ->whereIn('gpuse.region_id', $myArray)
    //                 ->orderBy('gpuse.updated_at','DESC')
    //                 ->where('gpuse.role', 9)
    //             ->get(['gpuse.user_id','gpuse.firstname','gpuse.lastname','gpuse.username','gpuse.email','gpuse.role','gpuse.is_active','gpuse.online_status','gpuse.city','gpuse.country','gpuse.language','gpuse.address','gpuse.country_code','gpuse.mobile','gpuse.permission','gpuse.avatar','gpuse.branch_id','gpuse.region_id','gpuse.cr_by_id','gpuse.created_at','gpuse.updated_at','gpbr.branch_name','gpreg.region_name']);
    //         }
    // }

    public function getRegionMangeBaseDetails($branch)
    {   
        $data = DB::table('gpff_users')
                    ->where('user_id', $branch->user_id)
                    ->First(['region_id']);

        $myArray = explode(',', $data->region_id);

        switch ($branch->type) {
            case 1:
                return DB::table('gpff_area as gpare')
                    ->join('gpff_branch as gpbr', 'gpare.branch_id', 'gpbr.branch_id')
                    ->join('gpff_region as gpreg', 'gpare.region_id', 'gpreg.region_id')
                    ->whereIn('gpare.region_id', $myArray)
                    ->orderBy('gpare.updated_at', 'DESC')
                    ->get(['gpare.area_id', 'gpare.area_name', 'gpare.area_address', 'gpare.area_description', 'gpare.area_status', 'gpare.area_contact', 'gpare.country_code', 'gpare.country', 'gpare.area_landline', 'gpare.pin_code', 'gpare.branch_id', 'gpare.region_id', 'gpare.created_at', 'gpare.updated_at', 'gpbr.branch_name', 'gpreg.region_name']);
            case 2:
                return DB::table('gpff_warehouse as gpwar')
                    ->join('gpff_branch as gpbr', 'gpwar.branch_id', 'gpbr.branch_id')
                    ->join('gpff_region as gpreg', 'gpwar.region_id', 'gpreg.region_id')
                    ->join('gpff_users as gpuse', 'gpwar.stockist_id', 'gpuse.user_id')
                    ->whereIn('gpwar.region_id', $myArray)
                    ->orderBy('gpwar.updated_at', 'DESC')
                    ->get(['gpwar.warehouse_id', 'gpwar.warehouse_name', 'gpwar.branch_id', 'gpwar.region_id', 'gpwar.area_id', 'gpwar.warehouse_cr_id', 'gpwar.stockist_id', 'gpuse.firstname', 'gpwar.warehouse_lan', 'gpwar.warehouse_lat', 'gpwar.warehouse_address', 'gpwar.warehouse_contact', 'gpwar.country_code', 'gpwar.country', 'gpwar.main_warehouse_id', 'gpwar.main_warehouse_name', 'gpwar.warehouse_type', 'gpwar.warehouse_status', 'gpwar.created_at', 'gpwar.updated_at', 'gpbr.branch_name', 'gpreg.region_name']);
            case 3:
                return  DB::table('gpff_users as gpuse')
                    ->join('gpff_branch as gpbr','gpuse.branch_id','gpbr.branch_id')
                    ->whereIn('gpuse.region_id', $myArray)
                    ->orderBy('gpuse.updated_at','DESC')
                    ->where('role', 3)
                    ->get(['gpuse.user_id','gpuse.firstname','gpuse.lastname','gpuse.username','gpuse.email','gpuse.role','gpuse.is_active','gpuse.online_status','gpuse.city','gpuse.country','gpuse.language','gpuse.address','gpuse.country_code','gpuse.mobile','gpuse.permission','gpuse.avatar','gpuse.branch_id','gpuse.region_id','gpuse.cr_by_id','gpuse.created_at','gpuse.updated_at','gpbr.branch_name']);
            case 4:
                return  DB::table('gpff_users as gpuse')
                    ->join('gpff_branch as gpbr','gpuse.branch_id','gpbr.branch_id')
                    ->join('gpff_region as gpreg','gpuse.region_id','gpreg.region_id')
                    // ->where('vendor', $branch->vendor)
                    ->whereIn('gpuse.region_id', $myArray)
                    ->orderBy('gpuse.updated_at','DESC')
                    ->where('gpuse.role', 4)
                    ->get(['gpuse.user_id','gpuse.firstname','gpuse.lastname','gpuse.username','gpuse.email','gpuse.role','gpuse.is_active','gpuse.online_status','gpuse.city','gpuse.country','gpuse.language','gpuse.address','gpuse.country_code','gpuse.mobile','gpuse.permission','gpuse.avatar','gpuse.branch_id','gpuse.region_id','gpuse.area_id','gpuse.cr_by_id','gpuse.created_at','gpuse.updated_at','gpbr.branch_name','gpreg.region_name']);
            case 5:
                return  DB::table('gpff_users as gpuse')
                ->join('gpff_branch as gpbr','gpuse.branch_id','gpbr.branch_id')
                ->join('gpff_region as gpreg','gpuse.region_id','gpreg.region_id')
                //->join('gpff_area as gpare','gpuse.area_id','gpare.area_id')
                // ->where('vendor', $branch->vendor)
                ->whereIn('gpuse.region_id', $myArray)
                ->orderBy('gpuse.updated_at','DESC')
                ->where('gpuse.role', 5)
            ->get(['gpuse.user_id','gpuse.firstname','gpuse.lastname','gpuse.username','gpuse.email','gpuse.role','gpuse.is_active','gpuse.online_status','gpuse.city','gpuse.country','gpuse.language','gpuse.address','gpuse.country_code','gpuse.mobile','gpuse.permission','gpuse.avatar','gpuse.branch_id','gpuse.region_id','gpuse.area_id','gpuse.cr_by_id','gpuse.created_at','gpuse.updated_at','gpuse.user_attendance_status','gpbr.branch_name','gpreg.region_name']);

            case 6:
                return  DB::table('gpff_users as gpuse')
                ->join('gpff_branch as gpbr','gpuse.branch_id','gpbr.branch_id')
                ->join('gpff_region as gpreg','gpuse.region_id','gpreg.region_id')
                // ->where('vendor', $branch->vendor)
                ->whereIn('gpuse.region_id', $myArray)
                ->orderBy('gpuse.updated_at','DESC')
                ->where('gpuse.role', 7)
            ->get(['gpuse.user_id','gpuse.firstname','gpuse.lastname','gpuse.username','gpuse.email','gpuse.role','gpuse.is_active','gpuse.online_status','gpuse.city','gpuse.country','gpuse.language','gpuse.address','gpuse.country_code','gpuse.mobile','gpuse.permission','gpuse.avatar','gpuse.branch_id','gpuse.region_id','gpuse.cr_by_id','gpuse.created_at','gpuse.updated_at','gpbr.branch_name','gpreg.region_name']);

            case 7:
                return  DB::table('gpff_customer as gpcus')
                ->join('gpff_branch as gpbr','gpcus.branch_id','gpbr.branch_id')
                ->join('gpff_region as gpreg','gpcus.region_id','gpreg.region_id')
                ->where('vendor', $branch->vendor)
            ->whereIn('gpcus.region_id', $myArray)
            ->orderBy('gpcus.updated_at','DESC')
            ->get(['gpcus.customer_id','gpcus.customer_login_id','gpcus.customer_name','gpcus.customer_email','gpcus.country_code','gpcus.customer_mobile','gpcus.customer_city','gpcus.customer_country','gpcus.customer_address','gpcus.customer_location','gpcus.customer_lan','gpcus.customer_lat','gpcus.customer_cr_id','gpcus.credit_limit','gpcus.area_manager_id','gpcus.region_manager_id','gpcus.branch_id','gpcus.region_id','gpcus.area_id','gpcus.website','gpcus.avatar','gpcus.customer_type','gpcus.is_active','gpcus.customer_emp_id','gpcus.created_at','gpcus.updated_at','gpbr.branch_name','gpreg.region_name']);

            case 8:
                return  DB::table('gpff_category as gpcat')
                ->join('gpff_branch as gpbr','gpcat.branch_id','gpbr.branch_id')
                ->join('gpff_region as gpreg','gpcat.region_id','gpreg.region_id')
                // ->where('vendor', $branch->vendor)
            ->whereIn('gpcat.region_id', $myArray)
            ->orderBy('gpcat.updated_at','DESC')
            ->get(['gpcat.category_id','gpcat.category_name','gpcat.branch_id','gpcat.region_id','gpcat.category_status','gpcat.created_at','gpcat.updated_at','gpcat.area_id','gpbr.branch_name','gpreg.region_name']);

            case 9:
                return  DB::table('gpff_product')
                // ->where('vendor', $branch->vendor)
                ->whereIn('region_id', $myArray)
                ->orderBy('updated_at','DESC')
                ->get();

            case 10:
                return  DB::table('gpff_samples as gpsam')
                ->join('gpff_product as gppr','gpsam.product_id','gppr.product_id')
                ->join('gpff_branch as gpbr','gpsam.branch_id','gpbr.branch_id')
                ->join('gpff_region as gpreg','gpsam.region_id','gpreg.region_id')
                ->join('gpff_users as gpuse','gpsam.stockist_id','gpuse.user_id')
                ->join('gpff_warehouse as gpwar','gpsam.warehouse_id','gpwar.warehouse_id')
                // ->where('vendor', $branch->vendor)
                ->whereIn('gpsam.region_id', $myArray)
                ->orderBy('gpsam.updated_at','DESC')
            ->get(['gpsam.samples_id','gpsam.product_id','gpsam.category_id','gpsam.category_name','gpsam.warehouse_id','gpwar.warehouse_name','gpsam.samples_name','gpsam.samples_description','gpsam.samples_tot_qty','gpsam.samples_blc_qty','gpsam.samples_sales_qty','gpsam.samples_cr_id','gpsam.stockist_id','gpuse.firstname','gpuse.lastname','gpsam.branch_id','gpsam.region_id','gpsam.area_id','gpsam.samples_status','gpsam.created_at','gpsam.updated_at','gpbr.branch_name','gpreg.region_name','gppr.product_netprice','gppr.product_grossprice','gppr.product_discount']);

            case 11:
                return  DB::table('gpff_gift as gpgif')
                ->join('gpff_branch as gpbr','gpgif.branch_id','gpbr.branch_id')
                ->join('gpff_region as gpreg','gpgif.region_id','gpreg.region_id')
                // ->where('vendor', $branch->vendor)
            ->whereIn('gpgif.region_id', $myArray)
            ->orderBy('gpgif.updated_at','DESC')
            ->get(['gpgif.gift_id','gpgif.gift_name','gpgif.gift_description','gpgif.gift_cr_id','gpgif.branch_id','gpgif.region_id','gpgif.region_name','gpgif.area_id','gpgif.gift_status','gpgif.created_at','gpgif.updated_at','gpbr.branch_name','gpreg.region_name']);

            case 12:
                $details = DB::table('gpff_order as gpor')
                ->join('gpff_branch as gpbr','gpor.branch_id','gpbr.branch_id')
                ->join('gpff_region as gpreg','gpor.region_id','gpreg.region_id')
                ->leftjoin('gpff_area as gpare','gpor.area_id','gpare.area_id')
                ->join('gpff_warehouse as gpwar','gpor.warehouse_id','gpwar.warehouse_id')
                // ->where('vendor', $branch->vendor)
                ->whereIn('gpor.region_id', $myArray)
                ->orderBy('gpor.updated_at','DESC');

                if($branch->start_date != ''){
                    $details =$details->whereBetween(DB::RAW('date(gpor.created_at)'), [date($branch->start_date), date($branch->end_date)]);
                }
                if($branch->field_officer_id != ''){
                    $details = $details->whereIn('gpor.field_officer_id', $branch->field_officer_id);
                }
                
                $details = $details->get(['gpor.order_id','gpor.task_id','gpor.field_officer_id','gpor.field_officer_name','gpor.area_manager_id','gpor.customer_id','gpor.customer_name','gpor.order_date','gpor.or_tot_price','gpor.branch_id','gpor.region_id','gpor.area_id','gpor.warehouse_id','gpwar.warehouse_name','gpor.order_status','gpor.created_at','gpor.updated_at','gpbr.branch_name','gpreg.region_name','gpare.area_name', 'gpor.payment_type','gpor.invoice_type','gpor.or_type']);

                return $details;

            case 13:
                return  DB::table('gpff_task as gta')
                ->join('gpff_customer as gcus','gta.customer_id', '=' , 'gcus.customer_id')
                ->join('gpff_users as guse','gta.field_officer_id', '=' , 'guse.user_id')
                // ->where('vendor', $branch->vendor)
                ->whereIn('gta.region_id', $myArray)
                ->orderBy('gta.created_at', 'DESC')
                ->get(['gta.task_id','gta.created_by','gta.field_officer_id','guse.firstname','gta.customer_id','gta.area_manager_id','gta.task_title','gta.task_desc','gta.task_status','gta.task_date_time','gta.task_reassign_status','gta.task_start_date_time','gta.task_end_date_time','gta.task_active_minu','gta.notes','gta.created_at','gta.updated_at','gcus.customer_name','gcus.customer_email','gcus.country_code','gcus.customer_mobile','gcus.customer_city','gcus.customer_country','gcus.customer_fax','gcus.customer_address','gcus.customer_location','gcus.customer_lan','gcus.customer_lat','gcus.customer_cr_id','gcus.website','gcus.avatar','gcus.is_active','gta.branch_id','gta.region_id','gta.area_id']);
    
            case 14:
                $today = date('Y-m-d') . '%';

                return DB::table('gpff_task as gta')
                ->join('gpff_customer as gcus', 'gta.customer_id', '=', 'gcus.customer_id')
                ->join('gpff_users as guse', 'gta.field_officer_id', '=', 'guse.user_id')
                ->where('gta.task_date_time', 'like', $today)
                ->whereIn('gta.region_id', $myArray)
                ->orderBy('gta.created_at', 'ASC')
                ->select(['gta.task_date_time','gcus.customer_name','gcus.customer_lan'])
                ->limit(10)
                ->get();
        
            case 15:
                return  DB::table('gpff_users as gpuse')
                ->join('gpff_branch as gpbr','gpuse.branch_id','gpbr.branch_id')
                ->join('gpff_region as gpreg','gpuse.region_id','gpreg.region_id')
                // ->where('vendor', $branch->vendor)
                ->whereIn('gpuse.region_id', $myArray)
                ->orderBy('gpuse.updated_at','DESC')
                ->where('gpuse.role', 9)
            ->get(['gpuse.user_id','gpuse.firstname','gpuse.lastname','gpuse.username','gpuse.email','gpuse.role','gpuse.is_active','gpuse.online_status','gpuse.city','gpuse.country','gpuse.language','gpuse.address','gpuse.country_code','gpuse.mobile','gpuse.permission','gpuse.avatar','gpuse.branch_id','gpuse.region_id','gpuse.cr_by_id','gpuse.created_at','gpuse.updated_at','gpbr.branch_name','gpreg.region_name']);

        }
    }

    public function getFOBaseAreaDetails($branch)
    {   
        $data = DB::table('gpff_users')
                    ->where('user_id', $branch->user_id)
                    ->First(['area_id']);

        $myArray = explode(',', $data->area_id);

            return  DB::table('gpff_area as gpare')
                    ->join('gpff_branch as gpbr','gpare.branch_id','gpbr.branch_id')
                    ->join('gpff_region as gpreg','gpare.region_id','gpreg.region_id')
                    ->whereIn('gpare.area_id', $myArray)
                    ->orderBy('gpare.updated_at','DESC')
                ->get(['gpare.area_id','gpare.area_name','gpare.area_address','gpare.area_description','gpare.area_status','gpare.area_contact','gpare.country_code','gpare.country','gpare.area_landline','gpare.branch_id','gpare.region_id','gpare.created_at','gpare.updated_at','gpbr.branch_name','gpreg.region_name']);

    }
    //
    public function getAreaManageBaseCusDetails($branch)
    {   
        $data = DB::table('gpff_users')
                    ->where('user_id', $branch->user_id)
                    ->First(['area_id']);

        $myArray = explode(',', $data->area_id);
        
            return  DB::table('gpff_customer as gpcus')
                    ->join('gpff_branch as gpbr','gpcus.branch_id','gpbr.branch_id')
                    ->join('gpff_region as gpreg','gpcus.region_id','gpreg.region_id')
                ->whereIn('gpcus.area_id', $myArray)
                ->where('vendor', $branch->vendor)
                ->orderBy('gpcus.updated_at','DESC')
                ->get(['gpcus.customer_id','gpcus.customer_login_id','gpcus.customer_name','gpcus.customer_email','gpcus.country_code','gpcus.customer_mobile','gpcus.customer_city','gpcus.customer_country','gpcus.customer_address','gpcus.customer_location','gpcus.customer_lan','gpcus.customer_lat','gpcus.customer_cr_id','gpcus.credit_limit','gpcus.area_manager_id','gpcus.region_manager_id','gpcus.branch_id','gpcus.region_id','gpcus.area_id','gpcus.website','gpcus.avatar','gpcus.customer_type','gpcus.is_active','gpcus.customer_emp_id','gpcus.created_at','gpcus.updated_at','gpbr.branch_name','gpreg.region_name']);

    }

    public function getAreaManageBaseVendorDetails($branch)
    {   
        // $data = DB::table('gpff_users')
        //             ->where('user_id', $branch->user_id)
        //             ->First('area_manager_id');
        // print_r($data);

        // $myArray = explode(',', $data->area_id);
        
            return  DB::table('gpff_customer as gpcus')
                    ->join('gpff_branch as gpbr','gpcus.branch_id','gpbr.branch_id')
                    ->join('gpff_region as gpreg','gpcus.region_id','gpreg.region_id')
                // ->whereIn('gpcus.area_id', $myArray)
                ->where('customer_cr_id', $branch->user_id)
                ->where('vendor', $branch->vendor)
                ->orderBy('gpcus.updated_at','DESC')
                ->get(['gpcus.customer_id','gpcus.customer_login_id','gpcus.customer_name','gpcus.customer_email','gpcus.country_code','gpcus.customer_mobile','gpcus.customer_city','gpcus.customer_country','gpcus.customer_address','gpcus.customer_location','gpcus.customer_lan','gpcus.customer_lat','gpcus.customer_cr_id','gpcus.credit_limit','gpcus.area_manager_id','gpcus.region_manager_id','gpcus.branch_id','gpcus.region_id','gpcus.area_id','gpcus.website','gpcus.avatar','gpcus.customer_type','gpcus.is_active','gpcus.customer_emp_id','gpcus.created_at','gpcus.updated_at','gpbr.branch_name','gpreg.region_name']);

    }

    public function getAreaManagerBasedStockistDetails($branch){
        $data = DB::table('gpff_users')
              ->where('user_id', $branch->user_id)
              ->First(['area_id']);

        $myArray = explode(',', $data->area_id);

        return DB::table('gpff_users as gpuse')
                ->join('gpff_branch as gpbr','gpuse.branch_id','gpbr.branch_id')
                ->join('gpff_area as gparea', 'gpuse.area_id', 'gparea.area_id')
                ->join('gpff_region as gpreg','gpuse.region_id','gpreg.region_id')
                ->whereIn('gpuse.area_id', $myArray)
                ->orderBy('gpuse.updated_at','DESC')
                ->where('gpuse.role', 7)
                ->get(['gpuse.user_id','gpuse.firstname','gpuse.lastname','gpuse.username','gpuse.email','gpuse.role','gpuse.is_active','gpuse.online_status','gpuse.city','gpuse.country','gpuse.language','gpuse.address','gpuse.country_code','gpuse.mobile','gpuse.permission','gpuse.avatar','gpuse.branch_id','gpuse.region_id','gpuse.cr_by_id','gpuse.created_at','gpuse.updated_at','gpbr.branch_name','gpreg.region_name', 'gparea.area_name', 'gparea.area_id']);

    }

    //Get Region Based Warehouse Details
    public function getAreaManageBasWare($branch)
    {   
        return  DB::table('gpff_warehouse')
                ->where('warehouse_cr_id', $branch->user_id)
                ->get();
    }

    public function getAllVendors(){
        $data = DB::table('gpff_customer as gcus')
                ->join('gpff_branch as gbran','gcus.branch_id','gbran.branch_id')
                ->join('gpff_region as gregi','gcus.region_id','gregi.region_id')
                ->join('gpff_area as gparea', 'gcus.area_id', 'gparea.area_id')
                ->where('vendor',1)
                ->orderBy('gcus.updated_at','DESC')
                ->get();

         return $data;   
    }

    public function getRegionBasVendor($branch){
        return  DB::table('gpff_customer')
                ->where('vendor', 1)
                ->where('area_id', $branch->area_id)
                ->get();
    }

    public function getAreaManagerBasedWarehouse($branch){
        return DB::table('gpff_warehouse')
                ->where('warehouse_cr_id', $branch->warehouse_cr_id)
                ->get();
    }

    public function getAreaManageBaseCusDetailsRemarkBase($branch)
    {   

        if($branch->remarks_type == 0){
            $data = DB::table('gpff_users')
                        ->where('user_id', $branch->user_id)
                        ->First(['area_id']);

            $myArray = explode(',', $data->area_id);
            
                return  DB::table('gpff_customer as gpcus')
                        ->join('gpff_branch as gpbr','gpcus.branch_id','gpbr.branch_id')
                        ->join('gpff_region as gpreg','gpcus.region_id','gpreg.region_id')
                    ->whereIn('gpcus.area_id', $myArray)
                    ->where('vendor', $branch->vendor)
                    ->orderBy('gpcus.updated_at','DESC')
                    ->get(['gpcus.customer_id','gpcus.customer_login_id','gpcus.customer_name','gpcus.customer_email','gpcus.country_code','gpcus.customer_mobile','gpcus.customer_city','gpcus.customer_country','gpcus.customer_address','gpcus.customer_location','gpcus.customer_lan','gpcus.customer_lat','gpcus.customer_cr_id','gpcus.credit_limit','gpcus.area_manager_id','gpcus.region_manager_id','gpcus.branch_id','gpcus.region_id','gpcus.area_id','gpcus.website','gpcus.avatar','gpcus.customer_type','gpcus.is_active','gpcus.customer_emp_id','gpcus.created_at','gpcus.updated_at','gpbr.branch_name','gpreg.region_name']);
        }else{

            $data = DB::table('gpff_users')
                        ->where('user_id', $branch->user_id)
                        ->First(['area_id']);

            $myArray = explode(',', $data->area_id);
            
                return  DB::table('gpff_customer as gpcus')
                        ->join('gpff_branch as gpbr','gpcus.branch_id','gpbr.branch_id')
                        ->join('gpff_region as gpreg','gpcus.region_id','gpreg.region_id')
                    ->whereIn('gpcus.area_id', $myArray)
                    ->where('gpcus.remarks_type', $branch->remarks_type)
                    ->where('vendor', $branch->vendor)
                    ->orderBy('gpcus.updated_at','DESC')
                    ->get(['gpcus.customer_id','gpcus.customer_login_id','gpcus.customer_name','gpcus.customer_email','gpcus.country_code','gpcus.customer_mobile','gpcus.customer_city','gpcus.customer_country','gpcus.customer_address','gpcus.customer_location','gpcus.customer_lan','gpcus.customer_lat','gpcus.customer_cr_id','gpcus.credit_limit','gpcus.area_manager_id','gpcus.region_manager_id','gpcus.branch_id','gpcus.region_id','gpcus.area_id','gpcus.website','gpcus.avatar','gpcus.customer_type','gpcus.is_active','gpcus.customer_emp_id','gpcus.created_at','gpcus.updated_at','gpbr.branch_name','gpreg.region_name']);
        }

    }

    //Get Branch Based Warehouse Details
    public function getBranchBasWare($branch)
    {   
        return  DB::table('gpff_warehouse')
                ->where('branch_id', $branch->branch_id)
                ->get();
    }
}
