<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        ////////////////////////////
        //User Managment Api Calls//
        ////////////////////////////
        '/addUser',
        '/updateUsers',
        '/userProfileUpdate',
        '/userBasedPriceUpdate',
        '/deleteUsers',
        '/getIndUsers',
        '/getRoleBasedUsers',
        '/getManagerBasedUserDetails',
        '/getAllUsers',
        '/userActiveOrDeactive',
        '/login',
        '/logout',
        '/userChangePassword',
        '/userForgetPassword',
        '/addCustomers',
        '/addAllCustomers',
        '/updateCustomers',
        '/deleteCustomers',
        '/getIndCustomers',
        '/getEmpBaseCustomers',
        '/getAllCustomers',
        '/getUserBasedCustomers',
        '/customerActiveOrDeactive',
        '/getBranchBasRegManage',
        '/getBranchBaseFO',
        '/getRegionBasAreaManage',
        '/getRegionBasCustomer',
        '/getAreaBasedField',
        '/getAreaBasCustomer',
        '/getMultiAreaBasCustomer',
        '/getUserBasedDetails',
        
        /////////////////////////////
        //Task Management Api Calls//
        /////////////////////////////
        '/addTask',
        '/addmultiTask',
        '/updateTask',
        '/updateTaskStatus',
        '/reassignTask',
        '/addNotes',
        '/removeTask',
        '/fetchAdminTaskList',
        '/fetchAreaManageTaskList',
        '/fetchTaskList',
        '/fetchAreaBaseTaskList',
        '/fetchRegionBaseTaskList',
        '/fetchIndTaskDetails',
        '/fetchIndTaskAllDetails',
        '/getFieldTaskAllDetails',
        //////////////////////////////
        //Leave Management Api Calls//
        //////////////////////////////
        '/addLeave',
        '/leaveAcceptOrReject',
        '/getManagerBasedLeaveRequest',
        '/getUserLeaveHistory',
        '/addAttendance',
        ///////////////////////////////////////
        //Gift Inventory Management Api Calls//
        ///////////////////////////////////////
        '/addGift',
        '/updateGift',
        '/removeGift',
        '/getIndGift',
        '/getAllGift',
        '/getRegionBasedGift',
        
        '/addGiftQty',
        '/updateGiftQty',
        '/getWarehouseBasedGiftStock',
        //'/getWarehouseBasedGift',        
        ////////////////////////////////////
        //Gift Assign Management Api Calls//
        ////////////////////////////////////
        '/giftAssign',
        '/giveGifttoFO',
        '/getReceivedGiftDet',
        '/getMyAssignGifts',
        '/getManagerAssignGifts',
        '/getAllAssignGifts',
        '/giftToCustomer',
        '/getFOGift',

        '/getRegionManageAssignGifts',
        ////////////////////////////////
        //Product Management Api Calls//
        ////////////////////////////////
        //Category
        '/addCategory',
        '/updateCategory',
        '/removeCategory',
        '/categoryActiveOrDeactive',
        '/getAllCategory',
        '/getRegionBasedCategory',
        '/getRegionBasedProducts',
        //Product
        '/addProduct',
        '/updateProduct',
        '/removeProduct',
        '/getIndProduct',
        '/productActiveOrDeactive',
        '/getAllProduct',
        '/getAllProductPagi',

        '/addGoodsRecivedStock',
        '/updateProductsBatchExp',
        '/getGRHistory',
        '/getIndGRBatch',
        '/getWarehouseBasedProductStock',
        '/getWarehouseBasedProductBatch',
        '/getWarehouseBasedProductStockList',
        '/getMultipleWarehouseBasedProductStockList',
        '/getBatchBasedQty',
        '/getIndProductQty',
        '/getIndProductsQtyDetails',

        '/getCategoryBasedProduct',
        '/getCategoryBasedProductPagi',
        '/getRegionBasedField',
        '/getRegionBasedAreaManager',
        //Assign Product
        '/productAssign',
        '/getfieldBasedAssignProduct',
        '/getFOCateBasedAssignProduct',
        '/getAllAssignProduct',

        '/getAreaManageAssignProduct',
        '/getRegionManageAssignProduct',
        '/getRegionandTypeBasedProducts',

        //Stock Request
        '/makeRequest',
        '/getStockRequest',
        '/getStockRequestList',
        '/getStockRequestAcceptList',
        '/getStockRequestBaseAcceptList',
        '/stockRequestAcceptOrReject',
        '/getStockRequestListInd',
        '/stockAcceptAcknowledge',
        '/warehouseStockCompare',
        ////////////////////////////////////////////
        //Normal Notification Management Api Calls//
        ///////////////////////////////////////////
        '/getNotification',
        '/getAppNotification',
        '/notificationStatusChange',
        '/deleteNotification',
        '/deleteAllNotification',
        '/getIndBasedNotiDetails',
        //////////////////////////////
        //Order Management Api Calls//
        //////////////////////////////
        '/addOrderDetails',
        '/getFieldOfficerOrder',
        '/getOrderBasedList',
        '/getOrderBasedBatchList',
        '/getOrderBasedEventsList',
        '/getManagerOrder',
        '/getManagerOrderBasedList',
        '/getAllOrder',
        '/getAllOrderDashboard',
        '/getWarAllOrder',
        '/getBranchAllOrder',
        '/updateOrderStatus',
        '/getPlacedOrder',
        '/getwarehouseOrder',
        '/order/Accept',  //MANI
        '/cusOverlimitOrderReq',
        '/getLimitOverData',
        '/getHistoryLimitOverData',

        '/getApprovalOrderData',
        '/orderFOCDiscountAccept',
        //////////////////////////////////////////
        //FeedBack & Review Management Api Calls//
        /////////////////////////////////////////
        '/addFeedBack',
        ////////////////////
        //Revist Api Calls//
        ////////////////////
        '/addRevisit',
        //////////////////////
        //Chating Api Calls//
        /////////////////////
        '/getAdminChatDetails',
        '/getBranchAdminChatDetails',
        '/getRegionManagerChatDetails',        
        '/getManagerChatDetails',
        '/getFieldOfficerChatDetails',
        '/getStockistChatDetails',
        '/getCustomerChatDetails',
        ///////////////////////
        //Dashboard Api Calls//
        //////////////////////
        '/getAdminDashCount',
        '/getAreaDashCount',
        '/getRegionDashCount',
        '/getBranchDashCount',
        '/getStockistDashCount',

        
        '/getAdminDashGraph',
        '/getDashTaskCount',
        '/getBranchDashTaskCount',
        '/getBranchDashGraph',
        '/getRegionDashTaskCount',
        '/getRegionDashGraph',
        '/getAreaDashTaskCount',
        '/getManagDashGraph',
        '/getManagDashCount',

        //////////////////////////////////////////
        //Samples Inventory Management Api Calls//
        //////////////////////////////////////////
        '/addSamples',
        '/updateSamplesQty',
        '/getWarehouseBasedSampleStock',

        '/updateSamples',
        '/removeSamples',
        '/getIndSamples',
        '/getAllSamples',
        ///////////////////////////////////////
        //Samples Assign Management Api Calls//
        ///////////////////////////////////////
        '/samplesAssign',
        '/getMyAssignSamples',
        '/sampleToCustomer',
        '/getManagerAssignSamples',
        '/getAllAssignSamples',
        '/getRegionBasedSamples',
        '/getReceivedSampleDet',
        '/giveSampletoFO',
        '/getFOSamples',

        '/getRegionManageAssignSamples',
        ////////////////////////////////
        //History Management Api Calls//
        ////////////////////////////////
        '/getStockAddHistory',
        '/getStockAssignHistory',
        ////////////////////////////
        //File Managment Api Calls//
        ////////////////////////////
        '/addFolder',
        '/updateFolder',
        '/deleteFolder',
        '/addFile',
        '/updateFile',
        '/deleteFile',
        '/getFolderFiles',
        '/getAllFiles',
        ////////////////////////////
        //Branch Managment Api Calls//
        ////////////////////////////
        '/addBranch',
        '/updateBranch',
        '/deleteBranch',
        '/branchActiveOrDeactive',
        '/getAllBranch',
        '/getIndBranch',
        '/getIdBasedBranch',
        ////////////////////////////
        //Region Managment Api Calls//
        ////////////////////////////
        '/addRegion',
        '/updateRegion',
        '/deleteRegion',
        '/regionActiveOrDeactive',
        '/getAllRegion',
        '/getIndRegion',
        '/getBranchBaseReg',
        '/getIdBasedRegion',
        ////////////////////////////
        //Area Managment Api Calls//
        ////////////////////////////
        '/addArea',
        '/updateArea',
        '/deleteArea',
        '/areaActiveOrDeactive',
        '/getAllArea',
        '/getIndArea',
        '/getRegionBasArea',
        '/getIdBasedArea',
        '/getAreaBasedField',
        '/getMultiAreaBasedField',
        '/getAreabasFO',
        ////////////////////////////
        //Warehouse Managment Api Calls//
        ////////////////////////////
        '/addWarehouse',
        '/updateWarehouse',
        '/deleteWarehouse',
        '/warehouseActiveOrDeactive',
        '/getAllWarehouse',
        '/getIndWarehouse',
        '/getMainBaseSubWarehouse',
        '/getAllStockist',
        '/getRegionBasStockist',
        '/getRegionBasAvaiStockist',
        '/getRegionBasWare',
        '/getRegionBasMainWare',
        '/getRegionBasStockRequestWare',

        //
        '/getBranchAdminBaseDetails',
        '/getRegionMangeBaseDetails',
        '/getFOBaseAreaDetails',
        '/getAreaManageBaseCusDetails',
        '/getAreaManageBaseCusDetailsRemarkBase',
        '/getAreaManageBaseVendorDetails',
        '/getAreaManagerBasedStockistDetails',
        //////////////////////////////
        //Target Managment Api Calls//
        //////////////////////////////
        '/assignTarget',
        '/removeTarget',
        '/getMyTargetDetails',
        '/getAssignToTargetDetails',
        /////////////////////////////
        //Report Management Api Calls//
        /////////////////////////////
        //sales Report
        '/salesCurrentYearReport',
        '/salesFilterReport',
        '/CustomercountsalesFilterReport',
        '/VendorcountsalesFilterReport',
        '/CustomersalesOrderFilterReport',
        '/VendorsalesOrderFilterReport',
        //Coverage Report
        '/coverageCurrentYearReport',
        '/coverageFilterReport',
        //Samples Report
        '/samplesCurrentYearReport',
        '/samplesFilterReport',
        //Gift Report
        '/giftCurrentYearReport',
        '/giftFilterReport',
        //Daily Call Report
        '/dailyCallReport',
        //Visited Report
        '/visitedCurrentYearReport',
        '/visitedFilterReport',
        //Target Report
        '/targetDetailReport',
        
        ///////////////////////////////////
        //APP Report Management Api Calls//
        ///////////////////////////////////
        '/appDailyCallReport',
        '/appVisitedReport',
        '/appSalesReport',
        '/appCoverageReport',
        '/appSamplesReport',
        '/appGiftReport',
        '/appTargetReport',
        '/appTargetDetailChart',
        '/appMissedTaskCallReport',
        
        //Pagination
        'getAllCustomersForApp',
        // Supplier
        '/supplier/create',
        '/supplier/list',
        '/supplier/status',
        '/supplier/details',
        '/supplier/delete',
        '/supplier/update',
        '/sms/smscampign',
        '/email/emailcampign',
        '/notification/noticampign',
        '/getAppPromoNotification',

        // Purchase
        '/purchase/addPurchase',

        //Field Office == > SALE (mani)
        '/sales/addSale',

        '/sales/updateStatus',

        'getIncentiveTargetInfo',
        'deleteIncentiveTarget',

        ////////////////////////////
        //Cart Management Api Call//
        ////////////////////////////
        '/customer/addToCart',
        '/customer/getCartDetails',
        '/customer/deleteCartDetails',
        '/customer/getCusOrderDetailsPagi',
        '/customer/getCusPrevOrderDetails',

        '/customer/addComplain',
        '/customer/getCusBasedComplain',
        '/customer/updateComplain',
        '/customer/getUserBasedComplain',
        '/customer/getIndComplain',
        '/customer/deleteComplain',

        '/getPreviousTargetDetails',
        '/getAreaManageBasWare',

        /////////////////////////////
        ////// Vendor api Call//////
        ///////////////////////////
        '/getAreaManagerBasedWarehouse',
        '/addVendorOrder',
        '/getIdBasedVendorList',
        '/getAreaManagerBasedOrder',
        '/getAreaManagerBasedVendorOrder',
        '/getOrderIdBasedList',
        '/getWarehouseIdBasedOrderDetails',
        '/fetchAreaManageBasedVendorOrderDetails',
        '/reportForAreaManager',
        '/reportForWarehouse',
        '/VendorOrderAccept',
        '/getOrderIdDetails',
        '/getWhIdBaseVenOrdDetails',
        '/getVendorOrderBasedEventsList',
        '/updateVendorOrderStatus',
        '/getPlacedVendorOrder',
        '/getApprovalVendorOrderData',
        '/getVendorOrderByAdmin',
        '/vendorOrderFOCDiscountAccept',
        '/getAreaIdBasedPriceType',
        '/acceptCustomerOrderdetails',
        '/productSalesReport',
        '/getOrderBeforeAdminApproval',
        '/getOrderBasedFOCDetails',
        '/vendorReportForProductIdBased',
        '/getOrderBatchFOCDetails',
        '/customerReportForProductIdBased',
        '/customerOrderReportForProductIdBased',
        '/getAllFieldOfficerAndStockistInOrder',
        '/fetchRegionBasedVendorList',

        /////////////////////////////
        ////Collection process//////
        ////////////////////////////

        '/addCollectionDetails',
        '/fetchCollectionDateBased',
        '/fetchInvoice_noBasedDetails',
        '/vendorReportForRegionManageBased',
        '/saleReturnIdBasedDetails',
        '/addSalereturn',
        '/acceptCollectionDetail',
        '/fetchVendorOrderList',
        '/fetchCollectionDetailsBasedId',
        '/deleteCollectionDetails',
        '/fetchCollectionDetailsOrderBase',
        '/fetchFinanceUserBasedCollections',
        '/updateCollectionUser',
        '/fetchRegionBaseCusOrderVenOrderDet',
        '/regionBaseCusOrVendorDetails',
        '/checkerStatusUpdate',
        '/fetchRegionBasedCollect',
        '/fetchAreaBasedCollect',
        '/expireDateReport',
        '/dailyCollectionReport',
        '/checkerDailyCollectionReport',
        '/invoiceBasedReturnDetails',
        '/orderReturnReport',
        '/discountReportSummary',
        '/discountReport',
        '/subWarehouseInStocksReport',
        '/subWarehouseItemMovingReport',
        '/subWarehouseItemMovingReportSumm',
        '/mainWarehouseItemMovingRequest',
        '/mainWarehouseItemMovingRequestSumm',
        '/getRegionBasedWarehouses',
        '/financeUsrBasedReturnDetails',
        '/orderIdBasedReturnDetails',
        '/doerStatusUpdate',
        '/regionBasedCollectionDetails',
        '/fetchCheckedCollectionDetails',
        '/fetchFieldOfficerCollection',
        '/doerCollectionDetails',
        '/checkerReport',
        '/financeUserReport',
        '/doer_list',
        '/regionBaseCusOrderDetails',
        '/financeUserAcceptedAndPendingReport',

        ///Telecalling///
        '/addTelecalling',
        '/fieldOfficerBaseTeleCalling',
        '/telecallingReport',
        '/addRecalling',
        '/fieldOfficerBasedRecalling',
        '/telecallingBasedAllDetails',
        '/telecallingStatusUpdate',

        // auditor report
        '/stockReport',
        '/financeUserPendingReport',
        '/dailySaleReport',
        '/getBranchBasWare',
        '/getBranchBasedCategory',
        '/updateProductsBatchQty',
        '/generateOrderPDF',
        '/getOrderScheme',
        '/schemeReport',
        '/updateOrderInvoiceDate',
        '/collectionPendingReport',
        '/getStockBalanceSheet',
        '/uploadGoodsRecivedStock',
        '/getBatchDetails',
        '/addBiller',
        '/updateBiller',
        '/updateBillerStatus',
        '/deleteBiller',
        '/getBillerDetails',
        '/stockAdjustAddProduct',
        '/salesAnalysisReport',
        '/acceptMultipleCollectionDetail',
        '/getDoerList',
        '/getCustomerCollections',
        '/getCheckerList',
        '/addDoerCollectionDetails',
        '/doerCollectionReport',
        '/financeCollectionReport',
        '/getFinanceUserList',
        '/getCustomersList',
        '/addPORequest',
        '/getAllPORequest',
        '/getPODetails',
        '/acceptPORequest',
        '/getPOInvoiceDetails',
        '/generatePOInvoice',
        '/getAllPOInvoice',
        '/getPOInvoiceProducts',
        '/poStockAcknowledge',
        '/addPOPaymentDetails',
        '/getPOPaymentTransaction',
        '/demo'
    ];
}
