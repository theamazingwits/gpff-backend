<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
require(base_path().'/app/Http/Middleware/Inventory.php');
require(base_path().'/app/Http/Controllers/Inventory_Validator.php');

class InventoryController extends Controller {
	private $Inventory;
	private $validator;
	function __construct() {
		if (!isset(self::$this->Inventory)) {
			$this->Inventory = new Inventory();
    		$this->validator = new InventoryValidator();	
		} 
    	
  	}

	// Create Supplier
	public function createSupplier(Request $request){		
		$list = $this->Inventory->getSupplier('supplier_code', $request->input('supplier_code'));
		if (count($list)) {
			return $response = response()->json(['status' => 'failure' , 'message' => 'Supplier Already exist']);
		}
		$this->Inventory->supplier_name = $request->input('supplier_name');
		$this->Inventory->supplier_code = $request->input('supplier_code');
		$this->Inventory->supplier_email = $request->input('supplier_email');
		$this->Inventory->supplier_contact_number_code = $request->input('supplier_contact_code');
		$this->Inventory->supplier_country = $request->input('supplier_country');
		$this->Inventory->supplier_contact_number = $request->input('supplier_contact_number');		
		$this->Inventory->branch_id = $request->input('branch_id');		
		$this->Inventory->region_id = $request->input('region_id');		
		$this->Inventory->created_by = $request->input('user_id');		
		$result = $this->Inventory->createSupplier($this->Inventory);
		if($result) {
				$response = response()->json(['status' => 'success', 'message' => 'Supplier stored successfully']);
		} else {
			$response = response()->json(['status' => 'failure' , 'message' => 'Error in storing details....']);
		}
		return $response;
	}

	// Get Supplier All
	public function getAllSupplier(Request $request) {
		if($request->input('branch_id')){
			$this->Inventory->branch_id = 	$request->input('branch_id');
		}else{
			$this->Inventory->branch_id = 	'';
		}
		if($request->input('region_id')){
			$this->Inventory->region_id = 	$request->input('region_id');
		}else{
			$this->Inventory->region_id = 	'';
		}
		if($request->input('status')){
			$this->Inventory->status = 	$request->input('status');
		}else{
			$this->Inventory->status = 	'';
		}
		$result = $this->Inventory->getSupplierList($this->Inventory);
		if(Count($result)) {
			$response = response()->json(['status' => 'success', 'message' => 'Supplier List', 'data' => $result]);
		} else {
			$response = response()->json(['status' => 'failure' , 'message' => 'No Data Found', 'data' => []]);
		}
		return $response;
	}

	public function updateSupplierStatus(Request $request){
        $this->Inventory->supplier_id    = $request->input('supplierId');
        $this->Inventory->supplier_status    = $request->input('supplier_status');
        $result = $this->Inventory->updateSupplierStatus($this->Inventory);
        if($result > 0){
            if($this->Inventory->supplier_status == 1){
                $response = response()->json(['status' => 'success', 'message' => 'Supplier Actived Successfully']);
            } elseif ($this->Inventory->supplier_status == 2) {
                $response = response()->json(['status' => 'success', 'message' => 'Supplier Deactived Successfully']);
            }
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Error in Updated details.. !']);
        }
        return $response;
        
    }

	// Get Supplier based on ID
	public function getSupplier(Request $request) {
		$this->validator->supplierID($request->input('supplierID'));
		$result = $this->Inventory->getSupplier('supplier_id', $request->input('supplierID'));
		if(count($result)) {
			$response = response()->json(['status' => 'success', 'message' => 'Supplier List', 'data' => $result]);
		} else {
			$response = response()->json(['status' => 'failure' , 'message' => 'No Data Found', 'data' => []]);
		}
		return $response;
	}

	// Delete Supplier
	public function deleteSupplier(Request $request) {		
		$this->Inventory->supplier_id = $request->input('supplierId');
		$result = $this->Inventory->deleteSupplier($this->Inventory);
		if ($result > 0) {
			$response = response()->json(['status' => 'success', 'message' => 'Supplier Deleted successfully' ]);
		} else {
			$response = response()->json(['status' => 'failure' , 'message' => 'Error in delete details' ]);
		}
		return $response;
	}

	// Update Supplier
	public function updateSupplier(Request $request) {
		$this->Inventory->supplier_id = $request->input('supplierId');
		$this->Inventory->supplier_name = $request->input('supplier_name');
		$this->Inventory->supplier_code = $request->input('supplier_code');
		$this->Inventory->supplier_email = $request->input('supplier_email');
		$this->Inventory->supplier_contact_number_code = $request->input('supplier_contact_code');
		$this->Inventory->supplier_country = $request->input('supplier_country');
		$this->Inventory->supplier_contact_number = $request->input('supplier_contact_number');		
		$this->Inventory->branch_id = $request->input('branch_id');		
		$this->Inventory->region_id = $request->input('region_id');		
		$this->Inventory->updated_by = $request->input('user_id');
		$result = $this->Inventory->updateSupplier($this->Inventory);
		if($result > 0) {
			$response = response()->json(['status' => 'success', 'message' => 'Supplier Updated successfully' ]);
		}  else {
			$response = response()->json(['status' => 'failure' , 'message' => 'Error in Updated details' ]);
		}
		return $response;
	}


}