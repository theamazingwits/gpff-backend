<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class Bulk extends Model
{
    protected $table = 'gpff_product';
    protected $fillable = [
        'name', 'email',
    ];
}