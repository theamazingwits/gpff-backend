<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use File;
use Mail;

require(base_path().'/app/Http/Middleware/Telecalling.php');

class TelecallingController extends Controller{

	public function addTelecalling(Request $request){
		$Telecallings = new Telecalling();
		$Telecallings->field_officer_id 	= $request->input('field_officer_id');
		$Telecallings->field_officer_name   = $request->input('field_officer_name');
		$Telecallings->customer_id 			= $request->input('customer_id');
		$Telecallings->customer_name 		= $request->input('customer_name');
		$Telecallings->area_id				= $request->input('area_id');
		$Telecallings->region_id 			= $request->input('region_id');
		$Telecallings->branch_id 			= $request->input('branch_id');

		$results = $Telecallings->addTelecalling($Telecallings);

		if($results == 1){
			$response = response()->json(['status'=>'success', 'message'=>"Telecalling details stored successfully"]);
		}else{
			$response = response()->json(['status'=>'failure', 'message'=>"Something went wrong"]);
		}
		return $response;
	}

	public function fieldOfficerBaseTeleCalling(Request $request){
		$Telecallings = new Telecalling();
		// print_r($Telecallings->field_officer_id);
		$Telecallings->field_officer_id = $request->input('field_officer_id');
		$Telecallings->type = $request->input('type');
		$Telecallings->date = $request->input('date');
		$results = $Telecallings->fieldOfficerBaseTeleCalling($Telecallings);

		if(count($results)>0){
				$response = response()->json(['status'=>'success', 'message'=>"Telecalling details fetched successfully", 'data'=>$results]);
		}else{
			$response = response()->json(['status'=>'failure', 'message'=>"No data available"]);
		}
		return $response;
	}

	public function telecallingReport(Request $request){
		$Telecallings = new Telecalling();

		$Telecallings->start_date = $request->input('start_date');
		$Telecallings->end_date = $request->input('end_date');

		if($request->input('branch_id')){
			$Telecallings->branch_id = $request->input('branch_id');
		}else{
			$Telecallings->branch_id = '';
		}
		if($request->input('region_id')){
			$Telecallings->region_id = $request->input('region_id');
		}else{
			$Telecallings->region_id = '';
		}
		if($request->input('area_id')){
			$Telecallings->area_id = $request->input('area_id');
		}else{
			$Telecallings->area_id = '';
		}
		if($request->input('field_officer_id')){
			$Telecallings->field_officer_id = $request->input('field_officer_id');
		}else{
			$Telecallings->field_officer_id = '';
		}

		if($request->input('customer_id')){
			$Telecallings->customer_id = $request->input('customer_id');
		}else{
			$Telecallings->customer_id = "";
		}
		//Check Status
        if($request->input('task_status')){
            $Telecallings->task_status = $request->input('task_status');

        }else{
            $Telecallings->task_status = '';
        }

		$Telecallings->report_type = $request->input('report_type');

		$results = $Telecallings->telecallingReport($Telecallings);

		if(count($results)>0){
			$response = response()->json(['status'=>'success','message'=>"",'data'=>$results]);
		}else{
			$response = response()->json(['status'=>'failure','message'=>"No data found"]);
		}
		return $response;
	}

	public function addRecalling(Request $request){

		$Telecallings = new Telecalling();

		$Telecallings->telecalling_id = $request->input('telecalling_id');
		$Telecallings->customer_name  = $request->input('customer_name');
		$Telecallings->customer_id	  = $request->input('customer_id');
		$Telecallings->field_officer_id = $request->input('field_officer_id');
		$Telecallings->field_officer_name = $request->input('field_officer_name');
		$Telecallings->telecalling_date = $request->input('telecalling_date');
		$Telecallings->recalling_date = $request->input('recalling_date');
		// $Telecallings->status 		  = $request->input('status');
		$Telecallings->message 		  = $request->input('message');
		$Telecallings->type 		  = $request->input('type');

		$results = $Telecallings->addRecalling($Telecallings);

		if($results==1){
			$response = response()->json(['status'=>'success', 'message'=>"Recalling added success", 'data'=>$results]);
		}else{
			$response = response()->json(['status'=>'failure','message'=>"No data found"]);
		}
		return $response;
	}

	public function fieldOfficerBasedRecalling(Request $request){
		$Telecallings = new Telecalling();

		$Telecallings->field_officer_id = $request->input('field_officer_id');
		// print_r($request->input('field_officer_id'));

		$results = $Telecallings->fieldOfficerBasedRecalling($Telecallings);

		if($results){
			$response = response()->json(['status'=>'success', 'message'=>"Field officer based recalling details", 'data'=>$results]);
		}else{
			$response = response()->json(['status'=>'failure','message'=>"No data found"]);
		}
		return $response;
	}

	public function telecallingBasedAllDetails(Request $request){
		$Telecallings = new Telecalling();

		$Telecallings->telecalling_id = $request->input('telecalling_id');

		$results = $Telecallings->telecallingBasedAllDetails($Telecallings);

		if($results){
			$response = response()->json(['status'=>'success', 'message'=>"Telecalling Id based details", 'data'=>$results]);
		}else{
			$response = response()->json(['status'=>'failure','message'=>"No data found"]);
		}
		return $response;
	}

	public function telecallingStatusUpdate(Request $request){
		$Telecallings = new Telecalling();

		$Telecallings->telecalling_id = $request->input('telecalling_id');
		// $Telecallings->status 		  = $request->input('status');

		$results = $Telecallings->telecallingStatusUpdate($Telecallings);

		if($results == 1){
			$response = response()->json(['status'=>'success', 'message'=>"Telecalling status updated"]);
		}else{
			$response = response()->json(['status'=>'failure','message'=>"No data found"]);
		}
		return $response;
	}
}