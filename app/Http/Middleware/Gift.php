<?php

namespace App\Http\Controllers;

use Closure;
use DB;
use Mail;
use Carbon\Carbon;

require(base_path().'/app/Http/Middleware/Common.php');

class Gift
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        return $next($request);
    }
///////////////////////////////////////
//Gift Inventory Management Api Calls//
///////////////////////////////////////
    //Admin Or Sub Admin Add the Gift
    public function addGift($Gifts) 
    {   
        $exit = DB::table('gpff_gift')
                ->where('gift_name', $Gifts->gift_name)
                ->where('region_id', $Gifts->region_id)
                ->where('branch_id', $Gifts->branch_id)
                ->get();

        if(count($exit) > 0){
            return 2;
        } else{
            $values = array(
                'gift_name'         => $Gifts->gift_name , 
                'gift_description'  => $Gifts->gift_description ,
                'gift_cr_id'        => $Gifts->gift_cr_id , 
                'region_id'         => $Gifts->region_id,
                'region_name'       => $Gifts->region_name,
                'branch_id'         => $Gifts->branch_id,
                'gift_status'       => 1,
                'created_at'        => date('Y-m-d H:i:s') , 
                'updated_at'        => date('Y-m-d H:i:s')
            );

            DB::table('gpff_gift')
            ->insert($values);
            
            return 1;
        }        
    }
    //Update Gift details
    public function updateGift($Gifts) 
    {
        $values = array(
                'gift_name'         => $Gifts->gift_name , 
                'gift_description'  => $Gifts->gift_description ,
                'gift_cr_id'        => $Gifts->gift_cr_id , 
                'region_id'         => $Gifts->region_id,
                'updated_at'        => date('Y-m-d H:i:s')
            );
        return  DB::table('gpff_gift')
                ->where('gift_id', $Gifts->gift_id)
                ->update($values);
    }
    //Delete Gift details
    public function removeGift($Gifts)
    {
        return  DB::table('gpff_gift')
                ->where('gift_id', $Gifts->gift_id)
                ->delete();
    }
    //Get Indivitual Gift Details
    public function getIndGift($Gifts)
    {   
        return  DB::table('gpff_gift')
                ->where('gift_id', $Gifts->gift_id)
                ->get();
    }
    //Get All Gift Details
    public function getAllGift() 
    {
        $data =[];
        $data['totaldata']= DB::table('gpff_gift as gpgif')
                            ->join('gpff_branch as gpbr','gpgif.branch_id', '=' , 'gpbr.branch_id')
                            ->orderBy('gpgif.updated_at','DESC')
                            ->get(['gpgif.gift_id','gpgif.gift_name','gpgif.gift_description','gpgif.gift_cr_id','gpgif.branch_id','gpgif.region_id','gpgif.region_name','gpgif.area_id','gpgif.gift_status','gpgif.created_at','gpgif.updated_at','gpbr.branch_name']);
         return $data;   
    }
    // Fetch Region based gift Details
    public function getRegionBasedGift($Gifts) 
    {
        $data =[];
        $data['totaldata']= DB::table('gpff_gift')
                            ->where('region_id',$Gifts->region_id)
                            ->orderBy('updated_at','DESC')
                            ->get();
         return $data;   
    }

//Stockist SIde ADD
    public function addGiftQty($Gifts)
    {  
        $exit = DB::table('gpff_gift_stock')
                ->where('gift_id', $Gifts->gift_id)
                ->where('warehouse_id', $Gifts->warehouse_id)
                ->get();

        if(count($exit) > 0){
            return 2;        
        } else{
            $values = array(
                'gift_name'         => $Gifts->gift_name ,
                'gift_id'           => $Gifts->gift_id ,
                'gift_blc_qty'      => $Gifts->gift_qty ,
                'gift_tot_qty'      => $Gifts->gift_qty ,
                'gift_description'  => $Gifts->gift_description ,
                'warehouse_id'      => $Gifts->warehouse_id , 
                'region_id'         => $Gifts->region_id , 
                'branch_id'         => $Gifts->branch_id,
                'gift_status'       => 1,
                'created_at'        => date('Y-m-d H:i:s') , 
                'updated_at'        => date('Y-m-d H:i:s')
            );

            $giftid   =   DB::table('gpff_gift_stock')
                          ->insertGetId($values);

            //History Maintaintance(type 2 = Gift)
            $values1 = array(
                'id'            => $giftid ,
                'orginal_id'    => $Gifts->gift_id ,
                'qty'           => $Gifts->gift_qty ,
                'warehouse_id'  => $Gifts->warehouse_id , 
                'region_id'     => $Gifts->region_id , 
                'branch_id'     => $Gifts->branch_id,
                'date'          => date('Y-m-d H:i:s') , 
                'type'          => 2 ,
                'created_at'    => date('Y-m-d H:i:s') , 
                'updated_at'    => date('Y-m-d H:i:s')
            );
            DB::table('gpff_stock_add_history')
            ->insert($values1);
            //End History Maintaintance(type 2 = Gift)
        return 1;
        }
    }
    //Update Gift details
    public function updateGiftQty($Gifts) 
    {   
        //History Maintaintance(type 2 = Gift)
        $values1 = array(
            'id'            => $Gifts->gift_stock_id ,
            'orginal_id'    => $Gifts->gift_id ,
            'qty'           => $Gifts->gift_qty ,
            'warehouse_id'  => $Gifts->warehouse_id , 
            'region_id'     => $Gifts->region_id , 
            'branch_id'     => $Gifts->branch_id,
            'date'          => date('Y-m-d H:i:s') , 
            'type'          => 2 ,
            'created_at'    => date('Y-m-d H:i:s') , 
            'updated_at'    => date('Y-m-d H:i:s')
        );
        DB::table('gpff_stock_add_history')
        ->insert($values1);
        //End History Maintaintance(type 2 = Gift)

        $tot_qty =  DB::table('gpff_gift_stock')
                    ->where('gift_stock_id', $Gifts->gift_stock_id)
                    ->where('warehouse_id', $Gifts->warehouse_id)
                    ->where('region_id', $Gifts->region_id)
                    ->sum('gift_tot_qty');

        $blc_qty =  DB::table('gpff_gift_stock')
                    ->where('gift_stock_id', $Gifts->gift_stock_id)
                    ->where('warehouse_id', $Gifts->warehouse_id)
                    ->where('region_id', $Gifts->region_id)
                    ->sum('gift_blc_qty');

        $qty = $Gifts->gift_qty;

        $tot = $tot_qty + $qty;
        $blc = $blc_qty + $qty;

        $values = array(
                'gift_tot_qty'      => $tot ,
                'gift_blc_qty'      => $blc ,
                'updated_at'        => date('Y-m-d H:i:s')
            );
        return  DB::table('gpff_gift_stock')
                ->where('gift_stock_id', $Gifts->gift_stock_id)
                ->where('warehouse_id', $Gifts->warehouse_id)
                ->where('region_id', $Gifts->region_id)
                ->update($values);
    }
    // Fetch Warehouse based gift Details
    public function getWarehouseBasedGiftStock($Gifts) 
    {
       return DB::table('gpff_gift_stock')
            ->where('warehouse_id',$Gifts->warehouse_id)
            ->where('region_id', $Gifts->region_id)
            ->orderBy('updated_at','DESC')
            ->get();
    }
//Stockist SIde
    
    

////////////////////////////////////
//Gift Assign Management Api Calls//
///////////////////////////////////
    //Admin Or Sub Admin Add the Gift 
    public function giftAssign($Gifts) 
    {   
        if($Gifts->area_manager_id){
        $manager  =  DB::table('gpff_users')
                    ->where('user_id', $Gifts->area_manager_id)
                    ->First(['firstname','lastname']);
        $managername =  $manager->firstname.' '.$manager->lastname;
        } else{
            $managername ="";
        }

        if($Gifts->region_manager_id){
            $manager  =  DB::table('gpff_users')
                    ->where('user_id', $Gifts->region_manager_id)
                    ->First(['firstname','lastname']);
            $regmanagername =  $manager->firstname.' '.$manager->lastname;
        } else{
            $regmanagername ="";
        }

        foreach ($Gifts->fieldofficers as $fieldofficerid) {

            $field_name  =  DB::table('gpff_users')
                    ->where('user_id', $fieldofficerid)
                    ->First(['firstname','lastname']);
            $fieldname =  $field_name->firstname.' '.$field_name->lastname;

            $val = array(
                'gift_id'           => $Gifts->gift_id,
                'gift_name'         => $Gifts->gift_name,
                'qty'               => $Gifts->gift_totqty,
                'field_officer_id'  => $fieldofficerid,
                'field_officer_name'=> $fieldname,
                'area_manager_id'   => $Gifts->area_manager_id,
                'manager_name'      => $managername,
                'warehouse_id'      => $Gifts->warehouse_id,
                'region_manager_id' => $Gifts->region_manager_id,
                'region_manager_name'=> $regmanagername,
                //'stockist_id'       => $Gifts->stockist_id,
                'date'              => date('Y-m-d H:i:s') , 
                'region_id'         => $Gifts->region_id,
                'created_at'        => date('Y-m-d H:i:s') , 
                'updated_at'        => date('Y-m-d H:i:s')
            );
            DB::table('gpff_giftassign')
            ->insert($val);

            //Notification Entry
            if($Gifts->area_manager_id){
                $name = DB::table('gpff_users')
                    ->where('user_id', $Gifts->area_manager_id)
                    ->First();
                $message = "Manager ".$name->firstname." has assign Gift to you.";
                $page_id = 'UNKNOWN';

                $cmn = new Common();
                $cmn->insertNotification($Gifts->area_manager_id,$name->firstname,$fieldofficerid,$message,$page_id);
                //Stockist
                $waremessage = "Manager ".$name->firstname." has assign Gift in your Warehouse.";
                $page_id = 'UNKNOWN';

                $warename = DB::table('gpff_users')
                    ->where('warehouse_id', $Gifts->warehouse_id)
                    ->First();

                $cmn = new Common();
                $cmn->insertNotification($Gifts->area_manager_id,$name->firstname,$warename->user_id,$waremessage,$page_id);

            } else if ($Gifts->region_manager_id) {
                $name = DB::table('gpff_users')
                    ->where('user_id', $Gifts->region_manager_id)
                    ->First();
                $message = "Manager ".$name->firstname." has assign Gift to you.";
                $page_id = 'UNKNOWN';

                $cmn = new Common();
                $cmn->insertNotification($Gifts->region_manager_id,$name->firstname,$fieldofficerid,$message,$page_id);
                //Stockist
                $warename = DB::table('gpff_users')
                    ->where('warehouse_id', $Gifts->warehouse_id)
                    ->First();

                $waremessage = "Manager ".$name->firstname." has assign Gift in your Warehouse.";
                $page_id = 'UNKNOWN';

                $cmn = new Common();
                $cmn->insertNotification($Gifts->region_manager_id,$name->firstname,$warename->user_id,$waremessage,$page_id);

            }   
        }
        //End Notification Entry
        return 1;
    }
    //Get Completed Assign Pending Gifts for Stockist
    public function getReceivedGiftDet($Gifts){

        if($Gifts->type == 0){ //Waiting 

              return  DB::table('gpff_giftassign')
                ->where('warehouse_id', $Gifts->warehouse_id)
                ->where('giftassign_status', 0)
                ->orderBy('updated_at','DESC')
                ->get();

        } else if($Gifts->type == 1){ //Received

              return  DB::table('gpff_giftassign')
                ->where('warehouse_id', $Gifts->warehouse_id)
                ->where('giftassign_status', 1)
                ->orderBy('updated_at','DESC')
                ->get();
        }
    }
    // Get My Assign Gift Details
    public function getMyAssignGifts($Gifts)
    {   
        return  DB::table('gpff_giftassign')
                ->where('field_officer_id', $Gifts->field_officer_id)
                ->where('giftassign_status', 0)
                ->orderBy('updated_at','DESC')
                ->get();
    }
    //Area Manager Assign Giftss List Details Fetch
    public function getManagerAssignGifts($Gifts)
    {   
        return  DB::table('gpff_giftassign as gpgi')
                ->leftjoin('gpff_warehouse as gpwa','gpgi.warehouse_id','gpwa.warehouse_id')
                ->leftjoin('gpff_region as gpre','gpgi.region_id','gpre.region_id')
                ->where('gpgi.area_manager_id', $Gifts->area_manager_id)
                //->where('gpgi.giftassign_status', )
                ->orderBy('gpgi.updated_at','DESC')
                ->get(['gpgi.giftassign_id', 'gpgi.created_at', 'gpgi.gift_name','gpgi.qty','gpgi.field_officer_name','gpre.region_name','gpwa.warehouse_name']);
    }
    //Region Manager Assign Giftss List Details Fetch
    public function getRegionManageAssignGifts($Gifts)
    {   
        $data = DB::table('gpff_users')
                    ->where('user_id', $Gifts->user_id)
                    ->First(['region_id']);

        $myArray = explode(',', $data->region_id);
        
        return  DB::table('gpff_giftassign')
                ->whereIn('region_id', $myArray)
                ->orderBy('updated_at','DESC')
                ->get();
    }

    //Give Gift to Field Officer
    public function giveGifttoFO($Gifts){

        //Get Gift Details
        $giftassign  =  DB::table('gpff_giftassign')
                    ->where('giftassign_id', $Gifts->giftassign_id)
                    ->First();

        $exit = DB::table('gppf_fo_received_gift')
                ->where('gift_id', $giftassign->gift_id)
                ->where('field_officer_id',$Gifts->field_officer_id)
                ->get();

        if(count($exit) > 0){

            $tot_qtys  =  DB::table('gppf_fo_received_gift')
                     ->where('gift_id', $giftassign->gift_id)
                    ->where('field_officer_id',$Gifts->field_officer_id)
                     ->sum('tot_qty');

            $blc_qtys  =  DB::table('gppf_fo_received_gift')
                     ->where('gift_id', $giftassign->gift_id)
                    ->where('field_officer_id',$Gifts->field_officer_id)
                     ->sum('blc_qty');

            $qtys   = $giftassign->qty;
            $tot    = $tot_qtys + $qtys;
            $blc    = $blc_qtys + $qtys;

            $value = array(
                        'tot_qty'      => $tot ,
                        'blc_qty'      => $blc ,
                        'updated_at'   => date('Y-m-d H:i:s')
                    );

            DB::table('gppf_fo_received_gift')
            ->where('gift_id', $giftassign->gift_id)
                    ->where('field_officer_id',$Gifts->field_officer_id)
            ->update($value);

        } else{
            //Insert FO Received Table
            $values1 = array(
                'giftassign_id'     => $Gifts->giftassign_id ,
                'gift_id'           => $giftassign->gift_id ,
                'field_officer_id'  => $Gifts->field_officer_id,
                'tot_qty'           => $giftassign->qty,
                'blc_qty'           => $giftassign->qty,
                'date'              => date('Y-m-d H:i:s') , 
                'status'            => 1 ,
                'warehouse_id'      => $giftassign->warehouse_id,
                'created_at'        => date('Y-m-d H:i:s') , 
                'updated_at'        => date('Y-m-d H:i:s')
            );

            DB::table('gppf_fo_received_gift')
            ->insert($values1);
        }
        //Reduce gift in Overall Stock
        $blc_qtys  =  DB::table('gpff_gift_stock')
                     ->where('gift_id', $giftassign->gift_id)
                     ->sum('gift_blc_qty');

        $sal_qtys  =  DB::table('gpff_gift_stock')
                    ->where('gift_id', $giftassign->gift_id)
                    ->sum('gift_sales_qty');

        $qtys = $giftassign->qty;
        $sales  = $sal_qtys + $qtys;
        $blc    = $blc_qtys - $qtys;

        $value = array(
                    'gift_blc_qty'      => $blc ,
                    'gift_sales_qty'    => $sales ,
                    'updated_at'        => date('Y-m-d H:i:s')
                );

        DB::table('gpff_gift_stock')
        ->where('gift_id', $giftassign->gift_id)
        ->update($value);
        //End Reduce Gifts in Overall Stock

        //Change Gifts Status
        $val = array(
                    'giftassign_status' => 1 ,
                    'updated_at'    => date('Y-m-d H:i:s')
                );

        DB::table('gpff_giftassign')
        ->where('giftassign_id',$Gifts->giftassign_id)
        ->where('field_officer_id',$Gifts->field_officer_id)
        ->update($val);

        return 1;
    }
    // Get My Assign Gift Details
    public function getFOGift($Gift)
    {   
        return  DB::table('gppf_fo_received_gift as gfrg')
                ->leftjoin('gpff_gift as gpgi','gfrg.gift_id','gpgi.gift_id')
                ->where('gfrg.field_officer_id', $Gift->field_officer_id)
                ->orderBy('gfrg.updated_at','DESC')
                ->get();
    }

    
    
    // Get Admin Assign Gift Details
    public function getAllAssignGifts()
    {   
        return  DB::table('gpff_giftassign as gpgi')
                ->leftjoin('gpff_warehouse as gpwa','gpgi.warehouse_id','gpwa.warehouse_id')
                ->leftjoin('gpff_region as gpre','gpgi.region_id','gpre.region_id')
                ->leftjoin('gpff_branch as gpbr','gpwa.branch_id','gpbr.branch_id')
                ->where('gpgi.giftassign_status', 1)
                ->orderBy('gpgi.updated_at','DESC')
                ->get();
    }
    //Field Officer Give the Gift to the Customer
    public function giftToCustomer($Gifts) 
    {   
        $blc_qty  =  DB::table('gppf_fo_received_gift')
                     ->where('field_officer_id', $Gifts->field_officer_id)
                     ->where('gift_id', $Gifts->gift_id)
                     ->sum('blc_qty');

        if($blc_qty > 0){
            $values = array(
                'gift_id'           => $Gifts->gift_id , 
                'gift_name'         => $Gifts->gift_name ,
                'gift_qty'          => $Gifts->gift_qty ,
                'field_officer_id'  => $Gifts->field_officer_id ,
                'field_officer_name'=> $Gifts->field_officer_name , 
                'customer_id'       => $Gifts->customer_id ,
                'task_id'           => $Gifts->task_id ,
                'gift_cmt'          => $Gifts->gift_cmt , 
                'created_at'        => date('Y-m-d H:i:s') , 
                'updated_at'        => date('Y-m-d H:i:s')
            );
            DB::table('gpff_delivered_gift')
            ->insert($values);

            //Reduce Gift in Field Officer SIde
            $sal_qty  =  DB::table('gppf_fo_received_gift')
                        ->where('field_officer_id', $Gifts->field_officer_id)
                        ->where('gift_id', $Gifts->gift_id)
                        ->sum('sales_qty');

            $qty = $Gifts->gift_qty;

            $sales  = $sal_qty + $qty;
            $blc    = $blc_qty - $qty;

            $assgin_value = array(
                        'blc_qty'       => $blc ,
                        'sales_qty'     => $sales ,
                        'updated_at'    => date('Y-m-d H:i:s')
                    );

                    DB::table('gppf_fo_received_gift')
                    ->where('gift_id', $Gifts->gift_id)
                    ->where('field_officer_id', $Gifts->field_officer_id)
                    ->update($assgin_value);
            //End Reduce Gift in Field Officer SIde
            //Notification Entry
            /*$name = DB::table('gpff_users')
                    ->where('user_id', $Gifts->field_officer_id)
                    ->First();
                $message = "Field Officer ".$name->firstname." has Gift giving to the customer.";
                $page_id = 'UNKNOWN';

                $cmn = new Common();
                $cmn->insertNotification($Gifts->field_officer_id,$name->firstname,$name->area_manager_id,$message,$page_id);*/
            //End Notification Entry
            return 1;
        } else{
            return 2;
        }
    }
}
