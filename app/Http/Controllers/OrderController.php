<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use File;
use Mail;
use App\Jobs\InvoiceGenerate;

require(base_path().'/app/Http/Middleware/Order.php');

class OrderController extends Controller
{
//////////////////////////////
//Order Management Api Calls//
//////////////////////////////
	//Add Order Details
    public function addOrderDetails(Request $request){
        
        $Orders = new Order();
        $Orders->task_id    		= $request->input('task_id');
        $Orders->field_officer_id   = $request->input('field_officer_id');
        $Orders->field_officer_name = $request->input('field_officer_name');
        $Orders->area_manager_id    = $request->input('area_manager_id');
        $Orders->customer_id     	= $request->input('customer_id');
        $Orders->customer_name        = $request->input('customer_name');
        $Orders->biller_id        = $request->input('biller_id');
        $Orders->biller_name        = $request->input('biller_name');
        $Orders->order_date         = $request->input('order_date');
        $Orders->or_gross_total        = $request->input('or_gross_total');
        $Orders->or_tot_price        = $request->input('or_tot_price');
        $Orders->order_discount        = $request->input('order_discount');
        $Orders->branch_id        = $request->input('branch_id');
        $Orders->region_id        = $request->input('region_id');
        $Orders->area_id        = $request->input('area_id');
        $Orders->stokist_id        = $request->input('stokist_id');
        $Orders->warehouse_id        = $request->input('warehouse_id');
        $Orders->net_or_gross_price = $request->input('net_or_gross_price'); //1=>net,2=>gross_price
        $Orders->payment_type = $request->input('payment_type');//1-COD,2-credit
        $Orders->credit_lim        = $request->input('credit_lim');
        $Orders->credit_valid_to        = $request->input('credit_valid_to');
        $Orders->or_type        = $request->input('or_type');
        $Orders->payment_img = $request->input('payment_img');
        $Orders->tot_box = $request->input('tot_box');
        $Orders->order_list      	= $request->input('order_list'); 
        $Orders->spl_discount        = $request->input('spl_discount');
        $Orders->field_officer_list = $request->input('field_officer_list');
        $Orders->order_created_by   = $request->input('order_created_by');

        if($request->input('telecalling_id')){
            $Orders->telecalling_id = $request->input('telecalling_id');
        }else{
            $Orders->telecalling_id = "";
        }

        if($request->input('telecalling_type')){
            $Orders->telecalling_type = $request->input('telecalling_type');
        }else{
            $Orders->telecalling_type = "";
        }

        if($request->input('events')){
            $Orders->events = $request->input('events');
        }else{
            $Orders->events = "";
        }

        if($Orders->or_type == 3){
            $result = $Orders->storeOrderDetails($Orders);
        }else{
            $result = $Orders->custmerOrderDetails($Orders);
        }

        // }
        if($result == 1){
            $response = response()->json(['status' => 'success' , 'message' => 'Order Placed successfully']);
        } else if($result == 2){
            $response = response()->json(['status' => 'failure' , 'message' => 'Your lat and lan not to Specified']);
        } else if($result == 3){
            $response = response()->json(['status' => 'failure' , 'message' => 'Please check your product and foc qty.']);
        }else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Order Placed failed']);
        }
        return $response;

    }
    // Fetch field officer Order Details
    public function getFieldOfficerOrder(Request $request)
    {
        $Orders = new Order();
        $Orders->field_officer_id   = $request->input('field_officer_id');
        $Orders->type   = $request->input('type');
        $result = $Orders->getFieldOfficerOrder($Orders);
        if($result){
            $response = response()->json(['status' => 'success' , 'message' => '', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Invalid Order details']); 
        }
        return $response;
    }

    // Fetch field officer Order List Details
    public function getOrderBasedList(Request $request)
    {
        $Orders = new Order();
        $Orders->order_id   = $request->input('order_id');

        $result = $Orders->getOrderBasedList($Orders);
        if($result){
            $response = response()->json(['status' => 'success' , 'message' => '', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Invalid Order details']); 
        }
        return $response;
    }
    // Fetch Order Batch List Details
    public function getOrderBasedBatchList(Request $request)
    {
        $Orders = new Order();
        $Orders->order_id   = $request->input('order_id');
        $Orders->vendor     = $request->input('vendor');

        $result = $Orders->getOrderBasedBatchList($Orders);
        // print_r($result);
        // print_r("expression");
        if($result){
            $response = response()->json(['status' => 'success' , 'message' => '', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Invalid Order details']); 
        }
        return $response;
    }
    // Fetch Order Based Events details
    public function getOrderBasedEventsList(Request $request)
    {
        $Orders = new Order();
        $Orders->order_id   = $request->input('order_id');

        $result = $Orders->getOrderBasedEventsList($Orders);
        if(count($result) > 0){
            $response = response()->json(['status' => 'success' , 'message' => 'The order event details fetch successfully', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'No data found', 'data' => $result]); 
        }
        return $response;
    }
    //Get manager Order Details
    public function getManagerOrder(Request $request)
    {
        $Orders = new Order();
        $Orders->area_manager_id   = $request->input('area_manager_id');
        $Orders->start_date = $request->input('start_date');
        $Orders->end_date = $request->input('end_date');

        if($request->input('field_officer_id')){
            $Orders->field_officer_id = $request->input('field_officer_id');
        }else{
            $Orders->field_officer_id = "";
        }

        $result = $Orders->getManagerOrder($Orders);
        if(count($result) > 0){
            $response = response()->json(['status' => 'success' , 'message' => '', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'No data found']); 
        }
        return $response;
    }

    
    // Fetch manager Order List Details
    public function getManagerOrderBasedList(Request $request)
    {
        $Orders = new Order();
        $Orders->order_id   = $request->input('order_id');

        $result = $Orders->getManagerOrderBasedList($Orders);
        if($result){
            $response = response()->json(['status' => 'success' , 'message' => '', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Invalid Order details']); 
        }
        return $response;
    }
    // Fetch (All)Order Details
    public function getAllOrder()
    {
        $Orders = new Order();

        $data = $Orders->getAllOrder();
        if(count($data) > 0 ){
            $response = response()->json(['status' => 'success' , 'message' => 'Order Data retrived successfully','data'=>$data]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Error in Get details..','data'=>'null']);
        }
        return $response;
    } 
    public function getAllOrderDashboard(Request $request)
    {
        $Orders = new Order();

        $Orders->start_date = $request->input('start_date');
        $Orders->end_date = $request->input('end_date');

        $data = $Orders->getAllOrderDashboard($Orders);
        if(count($data) > 0 ){
            $response = response()->json(['status' => 'success' , 'message' => 'Order Data retrived successfully','data'=>$data]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Error in Get details..','data'=>'null']);
        }
        return $response;
    }  
    // Fetch (All)Order Details
    public function getBranchAllOrder(Request $request)
    {
        $Orders = new Order();
        $Orders->user_id  = $request->input('user_id');
        $data = $Orders->getBranchAllOrder($Orders);
        if(count($data) > 0 ){
            $response = response()->json(['status' => 'success' , 'message' => 'Order Data retrived successfully','data'=>$data]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Error in Get details..','data'=>'null']);
        }
        return $response;
    }   

    // Fetch (All)Order Details
    public function getWarAllOrder(Request $request)
    {
        $Orders = new Order();
        $Orders->warehouse_id  = $request->input('warehouse_id');
        $Orders->type  = $request->input('type');
        $data = $Orders->getWarAllOrder($Orders);
        if(count($data) > 0 ){
            $response = response()->json(['status' => 'success' , 'message' => 'Order Data retrived successfully','data'=>$data]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'No data found..','data'=>'null']);
        }
        return $response;
    }   
    // Update Order Status
    public function updateOrderStatus(Request $request)
    {
        $Orders = new Order();
        $Orders->order_id  = $request->input('order_id');
        $Orders->order_status  = $request->input('order_status');
        $Orders->customer_id  = $request->input('customer_id');
        $Orders->or_tot_price  = $request->input('or_tot_price');
        $Orders->reject_reason  = $request->input('reject_reason');

        $result = $Orders->updateOrderStatus($Orders);
        if($result > 0){
            $response = response()->json(['status' => 'success', 'message' => 'Order Status Update successfully','data'=> $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Error in Update Order Status']);
        }
        return $response;
    }

    //Get Order For Stockist
    public function getPlacedOrder(Request $request)
    {
        $Orders = new Order();
        $Orders->warehouse_id  = $request->input('warehouse_id');
        $Orders->type  = $request->input('type');

        $Orders->start_date = $request->input('start_date');
        $Orders->end_date = $request->input('end_date');

        $result = $Orders->getPlacedOrder($Orders);
        if($result > 0){
            $response = response()->json(['status' => 'success', 'message' => 'Placed order fetched successfully','data'=> $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Error in Update Order Status']);
        }
        return $response;
    }

    //Get all Order For Stockist
    public function getwarehouseOrder(Request $request)
    {
        $Orders = new Order();

        $Orders->start_date = $request->input('start_date');
        $Orders->end_date = $request->input('end_date');
        $Orders->warehouse_id  = $request->input('warehouse_id');

        $result = $Orders->getwarehouseOrder($Orders);
        if($result){
            $response = response()->json(['status' => 'success', 'message' => 'Placed order fetched successfully','data'=> $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Error in Update Order Status']);
        }
        return $response;
    }

    // ORDER ACCEPT
    public function orderAccept(Request $request)
    {
        $Orders = new Order();
        $Orders->order_id  = $request->input('order_id');
        $Orders->order_status  = $request->input('order_status');
        $Orders->products  = $request->input('products');
        $Orders->grand_total  = $request->input('grand_total');
        $Orders->tot_box  = $request->input('tot_box');
    
        $result = $Orders->orderAccept($Orders);
        if($result > 0){
            $response = response()->json(['status' => 'success', 'message' => 'Order has been accepted successfully','data'=> $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Error in order accepting process']);
        }
        return $response;
    }

    //Customer Over limit Order Req
    public function cusOverlimitOrderReq(Request $request)
    {
        $Orders = new Order();
        $Orders->order_id  = $request->input('order_id');
        $Orders->customer_id  = $request->input('customer_id');
        $Orders->or_tot_price  = $request->input('or_tot_price');
        $Orders->credit_limit  = $request->input('credit_limit');
        $Orders->stokist_id  = $request->input('stokist_id');
        $Orders->warehouse_id  = $request->input('warehouse_id');
        $Orders->branch_id  = $request->input('branch_id');
        $Orders->region_id  = $request->input('region_id');
        $Orders->area_id  = $request->input('area_id');
    
        $result = $Orders->cusOverlimitOrderReq($Orders);
        if($result == 1){
            $response = response()->json(['status' => 'success', 'message' => 'Order successfully send to the admin ','data'=> $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Error in order send process']);
        }
        return $response;
    }

    //Get Limit Overdue Order Request
    public function getLimitOverData()
    {
        $Orders = new Order();
        $result = $Orders->getLimitOverData();
        if(count($result)){
            $response = response()->json(['status' => 'success', 'message' => 'Order Request fetched successfully','data'=> $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Error in Fetch Order Request Details']);
        }
        return $response;
    }
    //Get Limit Overdue Order Request History
    public function getHistoryLimitOverData(Request $request)
    {
        $Orders = new Order();
        $Orders->user_id  = $request->input('user_id');
        $result = $Orders->getHistoryLimitOverData($Orders);
        if(count($result)){
            $response = response()->json(['status' => 'success', 'message' => 'Order Request Details fetched successfully','data'=> $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Error in Fetch Order Request Details']);
        }
        return $response;
    }


    //Admin Get Approval Order Details
    public function getApprovalOrderData()
    {
        $Orders = new Order();
        $result = $Orders->getApprovalOrderData();
        if(count($result)){
            $response = response()->json(['status' => 'success', 'message' => 'Order Approval Request fetched successfully','data'=> $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Error in Fetch Order Approval Approval Details']);
        }
        return $response;
    }
    // ORDER Spl Foc or Discount Approve
    public function orderFOCDiscountAccept(Request $request)
    {
        $Orders = new Order();
        $Orders->order_id  = $request->input('order_id');
        $Orders->products_list  = $request->input('products_list');
        $Orders->spl_discount  = $request->input('spl_discount');
        $Orders->admin_cmt  = $request->input('admin_cmt');
        //$Orders->or_tot_price  = $request->input('or_tot_price');
    
        $result = $Orders->orderFOCDiscountAccept($Orders);
        if($result == 1){
            $response = response()->json(['status' => 'success', 'message' => 'Order has been accepted successfully','data'=> $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Error in order accepting process']);
        }
        return $response;
    }
    public function acceptCustomerOrderdetails(Request $request){
        $Orders = new Order();
        $Orders->order_id = $request->input('order_id');
        $Orders->tot_box = $request->input('tot_box');
        $Orders->order_list = $request->input('order_list');
        $Orders->order_status = $request->input('order_status');
        $Orders->or_type = $request->input('or_type');
        $Orders->credit_lim = $request->input('credit_lim');
        $Orders->credit_valid_to = $request->input('credit_valid_to');
        $Orders->spl_discount = $request->input('spl_discount');
        $Orders->or_gross_total = $request->input('or_gross_total');
        $Orders->order_discount  = $request->input('order_discount');
        $Orders->or_tot_price = $request->input('or_tot_price');
        $Orders->field_officer_id = $request->input('field_officer_id');
        $Orders->field_officer_name = $request->input('field_officer_name');
        $Orders->biller_id = $request->input('biller_id');
        $Orders->biller_name = $request->input('biller_name');
        $Orders->warehouse_id = $request->input('warehouse_id');
        // print_r($Orders->order_list[0]->comments);
        // exit;
        $result = $Orders->acceptCustomerOrderdetails($Orders);
        // return view('or_invoice_2',$result);
        // exit;
        if ($result == 1){
            $response = response()->json(['status'=>'Success', 'message'=>'Order updated']);
        }else if($result == 2){
            $response = response()->json(['status' => 'failure' , 'message' => 'This order already accepted']);
        }else if($result == 3){
            $response = response()->json(['status' => 'failure' , 'message' => 'Please check your product and foc qty.']);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Error in order accepting process']);
        }
        return $response;
    }

    public function getOrderBeforeAdminApproval(Request $request){

        $Orders = new Order();
        $Orders->order_id = $request->input('order_id');

        $result  = $Orders->getOrderBeforeAdminApproval($Orders);
        if (count($result)>0){
            $response = response()->json(['status'=>'success', 'message'=>'', 'data'=>$result]);
        }else{
            $response = response()->json(['status'=>'failure', 'message'=>'No data found']);
        }
        return $response;

    }

    public function getOrderBasedFOCDetails(Request $request){
        $Orders = new Order();
        $Orders->order_id = $request->input('order_id');
        $result = $Orders->getOrderBasedFOCDetails($Orders);
        if (count($result)>0){
            $response = response()->json(['status'=>'success', 'message'=>'', 'data'=>$result]);
        }else{
            $response = response()->json(['status'=>'failure', 'message'=>'No data found']);
        }
        return $response;
    }

    public function getOrderBatchFOCDetails(Request $request){
        $Orders = new Order();
        $Orders->order_id = $request->input('order_id');
        $result = $Orders->getOrderBatchFOCDetails($Orders);
        if (count($result)>0){
            $response = response()->json(['status'=>'success', 'message'=>'', 'data'=>$result]);
        }else{
            $response = response()->json(['status'=>'failure', 'message'=>'No data found']);
        }
        return $response;
    }

    // Order Based Order Details
    public function getOrderBasedOrderList($order_id)
    {
        $Orders = new Order();
        $Orders->order_id   = $order_id;

        $result = $Orders->getOrderBasedOrderList($Orders);
        if($result){
            $response = response()->json(['status' => 'success' , 'message' => '', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Invalid Order details']); 
        }
        return $response;
    }


    public function generateOrderPDF(Request $request){
        
        $Orders = new Order();
        $Orders->order_id            = $request->input('order_id');
        $Orders->invoice_date        = $request->input('invoice_date');
        $Orders->type        = $request->input('type');
        if($Orders->type == 1){
            $result = $Orders->generateOrderPDF($Orders);        
        }else if($Orders->type == 2){
            $result = $Orders->generateVendorOrderPDF($Orders);
        }
        
        if($result == 1){
            $response = response()->json(['status' => 'success' , 'message' => 'Order Placed successfully']);
        } else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Order Placed failed']);
        }
        return $response;

    }

    // Fetch field officer Order List Details
    public function getOrderScheme()
    {
        $Orders = new Order();
        $result = $Orders->getOrderScheme();
        if($result){
            $response = response()->json(['status' => 'success' , 'message' => '', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'No scheme data found']); 
        }
        return $response;
    }

     public function updateOrderInvoiceDate(Request $request){
        
        $Orders = new Order();
        $Orders->filepath = $request->file('orderdetails')->getClientOriginalName();
        $Orders->file = $request->file('orderdetails');
        $result = $Orders->updateOrderInvoiceDate($Orders);
        if($result == 1){
            $response = response()->json(['status' => 'success' , 'message' => 'Order updated successfully']);
        } else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Order updation failed']);
        }
        return $response;

    }
    public function demo(){
        InvoiceGenerate::dispatch();
    }
}
