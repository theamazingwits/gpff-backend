<?php

namespace App\Http\Controllers;

use Closure;
use DB;
use \PDF;
use Mail;
use Carbon\Carbon;
use App\Imports\UsersImport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;


require(base_path().'/app/Http/Middleware/Common.php');

class Collection
{
/**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        return $next($request);
    }

    public function addCollectionDetails($Collections){

        $role = DB::table('gpff_users')
        ->where('user_id', $Collections->collection_cr_id)
        ->First();
        $finance_user_details = DB::table('gpff_users')
        ->where('user_id', $Collections->finance_user_id)
        ->First();

        if($Collections->col_type == 1){

            $values = array(
                "finance_user_id"       =>$Collections->finance_user_id,
                "invoice_no"            =>$Collections->invoice_no,
                "order_id"              =>$Collections->order_id,
                "customer_id"           =>$Collections->customer_id,
                "customer_name"         =>$Collections->customer_name,
                "col_type"              =>$Collections->col_type,
                "collection_date"       =>date('Y-m-d H:i:s'),
                "collection_for"        =>$Collections->collection_for,
                "collection_status"     =>$Collections->collection_status,
                "collection_mode"       =>$Collections->collection_mode,
                "collection_amount"     =>$Collections->collection_amount,
                "area_id"               =>$Collections->area_id,
                "region_id"             =>$Collections->region_id,
                "doer_id"               =>$Collections->finance_user_id,
                "doer_name"             =>$finance_user_details->firstname." ".$finance_user_details->lastname,
                "doer_date"             =>date('Y-m-d H:i:s'),
                "doer_status"           =>1,
                "checker_id"            =>$Collections->finance_user_id,
                "checker_name"          =>$finance_user_details->firstname." ".$finance_user_details->lastname,
                "checker_update_date"   =>date('Y-m-d H:i:s'),
                "check_status"          =>1,
                "status"                =>$Collections->status,
                "warehouse_id"          => $Collections->warehouse_id,
                "bank_name"             =>$Collections->bank_name,
                "card_type"             =>$Collections->card_type,
                "card_no"               =>$Collections->card_no,
                "holder_name"           =>$Collections->holder_name,
                "account_no"            =>$Collections->account_no,
                "transaction_id"        =>$Collections->transaction_id,
                "cheque_no"             =>$Collections->cheque_no,
                "collection_cr_id"      =>$Collections->collection_cr_id,
                "collection_cr_name"    =>$role->firstname." ".$role->lastname,
                "collection_cr_date"    =>date('Y-m-d H:i:s'),
                "collect_approve_id"    =>$Collections->collection_cr_id,
                "collect_approve_name"  =>$role->firstname." ".$role->lastname,
                "collect_approve_date"  =>date('Y-m-d H:i:s'),
                "approve_status"        => 1,
                "created_at"            =>date('Y-m-d H:i:s'),
                "updated_at"            =>date('Y-m-d H:i:s')
            );

            $colectionid = DB::table('gpff_collection')
            ->insertGetId($values);
            if($Collections->collection_for == 1){

                if(($Collections->total_price-$Collections->collection_amount)==0){

                    $amount  = array(
                        'collection_status' => 1,
                        'approve_status'=>1,
                        'updated_at' => date('Y-m-d H:i:s')
                    );

                    DB::table('gpff_collection')
                    //->where('invoice_no', $Collections->invoice_no)
                    ->where('collection_id', $colectionid)
                    ->update($amount);

                    $payment = array(
                        'payment_status' => 1,
                        'or_blc_price'   => 0,
                        'updated_at'     => date('Y-m-d H:i:s')
                    );

                    DB::table('gpff_order')
                    ->where('invoice_no', $Collections->invoice_no)
                    ->update($payment);
                    if($Collections->collection_for==1){

                        $cmn = new Common();

                        $details = DB::table('gpff_order')
                        ->where('order_id', $Collections->order_id)
                        ->First();

                        $message = "Customer ".$details->customer_name."your collection has updated successfuly.Your balance amount is".$details->or_blc_price;

                        $page_id = 'UNKNOWN';

                        $cmn->insertNotification($details->customer_id,$details->customer_name,$Collections->finance_user_id,$message,$page_id);
                    }

                    return 1;

                }else{

                    $datas = DB::table('gpff_collection')
                    ->where('invoice_no', $Collections->invoice_no)
                    ->get();
                    // print_r($datas);

                // foreach($datas as $data){
                    $balance_amount = $Collections->total_price-$Collections->collection_amount;
                    if($balance_amount==0){

                        $amount  = array(

                            'collection_status' => 1,
                            'approve_status'=>1,
                            'updated_at' => date('Y-m-d H:i:s')

                        );

                        DB::table('gpff_collection')
                        //->where('invoice_no', $Collections->invoice_no)
                        ->where('collection_id', $colectionid)
                        ->update($amount);

                        $payment = array(

                            'payment_status' => 1,
                            'or_blc_price'   => $balance_amount,
                            'updated_at'     => date('Y-m-d H:i:s')

                        );

                        DB::table('gpff_order')
                        ->where('invoice_no', $Collections->invoice_no)
                        ->update($payment);
                        if($Collections->collection_for==1){

                            $cmn = new Common();

                            $details = DB::table('gpff_order')
                            ->where('order_id', $Collections->order_id)
                            ->First();

                            $message = "Customer ".$details->customer_name."your collection has updated successfuly.Your balance amount is".$details->or_blc_price;

                            $page_id = 'UNKNOWN';

                            $cmn->insertNotification($details->customer_id,$details->customer_name,$Collections->finance_user_id,$message,$page_id);
                        }
                        return 1;

                    }else{

                        $amount  = array(
                            'collection_status' => 2,
                            'approve_status'=>1,
                            'updated_at' => date('Y-m-d H:i:s')
                        );

                        DB::table('gpff_collection')
                        //->where('invoice_no', $Collections->invoice_no)
                        ->where('collection_id', $colectionid)
                        ->update($amount);

                        $payment = array(
                            'or_blc_price'   => $balance_amount,
                            'payment_status' => 2,
                            'updated_at'     => date('Y-m-d H:i:s')
                        );

                        DB::table('gpff_order')
                        ->where('invoice_no', $Collections->invoice_no)
                        ->update($payment);
                        if($Collections->collection_for==1){
                            $cmn = new Common();

                            $details = DB::table('gpff_order')
                            ->where('order_id', $Collections->order_id)
                            ->First();

                            $message = "Customer ".$details->customer_name."your collection has updated successfuly.Your balance amount is".$details->or_blc_price;

                            $page_id = 'UNKNOWN';

                            $cmn->insertNotification($details->customer_id,$details->customer_name,$Collections->finance_user_id,$message,$page_id);
                        }
                        return 1;
                    }
                // }
                }
            }else{
                if(($Collections->total_price-$Collections->collection_amount)==0){

                    $amount  = array(
                        'collection_status' => 1,
                        'approve_status'=>1,
                        'updated_at' => date('Y-m-d H:i:s')
                    );

                    DB::table('gpff_collection')
                    //->where('invoice_no', $Collections->invoice_no)
                    ->where('collection_id', $colectionid)
                    ->update($amount);

                    $payment = array(
                        'or_blc_price'   => 0,
                        'payment_status' => 1,
                        'updated_at'     => date('Y-m-d H:i:s')
                    );

                    DB::table('gpff_vendor_order')
                    ->where('invoice_no', $Collections->invoice_no)
                    ->update($payment);
                    if($Collections->collection_for==1){
                        $cmn = new Common();

                        $details = DB::table('gpff_order')
                        ->where('order_id', $Collections->order_id)
                        ->First();

                        $message = "Customer ".$details->customer_name."your collection has updated successfuly.Your balance amount is".$details->or_blc_price;

                        $page_id = 'UNKNOWN';

                        $cmn->insertNotification($details->customer_id,$details->customer_name,$Collections->finance_user_id,$message,$page_id);
                    }
                    return 1;

                }else{

                // $datas = DB::table('gpff_collection')
                // ->where('invoice_no', $Collections->invoice_no)
                // ->get();

                // foreach($datas as $data){
                    $balance_amount = $Collections->total_price-$Collections->collection_amount;
                    if($balance_amount==0){

                        $amount  = array(

                            'collection_status' => 1,
                            'approve_status'=>1,
                            'updated_at' => date('Y-m-d H:i:s')

                        );

                        DB::table('gpff_collection')
                        //->where('invoice_no', $Collections->invoice_no)
                        ->where('collection_id', $colectionid)
                        ->update($amount);

                        $payment = array(

                            'payment_status' => 1,
                            'or_blc_price'   => $balance_amount,
                            'updated_at'     => date('Y-m-d H:i:s')

                        );


                        DB::table('gpff_vendor_order')
                        ->where('invoice_no', $Collections->invoice_no)
                        ->update($payment);
                        if($Collections->collection_for==1){
                            $cmn = new Common();

                            $details = DB::table('gpff_order')
                            ->where('order_id', $Collections->order_id)
                            ->First();

                            $message = "Customer ".$details->customer_name."your collection has updated successfuly.Your balance amount is".$details->or_blc_price;

                            $page_id = 'UNKNOWN';

                            $cmn->insertNotification($details->customer_id,$details->customer_name,$Collections->finance_user_id,$message,$page_id);
                        }
                        return 1;

                    }else{

                        $amount  = array(
                            'collection_status' => 2,
                            'approve_status'=>2,
                            'updated_at' => date('Y-m-d H:i:s')
                        );

                        DB::table('gpff_collection')
                        //->where('invoice_no', $Collections->invoice_no)
                        ->where('collection_id', $colectionid)
                        ->update($amount);

                        $payment = array(
                            'payment_status' => 2,
                            'or_blc_price'   => $balance_amount,
                            'updated_at'     => date('Y-m-d H:i:s')
                        );

                        DB::table('gpff_vendor_order')
                        ->where('invoice_no', $Collections->invoice_no)
                        ->update($payment);
                        if($Collections->collection_for==1){
                            $cmn = new Common();

                            $details = DB::table('gpff_order')
                            ->where('order_id', $Collections->order_id)
                            ->First();

                            $message = "Customer ".$details->customer_name."your collection has updated successfuly.Your balance amount is".$details->or_blc_price;

                            $page_id = 'UNKNOWN';

                            $cmn->insertNotification($details->customer_id,$details->customer_name,$Collections->finance_user_id,$message,$page_id);
                        }
                        return 1;
                    // }
                    }
                }
            }

        }elseif($Collections->col_type==2){
            $user_name = DB::table('gpff_users')
            ->where('user_id', $Collections->collection_cr_id)
            ->First();

            $values = array(
                "finance_user_id"       => $Collections->finance_user_id,
                "invoice_no"            =>$Collections->invoice_no,
                "order_id"              =>$Collections->order_id,
                "customer_id"           =>$Collections->customer_id,
                "customer_name"         =>$Collections->customer_name,
                "col_type"              =>$Collections->col_type,
                "collection_date"       =>date('Y-m-d H:i:s'),
                "collection_for"        =>$Collections->collection_for,
                "collection_status"     =>$Collections->collection_status,
                "collection_mode"       =>$Collections->collection_mode,
                "collection_amount"     =>$Collections->collection_amount,
                "collection_cr_id"      =>$Collections->collection_cr_id,
                "area_id"               =>$Collections->area_id,
                "region_id"             =>$Collections->region_id,
                "checker_id"            =>$Collections->checker_id,
                "checker_name"          =>$Collections->checker_name,
                // "checker_update_date"   =>date('Y-m-d H:i:s'),
                "warehouse_id"          => $Collections->warehouse_id,
                "check_status"          =>0,
                "doer_id"               =>$Collections->collection_cr_id,
                "doer_name"             =>$user_name->firstname." ".$user_name->lastname,
                "doer_date"             =>date('Y-m-d H:i:s'),
                "doer_status"           =>1,
                "status"                =>$Collections->status,
                "bank_name"             =>$Collections->bank_name,
                "card_type"             =>$Collections->card_type,
                "card_no"               =>$Collections->card_no,
                "holder_name"           =>$Collections->holder_name,
                "account_no"            =>$Collections->account_no,
                "transaction_id"        =>$Collections->transaction_id,
                "cheque_no"             =>$Collections->cheque_no,
                "collection_cr_id"      =>$Collections->collection_cr_id,
                "collection_cr_name"    =>$role->firstname." ".$role->lastname,
                "collection_cr_date"    =>date('Y-m-d H:i:s'),
                "approve_status"        => 0,
                "created_at"            =>date('Y-m-d H:i:s'),
                "updated_at"            =>date('Y-m-d H:i:s')
            );

            $colectionid = DB::table('gpff_collection')
            ->insertGetId($values);

            if(($Collections->total_price-$Collections->collection_amount)==0){

                $amount  = array(
                // 'check_status'=>1,
                    'collection_status' => 1,
                    'approve_status'=>0,
                    'updated_at' => date('Y-m-d H:i:s')
                );

                DB::table('gpff_collection')
                //->where('invoice_no', $Collections->invoice_no)
                ->where('collection_id', $colectionid)
                ->update($amount);

                $payment = array(
                    'or_blc_price'   => 0,
                    'payment_status' => 3,
                    'updated_at'     => date('Y-m-d H:i:s')
                );

                DB::table('gpff_vendor_order')
                ->where('invoice_no', $Collections->invoice_no)
                ->update($payment);
                return 1;

            }else{

                // $datas = DB::table('gpff_collection')
                // ->where('invoice_no', $Collections->invoice_no)
                // ->get();

                // foreach($datas as $data){
                $balance_amount = $Collections->total_price-$Collections->collection_amount;
                if($balance_amount==0){

                    $amount  = array(
                    // 'check_status'=>1,
                        'collection_status' => 1,
                        'approve_status'=>0,
                        'updated_at' => date('Y-m-d H:i:s')

                    );

                    DB::table('gpff_collection')
                    //->where('invoice_no', $Collections->invoice_no)
                    ->where('collection_id', $colectionid)
                    ->update($amount);

                    $payment = array(

                        'payment_status' => 3,
                        'or_blc_price'   => $balance_amount,
                        'updated_at'     => date('Y-m-d H:i:s')

                    );


                    DB::table('gpff_vendor_order')
                    ->where('invoice_no', $Collections->invoice_no)
                    ->update($payment);

                    return 1;

                }else{

                    $amount  = array(
                    // 'check_status'=>1,
                        'collection_status' => 2,
                        'approve_status'=>0,
                        'updated_at' => date('Y-m-d H:i:s')
                    );

                    DB::table('gpff_collection')
                    //->where('invoice_no', $Collections->invoice_no)
                    ->where('collection_id', $colectionid)
                    ->update($amount);

                    $payment = array(
                        'payment_status' => 3,
                        'or_blc_price'   => $balance_amount,
                        'updated_at'     => date('Y-m-d H:i:s')
                    );

                    DB::table('gpff_vendor_order')
                    ->where('invoice_no', $Collections->invoice_no)
                    ->update($payment);
                    return 1;
                    // }
                }
            }
        }elseif($Collections->col_type==3){
            $values = array(
                "finance_user_id"       => $Collections->finance_user_id,
                "invoice_no"            =>$Collections->invoice_no,
                "order_id"              =>$Collections->order_id,
                "customer_id"           =>$Collections->customer_id,
                "customer_name"         =>$Collections->customer_name,
                "col_type"              =>$Collections->col_type,
                "collection_date"       =>date('Y-m-d H:i:s'),
                "collection_for"        =>$Collections->collection_for,
                "collection_status"     =>$Collections->collection_status,
                "collection_mode"       =>$Collections->collection_mode,
                "collection_amount"     =>$Collections->collection_amount,
                "collection_cr_id"      =>$Collections->collection_cr_id,
                "area_id"               =>$Collections->area_id,
                "region_id"             =>$Collections->region_id,
            // "checker_id"            =>$Collections->finance_user_id,
            // "checker_name"          =>$finance_user_details->firstname,
            // "checker_update_date"   =>date('Y-m-d H:i:s'),
                // "doer_id"               =>$Collections->collection_cr_id,
                // "doer_name"             =>$role->firstname." ".$role->lastname,
                // "doer_date"             =>date('Y-m-d H:i:s'),
                "doer_status"           =>0, 
                "check_status"          =>0,
                "warehouse_id"          => $Collections->warehouse_id, 
                "status"                =>$Collections->status,
                "bank_name"             =>$Collections->bank_name,
                "card_type"             =>$Collections->card_type,
                "card_no"               =>$Collections->card_no,
                "holder_name"           =>$Collections->holder_name,
                "account_no"            =>$Collections->account_no,
                "transaction_id"        =>$Collections->transaction_id,
                "cheque_no"             =>$Collections->cheque_no,
                "collection_cr_id"      =>$Collections->collection_cr_id,
                "collection_cr_name"    =>$role->firstname." ".$role->lastname,
                "collection_cr_date"    =>date('Y-m-d H:i:s'),
                "approve_status"        => 0,
                "created_at"            =>date('Y-m-d H:i:s'),
                "updated_at"            =>date('Y-m-d H:i:s')
            );

            $colectionid = DB::table('gpff_collection')
            ->insertGetId($values);

            if(($Collections->total_price-$Collections->collection_amount)==0){

                $amount  = array(
                    'collection_status' => 1,
                    'approve_status'=>2,
                    'updated_at' => date('Y-m-d H:i:s')
                );

                DB::table('gpff_collection')
                //->where('invoice_no', $Collections->invoice_no)
                ->where('collection_id', $colectionid)
                ->update($amount);

                $payment = array(
                    'payment_status' => 4,
                    'or_blc_price'   => 0,
                    'updated_at'     => date('Y-m-d H:i:s')
                );

                DB::table('gpff_order')
                ->where('invoice_no', $Collections->invoice_no)
                ->update($payment);
                if($Collections->collection_for==1){
                    $cmn = new Common();

                    $details = DB::table('gpff_order')
                    ->where('order_id', $Collections->order_id)
                    ->First();

                    $message = "Customer ".$details->customer_name."your collection has updated successfuly.Your balance amount is".$details->or_blc_price;

                    $page_id = 'UNKNOWN';

                    $cmn->insertNotification($details->customer_id,$details->customer_name,$Collections->finance_user_id,$message,$page_id);
                }
                return 1;

            }else{

                // $datas = DB::table('gpff_collection')
                // ->where('invoice_no', $Collections->invoice_no)
                // ->get();

                // print_r($datas);
                // exit();

                // foreach($datas as $data){
             $balance_amount = $Collections->total_price-$Collections->collection_amount;
                    // print_r($balance_amount);
                    // 'or_blc_price'   => $balance_amount,
             if($balance_amount==0){

                $amount  = array(

                    'collection_status' => 1,
                    'approve_status'=>2,
                    'updated_at' => date('Y-m-d H:i:s')

                );

                DB::table('gpff_collection')
                //->where('invoice_no', $Collections->invoice_no)
                ->where('collection_id', $colectionid)
                ->update($amount);

                $payment = array(

                    'payment_status' => 4,
                    'or_blc_price'   => $balance_amount,
                    'updated_at'     => date('Y-m-d H:i:s')

                );

                DB::table('gpff_order')
                ->where('invoice_no', $Collections->invoice_no)
                ->update($payment);
                if($Collections->collection_for==1){
                    $cmn = new Common();

                    $details = DB::table('gpff_order')
                    ->where('order_id', $Collections->order_id)
                    ->First();

                    $message = "Customer ".$details->customer_name."your collection has updated successfuly.Your balance amount is".$details->or_blc_price;

                    $page_id = 'UNKNOWN';

                    $cmn->insertNotification($details->customer_id,$details->customer_name,$Collections->finance_user_id,$message,$page_id);
                }
                return 1;

            }else{

                $amount  = array(
                    'collection_status' => 2,
                    'approve_status'=>2,
                    'updated_at' => date('Y-m-d H:i:s')
                );

                DB::table('gpff_collection')
                //->where('invoice_no', $Collections->invoice_no)
                ->where('collection_id', $colectionid)
                ->update($amount);

                $payment = array(
                    'payment_status' => 4,
                    'or_blc_price'   => $balance_amount,
                    'updated_at'     => date('Y-m-d H:i:s')
                );

                DB::table('gpff_order')
                ->where('invoice_no', $Collections->invoice_no)
                ->update($payment);
                if($Collections->collection_for==1){
                    $cmn = new Common();

                    $details = DB::table('gpff_order')
                    ->where('order_id', $Collections->order_id)
                    ->First();

                    $message = "Customer ".$details->customer_name."your collection has updated successfuly.Your balance amount is".$details->or_blc_price;

                    $page_id = 'UNKNOWN';

                    $cmn->insertNotification($details->customer_id,$details->customer_name,$Collections->finance_user_id,$message,$page_id);
                }
                return 1;
            }
         }       // }
        }else{
            $values = array(
                "finance_user_id"       => $Collections->finance_user_id,
                "invoice_no"            =>$Collections->invoice_no,
                "order_id"              =>$Collections->order_id,
                "customer_id"           =>$Collections->customer_id,
                "customer_name"         =>$Collections->customer_name,
                "col_type"              =>$Collections->col_type,
                "collection_date"       =>date('Y-m-d H:i:s'),
                "collection_for"        =>$Collections->collection_for,
                "collection_status"     =>$Collections->collection_status,
                "collection_mode"       =>$Collections->collection_mode,
                "collection_amount"     =>$Collections->collection_amount,
                "collection_cr_id"      =>$Collections->collection_cr_id,
                "area_id"               =>$Collections->area_id,
                "region_id"             =>$Collections->region_id,
                "checker_id"            =>$Collections->collection_cr_id,
                "checker_name"          =>$role->firstname." ".$role->lastname,
                "checker_update_date"   =>date('Y-m-d H:i:s'),
                // "doer_id"               =>$Collections->collection_cr_id,
                // "doer_name"             =>$role->firstname." ".$role->lastname,
                // "doer_date"             =>date('Y-m-d H:i:s'),
                "doer_status"           =>1, 
                "check_status"          =>1,
                "warehouse_id"          => $Collections->warehouse_id, 
                "status"                =>$Collections->status,
                "bank_name"             =>$Collections->bank_name,
                "card_type"             =>$Collections->card_type,
                "card_no"               =>$Collections->card_no,
                "holder_name"           =>$Collections->holder_name,
                "account_no"            =>$Collections->account_no,
                "transaction_id"        =>$Collections->transaction_id,
                "cheque_no"             =>$Collections->cheque_no,
                "collection_cr_id"      =>$Collections->collection_cr_id,
                "collection_cr_name"    =>$role->firstname." ".$role->lastname,
                "collection_cr_date"    =>date('Y-m-d H:i:s'),
                "approve_status"        => 0,
                "created_at"            =>date('Y-m-d H:i:s'),
                "updated_at"            =>date('Y-m-d H:i:s')
            );

            $colectionid = DB::table('gpff_collection')
            ->insertGetId($values);

            if(($Collections->total_price-$Collections->collection_amount)==0){

                $amount  = array(
                    'collection_status' => 1,
                    'approve_status'=>2,
                    'updated_at' => date('Y-m-d H:i:s')
                );

                DB::table('gpff_collection')
                ->where('collection_id', $colectionid)
                ->update($amount);

                $payment = array(
                    'payment_status' => 3,
                    'or_blc_price'   => 0,
                    'updated_at'     => date('Y-m-d H:i:s')
                );

                DB::table('gpff_order')
                ->where('invoice_no', $Collections->invoice_no)
                ->update($payment);
                if($Collections->collection_for==1){
                    $cmn = new Common();

                    $details = DB::table('gpff_order')
                    ->where('order_id', $Collections->order_id)
                    ->First();

                    $message = "Customer ".$details->customer_name."your collection has updated successfuly.Your balance amount is".$details->or_blc_price;

                    $page_id = 'UNKNOWN';

                    $cmn->insertNotification($details->customer_id,$details->customer_name,$Collections->finance_user_id,$message,$page_id);
                }
                return 1;

            }else{

                // $datas = DB::table('gpff_collection')
                // ->where('invoice_no', $Collections->invoice_no)
                // ->get();

                // print_r($datas);
                // exit();

                // foreach($datas as $data){
             $balance_amount = $Collections->total_price-$Collections->collection_amount;
                    // print_r($balance_amount);
                    // 'or_blc_price'   => $balance_amount,
             if($balance_amount==0){

                $amount  = array(

                    'collection_status' => 1,
                    'approve_status'=>2,
                    'updated_at' => date('Y-m-d H:i:s')

                );

                DB::table('gpff_collection')
                //->where('invoice_no', $Collections->invoice_no)
                ->where('collection_id', $colectionid)
                ->update($amount);

                $payment = array(

                    'payment_status' => 3,
                    'or_blc_price'   => $balance_amount,
                    'updated_at'     => date('Y-m-d H:i:s')

                );

                DB::table('gpff_order')
                ->where('invoice_no', $Collections->invoice_no)
                ->update($payment);
                if($Collections->collection_for==1){
                    $cmn = new Common();

                    $details = DB::table('gpff_order')
                    ->where('order_id', $Collections->order_id)
                    ->First();

                    $message = "Customer ".$details->customer_name."your collection has updated successfuly.Your balance amount is".$details->or_blc_price;

                    $page_id = 'UNKNOWN';

                    $cmn->insertNotification($details->customer_id,$details->customer_name,$Collections->finance_user_id,$message,$page_id);
                }
                return 1;

            }else{

                $amount  = array(
                    'collection_status' => 2,
                    'approve_status'=>2,
                    'updated_at' => date('Y-m-d H:i:s')
                );

                DB::table('gpff_collection')
                //->where('invoice_no', $Collections->invoice_no)
                ->where('collection_id', $colectionid)
                ->update($amount);

                $payment = array(
                    'payment_status' => 3,
                    'or_blc_price'   => $balance_amount,
                    'updated_at'     => date('Y-m-d H:i:s')
                );

                DB::table('gpff_order')
                ->where('invoice_no', $Collections->invoice_no)
                ->update($payment);
                if($Collections->collection_for==1){
                    $cmn = new Common();

                    $details = DB::table('gpff_order')
                    ->where('order_id', $Collections->order_id)
                    ->First();

                    $message = "Customer ".$details->customer_name."your collection has updated successfuly.Your balance amount is".$details->or_blc_price;

                    $page_id = 'UNKNOWN';

                    $cmn->insertNotification($details->customer_id,$details->customer_name,$Collections->finance_user_id,$message,$page_id);
                }
                return 1;
            }
                // }
        }

    }}

    public function doerStatusUpdate($Collections){

        $value = array(
            "doer_status" => 1,
            "doer_id"     => $Collections->doer_id,
            "doer_name"   => $Collections->doer_name,
            "doer_date"   => date("Y-m-d H:i:s"),
            "updated_at"  => date("Y-m-d H:i:s")  

        );

        DB::table('gpff_collection')
        ->where('collection_id', $Collections->collection_id)
        ->where('doer_status', 0)
        ->update($value);

        return 1;
    }

    public function doerCollectionDetails($Collections){
        return $results = DB::table('gpff_collection')
                        ->where('doer_id', $Collections->doer_id)
                        ->where('doer_status', 1)
                        ->get();
    }

    public function acceptCollectionDetail($Collections){
        $data = DB::table('gpff_collection')
        ->where('collection_id', $Collections->collection_id)
        ->First();

        $users = DB::table('gpff_users')
                ->where('user_id', $data->finance_user_id)
                ->First();

        $value = array(
            'collect_approve_id'=>$data->finance_user_id,
            'collect_approve_name'=> $users->firstname." ".$users->lastname,
            'approve_status'=> 1,
            'collect_approve_date'=>date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        );

        $update = DB::table('gpff_collection')
        ->where('collection_id', $Collections->collection_id)
        ->where('doer_status', 1)
        ->where('check_status', 1)
        ->update($value);
        if($data->collection_for == 1){

            $balance = DB::table('gpff_order')
            ->where('order_id', $data->order_id)
            ->First();

            if($balance->or_blc_price == 0){
                $values = array(
                    'payment_status' =>1,
                    'updated_at'=>date('Y-m-d H:i:s')
                );
            }else{
                $values = array(
                    'payment_status' =>2,
                    'updated_at'=>date('Y-m-d H:i:s')
                );
            }

            $updated = DB::table('gpff_order')        
            ->where('order_id', $data->order_id)
            ->update($values);
        }else{
            $balance = DB::table('gpff_vendor_order')
            ->where('vendor_order_id', $data->order_id)
            ->First();

            if($balance->or_blc_price == 0){
                $values = array(
                    'payment_status' =>1,
                    'updated_at'=>date('Y-m-d H:i:s')
                );
            }else{
                $values = array(
                    'payment_status' =>2,
                    'updated_at'=>date('Y-m-d H:i:s')
                );
            }

            $updated = DB::table('gpff_vendor_order')        
            ->where('vendor_order_id', $data->order_id)
            ->update($values);
        }

        return 1;
    }

    public function checkerStatusUpdate($Collections){

       $value = array(
        "checker_id" => $Collections->checker_id,
        "checker_name" =>$Collections->checker_name,
        "checker_update_date" =>date('Y-m-d H:i:s'),
        "check_status" => 1,
        "finance_user_id"=>$Collections->finance_user_id,
        "updated_at"=>date('Y-m-d H:i:s')
    );
       DB::table('gpff_collection')
       ->where('collection_id', $Collections->collection_id)
       ->where('doer_status', 1)
       ->update($value);

       $datas = DB::table('gpff_collection')
       ->where('collection_id', $Collections->collection_id)
       ->First();
             // print_r($datas);
             // print_r($datas->order_id);
             // exit();
       if($datas->collection_for == 2){
           $values = array(
            'payment_status'=>3,
            'updated_at'=>date('Y-m-d H:i:s')
        );

           DB::table('gpff_vendor_order')
           ->where('vendor_order_id', $datas->order_id)
           ->update($values);
       }
       return 1;
   }

   public function checkerStateReject($Collections){

     $value = array(
        "checker_id" => $Collections->checker_id,
        "checker_name" =>$Collections->checker_name,
        "checker_update_date" =>date('Y-m-d H:i:s'),
        "check_status" => 2,
        "updated_at"=>date('Y-m-d H:i:s')
    );
     DB::table('gpff_collection')
     ->where('collection_id', $Collections->collection_id)
     ->update($value);

    //    $datas = DB::table('gpff_collection')
    //    ->where('collection_id', $Collections->collection_id)
    //    ->First();
    //          // print_r($datas);
    //          // print_r($datas->order_id);
    //          // exit();

    //    $values = array(
    //     'payment_status'=>3,
    //     'updated_at'=>date('Y-m-d H:i:s')
    // );

    //    DB::table('gpff_order')
    //    ->where('order_id', $datas->order_id)
    //    ->update($values);

     return 1;
 }

 public function regionBasedCollectionDetails($Collections){

    if($Collections->collection_for == 1){

        $results = DB::table('gpff_collection as gpcol')
        ->join('gpff_order as gpor', 'gpcol.order_id', 'gpor.order_id')
        ->whereIn('gpcol.region_id', $Collections->region_id)
        ->where('gpcol.collection_for', 1)
        ->where('gpcol.doer_status', 1)
        ->where('gpcol.check_status', 0)
        ->orderBy('gpcol.updated_at', 'DESC');
        if($Collections->checker_id !=''){
            $results = $results->whereIn('gpcol.checker_id', $Collections->checker_id);            
        }
        $results = $results->get(['gpcol.collection_id','gpcol.customer_id','gpcol.customer_name','gpcol.invoice_no','gpcol.order_id','gpcol.finance_user_id','gpcol.col_type','gpcol.collection_date','gpcol.collection_for','gpcol.collection_status','gpcol.collection_mode','gpcol.collection_amount','gpcol.area_id','gpcol.region_id', 'gpcol.doer_id', 'gpcol.doer_name','gpcol.doer_date', 'gpcol.doer_status','gpcol.bank_name','gpcol.card_type','gpcol.card_no','gpcol.holder_name','gpcol.account_no','gpcol.transaction_id','gpcol.cheque_no','gpcol.collection_cr_id','gpcol.collection_cr_name','gpcol.collection_cr_date','gpcol.approve_status','gpcol.created_at','gpcol.updated_at','gpor.or_blc_price','gpor.payment_status','gpor.or_tot_price']);
    }else{

        $results = DB::table('gpff_collection as gpcol')
        ->join('gpff_vendor_order as gpvor', 'gpcol.order_id', 'gpvor.vendor_order_id')
        ->whereIn('gpcol.region_id', $Collections->region_id)
        ->where('gpcol.collection_for', 2)
        ->where('gpcol.doer_status', 1)
        ->where('gpcol.check_status', 0)
        ->orderBy('gpcol.updated_at', 'DESC');
        if($Collections->checker_id !=''){
            $results = $results->whereIn('gpcol.checker_id', $Collections->checker_id);            
        }
        $results = $results->get(['gpcol.collection_id','gpcol.customer_id','gpcol.customer_name','gpcol.invoice_no','gpcol.order_id','gpcol.finance_user_id','gpcol.col_type','gpcol.collection_date','gpcol.collection_for','gpcol.collection_status','gpcol.collection_mode','gpcol.collection_amount','gpcol.area_id','gpcol.region_id', 'gpcol.doer_id', 'gpcol.doer_name','gpcol.doer_date', 'gpcol.doer_status','gpcol.bank_name','gpcol.card_type','gpcol.card_no','gpcol.holder_name','gpcol.account_no','gpcol.transaction_id','gpcol.cheque_no','gpcol.collection_cr_id','gpcol.collection_cr_name','gpcol.collection_cr_date','gpcol.approve_status','gpcol.created_at','gpcol.updated_at','gpvor.or_blc_price','gpvor.payment_status','gpvor.or_tot_price']);
    }
    return $results;
}

    public function fetchFinanceUserBasedCollections($Collections)
    {
        $results = DB::table('gpff_collection')
        ->where('finance_user_id', $Collections->finance_user_id)
        ->where('doer_status', 1)
        ->where('check_status', 1)
        ->whereIn('approve_status', [0,2])
        ->where('collection_for', $Collections->collection_for)
        ->orderBy('updated_at','DESC');
        // ->get();
        if($Collections->start_date != ''){
            $results = $results->whereBetween(DB::RAW('date(checker_update_date)'), [date($Collections->start_date), date($Collections->end_date)]);
        }
        if($Collections->customer_id != ''){
            $results = $results->whereIn('customer_id',$Collections->customer_id);
        }
        if($Collections->order_id != ''){
            $results = $results->where('order_id',$Collections->order_id);
        }
        if($Collections->checker_id != ''){
            $results = $results->whereIn('checker_id',$Collections->checker_id);
        }
        return $results->get();
    }

    public function updateCollectionUser($Collections){
        $values = array(
            'finance_user_id' => $Collections->finance_user_id,
            //'approve_status' => 1,
            'updated_at'      => date('Y-m-d H:i:s')
        );

        DB::table('gpff_collection')
        ->where('collection_id', $Collections->collection_id)
        ->update($values);

        return 1;
    }

    public function fetchCollectionDateBased($Collections){
        $results = DB::table('gpff_collection')
        ->where('collection_date', $Collections->collection_date)
        ->paginate(10);
        return $results;
    }

    public function fetchInvoice_noBasedDetails($Collections){
        $results = DB::table('gpff_collection as gpcol')
        ->join('gpff_order as gpor', 'gpcol.order_id', 'gpor.order_id')
        ->where('gpcol.invoice_no', $Collections->invoice_no)
        ->get(['gpcol.collection_id','gpcol.finance_user_id','gpcol.invoice_no','gpcol.order_id','gpcol.customer_name','gpcol.col_type','gpcol.collection_date','gpcol.collection_for','gpcol.collection_status','gpcol.collection_mode','gpcol.collection_amount','gpcol.collection_cr_id','gpcol.status','gpcol.bank_name','gpcol.card_type','gpcol.card_no','gpcol.holder_name','gpcol.account_no','gpcol.transaction_id','gpcol.cheque_no','gpcol.collection_cr_id','gpcol.collection_cr_name','gpcol.collection_cr_date','gpcol.approve_status','gpcol.created_at','gpcol.updated_at','gpor.or_blc_price','gpor.payment_status']);
                // ->paginate(10);
        return $results;
    }

    public function fetchCheckedCollectionDetails($Collections){

        // $results = DB::table('gpff_collection')
        //         ->where('checker_id', $Collections->checker_id)
        //         ->where('check_status', 1)
        //         ->get();

        // return $results;

        if($Collections->collection_for == 1){

            $results = DB::table('gpff_collection as gpcol')
            ->join('gpff_order as gpor' , 'gpor.order_id', 'gpcol.order_id')
            ->where('gpcol.collection_for', 1)
            ->where('gpcol.checker_id', $Collections->checker_id)
            // ->get(['gpcol.collection_id','gpcol.finance_user_id','gpcol.invoice_no','gpcol.order_id','gpcol.customer_name','gpcol.col_type','gpcol.collection_date','gpcol.collection_for','gpcol.collection_status','gpcol.collection_mode','gpcol.collection_amount','gpcol.collection_cr_id','gpcol.status','gpcol.bank_name','gpcol.card_type','gpcol.card_no','gpcol.holder_name','gpcol.account_no','gpcol.transaction_id','gpcol.cheque_no','gpcol.collection_cr_id','gpcol.collection_cr_name','gpcol.collection_cr_date','gpcol.approve_status','gpcol.created_at','gpcol.updated_at','gpor.or_blc_price','gpor.payment_status','gpor.or_tot_price']);
        ->get(['gpcol.collection_id','gpcol.customer_id','gpcol.customer_name','gpcol.invoice_no','gpcol.order_id','gpcol.finance_user_id','gpcol.col_type','gpcol.collection_date','gpcol.collection_for','gpcol.collection_status','gpcol.collection_mode','gpcol.collection_amount','gpcol.area_id','gpcol.region_id', 'gpcol.doer_id', 'gpcol.doer_name','gpcol.doer_date', 'gpcol.doer_status','gpcol.bank_name','gpcol.card_type','gpcol.card_no','gpcol.holder_name','gpcol.account_no','gpcol.transaction_id','gpcol.cheque_no','gpcol.collection_cr_id','gpcol.collection_cr_name','gpcol.collection_cr_date','gpcol.approve_status','gpcol.created_at','gpcol.updated_at','gpor.or_blc_price','gpor.payment_status','gpor.or_tot_price']);
        }else{

            $results = DB::table('gpff_collection as gpcol')
            ->join('gpff_vendor_order as gvpor' , 'gvpor.vendor_order_id', 'gpcol.order_id')
            ->where('gpcol.collection_for', 2)
            ->where('gpcol.checker_id', $Collections->checker_id)
            // ->get(['gpcol.collection_id','gpcol.finance_user_id','gpcol.invoice_no','gpcol.order_id','gpcol.customer_name','gpcol.col_type','gpcol.collection_date','gpcol.collection_for','gpcol.collection_status','gpcol.collection_mode','gpcol.collection_amount','gpcol.collection_cr_id','gpcol.status','gpcol.bank_name','gpcol.card_type','gpcol.card_no','gpcol.holder_name','gpcol.account_no','gpcol.transaction_id','gpcol.cheque_no','gpcol.collection_cr_id','gpcol.collection_cr_name','gpcol.collection_cr_date','gpcol.approve_status','gpcol.created_at','gpcol.updated_at','gvpor.or_blc_price','gvpor.payment_status','gvpor.or_tot_price']);
        ->get(['gpcol.collection_id','gpcol.customer_id','gpcol.customer_name','gpcol.invoice_no','gpcol.order_id','gpcol.finance_user_id','gpcol.col_type','gpcol.collection_date','gpcol.collection_for','gpcol.collection_status','gpcol.collection_mode','gpcol.collection_amount','gpcol.area_id','gpcol.region_id', 'gpcol.doer_id', 'gpcol.doer_name','gpcol.doer_date', 'gpcol.doer_status','gpcol.bank_name','gpcol.card_type','gpcol.card_no','gpcol.holder_name','gpcol.account_no','gpcol.transaction_id','gpcol.cheque_no','gpcol.collection_cr_id','gpcol.collection_cr_name','gpcol.collection_cr_date','gpcol.approve_status','gpcol.created_at','gpcol.updated_at','gvpor.or_blc_price','gvpor.payment_status','gvpor.or_tot_price']);
        }
        return $results;
    }



    public function addSalereturn($Collections){

        if($Collections->or_return_type == 1){
            $ord = DB::table('gpff_order')
            ->where('order_id', $Collections->order_id)
            ->First(['invoice_no', 'warehouse_id']);
            $return_value = array(

                'or_gross_total'        => $Collections->or_gross_total,
                'finance_user_id'       => $Collections->finance_user_id,
                'invoice_no'            => $ord->invoice_no,
                'order_id'              => $Collections->order_id,
                'return_grand_total'    => $Collections->return_grand_total,
                'customer_id'           => $Collections->customer_id,
                'customer_name'         => $Collections->customer_name,
                'order_return_date'     => date('Y-m-d H:i:s'),
                'return_reason'         => $Collections->return_reason,
                'or_tot_price'          => $Collections->or_tot_price,
                'branch_id'             => $Collections->branch_id,
                'region_id'             => $Collections->region_id,
                'area_id'               => $Collections->area_id,
                'stockist_id'           => $Collections->stockist_id,
                'warehouse_id'          => $ord->warehouse_id,
                'payment_type'          => $Collections->payment_type,
                'or_return_type'        => $Collections->or_return_type,
                'payment_status'        => $Collections->payment_status,
                'payment_name'          => $Collections->payment_name,
                'created_at'            => date('Y-m-d H:i:s'), 
                'updated_at'            => date('Y-m-d H:i:s')
            );
        // print_r($return_value);

            $return_id = DB::table('gpff_sale_return')
            ->insertGetId($return_value);

            $pdf_name = "GPFF_RETURN_".$return_id;
            $file_name = $pdf_name.'.pdf';

            $value = array(
                'return_bill'       => $file_name ,
                'return_bill_type'      => 1,
                'updated_at'        => date('Y-m-d H:i:s')
            );
        // print_r($value);

            DB::table('gpff_sale_return')
            ->where('sale_return_id',$return_id)
            ->update($value);

            $or_re_details=  DB::table('gpff_order')
            ->where('order_id', $Collections->order_id)
            ->First();

            $total = $or_re_details->or_blc_price-$Collections->return_grand_total;
            // print_r("expression".$or_re_details->or_blc_price);
            // print_r($Collections->return_grand_total);

            if($total<=0){

                $bal_am = array(
                    'or_blc_price' => 0,
                    'updated_at'  => date('Y-m-d H:i:s')
                );

                $balance_details=  DB::table('gpff_order')
                ->where('order_id', $Collections->order_id)
                ->update($bal_am);

            }else{

                $bal_am = array(
                    'or_blc_price' => $total,
                    'updated_at'  => date('Y-m-d H:i:s')
                );

                $balance_details=  DB::table('gpff_order')
                ->where('order_id', $Collections->order_id)
                ->update($bal_am);
            }

            foreach($Collections->return_list as $return_lists){
                // $exit = DB::table("gpff_sale_return_list")
                // ->where('category_id', $return_lists['category_id'])
                // ->where('product_id', $return_lists['product_id'])
                //     // ->where('order_id', $Collections->order_id)
                // ->where('invoice_no', $Collections->invoice_no)
                // ->where('sale_return_id', $return_id)
                // ->First();
                // if($exit){

                //     $qty = $exit->return_qty + $return_lists['return_qty'];
                //     $totprice = $exit->product_return_total_price + $return_lists['product_return_total_price']; 

                //     $value = array(
                //         'return_qty'        => $qty ,
                //         'return_grand_total'  => $totprice ,
                //         'updated_at'        => date('Y-m-d H:i:s')
                //     );

                //     DB::table('gpff_sale_return_list')
                //     ->where('category_id', $return_lists['category_id'])
                //     ->where('product_id', $return_lists['product_id'])
                //     ->where('order_id', $Collections->order_id)
                //     ->where('sale_return_id', $return_id)
                //     ->update($value);

                //     $return_list_value = array(
                //         'order_id'              => $Collections->order_id,
                //         'sale_return_list_id'   => $exit->sale_return_list_id,
                //         'sale_return_id'        => $return_id,
                //         'category_id'           => $return_lists['category_id'],
                //         'product_id'            => $return_lists['product_id'],
                //         'batch_no'              => $return_lists['batch_no'],
                //         'batch_emp_date'        => $return_lists['batch_emp_date'],    
                //         'product_qty'           => $return_lists['product_qty'],
                //         'product_netprice'      => $return_lists['product_netprice'],
                //         'product_grossprice'    => $return_lists['product_grossprice'],
                //         'product_discount'      => $return_lists['product_discount'],
                //         'product_tot_price'     => $return_lists['product_tot_price'],
                //         'product_type'          => $return_lists['product_type'],
                //         'scheme'                => $return_lists['scheme'],
                //         'return_qty'            => $return_lists['return_qty'],
                //         'return_FOC'            => $return_lists['return_FOC'],
                //         'product_return_total_price'=> $return_lists['product_return_total_price'],
                //         'comments'              => $return_lists['comments'],
                //         'FOC'                   => $return_lists['FOC'],
                //         'FOC_amt'               => $return_lists['FOC_amt'],
                //         'spl_FOC'               => $return_lists['spl_FOC'],
                //         'pro_amt_cal'           => $return_lists['pro_amt_cal'],
                //         'created_at'            => date('Y-m-d H:i:s') ,
                //         'updated_at'            => date('Y-m-d H:i:s')
                //     );

                //     DB::table("gpff_sale_return_batchwise_list")
                //     ->insert($return_list_value);

                // }else{

                $list_value = array(
                    'order_id'              => $Collections->order_id,
                    'sale_return_id'        => $return_id,
                    'invoice_no'            => $ord->invoice_no,
                    'category_id'           => $return_lists['category_id'],
                    'category_name'         => $return_lists['category_name'],
                    'product_id'            => $return_lists['product_id'],
                    'product_genericname'   => $return_lists['product_genericname'],
                    'product_qty'           => $return_lists['product_qty'],
                    'product_tot_price'     => $return_lists['product_tot_price'],
                    'product_type'          => $return_lists['product_type'],
                    'return_qty'            => $return_lists['return_qty'],
                    'product_return_total_price'=> $return_lists['product_return_total_price'],
                    'created_at'            => date('Y-m-d H:i:s') ,
                    'updated_at'            => date('Y-m-d H:i:s')
                );
                $return_list_id = DB::table('gpff_sale_return_list')
                ->insertGetId($list_value);

                $return_list_values = array(
                    'order_id'              => $Collections->order_id,
                    'sale_return_list_id'   => $return_list_id,
                    'sale_return_id'        => $return_id,
                    'invoice_no'            => $Collections->invoice_no,
                    'category_id'           => $return_lists['category_id'],
                    'product_id'            => $return_lists['product_id'],
                    'batch_no'              => $return_lists['batch_no'],
                    'batch_emp_date'        => $return_lists['batch_emp_date'],    
                    'product_qty'           => $return_lists['product_qty'],
                    'product_netprice'      => $return_lists['product_netprice'],
                    'product_grossprice'    => $return_lists['product_grossprice'],
                    'product_discount'      => $return_lists['product_discount'],
                    'product_tot_price'     => $return_lists['product_tot_price'],
                    'product_type'          => $return_lists['product_type'],
                    'scheme'                => $return_lists['scheme'],
                    'return_qty'            => $return_lists['return_qty'],
                    'return_FOC'            => $return_lists['return_FOC'],
                    'product_return_total_price'=> $return_lists['product_return_total_price'],
                    'comments'              => $return_lists['comments'],
                    'FOC'                   => $return_lists['FOC'],
                    'FOC_amt'               => $return_lists['FOC_amt'],
                    // 'spl_FOC'               => $return_lists['spl_FOC'],
                    // 'pro_amt_cal'           => $return_lists['pro_amt_cal'],
                    'created_at'            => date('Y-m-d H:i:s') ,
                    'updated_at'            => date('Y-m-d H:i:s')
                );

                DB::table("gpff_sale_return_batchwise_list")
                ->insert($return_list_values);

                $blc_qty  =  DB::table('gpff_product_batch_stock')
                            ->where('product_id', $return_lists['product_id'])
                            ->where('batch_id', $return_lists['batch_no'])
                            ->where('warehouse_id', $ord->warehouse_id)
                            ->sum('product_blc_qty');
                // print_r($blc_qty);

                $sal_qty  =  DB::table('gpff_product_batch_stock')
                            ->where('product_id', $return_lists['product_id'])
                            ->where('batch_id', $return_lists['batch_no'])
                            ->where('warehouse_id', $ord->warehouse_id)
                            ->sum('product_sales_qty');

                $foc_qty  =  DB::table('gpff_product_batch_stock')
                            ->where('product_id', $return_lists['product_id'])
                            ->where('batch_id', $return_lists['batch_no'])
                            ->where('warehouse_id', $ord->warehouse_id)
                            ->sum('product_sale_FOC_qty');

                $qty = $return_lists['return_qty'];

                $sales  = $sal_qty - $qty;
                $blc    = $blc_qty + $qty;
                $foc    = $foc_qty;

                // if($return_lists['return_FOC'] != 0 && $return_lists['spl_FOC'] != 0){

                //     $sales = $sales - $return_lists['return_FOC'] - $return_lists['spl_FOC'];
                //     $blc = $blc + $return_lists['return_FOC'] + $return_lists['spl_FOC'];

                // }else
                // if($return_lists['spl_FOC'] != 0){

                //     $sales = $sales + $ord_value->spl_FOC;
                //     $blc = $blc - $ord_value->spl_FOC;

                // }else
                if ($return_lists['return_FOC'] != 0){

                    //$sales = $sales - $return_lists['return_FOC'];
                    $blc = $blc + $return_lists['return_FOC'];
                    $foc = $foc_qty - $return_lists['return_FOC'];

                }

                $value = array(
                    'product_blc_qty'       => $blc ,
                    'product_sales_qty'     => $sales ,
                    'product_sale_FOC_qty'  => $foc,
                    'product_status'        => 1,
                    'updated_at'            => date('Y-m-d H:i:s')
                );

                DB::table('gpff_product_batch_stock')
                ->where('product_id', $return_lists['product_id'])
                ->where('batch_id', $return_lists['batch_no'])
                ->where('warehouse_id', $ord->warehouse_id)
                ->update($value);
            }

            $customer_details = DB::table('gpff_customer')
            ->where('vendor', 2)
            ->where('customer_id',$Collections->customer_id)
            ->First();

    // $Collections->invoice_no;
            $return_details = DB::table('gpff_sale_return')
            ->where('sale_return_id', $return_id)
            ->First();

            $order_detail = DB::table('gpff_order as gpor')
            ->join('gpff_order_list as gporl', 'gporl.order_id', 'gpor.order_id')
            ->where('gpor.order_id', $Collections->order_id)
            ->First();

    // $Collections->branch_id;
        // $number = $this->numbertostring($return_details->product_tot_price);

            $com_details = DB::table('gpff_branch')
            ->where('branch_id',$Collections->branch_id)
            ->First();

            view()->share('datas',$Collections->return_list);
            view()->share('return_details',$return_details);
            view()->share('order_detail',$order_detail);
            view()->share('customer_details',$customer_details);
            view()->share('invoice',$Collections->invoice_no);
            view()->share('bill_no', $pdf_name);
            view()->share('com_details',$com_details);
            view()->share('date',Carbon::now());

            $pdf = \PDF::loadView('return_bill')
            ->setPaper('a4', 'landscape');
            // echo "string";
            $pdf->save(public_path('Return_invoice/'.$pdf_name.'.pdf'));
            $file_name = $pdf_name.'.pdf';
            $name = $file_name;
            $filePath = 'Return_invoice/'.$name; 

            // Storage::disk('s3')->put($filePath, file_get_contents(public_path('Return_invoice')."/".$file_name));
            
            return 1;
        }else{
            $ord = DB::table('gpff_vendor_order')
            ->where('vendor_order_id', $Collections->order_id)
            ->First(['invoice_no', 'warehouse_id']);
            $return_value = array(

                'or_gross_total'        => $Collections->or_gross_total,
                'finance_user_id'       => $Collections->finance_user_id,
                'invoice_no'            => $ord->invoice_no,
                'order_id'              => $Collections->order_id,
                'return_grand_total'    => $Collections->return_grand_total,
                'customer_id'           => $Collections->customer_id,
                'customer_name'         => $Collections->customer_name,
                'order_return_date'     => date('Y-m-d H:i:s'),
                'return_reason'         => $Collections->return_reason,
                'or_tot_price'          => $Collections->or_tot_price,
                'branch_id'             => $Collections->branch_id,
                'region_id'             => $Collections->region_id,
                'area_id'               => $Collections->area_id,
                'stockist_id'            => $Collections->stockist_id,
                'warehouse_id'          => $ord->warehouse_id,
                'payment_type'          => $Collections->payment_type,
                'or_return_type'        => $Collections->or_return_type,
                'payment_status'        => $Collections->payment_status,
                'payment_name'          => $Collections->payment_name,
                'created_at'            => date('Y-m-d H:i:s'), 
                'updated_at'            => date('Y-m-d H:i:s')
            );
        // print_r($return_value);

            $return_id = DB::table('gpff_sale_return')
            ->insertGetId($return_value);

            $pdf_name = "GPFF_RETURN_".$return_id;
            $file_name = $pdf_name.'.pdf';

            $value = array(
                'return_bill'       => $file_name ,
                'return_bill_type'  => 1,
                'updated_at'        => date('Y-m-d H:i:s')
            );
        // print_r($value);

            DB::table('gpff_sale_return')
            ->where('sale_return_id',$return_id)
            ->update($value);

            $or_re_details=  DB::table('gpff_vendor_order')
            ->where('vendor_order_id', $Collections->order_id)
            ->First();

            $total = $or_re_details->or_blc_price - $Collections->return_grand_total;
            if($total<=0){

                $bal_am = array(
                    'or_blc_price' => 0,
                    'updated_at'  => date('Y-m-d H:i:s')
                );

                $balance_details=  DB::table('gpff_vendor_order')
                ->where('vendor_order_id', $Collections->order_id)
                ->update($bal_am);
            }else{
                $bal_am = array(
                    'or_blc_price' => $total,
                    'updated_at'  => date('Y-m-d H:i:s')
                );

                $balance_details=  DB::table('gpff_vendor_order')
                ->where('vendor_order_id', $Collections->order_id)
                ->update($bal_am);
            }

            foreach($Collections->return_list as $return_lists){
                // $exit = DB::table("gpff_sale_return_list")
                // ->where('category_id', $return_lists['category_id'])
                // ->where('product_id', $return_lists['product_id'])
                //     // ->where('order_id', $Collections->order_id)
                // ->where('invoice_no', $Collections->invoice_no)
                // ->where('sale_return_id', $return_id)
                // ->First();
                // if($exit){

                //     $qty = $exit->return_qty + $return_lists['return_qty'];
                //     $totprice = $exit->product_return_total_price + $return_lists['product_return_total_price']; 

                //     $value = array(
                //         'return_qty'        => $qty ,
                //         'return_grand_total'  => $totprice ,
                //         'updated_at'        => date('Y-m-d H:i:s')
                //     );

                //     DB::table('gpff_sale_return_list')
                //     ->where('category_id', $return_lists['category_id'])
                //     ->where('product_id', $return_lists['product_id'])
                //     ->where('order_id', $Collections->order_id)
                //     ->where('sale_return_id', $return_id)
                //     ->update($value);

                    // $return_list_value = array(
                    //     'order_id'              => $Collections->order_id,
                    //     'sale_return_list_id'   => $exit->sale_return_list_id,
                    //     'sale_return_id'        => $return_id,
                    //     'category_id'           => $return_lists['category_id'],
                    //     'product_id'            => $return_lists['product_id'],
                    //     'batch_no'              => $return_lists['batch_no'],
                    //     'batch_emp_date'        => $return_lists['batch_emp_date'],    
                    //     'product_qty'           => $return_lists['product_qty'],
                    //     'product_netprice'      => $return_lists['product_netprice'],
                    //     'product_grossprice'    => $return_lists['product_grossprice'],
                    //     'product_discount'      => $return_lists['product_discount'],
                    //     'product_tot_price'     => $return_lists['product_tot_price'],
                    //     'product_type'          => $return_lists['product_type'],
                    //     'scheme'                => $return_lists['scheme'],
                    //     'return_qty'            => $return_lists['return_qty'],
                    //     'return_FOC'            => $return_lists['return_FOC'],
                    //     'product_return_total_price'=> $return_lists['product_return_total_price'],
                    //     'comments'              => $return_lists['comments'],
                    //     'FOC'                   => $return_lists['FOC'],
                    //     'FOC_amt'               => $return_lists['FOC_amt'],
                    //     'spl_FOC'               => $return_lists['spl_FOC'],
                    //     'pro_amt_cal'           => $return_lists['pro_amt_cal'],
                    //     'created_at'            => date('Y-m-d H:i:s') ,
                    //     'updated_at'            => date('Y-m-d H:i:s')
                    // );

                    // DB::table("gpff_sale_return_batchwise_list")
                    // ->insert($return_list_value);

                // }else{

                    $list_value = array(
                        'order_id'              => $Collections->order_id,
                        'sale_return_id'        => $return_id,
                        'invoice_no'            => $ord->invoice_no,
                        'category_id'           => $return_lists['category_id'],
                        'category_name'         => $return_lists['category_name'],
                        'product_id'            => $return_lists['product_id'],
                        'product_genericname'   => $return_lists['product_genericname'],
                        'product_qty'           => $return_lists['product_qty'],
                        'product_tot_price'     => $return_lists['product_tot_price'],
                        'product_type'          => $return_lists['product_type'],
                        'return_qty'            => $return_lists['return_qty'],
                        'product_return_total_price'=> $return_lists['product_return_total_price'],
                        'created_at'            => date('Y-m-d H:i:s') ,
                        'updated_at'            => date('Y-m-d H:i:s')
                    );
                    $return_list_id = DB::table('gpff_sale_return_list')
                    ->insertGetId($list_value);

                    $return_list_values = array(
                        'order_id'              => $Collections->order_id,
                        'sale_return_list_id'   => $return_list_id,
                        'sale_return_id'        => $return_id,
                        'invoice_no'            => $Collections->invoice_no,
                        'category_id'           => $return_lists['category_id'],
                        'product_id'            => $return_lists['product_id'],
                        'batch_no'              => $return_lists['batch_no'],
                        'batch_emp_date'        => $return_lists['batch_emp_date'],    
                        'product_qty'           => $return_lists['product_qty'],
                        'product_netprice'      => $return_lists['product_netprice'],
                        'product_grossprice'    => $return_lists['product_grossprice'],
                        'product_discount'      => $return_lists['product_discount'],
                        'product_tot_price'     => $return_lists['product_tot_price'],
                        'product_type'          => $return_lists['product_type'],
                        'scheme'                => $return_lists['scheme'],
                        'return_qty'            => $return_lists['return_qty'],
                        'return_FOC'            => $return_lists['return_FOC'],
                        'product_return_total_price'=> $return_lists['product_return_total_price'],
                        'comments'              => $return_lists['comments'],
                        'FOC'                   => $return_lists['FOC'],
                        'FOC_amt'               => $return_lists['FOC_amt'],
                        // 'spl_FOC'               => $return_lists['spl_FOC'],
                        // 'pro_amt_cal'           => $return_lists['pro_amt_cal'],
                        'created_at'            => date('Y-m-d H:i:s') ,
                        'updated_at'            => date('Y-m-d H:i:s')
                    );

                    DB::table("gpff_sale_return_batchwise_list")
                    ->insert($return_list_values);
                
                $blc_qty  =  DB::table('gpff_product_batch_stock')
                            ->where('product_id', $return_lists['product_id'])
                            ->where('batch_id', $return_lists['batch_no'])
                            ->where('warehouse_id', $ord->warehouse_id)
                            ->sum('product_blc_qty');
                // print_r($blc_qty);

                $sal_qty  =  DB::table('gpff_product_batch_stock')
                            ->where('product_id', $return_lists['product_id'])
                            ->where('batch_id', $return_lists['batch_no'])
                            ->where('warehouse_id', $ord->warehouse_id)
                            ->sum('product_sales_qty');

                $foc_qty  =  DB::table('gpff_product_batch_stock')
                            ->where('product_id', $return_lists['product_id'])
                            ->where('batch_id', $return_lists['batch_no'])
                            ->where('warehouse_id', $ord->warehouse_id)
                            ->sum('product_sale_FOC_qty');

                $qty = $return_lists['return_qty'];

                $sales  = $sal_qty - $qty;
                $blc    = $blc_qty + $qty;
                $foc    = $foc_qty;

                // if($return_lists['return_FOC'] != 0 && $return_lists['spl_FOC'] != 0){

                //     $sales = $sales - $return_lists['return_FOC'] - $return_lists['spl_FOC'];
                //     $blc = $blc + $return_lists['return_FOC'] + $return_lists['spl_FOC'];

                // }else
                // if($return_lists['spl_FOC'] != 0){

                //     $sales = $sales + $ord_value->spl_FOC;
                //     $blc = $blc - $ord_value->spl_FOC;

                // }else
                if ($return_lists['return_FOC'] != 0){

                    //$sales = $sales - $return_lists['return_FOC'];
                    $blc = $blc + $return_lists['return_FOC'];
                    $foc = $foc_qty - $return_lists['return_FOC'];

                }

                $value = array(
                    'product_blc_qty'       => $blc ,
                    'product_sales_qty'     => $sales ,
                    'product_status'        => 1,
                    'product_sale_FOC_qty'  => $foc,
                    'updated_at'            => date('Y-m-d H:i:s')
                );

                DB::table('gpff_product_batch_stock')
                ->where('product_id', $return_lists['product_id'])
                ->where('batch_id', $return_lists['batch_no'])
                ->where('warehouse_id', $ord->warehouse_id)
                ->update($value);
            }
            $customer_details = DB::table('gpff_customer')
            ->where('vendor', 1)
            ->where('customer_id',$Collections->customer_id)
            ->First();

    // $Collections->invoice_no;
            $return_details = DB::table('gpff_sale_return')
            ->where('sale_return_id', $return_id)
            ->First();

            $order_detail = DB::table('gpff_vendor_order as gpvor')
            ->join('gpff_vendor_order_list as gpvorl', 'gpvorl.vendor_order_id', 'gpvor.vendor_order_id')
            ->where('gpvor.vendor_order_id', $Collections->order_id)
            ->First();

    // $Collections->branch_id;
        // $number = $this->numbertostring($return_details->product_tot_price);

            $com_details = DB::table('gpff_branch')
            ->where('branch_id',$Collections->branch_id)
            ->First();

            view()->share('datas',$Collections->return_list);
            view()->share('return_details',$return_details);
            view()->share('order_detail',$order_detail);
            view()->share('customer_details',$customer_details);
            view()->share('invoice',$Collections->invoice_no);
            view()->share('bill_no', $pdf_name);
            view()->share('com_details',$com_details);
            view()->share('date',Carbon::now());

            $pdf = \PDF::loadView('return_bill')
            ->setPaper('a4', 'landscape');
            // echo "string";
            $pdf->save(public_path('Return_invoice/'.$pdf_name.'.pdf'));
            $file_name = $pdf_name.'.pdf';
            $name = $file_name;
            $filePath = 'Return_invoice/'.$name; 

            // Storage::disk('s3')->put($filePath, file_get_contents(public_path('Return_invoice')."/".$file_name));
            return 1;
        }
    }

    public function saleReturnIdBasedDetails($Collections){

        $values = DB::table('gpff_sale_return')
        ->where('sale_return_id', $Collections->sale_return_id)
        ->First(['customer_id', 'invoice_no', 'customer_name']);

        $customer = DB::table('gpff_customer')
        ->where('customer_id', $values->customer_id)
        ->get();

        $product = DB::table('gpff_sale_return_batchwise_list as gpsrbl')
        ->join('gpff_sale_return_list as gpsretl', 'gpsrbl.sale_return_list_id','gpsretl.sale_return_list_id')
        ->where('gpsrbl.sale_return_id', $Collections->sale_return_id)
        ->orderBy('gpsrbl.sale_return_id', 'ASC')
        ->get(['gpsrbl.order_id','gpsrbl.sale_return_list_id','gpsrbl.category_id','gpsretl.category_name','gpsrbl.product_id','gpsretl.product_genericname','gpsrbl.batch_no','gpsrbl.batch_emp_date','gpsrbl.product_qty','gpsrbl.product_netprice','gpsrbl.product_grossprice','gpsrbl.or_gross_total','gpsrbl.product_discount','gpsrbl.product_tot_price','gpsrbl.product_type','gpsrbl.scheme','gpsrbl.comments','gpsrbl.FOC','gpsrbl.FOC_amt','gpsrbl.spl_FOC','gpsrbl.pro_amt_cal','gpsrbl.created_at','gpsrbl.updated_at']);

        $status = DB::table('gpff_sale_return')
        ->where('sale_return_id', $Collections->sale_return_id)
        ->get(['order_return_date','return_reason','or_return_type','or_gross_total','or_tot_price','invoice_no']);

        $results =array(
            'customer'  =>  $customer,
            'product'   =>  $product,
            'status'    =>  $status
        );

        return $results;
    }



    public function fetchVendorOrderList($Collections){
        // print_r($Collections->area_manager_id);
        if($Collections->type == 1){
            $results = DB::table('gpff_vendor_order')
            ->where('area_manager_id', $Collections->area_manager_id)
            ->where('payment_status',1)
            ->where('vendor_order_status', 3)
            ->orderBy('updated_at','DESC')
            ->get();
        }else{
            $results = DB::table('gpff_vendor_order')
            ->where('area_manager_id', $Collections->area_manager_id)
            ->whereIn('payment_status',[2,3])
            ->where('vendor_order_status', 3)
            ->orderBy('updated_at','DESC')
            ->get();
        }
        

        return $results;
    }

    public function fetchCollectionDetailsBasedId($Collections){

        if($Collections->type == 1){

            $results = DB::table('gpff_collection as gpcol')
            ->join('gpff_order as gpor' , 'gpor.order_id', 'gpcol.order_id')
            ->where('gpcol.collection_id', $Collections->collection_id)
            ->get(['gpcol.collection_id','gpcol.finance_user_id','gpcol.invoice_no','gpcol.order_id','gpcol.customer_name','gpcol.col_type','gpcol.collection_date','gpcol.collection_for','gpcol.collection_status','gpcol.collection_mode','gpcol.collection_amount','gpcol.collection_cr_id','gpcol.status','gpcol.bank_name','gpcol.card_type','gpcol.card_no','gpcol.holder_name','gpcol.account_no','gpcol.transaction_id','gpcol.cheque_no','gpcol.collection_cr_id','gpcol.collection_cr_name','gpcol.collection_cr_date','gpcol.approve_status','gpcol.created_at','gpcol.updated_at','gpor.or_blc_price','gpor.payment_status','gpor.or_tot_price']);
        }else{

            $results = DB::table('gpff_collection as gpcol')
            ->join('gpff_vendor_order as gvpor' , 'gvpor.vendor_order_id', 'gpcol.order_id')
            ->where('gpcol.collection_id', $Collections->collection_id)
            ->get(['gpcol.collection_id','gpcol.finance_user_id','gpcol.invoice_no','gpcol.order_id','gpcol.customer_name','gpcol.col_type','gpcol.collection_date','gpcol.collection_for','gpcol.collection_status','gpcol.collection_mode','gpcol.collection_amount','gpcol.collection_cr_id','gpcol.status','gpcol.bank_name','gpcol.card_type','gpcol.card_no','gpcol.holder_name','gpcol.account_no','gpcol.transaction_id','gpcol.cheque_no','gpcol.collection_cr_id','gpcol.collection_cr_name','gpcol.collection_cr_date','gpcol.approve_status','gpcol.created_at','gpcol.updated_at','gvpor.or_blc_price','gvpor.payment_status','gvpor.or_tot_price']);
        }
        return $results;
    }

    public function deleteCollectionDetails($Collections){
        $datas = DB::table('gpff_collection')
        ->where('collection_id', $Collections->collection_id)
        ->First(['order_id', 'collection_amount', 'collection_for']);

        if($datas->collection_for==1){
            $order_detail = DB::table('gpff_order')
            ->where('order_id', $datas->order_id)
            ->First();

            $total_price = $order_detail->or_blc_price + $datas->collection_amount;

            $values = array(
                "or_blc_price" => $total_price,
                "payment_status"=> 2,
                'updated_at'=>date('Y-m-d H:i:s')
            );

            DB::table('gpff_order')
            ->where('order_id', $datas->order_id)
            ->update($values);

        }else{
            $order_detail = DB::table('gpff_vendor_order')
            ->where('vendor_order_id', $datas->order_id)
            ->First();

            $total_price = $order_detail->or_blc_price + $datas->collection_amount;

            $values = array(
                "or_blc_price" => $total_price,
                "payment_status"=>2,
                'updated_at'=>date('Y-m-d H:i:s')
            );

            DB::table('gpff_vendor_order')
            ->where('vendor_order_id', $datas->order_id)
            ->update($values);
        }

        $results = DB::table('gpff_collection')
        ->where('collection_id', $Collections->collection_id)
        ->delete();

        return 1;
    }
    public function fetchCollectionDetailsOrderBase($Collections){
        if($Collections->type == 1){
            $results = DB::table('gpff_collection as gpcol')
            ->join('gpff_order as gpor', 'gpcol.order_id', 'gpor.order_id')
            ->join('gpff_users as gpuse', 'gpuse.user_id', 'gpcol.finance_user_id')
            ->where('gpcol.collection_for', 1)
            ->where('gpcol.order_id', $Collections->order_id)
            ->orderBy('gpcol.updated_at','DESC')
            ->get(['gpcol.collection_id','gpcol.finance_user_id','gpcol.invoice_no','gpcol.order_id','gpcol.customer_name','gpcol.col_type','gpcol.collection_date','gpcol.collection_for','gpcol.collection_status','gpcol.collection_mode','gpcol.collection_amount','gpcol.collection_cr_id','gpcol.status','gpcol.bank_name','gpcol.card_type','gpcol.card_no','gpcol.holder_name','gpcol.account_no','gpcol.transaction_id','gpcol.cheque_no','gpcol.collection_cr_id','gpcol.collection_cr_name','gpcol.collection_cr_date','gpcol.approve_status','gpcol.created_at','gpcol.updated_at','gpor.or_blc_price','gpor.payment_status','gpuse.firstname','gpor.or_tot_price','gpcol.check_status']);

        }else{
            $results = DB::table('gpff_collection as gpcol')
            ->join('gpff_vendor_order as gvpor', 'gpcol.order_id', 'gvpor.vendor_order_id')
            ->where('gpcol.order_id', $Collections->order_id)
            ->where('gpcol.collection_for', 2)
            ->orderBy('gpcol.updated_at','DESC')
            ->get(['gpcol.collection_id','gpcol.finance_user_id','gpcol.invoice_no','gpcol.order_id','gpcol.customer_name','gpcol.col_type','gpcol.collection_date','gpcol.collection_for','gpcol.collection_status','gpcol.collection_mode','gpcol.collection_amount','gpcol.collection_cr_id','gpcol.status','gpcol.bank_name','gpcol.card_type','gpcol.card_no','gpcol.holder_name','gpcol.account_no','gpcol.transaction_id','gpcol.cheque_no','gpcol.collection_cr_id','gpcol.collection_cr_name','gpcol.collection_cr_date','gpcol.approve_status','gpcol.created_at','gpcol.updated_at','gvpor.or_blc_price','gvpor.payment_status','gvpor.or_tot_price','gpcol.check_status','gpcol.checker_name']);

        }
        return $results;
    }

    public function fetchRegionBaseCusOrderVenOrderDet($Collections){
        
        $region_id = $Collections->region_id;

        if ($Collections->type == 1){
            $details = DB::table('gpff_order as gpor')
            ->join('gpff_area as gpar', 'gpor.area_id', 'gpar.area_id')
            ->join('gpff_warehouse as gpwar', 'gpor.warehouse_id', 'gpwar.warehouse_id')
            ->where('gpor.region_id', $region_id)
            ->where('gpor.order_status', 3)
            ->whereIn('gpor.payment_status', [1,2])
            // ->whereBetween(DB::RAW('date(created_at)'), [date($Collections->start_date), date($Collections->end_date)])
            ->orderBy('gpor.updated_at','DESC');
            // ->get();
            if($Collections->order_id !=''){
                $details = $details->where('gpor.order_id', $Collections->order_id);
                // $
            }

            if($Collections->start_date != ''){
                $details = $details->whereBetween(DB::RAW('date(gpor.created_at)'), [date($Collections->start_date), date($Collections->end_date)]);
            }

            if($Collections->customer_id != ''){
                $details = $details->whereIn('gpor.customer_id', $Collections->customer_id);
            }

            if($Collections->area_id != ''){
                $details = $details->whereIn('gpor.area_id', $Collections->area_id);
            }

            if($Collections->field_officer_id != ''){
                $details = $details->whereIn('gpor.field_officer_id', $Collections->field_officer_id);
            }

            if($Collections->warehouse_id != ''){
                $details = $details->whereIn('gpor.warehouse_id', $Collections->warehouse_id);
            }

            // if($Collections->order_status != ''){
            //     $details = $details->where('order_status', $Collections->order_status);
            // }

            if($Collections->payment_status != ''){
                $details = $details->where('gpor.payment_status', $Collections->payment_status);
            }

            // if($Collections->start_date != ''){
            //     $details = $details->whereBetween(DB::RAW('date(created_at)'), [date($Collections->start_date), date($Collections->end_date)]);
            // }

            $or_details = $details->get(['gpor.created_at','gpor.order_id','gpor.or_tot_price', 'gpor.customer_id','gpor.customer_name','gpor.payment_type', 'gpor.payment_status','gpor.or_blc_price', 'gpor.invoice_no','gpor.area_id', 'gpor.region_id', 'gpor.field_officer_name','gpar.area_name', 'gpor.credit_lim', 'gpor.credit_valid_to','gpwar.warehouse_name']);

            return $or_details;
        }else{

            $details = DB::table('gpff_vendor_order as gpvor')
            ->join('gpff_area as gpar', 'gpvor.area_id', 'gpar.area_id')
            // ->join()
            ->join('gpff_warehouse as gpwar', 'gpvor.warehouse_id', 'gpwar.warehouse_id')
            ->where('gpvor.region_id', $region_id)
            ->where('gpvor.vendor_order_status', 3)
            ->whereIn('gpvor.payment_status', [1,2])
                    // ->whereIn('payment_status')
            // ->whereBetween(DB::RAW('date(gpvor.created_at)'), [date($Collections->start_date), date($Collections->end_date)])
            ->orderBy('gpvor.updated_at','DESC');

            if($Collections->order_id !=''){
                $details = $details->where('gpvor.vendor_order_id', $Collections->order_id);
            }

            if($Collections->start_date != ''){
                $details = $details->whereBetween(DB::RAW('date(gpvor.created_at)'), [date($Collections->start_date), date($Collections->end_date)]);
            }
            
            if($Collections->customer_id != ''){
                $details = $details->whereIn('gpvor.vendor_id', $Collections->customer_id);
            }

            if($Collections->area_id != ''){
                $details = $details->whereIn('gpvor.area_id', $Collections->area_id);
            }

            // if($Collections->field_officer_id != ''){
            //     $details = $details->whereIn('gpvor.field_officer_id', $Collections->field_officer_id);
            // }

            // if($Collections->order_status != ''){
            //     $details = $details->where('vendor_order_status', $Collections->order_status);
            // }

            if($Collections->payment_status != ''){
                $details = $details->where('gpvor.payment_status', $Collections->payment_status);
            }

            // if($Collections->start_date != ''){
            //     $details = $details->whereBetween(DB::RAW('date(created_at)'), [date($Collections->start_date), date($Collections->end_date)]);
            // }
            if($Collections->warehouse_id != ''){
                $details = $details->whereIn('gpvor.warehouse_id', $Collections->warehouse_id);
            }

            $or_details = $details->get(['gpvor.created_at','gpvor.vendor_order_id','gpvor.or_tot_price','gpvor.vendor_id', 'gpvor.vendor_name','gpvor.payment_type', 'gpvor.payment_status','gpvor.or_blc_price','gpvor.invoice_no', 'gpvor.area_id', 'gpvor.region_id', 'gpar.area_name', 'gpvor.credit_valid_to', 'gpvor.credit_lim', 'gpwar.warehouse_name']);

            return $or_details;
        }
        
    }

    public function regionBaseCusOrVendorDetails($Collections){
        if($Collections->type == 1){
            $results = DB::table('gpff_customer')
            ->where('region_id', $Collections->region_id)
            ->where('vendor', 1)
            ->get();
        }else{
            $results = DB::table('gpff_customer')
            ->where('region_id', $Collections->region_id)
            ->where('vendor', 2)
            ->get();
        }
        return $results;
    }

 //    public function checkerStatusUpdate($Collections){


 //     $value = array(
 //        "checker_id" => $Collections->checker_id,
 //        "checker_name" =>$Collections->checker_name,
 //        "checker_update_date" =>date('Y-m-d H:i:s'),
 //        "check_status" => 1,
 //        "updated_at"=>date('Y-m-d H:i:s')
 //    );
 //     DB::table('gpff_collection')
 //     ->where('collection_id', $Collections->collection_id)
 //     ->where('doer_status', 1)
 //     ->update($value);

 //     $datas = DB::table('gpff_collection')
 //     ->where('collection_id', $Collections->collection_id)
 //     ->First();
 //             // print_r($datas);
 //             // print_r($datas->order_id);
 //             // exit();
 //     if($datas->collection_for == 1){
 //         $values = array(
 //            'payment_status'=>3,
 //            'updated_at'=>date('Y-m-d H:i:s')
 //        );

 //         DB::table('gpff_order')
 //         ->where('order_id', $datas->order_id)
 //         ->update($values);
 //     }else{
 //         $values = array(
 //            'payment_status'=>3,
 //            'updated_at'=>date('Y-m-d H:i:s')
 //        );

 //         DB::table('gpff_vendor_order')
 //         ->where('order_id', $datas->order_id)
 //         ->update($values);
 //     }
 //     return 1;
 // }

     public function fetchRegionBasedCollect($Collections){
        $results = DB::table('gpff_collection as gpcol')
        ->join('gpff_order as gpor' , 'gpor.order_id', 'gpcol.order_id')
        ->where('gpcol.region_id', $Collections->region_id)
        ->get(['gpcol.collection_id','gpcol.finance_user_id','gpcol.invoice_no','gpcol.order_id','gpcol.customer_name','gpcol.col_type','gpcol.collection_date','gpcol.collection_for','gpcol.collection_status','gpcol.collection_mode','gpcol.collection_amount','gpcol.collection_cr_id','gpcol.status','gpcol.bank_name','gpcol.card_type','gpcol.card_no','gpcol.holder_name','gpcol.account_no','gpcol.transaction_id','gpcol.cheque_no','gpcol.collection_cr_id','gpcol.collection_cr_name','gpcol.collection_cr_date','gpcol.approve_status','gpcol.created_at','gpcol.updated_at','gpor.or_blc_price','gpor.payment_status','gpor.or_tot_price']);

        return $results;
    }

    public function fetchAreaBasedCollect($Collections){

        if($Collections->check_status == 1){
            $results = DB::table('gpff_collection as gpcol')
            ->join('gpff_order as gpor' , 'gpor.order_id', 'gpcol.order_id')
            ->whereIn('gpcol.area_id', $Collections->area_id)
            ->where('gpcol.check_status', 1)
            ->orderBy('gpcol.created_at', 'desc')
            ->get(['gpcol.collection_id','gpcol.finance_user_id','gpcol.invoice_no','gpcol.order_id','gpcol.customer_name','gpcol.col_type','gpcol.collection_date','gpcol.collection_for','gpcol.collection_status','gpcol.collection_mode','gpcol.collection_amount','gpcol.collection_cr_id','gpcol.status','gpcol.bank_name','gpcol.card_type','gpcol.card_no','gpcol.holder_name','gpcol.account_no','gpcol.transaction_id','gpcol.cheque_no','gpcol.collection_cr_id','gpcol.collection_cr_name','gpcol.check_status','gpcol.collection_cr_date','gpcol.approve_status','gpcol.created_at','gpcol.updated_at','gpor.or_blc_price','gpor.payment_status','gpor.or_tot_price']);

            return $results;
        }else{
            $results = DB::table('gpff_collection as gpcol')
            ->join('gpff_order as gpor' , 'gpor.order_id', 'gpcol.order_id')
            ->whereIn('gpcol.area_id', $Collections->area_id)
            ->where('gpcol.check_status', 2)
            ->orderBy('gpcol.created_at', 'desc')
            ->get(['gpcol.collection_id','gpcol.finance_user_id','gpcol.invoice_no','gpcol.order_id','gpcol.customer_name','gpcol.col_type','gpcol.collection_date','gpcol.collection_for','gpcol.collection_status','gpcol.collection_mode','gpcol.collection_amount','gpcol.collection_cr_id','gpcol.status','gpcol.bank_name','gpcol.card_type','gpcol.card_no','gpcol.holder_name','gpcol.account_no','gpcol.transaction_id','gpcol.cheque_no','gpcol.collection_cr_id','gpcol.collection_cr_name','gpcol.check_status','gpcol.collection_cr_date','gpcol.approve_status','gpcol.created_at','gpcol.updated_at','gpor.or_blc_price','gpor.payment_status','gpor.or_tot_price']);
            return $results;
        }
    }

    public function fetchFieldOfficerCollection($Collections){
        return $results = DB::table('gpff_collection as gpcol')
                        ->join('gpff_order as gpor', 'gpcol.order_id', 'gpor.order_id')
                        ->where('gpcol.warehouse_id', $Collections->warehouse_id)
                        ->where('gpcol.doer_status', 0)
                        ->orderBy('gpcol.created_at', 'desc')
                        ->get(['gpcol.collection_id','gpcol.customer_id','gpcol.customer_name','gpcol.invoice_no','gpcol.order_id','gpcol.finance_user_id','gpcol.col_type','gpcol.collection_date','gpcol.collection_for','gpcol.collection_status','gpcol.collection_mode','gpcol.collection_amount','gpcol.area_id','gpcol.region_id', 'gpcol.doer_id', 'gpcol.doer_name','gpcol.doer_date', 'gpcol.doer_status','gpcol.bank_name','gpcol.card_type','gpcol.card_no','gpcol.holder_name','gpcol.account_no','gpcol.transaction_id','gpcol.cheque_no','gpcol.collection_cr_id','gpcol.collection_cr_name','gpcol.collection_cr_date','gpcol.approve_status','gpcol.created_at','gpcol.updated_at','gpor.or_blc_price','gpor.payment_status','gpor.or_tot_price']);
    }

    public function expireDateReport($Collections){

        $today = Carbon::today();

        if($Collections->type == 0){  

            // print_r($Collections->branch_id);

            $details = DB::table('gpff_product_batch_stock as gppbst')
            ->join('gpff_product_stock as gpps', 'gpps.product_stock_id','gppbst.product_stock_id')
            ->join('gpff_branch as gpbr', 'gpbr.branch_id','gpps.branch_id')
            ->join('gpff_region as gpre', 'gpre.region_id', 'gpps.region_id')
            ->join('gpff_warehouse as gpwar', 'gppbst.warehouse_id','gpwar.warehouse_id')
            ->whereDate('exp_date', '<=', $today);
            // ->whereBetween(DB::RAW('date(gppbst.exp_date)'), [date($Collections->start_date), date($Collections->end_date)]);

            if($Collections->product_id!=''){
                $details = $details->whereIn('gppbst.product_id', $Collections->product_id);
            }

            if($Collections->start_date!=''){
                $details = $details->whereBetween(DB::RAW('date(gppbst.exp_date)'), [date($Collections->start_date), date($Collections->end_date)]);
            }

            if($Collections->branch_id!=''){
                $details = $details->whereIn('gpps.branch_id', $Collections->branch_id);
            }

            if($Collections->warehouse_id != ''){
                $details = $details->whereIn('gpps.warehouse_id', $Collections->warehouse_id);
            }

            if($Collections->region_id != ''){
                $details = $details->whereIn('gpps.region_id', $Collections->region_id);
            }

            $results= $details->get(['gpbr.branch_name','gpre.region_name','gpwar.warehouse_name','gpwar.warehouse_type','gppbst.product_genericname','gppbst.batch_id','gppbst.exp_date','gppbst.product_id','gppbst.warehouse_id','gppbst.product_tot_qty','gppbst.product_blc_qty']);

            return $results;

        }else{
        // print_r("expression");

            $details = DB::table('gpff_product_batch_stock as gppbst')
            ->join('gpff_product_stock as gpps', 'gpps.product_stock_id','gppbst.product_stock_id')
            ->join('gpff_branch as gpbr', 'gpbr.branch_id','gpps.branch_id')
            ->join('gpff_region as gpre', 'gpre.region_id', 'gpps.region_id')
            ->join('gpff_warehouse as gpwar', 'gppbst.warehouse_id','gpwar.warehouse_id');
            // ->whereDate('exp_date', '<=', $today);
            // ->whereBetween(DB::RAW('date(gppbst.exp_date)'), [date($Collections->start_date), date($Collections->end_date)]);

            if($Collections->product_id!=''){
                $details = $details->whereIn('gppbst.product_id', $Collections->product_id);
            }

            if($Collections->start_date!=''){
                $details = $details->whereBetween(DB::RAW('date(gppbst.exp_date)'), [date($Collections->start_date), date($Collections->end_date)]);
            }

            if($Collections->branch_id!=''){
                $details = $details->whereIn('gpps.branch_id', $Collections->branch_id);
            }

            if($Collections->warehouse_id != ''){
                $details = $details->whereIn('gpps.warehouse_id', $Collections->warehouse_id);
            }

            if($Collections->region_id != ''){
                $details = $details->whereIn('gpps.region_id', $Collections->region_id);
            }

            $results= $details->get(['gpbr.branch_name','gpre.region_name','gpwar.warehouse_name','gpwar.warehouse_type','gppbst.product_genericname','gppbst.batch_id','gppbst.exp_date','gppbst.product_id','gppbst.warehouse_id', 'gppbst.product_tot_qty','gppbst.product_blc_qty']);

            return $results;

        }
    }

    public function dailyCollectionReport($Collections){
        $details = DB::table('gpff_collection')
        ->join('gpff_region as gpr','gpr.region_id','gpff_collection.region_id')
        ->where('collection_for', $Collections->type);
        if($Collections->start_date != ''){
            $details = $details->whereBetween(DB::RAW('date(checker_update_date)'), [date($Collections->start_date), date($Collections->end_date)]);
        }
        if($Collections->branch_id != ''){
            $details = $details->whereIn('gpr.branch_id', $Collections->branch_id);
        }
        if($Collections->region_id != ''){
            $details = $details->whereIn('gpff_collection.region_id', $Collections->region_id);
        }
        if($Collections->customer_id != ''){
            $details = $details->whereIn('customer_id', $Collections->customer_id);
        }
        if($Collections->invoice_no != ''){
            $details = $details->where('order_id', $Collections->invoice_no);
        }
        if($Collections->collection_cr_id != ''){
            $details = $details->where('collection_cr_id', $Collections->collection_cr_id);
        }
        switch($Collections->report_type){
            case 1:
                $results = $details->get();
            break;
            case 2:
                $total_price = $details->sum('collection_amount');
                $total_collection = $details->groupBy(DB::raw("DATE_FORMAT(checker_update_date, '%d-%m-%Y')"))
                    // ->orderBy('checker_update_date','ASC')
                    ->get([DB::raw("(DATE_FORMAT(checker_update_date, '%d-%m-%Y')) as collection_date"),DB::Raw('SUM(collection_amount) as total_price')]);
                $result = array(
                  "total_collection"=>$total_collection,
                  "total_price"=> $total_price
                );
                $results = $result;
            break;
            case 3:
                $total_price = $details->sum('collection_amount');
                $collection_det = $details->join('gpff_customer as gcustomer','gcustomer.customer_id','gpff_collection.customer_id')->groupBy('gcustomer.customer_id')->get(['gpff_collection.customer_id','gcustomer.customer_name',DB::raw('sum(collection_amount) as total_price')]);
                $cus_data = [];
                foreach($collection_det as $customer)
                {
                  $cus_data[] =  (array) $customer;
                }
                $customer_wise = usort($cus_data, function($a, $b) {
                       return ($a['total_price'] > $b['total_price']? -1 : 1);
                     });
                $result = array(
                  "customer_wise"=>$cus_data,
                  "total_price"=> $total_price
                );
                $results = $result;
            break;
            case 4:
                $total_price = $details->sum('collection_amount');
                $collection_det = $details->join('gpff_users as guser','guser.user_id','collection_cr_id')->groupBy('collection_cr_id')->get(['collection_cr_id','guser.firstname',DB::raw('sum(collection_amount) as total_price')]);
                $cus_data = [];
                foreach($collection_det as $customer)
                {
                  $cus_data[] =  (array) $customer;
                }
                $customer_wise = usort($cus_data, function($a, $b) {
                       return ($a['total_price'] > $b['total_price']? -1 : 1);
                     });
                $result = array(
                  "fo_wise"=>$cus_data,
                  "total_price"=> $total_price
                );
                $results = $result;
            break;
        }
        

        return $results;
    }

    public function dailyCollectionVendorReport($Collections){
        $details = DB::table('gpff_collection')
        ->join('gpff_region as gpr','gpr.region_id','gpff_collection.region_id')
        ->where('collection_for', 2);
        if($Collections->start_date != ''){
            $details = $details->whereBetween(DB::RAW('date(checker_update_date)'), [date($Collections->start_date), date($Collections->end_date)]);
        }
        if($Collections->branch_id != ''){
            $details = $details->whereIn('gpr.branch_id', $Collections->branch_id);
        }
        if($Collections->region_id != ''){
            $details = $details->whereIn('gpff_collection.region_id', $Collections->region_id);
        }
        
        if($Collections->customer_id != ''){
            $details = $details->whereIn('customer_id', $Collections->customer_id);
        }

        if($Collections->invoice_no != ''){
            $details = $details->where('order_id', $Collections->invoice_no);
        }

        if($Collections->collection_cr_id != ''){
            $details = $details->whereIn('collection_cr_id', $Collections->collection_cr_id);
        }

        $results = $details->get();

        return $results;
    }

    public function invoiceBasedReturnDetails($Collections){

        $values = DB::table('gpff_sale_return')
        ->where('invoice_no', $Collections->invoice_no)
        ->First(['customer_id', 'sale_return_id', 'customer_name']);
            // print_r($values);
            // exit();

        $customer = DB::table('gpff_customer')
        ->where('customer_id', $values->customer_id)
        ->get();

        $product = DB::table('gpff_sale_return_batchwise_list as gpsrbl')
        ->join('gpff_sale_return_list as gpsretl', 'gpsrbl.sale_return_list_id','gpsretl.sale_return_list_id')
        ->where('gpsrbl.invoice_no', $Collections->invoice_no)
        ->orderBy('gpsrbl.sale_return_id', 'ASC')
        ->get(['gpsrbl.order_id','gpsrbl.sale_return_id','gpsrbl.sale_return_list_id','gpsrbl.category_id','gpsretl.category_name','gpsrbl.product_id','gpsretl.product_genericname','gpsrbl.batch_no','gpsrbl.batch_emp_date','gpsrbl.product_qty','gpsrbl.product_netprice','gpsrbl.product_grossprice','gpsrbl.or_gross_total','gpsrbl.product_discount','gpsrbl.product_tot_price','gpsrbl.product_type','gpsrbl.scheme','gpsrbl.comments','gpsrbl.FOC','gpsrbl.FOC_amt','gpsrbl.spl_FOC','gpsrbl.pro_amt_cal','gpsrbl.created_at','gpsrbl.updated_at']);

        $status = DB::table('gpff_sale_return')
        ->where('invoice_no', $Collections->invoice_no)
        ->get(['order_return_date','return_reason','or_return_type','or_gross_total','or_tot_price','invoice_no']);

        $results =array(
            'customer'  =>  $customer,
            'product'   =>  $product,
            'status'    =>  $status
        );

        return $results;
    }

    public function orderReturnReport($Collections){
        if($Collections->or_return_type == 1){

            $details = DB::table('gpff_sale_return_list as gpsalist')
            ->join('gpff_sale_return_batchwise_list as gpsrlb', 'gpsrlb.sale_return_list_id', 'gpsalist.sale_return_list_id')
            ->join('gpff_sale_return as gpsare', 'gpsare.sale_return_id', 'gpsalist.sale_return_id')
            ->join('gpff_order as gpor', 'gpor.order_id', 'gpsalist.order_id')
            ->join('gpff_warehouse as gw','gw.warehouse_id','gpor.warehouse_id')
            // ->join('gpff_sale_return_batchwise_list as gpsabal', 'gpsalist.sale_return_id', 'gpsabal.sale_return_id')            
            ->where('gpsalist.return_qty','!=',NULL)
            ->where('gpsare.or_return_type', 1)
            ->distinct("gpsalist.product_id");

            if($Collections->invoice_no !=''){
                $details = $details->where('gpsare.order_id', $Collections->invoice_no);
            }

            if($Collections->start_date !=''){
                $details = $details->whereBetween(DB::RAW('date(gpsare.created_at)'), [date($Collections->start_date), date($Collections->end_date)]);
            }

            if($Collections->customer_id != ''){
                $details = $details->where('gpsare.customer_id', $Collections->customer_id);
            }

            if($Collections->branch_id != ''){
                $details = $details->whereIn('gpsare.branch_id', $Collections->branch_id);
            }

            if($Collections->region_id != ''){
                $details = $details->whereIn('gpsare.region_id', $Collections->region_id);
            }

            if($Collections->warehouse_id != ''){
                $details = $details->whereIn('gpsare.warehouse_id', $Collections->warehouse_id);
            }
            if($Collections->field_officer_id != ''){
                $details = $details->whereIn('gpor.field_officer_id', $Collections->field_officer_id);
            }

             $results = $details->get(['gpsrlb.return_FOC','gpsare.invoice_no', 'gpsare.order_id', 'gpsare.return_grand_total','gpsare.customer_id','gpsare.customer_name','gpsare.order_return_date','gpsare.return_reason','gpsare.or_gross_total', 'gpsare.or_tot_price','gpsare.payment_type',
                'gpsare.payment_status','gpsrlb.batch_no','gpsalist.category_id','gpsalist.category_name','gpsalist.product_id','gpsalist.product_genericname','gpsalist.product_qty','gpsalist.return_qty','gpsalist.accepted_qty','gpsalist.product_tot_price','gpsalist.product_return_total_price','gpsalist.product_type','gpor.field_officer_name','gw.warehouse_name']);
            // $results = $details->get(['gpsare.invoice_no', 'gpsare.order_id', 'gpsare.return_grand_total','gpsare.customer_id','gpsare.customer_name','gpsare.order_return_date','gpsare.return_reason','gpsare.or_gross_total', 'gpsare.or_tot_price','gpsare.payment_type',
            //     'gpsare.payment_status','gpsalist.category_id','gpsalist.category_name','gpsalist.product_id','gpsalist.product_genericname','gpsalist.product_qty','gpsalist.return_qty','gpsalist.accepted_qty','gpsalist.product_tot_price','gpsalist.product_return_total_price','gpsalist.product_type','gpsabal.product_netprice','gpsabal.product_grossprice','gpsabal.batch_no','gpsabal.batch_emp_date','gpsabal.product_discount','gpsabal.product_tot_price','gpsabal.product_type','gpsabal.scheme','gpsabal.FOC','gpsabal.FOC_amt','gpsabal.spl_FOC','gpsabal.pro_amt_cal']);

            return $results;
        }else{
           $details = DB::table('gpff_sale_return_list as gpsalist')
            ->join('gpff_sale_return as gpsare', 'gpsare.sale_return_id', 'gpsalist.sale_return_id')
            ->join('gpff_vendor_order as gpvor', 'gpvor.vendor_order_id', 'gpsalist.order_id')
            ->join('gpff_users as gpu', 'gpu.user_id', 'gpvor.area_manager_id')
            ->join('gpff_warehouse as gw','gw.warehouse_id','gpvor.warehouse_id')
               ->join('gpff_sale_return_batchwise_list as gpsabal', 'gpsabal.order_id', 'gpvor.vendor_order_id')
            ->where('gpsalist.return_qty','!=',NULL)
            ->where('gpsare.or_return_type', 2)
             ->distinct("gpsalist.product_id");

            if($Collections->invoice_no !=''){
                $details = $details->where('gpsare.order_id', $Collections->invoice_no);
            }

            if($Collections->start_date !=''){
                $details = $details->whereBetween(DB::RAW('date(gpsare.created_at)'), [date($Collections->start_date), date($Collections->end_date)]);
            }

            if($Collections->customer_id != ''){
                $details = $details->where('gpsare.customer_id', $Collections->customer_id);
                // $details = $details->whereIn('gpsare.customer_id', $Collections->customer_id);
            }

            if($Collections->branch_id != ''){
                $details = $details->whereIn('gpsare.branch_id', $Collections->branch_id);
            }

            if($Collections->region_id != ''){
                $details = $details->whereIn('gpsare.region_id', $Collections->region_id);
            }

            if($Collections->warehouse_id != ''){
                $details = $details->whereIn('gpsare.warehouse_id', $Collections->warehouse_id);
            }
            if($Collections->area_manager_id != ''){
                $details = $details->where('gpvor.area_manager_id', $Collections->area_manager_id);
            }
            // $results = $details->get(['gpsare.invoice_no', 'gpsare.order_id', 'gpsare.return_grand_total','gpsare.customer_id','gpsare.customer_name','gpsare.order_return_date','gpsare.return_reason','gpsare.or_gross_total', 'gpsare.or_tot_price','gpsare.payment_type',
            //     'gpsare.payment_status','gpsalist.category_id','gpsalist.category_name','gpsalist.product_id','gpsalist.product_genericname','gpsalist.product_qty','gpsalist.return_qty','gpsalist.accepted_qty','gpsalist.product_tot_price','gpsalist.product_return_total_price','gpsalist.product_type','gw.warehouse_name']);
           
                $results = $details->get(['gpsabal.return_FOC','gpsare.invoice_no', 'gpsare.order_id', 'gpsare.return_grand_total','gpsare.customer_id','gpsare.customer_name','gpsare.order_return_date','gpsare.return_reason','gpsare.or_gross_total', 'gpsare.or_tot_price','gpsare.payment_type',
                'gpsare.payment_status','gpsalist.category_id','gpsalist.category_name','gpsalist.product_id','gpsalist.product_genericname','gpsalist.product_qty','gpsalist.return_qty','gpsalist.accepted_qty','gpsalist.product_tot_price','gpsalist.product_return_total_price','gpsalist.product_type','gpu.firstname','gpu.lastname','gw.warehouse_name']);

           
                // $results = $details->get(['gpsare.invoice_no', 'gpsare.order_id', 'gpsare.return_grand_total','gpsare.customer_id','gpsare.customer_name','gpsare.order_return_date','gpsare.return_reason','gpsare.or_gross_total', 'gpsare.or_tot_price','gpsare.payment_type',
            //     'gpsare.payment_status','gpsalist.category_id','gpsalist.category_name','gpsalist.product_id','gpsalist.product_genericname','gpsalist.product_qty','gpsalist.return_qty','gpsalist.accepted_qty','gpsalist.product_tot_price','gpsalist.product_return_total_price','gpsalist.product_type','gpsabal.product_netprice','gpsabal.product_grossprice','gpsabal.batch_no','gpsabal.batch_emp_date','gpsabal.product_discount','gpsabal.product_tot_price','gpsabal.product_type','gpsabal.scheme','gpsabal.FOC','gpsabal.FOC_amt','gpsabal.spl_FOC','gpsabal.pro_amt_cal']);

            return $results;
        }
    }

    public function getCollectionCrDetails(){
        $data = DB::table('gpff_collection')
        ->select('collection_cr_name','collection_cr_id')
        ->groupBy('collection_cr_name', 'collection_cr_id')
        ->get();

        return $data;
    }

    public function getRegionBasedWarehouses($Collections){
        $results = DB::table('gpff_warehouse')
        ->where('region_id', $Collections->region_id)
        ->get();
        return $results;
    }


    public function financeUsrBasedReturnDetails($Collections){
        if($Collections->or_return_type == 1){
            // // print_r($Collections->finance_user_id);
            // $values = DB::table('gpff_sale_return')
            // ->where('finance_user_id', $Collections->finance_user_id)
            // ->First(['customer_id', 'sale_return_id', 'customer_name']);
            // // print_r($values);
            // // exit();

            // // $customer = DB::table('gpff_customer')
            // // ->where('vendor', 2)
            // // ->where('customer_id', $values->customer_id)
            // // ->get();

            // $product = DB::table('gpff_sale_return_batchwise_list as gpsrbl')
            // ->join('gpff_sale_return_list as gpsretl', 'gpsrbl.sale_return_list_id','gpsretl.sale_return_list_id')
            // ->join('gpff_sale_return as gpsr', 'gpsr.sale_return_id','gpsretl.sale_return_id')
            // ->where('gpsr.or_return_type', 1)
            // ->where('gpsr.finance_user_id', $Collections->finance_user_id)
            // // ->orderBy('gpsr.sale_return_id', 'ASC')
            // ->get(['gpsrbl.order_id','gpsretl.sale_return_id','gpsretl.sale_return_list_id','gpsretl.category_id','gpsretl.category_name','gpsretl.product_id','gpsretl.product_genericname','gpsrbl.batch_no','gpsrbl.batch_emp_date','gpsrbl.product_qty','gpsrbl.product_netprice','gpsrbl.product_grossprice','gpsrbl.or_gross_total','gpsrbl.product_discount','gpsrbl.product_tot_price','gpsrbl.product_type','gpsrbl.scheme','gpsrbl.comments','gpsrbl.FOC','gpsrbl.FOC_amt','gpsrbl.spl_FOC','gpsrbl.pro_amt_cal','gpsrbl.created_at','gpsrbl.updated_at']);

            $results = DB::table('gpff_sale_return')
            ->where('or_return_type', 1)
            ->where('finance_user_id', $Collections->finance_user_id)
            ->get(['order_id','customer_id','customer_name','order_return_date','return_reason','or_return_type','or_gross_total','or_tot_price','invoice_no','return_grand_total']);

            // $results =array(
            //     // 'customer'  =>  $customer,
            //     'product'   =>  $product,
            //     'status'    =>  $status
            // );

            return $results;
        }else{
            // print_r($Collections->finance_user_id);            
            // $values = DB::table('gpff_sale_return')
            // ->where('finance_user_id', $Collections->finance_user_id)
            // ->First(['customer_id', 'sale_return_id', 'customer_name']);

            // print_r($values);
            // print_r($values);
            // exit();

            // $customer = DB::table('gpff_customer')
            // ->where('vendor', 1)
            // ->where('customer_id', $values->customer_id)
            // ->get();

            // $product = DB::table('gpff_sale_return_batchwise_list as gpsrbl')
            // ->join('gpff_sale_return_list as gpsretl', 'gpsrbl.sale_return_list_id','gpsretl.sale_return_list_id')
            // ->join('gpff_sale_return as gpsr', 'gpsr.sale_return_id','gpsretl.sale_return_id')
            // ->where('gpsr.or_return_type',2)
            // ->where('gpsr.invoice_no', $Collections->finance_user_id)
            // ->orderBy('gpsrbl.sale_return_id', 'ASC')
            // ->get(['gpsrbl.order_id','gpsrbl.sale_return_id','gpsrbl.sale_return_list_id','gpsrbl.category_id','gpsretl.category_name','gpsrbl.product_id','gpsretl.product_genericname','gpsrbl.batch_no','gpsrbl.batch_emp_date','gpsrbl.product_qty','gpsrbl.product_netprice','gpsrbl.product_grossprice','gpsrbl.or_gross_total','gpsrbl.product_discount','gpsrbl.product_tot_price','gpsrbl.product_type','gpsrbl.scheme','gpsrbl.comments','gpsrbl.FOC','gpsrbl.FOC_amt','gpsrbl.spl_FOC','gpsrbl.pro_amt_cal','gpsrbl.created_at','gpsrbl.updated_at']);
            // print_r($product);

            $results = DB::table('gpff_sale_return')
            ->where('or_return_type',2)
            ->where('finance_user_id', $Collections->finance_user_id)
            ->get(['order_id','customer_id','customer_name','order_return_date','return_reason','or_return_type','or_gross_total','or_tot_price','invoice_no','return_grand_total']);

            // $results =array(
            //     // 'customer'  =>  $customer,
            //     'product'   =>  $product,
            //     'status'    =>  $status
            // );

            return $results;
        }
    }

    public function orderIdBasedReturnDetails($Collections){
        if($Collections->or_return_type == 1){

            $values = DB::table('gpff_sale_return')
            ->where('order_id', $Collections->order_id)
            ->First(['customer_id', 'sale_return_id', 'customer_name']);
            if($values){
                $customer = DB::table('gpff_customer')
                ->where('vendor', 2)
                ->where('customer_id', $values->customer_id)
                ->get();

                $product = DB::table('gpff_sale_return_batchwise_list as gpsrbl')
                ->join('gpff_sale_return_list as gpsretl', 'gpsrbl.sale_return_list_id','gpsretl.sale_return_list_id')
                ->join('gpff_sale_return as gpsr', 'gpsr.sale_return_id','gpsretl.sale_return_id')
                ->where('gpsr.or_return_type', 1)
                ->where('gpsr.order_id', $Collections->order_id)
                ->orderBy('gpsr.sale_return_id', 'ASC')
                ->get(['gpsrbl.product_return_total_price','gpsrbl.return_FOC','gpsrbl.order_id','gpsretl.return_qty','gpsretl.sale_return_id','gpsretl.sale_return_list_id','gpsretl.category_id','gpsretl.category_name','gpsretl.product_id','gpsretl.product_genericname','gpsrbl.batch_no','gpsrbl.batch_emp_date','gpsrbl.product_qty','gpsrbl.product_netprice','gpsrbl.product_grossprice','gpsrbl.or_gross_total','gpsrbl.product_discount','gpsrbl.product_tot_price','gpsrbl.product_type','gpsrbl.scheme','gpsrbl.comments','gpsrbl.FOC','gpsrbl.FOC_amt','gpsrbl.spl_FOC','gpsrbl.pro_amt_cal','gpsrbl.created_at','gpsrbl.updated_at']);

                $status = DB::table('gpff_sale_return')
                ->where('or_return_type', 1)
                ->where('order_id', $Collections->order_id)
                ->get(['order_id','finance_user_id','customer_id','customer_name','order_return_date','return_reason','or_return_type','or_gross_total','or_tot_price','invoice_no','return_grand_total','branch_id', 'area_id', 'region_id','stockist_id','warehouse_id','return_bill','return_bill_type','payment_status','payment_type','or_return_type','payment_name','payment_img', 'payment_msg']);

                $results =array(
                    'customer'  =>  $customer,
                    'product'   =>  $product,
                    'status'    =>  $status
                );

                return $results;
            }else{
                return [];
            }
        }else{
            // print_r($Collections->finance_user_id);            
            $values = DB::table('gpff_sale_return')
            ->where('order_id', $Collections->order_id)
            ->First(['customer_id', 'sale_return_id', 'customer_name']);
            if($values){
                $customer = DB::table('gpff_customer')
                ->where('vendor', 1)
                ->where('customer_id', $values->customer_id)
                ->get();

                $product = DB::table('gpff_sale_return_batchwise_list as gpsrbl')
                ->join('gpff_sale_return_list as gpsretl', 'gpsrbl.sale_return_list_id','gpsretl.sale_return_list_id')
                ->join('gpff_sale_return as gpsr', 'gpsr.sale_return_id','gpsretl.sale_return_id')
                ->where('gpsr.or_return_type',2)
                ->where('gpsr.order_id', $Collections->order_id)
                ->orderBy('gpsrbl.sale_return_id', 'ASC')
                ->get(['gpsrbl.product_return_total_price','gpsrbl.return_FOC','gpsrbl.order_id','gpsrbl.sale_return_id','gpsrbl.sale_return_list_id','gpsrbl.category_id','gpsretl.category_name','gpsrbl.product_id','gpsretl.product_genericname','gpsretl.return_qty','gpsrbl.batch_no','gpsrbl.batch_emp_date','gpsrbl.product_qty','gpsrbl.product_netprice','gpsrbl.product_grossprice','gpsrbl.or_gross_total','gpsrbl.product_discount','gpsrbl.product_tot_price','gpsrbl.product_type','gpsrbl.scheme','gpsrbl.comments','gpsrbl.FOC','gpsrbl.FOC_amt','gpsrbl.spl_FOC','gpsrbl.pro_amt_cal','gpsrbl.created_at','gpsrbl.updated_at']);
                // print_r($product);

                $status = DB::table('gpff_sale_return')
                ->where('or_return_type',2)
                ->where('order_id', $Collections->order_id)
                ->get(['order_id','finance_user_id','customer_id','customer_name','order_return_date','return_reason','or_return_type','or_gross_total','or_tot_price','invoice_no','return_grand_total','branch_id', 'area_id', 'region_id','stockist_id','warehouse_id','return_bill','return_bill_type','payment_status','payment_type','or_return_type','payment_name','payment_img', 'payment_msg']);

                $results =array(
                    'customer'  =>  $customer,
                    'product'   =>  $product,
                    'status'    =>  $status
                );

                return $results;
            }else{
                return [];
            }
        }
    }

    public function doer_list($Collections){
        return $resuls = DB::table('gpff_collection as gpcol')
                        ->join('gpff_users as gpuser', 'gpuser.user_id', 'gpcol.doer_id')
                        ->where('gpcol.checker_id', $Collections->checker_id)
                        // ->where('gpuser.role', [4,7])
                        ->select('gpuser.user_id', DB::raw("CONCAT(gpuser.firstname,' ',gpuser.lastname) AS full_name"))
                        ->distinct()
                        ->get();
    }

    public function checker_list(){
        return $resuls = DB::table('gpff_users')
                        ->where('role', 10)
                        ->select('user_id', DB::raw("CONCAT(firstname,' ',lastname) AS full_name"))
                        ->get();
    }

    public function checkerReport($Collections){
        $details = DB::table('gpff_collection')
                    ->where('region_id', $Collections->region_id)
                    ->where('checker_id', $Collections->checker_id)
                    ->where('doer_status', 1)
                    ->whereBetween(DB::RAW('date(checker_update_date)'), [date($Collections->start_date), date($Collections->end_date)])
                    ->orderBy('updated_at','DESC');

        if($Collections->doer_id != ''){
            $details = $details->where('doer_id', $Collections->doer_id);
        }

        $results = $details->get();

        return $results;
    }

    public function financeUserReport($Collections){
        $details = DB::table('gpff_collection')
                    ->where('region_id', $Collections->region_id)
                    ->where('finance_user_id', $Collections->finance_user_id)
                    ->where('check_status', 1)
                    ->whereBetween(DB::RAW('date(created_at)'), [date($Collections->start_date), date($Collections->end_date)])
                    // ->select('doer_name', 'doer_id','customer_name', 'customer_id', 'collection_for','doer_date', 'collection_cr_id','collection_cr_name')
                    // ->select('doer_name', 'doer_id','customer_name', 'customer_id', 'collection_for','doer_date', 'collection_cr_id','collection_cr_name',DB::raw("SUM(collection_amount) as collection_amount"))
                    // ->groupBy('doer_name', 'doer_id','customer_name', 'customer_id', 'collection_for','doer_date', 'collection_cr_id','collection_cr_name', 'collection_amount');
                    ->orderBy('updated_at','DESC');

        if($Collections->checker_id != ''){
            $details = $details->whereIn('checker_id', $Collections->checker_id);
        }

        $results = $details->get();

        return $results;
    }

    public function regionBaseCusOrderDetails($Collections){

        $details = DB::table('gpff_order')
            ->where('region_id', $Collections->region_id)
            ->where('order_status', 3)
            ->where('payment_status', 2)
            ->orderBy('updated_at','DESC')
            ->get();
        return $details;

    }

    public function financeUserAcceptedAndPendingReport($Collections){

        if($Collections->type == 1){

            $details = DB::table('gpff_collection')
                ->where('region_id', $Collections->region_id)
                ->where('approve_status', 1)
                ->whereBetween(DB::RAW('date(collect_approve_date)'), [date($Collections->start_date), date($Collections->end_date)])
                ->orderBy('collect_approve_date','DESC');

            if($Collections->finance_user_id != ''){
                $details = $details->where('collect_approve_id', $Collections->finance_user_id);
            }

            $results = $details->get();

            return $results;
        }else{
            $details = DB::table('gpff_collection')
                ->where('check_status', 1)
                ->where('approve_status', 2)
                ->whereBetween(DB::RAW('date(created_at)'), [date($Collections->start_date), date($Collections->end_date)])
                ->orderBy('updated_at','DESC');

            if($Collections->finance_user_id != ''){
                $details = $details->whereIn('finance_user_id', $Collections->finance_user_id);
            }

            $results = $details->get();

            return $results;
        }
    }


    public function financeUserPendingReport($Collections){

        $details = DB::table('gpff_collection')
                    // ->where('region_id', $Collections->region_id)
                    ->where('check_status', 1)
                    ->where('approve_status', 2)
                    ->whereBetween(DB::RAW('date(created_at)'), [date($Collections->start_date), date($Collections->end_date)])
                    ->orderBy('updated_at','DESC');

        if($Collections->finance_user_id != ''){
            $details = $details->where('collect_approve_id', $Collections->finance_user_id);
        }

        $results = $details->get();

        return $results;
    }

    public function getAllFinanceUser(){

        $results = DB::table('gpff_users')
                ->where('role', 9)
                ->get();
                
        return $results;
    }

    public function checkerDailyCollectionReport($Collections){
        $details = DB::table('gpff_collection')
            ->where('checker_id', $Collections->checker_id)
            ->where('collection_for', $Collections->type)
            ->whereBetween(DB::RAW('date(created_at)'), [date($Collections->start_date), date($Collections->end_date)])
            ->get();
        return $details;
    }

    public function collectionPendingReport($Collections){
        
        $region_id = $Collections->region_id;

        if ($Collections->type == 1){
            $details = DB::table('gpff_order as gpor')
            ->join('gpff_area as gpar', 'gpor.area_id', 'gpar.area_id')
            ->join('gpff_warehouse as gpwar', 'gpor.warehouse_id', 'gpwar.warehouse_id')
            ->where('gpor.order_status', 3)
            ->whereIn('gpor.payment_status', [1,2]);
            if($Collections->order_id !=''){
                $details = $details->where('gpor.order_id', $Collections->order_id);
                // $
            }
            if($Collections->branch_id !=''){
                $details = $details->whereIn('gpor.branch_id', $Collections->branch_id);
            }

            if($Collections->region_id !=''){
                $details = $details->whereIn('gpor.region_id', $Collections->region_id);
            }

            if($Collections->start_date != ''){
                $details = $details->whereBetween(DB::RAW('date(gpor.created_at)'), [date($Collections->start_date), date($Collections->end_date)]);
            }

            if($Collections->customer_id != ''){
                $details = $details->whereIn('gpor.customer_id', $Collections->customer_id);
            }

            if($Collections->area_id != ''){
                $details = $details->whereIn('gpor.area_id', $Collections->area_id);
            }

            if($Collections->field_officer_id != ''){
                $details = $details->whereIn('gpor.field_officer_id', $Collections->field_officer_id);
            }

            if($Collections->warehouse_id != ''){
                $details = $details->whereIn('gpor.warehouse_id', $Collections->warehouse_id);
            }


            if($Collections->payment_status != ''){
                $details = $details->where('gpor.payment_status', $Collections->payment_status);
            }            
            switch($Collections->report_type){
                case 1:
                    $results = $details->orderBy('gpor.updated_at','DESC')->get(['gpor.created_at','gpor.order_id','gpor.or_tot_price', 'gpor.customer_id','gpor.customer_name','gpor.payment_type', 'gpor.payment_status','gpor.or_blc_price', 'gpor.invoice_no','gpor.area_id', 'gpor.region_id', 'gpor.field_officer_name as username','gpar.area_name', 'gpor.credit_lim', 'gpor.credit_valid_to','gpwar.warehouse_name']);
                break;
                case 2:
                    $total_price = $details->sum('gpor.or_blc_price');
                    $order_total = $details->sum('gpor.or_tot_price');
                    $total_collection = $details->groupBy(DB::raw("DATE_FORMAT(gpor.created_at, '%d-%m-%Y')"))
                        // ->orderBy('checker_update_date','ASC')
                        ->get([DB::raw("(DATE_FORMAT(gpor.created_at, '%d-%m-%Y')) as collection_date"),DB::Raw('SUM(gpor.or_blc_price) as total_price')]);
                    $result = array(
                      "total_collection"=>$total_collection,
                      "total_price"=> $total_price,
                      "order_total"=>$order_total
                    );
                    $results = $result;
                break;
                case 3:
                    $total_price = $details->sum('gpor.or_blc_price');
                    $order_total = $details->sum('gpor.or_tot_price');
                    $collection_det = $details->join('gpff_customer as gcustomer','gcustomer.customer_id','gpor.customer_id')->groupBy('gcustomer.customer_id')->get(['gpor.customer_id','gcustomer.customer_name',DB::raw('sum(gpor.or_blc_price) as total_price')]);
                    $cus_data = [];
                    foreach($collection_det as $customer)
                    {
                      $cus_data[] =  (array) $customer;
                    }
                    $customer_wise = usort($cus_data, function($a, $b) {
                           return ($a['total_price'] > $b['total_price']? -1 : 1);
                         });
                    $result = array(
                      "customer_wise"=>$cus_data,
                      "total_price"=> $total_price,
                      "order_total"=> $order_total
                    );
                    $results = $result;
                break;
                case 4:
                    $total_price = $details->sum('gpor.or_blc_price');
                    $order_total = $details->sum('gpor.or_tot_price');
                    $collection_det = $details->join('gpff_users as guser','guser.user_id','gpor.field_officer_id')->groupBy('gpor.field_officer_id')->get(['gpor.field_officer_id','guser.firstname',DB::raw('sum(gpor.or_blc_price) as total_price')]);
                    $cus_data = [];
                    foreach($collection_det as $customer)
                    {
                      $cus_data[] =  (array) $customer;
                    }
                    $customer_wise = usort($cus_data, function($a, $b) {
                           return ($a['total_price'] > $b['total_price']? -1 : 1);
                         });
                    $result = array(
                      "fo_wise"=>$cus_data,
                      "total_price"=> $total_price,
                      "order_total"=> $order_total
                    );
                    $results = $result;
                break;
            }
            return $results;
        }else{

            $details = DB::table('gpff_vendor_order as gpvor')
            ->join('gpff_users as guser', 'gpvor.area_manager_id', 'guser.user_id')
            ->join('gpff_area as gpar', 'gpvor.area_id', 'gpar.area_id')
            // ->join()
            ->join('gpff_warehouse as gpwar', 'gpvor.warehouse_id', 'gpwar.warehouse_id')
            ->where('gpvor.vendor_order_status', 3)
            ->whereIn('gpvor.payment_status', [1,2]);
            if($Collections->branch_id !=''){
                $details = $details->whereIn('gpvor.branch_id', $Collections->branch_id);
            }

            if($Collections->region_id !=''){
                $details = $details->whereIn('gpvor.region_id', $Collections->region_id);
            }
            if($Collections->order_id !=''){
                $details = $details->where('gpvor.vendor_order_id', $Collections->order_id);
            }

            if($Collections->start_date != ''){
                $details = $details->whereBetween(DB::RAW('date(gpvor.created_at)'), [date($Collections->start_date), date($Collections->end_date)]);
            }
            
            if($Collections->customer_id != ''){
                $details = $details->whereIn('gpvor.vendor_id', $Collections->customer_id);
            }

            if($Collections->area_id != ''){
                $details = $details->whereIn('gpvor.area_id', $Collections->area_id);
            }

            if($Collections->field_officer_id != ''){
                $details = $details->whereIn('gpvor.area_manager_id', $Collections->field_officer_id);
            }

            if($Collections->payment_status != ''){
                $details = $details->where('gpvor.payment_status', $Collections->payment_status);
            }

            if($Collections->warehouse_id != ''){
                $details = $details->whereIn('gpvor.warehouse_id', $Collections->warehouse_id);
            }
             switch($Collections->report_type){
                case 1:
                    $results = $details->orderBy('gpvor.updated_at','DESC')->get(['gpvor.created_at','gpvor.vendor_order_id as order_id','gpvor.or_tot_price','gpvor.vendor_id', 'gpvor.vendor_name as customer_name','gpvor.payment_type', 'gpvor.payment_status','gpvor.or_blc_price','gpvor.invoice_no', 'gpvor.area_id', 'gpvor.region_id', 'guser.firstname as username','gpar.area_name', 'gpvor.credit_valid_to', 'gpvor.credit_lim', 'gpwar.warehouse_name']);;
                break;
                case 2:
                    $total_price = $details->sum('gpvor.or_blc_price');
                    $order_total = $details->sum('gpvor.or_tot_price');
                    $total_collection = $details->groupBy(DB::raw("DATE_FORMAT(gpvor.created_at, '%d-%m-%Y')"))
                        // ->orderBy('checker_update_date','ASC')
                        ->get([DB::raw("(DATE_FORMAT(gpvor.created_at, '%d-%m-%Y')) as collection_date"),DB::Raw('SUM(gpvor.or_blc_price) as total_price')]);
                    $result = array(
                      "total_collection"=>$total_collection,
                      "total_price"=> $total_price,
                      "order_total"=> $order_total
                    );
                    $results = $result;
                break;
                case 3:
                    $total_price = $details->sum('gpvor.or_blc_price');
                    $order_total = $details->sum('gpvor.or_tot_price');
                    $collection_det = $details->join('gpff_customer as gcustomer','gcustomer.customer_id','gpvor.vendor_id')->groupBy('gcustomer.customer_id')->get(['gpvor.vendor_id','gcustomer.customer_name',DB::raw('sum(gpvor.or_blc_price) as total_price')]);
                    $cus_data = [];
                    foreach($collection_det as $customer)
                    {
                      $cus_data[] =  (array) $customer;
                    }
                    $customer_wise = usort($cus_data, function($a, $b) {
                           return ($a['total_price'] > $b['total_price']? -1 : 1);
                         });
                    $result = array(
                      "customer_wise"=>$cus_data,
                      "total_price"=> $total_price,
                      "order_total"=> $order_total
                    );
                    $results = $result;
                break;
                case 4:
                    $total_price = $details->sum('gpvor.or_blc_price');
                    $order_total = $details->sum('gpvor.or_tot_price');
                    $collection_det = $details->groupBy('gpvor.area_manager_id')->get(['gpvor.area_manager_id','guser.firstname',DB::raw('sum(gpvor.or_blc_price) as total_price')]);
                    $cus_data = [];
                    foreach($collection_det as $customer)
                    {
                      $cus_data[] =  (array) $customer;
                    }
                    $customer_wise = usort($cus_data, function($a, $b) {
                           return ($a['total_price'] > $b['total_price']? -1 : 1);
                         });
                    $result = array(
                      "fo_wise"=>$cus_data,
                      "total_price"=> $total_price,
                      "order_total"=> $order_total
                    );
                    $results = $result;
                break;
            }

            return $results;
        }
        
    }
    //Multiple collections approve
    public function acceptMultipleCollectionDetail($Collections){

        $users = DB::table('gpff_users')
                ->where('user_id', $Collections->finance_user_id)
                ->First();

        $value = array(
            'collect_approve_id'=>$Collections->finance_user_id,
            'collect_approve_name'=> $users->firstname." ".$users->lastname,
            'approve_status'=> 1,
            'collect_approve_date'=>date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        );

        $update = DB::table('gpff_collection')
        ->whereIn('collection_id', $Collections->collection_id)
        ->where('doer_status', 1)
        ->where('check_status', 1)
        ->update($value);
        if($Collections->collection_for == 1){
            foreach($Collections->order_id as $orderId){
                $balance = DB::table('gpff_order')
                ->where('order_id', $orderId)
                ->First();

                if($balance->or_blc_price == 0){
                    $values = array(
                        'payment_status' =>1,
                        'updated_at'=>date('Y-m-d H:i:s')
                    );
                }else{
                    $values = array(
                        'payment_status' =>2,
                        'updated_at'=>date('Y-m-d H:i:s')
                    );
                }

                $updated = DB::table('gpff_order')        
                ->where('order_id', $orderId)
                ->update($values);
            }
        }else{
            foreach($Collections->order_id as $orderId){
                $balance = DB::table('gpff_vendor_order')
                ->where('vendor_order_id', $orderId)
                ->First();

                if($balance->or_blc_price == 0){
                    $values = array(
                        'payment_status' =>1,
                        'updated_at'=>date('Y-m-d H:i:s')
                    );
                }else{
                    $values = array(
                        'payment_status' =>2,
                        'updated_at'=>date('Y-m-d H:i:s')
                    );
                }

                $updated = DB::table('gpff_vendor_order')        
                ->where('vendor_order_id', $orderId)
                ->update($values);
            }
        }

        return 1;
    }

    public function getCustomerCollections($Collections){
        
        $region_id = $Collections->region_id;
        $details = DB::table('gpff_order as gpor')
        ->join('gpff_area as gpar', 'gpor.area_id', 'gpar.area_id')
        ->join('gpff_warehouse as gpwar', 'gpor.warehouse_id', 'gpwar.warehouse_id')
        // ->where('gpor.region_id', $region_id)
        ->where('gpor.order_status', 3)
        ->where('gpor.or_blc_price', '>',0)
        ->orderBy('gpor.updated_at','DESC');
        if($Collections->order_id !=''){
            $details = $details->where('gpor.order_id', $Collections->order_id);
            // $
        }
        if($Collections->start_date != ''){
            $details = $details->whereBetween(DB::RAW('date(gpor.created_at)'), [date($Collections->start_date), date($Collections->end_date)]);
        }

        if($Collections->customer_id != ''){
            $details = $details->whereIn('gpor.customer_id', $Collections->customer_id);
        }

        if($Collections->area_id != ''){
            $details = $details->whereIn('gpor.area_id', $Collections->area_id);
        }

        $or_details = $details->get(['gpor.created_at','gpor.order_id','gpor.or_tot_price', 'gpor.customer_id','gpor.customer_name','gpor.payment_type', 'gpor.payment_status','gpor.or_blc_price', 'gpor.invoice_no','gpor.area_id', 'gpor.region_id', 'gpor.field_officer_name','gpar.area_name', 'gpor.credit_lim', 'gpor.credit_valid_to','gpwar.warehouse_name']);

        return $or_details;
        
    }

    public function addDoerCollectionDetails($Collections){
        $user_name = DB::table('gpff_users')
            ->where('user_id', $Collections->collection_cr_id)
            ->First();
        $order_details = DB::table('gpff_order')
                        ->where('order_id',$Collections->order_id)
                        ->first();
        $values = array(
            "invoice_no"            =>$Collections->invoice_no,
            "order_id"              =>$Collections->order_id,
            "customer_id"           =>$Collections->customer_id,
            "customer_name"         =>$Collections->customer_name,
            "col_type"              =>$Collections->col_type,
            "collection_date"       =>date('Y-m-d H:i:s'),
            "collection_for"        =>$Collections->collection_for,
            "collection_status"     =>$Collections->collection_status,
            "collection_mode"       =>$Collections->collection_mode,
            "collection_amount"     =>$Collections->collection_amount,
            "collection_cr_id"      =>$Collections->collection_cr_id,
            "area_id"               =>$Collections->area_id,
            "region_id"             =>$Collections->region_id,
            "checker_id"            =>$Collections->checker_id,
            "checker_name"          =>$Collections->checker_name,
            "warehouse_id"          => $Collections->warehouse_id,
            "check_status"          =>0,
            "doer_id"               =>$Collections->collection_cr_id,
            "doer_name"             =>$user_name->firstname." ".$user_name->lastname,
            "doer_date"             =>date('Y-m-d H:i:s'),
            "doer_status"           =>1,
            "status"                =>$Collections->status,
            "bank_name"             =>$Collections->bank_name,
            "card_type"             =>$Collections->card_type,
            "card_no"               =>$Collections->card_no,
            "holder_name"           =>$Collections->holder_name,
            "account_no"            =>$Collections->account_no,
            "transaction_id"        =>$Collections->transaction_id,
            "cheque_no"             =>$Collections->cheque_no,
            "collection_cr_id"      =>$Collections->collection_cr_id,
            "collection_cr_name"    =>$user_name->firstname." ".$user_name->lastname,
            "collection_cr_date"    =>date('Y-m-d H:i:s'),
            "approve_status"        => 0,
            "created_at"            =>date('Y-m-d H:i:s'),
            "updated_at"            =>date('Y-m-d H:i:s')
        );

        $colectionid = DB::table('gpff_collection')
        ->insertGetId($values);
        $balance_amount = $order_details->or_blc_price - $Collections->collection_amount;
        if($balance_amount == 0){
            $payment = array(
                'or_blc_price'   => 0,
                'payment_status' => 1,
                'updated_at'     => date('Y-m-d H:i:s')
            );
        }else{
            $payment = array(
                'or_blc_price'   => $balance_amount,
                'updated_at'     => date('Y-m-d H:i:s')
            );
        }


        DB::table('gpff_order')
        ->where('order_id',$Collections->order_id)
        ->update($payment);
        
        return 1;
    }

    public function doerCollectionReport($Collections){
        
        $results = DB::table('gpff_collection')
                    ->orderBy('updated_at','DESC');
        if($Collections->order_id !=''){
            $results = $results->where('order_id', $Collections->order_id);            
        }
        if($Collections->start_date){
            $results = $results->whereBetween(DB::RAW('date(doer_date)'), [date($Collections->start_date), date($Collections->end_date)]);
        }
        // if($Collections->start_date != '' && $Collections->checker_id !=''){
        //     $results = $results->whereBetween(DB::RAW('date(checker_update_date)'), [date($Collections->start_date), date($Collections->end_date)]);
        // }
        if($Collections->region_id !=''){
            $results = $results->whereIn('region_id', $Collections->region_id);            
        }
        if($Collections->collection_for !=''){
            $results = $results->where('collection_for', $Collections->collection_for);            
        }
        if($Collections->customer_id !=''){
            $results = $results->whereIn('customer_id', $Collections->customer_id);            
        }
        if($Collections->doer_id !=''){
            $results = $results->whereIn('doer_id', $Collections->doer_id);            
        }
        if($Collections->checker_id !=''){
            $results = $results->whereIn('checker_id', $Collections->checker_id);            
        }
        if($Collections->checker_status !=''){
            $results = $results->whereIn('check_status', $Collections->checker_status);            
        }

        return $results->get();
        
    }

    public function financeCollectionReport($Collections){
        
        $results = DB::table('gpff_collection')
                    ->join('gpff_users','gpff_users.user_id','gpff_collection.finance_user_id')
                    ->where('check_status',1)
                    ->orderBy('gpff_collection.updated_at','DESC');
        if($Collections->order_id !=''){
            $results = $results->where('order_id', $Collections->order_id);            
        }
        if($Collections->collection_for !=''){
            $results = $results->where('collection_for', $Collections->collection_for);            
        }
        if($Collections->start_date){
            $results = $results->whereBetween(DB::RAW('date(checker_update_date)'), [date($Collections->start_date), date($Collections->end_date)]);
        }
        if($Collections->region_id !=''){
            $results = $results->whereIn('gpff_collection.region_id', $Collections->region_id);            
        }
        if($Collections->finance_user_id !=''){
            $results = $results->whereIn('finance_user_id', $Collections->finance_user_id);            
        }
        if($Collections->checker_id !=''){
            $results = $results->whereIn('checker_id', $Collections->checker_id);            
        }
        if($Collections->status !=''){
            $results = $results->whereIn('approve_status', $Collections->status);            
        }

        return $results->get();
        
    }
}