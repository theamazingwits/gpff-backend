<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
//use File;
use Mail;

require(base_path().'/app/Http/Middleware/File.php');

class FileController extends Controller
{
/////////////////////////////
//File Management Api Calls//
/////////////////////////////
	//Add Folder
    public function addFolder(Request $request){

    	$Files = new File();
        $Files->folder_name  	= $request->input('folder_name');
        $Files->parent_folder   = $request->input('parent_folder');
        $Files->folder_desc    	= $request->input('folder_desc');
        $Files->user_id  		= $request->input('user_id');
        $Files->region_id       = $request->input('region_id');
        $Files->branch_id       = $request->input('branch_id');

        $result = $Files->storeFolder($Files);
        if($result == 1){
            $response = response()->json(['status' => 'success' , 'message' => 'Folder Created successfully']);
        } else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Folder Created failed']);
        }
        return $response;
    }
    //Update Folder
    public function updateFolder(Request $request){

    	$Files = new File();
    	$Files->folder_id  		= $request->input('folder_id');
        $Files->folder_name  	= $request->input('folder_name');
        $Files->parent_folder   = $request->input('parent_folder');
        $Files->folder_desc    	= $request->input('folder_desc');
        $Files->user_id  		= $request->input('user_id');
       
        $result = $Files->updateFolder($Files);
        if($result == 1){
            $response = response()->json(['status' => 'success' , 'message' => 'Folder Details Updated successfully']);
        } else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Folder Details failed Updated']);
        }
        return $response;
    }

    //Delete Folder
    public function deleteFolder(Request $request){

    	$Files = new File();
    	$Files->folder_id  		= $request->input('folder_id');
        $Files->region_id       = $request->input('region_id');
       
        $result = $Files->deleteFolder($Files);
        if($result == 1){
            $response = response()->json(['status' => 'success' , 'message' => 'Folder Deleted successfully']);
        } else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Folder Deleted failed']);
        }
        return $response;
    }

    //Add File
    public function addFile(Request $request){

    	$Files = new File();
        $Files->user_id  		= $request->input('user_id');
        $Files->folder_id   	= $request->input('folder_id');
        $Files->file_name    	= $request->input('file_name');
        $Files->file_extension  = $request->input('file_extension');
        $Files->region_id       = $request->input('region_id');
        $Files->branch_id       = $request->input('branch_id');
       
        $result = $Files->addFile($Files);
        if($result == 1){
            $response = response()->json(['status' => 'success' , 'message' => 'File Added successfully']);
        } else{
            $response = response()->json(['status' => 'failure' , 'message' => 'File Added failed']);
        }
        return $response;
    }
    //Update File
    public function updateFile(Request $request){

    	$Files = new File();
    	$Files->user_id  		= $request->input('user_id');
        $Files->folder_id   	= $request->input('folder_id');
        $Files->file_id  		= $request->input('file_id');
        $Files->file_name    	= $request->input('file_name');
        $Files->file_extension  = $request->input('file_extension');
       
        $result = $Files->updateFile($Files);
        if($result == 1){
            $response = response()->json(['status' => 'success' , 'message' => 'Folder Updated successfully']);
        } else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Folder Updated failed']);
        }
        return $response;
    }

    //Delete File
    public function deleteFile(Request $request){

    	$Files = new File();
    	$Files->file_id  = $request->input('file_id');
        $Files->region_id       = $request->input('region_id');

        $result = $Files->deleteFile($Files);
        if($result == 1){
            $response = response()->json(['status' => 'success' , 'message' => 'file Deleted successfully']);
        } else{
            $response = response()->json(['status' => 'failure' , 'message' => 'file Deleted failed']);
        }
        return $response;
    }

    // Get Folder And Files Details
    public function getFolderFiles(Request $request)
    {
        $Files = new File();
        $Files->folder_id  	= $request->input('folder_id');
        $Files->region_id   = $request->input('region_id');
        $Files->type  		= $request->input('type');

        $result = $Files->getFolderFiles($Files);
        if($result){
            $response = response()->json(['status' => 'success' , 'message' => '', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Invalid Folder & Files details']); 
        }
        return $response;
    } 

    // Get All Files Details
    public function getAllFiles(Request $request)
    {
        $Files = new File();
        $Files->region_id   = $request->input('region_id');
        $result = $Files->getAllFiles($Files);
        if($result){
            $response = response()->json(['status' => 'success' , 'message' => '', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Invalid Folder & Files details']); 
        }
        return $response;
    } 
}
