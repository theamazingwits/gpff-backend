<!DOCTYPE html>
<html lang="en">
<head>
	<title>Table V01</title>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css"/>
	<style>#logo {
	float: left;
	margin-top: 8px;
	margin-left: 30%;
}
body{

	background-color: #f5f5f5;
}

.container{
	padding:0em 4em;
	border: 1px solid #f5f5f5;
	background-color: #ffffff;
    margin: 2% 12%;
    min-height: 55em;
    box-shadow: 0px 2px 1px -1px rgba(0, 0, 0, 0.2), 0px 1px 1px 0px rgba(0, 0, 0, 0.14), 0px 1px 3px 0px rgba(0, 0, 0, 0.12);
    border-radius: 10px;
}

#logo img {
	height: 70px;
}

.display-1 {
	.font(@font-size-display-1, @line-height-display-1, @letter-spacing-display-1);
	font-weight: @font-weight-display-1;
	color: @font-color-display-1;
	text-transform: inherit;
}

h2 {
	text-align: center;
	padding-top: 20px 0;
}

.table-bordered {
	border: 1px solid #ddd !important;
}

table caption {
	padding: .5em 0;
}

table tfoot tr td {
	text-align: center !important;
}

@media (max-width: 39.9375em) {
	.tablesaw-stack tbody tr:not(:last-child) {
		border-bottom: 2px solid #0B0B0D;
	}
}

.p {
	text-align: center;
	padding-top: 140px;
	font-size: 14px;
}

.d-flex{
	display:flex!important;
}
.mb-2{
	 margin-bottom: 2em;
	}  
	.mb-1{
	 margin-bottom: 1em;
	}

</style>
</head>
<body>
	<header class="clearfix"> {{-- <div id="logo"></div>--}}

<!-- <h1 class="display-1" style="padding-top: 1%; padding-left: 17%;">Samples Report</h1>-->
</header>
	<div class="container">
		<div class="card">

		

		<center>
			<img src="https://aw-gpff.s3.ap-south-1.amazonaws.com/TARGETSFA1.png" height="1" alt="TargetSFA" class="mt-5" style="margin-top:3em;padding-bottom: 5%;width: 25%;height: 9em;padding-left: 3%"/>
			<div style="position: absolute;top: 6em">
				<div class="row" >
					<div class="col-md-5">PurchaseId </div>
						<div class="col-md-2">:</div>
					<div class="col-md-4">{{ $purchase_id }}</div>
				</div>
				<div class="row" >
					<div class="col-md-5 pl-0" style="padding-left:0px;">Date </div>
						<div class="col-md-2">:</div>
					<div class="col-md-4">{{ $purchased_date }}</div>
				</div>
			</div>
			
		</center>

		

		<div class="row">
			<div class="col-md-6">
				<div class="info">
					<h4 align="left">Supplier Details</h4>

					<div class="row">
						<div class="col-md-3">Supplier code </div>
						<div class="col-md-1">:</div>
						<div class="col-md-6">{{$supplier_code}}</div>
					</div>

					<div class="row">
						<div class="col-md-3">Supplier Name  </div>
						<div class="col-md-1">:</div>
						<div class="col-md-6">{{$supplier_name}}</div>
					</div>

					<div class="row">
						<div class="col-md-3">Contact Person </div>
						<div class="col-md-1">:</div>
						<div class="col-md-6">{{$contact_person}}</div>
					</div>

					<div class="row">
						<div class="col-md-3">Contact Number </div>
						<div class="col-md-1">:</div>
						<div class="col-md-6">{{$contact_number}}</div>
					</div>

					<div class="row">
						<div class="col-md-3">Email </div>
						<div class="col-md-1">:</div>
						<div class="col-md-6">{{$email}}</div>
					</div>

					<div class="row">
						<div class="col-md-3">Supplier Licence </div>
						<div class="col-md-1">:</div>
						<div class="col-md-6">{{$supplier_licence}}</div>
					</div>
					 
                </div>
			</div>

			<div class="col-md-6 text-right">
				<div class="info">
                    <div class="title">GLOBAL PHARMA INC.</div>
                    <div class="address">No.2,3rd F, 4th Street,Ganga Nagar,Chennai-600 024
                    </div>
                    <div class="phone">+91 44 2744 4408</div>
                    <div class="email">vengatesh@global-pharma.com</div>
                    <div class="website">www.global-pharma.com</div>
                </div>
			</div>
		</div>

		<h4 align="left"></h4>
		<div class="row">
			<div class="col-xs-12">
				<div class="mt-2 text-center"><h3 class=" mb-1">Product Details</h3></div>
				<table summary="This table shows how to create responsive tables using Tablesaw's functionality" class="table table-bordered table-hover tablesaw tablesaw-stack" data-tablesaw-mode="stack">
					<thead>
						<tr>
							<th>S.NO</th>
							<th>Product Name</th>
							<th>Quantity</th>
							<th>Unit price</th>
							<th>Total price</th>
						</tr>
					</thead>
					<tbody>

						@foreach($product_details_array  as $key=> $data)

						<tr>
							<td class="column0">{{ ++$key }} </td>
							<td class="column1">{{  $data->product_genericname }} </td>
							<td class="column2">{{  $data->quantity }} </td>
							<td class="column3">{{  $data->unit_price }} </td>
							<td class="column4">{{  $data->total_price }} </td>
						</tr>
						 @endforeach
					</tbody>
				</table>
			</div>
		</div>

		<div class="row">
			<div class="col-md-6"></div>
			<div class="col-md-6">
				<div class="row">
					<div class="col-md-6">
						<h4>Grand Total</h4><span>:</span>
					</div>
					<div class="col-md-3 text-right">
						<h4>Rs.{{$Grand_total}}</h4>
					</div>
				</div>
					
			</div>
		</div>
	</div>
</div>	
</body>
</html>