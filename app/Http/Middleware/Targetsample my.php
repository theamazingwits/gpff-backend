<?php

namespace App\Http\Controllers;

use Closure;
use DB;
use Mail;
use Carbon\Carbon;
use PDF;
use Illuminate\Support\Facades\Storage;

class Report
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        return $next($request);
    }


////////////////////////////
//Report Managment Api Calls//
////////////////////////////
    //Get Current Year Sales Report
    public function getCurrentYearSalesReport()
    {   
        $orders = DB::table('gpff_order as gorder')
                ->join('gpff_order_list as gorderlist','gorderlist.order_id','gorder.order_id')
                ->join('gpff_branch as gbranch','gorder.branch_id','gbranch.branch_id')
                ->join('gpff_region as gregion','gorder.region_id','gregion.region_id')
                ->join('gpff_area as garea','gorder.area_id','garea.area_id')
                ->whereYear('gorder.created_at', date('Y'))
                ->orderBy('gorder.created_at', 'DESC')
                ->get(['gorder.order_id','gorder.order_date','gorder.customer_name','gorderlist.product_genericname','gorderlist.product_qty','gorder.field_officer_name','gorderlist.product_type','gbranch.branch_name','gregion.region_name','garea.area_name','gorderlist.product_tot_price']);

        //Promotional Product Chart Count
        $promotional = [];
        for ($i=1; $i<=12; $i++) {

            $res_promotional = DB::table('gpff_order_list')
                      ->whereYear('created_at', date('Y'))
                      ->whereMonth('created_at', $i)
                      ->where('product_type','1')
                      ->sum('product_tot_price');
            
            array_push($promotional, $res_promotional);

        }

        //Generic Product Chart Count
        $generic = [];
        for ($i=1; $i<=12; $i++) {

            $res_generic = DB::table('gpff_order_list')
                      ->whereYear('created_at', date('Y'))
                      ->whereMonth('created_at', $i)
                      ->where('product_type','2')
                      ->sum('product_tot_price');

            array_push($generic, $res_generic);
        }

        //Promotional Product Total Amount
        $promotional_amt = DB::table('gpff_order_list')
                      ->whereYear('created_at', date('Y'))
                      ->where('product_type','1')
                      ->sum('product_tot_price');

        //General Product Total Amount
        $generic_amt = DB::table('gpff_order_list')
                      ->whereYear('created_at', date('Y'))
                      ->where('product_type','2')
                      ->sum('product_tot_price');


        //Promotional Product Chart Details    
        $promotional_prod_count = [];
        $promotional_prod =DB::table('gpff_order_list')
                      ->whereYear('created_at', date('Y'))
                      ->where('product_type','1')
                      ->groupBy('product_id')
                      ->get(['product_id']);

        foreach ($promotional_prod as $value) {

          $promo = [];

          $name =     DB::table('gpff_product')
                      ->where('product_id',$value->product_id)
                      ->First();

          $promo['product_genericname'] = $name->product_genericname;
          
          $promo['product_qty'] = DB::table('gpff_order_list')
                      ->where('product_id',$value->product_id)
                      ->sum('product_qty');

          $promo['product_price'] = DB::table('gpff_order_list')
                      ->where('product_id',$value->product_id)
                      ->sum('product_tot_price');

          array_push($promotional_prod_count, $promo);
        }


        //Generic Product Chart Details    
        $generic_prod_count = [];
        $generic_prod =DB::table('gpff_order_list')
                      ->whereYear('created_at', date('Y'))
                      ->where('product_type','2')
                      ->groupBy('product_id')
                      ->get(['product_id']);

        foreach ($generic_prod as $value) {

          $generic = [];

          $name =     DB::table('gpff_product')
                      ->where('product_id',$value->product_id)
                      ->First();

          $generic['product_genericname'] = $name->product_genericname;
          
          $generic['product_qty'] = DB::table('gpff_order_list')
                      ->where('product_id',$value->product_id)
                      ->sum('product_qty');

          $generic['product_price'] = DB::table('gpff_order_list')
                      ->where('product_id',$value->product_id)
                      ->sum('product_tot_price');

          array_push($generic_prod_count, $generic);
        }

        $result = array_merge(
                              ['order_details'=>$orders->toArray()], 
                              ['promotional_chart_count' => $promotional],
                              ['generic_chart_count' => $generic],
                              ['promotional_total_amount' => $promotional_amt],
                              ['generic_total_amount' => $generic_amt],
                              ['promotional_prod_det' => $promotional_prod_count],
                              ['generic_prod_det' => $generic_prod]
                          );

        return $result;

    }


  
    //Get Filter Based Sales Report
    public function getFilterSalesReport($report)
    {   
        $orders = DB::table('gpff_order as gorder')
                ->join('gpff_order_list as gorderlist','gorderlist.order_id','gorder.order_id')
                ->join('gpff_branch as gbranch','gorder.branch_id','gbranch.branch_id')
                ->join('gpff_region as gregion','gorder.region_id','gregion.region_id')
                ->join('gpff_area as garea','gorder.area_id','garea.area_id')
                ->orderBy('gorder.created_at', 'DESC');



        $promotional_res = [];
        $generic_res = [];

        //Promotional Product Chart Details
        $promotional_prod =  DB::table('gpff_order as gorder')
                    ->join('gpff_order_list as gorderlist','gorderlist.order_id','gorder.order_id');

        //Generic Product Chart Details
        $generic_prod =  DB::table('gpff_order as gorder')
                    ->join('gpff_order_list as gorderlist','gorderlist.order_id','gorder.order_id');

     
        if($report->branch_id != ''){
            $orders = $orders->whereIn('gorder.branch_id',$report->branch_id);
            $promotional_prod = $promotional_prod->whereIn('gorder.branch_id',$report->branch_id);
            $generic_prod = $generic_prod->whereIn('gorder.branch_id',$report->branch_id);
        }
        if($report->region_id != ''){
             $orders = $orders->whereIn('gorder.region_id',$report->region_id);
             $promotional_prod = $promotional_prod->whereIn('gorder.region_id',$report->region_id);
             $generic_prod = $generic_prod->whereIn('gorder.region_id',$report->region_id);
        }
        if($report->area_id != ''){
            $orders = $orders->whereIn('gorder.area_id',$report->area_id);
            $promotional_prod = $promotional_prod->whereIn('gorder.area_id',$report->area_id);
            $generic_prod = $generic_prod->whereIn('gorder.area_id',$report->area_id);
        }
        if($report->customer_id != ''){
            $orders = $orders->whereIn('gorder.customer_id',$report->customer_id);
            $promotional_prod = $promotional_prod->whereIn('gorder.customer_id',$report->customer_id);
            $generic_prod = $generic_prod->whereIn('gorder.customer_id',$report->customer_id);
        }
        if($report->field_officer_id != ''){
            $orders = $orders->whereIn('gorder.field_officer_id',$report->field_officer_id); 
            $promotional_prod = $promotional_prod->whereIn('gorder.field_officer_id',$report->field_officer_id); 
            $generic_prod = $generic_prod->whereIn('gorder.field_officer_id',$report->field_officer_id); 
        }
        if($report->product_id != ''){
            $orders = $orders->whereIn('gorderlist.product_id', $report->product_id);
            $promotional_prod = $promotional_prod->whereIn('gorderlist.product_id', $report->product_id);
            $generic_prod = $generic_prod->whereIn('gorderlist.product_id', $report->product_id);
        }
        if($report->year != ''){
            $orders = $orders->whereYear("gorder.created_at",$report->year);
            $promotional_prod = $promotional_prod->whereYear("gorder.created_at",$report->year);
            $generic_prod = $generic_prod->whereYear("gorder.created_at",$report->year);
        }
        if($report->months != ''){
            $orders = $orders->whereIn(DB::raw('MONTH(gorder.created_at)'), $report->months);
            $promotional_prod = $promotional_prod->whereIn(DB::raw('MONTH(gorder.created_at)'), $report->months);
            $generic_prod = $generic_prod->whereIn(DB::raw('MONTH(gorder.created_at)'), $report->months);
        }

        //$res = ['1','2','3','4','5','6','7','8','9','10','11','12'];

        foreach ($report->months as $value)
        {  
           $promotional['Month'] = $value;
           $promotional['count'] =  DB::table('gpff_order as gorder')
                ->join('gpff_order_list as gorderlist','gorderlist.order_id','gorder.order_id')
                ->where(
                    DB::raw('MONTH(gorderlist.created_at)'), $value)
                ->where('gorderlist.product_type','1')
                ->where(function($query) use ($report) {
                    if($report->branch_id != '') {
                        $query->whereIn('gorder.branch_id',$report->branch_id);
                    }
                 })
                ->where(function($query) use ($report) {
                    if($report->region_id != '') {
                        $query->whereIn('gorder.region_id',$report->region_id);
                    }
                 })
                ->where(function($query) use ($report) {
                    if($report->area_id != '') {
                        $query->whereIn('gorder.area_id', $report->area_id);
                    }
                 })
                ->where(function($query) use ($report) {
                    if($report->customer_id != '') {
                        $query->whereIn('gorder.customer_id', $report->customer_id);
                    }
                 })
                ->where(function($query) use ($report) {
                    if($report->field_officer_id != '') {
                        $query->whereIn('gorder.field_officer_id', $report->field_officer_id);
                    }
                 })
                ->where(function($query) use ($report) {
                    if($report->product_id != '') {
                        $query->whereIn('gorderlist.product_id', $report->product_id);
                    }
                 })
                ->where(function($query) use ($report) {
                    if($report->year != '') {
                        $query->whereYear("gorder.created_at",$report->year);
                    }
                 })
            ->sum('gorderlist.product_tot_price');

             array_push($promotional_res, $promotional);
        }


         foreach ($report->months as $value)
        {  
           $generic['Month'] = $value;
           $generic['count'] =  DB::table('gpff_order as gorder')
                ->join('gpff_order_list as gorderlist','gorderlist.order_id','gorder.order_id')
                ->where(
                    DB::raw('MONTH(gorderlist.created_at)'), $value)
                ->where('gorderlist.product_type','2')
                ->where(function($query) use ($report) {
                    if($report->branch_id != '') {
                        $query->whereIn('gorder.branch_id',$report->branch_id);
                    }
                 })
                ->where(function($query) use ($report) {
                    if($report->region_id != '') {
                        $query->whereIn('gorder.region_id',$report->region_id);
                    }
                 })
                ->where(function($query) use ($report) {
                    if($report->area_id != '') {
                        $query->whereIn('gorder.area_id', $report->area_id);
                    }
                 })
                ->where(function($query) use ($report) {
                    if($report->customer_id != '') {
                        $query->whereIn('gorder.customer_id', $report->customer_id);
                    }
                 })
                ->where(function($query) use ($report) {
                    if($report->field_officer_id != '') {
                        $query->whereIn('gorder.field_officer_id', $report->field_officer_id);
                    }
                 })
                ->where(function($query) use ($report) {
                    if($report->product_id != '') {
                        $query->whereIn('gorderlist.product_id', $report->product_id);
                    }
                 })
                ->where(function($query) use ($report) {
                    if($report->year != '') {
                        $query->whereYear("gorder.created_at",$report->year);
                    }
                 })
            ->sum('gorderlist.product_tot_price');

             array_push($generic_res, $generic);
        }
        
        //Promotional Product Total Amount
        $promotional_amt =  DB::table('gpff_order as gorder')
                ->join('gpff_order_list as gorderlist','gorderlist.order_id','gorder.order_id')
                ->whereIn(DB::raw('MONTH(gorder.created_at)'), $report->months)
                ->where('gorderlist.product_type','1')
                ->where(function($query) use ($report) {
                    if($report->branch_id != '') {
                        $query->whereIn('gorder.branch_id',$report->branch_id);
                    }
                 })
                ->where(function($query) use ($report) {
                    if($report->region_id != '') {
                        $query->whereIn('gorder.region_id',$report->region_id);
                    }
                 })
                ->where(function($query) use ($report) {
                    if($report->area_id != '') {
                        $query->whereIn('gorder.area_id', $report->area_id);
                    }
                 })
                ->where(function($query) use ($report) {
                    if($report->customer_id != '') {
                        $query->whereIn('gorder.customer_id', $report->customer_id);
                    }
                 })
                ->where(function($query) use ($report) {
                    if($report->field_officer_id != '') {
                        $query->whereIn('gorder.field_officer_id', $report->field_officer_id);
                    }
                 })
                ->where(function($query) use ($report) {
                    if($report->product_id != '') {
                        $query->whereIn('gorderlist.product_id', $report->product_id);
                    }
                 })
                ->where(function($query) use ($report) {
                    if($report->year != '') {
                        $query->whereYear("gorder.created_at",$report->year);
                    }
                 })
            ->sum('gorderlist.product_tot_price');

        //Generic Product Total Amount
        $generic_amt =  DB::table('gpff_order as gorder')
            ->join('gpff_order_list as gorderlist','gorderlist.order_id','gorder.order_id')
            ->whereIn(DB::raw('MONTH(gorder.created_at)'), $report->months)
            ->where('gorderlist.product_type','2')
            ->where(function($query) use ($report) {
                if($report->branch_id != '') {
                    $query->whereIn('gorder.branch_id',$report->branch_id);
                }
             })
            ->where(function($query) use ($report) {
                if($report->region_id != '') {
                    $query->whereIn('gorder.region_id',$report->region_id);
                }
             })
            ->where(function($query) use ($report) {
                if($report->area_id != '') {
                    $query->whereIn('gorder.area_id', $report->area_id);
                }
             })
            ->where(function($query) use ($report) {
                if($report->customer_id != '') {
                    $query->whereIn('gorder.customer_id', $report->customer_id);
                }
             })
            ->where(function($query) use ($report) {
                if($report->field_officer_id != '') {
                    $query->whereIn('gorder.field_officer_id', $report->field_officer_id);
                }
             })
            ->where(function($query) use ($report) {
                if($report->product_id != '') {
                    $query->whereIn('gorderlist.product_id', $report->product_id);
                }
             })
            ->where(function($query) use ($report) {
                if($report->year != '') {
                    $query->whereYear("gorder.created_at",$report->year);
                }
             })
        ->sum('gorderlist.product_tot_price');


        $order_det = $orders->get(['gorder.order_id','gorder.order_date','gorder.customer_name','gorderlist.product_genericname','gorderlist.product_qty','gorder.field_officer_name','gorderlist.product_type','gbranch.branch_name','gregion.region_name','garea.area_name','gorderlist.product_tot_price']);



             //Promotional Product Chart Details    
        $promotional_prod_count = [];
        $promotional_prod = $promotional_prod
                      ->where('gorderlist.product_type','1')
                      ->groupBy('gorderlist.product_id')
                      ->get(['gorderlist.product_id']);

        foreach ($promotional_prod as $value) {

          $promo = [];

          $name =     DB::table('gpff_product')
                      ->where('product_id',$value->product_id)
                      ->First();

          $promo['product_genericname'] = $name->product_genericname;
          
          $promo['product_qty'] = DB::table('gpff_order_list')
                      ->where('product_id',$value->product_id)
                      ->sum('product_qty');

          $promo['product_price'] = DB::table('gpff_order_list')
                      ->where('product_id',$value->product_id)
                      ->sum('product_tot_price');

          array_push($promotional_prod_count, $promo);
        }


        //Generic Product Chart Details    
        $generic_prod_count = [];
        $generic_prod = $generic_prod
                      ->where('gorderlist.product_type','2')
                      ->groupBy('gorderlist.product_id')
                      ->get(['gorderlist.product_id']);

        foreach ($generic_prod as $value) {

          $generic = [];

          $name =     DB::table('gpff_product')
                      ->where('product_id',$value->product_id)
                      ->First();

          $generic['product_genericname'] = $name->product_genericname;
          
          $generic['product_qty'] = DB::table('gpff_order_list')
                      ->where('product_id',$value->product_id)
                      ->sum('product_qty');

          $generic['product_price'] = DB::table('gpff_order_list')
                      ->where('product_id',$value->product_id)
                      ->sum('product_tot_price');

          array_push($generic_prod_count, $generic);
        }


        $result = array_merge(
            ['order_details'=>$order_det->toArray()], 
            ['promotional_chart_count' => $promotional_res],
            ['generic_chart_count' => $generic_res],
            ['promotional_total_amount' => $promotional_amt],
            ['generic_total_amount' => $generic_amt],
            ['promotional_prod_det' => $promotional_prod_count],
            ['generic_prod_det' => $generic_prod_count]
        );

        return $result;
    }



    //Get Current Year Coverage Report
    public function getCurrentYearCoverageReport()
    {   
        $task = DB::table('gpff_task as gtask')

                ->join('gpff_customer as gcustomer','gcustomer.customer_id','gtask.customer_id')
                ->join('gpff_users as gusers','gtask.field_officer_id','gusers.user_id')
                ->join('gpff_branch as gbranch','gbranch.branch_id','gtask.branch_id')
                ->join('gpff_region as gregion','gtask.region_id','gregion.region_id')
                ->join('gpff_area as garea','gtask.area_id','garea.area_id')
                ->whereYear('gtask.task_date_time', date('Y'))
                ->orderBy('gtask.task_date_time', 'DESC')
                ->get(['gtask.task_id','gtask.task_title','gcustomer.customer_name','gtask.task_date_time','gtask.task_status','gusers.firstname','gusers.lastname','gbranch.branch_name','gregion.region_name','garea.area_name','gtask.customer_type','gcustomer.customer_location']);
       
        //Covered Task List Count
        $covered_task = DB::table('gpff_task')
                      ->whereYear('created_at', date('Y'))
                      ->where('task_status','3')
                      ->count('task_id');

        //UnCovered Task List count
        $uncovered_task = DB::table('gpff_task')
                      ->whereYear('created_at', date('Y'))
                      ->where('task_status','2')
                      ->count('task_id');

        //Inprogress Task List count
        $inprogress_task = DB::table('gpff_task')
                      ->whereYear('created_at', date('Y'))
                      ->where('task_status','1')
                      ->count('task_id');

        //Not Started Task List count
        $notstarted_task = DB::table('gpff_task')
                      ->whereYear('created_at', date('Y'))
                      ->where('task_status','0')
                      ->count('task_id');

        //Total Pharamacy Visited Count
        $totalpharamact_visit = DB::table('gpff_task')
                      ->whereYear('created_at', date('Y'))
                      ->where('task_status','3')
                      ->where('customer_type',2)
                      ->groupBy('customer_id')
                      ->count('task_id');

        //Unvisited Pharamacy Visited Count
        $totalpharamact_unvisit = DB::table('gpff_task')
                      ->whereYear('created_at', date('Y'))
                      ->where('task_status','2')
                      ->where('customer_type',2)
                      ->groupBy('customer_id')
                      ->count('task_id');


        //Doctor Visited in Bar Chart Count
        $DoctorBarvisit = [];
        for ($i=1; $i<=12; $i++) {
            
            $totalDocvisiCount = DB::table('gpff_task')
                      ->whereYear('created_at', date('Y'))
                      ->whereMonth('created_at', $i)
                      ->where('task_status',3)
                      ->where('customer_type',1)
                      ->count();
            
            array_push($DoctorBarvisit, $totalDocvisiCount);
        }
        //Pharmacy Visited in Bar Chart Count
        $PharmacyBarvisit = [];
        for ($i=1; $i<=12; $i++) {
        
            $totalPharvisiCount = DB::table('gpff_task')
                      ->whereYear('created_at', date('Y'))
                      ->whereMonth('created_at', $i)
                      ->where('task_status',3)
                      ->where('customer_type',2)
                      ->count();
            
            array_push($PharmacyBarvisit, $totalPharvisiCount);
        }
        //Hospital Visited in Bar Chart Count
        $HospitalBarvisit = [];
        for ($i=1; $i<=12; $i++) {
        
            $totalHospivisiCount = DB::table('gpff_task')
                      ->whereYear('created_at', date('Y'))
                      ->whereMonth('created_at', $i)
                      ->where('task_status',3)
                      ->where('customer_type',3)
                      ->count();
            
            array_push($HospitalBarvisit, $totalHospivisiCount);
        }

        //Doctor Un Visited in Bar Chart Count
        $DoctorBarunvisit = [];
        for ($i=1; $i<=12; $i++) {
            
            $totalDocvisiCount = DB::table('gpff_task')
                      ->whereYear('created_at', date('Y'))
                      ->whereMonth('created_at', $i)
                      ->where('task_status',0)
                      ->where('customer_type',1)
                      ->count();
            
            array_push($DoctorBarunvisit, $totalDocvisiCount);
        }
        //Pharmacy Un Visited in Bar Chart Count
        $PharmacyBarunvisit = [];
        for ($i=1; $i<=12; $i++) {
        
            $totalPharvisiCount = DB::table('gpff_task')
                      ->whereYear('created_at', date('Y'))
                      ->whereMonth('created_at', $i)
                      ->where('task_status',0)
                      ->where('customer_type',2)
                      ->count();
            
            array_push($PharmacyBarunvisit, $totalPharvisiCount);
        }
        //Hospital Un Visited in Bar Chart Count
        $HospitalBarunvisit = [];
        for ($i=1; $i<=12; $i++) {
        
            $totalHospivisiCount = DB::table('gpff_task')
                      ->whereYear('created_at', date('Y'))
                      ->whereMonth('created_at', $i)
                      ->where('task_status',0)
                      ->where('customer_type',3)
                      ->count();
            
            array_push($HospitalBarunvisit, $totalHospivisiCount);
        }

       
        //Doctor Based Visited in Pie Chart Count
        $DoctorBasvisit = [];
        $DoctorBas = DB::table('gpff_task')
                      ->whereYear('created_at', date('Y'))
                      ->where('task_status',3)
                      ->where('customer_type',1)
                      ->distinct('customer_id')
                      ->get(['customer_id']);

        foreach ($DoctorBas as $value) {

          $Doctor = [];

          $name =     DB::table('gpff_customer')
                      ->where('customer_id',$value->customer_id)
                      ->First();

          $Doctor['Name'] = $name->customer_name;
          
          $Doctor['Count'] = DB::table('gpff_task')
                      ->where('customer_id',$value->customer_id)
                      ->count();

          array_push($DoctorBasvisit, $Doctor);
        }

        //Pharmachy Based Visited in Pie Chart Count
        $PharmachyBasvisit = [];
        $PharmachyBas = DB::table('gpff_task')
                      ->whereYear('created_at', date('Y'))
                      ->where('task_status',3)
                      ->where('customer_type',2)
                      ->distinct('customer_id')
                      ->get(['customer_id']);

        foreach ($PharmachyBas as $value) {

          $Pharmacy = [];

          $name =     DB::table('gpff_customer')
                      ->where('customer_id',$value->customer_id)
                      ->First();

          $Pharmacy['Name'] = $name->customer_name;
          
          $Pharmacy['Count'] = DB::table('gpff_task')
                      ->where('customer_id',$value->customer_id)
                      ->count();

          array_push($PharmachyBasvisit, $Pharmacy);
        }

        //Hospital Based Visited in Pie Chart Count
        $HospitalBasvisit = [];
        $HospitalBas = DB::table('gpff_task')
                      ->whereYear('created_at', date('Y'))
                      ->where('task_status',3)
                      ->where('customer_type',3)
                      ->distinct('customer_id')
                      ->get(['customer_id']);

        foreach ($HospitalBas as $value) {

          $Hospital = [];

          $name =     DB::table('gpff_customer')
                      ->where('customer_id',$value->customer_id)
                      ->First();

          $Hospital['Name'] = $name->customer_name;
          
          $Hospital['Count'] = DB::table('gpff_task')
                      ->where('customer_id',$value->customer_id)
                      ->count();

          array_push($HospitalBasvisit, $Hospital);
        }

        $result = array_merge(
                              ['task_details' => $task->toArray()], 
                              ['covered_chart_count' => $covered_task],
                              ['uncovered_chart_count' => $uncovered_task],
                              ['inprogress_chart_count' => $inprogress_task],
                              ['notstarted_chart_count' => $notstarted_task],
                              ['totalpharamacy_visit' => $totalpharamact_visit],
                              ['totalpharamacy_unvisit' => $totalpharamact_unvisit],
                              ['doctorbased_visit' => $DoctorBasvisit],
                              ['pharmacybased_visit' => $PharmachyBasvisit],
                              ['hospitalbased_visit' => $HospitalBasvisit],
                              ['doctorbased_visit_chart_count' => $DoctorBarvisit],
                              ['pharmacybased_visit_chart_count' => $PharmacyBarvisit],
                              ['hospitalbased_visit_chart_count' => $HospitalBarvisit],
                              ['doctorbased_unvisit_chart_count' => $DoctorBarunvisit],
                              ['pharmacybased_unvisit_chart_count' => $PharmacyBarunvisit],
                              ['hospitalbased_unvisit_chart_count' => $HospitalBarunvisit]
                          );

        return $result;

    }


     //Get Coverage Filter Based Report
    public function getCoverageFilterReport($report)
    {   
        $task = DB::table('gpff_task as gtask')
                ->join('gpff_customer as gcustomer','gcustomer.customer_id','gtask.customer_id')
                ->join('gpff_users as gusers','gtask.field_officer_id','gusers.user_id')
                ->join('gpff_branch as gbranch','gbranch.branch_id','gtask.branch_id')
                ->join('gpff_region as gregion','gtask.region_id','gregion.region_id')
                ->join('gpff_area as garea','gtask.area_id','garea.area_id')
                ->orderBy('gtask.task_date_time', 'DESC');

        $covered_task =  DB::table('gpff_task');
        $uncovered_task = DB::table('gpff_task');
        $inprogress_task = DB::table('gpff_task');
        $notstarted_task = DB::table('gpff_task');
        $totalpharamact_visit = DB::table('gpff_task');
        $totalpharamact_unvisit = DB::table('gpff_task');
        $DoctorBas = DB::table('gpff_task');
        $PharmachyBas = DB::table('gpff_task');


        if($report->branch_id != ''){
            $task = $task->whereIn('gtask.branch_id',$report->branch_id);
            $covered_task = $covered_task->whereIn('branch_id',$report->branch_id);
            $uncovered_task = $uncovered_task->whereIn('branch_id',$report->branch_id);
            $inprogress_task = $inprogress_task->whereIn('branch_id',$report->branch_id);
            $notstarted_task = $notstarted_task->whereIn('branch_id',$report->branch_id);
            $totalpharamact_visit = $totalpharamact_visit->whereIn('branch_id',$report->branch_id);
            $totalpharamact_unvisit = $totalpharamact_unvisit->whereIn('branch_id',$report->branch_id);
            $DoctorBas = $DoctorBas->whereIn('branch_id',$report->branch_id);
            $PharmachyBas = $PharmachyBas->whereIn('branch_id',$report->branch_id);
        }
        if($report->region_id != ''){
            $task = $task->whereIn('gtask.region_id',$report->region_id);
            $covered_task = $covered_task->whereIn('region_id',$report->region_id);
            $uncovered_task = $uncovered_task->whereIn('region_id',$report->region_id);
            $inprogress_task = $inprogress_task->whereIn('region_id',$report->region_id);
            $notstarted_task = $notstarted_task->whereIn('region_id',$report->region_id);
            $totalpharamact_visit = $totalpharamact_visit->whereIn('region_id',$report->region_id);
            $totalpharamact_unvisit = $totalpharamact_unvisit->whereIn('region_id',$report->region_id);
            $DoctorBas = $DoctorBas->whereIn('region_id',$report->region_id);
            $PharmachyBas = $PharmachyBas->whereIn('region_id',$report->region_id);
        }
        if($report->area_id != ''){
            $task = $task->whereIn('gtask.area_id',$report->area_id);
            $covered_task = $covered_task->whereIn('area_id',$report->area_id);
            $uncovered_task = $uncovered_task->whereIn('area_id',$report->area_id);
            $inprogress_task = $inprogress_task->whereIn('area_id',$report->area_id);
            $notstarted_task = $notstarted_task->whereIn('area_id',$report->area_id);
            $totalpharamact_visit = $totalpharamact_visit->whereIn('area_id',$report->area_id);
            $totalpharamact_unvisit = $totalpharamact_unvisit->whereIn('area_id',$report->area_id);
            $DoctorBas = $DoctorBas->whereIn('area_id',$report->area_id);
            $PharmachyBas = $PharmachyBas->whereIn('area_id',$report->area_id);

        }
        if($report->field_officer_id != ''){
            $task = $task->whereIn('gtask.field_officer_id',$report->field_officer_id); 
            $covered_task = $covered_task->whereIn('field_officer_id',$report->field_officer_id); 
            $uncovered_task = $uncovered_task->whereIn('field_officer_id', $report->field_officer_id);
            $inprogress_task = $inprogress_task->whereIn('field_officer_id',$report->field_officer_id);
            $notstarted_task = $notstarted_task->whereIn('field_officer_id',$report->field_officer_id);
            $totalpharamact_visit = $totalpharamact_visit->whereIn('field_officer_id',$report->field_officer_id);
            $totalpharamact_unvisit = $totalpharamact_unvisit->whereIn('field_officer_id',$report->field_officer_id);
            $DoctorBas = $DoctorBas->whereIn('field_officer_id',$report->field_officer_id);
            $PharmachyBas = $PharmachyBas->whereIn('field_officer_id',$report->field_officer_id);
            
        }
        if($report->customer_id != ''){
            $task = $task->whereIn('gtask.customer_id',$report->customer_id); 
            $covered_task = $covered_task->whereIn('customer_id',$report->customer_id); 
            $uncovered_task = $uncovered_task->whereIn('customer_id', $report->customer_id);
            $inprogress_task = $inprogress_task->whereIn('customer_id',$report->customer_id);
            $notstarted_task = $notstarted_task->whereIn('customer_id',$report->customer_id);
            $totalpharamact_visit = $totalpharamact_visit->whereIn('customer_id',$report->customer_id);
            $totalpharamact_unvisit = $totalpharamact_unvisit->whereIn('customer_id',$report->customer_id);
            $DoctorBas = $DoctorBas->whereIn('customer_id',$report->customer_id);
            $PharmachyBas = $PharmachyBas->whereIn('customer_id',$report->customer_id);
        }
        if($report->year != ''){
            $task = $task->whereYear("gtask.task_date_time", $report->year);
            $covered_task = $covered_task->whereYear("task_date_time", $report->year);
            $uncovered_task = $uncovered_task->whereYear('task_date_time',$report->year);
            $inprogress_task = $inprogress_task->whereYear('task_date_time',$report->year);
            $notstarted_task = $notstarted_task->whereYear('task_date_time',$report->year);
            $totalpharamact_visit = $totalpharamact_visit->whereYear('task_date_time',$report->year);
            $totalpharamact_unvisit = $totalpharamact_unvisit->whereYear('task_date_time',$report->year);
            $DoctorBas = $DoctorBas->whereYear('task_date_time',$report->year);
            $PharmachyBas = $PharmachyBas->whereYear('task_date_time',$report->year);
        }
        if($report->months != ''){
            $task = $task->whereIn(DB::raw('MONTH(gtask.task_date_time)'), $report->months);
            $covered_task = $covered_task->whereIn(DB::raw('MONTH(task_date_time)'), $report->months);
            $uncovered_task = $uncovered_task->whereIn(DB::raw('MONTH(task_date_time)'), $report->months);
            $inprogress_task = $inprogress_task->whereIn(DB::raw('MONTH(task_date_time)'), $report->months);
            $notstarted_task = $notstarted_task->whereIn(DB::raw('MONTH(task_date_time)'), $report->months);
            $totalpharamact_visit = $totalpharamact_visit->whereIn(DB::raw('MONTH(task_date_time)'), $report->months);
            $totalpharamact_unvisit = $totalpharamact_unvisit->whereIn(DB::raw('MONTH(task_date_time)'),$report->months);
            $DoctorBas = $DoctorBas->whereIn(DB::raw('MONTH(task_date_time)'),$report->months);
            $PharmachyBas = $PharmachyBas->whereIn(DB::raw('MONTH(task_date_time)'),$report->months);
        }       

        $res_task = $task ->get(['gtask.task_id','gtask.task_title','gcustomer.customer_name','gtask.task_date_time','gtask.task_status','gusers.firstname','gusers.lastname','gbranch.branch_name','gregion.region_name','garea.area_name','gtask.customer_type','gcustomer.customer_location']);
       
        //Covered Task List Count
        $covered_task = $covered_task
                      ->where('task_status','3')
                      ->count('task_id');

        //UnCovered Task List count
        $uncovered_task = $uncovered_task
                      ->where('task_status','2')
                      ->count('task_id');

        //Inprogress Task List count
        $inprogress_task = $inprogress_task
                      ->where('task_status','1')
                      ->count('task_id');

        //Not Started Task List count
        $notstarted_task = $notstarted_task
                      ->where('task_status','0')
                      ->count('task_id');

        //Total Pharamacy Visited Count
        $totalpharamact_visit = $totalpharamact_visit
                      ->where('task_status','3')
                      ->groupBy('customer_id')
                      ->count('task_id');

        //Unvisited Pharamacy Visited Count
        $totalpharamact_unvisit = $totalpharamact_unvisit
                      ->where('task_status','2')
                      ->groupBy('customer_id')
                      ->count('task_id');


         //Doctor Based Visited in Pie Chart Count
        $DoctorBasvisit = [];
        $DoctorBas = $DoctorBas
                      ->where('task_status',3)
                      ->where('customer_type',1)
                      ->distinct('customer_id')
                      ->orderBy('customer_id', 'DESC')
                      ->orderBy('customer_id', 'ASC')
                      ->get(['customer_id']);

        /*print_r($DoctorBas);
        exit;*/

        foreach ($DoctorBas as $value) {

          $Doctor = [];

          $name =     DB::table('gpff_customer')
                      ->where('customer_id',$value->customer_id)
                      ->First();
          if($name){
            $Doctor['Name'] = $name->customer_name;

            $Doctor['Count'] = DB::table('gpff_task')
                        ->where('customer_id',$value->customer_id)
                        ->count();

            array_push($DoctorBasvisit, $Doctor);
          }
          
        }

        //Pharmachy Based Visited in Pie Chart Count
        $PharmachyBasvisit = [];
        $PharmachyBas_res = $PharmachyBas
                      ->where('task_status',3)
                      ->where('customer_type',2)
                      ->distinct('customer_id')
                      ->get(['customer_id']);

        foreach ($PharmachyBas_res as $value) {

          $Pharmacy = [];

          $name =     DB::table('gpff_customer')
                      ->where('customer_id',$value->customer_id)
                      ->First();
          if($name){
            $Pharmacy['Name'] = $name->customer_name;
            
            $Pharmacy['Count'] = DB::table('gpff_task')
                        ->where('customer_id',$value->customer_id)
                        ->count();

            array_push($PharmachyBasvisit, $Pharmacy);
          }
        }

        //Hospital Based Visited in Pie Chart Count
        $HospitalBasvisit = [];
        $HospitalBas = $PharmachyBas
                      ->where('task_status',3)
                      ->where('customer_type',3)
                      ->distinct('customer_id')
                      ->orderBy('customer_id', 'ASC')
                      ->get(['customer_id']);

        foreach ($HospitalBas as $value) {

          $Hospital = [];

          $name =     DB::table('gpff_customer')
                      ->where('customer_id',$value->customer_id)
                      ->First();
          if($name){
            $Hospital['Name'] = $name->customer_name;
            
            $Hospital['Count'] = DB::table('gpff_task')
                        ->where('customer_id',$value->customer_id)
                        ->count();

            array_push($HospitalBasvisit, $Hospital);
          }
        }


        $Doctorvisit_res = [];
        $Pharmacyvisit_res = [];
        $Hospitalvisit_res = [];
        //Doctor Visited in Bar Chart Count
        foreach ($report->months as $value) {
           $totalDocvist['Month'] = $value;
           $totalDocvist['count'] =  DB::table('gpff_task')
                      ->where('task_status',3)
                      ->where('customer_type',1)
                      ->whereMonth('created_at', $value)
                      ->where(function($query) use ($report) {
                        if($report->branch_id != '') {
                            $query->whereIn('branch_id',$report->branch_id);
                        }
                     })
                ->where(function($query) use ($report) {
                    if($report->region_id != '') {
                        $query->whereIn('region_id',$report->region_id);
                    }
                 })
                ->where(function($query) use ($report) {
                    if($report->area_id != '') {
                        $query->whereIn('area_id', $report->area_id);
                    }
                 })
                ->where(function($query) use ($report) {
                    if($report->field_officer_id != '') {
                        $query->whereIn('field_officer_id', $report->field_officer_id);
                    }
                 })
                ->where(function($query) use ($report) {
                    if($report->customer_id != '') {
                        $query->whereIn('customer_id', $report->customer_id);
                    }
                 })
                ->where(function($query) use ($report) {
                    if($report->year != '') {
                        $query->whereYear("created_at",$report->year);
                    }
                 })
            ->count();

             array_push($Doctorvisit_res, $totalDocvist);
        }
        //Pharmacy Visited in Bar Chart Count
        foreach ($report->months as $value) {
           $totalPharvist['Month'] = $value;
           $totalPharvist['count'] =  DB::table('gpff_task')
                      ->where('task_status',3)
                      ->where('customer_type',2)
                      ->whereMonth('created_at', $value)
                      ->where(function($query) use ($report) {
                        if($report->branch_id != '') {
                            $query->whereIn('branch_id',$report->branch_id);
                        }
                     })
                ->where(function($query) use ($report) {
                    if($report->region_id != '') {
                        $query->whereIn('region_id',$report->region_id);
                    }
                 })
                ->where(function($query) use ($report) {
                    if($report->area_id != '') {
                        $query->whereIn('area_id', $report->area_id);
                    }
                 })
                ->where(function($query) use ($report) {
                    if($report->field_officer_id != '') {
                        $query->whereIn('field_officer_id', $report->field_officer_id);
                    }
                 })
                ->where(function($query) use ($report) {
                    if($report->customer_id != '') {
                        $query->whereIn('customer_id', $report->customer_id);
                    }
                 })
                ->where(function($query) use ($report) {
                    if($report->year != '') {
                        $query->whereYear("created_at",$report->year);
                    }
                 })
            ->count();

             array_push($Pharmacyvisit_res, $totalPharvist);
        }
        //Hospital Visited in Bar Chart Count
        foreach ($report->months as $value) {
           $totalHospivist['Month'] = $value;
           $totalHospivist['count'] =  DB::table('gpff_task')
                      ->where('task_status',3)
                      ->where('customer_type',3)
                      ->whereMonth('created_at', $value)
                      ->where(function($query) use ($report) {
                        if($report->branch_id != '') {
                            $query->whereIn('branch_id',$report->branch_id);
                        }
                     })
                ->where(function($query) use ($report) {
                    if($report->region_id != '') {
                        $query->whereIn('region_id',$report->region_id);
                    }
                 })
                ->where(function($query) use ($report) {
                    if($report->area_id != '') {
                        $query->whereIn('area_id', $report->area_id);
                    }
                 })
                ->where(function($query) use ($report) {
                    if($report->field_officer_id != '') {
                        $query->whereIn('field_officer_id', $report->field_officer_id);
                    }
                 })
                ->where(function($query) use ($report) {
                    if($report->customer_id != '') {
                        $query->whereIn('customer_id', $report->customer_id);
                    }
                 })
                ->where(function($query) use ($report) {
                    if($report->year != '') {
                        $query->whereYear("created_at",$report->year);
                    }
                 })
            ->count();

             array_push($Hospitalvisit_res, $totalHospivist);
        }

        $Doctorunvisit_res = [];
        $Pharmacyunvisit_res = [];
        $Hospitalunvisit_res = [];
         //Doctor Visited in Bar Chart Count
        foreach ($report->months as $value) {
           $totalDocvist['Month'] = $value;
           $totalDocvist['count'] =  DB::table('gpff_task')
                      ->where('task_status',0)
                      ->where('customer_type',1)
                      ->whereMonth('created_at', $value)
                      ->where(function($query) use ($report) {
                        if($report->branch_id != '') {
                            $query->whereIn('branch_id',$report->branch_id);
                        }
                     })
                ->where(function($query) use ($report) {
                    if($report->region_id != '') {
                        $query->whereIn('region_id',$report->region_id);
                    }
                 })
                ->where(function($query) use ($report) {
                    if($report->area_id != '') {
                        $query->whereIn('area_id', $report->area_id);
                    }
                 })
                ->where(function($query) use ($report) {
                    if($report->field_officer_id != '') {
                        $query->whereIn('field_officer_id', $report->field_officer_id);
                    }
                 })
                ->where(function($query) use ($report) {
                    if($report->customer_id != '') {
                        $query->whereIn('customer_id', $report->customer_id);
                    }
                 })
                ->where(function($query) use ($report) {
                    if($report->year != '') {
                        $query->whereYear("created_at",$report->year);
                    }
                 })
            ->count();

             array_push($Doctorunvisit_res, $totalDocvist);
        }
        //Pharmacy Visited in Bar Chart Count
        foreach ($report->months as $value) {
           $totalPharvist['Month'] = $value;
           $totalPharvist['count'] =  DB::table('gpff_task')
                      ->where('task_status',0)
                      ->where('customer_type',2)
                      ->whereMonth('created_at', $value)
                      ->where(function($query) use ($report) {
                        if($report->branch_id != '') {
                            $query->whereIn('branch_id',$report->branch_id);
                        }
                     })
                ->where(function($query) use ($report) {
                    if($report->region_id != '') {
                        $query->whereIn('region_id',$report->region_id);
                    }
                 })
                ->where(function($query) use ($report) {
                    if($report->area_id != '') {
                        $query->whereIn('area_id', $report->area_id);
                    }
                 })
                ->where(function($query) use ($report) {
                    if($report->field_officer_id != '') {
                        $query->whereIn('field_officer_id', $report->field_officer_id);
                    }
                 })
                ->where(function($query) use ($report) {
                    if($report->customer_id != '') {
                        $query->whereIn('customer_id', $report->customer_id);
                    }
                 })
                ->where(function($query) use ($report) {
                    if($report->year != '') {
                        $query->whereYear("created_at",$report->year);
                    }
                 })
            ->count();

             array_push($Pharmacyunvisit_res, $totalPharvist);
        }
        //Hospital Visited in Bar Chart Count
        foreach ($report->months as $value) {
           $totalHospivist['Month'] = $value;
           $totalHospivist['count'] =  DB::table('gpff_task')
                      ->where('task_status',0)
                      ->where('customer_type',2)
                      ->whereMonth('created_at', $value)
                      ->where(function($query) use ($report) {
                        if($report->branch_id != '') {
                            $query->whereIn('branch_id',$report->branch_id);
                        }
                     })
                ->where(function($query) use ($report) {
                    if($report->region_id != '') {
                        $query->whereIn('region_id',$report->region_id);
                    }
                 })
                ->where(function($query) use ($report) {
                    if($report->area_id != '') {
                        $query->whereIn('area_id', $report->area_id);
                    }
                 })
                ->where(function($query) use ($report) {
                    if($report->field_officer_id != '') {
                        $query->whereIn('field_officer_id', $report->field_officer_id);
                    }
                 })
                ->where(function($query) use ($report) {
                    if($report->customer_id != '') {
                        $query->whereIn('customer_id', $report->customer_id);
                    }
                 })
                ->where(function($query) use ($report) {
                    if($report->year != '') {
                        $query->whereYear("created_at",$report->year);
                    }
                 })
            ->count();

             array_push($Hospitalunvisit_res, $totalHospivist);
        }

        $result = array_merge(
                              ['task_details' => $res_task->toArray()], 
                              ['covered_chart_count' => $covered_task],
                              ['uncovered_chart_count' => $uncovered_task],
                              ['inprogress_chart_count' => $inprogress_task],
                              ['notstarted_chart_count' => $notstarted_task],
                              ['totalpharamacy_visit' => $totalpharamact_visit],
                              ['totalpharamacy_unvisit' => $totalpharamact_unvisit],
                              ['doctorbased_visit' => $DoctorBasvisit],
                              ['pharmacybased_visit' => $PharmachyBasvisit],
                              ['hospitalbased_visit' => $HospitalBasvisit],
                              ['doctorbased_visit_chart_count' => $Doctorvisit_res],
                              ['pharmacybased_visit_chart_count' => $Pharmacyvisit_res],
                              ['hospitalbased_visit_chart_count' => $Hospitalvisit_res],
                              ['doctorbased_unvisit_chart_count' => $Doctorunvisit_res],
                              ['pharmacybased_unvisit_chart_count' => $Pharmacyunvisit_res],
                              ['hospitalbased_unvisit_chart_count' => $Hospitalunvisit_res]
                          );

        return $result;

    }


  public function dailyCallReport($report){
    
    $today = date('Y-m-d');
    $today = Carbon::now()->format('Y-m-d').'%';

    if($report->type == 1)
    {
      $orders = DB::table('gpff_task as gta')
              ->join('gpff_customer as gcus','gta.customer_id', '=' , 'gcus.customer_id')
              ->join('gpff_users as guse','gta.field_officer_id', '=' , 'guse.user_id')
              ->join('gpff_branch as gpbr','gta.branch_id', '=' , 'gpbr.branch_id')
              ->join('gpff_region as greg','gta.region_id', '=' , 'greg.region_id')
              ->join('gpff_area as gpar','gta.area_id', '=' , 'gpar.area_id')
              ->whereBetween(DB::RAW('date(gta.created_at)'), [date($report->start_date), date($report->end_date)])
              //->where('gta.task_date_time', 'like', $today)
              ->orderBy('gta.created_at', 'DESC');
     
     /*print_r($orders->count());
     exit;*/
     
      if($report->branch_id != ''){
          $orders = $orders->whereIn('gta.branch_id',$report->branch_id);
      }
     
      if($report->region_id != ''){
           $orders = $orders->whereIn('gta.region_id',$report->region_id);
      }
      if($report->area_id != ''){
          $orders = $orders->whereIn('gta.area_id',$report->area_id);
      }

      if($report->customer_id != ''){
          $orders = $orders->whereIn('gta.customer_id',$report->customer_id);
      }
      if($report->field_officer_id != ''){
          $orders = $orders->whereIn('gta.field_officer_id',$report->field_officer_id);        
      }

      $order_det = $orders->get(['gta.task_id','gta.created_by','gta.field_officer_id','guse.firstname','gta.customer_id','gta.area_manager_id','gta.task_title','gta.task_desc','gta.task_status','gta.task_date_time','gta.task_reassign_status','gta.task_start_date_time','gta.task_end_date_time','gta.task_active_minu','gta.notes','gta.created_at','gta.updated_at','gcus.customer_name','gcus.customer_email','gcus.country_code','gcus.customer_mobile','gcus.customer_city','gcus.customer_country','gcus.customer_fax','gcus.customer_address','gcus.customer_location','gcus.customer_lan','gcus.customer_lat','gcus.customer_cr_id','gcus.website','gcus.avatar','gcus.is_active','gta.branch_id','gta.region_id','gta.area_id','gpbr.branch_name','greg.region_name','gpar.area_name']);

        $result = array_merge(
            ['DailyCall_details'=>$order_det->toArray()]
        );

        return $result;
    } else{

      $orders = DB::table('gpff_task as gta')
              ->join('gpff_customer as gcus','gta.customer_id', '=' , 'gcus.customer_id')
              ->join('gpff_users as guse','gta.field_officer_id', '=' , 'guse.user_id')
              ->join('gpff_branch as gpbr','gta.branch_id', '=' , 'gpbr.branch_id')
              ->join('gpff_region as greg','gta.region_id', '=' , 'greg.region_id')
              ->join('gpff_area as gpar','gta.area_id', '=' , 'gpar.area_id')
              ->whereBetween(DB::RAW('date(gta.created_at)'), [date($report->start_date), date($report->end_date)])
              ->where('gta.task_status', 0)
              ->orderBy('gta.created_at', 'DESC');
     
      if($report->branch_id != ''){
          $orders = $orders->whereIn('gta.branch_id',$report->branch_id);
      }
      if($report->region_id != ''){
           $orders = $orders->whereIn('gta.region_id',$report->region_id);
      }
      if($report->area_id != ''){
          $orders = $orders->whereIn('gta.area_id',$report->area_id);
      }
      if($report->customer_id != ''){
          $orders = $orders->whereIn('gta.customer_id',$report->customer_id);
      }
      if($report->field_officer_id != ''){
          $orders = $orders->whereIn('gta.field_officer_id',$report->field_officer_id);        
      }

      $order_det = $orders->get(['gta.task_id','gta.created_by','gta.field_officer_id','guse.firstname','gta.customer_id','gta.area_manager_id','gta.task_title','gta.task_desc','gta.task_status','gta.task_date_time','gta.task_reassign_status','gta.task_start_date_time','gta.task_end_date_time','gta.task_active_minu','gta.notes','gta.created_at','gta.updated_at','gcus.customer_name','gcus.customer_email','gcus.country_code','gcus.customer_mobile','gcus.customer_city','gcus.customer_country','gcus.customer_fax','gcus.customer_address','gcus.customer_location','gcus.customer_lan','gcus.customer_lat','gcus.customer_cr_id','gcus.website','gcus.avatar','gcus.is_active','gta.branch_id','gta.region_id','gta.area_id','gpbr.branch_name','greg.region_name','gpar.area_name']);

        $result = array_merge(
            ['pending_task_details'=>$order_det->toArray()]
        );

        return $result;
    }
    
  }
////////////////////
///Visited Report///
////////////////////
     //Current Year Visited Report
    public function getCurrentYearVisitedReport()
    {   
        $visited = DB::table('gpff_task as gta')
                ->join('gpff_customer as gcus','gta.customer_id', '=' , 'gcus.customer_id')
                ->join('gpff_users as guse','gta.field_officer_id', '=' , 'guse.user_id')
                ->join('gpff_branch as gpbr','gta.branch_id', '=' , 'gpbr.branch_id')
                ->join('gpff_region as greg','gta.region_id', '=' , 'greg.region_id')
                ->join('gpff_area as gpar','gta.area_id', '=' , 'gpar.area_id')
                ->whereYear('gta.created_at', date('Y'))
                ->where('gta.task_status', 3)
                ->orderBy('gta.created_at', 'DESC')
                ->get(['gta.task_id','gta.created_by','gta.field_officer_id','guse.firstname','gta.customer_id','gta.area_manager_id','gta.task_title','gta.task_desc','gta.task_status','gta.task_date_time','gta.task_reassign_status','gta.task_start_date_time','gta.task_end_date_time','gta.task_active_minu','gta.notes','gta.created_at','gta.updated_at','gcus.customer_name','gcus.customer_email','gcus.country_code','gcus.customer_mobile','gcus.customer_city','gcus.customer_country','gcus.customer_fax','gcus.customer_address','gcus.customer_location','gcus.customer_lan','gcus.customer_lat','gcus.customer_cr_id','gcus.website','gcus.avatar','gcus.customer_type','gcus.is_active','gta.branch_id','gta.region_id','gta.area_id','gpbr.branch_name','greg.region_name','gpar.area_name']);

        //Total Visited in Par Chart Count
        $totalvist = [];
        for ($i=1; $i<=12; $i++) {
  
            $totalvisiCount = DB::table('gpff_task')
                      ->whereYear('created_at', date('Y'))
                      ->whereMonth('created_at', $i)
                      ->where('task_status',3)
                      ->count();
            
            array_push($totalvist, $totalvisiCount);
        }
        //Doctor Visited in Par Chart Count
        $Doctorvisit = [];
        for ($i=1; $i<=12; $i++) {
            
            $totalDocvisiCount = DB::table('gpff_task')
                      ->whereYear('created_at', date('Y'))
                      ->whereMonth('created_at', $i)
                      ->where('task_status',3)
                      ->where('customer_type',1)
                      ->count();
            
            array_push($Doctorvisit, $totalDocvisiCount);
        }
        //Pharmacy Visited in Par Chart Count
        $Pharmacyvisit = [];
        for ($i=1; $i<=12; $i++) {
        
            $totalPharvisiCount = DB::table('gpff_task')
                      ->whereYear('created_at', date('Y'))
                      ->whereMonth('created_at', $i)
                      ->where('task_status',3)
                      ->where('customer_type',2)
                      ->count();
            
            array_push($Pharmacyvisit, $totalPharvisiCount);
        }
        //Hospital Visited in Par Chart Count
        $Hospitalvisit = [];
        for ($i=1; $i<=12; $i++) {
        
            $totalHospivisiCount = DB::table('gpff_task')
                      ->whereYear('created_at', date('Y'))
                      ->whereMonth('created_at', $i)
                      ->where('task_status',3)
                      ->where('customer_type',3)
                      ->count();
            
            array_push($Hospitalvisit, $totalHospivisiCount);
        }
        //Totoal Visited Totoal Count
        $totvisitotCount = DB::table('gpff_task')
                      ->whereYear('created_at', date('Y'))
                      ->where('task_status',3)
                      ->count();

        //Doctor Visited Totoal Count
        $DocvisitotCount = DB::table('gpff_task')
                      ->whereYear('created_at', date('Y'))
                      ->where('task_status',3)
                      ->where('customer_type',1)
                      ->count();

        //Pharmacy Visited Totoal Count
        $PhavisitotCount = DB::table('gpff_task')
                      ->whereYear('created_at', date('Y'))
                      ->where('task_status',3)
                      ->where('customer_type',2)
                      ->count();

        //Hospital Visited Totoal Count
        $HospivisitotCount = DB::table('gpff_task')
                      ->whereYear('created_at', date('Y'))
                      ->where('task_status',3)
                      ->where('customer_type',3)
                      ->count();

        //Doctor Based Visited in Pie Chart Count
        $DoctorBasvisit = [];
        $DoctorBas = DB::table('gpff_task')
                      ->whereYear('created_at', date('Y'))
                      ->where('task_status',3)
                      ->where('customer_type',1)
                      ->distinct('customer_id')
                      ->get(['customer_id']);

        foreach ($DoctorBas as $value) {

          $Doctor = [];

          $name =     DB::table('gpff_customer')
                      ->where('customer_id',$value->customer_id)
                      ->First();

          $Doctor['Name'] = $name->customer_name;
          
          $Doctor['Count'] = DB::table('gpff_task')
                      ->where('customer_id',$value->customer_id)
                      ->count();

          array_push($DoctorBasvisit, $Doctor);
        }

        //Pharmachy Based Visited in Pie Chart Count
        $PharmachyBasvisit = [];
        $PharmachyBas = DB::table('gpff_task')
                      ->whereYear('created_at', date('Y'))
                      ->where('task_status',3)
                      ->where('customer_type',2)
                      ->distinct('customer_id')
                      ->get(['customer_id']);

        foreach ($PharmachyBas as $value) {

          $Pharmacy = [];

          $name =     DB::table('gpff_customer')
                      ->where('customer_id',$value->customer_id)
                      ->First();

          $Pharmacy['Name'] = $name->customer_name;
          
          $Pharmacy['Count'] = DB::table('gpff_task')
                      ->where('customer_id',$value->customer_id)
                      ->count();

          array_push($PharmachyBasvisit, $Pharmacy);
        }
        //Hospital Based Visited in Pie Chart Count
        $HospitalBasvisit = [];
        $HospitalBas = DB::table('gpff_task')
                      ->whereYear('created_at', date('Y'))
                      ->where('task_status',3)
                      ->where('customer_type',3)
                      ->distinct('customer_id')
                      ->get(['customer_id']);

        foreach ($HospitalBas as $value) {

          $Hospital = [];

          $name =     DB::table('gpff_customer')
                      ->where('customer_id',$value->customer_id)
                      ->First();

          $Hospital['Name'] = $name->customer_name;
          
          $Hospital['Count'] = DB::table('gpff_task')
                      ->where('customer_id',$value->customer_id)
                      ->count();

          array_push($HospitalBasvisit, $Hospital);
        }

        $result = array_merge(
                              ['visited_details'=>$visited->toArray()], 
                              ['TotalVisit_count' => $totalvist],
                              ['TotalDocVisit_count' => $Doctorvisit],
                              ['TotalPharmacyVisit_count' => $Pharmacyvisit],
                              ['TotalHospitalVisit_count' => $Hospitalvisit],
                              ['TotalVisitTotal' => $totvisitotCount],
                              ['TotalDocVisitTotal' => $DocvisitotCount],
                              ['TotalPharmacyVisitTotal' => $PhavisitotCount],
                              ['TotalHospitalVisitTotal' => $HospivisitotCount],
                              ['DoctorBasedvisit_count' => $DoctorBasvisit],
                              ['PharmachyBasedvisit_count' => $PharmachyBasvisit],
                              ['HospitalBasedvisit_count' => $HospitalBasvisit]
                          );

        return $result;

    }
    ///Visited Filter based Report
    public function getVisitedFilterReport($report)
    {   
      $visited = DB::table('gpff_task as gta')
                ->join('gpff_customer as gcus','gta.customer_id', '=' , 'gcus.customer_id')
                ->join('gpff_users as guse','gta.field_officer_id', '=' , 'guse.user_id')
                ->join('gpff_branch as gpbr','gta.branch_id', '=' , 'gpbr.branch_id')
                ->join('gpff_region as greg','gta.region_id', '=' , 'greg.region_id')
                ->join('gpff_area as gpar','gta.area_id', '=' , 'gpar.area_id')
                ->where('gta.task_status', 3)
                ->orderBy('gta.created_at', 'DESC');
     
        if($report->branch_id != ''){
            $visited = $visited->whereIn('gta.branch_id',$report->branch_id);
        }
        if($report->region_id != ''){
             $visited = $visited->whereIn('gta.region_id',$report->region_id);
        }
        if($report->area_id != ''){
            $visited = $visited->whereIn('gta.area_id',$report->area_id);
        }
        if($report->field_officer_id != ''){
            $visited = $visited->whereIn('gta.field_officer_id',$report->field_officer_id); 
        }
        if($report->year != ''){
            $visited = $visited->whereYear("gta.created_at",$report->year);
        }
        if($report->months != ''){
            $visited = $visited->whereIn(DB::raw('MONTH(gta.created_at)'), $report->months);
        }

        $totalvist_res = [];
        $Doctorvisit_res = [];
        $Pharmacyvisit_res = [];
        $Hospitalvisit_res = [];

        //Total Visited in Par Chart Count  
        foreach ($report->months as $value) {
           $totalvist['Month'] = $value;
           $totalvist['count'] =  DB::table('gpff_task')
                      ->where('task_status',3)
                      ->whereMonth('created_at', $value)
                      ->where(function($query) use ($report) {
                        if($report->branch_id != '') {
                            $query->whereIn('branch_id',$report->branch_id);
                        }
                     })
                ->where(function($query) use ($report) {
                    if($report->region_id != '') {
                        $query->whereIn('region_id',$report->region_id);
                    }
                 })
                ->where(function($query) use ($report) {
                    if($report->area_id != '') {
                        $query->whereIn('area_id', $report->area_id);
                    }
                 })
                ->where(function($query) use ($report) {
                    if($report->field_officer_id != '') {
                        $query->whereIn('field_officer_id', $report->field_officer_id);
                    }
                 })
                ->where(function($query) use ($report) {
                    if($report->year != '') {
                        $query->whereYear("created_at",$report->year);
                    }
                 })
            ->count();

             array_push($totalvist_res, $totalvist);
        }
        //Doctor Visited in Par Chart Count
        foreach ($report->months as $value) {
           $totalDocvist['Month'] = $value;
           $totalDocvist['count'] =  DB::table('gpff_task')
                      ->where('task_status',3)
                      ->where('customer_type',1)
                      ->whereMonth('created_at', $value)
                      ->where(function($query) use ($report) {
                        if($report->branch_id != '') {
                            $query->whereIn('branch_id',$report->branch_id);
                        }
                     })
                ->where(function($query) use ($report) {
                    if($report->region_id != '') {
                        $query->whereIn('region_id',$report->region_id);
                    }
                 })
                ->where(function($query) use ($report) {
                    if($report->area_id != '') {
                        $query->whereIn('area_id', $report->area_id);
                    }
                 })
                ->where(function($query) use ($report) {
                    if($report->field_officer_id != '') {
                        $query->whereIn('field_officer_id', $report->field_officer_id);
                    }
                 })
                ->where(function($query) use ($report) {
                    if($report->year != '') {
                        $query->whereYear("created_at",$report->year);
                    }
                 })
            ->count();

             array_push($Doctorvisit_res, $totalDocvist);
        }
        //Pharmacy Visited in Par Chart Count
        foreach ($report->months as $value) {
           $totalPharvist['Month'] = $value;
           $totalPharvist['count'] =  DB::table('gpff_task')
                      ->where('task_status',3)
                      ->where('customer_type',2)
                      ->whereMonth('created_at', $value)
                      ->where(function($query) use ($report) {
                        if($report->branch_id != '') {
                            $query->whereIn('branch_id',$report->branch_id);
                        }
                     })
                ->where(function($query) use ($report) {
                    if($report->region_id != '') {
                        $query->whereIn('region_id',$report->region_id);
                    }
                 })
                ->where(function($query) use ($report) {
                    if($report->area_id != '') {
                        $query->whereIn('area_id', $report->area_id);
                    }
                 })
                ->where(function($query) use ($report) {
                    if($report->field_officer_id != '') {
                        $query->whereIn('field_officer_id', $report->field_officer_id);
                    }
                 })
                ->where(function($query) use ($report) {
                    if($report->year != '') {
                        $query->whereYear("created_at",$report->year);
                    }
                 })
            ->count();

             array_push($Pharmacyvisit_res, $totalPharvist);
        }
        //Pharmacy Visited in Par Chart Count
        foreach ($report->months as $value) {
           $totalHospivist['Month'] = $value;
           $totalHospivist['count'] =  DB::table('gpff_task')
                      ->where('task_status',3)
                      ->where('customer_type',3)
                      ->whereMonth('created_at', $value)
                      ->where(function($query) use ($report) {
                        if($report->branch_id != '') {
                            $query->whereIn('branch_id',$report->branch_id);
                        }
                     })
                ->where(function($query) use ($report) {
                    if($report->region_id != '') {
                        $query->whereIn('region_id',$report->region_id);
                    }
                 })
                ->where(function($query) use ($report) {
                    if($report->area_id != '') {
                        $query->whereIn('area_id', $report->area_id);
                    }
                 })
                ->where(function($query) use ($report) {
                    if($report->field_officer_id != '') {
                        $query->whereIn('field_officer_id', $report->field_officer_id);
                    }
                 })
                ->where(function($query) use ($report) {
                    if($report->year != '') {
                        $query->whereYear("created_at",$report->year);
                    }
                 })
            ->count();

             array_push($Hospitalvisit_res, $totalHospivist);
        }
        
        //Totoal Visited Totoal Count
        $totvisitotCount = DB::table('gpff_task')
                      ->where('task_status',3)
                      ->whereIn(DB::raw('MONTH(created_at)'), $report->months)
                      ->where(function($query) use ($report) {
                        if($report->branch_id != '') {
                            $query->whereIn('branch_id',$report->branch_id);
                        }
                     })
                ->where(function($query) use ($report) {
                    if($report->region_id != '') {
                        $query->whereIn('region_id',$report->region_id);
                    }
                 })
                ->where(function($query) use ($report) {
                    if($report->area_id != '') {
                        $query->whereIn('area_id', $report->area_id);
                    }
                 })
                ->where(function($query) use ($report) {
                    if($report->field_officer_id != '') {
                        $query->whereIn('field_officer_id', $report->field_officer_id);
                    }
                 })
                ->where(function($query) use ($report) {
                    if($report->year != '') {
                        $query->whereYear("created_at",$report->year);
                    }
                 })
            ->count();

        //Doctor Visited Totoal Count
        $DocvisitotCount = DB::table('gpff_task')
                      ->where('task_status',3)
                      ->where('customer_type',1)
                      ->whereIn(DB::raw('MONTH(created_at)'), $report->months)
                      ->where(function($query) use ($report) {
                        if($report->branch_id != '') {
                            $query->whereIn('branch_id',$report->branch_id);
                        }
                     })
                ->where(function($query) use ($report) {
                    if($report->region_id != '') {
                        $query->whereIn('region_id',$report->region_id);
                    }
                 })
                ->where(function($query) use ($report) {
                    if($report->area_id != '') {
                        $query->whereIn('area_id', $report->area_id);
                    }
                 })
                ->where(function($query) use ($report) {
                    if($report->field_officer_id != '') {
                        $query->whereIn('field_officer_id', $report->field_officer_id);
                    }
                 })
                ->where(function($query) use ($report) {
                    if($report->year != '') {
                        $query->whereYear("created_at",$report->year);
                    }
                 })
                ->where(function($query) use ($report) {
                    if($report->months != '') {
                        $query->whereIn(DB::raw('MONTH(created_at)'),$report->months);
                    }
                 })
            ->count();

        //Pharmacy Visited Totoal Count
        $PhavisitotCount = DB::table('gpff_task')
                      ->where('task_status',3)
                      ->where('customer_type',2)
                      ->whereIn(DB::raw('MONTH(created_at)'), $report->months)
                      ->where(function($query) use ($report) {
                        if($report->branch_id != '') {
                            $query->whereIn('branch_id',$report->branch_id);
                        }
                     })
                ->where(function($query) use ($report) {
                    if($report->region_id != '') {
                        $query->whereIn('region_id',$report->region_id);
                    }
                 })
                ->where(function($query) use ($report) {
                    if($report->area_id != '') {
                        $query->whereIn('area_id', $report->area_id);
                    }
                 })
                ->where(function($query) use ($report) {
                    if($report->field_officer_id != '') {
                        $query->whereIn('field_officer_id', $report->field_officer_id);
                    }
                 })
                ->where(function($query) use ($report) {
                    if($report->year != '') {
                        $query->whereYear("created_at",$report->year);
                    }
                 })
                ->where(function($query) use ($report) {
                    if($report->months != '') {
                        $query->whereIn(DB::raw('MONTH(created_at)'),$report->months);
                    }
                 })
            ->count();
        //Hospital Visited Totoal Count
        $HospivisitotCount = DB::table('gpff_task')
                      ->where('task_status',3)
                      ->where('customer_type',3)
                      ->whereIn(DB::raw('MONTH(created_at)'), $report->months)
                      ->where(function($query) use ($report) {
                        if($report->branch_id != '') {
                            $query->whereIn('branch_id',$report->branch_id);
                        }
                     })
                ->where(function($query) use ($report) {
                    if($report->region_id != '') {
                        $query->whereIn('region_id',$report->region_id);
                    }
                 })
                ->where(function($query) use ($report) {
                    if($report->area_id != '') {
                        $query->whereIn('area_id', $report->area_id);
                    }
                 })
                ->where(function($query) use ($report) {
                    if($report->field_officer_id != '') {
                        $query->whereIn('field_officer_id', $report->field_officer_id);
                    }
                 })
                ->where(function($query) use ($report) {
                    if($report->year != '') {
                        $query->whereYear("created_at",$report->year);
                    }
                 })
                ->where(function($query) use ($report) {
                    if($report->months != '') {
                        $query->whereIn(DB::raw('MONTH(created_at)'),$report->months);
                    }
                 })
            ->count();

        //Doctor Based Visited in Pie Chart Count
        $DoctorBasvisit = [];

        $DoctorBas =  DB::table('gpff_task')
                      ->where('task_status',3)
                      ->where('customer_type',1)
                      ->where(function($query) use ($report) {
                        if($report->branch_id != '') {
                            $query->whereIn('branch_id',$report->branch_id);
                        }
                     })
                ->where(function($query) use ($report) {
                    if($report->region_id != '') {
                        $query->whereIn('region_id',$report->region_id);
                    }
                 })
                ->where(function($query) use ($report) {
                    if($report->area_id != '') {
                        $query->whereIn('area_id', $report->area_id);
                    }
                 })
                ->where(function($query) use ($report) {
                    if($report->field_officer_id != '') {
                        $query->whereIn('field_officer_id', $report->field_officer_id);
                    }
                 })
                ->where(function($query) use ($report) {
                    if($report->year != '') {
                        $query->whereYear("created_at",$report->year);
                    }
                 })
                ->where(function($query) use ($report) {
                    if($report->months != '') {
                        $query->whereIn(DB::raw('MONTH(created_at)'),$report->months);
                    }
                 })
                ->distinct('customer_id')
                ->get(['customer_id']);

        foreach ($DoctorBas as $value) {

          $Doctor = [];

          $name =     DB::table('gpff_customer')
                      ->where('customer_id',$value->customer_id)
                      ->First();
          if($name){
            $Doctor['Name'] = $name->customer_name;
          }
    
          $Doctor['Count'] = DB::table('gpff_task')
                      ->where('customer_id',$value->customer_id)
                      ->where('task_status',3)
                      ->where(function($query) use ($report) {
                        if($report->branch_id != '') {
                            $query->whereIn('branch_id',$report->branch_id);
                        }
                     })
                ->where(function($query) use ($report) {
                    if($report->region_id != '') {
                        $query->whereIn('region_id',$report->region_id);
                    }
                 })
                ->where(function($query) use ($report) {
                    if($report->area_id != '') {
                        $query->whereIn('area_id', $report->area_id);
                    }
                 })
                ->where(function($query) use ($report) {
                    if($report->field_officer_id != '') {
                        $query->whereIn('field_officer_id', $report->field_officer_id);
                    }
                 })
                ->where(function($query) use ($report) {
                    if($report->year != '') {
                        $query->whereYear("created_at",$report->year);
                    }
                 })
                ->where(function($query) use ($report) {
                    if($report->months != '') {
                        $query->whereIn(DB::raw('MONTH(created_at)'),$report->months);
                    }
                 })
                ->count();

          array_push($DoctorBasvisit, $Doctor);
        }
        //Pharmachy Based Visited in Pie Chart Count
        $PharmachyBasvisit = [];

        $PharmachyBas =  DB::table('gpff_task')
                      ->where('task_status',3)
                      ->where('customer_type',2)
                      ->where(function($query) use ($report) {
                        if($report->branch_id != '') {
                            $query->whereIn('branch_id',$report->branch_id);
                        }
                     })
                ->where(function($query) use ($report) {
                    if($report->region_id != '') {
                        $query->whereIn('region_id',$report->region_id);
                    }
                 })
                ->where(function($query) use ($report) {
                    if($report->area_id != '') {
                        $query->whereIn('area_id', $report->area_id);
                    }
                 })
                ->where(function($query) use ($report) {
                    if($report->field_officer_id != '') {
                        $query->whereIn('field_officer_id', $report->field_officer_id);
                    }
                 })
                ->where(function($query) use ($report) {
                    if($report->year != '') {
                        $query->whereYear("created_at",$report->year);
                    }
                 })
                ->where(function($query) use ($report) {
                    if($report->months != '') {
                        $query->whereIn(DB::raw('MONTH(created_at)'),$report->months);
                    }
                 })
                ->distinct('customer_id')
                ->get(['customer_id']);

        foreach ($PharmachyBas as $value) {

          $Pharmacy = [];

          $name =     DB::table('gpff_customer')
                      ->where('customer_id',$value->customer_id)
                      ->First();
          if($name){
            $Pharmacy['Name'] = $name->customer_name;
          }
          
          $Pharmacy['Count'] = DB::table('gpff_task')
                      ->where('customer_id',$value->customer_id)
                      ->where('task_status',3)
                      ->where(function($query) use ($report) {
                        if($report->branch_id != '') {
                            $query->whereIn('branch_id',$report->branch_id);
                        }
                     })
                ->where(function($query) use ($report) {
                    if($report->region_id != '') {
                        $query->whereIn('region_id',$report->region_id);
                    }
                 })
                ->where(function($query) use ($report) {
                    if($report->area_id != '') {
                        $query->whereIn('area_id', $report->area_id);
                    }
                 })
                ->where(function($query) use ($report) {
                    if($report->field_officer_id != '') {
                        $query->whereIn('field_officer_id', $report->field_officer_id);
                    }
                 })
                ->where(function($query) use ($report) {
                    if($report->year != '') {
                        $query->whereYear("created_at",$report->year);
                    }
                 })
                ->where(function($query) use ($report) {
                    if($report->months != '') {
                        $query->whereIn(DB::raw('MONTH(created_at)'),$report->months);
                    }
                 })
                ->count();

          array_push($PharmachyBasvisit, $Pharmacy);
        }

        //Hospital Based Visited in Pie Chart Count
        $HospitalBasvisit = [];

        $HospitalBas =  DB::table('gpff_task')
                      ->where('task_status',3)
                      ->where('customer_type',3)
                      ->where(function($query) use ($report) {
                        if($report->branch_id != '') {
                            $query->whereIn('branch_id',$report->branch_id);
                        }
                     })
                ->where(function($query) use ($report) {
                    if($report->region_id != '') {
                        $query->whereIn('region_id',$report->region_id);
                    }
                 })
                ->where(function($query) use ($report) {
                    if($report->area_id != '') {
                        $query->whereIn('area_id', $report->area_id);
                    }
                 })
                ->where(function($query) use ($report) {
                    if($report->field_officer_id != '') {
                        $query->whereIn('field_officer_id', $report->field_officer_id);
                    }
                 })
                ->where(function($query) use ($report) {
                    if($report->year != '') {
                        $query->whereYear("created_at",$report->year);
                    }
                 })
                ->where(function($query) use ($report) {
                    if($report->months != '') {
                        $query->whereIn(DB::raw('MONTH(created_at)'),$report->months);
                    }
                 })
                ->distinct('customer_id')
                ->get(['customer_id']);

        foreach ($HospitalBas as $value) {

          $Hospital = [];

          $name =     DB::table('gpff_customer')
                      ->where('customer_id',$value->customer_id)
                      ->First();
          if($name){
            $Hospital['Name'] = $name->customer_name;
          }
          
          $Hospital['Count'] = DB::table('gpff_task')
                      ->where('customer_id',$value->customer_id)
                      ->where('task_status',3)
                      ->where(function($query) use ($report) {
                        if($report->branch_id != '') {
                            $query->whereIn('branch_id',$report->branch_id);
                        }
                     })
                ->where(function($query) use ($report) {
                    if($report->region_id != '') {
                        $query->whereIn('region_id',$report->region_id);
                    }
                 })
                ->where(function($query) use ($report) {
                    if($report->area_id != '') {
                        $query->whereIn('area_id', $report->area_id);
                    }
                 })
                ->where(function($query) use ($report) {
                    if($report->field_officer_id != '') {
                        $query->whereIn('field_officer_id', $report->field_officer_id);
                    }
                 })
                ->where(function($query) use ($report) {
                    if($report->year != '') {
                        $query->whereYear("created_at",$report->year);
                    }
                 })
                ->where(function($query) use ($report) {
                    if($report->months != '') {
                        $query->whereIn(DB::raw('MONTH(created_at)'),$report->months);
                    }
                 })
                ->count();

          array_push($HospitalBasvisit, $Hospital);
        }

        $visited_de = $visited->get(['gta.task_id','gta.created_by','gta.field_officer_id','guse.firstname','gta.customer_id','gta.area_manager_id','gta.task_title','gta.task_desc','gta.task_status','gta.task_date_time','gta.task_reassign_status','gta.task_start_date_time','gta.task_end_date_time','gta.task_active_minu','gta.notes','gta.created_at','gta.updated_at','gcus.customer_name','gcus.customer_email','gcus.country_code','gcus.customer_mobile','gcus.customer_city','gcus.customer_country','gcus.customer_fax','gcus.customer_address','gcus.customer_location','gcus.customer_lan','gcus.customer_lat','gcus.customer_cr_id','gcus.website','gcus.avatar','gcus.customer_type','gcus.is_active','gta.branch_id','gta.region_id','gta.area_id','gpbr.branch_name','greg.region_name','gpar.area_name']);

        $result = array_merge(
                              ['visited_details'=>$visited_de->toArray()], 
                              ['TotalVisit_count' => $totalvist_res],
                              ['TotalDocVisit_count' => $Doctorvisit_res],
                              ['TotalPharmacyVisit_count' => $Pharmacyvisit_res],
                              ['TotalHospitalVisit_count' => $Hospitalvisit_res],
                              ['TotalVisitTotal' => $totvisitotCount],
                              ['TotalDocVisitTotal' => $DocvisitotCount],
                              ['TotalPharmacyVisitTotal' => $PhavisitotCount],
                              ['TotalHospitalVisitTotal' => $HospivisitotCount],
                              ['DoctorBasedvisit_count' => $DoctorBasvisit],
                              ['PharmachyBasedvisit_count' => $PharmachyBasvisit],
                              ['HospitalBasedvisit_count' => $HospitalBasvisit]
                          );

        return $result;
    }

////////////////////
///Target Report////
////////////////////

    ////////////////////
///Target Report////
////////////////////
  public function targetDetailReport($req){
    $months = $req->months;
    //$months = explode(',', $req->months);
    $array_ret = [];
    $array_val = [];

    for($i=0; $i< count($months); $i++) {
        $array_val['month'] = $months[$i];
        $array_val['year'] = $req->year;
        $array_val['target'] = $this->getTargetBasedInsentive($req, $months[$i]);
        $array_val['unit'] = $this->getUnitBasedInsentive($req, $months[$i]); // NO Changes
        $array_val['percentage'] = $this->getPercentageBasedInsentive($req, $months[$i]); 
        array_push($array_ret, $array_val);
    }

    return $array_ret;
    /*echo json_encode($array_ret);
    exit;*/
}

private function getUnitBasedInsentive($req, $month) {
    $array_ret = [];
    $prdObject = [];
    $year = $req->year;
    $filedOfficer = $req->field_officer_id;
    $insResult = DB::table('gpff_insentive')
                    ->where('fo_id', $filedOfficer)
                    ->where('insentive_type', 2)
                    ->where('insentive_status', 1)
                    ->whereRaw('year(from_date) = '. $year .' AND month(from_date) = '. $month .' order by cast(product_id as unsigned) ASC')
                    ->select('insentive_id', 'product_id', 'unit', 'insentive_amt', 'from_date', 'to_date')
                    ->get();
    
    if(count($insResult)) {
        foreach($insResult as $row) {
            $prdIdQry = $row->product_id === 'all' ? 'ol.product_id != 0 ' : 'ol.product_id = '. $row->product_id;
            $orderRes = DB::table('gpff_order as o')
                        ->join('gpff_order_list as ol', 'o.order_id', 'ol.order_id')
                        ->join('gpff_product as p', 'ol.product_id', 'p.product_id')
                        ->where('field_officer_id', $filedOfficer)
                        ->where('p.product_type', 1)    
                        ->whereRaw($prdIdQry . ' and DATE(o.created_at) >= DATE("'. $row->from_date .'") AND DATE(o.created_at) <= DATE("'. $row->to_date.'")')
                        ->select('o.order_id', 'o.field_officer_id', 'o.or_tot_price', 'ol.product_id', 'ol.product_qty','o.created_at', 'p.product_type', 'p.product_netprice', 'p.product_genericname', 'p.product_id')
                        ->get();
            
            
            foreach($orderRes as $repRow) {
                if(isset($prdObject[$repRow->product_id])) {
                    $prdObject[$repRow->product_id]['sale_unit'] = (int)$prdObject[$repRow->product_id]['sale_unit'] + (int)$repRow->product_qty;
                } else {
                    $prdObject[$repRow->product_id] = [
                        'pid' => $repRow->product_id,
                        'pname' => $repRow->product_genericname,
                        'sale_unit' => $repRow->product_qty,
                        'unit_target' => $row->unit,
                        'insentive_amt' => $row->insentive_amt,
                        'pprice' => $repRow->product_netprice
                        
                    ];
                }
            }
        }
    }

    //return $prdObject;

    foreach($prdObject as $key => $value) {
        array_push($array_ret, $value);
    }
    return $array_ret;
}

private function getTargetBasedInsentive($req, $month) { 
    $array_ret = [];
    $prdObject = [];
    $totalSaleAmt = 0;
    $year = $req->year;
    $filedOfficer = $req->field_officer_id;

    $insResult = DB::table('gpff_insentive')
                    ->where('fo_id', $filedOfficer)
                    ->where('insentive_type', 1)
                    ->where('insentive_status', 1)
                    ->whereRaw('year(from_date) = '. $year .' AND month(from_date) = '. $month .' order by cast(target_amt as unsigned) DESC')
                    ->select('insentive_id', 'product_id', 'unit', 'insentive_amt', 'target_amt' ,'from_date', 'to_date')
                    ->get();

    if(count($insResult)) {
        foreach($insResult as $row) {

            $orderRes = DB::table('gpff_order as o')
                        ->leftjoin('gpff_order_list as ol', 'o.order_id', 'ol.order_id')
                        ->where('o.field_officer_id', $filedOfficer)
                        ->where('ol.product_type', 1)    
                        ->groupBy('o.order_id')
                        ->whereRaw('DATE(o.created_at) >= DATE("'. $row->from_date .'") AND DATE(o.created_at) <= DATE("'. $row->to_date.'")')
                        ->select('o.order_id', 'o.field_officer_id', 'o.or_tot_price')
                        ->get();

            if(count($orderRes)) {
                foreach($orderRes as $repRow) {
                    $totalSaleAmt = $totalSaleAmt + (int)$repRow->or_tot_price;
                }
            }
        }
    }

    $condResult = DB::table('gpff_insentive')
                    ->where('fo_id', $filedOfficer)
                    ->where('insentive_type', 2)
                    ->where('insentive_status', 1)
                    ->where('target_amt', '<', $totalSaleAmt)
                    ->whereRaw('year(from_date) = '. $year .' AND month(from_date) = '. $month .' order by cast(target_amt as unsigned) DESC limit 0,1')
                    ->select('insentive_id', 'insentive_amt', 'target_amt', DB::Raw($totalSaleAmt . ' as totalsaleAmt1'), DB::RAW('('.$totalSaleAmt .'/100) * insentive_amt as totalsaleAmt'))
                    ->get();
    return $condResult;
}

private function getPercentageBasedInsentive($req, $month) { 
    $array_ret = [];
    $prdObject = [];
    $year = $req->year;
    $filedOfficer = $req->field_officer_id;
    $insResult = DB::table('gpff_insentive')
                    ->where('fo_id', $filedOfficer)
                    ->where('insentive_type', 3)
                    ->where('insentive_status', 1)
                    ->whereRaw('year(from_date) = '. $year .' AND month(from_date) = '. $month .' order by cast(product_id as unsigned) ASC')
                    ->select('insentive_id', 'product_id', 'unit', 'insentive_amt', 'from_date', 'to_date')
                    ->get();
    if(count($insResult)) {
        foreach($insResult as $row) {
            $prdIdQry = $row->product_id === 'all' ? 'ol.product_id != 0 ' : 'ol.product_id = '. $row->product_id;
            $orderRes = DB::table('gpff_order as o')
                        ->join('gpff_order_list as ol', 'o.order_id', 'ol.order_id')
                        ->join('gpff_product as p', 'ol.product_id', 'p.product_id')
                        ->where('field_officer_id', $filedOfficer)
                        ->where('p.product_type', 2)    
                        ->whereRaw($prdIdQry . ' and DATE(o.created_at) >= DATE("'. $row->from_date .'") AND DATE(o.created_at) <= DATE("'. $row->to_date.'")')
                        ->select('o.order_id', 'o.field_officer_id', 'o.or_tot_price', 'ol.product_id', 'ol.product_qty','o.created_at', 'p.product_type', 'p.product_netprice', 'p.product_genericname', 'p.product_id')
                        ->get();
            
            
            foreach($orderRes as $repRow) {
                if(isset($prdObject[$repRow->product_id])) {
                    $prdObject[$repRow->product_id]['sale_unit'] = (int)$prdObject[$repRow->product_id]['sale_unit'] + (int)$repRow->product_qty;
                } else {
                    $prdObject[$repRow->product_id] = [
                        'pid' => $repRow->product_id,
                        'pname' => $repRow->product_genericname,
                        'sale_unit' => $repRow->product_qty,
                        'insentive_amt' => $row->insentive_amt,
                        'pprice' => $repRow->product_netprice
                    ];
                }
            }
        }
    }

    foreach($prdObject as $key => $value) {
        array_push($array_ret, $value);
    }
    return $array_ret;
}
     //Get Current Year Samples Report
    public function getCurrentYearSamplesReport()
    {   
        $samples = DB::table('gpff_delivered_samples as gdeliversample')
                ->join('gpff_samples as gsample','gsample.samples_id','gdeliversample.samples_id')
                ->join('gpff_customer as gcustomer','gdeliversample.customer_id','gcustomer.customer_id')
                ->join('gpff_branch as gbranch','gsample.branch_id','gbranch.branch_id')
                ->join('gpff_region as gregion','gsample.region_id','gregion.region_id')
                ->whereYear('gdeliversample.created_at', date('Y'))
                ->orderBy('gdeliversample.created_at', 'DESC')
                ->get(['gdeliversample.samples_id','gdeliversample.created_at','gdeliversample.samples_name','gdeliversample.samples_qty','gdeliversample.field_officer_name','gdeliversample.field_officer_name','gcustomer.customer_name','gcustomer.customer_type','gbranch.branch_name','gregion.region_name','gsample.product_type']);

        //Promotional Product Chart Count
        $promotional = [];
        for ($i=1; $i<=12; $i++) {

            $res_promotional = DB::table('gpff_delivered_samples as gdeliversample')
                      ->join('gpff_samples as gsample','gsample.samples_id','gdeliversample.samples_id')
                      ->whereYear('gdeliversample.created_at', date('Y'))
                      ->whereMonth('gdeliversample.created_at', $i)
                      ->where('gsample.product_type','1')
                      ->sum('gdeliversample.samples_qty');
            
            array_push($promotional, $res_promotional);

        }

        //Generic Product Chart Count
        $generic = [];
        for ($i=1; $i<=12; $i++) {

            $res_generic = DB::table('gpff_delivered_samples as gdeliversample')
                      ->join('gpff_samples as gsample','gsample.samples_id','gdeliversample.samples_id')
                      ->whereYear('gdeliversample.created_at', date('Y'))
                      ->whereMonth('gdeliversample.created_at', $i)
                      ->where('gsample.product_type','2')
                      ->sum('gdeliversample.samples_qty');

            array_push($generic, $res_generic);
        }


         //Doctor Based Promotional Product in Pie Chart Count
        $DoctorBasPromo = [];
        $DoctorBasGeneric = [];
        $DoctorBas = DB::table('gpff_delivered_samples')
                      ->whereYear('created_at', date('Y'))
                      ->distinct('customer_id')
                      ->get(['customer_id']);

        foreach ($DoctorBas as $value) {

          $Doctor = [];

          $name =     DB::table('gpff_customer')
                      ->where('customer_id',$value->customer_id)
                      ->First();
          if($name){
            $Doctor['Name'] = $name->customer_name;
          }
          
          $Doctor['Promo_Count'] = DB::table('gpff_delivered_samples as gdeliversample')
                      ->join('gpff_samples as gsample','gsample.samples_id','gdeliversample.samples_id')
                      ->where('gsample.product_type','1')
                      ->where('gdeliversample.customer_id',$value->customer_id)
                      ->sum('gdeliversample.samples_qty');

          array_push($DoctorBasPromo, $Doctor);
        }

        foreach ($DoctorBas as $value) {

          $Doctor = [];

          $name =     DB::table('gpff_customer')
                      ->where('customer_id',$value->customer_id)
                      ->First();

          if($name){
            $Doctor['Name'] = $name->customer_name;
          }
          
          $Doctor['Promo_Count'] = DB::table('gpff_delivered_samples as gdeliversample')
                      ->join('gpff_samples as gsample','gsample.samples_id','gdeliversample.samples_id')
                      ->where('gsample.product_type','2')
                      ->where('gdeliversample.customer_id',$value->customer_id)
                      ->sum('gdeliversample.samples_qty');

          array_push($DoctorBasGeneric, $Doctor);
        }

        $total_samples_deliveried =  DB::table('gpff_delivered_samples')
                                    ->whereYear('created_at', date('Y'))
                                    ->sum('samples_qty');

        $total_promotional_samples_deliveried =  DB::table('gpff_delivered_samples as gdeliversample')
                                    ->join('gpff_samples as gsample','gsample.samples_id','gdeliversample.samples_id')
                                    ->where('gsample.product_type','1')
                                    ->whereYear('gdeliversample.created_at', date('Y'))
                                    ->sum('gdeliversample.samples_qty');

        $total_generic_samples_deliveried =  DB::table('gpff_delivered_samples as gdeliversample')
                                    ->join('gpff_samples as gsample','gsample.samples_id','gdeliversample.samples_id')
                                    ->where('gsample.product_type','2')
                                    ->whereYear('gdeliversample.created_at', date('Y'))
                                    ->sum('gdeliversample.samples_qty');

        $result = array_merge(
                              ['sample_details'=>$samples->toArray()], 
                              ['promotional_chart_count' => $promotional],
                              ['generic_chart_count' => $generic],
                              ['doctorbased_promo_product' => $DoctorBasPromo],
                              ['doctorbased_generic_product' => $DoctorBasGeneric],
                              ['total_samples_deliveried' => $total_samples_deliveried],
                              ['promotion_samples_delivered' => $total_promotional_samples_deliveried],
                              ['generic_samples_delivered' => $total_generic_samples_deliveried]
                          );

        return $result;

    }


    //Get Filter Based Sales Report
    public function getFilterSamplesReport($report)
    {   
        $samples = DB::table('gpff_delivered_samples as gdeliversample')
                ->join('gpff_samples as gsample','gsample.samples_id','gdeliversample.samples_id')
                ->join('gpff_customer as gcustomer','gdeliversample.customer_id','gcustomer.customer_id')
                ->join('gpff_branch as gbranch','gsample.branch_id','gbranch.branch_id')
                ->join('gpff_region as gregion','gsample.region_id','gregion.region_id')
                ->orderBy('gdeliversample.created_at', 'DESC');

        $sampleshospha = DB::table('gpff_delivered_samples as gdeliversample')
                ->join('gpff_samples as gsample','gsample.samples_id','gdeliversample.samples_id')
                ->join('gpff_customer as gcustomer','gdeliversample.customer_id','gcustomer.customer_id')
                ->join('gpff_branch as gbranch','gsample.branch_id','gbranch.branch_id')
                ->join('gpff_region as gregion','gsample.region_id','gregion.region_id');
                //->orderBy('gdeliversample.created_at', 'DESC');

        $total_promotional = DB::table('gpff_delivered_samples as gdeliversample')
                ->join('gpff_samples as gsample','gsample.samples_id','gdeliversample.samples_id')
                ->join('gpff_customer as gcustomer','gdeliversample.customer_id','gcustomer.customer_id')
                ->join('gpff_branch as gbranch','gsample.branch_id','gbranch.branch_id')
                ->join('gpff_region as gregion','gsample.region_id','gregion.region_id')
                ->orderBy('gdeliversample.created_at', 'DESC');

        $total_generic = DB::table('gpff_delivered_samples as gdeliversample')
                ->join('gpff_samples as gsample','gsample.samples_id','gdeliversample.samples_id')
                ->join('gpff_customer as gcustomer','gdeliversample.customer_id','gcustomer.customer_id')
                ->join('gpff_branch as gbranch','gsample.branch_id','gbranch.branch_id')
                ->join('gpff_region as gregion','gsample.region_id','gregion.region_id')
                ->orderBy('gdeliversample.created_at', 'DESC');

        $total_samples =  DB::table('gpff_delivered_samples as gdeliversample')
                ->join('gpff_samples as gsample','gsample.samples_id','gdeliversample.samples_id')
                ->join('gpff_customer as gcustomer','gdeliversample.customer_id','gcustomer.customer_id')
                ->join('gpff_branch as gbranch','gsample.branch_id','gbranch.branch_id')
                ->join('gpff_region as gregion','gsample.region_id','gregion.region_id')
                ->orderBy('gdeliversample.created_at', 'DESC');

     
        if($report->branch_id != ''){
            $samples = $samples->whereIn('gsample.branch_id',$report->branch_id);
            $sampleshospha = $sampleshospha->whereIn('gsample.branch_id',$report->branch_id);
            $total_promotional = $total_promotional->whereIn('gsample.branch_id',$report->branch_id);
            $total_generic = $total_generic->whereIn('gsample.branch_id',$report->branch_id);
            $total_samples = $total_samples->whereIn('gsample.branch_id',$report->branch_id);
        }
        if($report->region_id != ''){
             $samples = $samples->whereIn('gsample.region_id',$report->region_id);
             $sampleshospha = $sampleshospha->whereIn('gsample.region_id',$report->region_id);
             $total_promotional = $total_promotional->whereIn('gsample.region_id',$report->region_id);
             $total_generic = $total_generic->whereIn('gsample.region_id',$report->region_id);
             $total_samples = $total_samples->whereIn('gsample.region_id',$report->region_id);
        }
        if($report->customer_id != ''){
            $samples = $samples->whereIn('gdeliversample.customer_id',$report->customer_id);
            $sampleshospha = $sampleshospha->whereIn('gdeliversample.customer_id',$report->customer_id);
            $total_promotional = $total_promotional->whereIn('gdeliversample.customer_id',$report->customer_id);
            $total_generic = $total_generic->whereIn('gdeliversample.customer_id',$report->customer_id);
            $total_samples = $total_samples->whereIn('gdeliversample.customer_id',$report->customer_id);
        }
        if($report->field_officer_id != ''){
            $samples = $samples->whereIn('gdeliversample.field_officer_id',$report->field_officer_id); 
            $sampleshospha = $sampleshospha->whereIn('gdeliversample.field_officer_id',$report->field_officer_id); 
            $total_promotional = $total_promotional->whereIn('gdeliversample.field_officer_id',$report->field_officer_id); 
            $total_generic = $total_generic->whereIn('gdeliversample.field_officer_id',$report->field_officer_id); 
            $total_samples = $total_samples->whereIn('gdeliversample.field_officer_id',$report->field_officer_id); 
        }
        if($report->year != ''){
            $samples = $samples->whereYear("gdeliversample.created_at",$report->year);
            $sampleshospha = $sampleshospha->whereYear("gdeliversample.created_at",$report->year);
            $total_promotional = $total_promotional->whereYear("gdeliversample.created_at",$report->year);
            $total_generic = $total_generic->whereYear("gdeliversample.created_at",$report->year);
            $total_samples = $total_samples->whereYear("gdeliversample.created_at",$report->year);
        }
        if($report->months != ''){
            $samples = $samples->whereIn(DB::raw('MONTH(gdeliversample.created_at)'), $report->months);
            $sampleshospha = $sampleshospha->whereIn(DB::raw('MONTH(gdeliversample.created_at)'), $report->months);
            $total_promotional = $total_promotional->whereIn(DB::raw('MONTH(gdeliversample.created_at)'), $report->months);
            $total_generic = $total_generic->whereIn(DB::raw('MONTH(gdeliversample.created_at)'), $report->months);
            $total_samples = $total_samples->whereIn(DB::raw('MONTH(gdeliversample.created_at)'), $report->months);
        }

        //$res = ['1','2','3','4','5','6','7','8','9','10','11','12'];

        $promotional_res = [];
        $generic_res = [];

        foreach ($report->months as $value)
        {  
           $promotional['Month'] = $value;
           $promotional['count'] = DB::table('gpff_delivered_samples as gdeliversample')
                ->join('gpff_samples as gsample','gsample.samples_id','gdeliversample.samples_id')
                ->where(
                    DB::raw('MONTH(gdeliversample.created_at)'), $value)
                 ->where('gsample.product_type','1')
                ->where(function($query) use ($report) {
                    if($report->branch_id != '') {
                        $query->whereIn('gsample.branch_id',$report->branch_id);
                    }
                 })
                ->where(function($query) use ($report) {
                    if($report->region_id != '') {
                        $query->whereIn('gsample.region_id',$report->region_id);
                    }
                 })
                ->where(function($query) use ($report) {
                    if($report->customer_id != '') {
                        $query->whereIn('gdeliversample.customer_id', $report->customer_id);
                    }
                 })
                ->where(function($query) use ($report) {
                    if($report->field_officer_id != '') {
                        $query->whereIn('gdeliversample.field_officer_id', $report->field_officer_id);
                    }
                 })
                ->where(function($query) use ($report) {
                    if($report->year != '') {
                        $query->whereYear("gdeliversample.created_at",$report->year);
                    }
                 })
            ->sum('gdeliversample.samples_qty');

             array_push($promotional_res, $promotional);
        }


         foreach ($report->months as $value)
        {  
           $generic['Month'] = $value;
           $generic['count'] =  DB::table('gpff_delivered_samples as gdeliversample')
                ->join('gpff_samples as gsample','gsample.samples_id','gdeliversample.samples_id')
                ->where(
                    DB::raw('MONTH(gdeliversample.created_at)'), $value)
                 ->where('gsample.product_type','2')
                ->where(function($query) use ($report) {
                    if($report->branch_id != '') {
                        $query->whereIn('gsample.branch_id',$report->branch_id);
                    }
                 })
                ->where(function($query) use ($report) {
                    if($report->region_id != '') {
                        $query->whereIn('gsample.region_id',$report->region_id);
                    }
                 })
                ->where(function($query) use ($report) {
                    if($report->customer_id != '') {
                        $query->whereIn('gdeliversample.customer_id', $report->customer_id);
                    }
                 })
                ->where(function($query) use ($report) {
                    if($report->field_officer_id != '') {
                        $query->whereIn('gdeliversample.field_officer_id', $report->field_officer_id);
                    }
                 })
                ->where(function($query) use ($report) {
                    if($report->year != '') {
                        $query->whereYear("gdeliversample.created_at",$report->year);
                    }
                 })
            ->sum('gdeliversample.samples_qty');

             array_push($generic_res, $generic);
        }
        

        //Doctor Based Promotional Product in Pie Chart Count
        $DoctorBasPromo = [];
        $DoctorBasGeneric = [];
        $DoctorBas = $sampleshospha
                      ->distinct('gdeliversample.customer_id')
                      ->get(['gdeliversample.customer_id']);

        foreach ($DoctorBas as $value) {

          $Doctor = [];

          $name =     DB::table('gpff_customer')
                      ->where('customer_id',$value->customer_id)
                      ->First();

          $Doctor['Name'] = $name->customer_name;
          
          $Doctor['Promo_Count'] = DB::table('gpff_delivered_samples as gdeliversample')
                      ->join('gpff_samples as gsample','gsample.samples_id','gdeliversample.samples_id')
                      ->where('gsample.product_type','1')
                      ->where('gdeliversample.customer_id',$value->customer_id)
                      ->sum('gdeliversample.samples_qty');

          array_push($DoctorBasPromo, $Doctor);
        }

        foreach ($DoctorBas as $value) {

          $Doctor = [];

          $name =     DB::table('gpff_customer')
                      ->where('customer_id',$value->customer_id)
                      ->First();

          $Doctor['Name'] = $name->customer_name;
          
          $Doctor['Generic_Count'] = DB::table('gpff_delivered_samples as gdeliversample')
                      ->join('gpff_samples as gsample','gsample.samples_id','gdeliversample.samples_id')
                      ->where('gsample.product_type','2')
                      ->where('gdeliversample.customer_id',$value->customer_id)
                      ->sum('gdeliversample.samples_qty');

          array_push($DoctorBasGeneric, $Doctor);
        }


        $total_samples_deliveried = $total_samples
                                    ->sum('gdeliversample.samples_qty');

        $total_promotional_samples_deliveried = $total_promotional
                                    ->where('gsample.product_type','1')
                                    ->sum('gdeliversample.samples_qty');

        $total_generic_samples_deliveried =  $total_generic
                                    ->where('gsample.product_type','2')
                                    ->sum('gdeliversample.samples_qty');

        $samples_det = $samples->get(['gdeliversample.samples_id','gdeliversample.created_at','gdeliversample.samples_name','gdeliversample.samples_qty','gdeliversample.field_officer_name','gdeliversample.field_officer_name','gcustomer.customer_name','gcustomer.customer_type','gbranch.branch_name','gregion.region_name','gsample.product_type']);

 
             $result = array_merge(
                              ['sample_details'=>$samples_det->toArray()], 
                              ['promotional_chart_count' => $promotional_res],
                              ['generic_chart_count' => $generic_res],
                              ['doctorbased_promo_product' => $DoctorBasPromo],
                              ['doctorbased_generic_product' => $DoctorBasGeneric],
                              ['total_samples_deliveried' => $total_samples_deliveried],
                              ['promotion_samples_delivered' => $total_promotional_samples_deliveried],
                              ['generic_samples_delivered' => $total_generic_samples_deliveried]
                          );


        return $result;
    }


    //Get Current Year Gift Report
    public function getCurrentYearGiftReport()
    {   
        $gift = DB::table('gpff_delivered_gift as gdeliveredgift')
                ->join('gpff_gift as ggift','ggift.gift_id','gdeliveredgift.gift_id')
                ->join('gpff_customer as gcustomer','gdeliveredgift.customer_id','gcustomer.customer_id')
                ->join('gpff_branch as gbranch','ggift.branch_id','gbranch.branch_id')
                ->join('gpff_region as gregion','ggift.region_id','gregion.region_id')
                ->whereYear('gdeliveredgift.created_at', date('Y'))
                ->orderBy('gdeliveredgift.created_at', 'DESC')
                ->get(['gdeliveredgift.gift_id','gdeliveredgift.created_at','gdeliveredgift.gift_name','gdeliveredgift.gift_qty','gdeliveredgift.field_officer_name','gcustomer.customer_name','gcustomer.customer_type','gbranch.branch_name','gregion.region_name']);

        //Delivered Gift Bar Chart Count
        $gift_delivered = [];
        for ($i=1; $i<=12; $i++) {

            $res_gift = DB::table('gpff_delivered_gift as gdeliveredgift')
                      ->join('gpff_gift as ggift','ggift.gift_id','gdeliveredgift.gift_id')
                      ->whereYear('gdeliveredgift.created_at', date('Y'))
                      ->whereMonth('gdeliveredgift.created_at', $i)
                      ->sum('gdeliveredgift.gift_qty');
            
            array_push($gift_delivered, $res_gift);

        }

        //Doctor Based Gift in Pie Chart Count
        $DoctorBasGift = [];
        $DoctorBas = DB::table('gpff_delivered_gift')
                      ->whereYear('created_at', date('Y'))
                      ->distinct('customer_id')
                      ->get(['customer_id']);

        foreach ($DoctorBas as $value) {

          $Doctor = [];
          $name =     DB::table('gpff_customer')
                      ->where('customer_id',$value->customer_id)
                      ->where('customer_type',1)
                      ->First();

         if (isset($name->customer_name)){

          $Doctor['Name'] = $name->customer_name;

          $Doctor['Promo_Count'] = DB::table('gpff_delivered_gift')
                      ->where('customer_id',$value->customer_id)
                      ->sum('gift_qty');

          array_push($DoctorBasGift, $Doctor);
         }
        }
        //Pharma Based Gift in Pie Chart Count
        $PharmaBasGift = [];
        $PharmaBas = DB::table('gpff_delivered_gift')
                      ->whereYear('created_at', date('Y'))
                      ->distinct('customer_id')
                      ->get(['customer_id']);


        foreach ($PharmaBas as $value) {

          $Pharmacy = [];

          $name =     DB::table('gpff_customer')
                      ->where('customer_id',$value->customer_id)
                      ->where('customer_type',2)
                      ->First();

          if (isset($name->customer_name)){

          $Pharmacy['Name'] = $name->customer_name;
          
          $Pharmacy['Promo_Count'] = DB::table('gpff_delivered_gift')
                      ->where('customer_id',$value->customer_id)
                      ->sum('gift_qty');

          array_push($PharmaBasGift, $Pharmacy);
        }
        }
        //Hospital Based Gift in Pie Chart Count
        $HospitalBasGift = [];
        $HospitalBas = DB::table('gpff_delivered_gift')
                      ->whereYear('created_at', date('Y'))
                      ->distinct('customer_id')
                      ->get(['customer_id']);

        foreach ($HospitalBas as $value) {

          $Hospital = [];

          $name =     DB::table('gpff_customer')
                      ->where('customer_id',$value->customer_id)
                      ->where('customer_type',3)
                      ->First();

          if (isset($name->customer_name)){

          $Hospital['Name'] = $name->customer_name;
          
          $Hospital['Promo_Count'] = DB::table('gpff_delivered_gift')
                      ->where('customer_id',$value->customer_id)
                      ->sum('gift_qty');

          array_push($HospitalBasGift, $Hospital);
        }
        }


        $total_gift_deliveried =  DB::table('gpff_delivered_gift')
                                    ->whereYear('created_at', date('Y'))
                                    ->sum('gift_qty');

        $result = array_merge(
                              ['gift_details' => $gift->toArray()], 
                              ['gift_delivered_chart_count' => $gift_delivered],
                              ['doctorbased_gift' => $DoctorBasGift],
                              ['pharmacybased_gift' => $PharmaBasGift],
                              ['hospitalbased_gift' => $HospitalBasGift],
                              ['total_gift_deliveried' => $total_gift_deliveried]
                          );

        return $result;

    }


    //Get Filter Based Gift Report
    public function getFilterGiftReport($report)
    {   
        $gift = DB::table('gpff_delivered_gift as gdeliveredgift')
                ->join('gpff_gift as ggift','ggift.gift_id','gdeliveredgift.gift_id')
                ->join('gpff_customer as gcustomer','gdeliveredgift.customer_id','gcustomer.customer_id')
                ->join('gpff_branch as gbranch','ggift.branch_id','gbranch.branch_id')
                ->join('gpff_region as gregion','ggift.region_id','gregion.region_id')
                ->orderBy('gdeliveredgift.created_at', 'DESC');

        $giftdocph = DB::table('gpff_delivered_gift as gdeliveredgift')
                ->join('gpff_gift as ggift','ggift.gift_id','gdeliveredgift.gift_id')
                ->join('gpff_customer as gcustomer','gdeliveredgift.customer_id','gcustomer.customer_id')
                ->join('gpff_branch as gbranch','ggift.branch_id','gbranch.branch_id')
                ->join('gpff_region as gregion','ggift.region_id','gregion.region_id');
                //->orderBy('gdeliveredgift.created_at', 'DESC');

        $total_gift = DB::table('gpff_delivered_gift as gdeliveredgift')
                ->join('gpff_gift as ggift','ggift.gift_id','gdeliveredgift.gift_id')
                ->join('gpff_customer as gcustomer','gdeliveredgift.customer_id','gcustomer.customer_id')
                ->join('gpff_branch as gbranch','ggift.branch_id','gbranch.branch_id')
                ->join('gpff_region as gregion','ggift.region_id','gregion.region_id')
                ->orderBy('gdeliveredgift.created_at', 'DESC');

        $gift_delivered = [];
        
        if($report->branch_id != ''){
            $gift = $gift->whereIn('ggift.branch_id',$report->branch_id);
            $giftdocph = $giftdocph->whereIn('ggift.branch_id',$report->branch_id);
            $total_gift = $total_gift->whereIn('ggift.branch_id',$report->branch_id);
        }
        if($report->region_id != ''){
             $gift = $gift->whereIn('ggift.region_id',$report->region_id);
             $giftdocph = $giftdocph->whereIn('ggift.region_id',$report->region_id);
             $total_gift = $total_gift->whereIn('ggift.region_id',$report->region_id);
        }
        if($report->customer_id != ''){
            $gift = $gift->whereIn('gdeliveredgift.customer_id',$report->customer_id);
            $giftdocph = $giftdocph->whereIn('gdeliveredgift.customer_id',$report->customer_id);
            $total_gift = $total_gift->whereIn('gdeliveredgift.customer_id',$report->customer_id);
        }
        if($report->field_officer_id != ''){
            $gift = $gift->whereIn('gdeliveredgift.field_officer_id',$report->field_officer_id); 
            $giftdocph = $giftdocph->whereIn('gdeliveredgift.field_officer_id',$report->field_officer_id); 
            $total_gift = $total_gift->whereIn('gdeliveredgift.field_officer_id',$report->field_officer_id); 
        }
        if($report->year != ''){
            $gift = $gift->whereYear("gdeliveredgift.created_at",$report->year);
            $giftdocph = $giftdocph->whereYear("gdeliveredgift.created_at",$report->year);
            $total_gift = $total_gift->whereYear("gdeliveredgift.created_at",$report->year);
        }
        if($report->months != ''){
            $gift = $gift->whereIn(DB::raw('MONTH(gdeliveredgift.created_at)'), $report->months);
            $giftdocph = $giftdocph->whereIn(DB::raw('MONTH(gdeliveredgift.created_at)'), $report->months);
            $total_gift = $total_gift->whereIn(DB::raw('MONTH(gdeliveredgift.created_at)'), $report->months);
        }

        //$res = ['1','2','3','4','5','6','7','8','9','10','11','12'];

        foreach ($report->months as $value)
        {  
           $gift_val['Month'] = $value;
           $gift_val['count'] =DB::table('gpff_delivered_gift as gdeliveredgift')
                ->join('gpff_gift as ggift','ggift.gift_id','gdeliveredgift.gift_id')
                ->where(
                    DB::raw('MONTH(gdeliveredgift.created_at)'), $value)

                ->where(function($query) use ($report) {
                    if($report->branch_id != '') {
                        $query->whereIn('ggift.branch_id',$report->branch_id);
                    }
                 })
                ->where(function($query) use ($report) {
                    if($report->region_id != '') {
                        $query->whereIn('ggift.region_id',$report->region_id);
                    }
                 })
                ->where(function($query) use ($report) {
                    if($report->customer_id != '') {
                        $query->whereIn('gdeliveredgift.customer_id', $report->customer_id);
                    }
                 })
                ->where(function($query) use ($report) {
                    if($report->field_officer_id != '') {
                        $query->whereIn('gdeliveredgift.field_officer_id', $report->field_officer_id);
                    }
                 })
                ->where(function($query) use ($report) {
                    if($report->year != '') {
                        $query->whereYear("gdeliveredgift.created_at",$report->year);
                    }
                 })
            ->sum('gdeliveredgift.gift_qty');

             array_push($gift_delivered, $gift_val);
        }

        //Doctor Based Gift in Pie Chart Count
        $DoctorBasGift = [];
        
        $DoctorBas = $giftdocph
                      ->distinct('gdeliveredgift.customer_id')
                      ->get(['gdeliveredgift.customer_id']);

        foreach ($DoctorBas as $value) {

          $Doctor = [];

          $name =     DB::table('gpff_customer')
                      ->where('customer_id',$value->customer_id)
                      ->where('customer_type',1)
                      ->First();
          if (isset($name->customer_name)){

          $Doctor['Name'] = $name->customer_name;
          
          $Doctor['Gift_Count'] =DB::table('gpff_delivered_gift as gdeliveredgift')
                      ->join('gpff_gift as ggift','ggift.gift_id','gdeliveredgift.gift_id')
                      ->where('gdeliveredgift.customer_id',$value->customer_id)
                      ->sum('gdeliveredgift.gift_qty');

          array_push($DoctorBasGift, $Doctor);
        }
        }
        //Pharma Based Gift in Pie Chart Count
        $PharmaBasGift = [];
        $PharmaBas = $giftdocph
                      ->distinct('gdeliveredgift.customer_id')
                      ->get(['gdeliveredgift.customer_id']);

        foreach ($PharmaBas as $value) {

          $Pharma = [];

          $name =     DB::table('gpff_customer')
                      ->where('customer_id',$value->customer_id)
                      ->where('customer_type',2)
                      ->First();

          if (isset($name->customer_name)){

          $Pharma['Name'] = $name->customer_name;
          
          $Pharma['Gift_Count'] =DB::table('gpff_delivered_gift as gdeliveredgift')
                      ->join('gpff_gift as ggift','ggift.gift_id','gdeliveredgift.gift_id')
                      ->where('gdeliveredgift.customer_id',$value->customer_id)
                      ->sum('gdeliveredgift.gift_qty');

          array_push($PharmaBasGift, $Pharma);
        }
        }
        //Hospital Based Gift in Pie Chart Count
        $HospitalBasGift = [];
        $HospitalBas = $giftdocph
                      ->distinct('gdeliveredgift.customer_id')
                      ->get(['gdeliveredgift.customer_id']);

        foreach ($HospitalBas as $value) {

          $Hospital = [];

          $name =     DB::table('gpff_customer')
                      ->where('customer_id',$value->customer_id)
                      ->where('customer_type',3)
                      ->First();
        if (isset($name->customer_name)){

          $Hospital['Name'] = $name->customer_name;
          
          $Hospital['Gift_Count'] =DB::table('gpff_delivered_gift as gdeliveredgift')
                      ->join('gpff_gift as ggift','ggift.gift_id','gdeliveredgift.gift_id')
                      ->where('gdeliveredgift.customer_id',$value->customer_id)
                      ->sum('gdeliveredgift.gift_qty');

          array_push($HospitalBasGift, $Hospital);
        }
        }

        $total_gift_deliveried = $total_gift
                                    ->sum('gdeliveredgift.gift_qty');

        

        $gift_det = $gift->get(['gdeliveredgift.gift_id','gdeliveredgift.created_at','gdeliveredgift.gift_name','gdeliveredgift.gift_qty','gdeliveredgift.field_officer_name','gcustomer.customer_name','gcustomer.customer_type','gbranch.branch_name','gregion.region_name']);

 
            $result = array_merge(
                              ['gift_details' => $gift_det->toArray()], 
                              ['gift_delivered_chart_count' => $gift_delivered],
                              ['doctorbased_gift' => $DoctorBasGift],
                              ['pharmacybased_gift' => $PharmaBasGift],
                              ['hospitalbased_gift' => $HospitalBasGift],
                              ['total_gift_deliveried' => $total_gift_deliveried]
                          );


        return $result;
    }
//APP SIDE Report WITH PDF GENERATION
    //1-Daily Call Report
    public function appDailyCallReport($report){
    
    $today = date('Y-m-d');
    $today1 = date('Y-m-d');
    $today = Carbon::now()->format('Y-m-d').'%';

    $orders = DB::table('gpff_task as gta')
              ->join('gpff_customer as gcus','gta.customer_id', '=' , 'gcus.customer_id')
              ->join('gpff_users as guse','gta.field_officer_id', '=' , 'guse.user_id')
              ->join('gpff_branch as gpbr','gta.branch_id', '=' , 'gpbr.branch_id')
              ->join('gpff_region as greg','gta.region_id', '=' , 'greg.region_id')
              ->join('gpff_area as gpar','gta.area_id', '=' , 'gpar.area_id')
              ->whereBetween(DB::RAW('date(gta.created_at)'), [date($report->start_date), date($report->end_date)])
              //->where('gta.task_date_time', 'like', $today)
              ->orderBy('gta.created_at', 'DESC');
     
      if($report->branch_id != ''){
          $orders = $orders->whereIn('gta.branch_id',$report->branch_id);
      }
      if($report->region_id != ''){
           $orders = $orders->whereIn('gta.region_id',$report->region_id);
      }
      if($report->area_id != ''){
          $orders = $orders->whereIn('gta.area_id',$report->area_id);
      }
      if($report->field_officer_id != ''){
          $orders = $orders->whereIn('gta.field_officer_id',$report->field_officer_id);        
      }

      $order_det = $orders->get(['gta.task_id','gta.created_by','gta.field_officer_id','guse.firstname','gta.customer_id','gta.area_manager_id','gta.task_title','gta.task_desc','gta.task_status','gta.task_date_time','gta.task_reassign_status','gta.task_start_date_time','gta.task_end_date_time','gta.task_active_minu','gta.notes','gta.created_at','gta.updated_at','gcus.customer_name','gcus.customer_email','gcus.country_code','gcus.customer_mobile','gcus.customer_city','gcus.customer_country','gcus.customer_fax','gcus.customer_address','gcus.customer_location','gcus.customer_lan','gcus.customer_lat','gcus.customer_cr_id','gcus.website','gcus.avatar','gcus.is_active','gcus.customer_type','gta.branch_id','gta.region_id','gta.area_id','gpbr.branch_name','greg.region_name','gpar.area_name']);

      if(count($order_det) >0){
        $customPaper = array(0,0,767.00,883.80);

        view()->share('datas',$order_det);
        view()->share('Start_date',$today1);
        view()->share('End_date',$today1);
        view()->share('Title','Daily Call Report');
        $pdf = PDF::loadView('DailyCallReport')
        ->setPaper('a4', 'landscape');
        $pdf->save(public_path('Report/DailyCallReport.pdf'));

        $file_name = 'DailyCallReport.pdf';
        $name = $file_name;
        $filePath = 'Report/DailyCallReport/'.$name;
      
        Storage::disk('s3')->put($filePath, file_get_contents(public_path('Report')."/".$file_name));  

        $this->sendReportToEmail($report->email_id,$file_name,$filePath); 
       
        return 1;
      } else{
        return 2;
      }
  }
  //2-App Visited Report
  public function appVisitedReport($report)
  {   
      $visited = DB::table('gpff_task as gta')
                ->join('gpff_customer as gcus','gta.customer_id', '=' , 'gcus.customer_id')
                ->join('gpff_users as guse','gta.field_officer_id', '=' , 'guse.user_id')
                ->join('gpff_branch as gpbr','gta.branch_id', '=' , 'gpbr.branch_id')
                ->join('gpff_region as greg','gta.region_id', '=' , 'greg.region_id')
                ->join('gpff_area as gpar','gta.area_id', '=' , 'gpar.area_id')
                ->where('gta.task_status', 3)
                ->orderBy('gta.created_at', 'DESC');
     
        if($report->branch_id != ''){
            $visited = $visited->whereIn('gta.branch_id',$report->branch_id);
        }
        if($report->region_id != ''){
             $visited = $visited->whereIn('gta.region_id',$report->region_id);
        }
        if($report->area_id != ''){
            $visited = $visited->whereIn('gta.area_id',$report->area_id);
        }
        if($report->field_officer_id != ''){
            $visited = $visited->whereIn('gta.field_officer_id',$report->field_officer_id); 
        }
        if($report->year != ''){
            $visited = $visited->whereYear("gta.created_at",$report->year);
        }
        if($report->months != ''){
            $visited = $visited->whereIn(DB::raw('MONTH(gta.created_at)'), $report->months);
        }

        $visited_de = $visited->get(['gta.task_id','gta.created_by','gta.field_officer_id','guse.firstname','gta.customer_id','gta.area_manager_id','gta.task_title','gta.task_desc','gta.task_status','gta.task_date_time','gta.task_reassign_status','gta.task_start_date_time','gta.task_end_date_time','gta.task_active_minu','gta.notes','gta.created_at','gta.updated_at','gcus.customer_name','gcus.customer_email','gcus.country_code','gcus.customer_mobile','gcus.customer_city','gcus.customer_country','gcus.customer_fax','gcus.customer_address','gcus.customer_location','gcus.customer_lan','gcus.customer_lat','gcus.customer_cr_id','gcus.website','gcus.avatar','gcus.customer_type','gcus.is_active','gta.branch_id','gta.region_id','gta.area_id','gpbr.branch_name','greg.region_name','gpar.area_name']);

      /*print_r($visited_de);
      exit;*/
      if(count($visited_de) >0){
        $customPaper = array(0,0,767.00,883.80);

        view()->share('datas',$visited_de);
        view()->share('Month',$report->year);
        view()->share('Title','Visited Report');
        $pdf = PDF::loadView('VisitedReport')
        ->setPaper('a4', 'landscape');
        $pdf->save(public_path('Report/VisitedReport.pdf'));

        $file_name = 'VisitedReport.pdf';
        $name = $file_name;
        $filePath = 'Report/VisitedReport/'.$name;
      
        Storage::disk('s3')->put($filePath, file_get_contents(public_path('Report')."/".$file_name));  

        $this->sendReportToEmail($report->email_id,$file_name,$filePath); 
       
        return 1;
      } else{
        return 2;
      }
  }
  
  /// 3-App Sales Report
  public function appSalesReport($report)
  {   
      $orders = DB::table('gpff_order as gorder')
              ->join('gpff_order_list as gorderlist','gorderlist.order_id','gorder.order_id')
              ->join('gpff_branch as gbranch','gorder.branch_id','gbranch.branch_id')
              ->join('gpff_region as gregion','gorder.region_id','gregion.region_id')
              ->join('gpff_area as garea','gorder.area_id','garea.area_id')
              ->orderBy('gorder.created_at', 'DESC');

      if($report->branch_id != ''){
          $orders = $orders->whereIn('gorder.branch_id',$report->branch_id);
      }
      if($report->region_id != ''){
           $orders = $orders->whereIn('gorder.region_id',$report->region_id);
      }
      if($report->area_id != ''){
          $orders = $orders->whereIn('gorder.area_id',$report->area_id);
      }
      if($report->customer_id != ''){
          $orders = $orders->whereIn('gorder.customer_id',$report->customer_id);
      }
      if($report->field_officer_id != ''){
          $orders = $orders->whereIn('gorder.field_officer_id',$report->field_officer_id); 
      }
      if($report->product_id != ''){
          $orders = $orders->whereIn('gorderlist.product_id', $report->product_id);
      }
      if($report->year != ''){
          $orders = $orders->whereYear("gorder.created_at",$report->year);
      }
      if($report->months != ''){
          $orders = $orders->whereIn(DB::raw('MONTH(gorder.created_at)'), $report->months);
      }

      $order_det = $orders->get(['gorder.order_id','gorder.order_date','gorder.customer_name','gorderlist.product_genericname','gorderlist.product_qty','gorder.field_officer_name','gorderlist.product_type','gbranch.branch_name','gregion.region_name','garea.area_name','gorderlist.product_tot_price']);

      if(count($order_det) >0){
        $customPaper = array(0,0,767.00,883.80);

        view()->share('datas',$order_det);
        view()->share('Month',$report->year);
        view()->share('Title','Sales Report');
        $pdf = PDF::loadView('SalesReport')
        ->setPaper('a4', 'landscape');
        $pdf->save(public_path('Report/SalesReport.pdf'));

        $file_name = 'SalesReport.pdf';
        $name = $file_name;
        $filePath = 'Report/SalesReport/'.$name;
      
        Storage::disk('s3')->put($filePath, file_get_contents(public_path('Report')."/".$file_name));  

        $this->sendReportToEmail($report->email_id,$file_name,$filePath); 
       
        return 1;
      } else{
        return 2;
      }
  }

    ///4-App coverage Report
  public function appCoverageReport($report)
  {   
      $task = DB::table('gpff_task as gtask')
                ->join('gpff_customer as gcustomer','gcustomer.customer_id','gtask.customer_id')
                ->join('gpff_users as gusers','gtask.field_officer_id','gusers.user_id')
                ->join('gpff_branch as gbranch','gbranch.branch_id','gtask.branch_id')
                ->join('gpff_region as gregion','gtask.region_id','gregion.region_id')
                ->join('gpff_area as garea','gtask.area_id','garea.area_id')
                ->orderBy('gtask.task_date_time', 'DESC');

      if($report->branch_id != ''){
          $task = $task->whereIn('gtask.branch_id',$report->branch_id);
      }
      if($report->region_id != ''){
           $task = $task->whereIn('gtask.region_id',$report->region_id);
      }
      if($report->area_id != ''){
          $task = $task->whereIn('gtask.area_id',$report->area_id);
      }
      if($report->customer_id != ''){
          $task = $task->whereIn('gtask.customer_id',$report->customer_id);
      }
      if($report->field_officer_id != ''){
          $task = $task->whereIn('gtask.field_officer_id',$report->field_officer_id);
      }
      if($report->year != ''){
          $task = $task->whereYear("gtask.task_date_time", $report->year);
      }
      if($report->months != ''){
          $task = $task->whereIn(DB::raw('MONTH(gtask.task_date_time)'), $report->months);
      }

      $res_task = $task ->get(['gtask.task_id','gtask.task_title','gcustomer.customer_name','gtask.task_date_time','gtask.task_status','gusers.firstname','gusers.lastname','gbranch.branch_name','gregion.region_name','garea.area_name','gtask.customer_type','gcustomer.customer_location']);

      if(count($res_task) >0){
        $customPaper = array(0,0,767.00,883.80);

        view()->share('datas',$res_task);
        view()->share('Month',$report->year);
        view()->share('Title','Coverage Report');
        $pdf = PDF::loadView('CoverageReport')
        ->setPaper('a4', 'landscape');
        $pdf->save(public_path('Report/CoverageReport.pdf'));

        $file_name = 'CoverageReport.pdf';
        $name = $file_name;
        $filePath = 'Report/CoverageReport/'.$name;
      
        Storage::disk('s3')->put($filePath, file_get_contents(public_path('Report')."/".$file_name));  

        $this->sendReportToEmail($report->email_id,$file_name,$filePath); 
       
        return 1;
      } else{
        return 2;
      }
  }

  ///5-App Samples Report
  public function appSamplesReport($report)
  {   
      $samples = DB::table('gpff_delivered_samples as gdeliversample')
                ->join('gpff_samples as gsample','gsample.samples_id','gdeliversample.samples_id')
                ->join('gpff_customer as gcustomer','gdeliversample.customer_id','gcustomer.customer_id')
                ->join('gpff_branch as gbranch','gsample.branch_id','gbranch.branch_id')
                ->join('gpff_region as gregion','gsample.region_id','gregion.region_id')
                ->orderBy('gdeliversample.created_at', 'DESC');

      if($report->branch_id != ''){
            $samples = $samples->whereIn('gsample.branch_id',$report->branch_id);
        }
        if($report->region_id != ''){
             $samples = $samples->whereIn('gsample.region_id',$report->region_id);
        }
        if($report->customer_id != ''){
            $samples = $samples->whereIn('gdeliversample.customer_id',$report->customer_id);
        }
        if($report->field_officer_id != ''){
            $samples = $samples->whereIn('gdeliversample.field_officer_id',$report->field_officer_id); 
        }
        if($report->year != ''){
            $samples = $samples->whereYear("gdeliversample.created_at",$report->year);
        }
        if($report->months != ''){
            $samples = $samples->whereIn(DB::raw('MONTH(gdeliversample.created_at)'), $report->months);
        }

      $samples_det = $samples->get(['gdeliversample.samples_id','gdeliversample.created_at','gdeliversample.samples_name','gdeliversample.samples_qty','gdeliversample.field_officer_name','gdeliversample.field_officer_name','gcustomer.customer_name','gcustomer.customer_type','gbranch.branch_name','gregion.region_name','gsample.product_type']);

      if(count($samples_det) >0){
        $customPaper = array(0,0,767.00,883.80);

        view()->share('datas',$samples_det);
        view()->share('Month',$report->year);
        view()->share('Title','Samples Report');
        $pdf = PDF::loadView('SamplesReport')
        ->setPaper('a4', 'landscape');
        $pdf->save(public_path('Report/SamplesReport.pdf'));

        $file_name = 'SamplesReport.pdf';
        $name = $file_name;
        $filePath = 'Report/SamplesReport/'.$name;
      
        Storage::disk('s3')->put($filePath, file_get_contents(public_path('Report')."/".$file_name));  

        $this->sendReportToEmail($report->email_id,$file_name,$filePath); 
       
        return 1;
      } else{
        return 2;
      }
  }

  ///6-App Gift Report
  public function appGiftReport($report)
  {   
      $gift = DB::table('gpff_delivered_gift as gdeliveredgift')
                ->join('gpff_gift as ggift','ggift.gift_id','gdeliveredgift.gift_id')
                ->join('gpff_customer as gcustomer','gdeliveredgift.customer_id','gcustomer.customer_id')
                ->join('gpff_branch as gbranch','ggift.branch_id','gbranch.branch_id')
                ->join('gpff_region as gregion','ggift.region_id','gregion.region_id')
                ->orderBy('gdeliveredgift.created_at', 'DESC');

      if($report->branch_id != ''){
            $gift = $gift->whereIn('ggift.branch_id',$report->branch_id);
        }
        if($report->region_id != ''){
             $gift = $gift->whereIn('ggift.region_id',$report->region_id);
        }
        if($report->customer_id != ''){
            $gift = $gift->whereIn('gdeliveredgift.customer_id',$report->customer_id);
        }
        if($report->field_officer_id != ''){
            $gift = $gift->whereIn('gdeliveredgift.field_officer_id',$report->field_officer_id); 
        }
        if($report->year != ''){
            $gift = $gift->whereYear("gdeliveredgift.created_at",$report->year);
        }
        if($report->months != ''){
            $gift = $gift->whereIn(DB::raw('MONTH(gdeliveredgift.created_at)'), $report->months);
        }

      $gift_det = $gift->get(['gdeliveredgift.gift_id','gdeliveredgift.created_at','gdeliveredgift.gift_name','gdeliveredgift.gift_qty','gdeliveredgift.field_officer_name','gcustomer.customer_name','gcustomer.customer_type','gbranch.branch_name','gregion.region_name']);

      if(count($gift_det) >0){
        $customPaper = array(0,0,767.00,883.80);

        view()->share('datas',$gift_det);
        view()->share('Month',$report->year);
        view()->share('Title','Gifts Report');
        $pdf = PDF::loadView('GiftsReport')
        ->setPaper('a4', 'landscape');
        $pdf->save(public_path('Report/GiftsReport.pdf'));

        $file_name = 'GiftsReport.pdf';
        $name = $file_name;
        $filePath = 'Report/GiftsReport/'.$name;
      
        Storage::disk('s3')->put($filePath, file_get_contents(public_path('Report')."/".$file_name));  

        $this->sendReportToEmail($report->email_id,$file_name,$filePath); 
       
        return 1;
      } else{
        return 2;
      }
  }

  ///7-App Target Report
  public function appTargetReport($report)
  {   
      $target = DB::table('gpff_target as gpta')
              ->join('gpff_users as guse','gpta.target_user_id', '=' , 'guse.user_id')
              ->join('gpff_branch as gpbr','gpta.branch_id', '=' , 'gpbr.branch_id')
              ->join('gpff_region as greg','gpta.region_id', '=' , 'greg.region_id')
              ->join('gpff_area as gpar','gpta.area_id', '=' , 'gpar.area_id')
              ->orderBy('gpta.created_at', 'DESC');
     
      if($report->branch_id != ''){
          $orders = $target->whereIn('gpta.branch_id',$report->branch_id);
      }
      if($report->region_id != ''){
           $target = $target->whereIn('gpta.region_id',$report->region_id);
      }
      if($report->area_id != ''){
          $target = $target->whereIn('gpta.area_id',$report->area_id);
      }
      if($report->field_officer_id != ''){
          $target = $target->whereIn('gpta.target_user_id',$report->field_officer_id);        
      }
      if($report->year != ''){
            $target = $target->whereYear("gpta.created_at",$report->year);
      }
      if($report->months != ''){
            $target = $target->whereIn(DB::raw('MONTH(gpta.created_at)'), $report->months);
      }

      $target_det = $target->get(['gpta.target_id','gpta.target_user_id','gpta.target_user_name','gpta.target_ass_userid','gpta.target_ass_name','gpta.target_type','gpta.target_tot_price','gpta.target_achived_price','gpta.target_pending_price','gpta.target_percentage','gpta.target_product_id','gpta.target_product_tot_qty','gpta.target_product_achived_qty','gpta.target_product_amt','gpta.target_product_pending_qty','gpta.target_tenure','gpta.target_tenure_start','gpta.target_tenure_end','gpta.branch_id','gpta.region_id','gpta.area_id','gpta.target_status','gpta.created_at','gpta.updated_at','guse.firstname','gpbr.branch_name','greg.region_name','gpar.area_name']);

      if(count($target_det) >0){
        $customPaper = array(0,0,767.00,883.80);

        view()->share('datas',$target_det);
        view()->share('Month',$report->year);
        view()->share('Title','Target Report');
        $pdf = PDF::loadView('TargetReport')
        ->setPaper('a4', 'landscape');
        $pdf->save(public_path('Report/TargetReport.pdf'));

        $file_name = 'TargetReport.pdf';
        $name = $file_name;
        $filePath = 'Report/TargetReport/'.$name;
      
        Storage::disk('s3')->put($filePath, file_get_contents(public_path('Report')."/".$file_name));  

        $this->sendReportToEmail($report->email_id,$file_name,$filePath); 
       
        return 1;
      } else{
        return 2;
      }
  }

  //7-App Side Target Chart Data
  public function appTargetDetailChart($report){

        $target = DB::table('gpff_insentive as gpinc')
              ->leftjoin('gpff_branch as gpbr','gpinc.branch_id', '=' , 'gpbr.branch_id')
              ->leftjoin('gpff_region as greg','gpinc.region_id', '=' , 'greg.region_id')
              ->where('gpinc.fo_id',$report->field_officer_id)
              ->orderBy('gpinc.from_date', 'DESC'); 
      if($report->year != ''){
            $target = $target->whereYear("gpinc.from_date",$report->year);
      }
      if($report->months != ''){
            $target = $target->whereIn(DB::raw('MONTH(gpinc.from_date)'), $report->months);
      }

      $target_det = $target->get(['gpinc.insentive_id','gpinc.fo_id','gpinc.fo_name','gpinc.created_by','gpinc.insentive_type','gpinc.product_id','gpinc.product_name','gpinc.unit','gpinc.insentive_amt','gpinc.target_amt','gpinc.from_date','gpinc.to_date','gpinc.branch_id','gpbr.branch_name','greg.region_name','gpinc.region_id','gpinc.insentive_status']);

    $pricedata = [];
      foreach ($report->months as $value)
      {  
           $price['Month'] = $value;
           $pricede =  DB::table('gpff_insentive')
                      ->where(DB::raw('MONTH(from_date)'), $value)
                      ->where('insentive_type',1)
                      ->where('fo_id',$report->field_officer_id)
                      ->where(function($query) use ($report) {
                        if($report->year != '') {
                          $query->whereYear("from_date",$report->year);
                          }
                        });

            $price['Tot_price'] = $pricede->sum('target_amt');
/*            $price['Achived_price'] = $pricede->sum('target_achived_price');
            $price['Pending_price'] = $pricede->sum('target_pending_price');
*/
             array_push($pricedata, $price);
        }

      $productdata = [];
      foreach ($report->months as $value)
      {  
           $product['Month'] = $value;
           $productde =  DB::table('gpff_insentive')
                        ->where(DB::raw('MONTH(created_at)'), $value)
                        ->where('insentive_type',2)
                        ->where('fo_id',$report->field_officer_id)
                        ->where(function($query) use ($report) {
                        if($report->year != '') {
                          $query->whereYear("created_at",$report->year);
                          }
                        });

            $product['Tot_product'] = $productde->sum('unit');
/*            $product['Achived_product'] = $productde->sum('target_product_achived_qty');
            $product['Pending_product'] = $productde->sum('target_product_pending_qty');
*/
             array_push($productdata, $product);
        } 

      $Percentage_data = [];
      foreach ($report->months as $value)
      {  
           $price['Month'] = $value;
           $pricede =  DB::table('gpff_insentive')
                      ->where(DB::raw('MONTH(from_date)'), $value)
                      ->where('insentive_type',3)
                      ->where('fo_id',$report->field_officer_id)
                      ->where(function($query) use ($report) {
                        if($report->year != '') {
                          $query->whereYear("from_date",$report->year);
                          }
                        });

            $price['Tot_price'] = $pricede->sum('target_amt');
/*            $price['Achived_price'] = $pricede->sum('target_achived_price');
            $price['Pending_price'] = $pricede->sum('target_pending_price');
*/
             array_push($Percentage_data, $price);
        }

        $result = array_merge(
            ['Target_details'=>$target_det->toArray()],
            ['Amount_data' => $pricedata],
            ['Product_data' => $productdata],
            ['Percentage_data' => $Percentage_data]
        );

        return $result;
  }

  //9-Field Officer Task Missed Call Report
    public function appMissedTaskCallReport($report){
    
    $today = date('Y-m-d');
    $today1 = date('Y-m-d');
    $today = Carbon::now()->format('Y-m-d').'%';

    $orders = DB::table('gpff_task as gta')
              ->join('gpff_customer as gcus','gta.customer_id', '=' , 'gcus.customer_id')
              ->join('gpff_users as guse','gta.field_officer_id', '=' , 'guse.user_id')
              ->join('gpff_branch as gpbr','gta.branch_id', '=' , 'gpbr.branch_id')
              ->join('gpff_region as greg','gta.region_id', '=' , 'greg.region_id')
              ->join('gpff_area as gpar','gta.area_id', '=' , 'gpar.area_id')
              ->whereBetween(DB::RAW('date(gta.created_at)'), [date($report->start_date), date($report->end_date)])
              //->where('gta.task_date_time', 'like', $today)
              ->orderBy('gta.created_at', 'DESC');
     
      if($report->branch_id != ''){
          $orders = $orders->whereIn('gta.branch_id',$report->branch_id);
      }
      if($report->region_id != ''){
           $orders = $orders->whereIn('gta.region_id',$report->region_id);
      }
      if($report->area_id != ''){
          $orders = $orders->whereIn('gta.area_id',$report->area_id);
      }
      if($report->field_officer_id != ''){
          $orders = $orders->whereIn('gta.field_officer_id',$report->field_officer_id);        
      }

      $order_det = $orders->get(['gta.task_id','gta.created_by','gta.field_officer_id','guse.firstname','gta.customer_id','gta.area_manager_id','gta.task_title','gta.task_desc','gta.task_status','gta.task_date_time','gta.task_reassign_status','gta.task_start_date_time','gta.task_end_date_time','gta.task_active_minu','gta.notes','gta.created_at','gta.updated_at','gcus.customer_name','gcus.customer_email','gcus.country_code','gcus.customer_mobile','gcus.customer_city','gcus.customer_country','gcus.customer_fax','gcus.customer_address','gcus.customer_location','gcus.customer_lan','gcus.customer_lat','gcus.customer_cr_id','gcus.website','gcus.avatar','gcus.is_active','gcus.customer_type','gta.branch_id','gta.region_id','gta.area_id','gpbr.branch_name','greg.region_name','gpar.area_name']);

      if(count($order_det) >0){
        $customPaper = array(0,0,767.00,883.80);

        view()->share('datas',$order_det);
        view()->share('Start_date',$today1);
        view()->share('End_date',$today1);
        view()->share('Title','Task Missed Report');
        $pdf = PDF::loadView('MissedTaskCallReport')
        ->setPaper('a4', 'landscape');
        $pdf->save(public_path('Report/MissedTaskCallReport.pdf'));

        $file_name = 'MissedTaskCallReport.pdf';
        $name = $file_name;
        $filePath = 'Report/MissedTaskCallReport/'.$name;
      
        Storage::disk('s3')->put($filePath, file_get_contents(public_path('Report')."/".$file_name));  

        $this->sendReportToEmail($report->email_id,$file_name,$filePath); 
       
        return 1;
      } else{
        return 2;
      }
  }
//Reports Send To The Mail
    public function sendReportToEmail($report,$file_name,$filePath)
     {
            $pathToFile = 'https://tgetsfa-live.s3.ap-south-1.amazonaws.com/'.$filePath;
           // amazingwits-bucket.s3.ap-south-1.amazonaws.com

             $emailObject = array(
             'user_id' => 0,
             'email_to' => $report,
             'email_cc' => '',
             'email_bcc' => '',
             'email_subject' => 'You Have New Notification from GPFF',
             'email_content' => '',
             'email_template'=> 'sample',
             'email_template_data'=> json_encode(array('data' => 'data')),
             'email_file_path' => $pathToFile,
             'email_file_name' => $file_name,
             'created_date' => date('Y-m-d H:i:s'),
             'updated_date' => date('Y-m-d H:i:s')
            ); 

            $cmn = new Common();
            $cmn->insertEmailQueue($emailObject);

           return "Basic Email Sent. Check your inbox.";
              
    }
}
