<?php
namespace App\Console\Commands;

use Closure;
use DB;
use Mail;
use Carbon\Carbon;

//require(base_path().'/app/Http/Middleware/Common.php');

class Common {

	/**
	* @method insertEmailQueue
	* @desc This method we used for insert the email content store to the * 		table
	* @member private
	* @return 
	*/
	public function insertEmailQueue($emailArray){

		DB::table('gpff_email_queue')
				->insert($emailArray);
	}

	//Notification Table
    public function insertNotification($user_id,$user_name,$notify_user_id,$message,$page_id){

            $notification_values = array(

                'user_id' => $user_id,
                'user_name' => $user_name,
                'notification_user_id' => $notify_user_id,
                'message' => $message,
                'page_id' => $page_id,
                'created_at' => date('Y-m-d H:i:s') , 
                'updated_at' => date('Y-m-d H:i:s')
            );

            DB::table('gpff_notification')
           ->insert($notification_values);

            $push_token = DB::table('gpff_users')
                        ->where('user_id',$notify_user_id)
                        ->first(['web_push_token','app_push_token','os_type']);
        if($push_token){
            if($push_token->app_push_token){
                $this->appPush($push_token,$message,$page_id);
            }
            if($push_token->web_push_token){
                $this->webPush($push_token,$message,$page_id);
            }
        }
           return "sucess";
    }

	//Push
    //Webpush FCM Notification
    //App one signal PUSH NOTIFICATION
    public function webPush($push_token,$message,$page_id){
        
        $content = array(
        "en" => $message,
      );
        $fields = array(

          'app_id' => "a7a11e3f-549e-4bd7-9673-44492124b0ba",
          'include_player_ids' => array($push_token->web_push_token),
          'large_icon' => "https://aw-gpff.s3.ap-south-1.amazonaws.com/icon.png",
          'contents' => $content
          
        );

        $fields = json_encode($fields);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
                               'Authorization: Basic MjQ2ZjZiMDAtNzhlYi00NjA4LTkwY2EtYjljZmIyMTY4Mjlm'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);    
      
        $response = curl_exec($ch);
        //echo "Response-->  ".$response;
        curl_close($ch);

        /*$content = array(
            "en" => $message,
        );
        $fields = array(

            'app_id' => "a7a11e3f-549e-4bd7-9673-44492124b0ba",
            'include_android_reg_ids' => array($push_token->web_push_token),
            'data' => array("page_id" => $page_id),
            'large_icon' => "https://aw-gpff.s3.ap-south-1.amazonaws.com/icon.png",
            'contents' => $content
        );

        $fields = json_encode($fields);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
            'Authorization: Basic MjQ2ZjZiMDAtNzhlYi00NjA4LTkwY2EtYjljZmIyMTY4Mjlm'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);    
    
        $response = curl_exec($ch);
        curl_close($ch);*/
        
        return $response;
    }

    //App one signal PUSH NOTIFICATION
    public function appPush($push_token,$message,$page_id){
        

        $content = array(
            "en" => $message,
        );
    
        if($push_token->os_type == '1'){

            $fields = array(
                'app_id' => "d99b790d-5e89-426f-961e-f799a9899ba3",
                'include_android_reg_ids' => array($push_token->app_push_token),
                'data' => array("page_id" => $page_id),
                'large_icon' => "https://aw-gpff.s3.ap-south-1.amazonaws.com/icon.png",
                'contents' => $content
                
            );

            $fields = json_encode($fields);
    
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
                                                         'Authorization: Basic N2Q1ZTZkNjktNmQ4ZS00NDA5LTk5NDAtMzI5YzY3Yjg3Yjc4'));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_HEADER, FALSE);
            curl_setopt($ch, CURLOPT_POST, TRUE);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);    
        
            $response = curl_exec($ch);
            curl_close($ch);
        
            return $response;
        
        } else if($push_token->os_type == '2'){

            $fields = array(

                'app_id' => "3563ce1c-c356-4673-9dcb-b3a4905374c2",
                'include_ios_tokens' => array($push_token->app_push_token),
                'data' => array("page_id" => $page_id),
                'large_icon' => "https://aw-gpff.s3.ap-south-1.amazonaws.com/icon.png",
                'contents' => $content
                
            );

            $fields = json_encode($fields);
    
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
                                                         'Authorization: Basic NWJiNTc3MzYtMDY4Ny00MTgxLThjYjEtNGZkNjRjOTVhYTEy'));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_HEADER, FALSE);
            curl_setopt($ch, CURLOPT_POST, TRUE);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);    
        
            $response = curl_exec($ch);
            curl_close($ch);
        
            return $response;
        
        }   
    }
//End Push
}

?>