<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
// use Illuminate\Http\Request;
use File;
use Mail;
require(base_path().'/app/Http/Middleware/Sale.php');

class SaleController extends Controller{
		private $Sales;
		function __construct() {
		if (!isset(self::$this->Sales)) {
			$this->Sale = new Sale();
    		
		} 
    	
  	}

  	//=============  STORE SALE  ===========
	public function addSale(Request $request){
		$Sale = new Sale();
		$Sale->warehouse_id 			= $request->input('warehouse_id');
		$Sale->customer_id				= $request->input('customer_id');
		$Sale->customer_name 			= $request->input('customer_name');
		$Sale->customer_code			= $request->input('customer_code');
		$Sale->date						= $request->input('date');
		$Sale->discount_per				= $request->input('discount_per');
		$Sale->vat						= $request->input('vat');
		$Sale->amount					= $request->input('amount');
		$Sale->due 						= $request->input('due');
		$Sale->payment_type 			= $request->input('payment_type');
		$Sale->payment_status 			= $request->input('payment_status');
		// $Sale->sale_status 			    = $request->input('sale_status');
		$Sale->user_id 					= $request->input('user_id');
		$Sale->region_id 				= $request->input('region_id');
		$Sale->branch_id 				= $request->input('branch_id');
		//  ARRAY VALUE
		$Sale->sale_product_list		= $request->input('sale_product_list');
		//  END ARRAY

		$result = $Sale->addSale($Sale);
		// $result = $this->$Purchases->addPurchase($request->input())
		if($result == 1){
			$response = response()->json(['status' => 'success', 'message' => 'sale process added successfully']);
		} else{
			$response = response()->json(['status' => 'success', 'message' => 'Error in Purchase']);
		}

		return $response;

	}

	//=============  GET ALL PRODUCTS SALE  ===========

	public function getAllProductSale(){

		$Sale = new Sale();

		$result = $Sale->getAllProductSale();



		if(count($result)>0){
			$response = response()->json(['status'=>'success', 'message'=>"Purchase data", 'result' => $result]);
		}else{
			$response = response()->json(['status'=>'Failed', 'message'=>"No Purchase data"]);
		}
		return $response;
	}

	//=============  GET INDUVIDUAL PRODUCTS SALE  ===========

	public function getIndividualProductSale($sale_id){
		$Sale = new Sale();
		$Sale->sale_id = $sale_id;
		$result = $Sale->getIndividualProductSale($Sale);

		if(count($result) > 0){
			$response = response()->json(['status'=>'success', 'message'=>"ProductSales data", 'result' => $result]);
			
		}else{
			$response = response()->json(['status'=>'Failed', 'message'=>"No ProductSales data"]);
		}
		return $response;

	}


	//============= UPDATE SOLED PRODUCT STATUS   ===========

	public function UpdateSaleStatus(Request $request){
		$Sale = new Sale();
		$Sale->sale_id = $request->input('sale_id');
		$Sale->sale_status = $request->input('sale_status');
		
		$result = $Sale->UpdateOrderSaleStatus($Sale);

		if($result == 1){
			$response = response()->json(['status'=>'success', 'message'=>"Status has been changed Successfully", 'result' => $result]);
			
		}else{
			$response = response()->json(['status'=>'Failed', 'message'=>"Please check the datas"]);
		}
		return $response;

	}


}
?>