<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use File;
use Mail;

require(base_path().'/app/Http/Middleware/Vendor.php');

class VendorController extends Controller{

	Public function addVendorOrder(Request $request){

		$Orders = new VendorOrder();

        $Orders->vendor_id    		 = $request->input('vendor_id');
        $Orders->vendor_name         = $request->input('vendor_name');
        $Orders->area_manager_id     = $request->input('area_manager_id');
        $Orders->order_date          = $request->input('order_date');
        $Orders->or_gross_total      = $request->input('or_gross_total');
        $Orders->or_tot_price        = $request->input('or_tot_price');
        $Orders->acc_or_tot_price	 = $request->input('acc_or_tot_price');
        $Orders->order_discount      = $request->input('order_discount');
        $Orders->spl_discount        = $request->input('spl_discount');
        $Orders->branch_id     		 = $request->input('branch_id');
        $Orders->region_id      	 = $request->input('region_id');
        $Orders->area_id        	 = $request->input('area_id');
        $Orders->stokist_id          = $request->input('stokist_id');
        $Orders->warehouse_id        = $request->input('warehouse_id');
        $Orders->order_rej_date 	 = $request->input('order_rej_date');
        $Orders->admin_cmt 			 = $request->input('admin_cmt');
        $Orders->invoice_no 		 = $request->input('invoice_no');
        $Orders->reject_reason 		 = $request->input('reject_reason');
        $Orders->payment_type        = $request->input('payment_type');//1-COD,2-credit
        $Orders->credit_lim          = $request->input('credit_lim');
        $Orders->credit_valid_to     = $request->input('credit_valid_to');
        $Orders->or_type             = $request->input('or_type');
        $Orders->tot_box             = $request->input('tot_box');
        $Orders->payment_img 		 = $request->input('payment_img');
        $Orders->payment_name 		 = $request->input('payment_name');
        $Orders->payment_msg 		 = $request->input('payment_msg');
        $Orders->vendor_order_list   = $request->input('vendor_order_list'); 
        // print_r($request->input('vendor_order_list'));
        // exit();

        $results = $Orders->storeVendorOrderDetails($Orders);

        if($results == 1){
            $response = response()->json(['status' => 'success' , 'message' => 'Order Placed successfully']);
        }else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Order Placed failed']);
        }
        return $response;

	}

	public function getIdBasedVendorList(Request $request){

		$Orders = new VendorOrder();

		$Orders->user_id = $request->input('user_id');

		$results = $Orders->getIdBasedVendorList($Orders);

		if(count($results)>0){
			$response = response()->json(['status' => 'success', 'message' => '', 'data' => $results ]);
		}else{
			$response = response()->json-(['status' => 'failure', 'message' => 'Not found details']);
		}
		return $response;
	}

	public function getAreaManagerBasedOrder(Request $request){

		$Orders = new VendorOrder();

		$Orders->user_id = $request->input('user_id');

		$results = $Orders->getAreaManagerBasedOrder($Orders);

		if(count($results)>0){
			$response = response()->json(['status'=> 'success', 'message' =>'', 'data'=> $results]);
		}else{
			$response = response()->json(['status'=>'failure', 'message'=>'Not found details']);
		}
    	return $response;
    }

	public function getAreaManagerBasedVendorOrder(Request $request){

		$Orders = new VendorOrder();

		$Orders->user_id = $request->input('user_id');
		$Orders->type = $request->input('type');

        if($request->input('start_date')){
            $Orders->start_date = $request->input('start_date');
            $Orders->end_date = $request->input('end_date');
        }else{
            $Orders->start_date = "";
            $Orders->end_date = "";
        }

		$results = $Orders->getAreaManagerBasedVendorOrder($Orders);

		if(count($results)>0){
			$response = response()->json(['status'=> 'success', 'message' =>'', 'data'=> $results]);
		}else{
			$response = response()->json(['status'=>'failure', 'message'=>'Not found details']);
		}
    	return $response;
    }

	public function getOrderIdBasedList(Request $request){

		$Orders = new VendorOrder();

		$Orders->vendor_order_id = $request->input('vendor_order_id');
		$Orders->vendor = $request->input('vendor');

		$results = $Orders->getOrderIdBasedList($Orders);

		if(count($results)>0){
			$response = response()->json(['status'=> 'success', 'message' =>'', 'data'=> $results]);
		}else{
			$response = response()->json(['status'=>'failure', 'message'=>'Not found details']);
		}
    	return $response;
    }

    public function getStockistBasedVendorOrder(Request $request){

    	$Orders = new VendorOrder();

    	$Orders->warehouse_id = $request->input('warehouse_id');
    	$Orders->type = $request->input('type');

    	$results = $Orders->getStockistBasedVendorOrder($Orders);
    	if(count($results)>0){
    		$response = response()->json(['status'=> 'success', 'message'=>'', 'data'=>$results]);
    	}else{
    		$response = response()->json(['status'=>'failure', 'message'=> 'Not found details']);
    	}
    	return $response;
    }

    public function getWarehouseIdBasedOrderDetails(Request $request){

    	$Orders = new VendorOrder();

    	$Orders->warehouse_id = $request->input('warehouse_id');

    	$results = $Orders->getWarehouseIdBasedOrderDetails($Orders);
    	if(count($results)>0){
    		$response = response()->json(['status'=> 'success', 'message'=>'', 'data'=>$results]);
    	}else{
    		$response = response()->json(['status'=>'failure', 'message'=> 'Not data found']);
    	}
    	return $response;
    }
    public function reportForAreaManager(Request $request){

    	$Orders = new VendorOrder();
    	$Orders->start_date = $request->input('start_date');
        $Orders->end_date = $request->input('end_date');
    	// if($request->input('year')){
     //    	$Orders->year = $request->input('year');
     //    	$Orders->months = $request->input('months');
     //    }else{
     //    	$Orders->year = '';
     //    	$Orders->months = '';
     //    }

        if($request->input('area_manager_id')){
        	$Orders->area_manager_id = $request->input('area_manager_id');
        }else{
        	$Orders->area_manager_id = '';
        }

        if($request->input('vendor_id')){
        	$Orders->vendor_id = $request->input('vendor_id');
        }else{
        	$Orders->vendor_id = '';
        }

    	$results = $Orders->reportForAreaManager($Orders);

    	if(count($results) > 0){
            $response = response()->json(['status' => 'success' , 'message' => '', 'data' => $results]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'No Sales data found']); 
        }
        return $response;

    }

    public function reportForWarehouse(Request $request){

    	$Orders = new VendorOrder();

    	$Orders->start_date = $request->input('start_date');
        $Orders->end_date = $request->input('end_date');
    	// if($request->input('year')){
     //    	$Orders->year = $request->input('year');
     //    	$Orders->months = $request->input('months');
     //    }else{
     //    	$Orders->year = '';
     //    	$Orders->months = '';
     //    }

        if($request->input('area_manager_id')){
        	$Orders->area_manager_id = $request->input('area_manager_id');
        }else{
        	$Orders->area_manager_id = '';
        }

        if($request->input('warehouse_id')){
        	$Orders->warehouse_id = $request->input('warehouse_id');
        }else{
        	$Orders->warehouse_id = '';
        }

    	$results = $Orders->reportForWarehouse($Orders);

    	if(count($results) > 0){
            $response = response()->json(['status' => 'success' , 'message' => '', 'data' => $results]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'No Sales data found']); 
        }
        return $response;

    }

    public function VendorOrderAccept(Request $request)
    {
        $Orders = new VendorOrder();
        // print_r($request->input('vendor_order_id'));
        // print_r($request->input('vendor_order_status'));
        // print_r($request->input('products'));
        // print_r($request->input('acc_or_tot_price'));
        // $Orders->tot_box  = $request->input('tot_box');
        $Orders->vendor_order_id  = $request->input('vendor_order_id');
        $Orders->vendor_order_status  = $request->input('vendor_order_status');
        $Orders->products  = $request->input('products');
        $Orders->acc_or_tot_price  = $request->input('acc_or_tot_price');
        // print_r($Orders);
    
        $result = $Orders->VendorOrderAccept($Orders);
        if($result > 0){
            $response = response()->json(['status' => 'success', 'message' => 'Order has been accepted successfully','data'=> $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Error in order accepting process']);
        }
        return $response;
    }

    public function getOrderIdDetails(Request $request){

    	$Orders = new VendorOrder();

    	$Orders->vendor_order_id = $request->input('vendor_order_id');

    	$results = $Orders->getOrderIdDetails($Orders);
    	if(count($results)>0){
    		$response = response()->json(['status'=> 'success', 'message'=>'', 'data'=>$results]);
    	}else{
    		$response = response()->json(['status'=>'failure', 'message'=> 'Not data found']);
    	}
    	return $response;
    }

    public function getWhIdBaseVenOrdDetails(Request $request){

    	$Orders = new VendorOrder();
    	// print_r($request->input('warehouse_id'));

    	$Orders->warehouse_id = $request->input('warehouse_id');
        $Orders->start_date = $request->input('start_date');
        $Orders->end_date = $request->input('end_date');

    	$results = $Orders->getWhIdBaseVenOrdDetails($Orders);

    	if(count($results)>0){
    		$response = response()->json(['status'=> 'success', 'message'=>'', 'data'=>$results]);
    	}else{
    		$response = response()->json(['status'=>'failure', 'message'=> 'Not data found']);
    	}
    	return $response;
    }

    public function getVendorOrderBasedEventsList(Request $request)
    {
        $Orders = new VendorOrder();
        $Orders->vendor_order_id   = $request->input('vendor_order_id');

        $result = $Orders->getVendorOrderBasedEventsList($Orders);
        if(count($result) > 0){
            $response = response()->json(['status' => 'success' , 'message' => 'The order event details fetch successfully', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'No data found', 'data' => $result]); 
        }
        return $response;
    }

    public function updateVendorOrderStatus(Request $request)
    {
        $Orders = new VendorOrder();
        $Orders->vendor_order_id  = $request->input('vendor_order_id');
        $Orders->vendor_order_status  = $request->input('vendor_order_status');
        $Orders->vendor_id  = $request->input('vendor_id');
        $Orders->or_tot_price  = $request->input('or_tot_price');
        $Orders->reject_reason  = $request->input('reject_reason');

        $result = $Orders->updateVendorOrderStatus($Orders);
        if($result > 0){
            $response = response()->json(['status' => 'success', 'message' => 'Order Status Update successfully','data'=> $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Error in Update Order Status']);
        }
        return $response;
    }

    public function getPlacedVendorOrder(Request $request)
    {
        $Orders = new VendorOrder();
        $Orders->warehouse_id  = $request->input('warehouse_id');
        $Orders->type  = $request->input('type');
        $Orders->start_date = $request->input('start_date');
        $Orders->end_date = $request->input('end_date');

        $result = $Orders->getPlacedVendorOrder($Orders);
        if($result > 0){
            $response = response()->json(['status' => 'success', 'message' => 'Placed order fetched successfully','data'=> $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Error in Update Order Status']);
        }
        return $response;
    }

    public function getApprovalVendorOrderData()
    {
        $Orders = new VendorOrder();
        $result = $Orders->getApprovalVendorOrderData();
        if(count($result)){
            $response = response()->json(['status' => 'success', 'message' => 'Vendor Order Approval Request fetched successfully','data'=> $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Error in Fetch Vendor Order Approval  Details']);
        }
        return $response;
    }

    public function getVendorOrderByAdmin(Request $request){

        $Orders = new VendorOrder();
        $Orders->branch_id = $request->input('branch_id');

        if($request->input('start_date')){
            $Orders->start_date = $request->input('start_date');
            $Orders->end_date = $request->input('end_date');
        }else{
            $Orders->start_date = "";
            $Orders->end_date = "";
        }

        $results = $Orders->getVendorOrderByAdmin($Orders);
        if(count($results) > 0){
            $response = response()->json(['status' => 'success', 'message' => 'Vendor order fetched successfully','data'=> $results]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Error in fetch the vendor order details']);
        }
        return $response;
    }

    public function vendorOrderFOCDiscountAccept(Request $request)
    {
        $Orders = new VendorOrder();
        $Orders->vendor_order_id  = $request->input('vendor_order_id');
        $Orders->products_list  = $request->input('products_list');
        $Orders->spl_discount  = $request->input('spl_discount');
        $Orders->admin_cmt  = $request->input('admin_cmt');
        $Orders->warehouse_id  = $request->input('warehouse_id');
        //$Orders->or_tot_price  = $request->input('or_tot_price');

        $result = $Orders->vendorOrderFOCDiscountAccept($Orders);
        if($result == 1){
            $response = response()->json(['status' => 'success', 'message' => 'Order has been accepted successfully','data'=> $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Error in order accepting process']);
        }
        return $response;
    }

    public function getAreaIdBasedPriceType(Request $request){
        $Orders  = new VendorOrder();
        // $Orders->area_id = explode(',', $request->input('area_id')); 
        $Orders->area_id = $request->input('area_id');
        // $Orders->area_id = implode(" ", $request->input('area_id'));
        // print_r($Orders->area_id);
        // $area_id = $request->input('area_id');
        // print_r(($area_id));
        

        $results = $Orders->getAreaIdBasedPriceType($Orders);

        if($results){
            $response = response()->json(['status'=>'success', 'message'=> "Price type are fetched", 'data'=> $results]);
        }else{
            $response = response()->json(['status'=> 'failure', 'message'=>"No data found"]);
        }
        return $response;
    }

    public function vendorReportForProductIdBased(Request $request){
        $Orders = new VendorOrder();

        $Orders->start_date = $request->input('start_date');
        $Orders->end_date = $request->input('end_date');

        if($request->input('area_manager_id')){
            $Orders->area_manager_id = $request->input('area_manager_id');
        }else{
            $Orders->area_manager_id = '';
        }

        if($request->input('product_id')){
            $Orders->product_id = $request->input('product_id');
        }else{
            $Orders->product_id = '';
        }

        if($request->input('vendor_id')){
            $Orders->vendor_id = $request->input('vendor_id');
        }else{
            $Orders->vendor_id = '';
        }

        $result = $Orders->vendorReportForProductIdBased($Orders);
        
        if(count($result) > 0){
            $response = response()->json(['status' => 'success' , 'message' => '', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'No Sales data found']); 
        }
        return $response;
    }

    public function vendorReportForRegionManageBased(Request $request){
        $Orders = new VendorOrder();

        $Orders->start_date = $request->input('start_date');
        $Orders->end_date = $request->input('end_date');
        $Orders->region_id = $request->input('region_id');
        if($request->input('region_id')){
            $Orders->region_id = $request->input('region_id');
        }else{
            $Orders->region_id = '';
        }
        if($request->input('branch_id')){
            $Orders->branch_id = $request->input('branch_id');
        }else{
            $Orders->branch_id = '';
        }
        if($request->input('area_manager_id')){
            $Orders->area_manager_id = $request->input('area_manager_id');
        }else{
            $Orders->area_manager_id = '';
        }

        if($request->input('product_id')){
            $Orders->product_id = $request->input('product_id');
        }else{
            $Orders->product_id = '';
        }

        if($request->input('vendor_id')){
            $Orders->vendor_id = $request->input('vendor_id');
        }else{
            $Orders->vendor_id = '';
        }

        if($request->input('warehouse_id')){
            $Orders->warehouse_id = $request->input('warehouse_id');
        }else{
            $Orders->warehouse_id = '';
        }

        $result = $Orders->vendorReportForRegionManageBased($Orders);
        
        if(count($result) > 0){
            $response = response()->json(['status' => 'success' , 'message' => '', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'No Sales data found']); 
        }
        return $response;
    }

    public function getAllFieldOfficerAndStockistInOrder(Request $request){
        $Orders = new VendorOrder();
        $Orders->role = $request->input('role');
        $Orders->vendor = $request->input('vendor');
        $Orders->region_id = $request->input('region_id');

        $result = $Orders->getAllFieldOfficerAndStockistInOrder($Orders);
        
        if($result){
            $response = response()->json(['status' => 'success' , 'message' => '', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'No Sales data found']); 
        }
        return $response;

    }

    // public function getAllFieldOfficerInOrder(Request $request){

    //     $Orders = new VendorOrder();

    // }fetchRegionBasedVendorList
    public function fetchRegionBasedVendorList(Request $request){
        $Orders = new VendorOrder();
        $Orders->region_id = $request->input('region_id');

        $result = $Orders->fetchRegionBasedVendorList($Orders);
        
        if($result){
            $response = response()->json(['status' => 'success' , 'message' => '', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'No Sales data found']); 
        }
        return $response;

    }

    // vendor order detail based upon the area manager
    public function fetchAreaManageBasedVendorOrderDetails(Request $request){
        $Orders = new VendorOrder();
        $Orders->area_manager_id = $request->input('area_manager_id');
        $Orders->start_date = $request->input('start_date');
        $Orders->end_date = $request->input('end_date');

        // print_r($request->input('end_date'));

        $result = $Orders->fetchAreaManageBasedVendorOrderDetails($Orders);
        
        if($result){
            $response = response()->json(['status' => 'success' , 'message' => '', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'No data found']); 
        }
        return $response;
    }
}