<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use File;
use Mail;

require(base_path().'/app/Http/Middleware/FeedAndReview.php');

class FeedAndReviewController extends Controller
{
//////////////////////////////////////////
//FeedBack & Review Management Api Calls//
/////////////////////////////////////////
	//Add FeedBack Details
    public function addFeedBack(Request $request){
        
        $FeedAndReviews = new FeedAndReview();
        $FeedAndReviews->task_id    		= $request->input('task_id');
        $FeedAndReviews->field_officer_id 	= $request->input('field_officer_id');
        $FeedAndReviews->field_officer_name = $request->input('field_officer_name');
        $FeedAndReviews->area_manager_id   = $request->input('area_manager_id');
        $FeedAndReviews->customer_id     = $request->input('customer_id');
        $FeedAndReviews->feedback    = $request->input('feedback'); 

        $FeedAndReviews->type        = $request->input('type');
        $FeedAndReviews->product_details  = $request->input('product_details');

        if($request->input('telecalling_id')){
            $FeedAndReviews->telecalling_id = $request->input('telecalling_id');
        }else{
            $FeedAndReviews->telecalling_id = "";
        }

        if($request->input('telecalling_type')){
            $FeedAndReviews->telecalling_type = $request->input('telecalling_type');
        }else{
            $FeedAndReviews->telecalling_type = "";
        }

        if($request->input('events')){
            $FeedAndReviews->events = $request->input('events');
        }else{
            $FeedAndReviews->events = "";
        }
        //$FeedAndReviews->product_id  = $request->input('product_id');
        //$FeedAndReviews->product_genericname = $request->input('product_genericname');

        $result = $FeedAndReviews->storeFeedBack($FeedAndReviews);
        if($result == 1){
            $response = response()->json(['status' => 'success' , 'message' => 'FeedBack Added successfully']);
        } else{
            $response = response()->json(['status' => 'failure' , 'message' => 'FeedBack Added failed']);
        }
        return $response;

    }


//Add Revist Details
    public function addRevisit(Request $request){
        
        $FeedAndReviews = new FeedAndReview();
        $FeedAndReviews->task_id            = $request->input('task_id');
        $FeedAndReviews->field_officer_id   = $request->input('field_officer_id');
        $FeedAndReviews->field_officer_name = $request->input('field_officer_name');
        $FeedAndReviews->area_manager_id         = $request->input('area_manager_id');
        $FeedAndReviews->customer_id        = $request->input('customer_id');
        $FeedAndReviews->date               = $request->input('date');
        $FeedAndReviews->Revisit_reason      = $request->input('Revisit_reason'); 
       
        $FeedAndReviews->created_by    = $request->input('created_by');
        $FeedAndReviews->region_manager_id  = $request->input('region_manager_id');
        $FeedAndReviews->region_id       = $request->input('region_id');
        $FeedAndReviews->branch_id       = $request->input('branch_id');
        $FeedAndReviews->area_id       = $request->input('area_id');
        $FeedAndReviews->customer_type     = $request->input('customer_type');
        $FeedAndReviews->task_title      = $request->input('task_title');
        $FeedAndReviews->task_desc     = $request->input('task_desc');
        $FeedAndReviews->task_date_time      = $request->input('task_date_time');   

        $result = $FeedAndReviews->storeRevisit($FeedAndReviews);
        if($result == 1){
            $response = response()->json(['status' => 'success' , 'message' => 'Revist Added successfully']);
        } else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Revist Added failed']);
        }
        return $response;

    }

}
