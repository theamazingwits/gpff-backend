<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use File;
use Mail;
// use Excel;
use DB;
use App\Imports\UsersImport;
use Maatwebsite\Excel\Facades\Excel;

use DOMDocument;
require(base_path().'/app/Http/Middleware/Product.php');

class ProductController extends Controller
{
    //
////////////////////////////////
//Product Management Api Calls//
////////////////////////////////
//Category
    //Admin Or Sub Admin Add the Category 
	public function addCategory(Request $request){
    	
        $Products = new Product();
        $Products->category_name  = $request->input('category_name');
        $Products->region_id  = $request->input('region_id');
        $Products->branch_id  = $request->input('branch_id');

        $result = $Products->addCategory($Products);
        if($result == 1){
            $response = response()->json(['status' => 'success' , 'message' => 'Category added successfully']);
        } else if($result == 2){
            $response = response()->json(['status' => 'success' , 'message' => 'Already Category Exit']);
        } else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Category added failed']);
        }
        return $response;
    }
    // Update Category Details
    public function updateCategory(Request $request)
    {
        $Products = new Product();
        $Products->category_name  	= $request->input('category_name');
        $Products->category_id  	= $request->input('category_id');
        $Products->region_id  = $request->input('region_id');
        $Products->branch_id  = $request->input('branch_id');

        $result = $Products->updateCategory($Products);
        if($result > 0){
            $response = response()->json(['status' => 'success', 'message' => 'Category Name Update successfully','data'=> $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Error in Update Category Details']);
        }
        return $response;
    }
    // Delete Category details
    public function removeCategory(Request $request){
        
        $Products = new Product();
        $Products->category_id  = $request->input('category_id');

        $result = $Products->removeCategory($Products);
        if($result > 0){
            $response = response()->json(['status' => 'success' ,'message' => 'Category details deleted successfully']);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Error in deleting']); 
        }
        return $response;
    }
    //Category Activate Or Deactivate
    public function categoryActiveOrDeactive(Request $request)
    {
        $Products = new Product();
        $Products->category_id 	= $request->input('category_id');
        $Products->category_status = $request->input('category_status');
        
        $result = $Products->categoryActiveOrDeactive($Products);
        if($result > 0){
            if($Products->category_status == 1){
                $response = response()->json(['status' => 'success', 'message' => 'Category Actived Successfully']);
            } else{
                $response = response()->json(['status' => 'success', 'message' => 'Category Deactived Successfully']);
            }
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Error in Updated details.. !']);
        }
        return $response;
    }
    // Fetch (All)Category Details
    public function getAllCategory()
    {
        $Products = new Product();

        $data = $Products->getAllCategory();
        if(count($data) > 0 ){
            $response = response()->json(['status' => 'success' , 'message' => 'Category Data retrived successfully','data'=>$data]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Error in Get details..','data'=>'null']);
        }
        return $response;
    } 

    // Fetch Region Based Category Details
    public function getRegionBasedCategory($region_id)
    {
        $Products = new Product();
        $Products->region_id = $region_id;

        $data = $Products->getRegionBasedCategory($Products);
        if(count($data) > 0 ){
            $response = response()->json(['status' => 'success' , 'message' => 'Region Based Category Data retrived successfully','data'=>$data]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Error in Get details..','data'=>'null']);
        }
        return $response;
    }     
//Product
    //Admin Or Sub Admin Add the Product 
    public function addProduct(Request $request){
        
        $Products = new Product();
        $Products->category_id  = $request->input('category_id');
        $Products->category_name = $request->input('category_name');
        $Products->product_genericname = $request->input('product_genericname');
        $Products->product_description = $request->input('product_description');
        $Products->product_packingsize = $request->input('product_packingsize');
        //$Products->product_price  = $request->input('product_price');
        $Products->product_netprice  = $request->input('product_netprice');
        $Products->product_grossprice  = $request->input('product_grossprice');
        $Products->product_discount  = $request->input('product_discount');
        $Products->product_img  = $request->input('product_img');
        $Products->product_type  = $request->input('product_type');
        $Products->region_id = $request->input('region_id');
        $Products->branch_id  = $request->input('branch_id');
        $Products->region_name = $request->input('region_name');
        $Products->product_foc = $request->input('product_foc');
        
        $Products->type = $request->input('type');
        if($request->input('type') == 1){
            $Products->productpath = $Products->getFilePath($request->file('productall'));
            $Products->productall = $request->file('productall');
        }

        $result = $Products->addProduct($Products);
        if($result == 1){
            $response = response()->json(['status' => 'success' , 'message' => 'Product added successfully']);
        } else if($result == 2){
            $response = response()->json(['status' => 'success' , 'message' => 'Already Product Exit']);
        } else if($result == 3){
            $response = response()->json(['status' => 'failure' , 'message' => 'Please upload excel file only']);
        } else if($result == 4){
            $response = response()->json(['status' => 'failure' , 'message' => 'Please Fill All Field in the excel']);
        } else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Product added failed']);
        }
        return $response;
    }
    // Update Product Details
    public function updateProduct(Request $request)
    {
        $Products = new Product();
        $Products->user_id  = $request->input('user_id');
        $Products->product_id  = $request->input('product_id');
        $Products->category_id  = $request->input('category_id');
        $Products->category_name = $request->input('category_name');
        $Products->product_genericname = $request->input('product_genericname');
        $Products->product_description = $request->input('product_description');
        $Products->product_packingsize = $request->input('product_packingsize');
        $Products->product_price  = $request->input('product_price');
        $Products->product_old_netprice  = $request->input('product_old_netprice');
        $Products->product_old_grossprice  = $request->input('product_old_grossprice');
        $Products->product_netprice  = $request->input('product_netprice');
        $Products->product_grossprice  = $request->input('product_grossprice');
        $Products->product_discount  = $request->input('product_discount');
        $Products->product_img  = $request->input('product_img');
        $Products->product_type  = $request->input('product_type');
        $Products->region_id = $request->input('region_id');
        $Products->branch_id  = $request->input('branch_id');
        $Products->region_name = $request->input('region_name');
        $Products->product_foc = $request->input('product_foc');

        $result = $Products->updateProduct($Products);
        if($result > 0){
            $response = response()->json(['status' => 'success', 'message' => 'Product Details Update successfully','data'=> $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Error in Update Product Details']);
        }
        return $response;
    }

    // Delete Product details
    public function removeProduct(Request $request){
        
        $Products = new Product();
        $Products->product_id  = $request->input('product_id');

        $result = $Products->removeProduct($Products);
        if($result > 0){
            $response = response()->json(['status' => 'success' ,'message' => 'Product details deleted successfully']);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Error in deleting']); 
        }
        return $response;
    }
    //Get Indivitual Product Details
    public function getIndProduct(Request $request)
    {
        $Products = new Product();
        $Products->product_id  = $request->input('product_id');

        $result = $Products->getIndProduct($Products);
        if(count($result) > 0){
            $response = response()->json(['status' => 'success' , 'message' => '', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Invalid Product details']); 
        }
        return $response;
    } 
    //product Activate Or Deactivate
    public function productActiveOrDeactive(Request $request)
    {
        $Products = new Product();
        $Products->product_id  = $request->input('product_id');
        $Products->product_status = $request->input('product_status');
        
        $result = $Products->productActiveOrDeactive($Products);
        if($result > 0){
            if($Products->product_status == 1){
                $response = response()->json(['status' => 'success', 'message' => 'Product Actived Successfully']);
            } else{
                $response = response()->json(['status' => 'success', 'message' => 'Product Deactived Successfully']);
            }
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Error in Updated details.. !']);
        }
        return $response;
    }
    // Fetch (All)product Details
    public function getAllProduct()
    {
        $Products = new Product();

        $data = $Products->getAllProduct();
        if(count($data) > 0 ){
            $response = response()->json(['status' => 'success' , 'message' => 'Product Data retrived successfully','data'=>$data]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Error in Get details..','data'=>'null']);
        }
        return $response;
    }   
    
    //Product Stock
    //Admin Or Sub Admin Add the Product Stock
    public function addGoodsRecivedStock(Request $request){
        
        $Products = new Product();
        $Products->warehouse_id = $request->input('warehouse_id');
        $Products->stockist_id = $request->input('stockist_id');
        $Products->region_id = $request->input('region_id');
        $Products->branch_id  = $request->input('branch_id');
        $Products->invoice_no  = $request->input('invoice_no');
        $Products->supplier_name  = $request->input('supplier_name');
        $Products->invoice_date  = $request->input('invoice_date');
        $Products->branch_id  = $request->input('branch_id');
        $Products->payment_type  = $request->input('payment_type');
        $Products->grand_tot_amt  = $request->input('grand_tot_amt');
        $Products->prduct_details = $request->input('prduct_details');
        
        if($request->input('type')){
            $Products->productpath = $Products->getFilePath($request->file('productall'));
            $Products->productall = $request->file('productall');
            $result = $Products->addBulkProductStock($Products);
            // print_r($result);
            // exit();
        }else{
            $result = $Products->addProductStock($Products);
        }

        if($result == 1){
            $response = response()->json(['status' => 'success' , 'message' => 'Product Stock added successfully']);
        }else if($result == 2){
            $response = response()->json(['status' => 'failure' , 'message' => 'Please Fill Records in the excel']);
        }else if($result == 3){
            $response = response()->json(['status' => 'failure' , 'message' => 'Please upload excel file only']);
        }else if($result == 4){
            $response = response()->json(['status' => 'failure' , 'message' => 'Please Fill All Field in the excel']);
        }else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Product added failed']);
        }
        return $response;
    }

    // Update Product Details
    // public function updateProductsBatchExp(Request $request)
    // {
    //     $Products = new Product();
    //     $Products->product_batch_stock_id  = $request->input('product_batch_stock_id');
    //     $Products->new_tot_qty = $request->input('new_tot_qty');
    //     $Products->new_avl_qty = $request->input('new_avl_qty');
    //     $Products->new_batch_id = $request->input('new_batch_id');
    //     $Products->new_exp_date = $request->input('new_exp_date');
    //     $Products->product_id = $request->input('product_id');
    //     // $Products->batch_id = $request->input('batch_id');
    //     // $Products->exp_date = $request->input('exp_date');
    //     $Products->tot_qty = $request->input('tot_qty');
    //     $Products->avl_qty = $request->input('avl_qty');

    //     $result = $Products->updateProductsBatchExp($Products);
    //     if($result > 0){
    //         $response = response()->json(['status' => 'success', 'message' => 'Product Qty Update successfully','data'=> $result]);
    //     }
    //     else{
    //         $response = response()->json(['status' => 'failure' , 'message' => 'Error in Update Product Details']);
    //     }
    //     return $response;
    // }

    public function updateProductsBatchExp(Request $request)
    {
        $Products = new Product();
        $Products->product_id  = $request->input('product_id');
        $Products->category_id  = $request->input('category_id');
        // $Products->new_batch_id = $request->input('new_batch_id');
        $Products->new_exp_date = $request->input('new_exp_date');
        $Products->batch_id = $request->input('batch_id');
        $Products->exp_date = $request->input('exp_date');

        $result = $Products->updateProductsBatchExp($Products);
        if($result > 0){
            $response = response()->json(['status' => 'success', 'message' => 'Product Details Update successfully','data'=> $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Error in Update Product Details']);
        }
        return $response;
    }

    // Fetch Warehouse Based Product Stock Recived Details
    public function getGRHistory(Request $request)
    {
        $Products = new Product();
        $Products->region_id  = $request->input('region_id');
        $Products->warehouse_id  = $request->input('warehouse_id');
        
        $data = $Products->getGRHistory($Products);
        if(count($data) > 0 ){
            $response = response()->json(['status' => 'success' , 'message' => 'Good Recived History fetched','data'=>$data]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'No data found','data'=>$data]);
        }
        return $response;
    } 
    // Fetch Ind Based Stock Details Fetch
    public function getIndGRBatch($product_recived_history_id)
    {
        $Products = new Product();
        $Products->product_recived_history_id = $product_recived_history_id;

        $data = $Products->getIndGRBatch($Products);
        if(count($data) > 0 ){
            $response = response()->json(['status' => 'success' , 'message' => 'Data retrived successfully','data'=>$data]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'No data found.','data'=>$data]);
        }
        return $response;
    } 

    // Fetch Warehouse Based Product Stock Details
    public function getWarehouseBasedProductStock(Request $request)
    {
        $Products = new Product();
        $Products->region_id  = $request->input('region_id');
        $Products->warehouse_id  = $request->input('warehouse_id');
        
        $data = $Products->getWarehouseBasedProductStock($Products);
        if(count($data) > 0 ){
            $response = response()->json(['status' => 'success' , 'message' => 'Region Based Products Data retrived successfully','data'=>$data]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Error in Get details..','data'=>'null']);
        }
        return $response;
    } 

    // Fetch Warehouse Based Product Batch Details
    public function getWarehouseBasedProductBatch(Request $request)
    {
        $Products = new Product();
        $Products->product_id  = $request->input('product_id');
        $Products->warehouse_id  = $request->input('warehouse_id');
        
        $data = $Products->getWarehouseBasedProductBatch($Products);
        if(count($data) > 0 ){
            $response = response()->json(['status' => 'success' , 'message' => 'Warehouse Based Products Data retrived successfully','data'=>$data]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'No data found','data'=>$data]);
        }
        return $response;
    } 
    // Fetch Warehouse Based Product Stock Stock List
    public function getWarehouseBasedProductStockList(Request $request)
    {
        $Products = new Product();
        $Products->product_id  = $request->input('product_id');
        $Products->warehouse_id  = $request->input('warehouse_id');
        $Products->type  = $request->input('product_type');
        
        $data = $Products->getWarehouseBasedProductStockList($Products);
        if(count($data) > 0 ){
            $response = response()->json(['status' => 'success' , 'message' => 'Warehouse Based Products Data retrived successfully','data'=>$data]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'No data found','data'=>$data]);
        }
        return $response;
    } 

    // Fetch Multiple Warehouse Based Product Stock Stock List
    public function getMultipleWarehouseBasedProductStockList(Request $request)
    {
        $Products = new Product();
        if($request->input('branch_id')){
            $Products->branch_id  = $request->input('branch_id');
        }else{
            $Products->branch_id  = '';
        }
        if($request->input('product_id')){
            $Products->product_id  = $request->input('product_id');
        }else{
            $Products->product_id  = '';
        }
        if($request->input('warehouse_id')){
            $Products->warehouse_id  = $request->input('warehouse_id');
        }else{
            $Products->warehouse_id  = '';
        }
        if($request->input('region_id')){
            $Products->region_id  = $request->input('region_id');
        }else{
            $Products->region_id  = '';
        }
        $Products->type  = $request->input('product_type');
        
        $data = $Products->getMultipleWarehouseBasedProductStockList($Products);
        if(count($data) > 0 ){
            $response = response()->json(['status' => 'success' , 'message' => 'Warehouse Based Products Data retrived successfully','data'=>$data]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'No data found','data'=>$data]);
        }
        return $response;
    } 
    // Fetch Warehouse Based Product Batch Details
    public function getBatchBasedQty(Request $request)
    {
        $Products = new Product();
        $Products->product_id  = $request->input('product_id');
        $Products->warehouse_id  = $request->input('warehouse_id');
        $Products->batch_id  = $request->input('batch_id');
        
        $data = $Products->getBatchBasedQty($Products);
        if(count($data) > 0 ){
            $response = response()->json(['status' => 'success' , 'message' => 'Batch Based Products Qty retrived successfully','data'=>$data]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'No data found','data'=>$data]);
        }
        return $response;
    } 

    //End Warehouse

    //Get Indivitual Product Qty Details
    public function getIndProductQty(Request $request)
    {
        $Products = new Product();
        $Products->product_id  = $request->input('product_id');

        $result = $Products->getIndProductQty($Products);
        if(count($result) > 0){
            $response = response()->json(['status' => 'success' , 'message' => '', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Invalid Product details']); 
        }
        return $response;
    } 
    //Get Indivitual Product Qty Details
    public function getIndProductsQtyDetails(Request $request)
    {
        $Products = new Product();
        $Products->warehouse_id  = $request->input('warehouse_id');
        $Products->product_id  = $request->input('product_id');
        $result = $Products->getIndProductsQtyDetails($Products);
        if(count($result) > 0){
            $response = response()->json(['status' => 'success' , 'message' => '', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Invalid Product details']); 
        }
        return $response;
    } 


    // Fetch Region Based Product Details
    public function getRegionBasedProducts($region_id)
    {
        $Products = new Product();
        $Products->region_id = $region_id;

        $data = $Products->getRegionBasedProducts($Products);
        if(count($data) > 0 ){
            $response = response()->json(['status' => 'success' , 'message' => 'Region Based Products Data retrived successfully','data'=>$data]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Error in Get details..','data'=>'null']);
        }
        return $response;
    } 

    // Fetch Category Based (all)Product Details
    public function getCategoryBasedProduct(Request $request)
    {
        $Products = new Product();
        $Products->category_id  = $request->input('category_id');

        $result = $Products->getCategoryBasedProduct($Products);
        if(count($result) > 0){
            $response = response()->json(['status' => 'success' , 'message' => '', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Invalid Product details']); 
        }
        return $response;
    }
    // Fetch Category Based (all)Product Details
    public function getCategoryBasedProductPagi(Request $request)
    {
        $Products = new Product();

        if($request->input('draw')){
            $Products->draw = $request->input('draw');
        }else{
            $Products->draw = 1; 
        }
        if($request->input('start')){
             $Products->start = $request->input('start');
        }else{
            $Products->start = 0; 
        }
        if($request->input('length')){
            $Products->length = $request->input('length');
        }else{
            $Products->length = 10; 
        }

        $Products->category_id  = $request->input('category_id');

        $result = $Products->getCategoryBasedProductPagi($Products);

        if(count($result[2]) > 0){
            $response = response()->json(['status' => 'success' , 'message' => 'Products details fetched successfully', 'data' => $result[2],
                'draw' => $Products->draw,
                'recordsTotal' => $result[0],
                'recordsFiltered' => $result[1],
            ]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'No Data found',
                'data' => [],
                'draw' => $Products->draw,
                'recordsTotal' => 0,
                'recordsFiltered' => 0]);
        }
        return $response;

    }
    // Fetch all Product Details Pagination
    public function getAllProductPagi(Request $request)
    {
        $Products = new Product();

        if($request->input('draw')){
            $Products->draw = $request->input('draw');
        }else{
            $Products->draw = 1; 
        }
        if($request->input('start')){
             $Products->start = $request->input('start');
        }else{
            $Products->start = 0; 
        }
        if($request->input('length')){
            $Products->length = $request->input('length');
        }else{
            $Products->length = 10; 
        }

        $Products->region_id  = $request->input('region_id');

        $result = $Products->getAllProductPagi($Products);

        if(count($result[2]) > 0){
            $response = response()->json(['status' => 'success' , 'message' => 'Products details fetched successfully', 'data' => $result[2],
                'draw' => $Products->draw,
                'recordsTotal' => $result[0],
                'recordsFiltered' => $result[1],
            ]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'No Data found',
                'data' => [],
                'draw' => $Products->draw,
                'recordsTotal' => 0,
                'recordsFiltered' => 0]);
        }
        return $response;

    }
    //Region Based Field Details Fetch 
    public function getRegionBasedField(Request $request)
    {
        $Products = new Product();
        $Products->region_id = $request->input('region_id');
        
        $result = $Products->getRegionBasedField($Products);
        if(count($result) > 0){
            $response = response()->json(['status' => 'success' , 'message' => '', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Invalid Field Officer details']); 
        }
        return $response;
    }

     //Region Based Area Manager Details Fetch 
    public function getRegionBasedAreaManager(Request $request)
    {
        $Products = new Product();
        $Products->region_id = $request->input('region_id');
        
        $result = $Products->getRegionBasedAreaManager($Products);
        if(count($result) > 0){
            $response = response()->json(['status' => 'success' , 'message' => '', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Invalid User details']); 
        }
        return $response;
    }
    //Admin Or Sub Admin Product Assign to the field Officer 
    public function productAssign(Request $request){
        
        $Products = new Product();
        $Products->product  = $request->input('product');
        $Products->category_id  = $request->input('category_id');
        $Products->area_manager_id  = $request->input('area_manager_id');
        $Products->region_manager_id  = $request->input('region_manager_id');
        $Products->region_id  = $request->input('region_id');
        $Products->field_officer_id  = $request->input('field_officer_id');
        $Products->type  = $request->input('type');

        $result = $Products->productAssign($Products);
        if($result == 1){
            if($Products->type == 2){                            
                $response = response()->json(['status' => 'success' , 'message' => 'Product Assign Update successfully']);
            } else{
                $response = response()->json(['status' => 'success' , 'message' => 'Product Assign successfully']);            
            }
        } else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Product Assign failed']);
        }
        return $response;
    }
    // Fetch field officer Based Assign Product Details
    public function getfieldBasedAssignProduct(Request $request)
    {
        $Products = new Product();
        $Products->field_officer_id  = $request->input('field_officer_id');

        $result = $Products->getfieldBasedAssignProduct($Products);
        if(count($result) > 0){
            $response = response()->json(['status' => 'success' , 'message' => '', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Invalid Product details']); 
        }
        return $response;
    }
    // Fetch field officer Cate Based Assign Product Details
    public function getFOCateBasedAssignProduct(Request $request)
    {
        $Products = new Product();
        $Products->field_officer_id  = $request->input('field_officer_id');
        $Products->category_id  = $request->input('category_id');

        $result = $Products->getFOCateBasedAssignProduct($Products);
        if(count($result) > 0){
            $response = response()->json(['status' => 'success' , 'message' => '', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Invalid Product details']); 
        }
        return $response;
    }

    // Fetch (All)product Assign Details
    public function getAllAssignProduct()
    {
        $Products = new Product();

        $data = $Products->getAllAssignProduct();
        if(count($data) > 0 ){
            $response = response()->json(['status' => 'success' , 'message' => 'Assign Product Data retrived successfully','data'=>$data]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Error in Get details..','data'=>'null']);
        }
        return $response;
    }   

    //Area Manager Assign Product List Details Fetch
    public function getAreaManageAssignProduct(Request $request)
    {
        $Products = new Product();
        $Product->user_id = $request->input('user_id');

        $result = $Product->getAreaManageAssignProduct($Product);
        if(count($result) > 0){
            $response = response()->json(['status' => 'success' , 'message' => '', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Invalid Product details']); 
        }
        return $response;
    } 
    //Region Manager Assign Product List Details Fetch
    public function getRegionManageAssignProduct(Request $request)
    {
        $Products = new Product();
        $Product->user_id = $request->input('user_id');

        $result = $Product->getRegionManageAssignProduct($Product);
        if(count($result) > 0){
            $response = response()->json(['status' => 'success' , 'message' => '', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Invalid Product details']); 
        }
        return $response;
    } 


    // Fetch Region Based Promotional & Generic Product
    public function getRegionandTypeBasedProducts(Request $request)
    {
        $Products = new Product();
        $Products->region_id = $request->input('region_id');
        $Products->product_type = $request->input('product_type');

        $data = $Products->getRegionandTypeBasedProducts($Products);
        if(count($data) > 0 ){
            $response = response()->json(['status' => 'success' , 'message' => 'Products Data retrived successfully','data'=>$data]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Error in Get details..','data'=>'null']);
        }
        return $response;
    }
    

//Stock Maintain
    //Product Stock Request Sub to Main
    public function makeRequest(Request $request){

        $Products = new Product();
        $Products->warehouse_id  = $request->input('warehouse_id');
        $Products->warehouse_name  = $request->input('warehouse_name');
        $Products->sub_ware_id  = $request->input('sub_ware_id');
        $Products->purchase_product_list  = $request->input('purchase_product_list');
        $Products->region_id  = $request->input('region_id');
        $Products->branch_id  = $request->input('branch_id');

        $result = $Products->storeRequest($Products);
        if($result == 1){
            $response = response()->json(['status' => 'success' , 'message' => 'Product request added successfully']);
        } else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Request added failed']);
        }
        return $response;
    }
    // Fetch Stock Request Details
    public function getStockRequest(Request $request)
    {
        $Products = new Product();
        $Products->warehouse_id = $request->input('warehouse_id');
        $Products->type = $request->input('type');

        $data = $Products->getStockRequest($Products);
        if(count($data) > 0 ){
            $response = response()->json(['status' => 'success' , 'message' => 'Stock Request Data retrived successfully','data'=>$data]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'No data found..','data'=>$data]);
        }
        return $response;
    }
    // Fetch Stock Request Details List
    public function getStockRequestList($product_request_id)
    {
        $Products = new Product();
        $Products->product_request_id = $product_request_id;

        $data = $Products->getStockRequestList($Products);
        if(count($data) > 0 ){
            $response = response()->json(['status' => 'success' , 'message' => 'Stock Request Details retrived successfully','data'=>$data]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'No data found..','data'=>$data]);
        }
        return $response;
    }
    // Fetch Stock Request Details List
    public function getStockRequestAcceptList($product_request_list_id)
    {
        $Products = new Product();
        $Products->product_request_list_id = $product_request_list_id;

        $data = $Products->getStockRequestAcceptList($Products);
        if(count($data) > 0 ){
            $response = response()->json(['status' => 'success' , 'message' => 'Stock Request Details retrived successfully','data'=>$data]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'No data found..']);
        }
        return $response;
    }
    // Fetch Stock Request Details List
    public function getStockRequestBaseAcceptList($product_request_id)
    {
        $Products = new Product();
        $Products->product_request_id = $product_request_id;

        $data = $Products->getStockRequestBaseAcceptList($Products);
        if(count($data) > 0 ){
            $response = response()->json(['status' => 'success' , 'message' => 'Stock Request Details retrived successfully','data'=>$data]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'No data found..','data'=>$data]);
        }
        return $response;
    }
    // Fetch Stock Request Details List
    public function getStockRequestListInd($product_request_id,$product_id)
    {
        $Products = new Product();
        $Products->product_request_id = $product_request_id;
        $Products->product_id = $product_id;

        $data = $Products->getStockRequestListInd($Products);
        if(count($data) > 0 ){
            $response = response()->json(['status' => 'success' , 'message' => 'Stock Request Details retrived successfully','data'=>$data]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'No data found..','data'=>$data]);
        }
        return $response;
    }
    // Fetch Stock Request Details
    public function stockRequestAcceptOrReject(Request $request)
    {
        $Products = new Product();
        $Products->product_request_id = $request->input('product_request_id');
        $Products->product_request_list_id = $request->input('product_request_list_id');
        $Products->category_id = $request->input('category_id');
        $Products->category_name = $request->input('category_name');
        $Products->product_id = $request->input('product_id');
        $Products->product_genericname = $request->input('product_genericname');
        $Products->batch_no = $request->input('batch_no');
        $Products->product_qty = $request->input('product_qty');
        $Products->exp_date = $request->input('exp_date');
        $Products->product_netprice = $request->input('product_netprice');
        $Products->product_grossprice = $request->input('product_grossprice');
        $Products->product_type = $request->input('product_type');
        $Products->warehouse_id = $request->input('warehouse_id');
        $Products->price_type = $request->input('price_type');
        $Products->total_price = $request->input('total_price');

        $data = $Products->stockRequestAcceptOrReject($Products);
        if($data == 1 ){
            $response = response()->json(['status' => 'success' , 'message' => 'Stock Request Status Updated','data'=>$data]);
        }else if($data == 2){
            $response = response()->json(['status' => 'failure' , 'message' => 'Invalid issue qty']);
        }else{
            $response = response()->json(['status' => 'failure' , 'message' => 'No data found..','data'=>$data]);
        }
        return $response;
    }
    // Stock acknowledge
    public function stockAcceptAcknowledge(Request $request)
    {
        $Products = new Product();
        $Products->product_request_accept_list_id = $request->input('product_request_accept_list_id');  
        $Products->category_id = $request->input('category_id');
        $Products->category_name = $request->input('category_name');
        $Products->product_id = $request->input('product_id');
        $Products->product_genericname = $request->input('product_genericname');
        $Products->product_type = $request->input('product_type');
        $Products->batch_no = $request->input('batch_no');
        $Products->product_qty = $request->input('product_qty');
        $Products->exp_date = $request->input('exp_date');
        $Products->stockist_id = $request->input('stockist_id');
        $Products->product_netprice = $request->input('product_netprice');
        $Products->product_grossprice = $request->input('product_grossprice');
        $Products->warehouse_id = $request->input('warehouse_id');

        $data = $Products->stockAcceptAcknowledge($Products);
        if($data == 1 ){
            $response = response()->json(['status' => 'success' , 'message' => 'Stock Request Status Updated','data'=>$data]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'No data found..','data'=>$data]);
        }
        return $response;
    }
//Stock Comparision
    // Stock Compare
    public function warehouseStockCompare(Request $request)
    {
        $Products = new Product();
        $Products->warehouse_id = $request->input('warehouse_id');
        $Products->product_id = $request->input('product_id');
        $Products->type = $request->input('type');
        $Products->date = $request->input('date');

        $data = $Products->getStockCompare($Products);
        // print_r(count($data));
        if($data){
            $response = response()->json(['status' => 'success' , 'message' => 'Stock Request Data retrived successfully','data'=>$data]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'No data found..','data'=>$data]);
        }
        return $response;
    }

    // Fetch Branch Based Category Details
    public function getBranchBasedCategory(Request $request)
    {
        $Products = new Product();

        $Products->branch_id = $request->input('branch_id');
        
        $result = $Products->getBranchBasedCategory($Products);
        if(count($result) > 0){
            $response = response()->json(['status' => 'success' , 'message' => '', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'No Product Categories are available for this company']); 
        }
        return $response;
    } 

    // Update Product Details
    public function updateProductsBatchQty(Request $request)
    {
        $Products = new Product();
        $Products->user_id = $request->input('user_id');
        $Products->product_batch_stock_id  = $request->input('product_batch_stock_id');
        $Products->new_tot_qty = $request->input('new_tot_qty');
        $Products->new_avl_qty = $request->input('new_avl_qty');
        $Products->tot_qty = $request->input('tot_qty');
        $Products->avl_qty = $request->input('avl_qty');


        $result = $Products->updateProductsBatchQty($Products);
        if($result > 0){
            $response = response()->json(['status' => 'success', 'message' => 'Product Details Update successfully','data'=> $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Error in Update Product Details']);
        }
        return $response;
    }

    public function getStockBalanceSheet(Request $request)
    {
        $Products = new Product();
        $Products->start_date  = $request->input('start_date');
        $Products->end_date  = $request->input('end_date');
        $Products->product_id  = $request->input('product_id');
        $Products->batch_id  = $request->input('batch_id');
        $Products->warehouse_id  = $request->input('warehouse_id');

        $result = $Products->getStockBalanceSheet($Products);
        if(count($result) > 0){
            $response = response()->json(['status' => 'success' , 'message' => '', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'No data found']); 
        }
        return $response;
    } 
    public function uploadGoodsRecivedStock(Request $request){
        
        $Products = new Product();
        $Products->warehouse_id = $request->input('warehouse_id');
        $Products->stockist_id = $request->input('stockist_id');
        $Products->region_id = $request->input('region_id');
        $Products->branch_id  = $request->input('branch_id');
        $Products->invoice_no  = $request->input('invoice_no');
        $Products->supplier_name  = $request->input('supplier_name');
        $Products->invoice_date  = $request->input('invoice_date');
        $Products->branch_id  = $request->input('branch_id');
        $Products->created_at  = $request->input('created_at');
        $Products->updated_at  = $request->input('updated_at');
        $Products->payment_type  = $request->input('payment_type');
        $Products->productpath = $Products->getFilePath($request->file('productall'));
        $Products->productall = $request->file('productall');
        $result = $Products->uploadGoodsRecivedStock($Products);
        // print_r($result);
        // exit();
        if($result == 1){
            $response = response()->json(['status' => 'success' , 'message' => 'Product Stock added successfully']);
        }else if($result == 2){
            $response = response()->json(['status' => 'failure' , 'message' => 'Please Fill Records in the excel']);
        }else if($result == 3){
            $response = response()->json(['status' => 'failure' , 'message' => 'Please upload excel file only']);
        }else if($result == 4){
            $response = response()->json(['status' => 'failure' , 'message' => 'Please Fill All Field in the excel']);
        }else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Product added failed']);
        }
        return $response;
    }
    public function getBatchDetails(Request $request)
    {
        $Products = new Product();
        $Products->product_id  = $request->input('product_id');
        $Products->warehouse_id  = $request->input('warehouse_id');
        
        $data = $Products->getBatchDetails($Products);
        if(count($data) > 0 ){
            $response = response()->json(['status' => 'success' , 'message' => 'Warehouse Based Products Data retrived successfully','data'=>$data]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'No data found','data'=>$data]);
        }
        return $response;
    } 
    //Admin Or Sub Admin Add the Product Stock
    public function stockAdjustAddProduct(Request $request){
        
        $Products = new Product();
        $Products->warehouse_id = $request->input('warehouse_id');
        $Products->stockist_id = $request->input('stockist_id');
        $Products->region_id = $request->input('region_id');
        $Products->branch_id  = $request->input('branch_id');
        $Products->invoice_no  = $request->input('invoice_no');
        $Products->supplier_name  = $request->input('supplier_name');
        $Products->invoice_date  = $request->input('invoice_date');
        $Products->branch_id  = $request->input('branch_id');
        $Products->payment_type  = $request->input('payment_type');
        $Products->grand_tot_amt  = $request->input('grand_tot_amt');
        $Products->prduct_details = $request->input('prduct_details');
        $Products->created_at = $request->input('created_at');
        $Products->updated_at = $request->input('updated_at');
        
        $result = $Products->stockAdjustAddProduct($Products);

        if($result == 1){
            $response = response()->json(['status' => 'success' , 'message' => 'Product Stock added successfully']);
        }else if($result == 2){
            $response = response()->json(['status' => 'failure' , 'message' => 'Please Fill Records in the excel']);
        }else if($result == 3){
            $response = response()->json(['status' => 'failure' , 'message' => 'Please upload excel file only']);
        }else if($result == 4){
            $response = response()->json(['status' => 'failure' , 'message' => 'Please Fill All Field in the excel']);
        }else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Product added failed']);
        }
        return $response;
    }

    //Purchase Order
    public function addPORequest(Request $request){
        $Products = new Product();
        $Products->supplier_id = $request->input('supplier_id');
        $Products->supplier_name = $request->input('supplier_name');
        $Products->warehouse_id = $request->input('warehouse_id');
        $Products->delivery_schedule = $request->input('delivery_schedule');
        $Products->delivery_address = $request->input('delivery_address');
        $Products->grand_total = $request->input('grand_total');
        $Products->payment_terms = $request->input('payment_terms');
        $Products->freight_terms = $request->input('freight_terms');
        $Products->stockist_id = $request->input('stockist_id');
        $Products->region_id = $request->input('region_id');
        $Products->branch_id  = $request->input('branch_id');
        $Products->product_details = $request->input('product_details');
        
        $result = $Products->addPORequest($Products);

        if($result == 1){
            $response = response()->json(['status' => 'success' , 'message' => 'Purchase order request generated successfully']);
        }else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Purchase order request generated falied. Please try again']);
        }
        return $response;
    }

    public function getAllPORequest(Request $request) {
        $Products = new Product();
        if($request->input('branch_id')){
            $Products->branch_id =   $request->input('branch_id');
        }else{
            $Products->branch_id =   '';
        }
        if($request->input('region_id')){
            $Products->region_id =   $request->input('region_id');
        }else{
            $Products->region_id =   '';
        }
        if($request->input('status')){
            $Products->status =  $request->input('status');
        }else{
            $Products->status =  '';
        }
        if($request->input('stockist_id')){
            $Products->stockist_id =  $request->input('stockist_id');
        }else{
            $Products->stockist_id =  '';
        }
        if($request->input('warehouse_id')){
            $Products->warehouse_id =  $request->input('warehouse_id');
        }else{
            $Products->warehouse_id =  '';
        }
        $result = $Products->getAllPORequest($Products);
        if(Count($result)) {
            $response = response()->json(['status' => 'success', 'message' => 'Purchase Order List', 'data' => $result]);
        } else {
            $response = response()->json(['status' => 'failure' , 'message' => 'No Data Found', 'data' => []]);
        }
        return $response;
    }

    public function getPODetails(Request $request) {
        $Products = new Product();
        $Products->purchase_order_id =   $request->input('po_id');
        $result = $Products->getPODetails($Products);
        if($result) {
            $response = response()->json(['status' => 'success', 'message' => 'Purchase Order Details', 'data' => $result]);
        } else {
            $response = response()->json(['status' => 'failure' , 'message' => 'No Data Found', 'data' => []]);
        }
        return $response;
    }

    public function getPOInvoiceDetails(Request $request) {
        $Products = new Product();
        $Products->purchase_order_id =   $request->input('po_id');
        $result = $Products->getPOInvoiceDetails($Products);
        if($result) {
            $response = response()->json(['status' => 'success', 'message' => 'Purchase Order Details', 'data' => $result]);
        } else {
            $response = response()->json(['status' => 'failure' , 'message' => 'No Data Found', 'data' => []]);
        }
        return $response;
    }

    public function acceptPORequest(Request $request) {
        $Products = new Product();
        $Products->purchase_order_id =   $request->input('po_id');
        $result = $Products->acceptPORequest($Products);
        if($result) {
            $response = response()->json(['status' => 'success', 'message' => 'Request accepted']);
        } else {
            $response = response()->json(['status' => 'failure' , 'message' => 'Please try again']);
        }
        return $response;
    }

    public function generatePOInvoice(Request $request){
        $Products = new Product();
        $Products->purchase_order_id = $request->input('purchase_order_id');
        $Products->receipt_date = $request->input('receipt_date');
        $Products->supplier_id = $request->input('supplier_id');
        $Products->supplier_name = $request->input('supplier_name');
        $Products->warehouse_id = $request->input('warehouse_id');
        $Products->warehouse_name = $request->input('warehouse_name');
        $Products->delivery_address = $request->input('delivery_address');
        $Products->grand_total = $request->input('grand_total');
        $Products->payment_terms = $request->input('payment_terms');
        $Products->freight_terms = $request->input('freight_terms');
        $Products->region_id = $request->input('region_id');
        $Products->branch_id  = $request->input('branch_id');
        $Products->created_by_id  = $request->input('created_by_id');
        $Products->created_by_name  = $request->input('created_by_name');
        $Products->product_details = $request->input('product_details');
        
        $result = $Products->generatePOInvoice($Products);

        if($result == 1){
            $response = response()->json(['status' => 'success' , 'message' => 'Purchase order invoice generated successfully']);
        }else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Purchase order invoice generated falied. Please try again']);
        }
        return $response;
    }

    public function getAllPOInvoice(Request $request) {
        $Products = new Product();
        if($request->input('start_date')){
            $Products->start_date = $request->input('start_date');
            $Products->end_date = $request->input('end_date');
        }else{
            $Products->start_date = '';
            $Products->end_date = '';
        }
        if($request->input('invoice_id')){
            $Products->invoice_id =   $request->input('invoice_id');
        }else{
            $Products->invoice_id =   '';
        }
        if($request->input('branch_id')){
            $Products->branch_id =   $request->input('branch_id');
        }else{
            $Products->branch_id =   '';
        }
        if($request->input('region_id')){
            $Products->region_id =   $request->input('region_id');
        }else{
            $Products->region_id =   '';
        }
        if($request->input('payment_status')){
            $Products->payment_status =  $request->input('payment_status');
        }else{
            $Products->payment_status =  '';
        }

        if($request->input('product_status')){
            $Products->product_status =  $request->input('product_status');
        }else{
            $Products->product_status =  '';
        }
        if($request->input('warehouse_id')){
            $Products->warehouse_id =  $request->input('warehouse_id');
        }else{
            $Products->warehouse_id =  '';
        }
        if($request->input('supplier_id')){
            $Products->supplier_id =  $request->input('supplier_id');
        }else{
            $Products->supplier_id =  '';
        }
        $result = $Products->getAllPOInvoice($Products);
        if(Count($result)) {
            $response = response()->json(['status' => 'success', 'message' => 'Purchase Order Invoice', 'data' => $result]);
        } else {
            $response = response()->json(['status' => 'failure' , 'message' => 'No Data Found', 'data' => []]);
        }
        return $response;
    }

    public function getPOInvoiceProducts(Request $request) {
        $Products = new Product();
        $Products->invoice_no =  $request->input('invoice_no');
        $Products->product_status =  $request->input('product_status');
        $result = $Products->getPOInvoiceProducts($Products);
        if(Count($result)) {
            $response = response()->json(['status' => 'success', 'message' => 'Purchase Order Invoice Products', 'data' => $result]);
        } else {
            $response = response()->json(['status' => 'failure' , 'message' => 'No Data Found', 'data' => []]);
        }
        return $response;
    }

    // Stock acknowledge
    public function poStockAcknowledge(Request $request)
    {
        $Products = new Product();
        $Products->purchase_invoice_product_id = $request->input('purchase_invoice_product_id');  
        $Products->purchase_invoice_id = $request->input('purchase_invoice_id');  
        $Products->category_id = $request->input('category_id');
        $Products->category_name = $request->input('category_name');
        $Products->product_id = $request->input('product_id');
        $Products->product_genericname = $request->input('product_genericname');
        $Products->batch_no = $request->input('batch_no');
        $Products->product_qty = $request->input('product_qty');
        $Products->exp_date = $request->input('exp_date');
        $Products->stockist_id = $request->input('stockist_id');
        $Products->warehouse_id = $request->input('warehouse_id');
        $data = $Products->poStockAcknowledge($Products);
        if($data == 1 ){
            $response = response()->json(['status' => 'success' , 'message' => 'Stock Acknowledged','data'=>$data]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'No data found..','data'=>$data]);
        }
        return $response;
    }

    public function addPOPaymentDetails(Request $request){
        $Products = new Product();

        $Products->invoice_id            = $request->input('invoice_id');
        $Products->supplier_id           = $request->input('supplier_id');
        $Products->supplier_name         = $request->input('supplier_name');
        $Products->payment_type       = $request->input('payment_type');
        $Products->balance_amount     = $request->input('balance_amount');
        $Products->payment_amount     = $request->input('payment_amount');
        $Products->created_by_id      = $request->input('created_by_id');
        $Products->created_by_name    = $request->input('created_by_name');        
        if($request->input('bank_name')){
            $Products->bank_name = $request->input('bank_name');
        }else{
            $Products->bank_name = "";
        }
        if($request->input('card_type')){
            $Products->card_type = $request->input('card_type');
        }else{
            $Products->card_type = "";
        }
        if($request->input('card_no')){
            $Products->card_no = $request->input('card_no');
        }else{
            $Products->card_no = "";
        }
        if($request->input('holder_name')){
            $Products->holder_name = $request->input('holder_name');
        }else{
            $Products->holder_name = "";
        }
        if($request->input('account_no')){
            $Products->account_no = $request->input('account_no');
        }else{
            $Products->account_no = "";
        }
        if($request->input('transaction_id')){
            $Products->transaction_id = $request->input('transaction_id');
        }else{
            $Products->transaction_id = "";
        }
        if($request->input('cheque_no')){
            $Products->cheque_no = $request->input('cheque_no');
        }else{
            $Products->cheque_no = "";
        }

        $results = $Products->addPOPaymentDetails($Products);

        if($results == 1){
            $response = response()->json(['status'=>'success', 'message'=>"Payment details added successfully"]);
        }else{
            $response = response()->json(['status'=>'failure', 'message'=>"Something went wrong"]);
        }
        return $response;
    }
    public function getPOPaymentTransaction(Request $request) {
        $Products = new Product();
        $Products->invoice_id =  $request->input('invoice_id');
        $result = $Products->getPOPaymentTransaction($Products);
        if(Count($result)) {
            $response = response()->json(['status' => 'success', 'message' => 'Purchase Order Payment Transaction', 'data' => $result]);
        } else {
            $response = response()->json(['status' => 'failure' , 'message' => 'No Data Found', 'data' => []]);
        }
        return $response;
    }
}
