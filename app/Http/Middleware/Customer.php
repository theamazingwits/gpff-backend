<?php

namespace App\Http\Controllers;

use Closure;
use DB;
use Mail;
use Carbon\Carbon;

require(base_path().'/app/Http/Middleware/Common.php');

class Customer
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        return $next($request);
    }

//////////////////////////////
//Cart Management Api Calls//
//////////////////////////////
    //Store Product to the Cart
    public function storeToCart($Customers){

        $cart = DB::table('gpff_cus_cart')
              ->where('customer_id', $Customers->customer_id)
              ->where('product_id', $Customers->product_id)
              ->get();
        if(count($cart)>0){
            return 2;
        }else{

         $values = array(
            'product_id'   => $Customers->product_id , 
            'customer_id'  => $Customers->customer_id ,
            'customer_name' => $Customers->customer_name ,
            'category_id'  => $Customers->category_id ,
            'category_name'  => $Customers->category_name ,
            'product_genericname' => $Customers->product_genericname ,
            'product_qty' => $Customers->product_qty ,
            'product_packingsize' => $Customers->product_packingsize ,
            'product_price'       => $Customers->product_price ,
            'product_netprice'    => $Customers->product_netprice ,
            'product_grossprice'  => $Customers->product_grossprice ,
            'product_discount'    => $Customers->product_discount ,
            'product_tot_price'  => $Customers->product_tot_price ,
            'product_type'      => $Customers->product_type ,
            'branch_id'        => $Customers->branch_id ,
            'region_id'        => $Customers->region_id ,
            'area_id'        => $Customers->area_id , 
            'created_at'        => date('Y-m-d H:i:s') , 
            'updated_at'        => date('Y-m-d H:i:s')
        );

        $cart_id   =   DB::table('gpff_cus_cart')
                        ->insertGetId($values);
        return 1;   
        }
    }

    // Fetch Cart Details 
    public function getCartDetails($Customers) 
    {
        //Placed Order Details
        return  DB::table('gpff_cus_cart')
                        ->where('customer_id', $Customers->customer_id)
                        ->orderBy('updated_at','DESC')
                        ->get();    
    }

    //Delete Cart details
    public function deleteCartDetails($Customers)
    {
        return  DB::table('gpff_cus_cart')
                ->where('gpff_cus_cart_id', $Customers->gpff_cus_cart_id)
                ->delete();
        return 1;
    }
    // Fetch Customer Based Order History Details Fetch
    public function getCusOrderDetailsPagi($Customers)
    {   
        $data = DB::table('gpff_order as gpor')
                ->join('gpff_warehouse as gpwh', 'gpor.warehouse_id', 'gpwh.warehouse_id')
                ->where('customer_id', $Customers->customer_id)
                ->orderBy('gpor.order_id','DESC')
                ->select(DB::RAW('SQL_CALC_FOUND_ROWS gpor.order_id'),'gpor.task_id','gpor.field_officer_id','gpor.field_officer_name','gpor.area_manager_id','gpor.customer_id','gpor.customer_name','gpor.order_date','gpor.or_tot_price','gpor.acc_or_tot_price','gpor.branch_id','gpor.region_id','gpor.area_id','gpor.stokist_id','gpor.warehouse_id','gpor.order_status','gpwh.warehouse_name','gpor.reject_reason','gpor.or_type','gpor.invoice_type','gpor.created_at','gpor.updated_at');

        $total_count = $data->count();      
        
        $data->offset($Customers->start);
        $data->limit($Customers->length);
        $data = $data->get();

        $total_filter_count = DB::select(DB::Raw('SELECT FOUND_ROWS() AS    totalfiltercount;'));

        return array(
                0 => $total_count,
                1 => $total_filter_count[0]->totalfiltercount,
                2 => $data
            );
    }

    // Fetch Cart Details 
    public function getCusPrevOrderDetails($Customers) 
    {
        return  DB::table('gpff_order as gpor')
                        ->join('gpff_users as gpusr', 'gpor.order_created_by','gpusr.user_id')
                        ->join('gpff_warehouse as gpwar', 'gpwar.warehouse_id', 'gpor.warehouse_id')
                        ->whereNotIn('gpor.order_status',[0,4])
                        ->where('gpor.payment_status',2)
                        ->where('gpor.customer_id', $Customers->customer_id)
                        ->orderBy('gpor.created_at','DESC')
                        // ->limit(5)
                        // ->select(DB::raw())
                        ->get(['gpor.order_id','gpor.task_id','gpor.field_officer_id','gpor.field_officer_name','gpor.area_manager_id','gpor.customer_id','gpor.customer_name','gpor.order_date','gpor.or_gross_total','gpor.or_tot_price','gpor.or_blc_price','gpor.acc_or_tot_price','gpor.order_discount','gpor.spl_discount','gpor.order_created_by','gpor.branch_id','gpor.region_id','gpor.area_id','gpor.stokist_id','gpor.warehouse_id','gpor.order_rej_date','gpor.order_status','gpor.admin_cmt','gpor.invoice_no','gpor.invoice_type','gpor.reject_reason','gpor.net_or_gross_price','gpor.payment_type','gpor.credit_lim','gpor.credit_valid_to','gpor.or_type','gpor.telecalling_id','gpor.telecalling_type','gpor.payment_status','gpor.payment_name','gpor.payment_img','gpor.payment_msg','gpor.created_at',DB::raw("CONCAT(gpusr.firstname,' ',gpusr.lastname) AS full_name"),'gpwar.warehouse_name']);    
    }

    //Store Complain
    public function addComplain($Customers){
         $values = array(
            'customer_id'       => $Customers->customer_id ,
            'customer_name'     => $Customers->customer_name ,
            'complain_title'    => $Customers->complain_title ,
            'complain_name'     => $Customers->complain_name ,
            'complain_text'     => $Customers->complain_text ,
            'complain_file'     => $Customers->complain_file ,
            'branch_id'         => $Customers->branch_id ,
            'region_id'         => $Customers->region_id ,
            'area_id'           => $Customers->area_id , 
            'created_at'        => date('Y-m-d H:i:s') , 
            'updated_at'        => date('Y-m-d H:i:s')
        );

        $cart_id   =   DB::table('gpff_cus_complain')
                        ->insertGetId($values);
        return 1;   
    }
    //Update Complain Details Status
    public function updateComplain($Customers)
    {
        $update_values = array(
            'res_msg'      => $Customers->res_msg,
            'status'       => $Customers->status,
            'approve_id'   => $Customers->approve_id,
            'approve_name' => $Customers->approve_name,
            'updated_at'   => date('Y-m-d H:i:s')
        );
        
        DB::table('gpff_cus_complain')
        ->where('complain_id', $Customers->complain_id)
        ->update($update_values);
        return 1;
    }

    // Fetch Complain Details 
    public function getCusBasedComplain($Customers) 
    {
        return  DB::table('gpff_cus_complain')
                ->where('customer_id', $Customers->customer_id)
                // ->where('status', $Customers->status)
                ->orderBy('updated_at','DESC')
                ->get();    
    }

    // Fetch Complain Details For Area and Region Manager
    public function getUserBasedComplain($Customers) 
    {   
        $user_role = DB::table('gpff_users')
                    ->where('user_id', $Customers->user_id)
                    ->first();
                    
        if($Customers->type == 1){    
            if($user_role->role == 3){
                $myArray = explode(',', $user_role->region_id);

                return  DB::table('gpff_cus_complain')
                    ->whereIn('region_id', $myArray)
                    ->where('status', 0)
                    ->orderBy('updated_at','DESC')
                    ->get();    
            }else if($user_role->role == 4){
                $myArray = explode(',', $user_role->area_id);

                return  DB::table('gpff_cus_complain')
                    ->whereIn('area_id', $myArray)
                    ->where('status', 0)
                    ->orderBy('updated_at','DESC')
                    ->get();    
            }
        }else if($Customers->type == 2){
            return  DB::table('gpff_cus_complain')
                    ->where('approve_id', $user_role->user_id)
                    ->where('status', 1)
                    ->orderBy('updated_at','DESC')
                    ->get();    
        } else{
            return  DB::table('gpff_cus_complain')
                    ->where('status', 1)
                    ->orderBy('updated_at','DESC')
                    ->get();
        }
    }

    // Fetch Ind Complain Details 
    public function getIndComplain($Customers) 
    {
        return  DB::table('gpff_cus_complain')
                ->where('complain_id', $Customers->complain_id)
                ->get();    
    }
    // Delete Ind Complain Details 
    public function deleteComplain($Customers) 
    {   
        return  DB::table('gpff_cus_complain')
                ->where('complain_id', $Customers->complain_id)
                ->delete();
        return 1;
    }
}
