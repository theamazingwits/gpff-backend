<?php

namespace App\Http\Controllers;

use Closure;
use DB;
use Mail;
use Carbon\Carbon;

require(base_path().'/app/Http/Middleware/Common.php');

class FeedAndReview
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        return $next($request);
    }

    //1-NOT AVAILABLE,2-FIRST CALL( LEVEL OF DOCTOR),3-PROMOTION ITEMS DETAILS,4-FEEDBACK,5-COMPLAINT DISTRIBUTION,6-COMPLAINT PRODUCTS,7-COMPLAINT PRICE ,8-COMPETITOR
    //Store FeedAndReviews Details 
    public function storeFeedBack($FeedAndReviews){

        if($FeedAndReviews->type == 3 || $FeedAndReviews->type == 6 || $FeedAndReviews->type == 7){

            foreach($FeedAndReviews->product_details as $productdetaile)
            {
                $values = array(
                    'task_id'           => $FeedAndReviews->task_id , 
                    'field_officer_id'  => $FeedAndReviews->field_officer_id ,
                    'field_officer_name'=> $FeedAndReviews->field_officer_name ,
                    'area_manager_id'   => $FeedAndReviews->area_manager_id ,
                    'customer_id'       => $FeedAndReviews->customer_id , 
                    'feedback'          => $productdetaile['feedback'],
                    'product_id'        => $productdetaile['product_id'],
                    'product_genericname' => $productdetaile['product_genericname'],
                    'type'              => $FeedAndReviews->type , 
                    'created_at'        => date('Y-m-d H:i:s') , 
                    'updated_at'        => date('Y-m-d H:i:s')
                );

                DB::table('gpff_feedback')
                ->insert($values);
            }
        } elseif($FeedAndReviews->type == 1 || $FeedAndReviews->type == 2 || $FeedAndReviews->type == 4 || $FeedAndReviews->type == 5 || $FeedAndReviews->type == 8){
            $values = array(
                'task_id'           => $FeedAndReviews->task_id , 
                'field_officer_id'  => $FeedAndReviews->field_officer_id ,
                'field_officer_name'=> $FeedAndReviews->field_officer_name ,
                'area_manager_id'   => $FeedAndReviews->area_manager_id ,
                'customer_id'       => $FeedAndReviews->customer_id , 
                'feedback'          => $FeedAndReviews->feedback , 
                'type'              => $FeedAndReviews->type ,
                'telecalling_id'    => $FeedAndReviews->telecalling_id,
                'telecalling_type'  => $FeedAndReviews->telecalling_type, 
                'created_at'        => date('Y-m-d H:i:s') , 
                'updated_at'        => date('Y-m-d H:i:s')
            );

            $feed_id = DB::table('gpff_feedback')
            ->insertGetId($values);

            if($FeedAndReviews->telecalling_id != ''){
                if($FeedAndReviews->telecalling_type == 2){

                    $tele_value = array(
                // 'events'            => $FeedAndReviews->events,
                        'telecalling_type'  => $FeedAndReviews->telecalling_type,
                        'feedback_id'       => $feed_id,
                        'feedback' => $FeedAndReviews->feedback,
                        'status'            =>1,
                        'updated_at'        => date('Y-m-d H:i:s')
                    );

                    DB::table('gpff_telecalling')
                    ->where('telecalling_id', $FeedAndReviews->telecalling_id)
                    ->update($tele_value);
                }else{
                    $tele_value = array(
                // 'events'            => $FeedAndReviews->events,
                        'telecalling_type'  => $FeedAndReviews->telecalling_type,
                        'feedback' => $FeedAndReviews->feedback,
                        'not_available_id'       => $feed_id,
                        'status'            =>1,
                        'updated_at'        => date('Y-m-d H:i:s')
                    );

                    DB::table('gpff_telecalling')
                    ->where('telecalling_id', $FeedAndReviews->telecalling_id)
                    ->update($tele_value);
                }
            }else{
                $task_value = array(
                // 'events'            => $FeedAndReviews->events,
                        'feedback' => $FeedAndReviews->feedback,
                        'task_status' =>3,
                        'updated_at'  => date('Y-m-d H:i:s')
                    );

                    DB::table('gpff_task')
                    ->where('task_id', $FeedAndReviews->task_id)
                    ->update($task_value);
            }
        }

        return 1;   
    }

    //Store Revist Details
    public function storeRevisit($FeedAndReviews){

         $values = array(
            'task_id'           => $FeedAndReviews->task_id , 
            'field_officer_id'  => $FeedAndReviews->field_officer_id ,
            'field_officer_name'=> $FeedAndReviews->field_officer_name ,
            'area_manager_id'        => $FeedAndReviews->area_manager_id ,
            'customer_id'       => $FeedAndReviews->customer_id , 
            'date'              => $FeedAndReviews->date ,
            'Revisit_reason'     => $FeedAndReviews->Revisit_reason , 
            'created_at'        => date('Y-m-d H:i:s') , 
            'updated_at'        => date('Y-m-d H:i:s')
        );

        DB::table('gpff_Revisit')
        ->insert($values);
            
        //Status Update
            $task_det = DB::table('gpff_task')
                    ->where('task_id',$FeedAndReviews->task_id)
                    ->First(['task_start_date_time']);
       
            $current_date = date('Y-m-d H:i:s');
            $to = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $current_date);
            $from = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $task_det->task_start_date_time);
            $diff_in_minutes = $to->diffInMinutes($from);

            $update_values = array(
                'task_status'           => 3 , 
                'task_end_date_time'    => date('Y-m-d H:i:s'),
                'task_active_minu'      => $diff_in_minutes,
                'updated_at'            => date('Y-m-d H:i:s')
            );

            DB::table('gpff_task')
            ->where('task_id',$FeedAndReviews->task_id)
            ->update($update_values);
        //End
        //Reassign For the Task
            $userrole = DB::table('gpff_users')
                ->where('user_id',$FeedAndReviews->created_by)
                ->First();

             $values = array(
                'created_by'        => $FeedAndReviews->created_by , 
                'field_officer_id'  => $FeedAndReviews->field_officer_id ,
                'area_manager_id'   => $FeedAndReviews->area_manager_id ,
                'region_id'         => $FeedAndReviews->region_id ,
                'branch_id'         => $FeedAndReviews->branch_id ,
                'area_id'           => $FeedAndReviews->area_id ,
                'customer_id'       => $FeedAndReviews->customer_id ,
                'customer_type'     => $FeedAndReviews->customer_type , 
                'task_title'        => $FeedAndReviews->task_title ,
                'task_desc'         => $FeedAndReviews->task_desc , 
                'task_date_time'    => $FeedAndReviews->task_date_time , 
                'created_at'        => date('Y-m-d H:i:s') , 
                'updated_at'        => date('Y-m-d H:i:s')
            );
        //3-Regionadmin,4-Areamanager,5-fieldofficer,6-salesref,7-stockist
        //Notification Entry
                if($userrole->role == 3){
                    $message = "Region Manager ".$userrole->firstname."Has Assign One Task To you.";
                    $page_id = 'UNKNOWN';

                    $cmn = new Common();
                    $cmn->insertNotification($FeedAndReviews->created_by,$userrole->firstname,$FeedAndReviews->field_officer_id,$message,$page_id);
                } else if($userrole->role == 4){
                    $message = "Manager ".$userrole->firstname." Has Assign One Task To you.";
                    $page_id = 'UNKNOWN';

                    $cmn = new Common();
                    $cmn->insertNotification($FeedAndReviews->created_by,$userrole->firstname,$FeedAndReviews->field_officer_id,$message,$page_id);
                }
        //End Notification Entry
            DB::table('gpff_task')
            ->insert($values);
        //End Reassign For the Task
        return 1;   
    }
}
