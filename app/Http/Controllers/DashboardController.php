<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use File;
use Mail;

require(base_path().'/app/Http/Middleware/Dashboard.php');

class DashboardController extends Controller
{
//// Add SIde DashBoard
    //Over all Admin Dashboard Count
    public function getAdminDashCount(Request $request)
    {
        $Dashboards = new Dashboard();
        $Dashboards->user_id = $request->input('user_id');

        $result = $Dashboards->getAdminDashCount($Dashboards);
        if(count($result) > 0){
            $response = response()->json(['status' => 'success' , 'message' => '', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Invalid Count details']); 
        }
        return $response;
    }
    // Area Manager Dashboard Count
    public function getAreaDashCount(Request $request)
    {
        $Dashboards = new Dashboard();
        $Dashboards->area_manager_id = $request->input('area_manager_id');

        $result = $Dashboards->getAreaDashCount($Dashboards);
        if(count($result) > 0){
            $response = response()->json(['status' => 'success' , 'message' => '', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Invalid Count details']); 
        }
        return $response;
    }
    // Region Manager Dashboard Count
    public function getRegionDashCount(Request $request)
    {
        $Dashboards = new Dashboard();
        $Dashboards->user_id = $request->input('region_manager_id');
        $result = $Dashboards->getRegionDashCount($Dashboards);
        if(count($result) > 0){
            $response = response()->json(['status' => 'success' , 'message' => '', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Invalid Count details']); 
        }
        return $response;
    }
    // Branch Manager Dashboard Count
    public function getBranchDashCount(Request $request)
    {
        $Dashboards = new Dashboard();
        $Dashboards->user_id = $request->input('Branch_manager_id');
        $result = $Dashboards->getBranchDashCount($Dashboards);
        if(count($result) > 0){
            $response = response()->json(['status' => 'success' , 'message' => '', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Invalid Count details']); 
        }
        return $response;
    }
    // Stockist Dashboard Count
    public function getStockistDashCount(Request $request)
    {
        $Dashboards = new Dashboard();
        $Dashboards->user_id = $request->input('stockist_id');
        $result = $Dashboards->getStockistDashCount($Dashboards);
        if(count($result) > 0){
            $response = response()->json(['status' => 'success' , 'message' => '', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Invalid Count details']); 
        }
        return $response;
    }
// Add SIde DashBoard

//Web Side Dashboard
    // Admin Dashboard Count
    public function getDashTaskCount(Request $request)
    {
        $Dashboards = new Dashboard();
        $Dashboards->user_id = $request->input('user_id');

        $result = $Dashboards->getDashTaskCount($Dashboards);
        if(count($result) > 0){
            $response = response()->json(['status' => 'success' , 'message' => '', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Invalid Grpah Count details']); 
        }
        return $response;
    }
    // Admin Dashboard Graph Count sales based Product 
    public function getAdminDashGraph(Request $request)
    {
        $Dashboards = new Dashboard();
        $Dashboards->user_id = $request->input('user_id');

        $result = $Dashboards->getAdminDashGraph($Dashboards);
        if(count($result) > 0){
            $response = response()->json(['status' => 'success' , 'message' => '', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Invalid Graph details']); 
        }
        return $response;
    }

    // Branch Manager Dashboard Count
    public function getBranchDashTaskCount(Request $request)
    {
        $Dashboards = new Dashboard();
        $Dashboards->user_id = $request->input('user_id');

        $result = $Dashboards->getBranchDashTaskCount($Dashboards);
        if(count($result) > 0){
            $response = response()->json(['status' => 'success' , 'message' => '', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Invalid Grpah Count details']); 
        }
        return $response;
    }
    // Branch Dashboard Graph Count sales based Product 
    public function getBranchDashGraph(Request $request)
    {
        $Dashboards = new Dashboard();
        $Dashboards->user_id = $request->input('user_id');

        $result = $Dashboards->getBranchDashGraph($Dashboards);
        if(count($result) > 0){
            $response = response()->json(['status' => 'success' , 'message' => '', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Invalid Graph details']); 
        }
        return $response;
    }

    // Region Manager Dashboard Count
    public function getRegionDashTaskCount(Request $request)
    {
        $Dashboards = new Dashboard();
        $Dashboards->user_id = $request->input('user_id');

        $result = $Dashboards->getRegionDashTaskCount($Dashboards);
        if(count($result) > 0){
            $response = response()->json(['status' => 'success' , 'message' => '', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Invalid Grpah Count details']); 
        }
        return $response;
    }
    // Region Dashboard Graph Count sales based Product 
    public function getRegionDashGraph(Request $request)
    {
        $Dashboards = new Dashboard();
        $Dashboards->user_id = $request->input('user_id');

        $result = $Dashboards->getRegionDashGraph($Dashboards);
        if(count($result) > 0){
            $response = response()->json(['status' => 'success' , 'message' => '', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Invalid Graph details']); 
        }
        return $response;
    }

    // Area Manager Dashboard Count
    public function getAreaDashTaskCount(Request $request)
    {
        $Dashboards = new Dashboard();
        $Dashboards->user_id = $request->input('user_id');

        $result = $Dashboards->getAreaDashTaskCount($Dashboards);
        if(count($result) > 0){
            $response = response()->json(['status' => 'success' , 'message' => '', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Invalid Grpah Count details']); 
        }
        return $response;
    }
    // Manager Dashboard Graph Count
    public function getManagDashGraph(Request $request)
    {
        $Dashboards = new Dashboard();
        $Dashboards->area_manager_id = $request->input('area_manager_id');

        $result = $Dashboards->getManagDashGraph($Dashboards);
        if($result){
            $response = response()->json(['status' => 'success' , 'message' => '', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Invalid Graph details']); 
        }
        return $response;
    }
    

    // Manager Dashboard Count
    public function getManagDashCount(Request $request)
    {
        $Dashboards = new Dashboard();
        $Dashboards->area_manager_id = $request->input('area_manager_id');

        $result = $Dashboards->getManagDashCount($Dashboards);
        if(count($result) > 0){
            $response = response()->json(['status' => 'success' , 'message' => '', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Invalid Count details']); 
        }
        return $response;
    }
    

}
