<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use Mail;
use PDF;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
//require(base_path().'/app/Http/Middleware/Common.php');

class TargetExpire extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Target:Expired';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Automated Email Notification For User Target Expired Alert';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //Establishment Document
        $exp_date = DB::table('gpff_insentive')
                ->where('insentive_status','1')
                ->get(['insentive_id','from_date','to_date','created_by','fo_id','insentive_type','fo_name']);
                
        foreach($exp_date as $value){
                
            $cr_name = DB::table('gpff_users')
                        ->where('user_id',$value->created_by)
                        ->First();

            $now = Carbon::now();
            $start_date = Carbon::parse($value->from_date);
            $end_date = Carbon::parse($value->to_date);

            if($now->between($start_date,$end_date)){

                $find_diff = Carbon::now()->diffInDays($value->to_date);

                if($find_diff == 7) {
                    $message = 'Your Target Will be Expired With In 7 Days.';
                    $message1 = 'Your User '.$value->fo_name.' Target Will be Expired With In 7 Days.';
                    $page_id = 'UNKNOWN';
                    $cmn = new Common();
                    $cmn->insertNotification($value->created_by,$cr_name->$firstname,$value->fo_id,$message,$page_id);
                    $cmn->insertNotification($value->fo_id,$value->fo_name,$value->created_by,$message1,$page_id);
                    
                } else if($find_diff == 1) {
                    $message = 'Your Target Will be Expired With In 1 Days.';
                    $message1 = 'Your User '.$value->fo_name.' Target Will be Expired With In 1 Days.';
                    $page_id = 'UNKNOWN';
                    $cmn = new Common();
                    $cmn->insertNotification($value->created_by,$cr_name->$firstname,$value->fo_id,$message,$page_id);
                    $cmn->insertNotification($value->fo_id,$value->fo_name,$value->created_by,$message1,$page_id);
                    
                } else if($find_diff == 0) {
                    $message = 'Your Target Will be Expired Today.';
                    $message1 = 'Your User '.$value->fo_name.' Target Will be Expired Today.';
                    $page_id = 'UNKNOWN';
                    $cmn = new Common();
                    $cmn->insertNotification($value->created_by,$cr_name->$firstname,$value->fo_id,$message,$page_id);
                    $cmn->insertNotification($value->fo_id,$value->fo_name,$value->created_by,$message1,$page_id);
                    
                }
            } else {
                $message = 'Your Target Will be Expired.';
                $message1 = 'Your User '.$value->fo_name.' Target Will be Expired.Please Assign New Target.';
                $page_id = 'UNKNOWN';
                $cmn = new Common();
                $cmn->insertNotification($value->created_by,$cr_name->$firstname,$value->fo_id,$message,$page_id);
                $cmn->insertNotification($value->fo_id,$value->fo_name,$value->created_by,$message1,$page_id);

                //Target Status Changes
                $update_values = array(
                    'insentive_status'     => 2, 
                    'updated_at'        => date('Y-m-d H:i:s')
                );

                DB::table('gpff_insentive')
                ->where('insentive_id',$value->insentive_id)
                ->update($update_values);       
            }
        }
    }
}
