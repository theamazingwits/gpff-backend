<?php

namespace App\Http\Controllers;

use Closure;
use DB;
use \PDF;
use Mail;
use Carbon\Carbon;
use App\Imports\UsersImport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;

// require(base_path().'/app/Http/Middleware/Common.php');
// require(base_path().'/app/Http/Middleware/Common.php');

class VendorOrder
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        return $next($request);
    }

/////////////////////////////////////
//Vendor Order Management Api Calls//
////////////////////////////////////
    //Store Vendor Orders Details

    public function storeVendorOrderDetails($Orders){
        $name = DB::table('gpff_customer')
                ->where('customer_id', $Orders->vendor_id)
                ->First(['customer_name','customer_lan','customer_lat','region_id']);

    	$values = array(

			"vendor_id"				=> $Orders->vendor_id,
			"vendor_name"			=> $Orders->vendor_name,
			"area_manager_id"		=> $Orders->area_manager_id,
			"order_date"			=> $Orders->order_date,
			"or_gross_total"		=> $Orders->or_gross_total,
			"or_tot_price"			=> $Orders->or_tot_price,
            "or_blc_price"          => $Orders->or_tot_price,
			"acc_or_tot_price"		=> $Orders->acc_or_tot_price,
			"order_discount"		=> $Orders->order_discount,
			"spl_discount"			=> $Orders->spl_discount,
			"branch_id"				=> $Orders->branch_id,
			"region_id"				=> $Orders->region_id,
			"area_id"				=> $Orders->area_id,
			"stokist_id"			=> $Orders->stokist_id,
			"warehouse_id"			=> $Orders->warehouse_id,
			"order_rej_date"		=> $Orders->order_rej_date,
			"admin_cmt"				=> $Orders->admin_cmt,
			"invoice_no"			=> $Orders->invoice_no,
			"reject_reason"			=> $Orders->reject_reason,
			"payment_type"			=> $Orders->payment_type,
			"credit_lim"			=> $Orders->credit_lim,
			"credit_valid_to"		=> $Orders->credit_valid_to,
			"or_type"				=> $Orders->or_type,
			"payment_img"			=> $Orders->payment_img,
			"payment_name"			=> $Orders->payment_name,
            "tot_box"               => $Orders->tot_box,
			"payment_msg"			=> $Orders->payment_msg,
			// "vendor_order_list"		=> $Orders->vendor_order_list,
			'created_at'       		=> date('Y-m-d H:i:s'), 
            'updated_at'        	=> date('Y-m-d H:i:s')

    	);
        // print_r($values);
        // exit();
        // print_r("expression");


    	$vendor_order_id = DB::table('gpff_vendor_order')
                    	->insertGetId($values);


        $pdf_name  = "GPFF_VENDOR_OR".$vendor_order_id;
        $filename = $pdf_name.'.pdf';

        $values = array(

        	'invoice_no' => $filename,
        	'updated_at' => date('Y-m-d H:i:s')
        );

        DB::table('gpff_vendor_order')
        	->where('vendor_order_id', $vendor_order_id)
       		->update($values);

        $SP_FOC = 0;
        foreach($Orders->vendor_order_list as $vendor_order_lists){

       		$exit = DB::table('gpff_vendor_order_list')
       				->where('category_id', $vendor_order_lists['category_id'])
       				->where('product_id', $vendor_order_lists['product_id'])
       				->where('vendor_order_id', $vendor_order_id)
       				->First();
            // print_r($exit);


       		$SP_FOC = $SP_FOC + $vendor_order_lists['spl_FOC'];
            if($exit){
                // print_r("if");
                // exit();


                $qty = $exit->product_qty + $vendor_order_lists['product_qty'];
                $totprice = $exit->product_tot_price + $vendor_order_lists['product_tot_price'];

                $value = array(
                    'product_qty'        => $qty ,
                    'product_tot_price'  => $totprice ,
                    'updated_at'        => date('Y-m-d H:i:s')
                );

                DB::table('gpff_vendor_order_list')
                ->where('category_id', $vendor_order_lists['category_id'])
                ->where('product_id', $vendor_order_lists['product_id'])
                ->where('vendor_order_id', $vendor_order_id)
                ->update($value);

                 $vendor_list_value_batch = array(
                    'vendor_order_id'       => $vendor_order_id,
                    'vendor_order_list_id'  => $exit->vendor_order_list_id,
                    'category_id'           => $vendor_order_lists['category_id'],
                    'product_id'            => $vendor_order_lists['product_id'],
                    'batch_no'              => $vendor_order_lists['batch_no'],
                    'batch_emp_date'        => $vendor_order_lists['batch_emp_date'],    
                    'product_qty'           => $vendor_order_lists['product_qty'],
                    'product_netprice'      => $vendor_order_lists['product_netprice'],
                    'product_grossprice'    => $vendor_order_lists['product_grossprice'],
                    'product_discount'      => $vendor_order_lists['product_discount'],
                    'product_tot_price'     => $vendor_order_lists['product_tot_price'],
                    'product_type'          => $vendor_order_lists['product_type'],
                    'scheme'                => $vendor_order_lists['scheme'],
                    'comments'              => $vendor_order_lists['comments'],
                    'FOC'                   => $vendor_order_lists['FOC'],
                    'FOC_amt'               => $vendor_order_lists['FOC_amt'],
                    'spl_FOC'               => $vendor_order_lists['spl_FOC'],
                    'pro_amt_cal'           => $vendor_order_lists['pro_amt_cal'],
                    'created_at'            => date('Y-m-d H:i:s') ,
                    'updated_at'            => date('Y-m-d H:i:s')
                );
                DB::table('gpff_vendor_order_list_batchwise')
                ->insert($vendor_list_value_batch);
            }else{
                // print_r("esle");
                // exit();
                $list_value = array(
                    'vendor_order_id'       => $vendor_order_id,
                    'category_id'           => $vendor_order_lists['category_id'],
                    'category_name'         => $vendor_order_lists['category_name'],
                    'product_id'            => $vendor_order_lists['product_id'],
                    'product_genericname'   => $vendor_order_lists['product_genericname'],
                    'product_qty'         => $vendor_order_lists['product_qty'],
                    'product_tot_price'         => $vendor_order_lists['product_tot_price'],
                    'product_type'          => $vendor_order_lists['product_type'],
                    'created_at'          => date('Y-m-d H:i:s') ,
                    'updated_at'          => date('Y-m-d H:i:s')
                );

               
                $order_list_id = DB::table('gpff_vendor_order_list')
                                ->insertGetId($list_value);

                $list_value_batch = array(
                    'vendor_order_id'       => $vendor_order_id,
                    'vendor_order_list_id'  => $order_list_id,
                    'category_id'           => $vendor_order_lists['category_id'],
                    'product_id'            => $vendor_order_lists['product_id'],
                    'batch_no'              => $vendor_order_lists['batch_no'],
                    'batch_emp_date'        => $vendor_order_lists['batch_emp_date'],    
                    'product_qty'           => $vendor_order_lists['product_qty'],
                    'product_netprice'      => $vendor_order_lists['product_netprice'],
                    'product_grossprice'    => $vendor_order_lists['product_grossprice'],
                    'product_discount'      => $vendor_order_lists['product_discount'],
                    'product_tot_price'     => $vendor_order_lists['product_tot_price'],
                    'product_type'          => $vendor_order_lists['product_type'],
                    'scheme'                => $vendor_order_lists['scheme'],
                    'comments'              => $vendor_order_lists['comments'],
                    'FOC'                   => $vendor_order_lists['FOC'],
                    'FOC_amt'               => $vendor_order_lists['FOC_amt'],
                    'spl_FOC'               => $vendor_order_lists['spl_FOC'],
                    'pro_amt_cal'           => $vendor_order_lists['pro_amt_cal'],
                    'created_at'            => date('Y-m-d H:i:s') ,
                    'updated_at'            => date('Y-m-d H:i:s')
                );
                DB::table('gpff_vendor_order_list_batchwise')
                ->insert($list_value_batch);
            }
            $batch_details = DB::table('gpff_product_batch_stock')
                            ->where('product_id',$vendor_order_lists['product_id'])
                            ->where('batch_id',$vendor_order_lists['batch_no'])
                            ->where('warehouse_id',$Orders->warehouse_id)
                            ->select(DB::raw('SUM(product_sales_qty) as product_sales_qty'),DB::raw('SUM(product_tot_qty) as product_tot_qty'),DB::raw('SUM(product_blc_qty) as product_blc_qty'),DB::raw('SUM(product_sale_FOC_qty) as product_sale_FOC_qty'))
                            ->first();

            $product_sales_qty = $batch_details->product_sales_qty + $vendor_order_lists['product_qty'];
            $product_blc_qty = $batch_details->product_blc_qty - $vendor_order_lists['product_qty'];
            $product_sale_FOC_qty = $batch_details->product_sale_FOC_qty;

            if($vendor_order_lists['FOC'] != 0){
                $product_sale_FOC_qty = $batch_details->product_sale_FOC_qty + $vendor_order_lists['FOC'];
                $product_blc_qty = $product_blc_qty - $vendor_order_lists['FOC'];
            }

            if($product_blc_qty <= 0){
                $product_status = 2;
            }else{
                $product_status = 1;
            }

            $value = array(
                'product_blc_qty'       => $product_blc_qty ,
                'product_sales_qty'     => $product_sales_qty ,
                'product_sale_FOC_qty'  => $product_sale_FOC_qty,
                'product_status'        => $product_status,
                'updated_at'            => date('Y-m-d H:i:s')
            );

            DB::table('gpff_product_batch_stock')
                ->where('product_id',$vendor_order_lists['product_id'])
                ->where('batch_id',$vendor_order_lists['batch_no'])
                ->where('warehouse_id',$Orders->warehouse_id)
                ->update($value);

        }
        if($SP_FOC == 0 && $Orders->spl_discount == 0){

            $event_values = array(
                'vendor_order_id'    => $vendor_order_id, 
                'event_name'  => "The order has been Placed" ,
                'event_date'  => now() ,
                'vendor_order_status'  => 0 ,
                'created_at'  => date('Y-m-d H:i:s') , 
                'updated_at'  => date('Y-m-d H:i:s')
            );

            DB::table('gpff_vendor_order_events')
            ->insert($event_values);
        } else{
            $orvalue1 = array(
                    'vendor_order_status'       => 5,
                    'updated_at'        => date('Y-m-d H:i:s')
                );

             DB::table('gpff_vendor_order')
            ->where('vendor_order_id', $vendor_order_id)
            ->update($orvalue1);

            $event_values = array(
                    'vendor_order_id'    => $vendor_order_id, 
                    'event_name'  => "The order send to the admin" ,
                    'event_date'  => now() ,
                    'vendor_order_status'  => 5 ,
                    'created_at'  => date('Y-m-d H:i:s') , 
                    'updated_at'  => date('Y-m-d H:i:s')
                );
                
            DB::table('gpff_vendor_order_events')
            ->insert($event_values);
        }
        $vendor_order_details = DB::table('gpff_vendor_order')
        						->where('vendor_order_id',$vendor_order_id)
        						->First();

        $warehouse = DB::table('gpff_warehouse')
                    ->where('warehouse_id', $vendor_order_details->warehouse_id)
                    ->First();

        $area_manager = DB::table('gpff_users')
                        ->where('user_id', $vendor_order_details->area_manager_id)
                        ->First();

        $warehouse = DB::table('gpff_warehouse')
                    ->where('warehouse_id', $vendor_order_details->warehouse_id)
                    ->First();

        $vendor_details = DB::table('gpff_customer')
        				->where('customer_id', $vendor_order_details->vendor_id)
        				->First();

        $branch_details = DB::table('gpff_branch')
        				->where('branch_id', $vendor_order_details->branch_id)
        				->First();

        $number = $this->numbertostring($vendor_order_details->or_tot_price);

        view()->share('datas', $Orders->vendor_order_list);
        view()->share('order_details',$vendor_order_details);
		view()->share('cus_de', $vendor_details);
        view()->share('war', $warehouse);
        view()->share('area_manager_details', $area_manager);
		view()->share('com_details', $branch_details);
		view()->share('invoice',$pdf_name);
		view()->share('value', $number);
		view()->share('date', Carbon::now());

        $pdf = PDF::loadView('vendor_invoice')
        		->setPaper('a4', 'landscape');

        $pdf->save(public_path('Order_invoice/'.$pdf_name.'.pdf'));
        $file_name = $pdf_name.'.pdf';
        $names = $file_name;
        // print_r($names);
        // exit();
        $filePath = 'Order_invoice/'.$names; 
  
        Storage::disk('s3')->put($filePath, file_get_contents(public_path('Order_invoice')."/".$file_name));

        $cmn = new Common();

        $waremessage = "Customer ".$name->customer_name." has placed one Order to your WareHouse.";

            $warename = DB::table('gpff_users')
                    ->where('warehouse_id', $Orders->warehouse_id)
                    ->First();

            $page_id = 'UNKNOWN';

            $cmn->insertNotification($Orders->vendor_id,$name->customer_name,$warename->user_id,$waremessage,$page_id);
        return 1;
    }
    public function numbertostring($number)
    {   
    //     $ones = array(
    //         0 =>"ZERO",
    //         1 => "ONE",
    //         2 => "TWO",
    //         3 => "THREE",
    //         4 => "FOUR",
    //         5 => "FIVE",
    //         6 => "SIX",
    //         7 => "SEVEN",
    //         8 => "EIGHT",
    //         9 => "NINE",
    //         10 => "TEN",
    //         11 => "ELEVEN",
    //         12 => "TWELVE",
    //         13 => "THIRTEEN",
    //         14 => "FOURTEEN",
    //         15 => "FIFTEEN",
    //         16 => "SIXTEEN",
    //         17 => "SEVENTEEN",
    //         18 => "EIGHTEEN",
    //         19 => "NINETEEN",
    //         "014" => "FOURTEEN"
    //         );
    //         $tens = array( 
    //         0 => "ZERO",
    //         1 => "TEN",
    //         2 => "TWENTY",
    //         3 => "THIRTY", 
    //         4 => "FORTY", 
    //         5 => "FIFTY", 
    //         6 => "SIXTY", 
    //         7 => "SEVENTY", 
    //         8 => "EIGHTY", 
    //         9 => "NINETY" 
    //         ); 
    //         $hundreds = array( 
    //         "HUNDRED", 
    //         "THOUSAND", 
    //         "MILLION", 
    //         "BILLION", 
    //         "TRILLION", 
    //         "QUARDRILLION" 
    //         ); /*limit t quadrillion */
    //         $num = number_format($num,2,".",","); 
    //         $num_arr = explode(".",$num); 
    //         $wholenum = $num_arr[0]; 
    //         $decnum = $num_arr[1]; 
    //         $whole_arr = array_reverse(explode(",",$wholenum)); 
    //         krsort($whole_arr,1); 
    //         $rettxt = ""; 
    //         foreach($whole_arr as $key => $i){
                
    //         while(substr($i,0,1)=="0")
    //                 $i=substr($i,1,5);
    //         if($i < 20){ 
    //         /* echo "getting:".$i; */
    //         $rettxt .= $ones[$i]; 
    //         }elseif($i < 100){ 
    //         if(substr($i,0,1)!="0")  $rettxt .= $tens[substr($i,0,1)]; 
    //         if(substr($i,1,1)!="0") $rettxt .= " ".$ones[substr($i,1,1)]; 
    //         }else{ 
    //         if(substr($i,0,1)!="0") $rettxt .= $ones[substr($i,0,1)]." ".$hundreds[0]; 
    //         if(substr($i,1,1)!="0")$rettxt .= " ".$tens[substr($i,1,1)]; 
    //         if(substr($i,2,1)!="0")$rettxt .= " ".$ones[substr($i,2,1)]; 
    //         } 
    //         if($key > 0){ 
    //         $rettxt .= " ".$hundreds[$key]." "; 
    //         }
    //         } 
    //         if($decnum > 0){
    //         $rettxt .= " and ";
    //         if($decnum < 20){
    //         $rettxt .= $ones[$decnum];
    //         }elseif($decnum < 100){
    //         $rettxt .= $tens[substr($decnum,0,1)];
    //         $rettxt .= " ".$ones[substr($decnum,1,1)];
    //         }
    //         }
    //         return $rettxt;
    // }
        $decimal = round($number - ($no = floor($number)), 2) * 100;
        $decimal_part = $decimal;
        $hundred = null;
        $hundreds = null;
        $digits_length = strlen($no);
        $decimal_length = strlen($decimal);
        $i = 0;
        $str = array();
        $str2 = array();
        $words = array(0 => '', 1 => 'one', 2 => 'two',
            3 => 'three', 4 => 'four', 5 => 'five', 6 => 'six',
            7 => 'seven', 8 => 'eight', 9 => 'nine',
            10 => 'ten', 11 => 'eleven', 12 => 'twelve',
            13 => 'thirteen', 14 => 'fourteen', 15 => 'fifteen',
            16 => 'sixteen', 17 => 'seventeen', 18 => 'eighteen',
            19 => 'nineteen', 20 => 'twenty', 30 => 'thirty',
            40 => 'forty', 50 => 'fifty', 60 => 'sixty',
            70 => 'seventy', 80 => 'eighty', 90 => 'ninety');
        $digits = array('', 'hundred','thousand','lakh', 'crore');

        while( $i < $digits_length ) {
            $divider = ($i == 2) ? 10 : 100;
            $number = floor($no % $divider);
            $no = floor($no / $divider);
            $i += $divider == 10 ? 1 : 2;
            if ($number) {
                $plural = (($counter = count($str)) && $number > 9) ? 's' : null;
                $hundred = ($counter == 1 && $str[0]) ? ' and ' : null;
                $str [] = ($number < 21) ? $words[$number].' '. $digits[$counter]. $plural.' '.$hundred:$words[floor($number / 10) * 10].' '.$words[$number % 10]. ' '.$digits[$counter].$plural.' '.$hundred;
            } else $str[] = null;
        }

        $d = 0;
        while( $d < $decimal_length ) {
            $divider = ($d == 2) ? 10 : 100;
            $decimal_number = floor($decimal % $divider);
            $decimal = floor($decimal / $divider);
            $d += $divider == 10 ? 1 : 2;
            if ($decimal_number) {
                $plurals = (($counter = count($str2)) && $decimal_number > 9) ? 's' : null;
                $hundreds = ($counter == 1 && $str2[0]) ? ' and ' : null;
                @$str2 [] = ($decimal_number < 21) ? $words[$decimal_number].' '. $digits[$decimal_number]. $plural.' '.$hundred:$words[floor($decimal_number / 10) * 10].' '.$words[$decimal_number % 10]. ' '.$digits[$counter].$plural.' '.$hundred;
            } else $str2[] = null;
        }

        $Rupees = implode('', array_reverse($str));
        $paise = implode('', array_reverse($str2));
        $paise = ($decimal_part > 0) ? $paise . ' Kyat' : '';
        return ($Rupees ? $Rupees . 'Kyat ' : '') . $paise;
    }

    public function getIdBasedVendorList($Orders){

    	$results = DB::table('gpff_customer')
    				->where('customer_cr_id', $Orders->user_id)
    				->get();

    	return $results;

    }

    public function getAreaManagerBasedVendorOrder($Orders){

    	$results = [];

		if($Orders->type == 0){
			$results['data'] = DB::table('gpff_vendor_order as gvo')
						->join('gpff_warehouse as gwh','gvo.warehouse_id','gwh.warehouse_id')
						->where('area_manager_id', $Orders->user_id)
						->whereIn('vendor_order_status', [0,5])
                        ->whereBetween(DB::RAW('date(gvo.created_at)'), [date($Orders->start_date), date($Orders->end_date)])
						->orderBy('updated_at','DESC')
						->get(['gvo.vendor_id','gvo.vendor_order_id','gvo.vendor_name','gvo.area_manager_id','gvo.order_date','gvo.or_gross_total','gvo.or_tot_price','gvo.acc_or_tot_price','gvo.order_discount','gvo.spl_discount','gvo.branch_id','gvo.region_id','gvo.area_id','gvo.stokist_id','gvo.warehouse_id','gvo.order_rej_date','gvo.vendor_order_status','gvo.admin_cmt','gvo.invoice_no','gvo.reject_reason','gvo.payment_type','gvo.credit_lim','gvo.credit_valid_to','gvo.or_type','gvo.payment_img','gvo.payment_name','gvo.payment_msg','gvo.created_at','gvo.updated_at','gwh.warehouse_name']);
		}else{
			$results['data'] = DB::table('gpff_vendor_order as gvo')
						->join('gpff_warehouse as gwh','gvo.warehouse_id','gwh.warehouse_id')
						->where('area_manager_id', $Orders->user_id)
						->where('vendor_order_status',$Orders->type)
                        ->whereBetween(DB::RAW('date(gvo.created_at)'), [date($Orders->start_date), date($Orders->end_date)])
						->orderBy('updated_at','DESC')
						->get(['gvo.vendor_id','gvo.vendor_order_id','gvo.vendor_name','gvo.area_manager_id','gvo.order_date','gvo.or_gross_total','gvo.or_tot_price','gvo.acc_or_tot_price','gvo.order_discount','gvo.spl_discount','gvo.branch_id','gvo.region_id','gvo.area_id','gvo.stokist_id','gvo.warehouse_id','gvo.order_rej_date','gvo.vendor_order_status','gvo.admin_cmt','gvo.invoice_no','gvo.reject_reason','gvo.payment_type','gvo.credit_lim','gvo.credit_valid_to','gvo.or_type','gvo.payment_img','gvo.payment_name','gvo.payment_msg','gvo.created_at','gvo.updated_at','gwh.warehouse_name']);
		}
		return $results;
    }

    public function getAreaManagerBasedOrder($Orders){
    	$results = DB::table('gpff_vendor_order')
    				->where('area_manager_id', $Orders->user_id)
    				->get();

    	return $results;
    }

    public function getOrderIdBasedList($Orders){

    	$data = DB::table('gpff_vendor_order')
    			->where('vendor_order_id', $Orders->vendor_order_id)
    			->First(['vendor_id','branch_id','warehouse_id']);
        $warehouse =  DB::table('gpff_branch as gb')
                ->join('gpff_warehouse as gw', 'gw.branch_id', 'gb.branch_id')
                ->where('gw.warehouse_id', $data->warehouse_id)
                ->where('gb.branch_id', $data->branch_id)
                ->get(['gw.warehouse_name','gw.warehouse_address','gw.warehouse_contact','gb.branch_name']);

    	$vendors = DB::table('gpff_customer')
    			->where('customer_id', $data->vendor_id)
    			->where('vendor', $Orders->vendor)
    			->get();

    	$product = DB::table('gpff_vendor_order_list_batchwise as gpvolbw')
    					->join('gpff_vendor_order_list as gpvol', 'gpvolbw.vendor_order_list_id', 'gpvol.vendor_order_list_id')
    					->where('gpvolbw.vendor_order_id', $Orders->vendor_order_id)
    					->orderBy('gpvolbw.created_at','ASC')
    					->get(['gpvolbw.vendor_order_id','gpvolbw.vendor_order_list_id','gpvolbw.category_id','gpvol.category_name','gpvolbw.product_id','gpvol.product_genericname','gpvolbw.batch_no','gpvolbw.batch_emp_date','gpvolbw.product_qty','gpvolbw.product_netprice','gpvolbw.product_grossprice','gpvolbw.or_gross_total','gpvolbw.product_discount','gpvolbw.product_tot_price','gpvolbw.product_type','gpvolbw.scheme','gpvolbw.comments','gpvolbw.FOC','gpvolbw.FOC_amt','gpvolbw.spl_FOC','gpvolbw.pro_amt_cal','gpvolbw.created_at','gpvolbw.updated_at']);

    	$status = DB::table('gpff_vendor_order as gpvor')
                        ->join('gpff_users as gpusr', 'gpusr.user_id', 'gpvor.area_manager_id')
    					->where('vendor_order_id', $Orders->vendor_order_id)
    					->get(['gpvor.vendor_order_status','gpvor.order_date','gpvor.order_rej_date','gpvor.reject_reason','gpvor.acc_or_tot_price','gpvor.or_tot_price','gpvor.or_gross_total','gpvor.order_discount','gpvor.payment_type','gpvor.invoice_no','gpvor.spl_discount', 'gpusr.firstname','gpvor.warehouse_id']);

    	$results = array(
    		'vendor' => $vendors,
			'product' => $product,
			'status' => $status,
            'warehouse' => $warehouse
    	);

    	return $results;

    }

    public function getWarehouseIdBasedOrderDetails($Orders){
    	
    	$results = DB::table('gpff_vendor_order as gvo')
    				->join('gpff_warehouse as gpwh', 'gvo.warehouse_id', 'gpwh.warehouse_id')
    				->whereIn('gvo.warehouse_id', $Orders->warehouse_id)
    				// ->get();
    				->get(["gvo.vendor_order_id as order_id","gvo.vendor_id","gvo.vendor_name","gvo.area_manager_id","gvo.order_date","gvo.or_gross_total","gvo.or_tot_price","gvo.acc_or_tot_price","gvo.order_discount","gvo.spl_discount","gvo.branch_id","gvo.region_id","gvo.area_id","gvo.stokist_id","gvo.warehouse_id","gvo.order_rej_date","gvo.vendor_order_status","gvo.admin_cmt","gvo.invoice_no","gvo.reject_reason","gvo.payment_type","gvo.credit_lim","gvo.credit_valid_to","gvo.or_type","gvo.payment_name","gvo.payment_img","gvo.payment_msg","gvo.created_at","gvo.updated_at", 'gpwh.warehouse_name']);

    	return $results;
    }

    public function getWarehouseIdBasedVendorOrderDetails($Orders){
    	
    	$results = DB::table('gpff_vendor_order as gvo')
    				->join('gpff_warehouse as gpwh', 'gvo.warehouse_id', 'gpwh.warehouse_id')
    				->where()
    				->whereIn('gvo.warehouse_id', $Orders->warehouse_id)
    				// ->get();
    				->get(["gvo.vendor_order_id","gvo.vendor_id","gvo.vendor_name","gvo.area_manager_id","gvo.order_date","gvo.or_gross_total","gvo.or_tot_price","gvo.acc_or_tot_price","gvo.order_discount","gvo.spl_discount","gvo.branch_id","gvo.region_id","gvo.area_id","gvo.stokist_id","gvo.warehouse_id","gvo.order_rej_date","gvo.vendor_order_status","gvo.admin_cmt","gvo.invoice_no","gvo.reject_reason","gvo.payment_type","gvo.credit_lim","gvo.credit_valid_to","gvo.or_type","gvo.payment_name","gvo.payment_img","gvo.payment_msg","gvo.created_at","gvo.updated_at", 'gpwh.warehouse_name']);

    	return $results;
    }

    public function reportForAreaManager($Orders){
    	$orders = DB::table('gpff_vendor_order as gvenorder')
        ->join('gpff_vendor_order_list as gvenorderlist','gvenorderlist.vendor_order_id','gvenorder.vendor_order_id')
        ->join('gpff_branch as gbranch','gvenorder.branch_id','gbranch.branch_id')
        ->join('gpff_region as gregion','gvenorder.region_id','gregion.region_id')
        ->join('gpff_area as garea','gvenorder.area_id','garea.area_id')
        ->whereBetween(DB::RAW('date(gvenorder.created_at)'), [date($Orders->start_date), date($Orders->end_date)])
        ->where('gvenorder.area_manager_id',$Orders->area_manager_id)
        ->where('gvenorder.vendor_order_status', '3')
        ->orderBy('gvenorder.created_at', 'DESC');
        // echo "string";
        // print_r($orders);
     	// exit;
        
        if($Orders->vendor_id != ''){
            $orders = $orders->whereIn('gvenorder.vendor_id',$Orders->vendor_id);
        }
        // if($Orders->year != ''){
        //     $orders = $orders->whereYear("gvenorder.created_at",$Orders->year);
        // }
        // if($Orders->months != ''){
        //     $orders = $orders->whereIn(DB::raw('MONTH(gvenorder.created_at)'), $Orders->months);
        // }

        //$res = ['1','2','3','4','5','6','7','8','9','10','11','12'];
        $order_det = $orders->get(['gvenorder.vendor_order_id','gvenorder.order_date','gvenorder.vendor_name','gvenorderlist.product_genericname','gvenorderlist.product_qty','gvenorderlist.product_type','gbranch.branch_name','gregion.region_name','garea.area_name','gvenorderlist.product_tot_price']);

        $result = array_merge(
            ['order_details'=>$order_det->toArray()]
        );

        return $result;
    }
    public function reportForWarehouse($Orders){

    	// $today = date('Y-m-d');
    	// $today = Carbon::now()->format('Y-m-d').'%';

    	$orders = DB::table('gpff_vendor_order as gvenorder')
                ->join('gpff_vendor_order_list as gvenorderlist','gvenorderlist.vendor_order_id','gvenorder.vendor_order_id')
                ->join('gpff_branch as gbranch','gvenorder.branch_id','gbranch.branch_id')
                ->join('gpff_region as gregion','gvenorder.region_id','gregion.region_id')
                ->join('gpff_area as garea','gvenorder.area_id','garea.area_id')
                ->join('gpff_warehouse as gpwh', 'gvenorder.warehouse_id', 'gpwh.warehouse_id')
                ->whereBetween(DB::RAW('date(gvenorder.created_at)'), [date($Orders->start_date), date($Orders->end_date)])
                ->where('gvenorder.area_manager_id',$Orders->area_manager_id)
                ->where('gvenorder.vendor_order_status', '3')
                ->orderBy('gvenorder.created_at', 'DESC');
        // echo "string";
        // print_r($orders);
     	// exit;
     	
        if($Orders->warehouse_id != ''){
            $orders = $orders->whereIn('gvenorder.warehouse_id',$Orders->warehouse_id);
        }
        // if($Orders->area_manager_id != ''){
        //     $orders = $orders->whereYear("gvenorder.area_manager_id",$Orders->area_manager_id);
        // }
        // if($Orders->months != ''){
        //     $orders = $orders->whereIn(DB::raw('MONTH(gvenorder.created_at)'), $Orders->months);
        // }

        //$res = ['1','2','3','4','5','6','7','8','9','10','11','12'];
        $order_det = $orders->get(['gvenorder.vendor_order_id','gvenorder.order_date','gvenorder.vendor_name','gvenorderlist.product_genericname','gvenorderlist.product_qty','gvenorderlist.product_type','gbranch.branch_name','gregion.region_name','garea.area_name','gvenorderlist.product_tot_price', 'gpwh.warehouse_name']);

        $result = array_merge(
            ['order_details'=>$order_det->toArray()] 
        );

        return $result;
    }
        // ORDER ACCEPT
    public function VendorOrderAccept($Orders) 
    {
        $products = $Orders->products;
        // print_r("expression1");
        // print_r($products);
        $vendor_order_id = $Orders->vendor_order_id;

        if($Orders->vendor_order_status == 1){
            // ORDER STATUS UPDATED IN GPFF_ORDER TABLE
            $value1 = array(
                    'vendor_order_status'       => $Orders->vendor_order_status,
                    'acc_or_tot_price'       => $Orders->acc_or_tot_price,
                    // 'tot_box'                => $Orders->tot_box,
                    'updated_at' => date('Y-m-d H:i:s')
                );
            // print_r($value1);
            // exit();

             DB::table('gpff_vendor_order')
            ->where('vendor_order_id', $vendor_order_id)
            ->update($value1);

            // print_r($Orders->products);
            // exit()

            // ORDER DETAILS ACCEPTED IN GPFF_ORDER_LIST TABLE
            foreach ($products as $product) {
                $value = array(
                    'accepted_qty'       => $product['accepted_qty'] ,
                    'acc_or_tot_price'     => $product['acc_or_tot_price'] ,
                    'updated_at'            => date('Y-m-d H:i:s')
                );

             DB::table('gpff_vendor_order_list')
            ->where('product_id', $product['product_id'])
            ->where('vendor_order_id', $Orders->vendor_order_id)
            ->update($value);
                
            }
            return 1;
        }
    }

    public function getOrderIdDetails($Orders){
    	$results = DB::table('gpff_vendor_order')
    				->where('vendor_order_id', $Orders->vendor_order_id)
    				->get();
    	return $results;
    }

    // public function getWhIdBaseVenOrdDetails($Orders){
    // 	$results = DB::table('gpff_vendor_order')
    // 				->where('warehouse_id', $Orders->warehouse_id)
    // 				->orderBy('created_at', 'DESC')
    // 				->get();
    // 	return $results;
    // }

    public function getWhIdBaseVenOrdDetails($Orders){
        $results = DB::table('gpff_vendor_order')
                    ->where('warehouse_id', $Orders->warehouse_id)
                    ->whereBetween(DB::RAW('date(created_at)'), [date($Orders->start_date), date($Orders->end_date)])
                    ->whereNotIn('vendor_order_status', [0,5])
                    ->orderBy('created_at','DESC')
                    ->get();
        return $results;
    }

    public function getVendorOrderBasedEventsList($Orders){
    	$results = DB::table('gpff_vendor_order_events')
    				->where('vendor_order_id', $Orders->vendor_order_id)
    				->get();
    	return $results;
    }

    public function updateVendorOrderStatus($Orders){
     //Qty Reduce In My Stock
        if($Orders->vendor_order_status == 2){

            $vendor_ord_de  =  DB::table('gpff_vendor_order_list_batchwise')
                        ->where('vendor_order_id', $Orders->vendor_order_id)
                        ->get(['product_id','product_qty','batch_no', 'FOC', 'spl_FOC']);

            $ware = DB::table('gpff_vendor_order')
                    ->where('vendor_order_id', $Orders->vendor_order_id)
                    ->First();
            
            

            $update_values = array(
                'vendor_order_status'  => $Orders->vendor_order_status ,
                'updated_at'    => date('Y-m-d H:i:s')
            );

            $event_values = array(
                'vendor_order_id'    => $Orders->vendor_order_id, 
                'event_name'  => "The order has been Packaged" ,
                'event_date'  => now() ,
                'vendor_order_status'  => $Orders->vendor_order_status ,
                'created_at'  => date('Y-m-d H:i:s'), 
                'updated_at'  => date('Y-m-d H:i:s')
            );
        } else if ($Orders->vendor_order_status == 1) {
            $update_values = array(
                'vendor_order_status'  => $Orders->vendor_order_status ,
                'updated_at'    => date('Y-m-d H:i:s')
            );

            $event_values = array(
                'vendor_order_id'    => $Orders->vendor_order_id, 
                'event_name'  => "The order is in-progress",
                'event_date'  => now() ,
                'vendor_order_status'  => $Orders->vendor_order_status ,
                'created_at'  => date('Y-m-d H:i:s') , 
                'updated_at'  => date('Y-m-d H:i:s')
            );
        } else if ($Orders->vendor_order_status == 3) {
            $update_values = array(
                'vendor_order_status'  => $Orders->vendor_order_status ,
                'updated_at'    => date('Y-m-d H:i:s')
            );

            $event_values = array(
                'vendor_order_id'    => $Orders->vendor_order_id, 
                'event_name'  => "your order has been Delivered" ,
                'event_date'  => now() ,
                'vendor_order_status'  => $Orders->vendor_order_status ,
                'created_at'  => date('Y-m-d H:i:s') , 
                'updated_at'  => date('Y-m-d H:i:s')
            );
        }else if ($Orders->vendor_order_status == 4) {
            $update_values = array(
                'reject_reason' => $Orders->reject_reason,
                'vendor_order_status'  => $Orders->vendor_order_status ,
                'order_rej_date'  => date('Y-m-d H:i:s') ,
                'updated_at'    => date('Y-m-d H:i:s')
            );

            $event_values = array(
                'vendor_order_id'    => $Orders->vendor_order_id, 
                'event_name'  => "The order has been Rejected" ,
                'event_date'  => now() ,
                'vendor_order_status'  => $Orders->vendor_order_status ,
                'created_at'  => date('Y-m-d H:i:s') , 
                'updated_at'  => date('Y-m-d H:i:s')
            );
        }else if ($Orders->vendor_order_status == 6) {
            $update_values = array(
                'vendor_order_status'  => $Orders->vendor_order_status ,
                'updated_at'    => date('Y-m-d H:i:s')
            );

            $update_values1 = array(
                'status'  => 1 ,
                'updated_at'    => date('Y-m-d H:i:s')
            );

            DB::table('gpff_cus_overlimit_order_req')
                ->where('order_id', $Orders->vendor_order_id)
                ->update($update_values1);


            $cuslim    =   DB::table('gpff_customer')
                        ->where('customer_id', $Orders->vendor_id)
                        ->First('credit_limit');

            $val = $cuslim->credit_limit - $Orders->or_tot_price;

            if($val < 0){
                $update_values2 = array(
                    'credit_limit'  => 0 ,
                    'updated_at'    => date('Y-m-d H:i:s')
                );
            }else{
                $update_values2 = array(
                    'credit_limit'  => $val ,
                    'updated_at'    => date('Y-m-d H:i:s')
                );
            }
            DB::table('gpff_customer')
                ->where('customer_id', $Orders->vendor_id)
                ->update($update_values2);

            $event_values = array(
                'vendor_order_id'    => $Orders->vendor_order_id, 
                'event_name'  => "The order has been Accepted by Admin" ,
                'event_date'  => now() ,
                'vendor_order_status'  => $Orders->vendor_order_status ,
                'created_at'  => date('Y-m-d H:i:s') , 
                'updated_at'  => date('Y-m-d H:i:s')
            );

        }else if ($Orders->vendor_order_status == 7) {

            $update_values = array(
                'reject_reason' => $Orders->reject_reason,
                'vendor_order_status'  => $Orders->vendor_order_status ,
                'updated_at'    => date('Y-m-d H:i:s')
            );

            $update_values1 = array(
                'remark' => $Orders->reject_reason,
                'status'  => 2 ,
                'updated_at'    => date('Y-m-d H:i:s')
            );

            DB::table('gpff_cus_overlimit_order_req')
                ->where('order_id', $Orders->vendor_order_id)
                ->update($update_values1);

            $event_values = array(
                'vendor_order_id'    => $Orders->vendor_order_id, 
                'event_name'  => "The order has been Rejected by Admin" ,
                'event_date'  => now() ,
                'vendor_order_status'  => $Orders->vendor_order_status ,
                'created_at'  => date('Y-m-d H:i:s') , 
                'updated_at'  => date('Y-m-d H:i:s')
            );
        }

        DB::table('gpff_vendor_order_events')
        ->insert($event_values);

        return  DB::table('gpff_vendor_order')
                ->where('vendor_order_id', $Orders->vendor_order_id)
                ->update($update_values);
    }

    public function getPlacedVendorOrder($Orders) 
    {
        $data =[];

        if($Orders->type == 0){
            $data['totaldata']= DB::table('gpff_vendor_order')
                        ->whereBetween(DB::RAW('date(created_at)'), [date($Orders->start_date), date($Orders->end_date)])
                        ->where('warehouse_id', $Orders->warehouse_id)
                        ->whereIn('vendor_order_status', [0,5])
                        ->orderBy('updated_at','DESC')
                        ->get();    
        }else{
            $data['totaldata']= DB::table('gpff_vendor_order')
                        ->whereBetween(DB::RAW('date(created_at)'), [date($Orders->start_date), date($Orders->end_date)])
                        ->where('warehouse_id', $Orders->warehouse_id)
                        ->where('vendor_order_status', $Orders->type)
                        ->orderBy('updated_at','DESC')
                        ->get();    
        }        
         return $data;   
    }

    public function getApprovalVendorOrderData()
    {
        return  DB::table('gpff_vendor_order')
                ->orderBy('created_at','DESC')
                ->where('vendor_order_status',5)
                ->get();
    }

    public function getVendorOrderByAdmin($Orders){
    	$results = DB::table('gpff_vendor_order')
                ->whereBetween(DB::RAW('date(created_at)'), [date($Orders->start_date), date($Orders->end_date)])
    			 ->orderBy('created_at','DESC')
    			 ->whereIn('branch_id', [$Orders->branch_id])
    			 ->get();
    	return $results;
    }

    public function vendorOrderFOCDiscountAccept($Orders) 
    {   
        //SPL FOC Update 
        foreach ($Orders->products_list as $product) {

            // $batch_details = DB::table('gpff_product_batch_stock')
            //                 ->where('product_id',$product['product_id'])
            //                 ->where('batch_id',$product['batch_no'])
            //                 ->where('warehouse_id',$Orders->warehouse_id)
            //                 ->select(DB::raw('SUM(product_sale_spl_FOC_qty) as product_sale_spl_FOC_qty'),DB::raw('SUM(product_blc_qty) as product_blc_qty'))
            //                 ->first();
            // print_r($batch_details);
            // exit;
            $value = array(
                'spl_FOC'       => $product['spl_FOC'] ,
                'updated_at'            => date('Y-m-d H:i:s')
            );

            DB::table('gpff_vendor_order_list_batchwise')
            ->where('product_id', $product['product_id'])
            ->where('batch_no', $product['batch_no'])
            ->where('vendor_order_id', $Orders->vendor_order_id)
            ->update($value);

            
            if($product['spl_FOC'] > 0){
                $batch_details = DB::table('gpff_product_batch_stock')
                            ->where('product_id',$product['product_id'])
                            ->where('batch_id',$product['batch_no'])
                            ->where('warehouse_id',$Orders->warehouse_id)
                            ->select(DB::raw('SUM(product_sale_spl_FOC_qty) as product_sale_spl_FOC_qty'),DB::raw('SUM(product_blc_qty) as product_blc_qty'))
                            ->first();
                $product_blc_qty = $batch_details->product_blc_qty - $product['spl_FOC'];
                $product_sale_spl_FOC_qty = $batch_details->product_sale_spl_FOC_qty + $product['spl_FOC'];
                $value = array(
                    'product_blc_qty'       => $product_blc_qty ,
                    'product_sale_spl_FOC_qty'     => $product_sale_spl_FOC_qty,
                    'updated_at'            => date('Y-m-d H:i:s')
                );
                DB::table('gpff_product_batch_stock')
                    ->where('product_id',$product['product_id'])
                    ->where('batch_id',$product['batch_no'])
                    ->where('warehouse_id',$Orders->warehouse_id)
                    ->update($value);    
            }
            


        }

        $data = DB::table('gpff_vendor_order')
                ->where('vendor_order_id', $Orders->vendor_order_id)
                ->First();

        $value1 = array(
                    'spl_discount'       => $Orders->spl_discount ,
                    'admin_cmt'         => $Orders->admin_cmt ,
                    'vendor_order_status'      => 6 ,
                    'updated_at'         => date('Y-m-d H:i:s')
                );

        if($Orders->spl_discount == $data->spl_discount){
            $value1 = array(
                    'admin_cmt'         => $Orders->admin_cmt ,
                    'vendor_order_status'      => 6 ,
                    'updated_at'        => date('Y-m-d H:i:s')
            );
        }elseif($Orders->spl_discount > $data->spl_discount){
            $dis= $Orders->spl_discount - $data->spl_discount;
            $value1 = array(
                    'or_tot_price'       => $data->or_tot_price - $dis,
                    'spl_discount'       => $Orders->spl_discount ,
                    'admin_cmt'         => $Orders->admin_cmt ,
                    'vendor_order_status'  => 6 ,
                    'updated_at'         => date('Y-m-d H:i:s')
            );
        }else{
            $dis= $data->spl_discount - $Orders->spl_discount;
            $value1 = array(
                    'or_tot_price'       => $data->or_tot_price + $dis,
                    'spl_discount'       => $Orders->spl_discount ,
                    'admin_cmt'         => $Orders->admin_cmt ,
                    'vendor_order_status'  => 6 ,
                    'updated_at'         => date('Y-m-d H:i:s')
            );
        }

        //SPL_DISC Update
        DB::table('gpff_vendor_order')
        ->where('vendor_order_id', $Orders->vendor_order_id)
        ->update($value1);

        $event_values = array(
                'vendor_order_id'    => $Orders->vendor_order_id, 
                'event_name'  => "The order has been Accepted by Admin" ,
                'event_date'  => now() ,
                'vendor_order_status'  => 6 ,
                'created_at'  => date('Y-m-d H:i:s') , 
                'updated_at'  => date('Y-m-d H:i:s')
            );

        DB::table('gpff_vendor_order_events')
        ->insert($event_values);


        //Order Invoice
        $order_details = DB::table('gpff_vendor_order')
                        ->where('vendor_order_id',$Orders->vendor_order_id)
                        ->First();

        $order_list = DB::table('gpff_vendor_order_list as gvol')
                        ->leftjoin('gpff_vendor_order_list_batchwise as gpvorl','gvol.vendor_order_list_id','gpvorl.vendor_order_list_id')
                        ->where('gvol.vendor_order_id',$Orders->vendor_order_id)
                        ->get();

        $warehouse = DB::table('gpff_warehouse')
                    ->where('warehouse_id', $order_details->warehouse_id)
                    ->First();

        $area_manager = DB::table('gpff_users')
                        ->where('user_id', $order_details->area_manager_id)
                        ->First();        

        $cus_de = DB::table('gpff_customer')
                        ->where('customer_id',$order_details->vendor_id)
                        ->First();

        $com_details = DB::table('gpff_branch')
                        ->where('branch_id',$order_details->branch_id)
                        ->First();

        $number = $this->numbertostring($order_details->or_tot_price);

        $pdf_name = "GPFF_VENDOR_OR".$Orders->vendor_order_id;
        //Invoice
        view()->share('datas',$order_list);
        view()->share('order_details',$order_details);
        view()->share('cus_de',$cus_de);
        view()->share('war', $warehouse);
        view()->share('area_manager_details', $area_manager);
        view()->share('com_details',$com_details);
        view()->share('invoice',$pdf_name);
        view()->share('value',$number);
        view()->share('date',Carbon::now());

        $pdf = \PDF::loadView('vendor_invoice_2')
        ->setPaper('a4', 'landscape');
        $pdf->save(public_path('Order_invoice/'.$pdf_name.'.pdf'));
        $file_name = $pdf_name.'.pdf';
        $name = $file_name;
        // print_r(($name));
        
        $filePath = 'Order_invoice/'.$name; 
        // print_r($filePath);
  
        Storage::disk('s3')->put($filePath, file_get_contents(public_path('Order_invoice')."/".$file_name));

        return 1;
    }
    
    public function getAreaIdBasedPriceType($Orders){
        $value = DB::table('gpff_area')
                ->where('area_id', $Orders->area_id)
                ->First('region_id');
   
        $user_detail = DB::table('gpff_users')
                    ->where('region_id', $value->region_id)
                    ->where('role', 4)
                    ->get();
        // print_r($user_detail);
        // print_r("expression");

        $results = 0;
        $vars = [];

        foreach($user_detail as $user){
            $area_ids = $user->area_id;
            $area_explode = explode(',', $area_ids);

            foreach ($area_explode as $area_explodes){
                if($Orders->area_id == $area_explodes){
                    $results = $results + 1;
                    array_push($vars, $results);
                }
            }
            if(isset($vars)){
                $results = DB::table('gpff_users')
                    ->where('user_id', $user->user_id)
                    ->First(['price_type','user_id']);
                return $results;
            }
        }
    }

    public function vendorReportForProductIdBased($Orders){
        $orderId = [];
          $oreder_details = DB::table('gpff_vendor_order as gvenorder')
                            ->whereBetween(DB::RAW('date(gvenorder.created_at)'), [date($Orders->start_date), date($Orders->end_date)])
                            ->orderBy('gvenorder.updated_at', 'DESC')
                            ->get();
          foreach ($oreder_details as $order) {
              $orderId[] = $order->vendor_order_id;
          }
        $results = DB::table('gpff_vendor_order_list as gvenorderlist')
             ->join('gpff_vendor_order as gvenorder','gvenorder.vendor_order_id', 'gvenorderlist.vendor_order_id')
             ->join('gpff_users as gpusr', 'gpusr.user_id', 'gvenorder.area_manager_id')
            ->whereIn('gvenorder.vendor_order_id',$orderId)
             ->select('gvenorderlist.product_id','gvenorder.vendor_id','gvenorderlist.product_genericname','gvenorder.vendor_name','gpusr.firstname')             
             ->selectRaw(('SUM(gvenorderlist.product_qty) as product_quantity'))
             ->selectRaw(('SUM(gvenorderlist.product_tot_price) as product_tot_price'))
             ->groupBy('gvenorderlist.product_id','gvenorder.vendor_id','gpusr.firstname','gvenorderlist.product_genericname','gvenorder.vendor_name','gpusr.firstname');

        if($Orders->area_manager_id != ''){
             $orders = $results->where('gvenorder.area_manager_id',$Orders->area_manager_id);
        }
        if($Orders->product_id != ''){
            $orders = $results->whereIn('gvenorderlist.product_id', $Orders->product_id);
        }
        if($Orders->vendor_id != ''){
            $orders = $results->whereIn('gvenorder.vendor_id', $Orders->vendor_id);
        }


        $order_det = $results->get();

        $result = array_merge(
            ['order_details'=>$order_det->toArray()] 
        );

        return $result;
    }

    public function vendorReportForRegionManageBased($Orders){
        $results = DB::table('gpff_vendor_order as gvenorder')
             ->join('gpff_vendor_order_list as gvenorderlist', 'gvenorder.vendor_order_id', 'gvenorderlist.vendor_order_id')
             ->join('gpff_users as gpusr', 'gpusr.user_id', 'gvenorder.area_manager_id')
             ->join('gpff_area as gp_area','gvenorder.area_id',  'gp_area.area_id')
             ->join('gpff_warehouse as gpwar', 'gpwar.warehouse_id', 'gvenorder.warehouse_id')
             ->whereBetween(DB::RAW('date(gvenorder.created_at)'), [date($Orders->start_date), date($Orders->end_date)])
             ->select('gpusr.firstname','gvenorderlist.product_genericname','gp_area.area_name','gpwar.warehouse_name','gvenorder.vendor_name')
             ->selectRaw(('SUM(gvenorderlist.product_qty) as product_quantity'))
             ->selectRaw(('SUM(gvenorderlist.product_tot_price) as product_tot_price'))
             ->groupBy('gpusr.firstname','gvenorderlist.product_genericname','gp_area.area_name','gpwar.warehouse_name','gvenorder.vendor_name');
        if($Orders->region_id != ''){
             $orders = $results->whereIn('gvenorder.region_id', $Orders->region_id);
        }
        if($Orders->branch_id != ''){
             $orders = $results->whereIn('gvenorder.branch_id', $Orders->branch_id);
        }
        if($Orders->area_manager_id != ''){
             $orders = $results->whereIn('gvenorder.area_manager_id',$Orders->area_manager_id);
        }
        
        if($Orders->product_id != ''){
            $orders = $results->whereIn('gvenorderlist.product_id', $Orders->product_id);
        }

        if($Orders->warehouse_id != ''){
            $orders = $results->whereIn('gvenorder.warehouse_id', $Orders->warehouse_id);
        }

        if($Orders->vendor_id != ''){
            $orders = $results->whereIn('gvenorder.vendor_id', $Orders->vendor_id);
        }

        $order_det = $results->get();

        $result = array_merge(
            ['order_details'=>$order_det->toArray()]
        );
        return $result;
    }

    public function getAllFieldOfficerAndStockistInOrder($Orders){

            $results = DB::table('gpff_order as gpor')
                    ->join('gpff_users as gpusr', 'gpor.order_created_by', 'gpusr.user_id')
                    ->where('gpor.or_type', $Orders->role)
                    ->where('gpor.region_id', $Orders->region_id)
                    ->select('gpusr.user_id',DB::raw('CONCAT(gpusr.firstname,gpusr.lastname) as fullname' ))
                    ->distinct()
                    ->get();

            return $results;
    }

    public function fetchRegionBasedVendorList($Orders){

        return  DB::table('gpff_customer')
                ->where('vendor', 1)
                ->whereIn('region_id', $Orders->region_id)
                ->get();
    }

    public function fetchAreaManageBasedVendorOrderDetails($Orders){

        // $details = DB::table('gpff_vendor_order as gpvor')
        //     ->join('gpff_vendor_order_list as gpvorl', 'gpvor.vendor_order_id', 'gpvorl.vendor_order_id')
        //     ->join('gpff_vendor_order_list_batchwise as gpvorlb', 'gpvorl.vendor_order_list_id', 'gpvorlb.vendor_order_list_id')
        //     ->join('gpff_warehouse as gpwh', 'gpvor.warehouse_id', 'gpwh.warehouse_id')
        //     ->whereBetween(DB::RAW('date(gpvor.created_at)'), [date($Orders->start_date), date($Orders->end_date)])
        //     ->get(["gpvor.vendor_order_id as order_id","gpvor.vendor_id","gpvor.vendor_name","gpvor.area_manager_id","gpvor.order_date","gpvor.or_gross_total","gpvor.or_tot_price","gpvor.acc_or_tot_price","gpvor.order_discount","gpvor.spl_discount","gpvor.branch_id","gpvor.region_id","gpvor.area_id","gpvor.stokist_id","gpvor.warehouse_id","gpvor.order_rej_date","gpvor.vendor_order_status","gpvor.admin_cmt","gpvor.invoice_no","gpvor.reject_reason","gpvor.payment_type","gpvor.credit_lim","gpvor.credit_valid_to","gpvor.or_type","gpvor.payment_name","gpvor.payment_img","gpvor.payment_msg","gpvor.created_at","gpvor.updated_at",'gpvorl.product_genericname','gpvorl.category_name','gpvor.vendor_name as customer_name', 'gpvor.vendor_order_id as order_id','gpvor.order_discount','gpvor.order_date','gpvorlb.batch_no', 'gpvorlb.batch_emp_date','gpvorl.product_qty', 'gpwh.warehouse_name']);

        // return $details;

        $values =  DB::table('gpff_vendor_order as gpvor')
                    ->join('gpff_warehouse as gpwh', 'gpvor.warehouse_id', 'gpwh.warehouse_id')
                    ->where('gpvor.area_manager_id', $Orders->area_manager_id)
                    ->whereBetween(DB::RAW('date(gpvor.created_at)'), [date($Orders->start_date), date($Orders->end_date)])
                    ->orderBy('gpvor.created_at','DESC')
                    ->get(["gpvor.vendor_order_id as order_id","gpvor.vendor_id","gpvor.vendor_name","gpvor.area_manager_id","gpvor.order_date","gpvor.or_gross_total","gpvor.or_tot_price","gpvor.acc_or_tot_price","gpvor.order_discount","gpvor.spl_discount","gpvor.branch_id","gpvor.region_id","gpvor.area_id","gpvor.stokist_id","gpvor.warehouse_id","gpvor.order_rej_date","gpvor.vendor_order_status","gpvor.admin_cmt","gpvor.invoice_no","gpvor.reject_reason","gpvor.payment_type","gpvor.credit_lim","gpvor.credit_valid_to","gpvor.or_type","gpvor.payment_name","gpvor.payment_img","gpvor.payment_msg",'gpvor.vendor_name as customer_name',"gpvor.created_at","gpvor.updated_at",'gpwh.warehouse_name','gpvor.or_blc_price']);

        return $values;
    }

}
