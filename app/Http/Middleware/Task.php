<?php

namespace App\Http\Controllers;

use Closure;
use DB;
use Carbon\Carbon;

require(base_path().'/app/Http/Middleware/Common.php');

class Task
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        return $next($request);
    }

//Task Details
    //Store Task Details 
    public function storeTask($Task){

        $userrole = DB::table('gpff_users')
                ->where('user_id',$Task->created_by)
                ->First();

         $values = array(
            'created_by'        => $Task->created_by , 
            'field_officer_id'  => $Task->field_officer_id ,
            'area_manager_id'   => $Task->area_manager_id ,
            'region_id'         => $Task->region_id ,
            'branch_id'         => $Task->branch_id ,
            'area_id'           => $Task->area_id ,
            'customer_id'       => $Task->customer_id ,
            'customer_type'     => $Task->customer_type , 
            'task_title'        => $Task->task_title ,
            'task_desc'         => $Task->task_desc , 
            'task_date_time'    => $Task->task_date_time , 
            'created_at'        => date('Y-m-d H:i:s') , 
            'updated_at'        => date('Y-m-d H:i:s')
        );
         //3-Regionadmin,4-Areamanager,5-fieldofficer,6-salesref,7-stockist
        //Notification Entry
        if($userrole->role == 3){
            $message = "Region Manager ".$userrole->firstname."Has Assign One Task To you.";
            $page_id = 'UNKNOWN';

            $cmn = new Common();
            $cmn->insertNotification($Task->created_by,$userrole->firstname,$Task->field_officer_id,$message,$page_id);
        } else if($userrole->role == 4){
            $message = "Manager ".$userrole->firstname." Has Assign One Task To you.";
            $page_id = 'UNKNOWN';

            $cmn = new Common();
            $cmn->insertNotification($Task->created_by,$userrole->firstname,$Task->field_officer_id,$message,$page_id);
        }
        
        //End Notification Entry
        return  DB::table('gpff_task')
                ->insert($values);
    }

         //Store multiTask Details 
         public function storemultitask($Task){

            $userrole = DB::table('gpff_users')
                    ->where('user_id',$Task->created_by)
                    ->First();
    
            foreach($Task->customer_list as $customer_lists)
            {   
             $values = array(
                'created_by'        => $Task->created_by , 
                'field_officer_id'  => $Task->field_officer_id ,
                'area_manager_id'   => $Task->area_manager_id ,
                'region_id'         => $Task->region_id ,
                'branch_id'         => $Task->branch_id ,
                'area_id'           => $Task->area_id ,
                'customer_id'       => $customer_lists['customer_id'],
                'customer_type'     => $customer_lists['customer_type'], 
                'task_title'        => $customer_lists['task_title'], 
                'task_desc'         => $customer_lists['task_desc'],  
                'task_date_time'    => $customer_lists['task_date_time'],  
                'created_at'        => date('Y-m-d H:i:s'), 
                'updated_at'        => date('Y-m-d H:i:s')
            );
                $data = DB::table('gpff_task')
                ->insert($values);
            }
             //3-Regionadmin,4-Areamanager,5-fieldofficer,6-salesref,7-stockist
            //Notification Entry
            if($userrole->role == 3){
                $message = "Region Manager ".$userrole->firstname."Has Assign One Task To you.";
                $page_id = 'UNKNOWN';
    
                $cmn = new Common();
                $cmn->insertNotification($Task->created_by,$userrole->firstname,$Task->field_officer_id,$message,$page_id);
            } else if($userrole->role == 4){
                $message = "Manager ".$userrole->firstname." Has Assign One Task To you.";
                $page_id = 'UNKNOWN';
    
                $cmn = new Common();
                $cmn->insertNotification($Task->created_by,$userrole->firstname,$Task->field_officer_id,$message,$page_id);
            }
            
            //End Notification Entry
            return $data;
        }

    //Update Task Details
    public function updateTask($Task){

         $update_values = array(
            'created_by'        => $Task->created_by , 
            'field_officer_id'  => $Task->field_officer_id , 
            'customer_id'       => $Task->customer_id ,
            'customer_type'     => $Task->customer_type , 
            'area_manager_id'   => $Task->area_manager_id ,
            'region_id'         => $Task->region_id ,
            'branch_id'         => $Task->branch_id ,
            'area_id'           => $Task->area_id ,
            'task_title'        => $Task->task_title ,
            'task_desc'         => $Task->task_desc , 
            'task_date_time'    => $Task->task_date_time , 
            'updated_at'        => date('Y-m-d H:i:s')
        );

        return  DB::table('gpff_task')
            ->where('task_id',$Task->task_id)
            ->update($update_values);
    }

    //update Task Status
    public function updateTaskStatus($Task){

        if($Task->task_status == 1){

        $update_values = array(
            'task_status'           => $Task->task_status, 
            'task_start_date_time'  => date('Y-m-d H:i:s'),
            'updated_at'            => date('Y-m-d H:i:s')
        );
        } else if($Task->task_status == 3){
         
            $task_det = DB::table('gpff_task')
                    ->where('task_id',$Task->task_id)
                    ->First(['task_start_date_time']);
       
            $current_date = date('Y-m-d H:i:s');
            $to = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $current_date);
            $from = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $task_det->task_start_date_time);
            $diff_in_minutes = $to->diffInMinutes($from);

            $update_values = array(
                'task_status'           => $Task->task_status, 
                'task_end_date_time'    => date('Y-m-d H:i:s'),
                'task_active_minu'      => $diff_in_minutes,
                'updated_at'            => date('Y-m-d H:i:s')
            );
        } else{

        $update_values = array(
            'task_status'   => $Task->task_status, 
            'updated_at'    => date('Y-m-d H:i:s')
        );
        }
        
       return  DB::table('gpff_task')
            ->where('task_id',$Task->task_id)
            ->update($update_values);
    }

    //reassign Task 
    public function reassignTask($Task){

         $update_values = array(
            'field_officer_id'  => $Task->task_reassign_field_officer_id,
            'created_by'  => $Task->created_by, 
            'task_reassign_status' => $Task->task_reassign_status,
            'updated_at'    => date('Y-m-d H:i:s')
        );

         DB::table('gpff_task')
            ->where('task_id',$Task->task_id)
            ->update($update_values);

        //Notification Entry
        /*$userrole = DB::table('gpff_users')
                ->where('user_id',$Task->created_by)
                ->First();

        if($userrole->role == 1 || $userrole->role == 2){
            if($userrole->role == 1){
                $message = "Admin Has Assign One Task To you.";
            } else if($userrole->role == 2){
                $message = "Manager ".$userrole->firstname." Has Assign One Task To you.";
            }
            $page_id = 'UNKNOWN';

            $cmn = new Common();
            $cmn->insertNotification($Task->created_by,$userrole->firstname,$Task->task_reassign_field_officer_id,$message,$page_id);
        }*/
        //End Notification Entry
        return 1;
    }

    //add notes Task 
    public function addNotes($Task){

         $update_values = array(
            'notes'         => $Task->notes, 
            'updated_at'    => date('Y-m-d H:i:s')
        );

       return  DB::table('gpff_task')
            ->where('task_id',$Task->task_id)
            ->update($update_values);
    }

    //remove Task 
    public function removeTask($Task){

       return  DB::table('gpff_task')
            ->where('task_id',$Task->task_id)
            ->delete();
    }

    //Admin Fetch Task All and current Date
    public function fetchAdminTaskList($Task){

        //Fetch All Tasks based on Field Officer Id
        if($Task->type == '1'){

        return DB::table('gpff_task as gta')
            ->join('gpff_customer as gcus','gta.customer_id', '=' , 'gcus.customer_id')
            ->join('gpff_users as guse','gta.field_officer_id', '=' , 'guse.user_id')
            ->orderBy('gta.created_at', 'ASC')
            ->whereMonth('gta.created_at', 12)
            ->get(['gta.task_id','gta.created_by','gta.field_officer_id','guse.firstname','gta.customer_id','gta.area_manager_id','gta.task_title','gta.task_desc','gta.task_status','gta.task_date_time','gta.task_reassign_status','gta.task_start_date_time','gta.task_end_date_time','gta.task_active_minu','gta.notes','gta.created_at','gta.updated_at','gcus.customer_name','gcus.customer_email','gcus.country_code','gcus.customer_mobile','gcus.customer_city','gcus.customer_country','gcus.customer_fax','gcus.customer_address','gcus.customer_location','gcus.customer_lan','gcus.customer_lat','gcus.customer_cr_id','gcus.website','gcus.avatar','gcus.is_active','gta.branch_id','gta.region_id','gta.area_id']);
        } 
        //Fetch Current Date Tasks
        else if($Task->type == '2'){

            $today = date('Y-m-d');

        return $query = DB::table('gpff_task as gta')
                ->join('gpff_customer as gcus', 'gta.customer_id', '=', 'gcus.customer_id')
                ->join('gpff_users as guse', 'gta.field_officer_id', '=', 'guse.user_id')
                ->where('gta.task_date_time', 'like', $today . '%')
                ->orderBy('gta.created_at', 'ASC')
                ->limit(10)
                ->select('gta.task_date_time', 'gta.task_start_date_time', 'gta.task_end_date_time', 'gta.created_at', 'gta.updated_at', 'gcus.customer_name','gcus.customer_country', 'gcus.customer_location')
                ->get();


        // $today = date('Y-m-d');
        // $today = Carbon::now()->format('Y-m-d').'%';

        // return DB::table('gpff_task as gta')
        //     ->join('gpff_customer as gcus','gta.customer_id', '=' , 'gcus.customer_id')
        //     ->join('gpff_users as guse','gta.field_officer_id', '=' , 'guse.user_id')
        //     ->where('gta.task_date_time', 'like', $today)
        //     ->orderBy('gta.created_at', 'ASC')->limit(10)
        //     ->get(['gta.task_id','gta.created_by','gta.field_officer_id','guse.firstname','gta.customer_id','gta.area_manager_id','gta.task_title','gta.task_desc','gta.task_status','gta.task_date_time','gta.task_reassign_status','gta.task_start_date_time','gta.task_end_date_time','gta.task_active_minu','gta.notes','gta.created_at','gta.updated_at','gcus.customer_name','gcus.customer_email','gcus.country_code','gcus.customer_mobile','gcus.customer_city','gcus.customer_country','gcus.customer_fax','gcus.customer_address','gcus.customer_location','gcus.customer_lan','gcus.customer_lat','gcus.customer_cr_id','gcus.website','gcus.avatar','gcus.is_active','gta.branch_id','gta.region_id','gta.area_id']);

        } 
        //Error 
        else{
            return 2;
        }
    }

    //Area Manager Fetch Task All and current Date
    public function fetchAreaManageTaskList($Task){

        //Fetch All Tasks based on Area
        if($Task->type == '1'){

        return DB::table('gpff_task as gta')
            ->join('gpff_customer as gcus','gta.customer_id', '=' , 'gcus.customer_id')
            ->join('gpff_users as guse','gta.field_officer_id', '=' , 'guse.user_id')
            ->where('gta.created_by', $Task->user_id)
            ->orderBy('gta.created_at', 'DESC')
            ->get(['gta.task_id','gta.created_by','gta.field_officer_id','guse.firstname','gta.customer_id','gta.area_manager_id','gta.task_title','gta.task_desc','gta.task_status','gta.task_date_time','gta.task_reassign_status','gta.task_start_date_time','gta.task_end_date_time','gta.task_active_minu','gta.notes','gta.created_at','gta.updated_at','gcus.customer_name','gcus.customer_email','gcus.country_code','gcus.customer_mobile','gcus.customer_city','gcus.customer_country','gcus.customer_fax','gcus.customer_address','gcus.customer_location','gcus.customer_lan','gcus.customer_lat','gcus.customer_cr_id','gcus.website','gcus.avatar','gcus.is_active','gta.branch_id','gta.region_id','gta.area_id']);
        } 
        //Fetch Current Date Tasks
        else if($Task->type == '2'){

            $today = date('Y-m-d') . '%';

            $query = DB::table('gpff_task as gta')
                ->join('gpff_customer as gcus', 'gta.customer_id', '=', 'gcus.customer_id')
                ->join('gpff_users as guse', 'gta.field_officer_id', '=', 'guse.user_id')
                ->where('gta.area_manager_id', $Task->user_id)
                ->where('gta.task_date_time', 'like', $today)
                ->orderBy('gta.created_at', 'ASC')
                ->limit(10)
                ->select('gta.task_date_time', 'gta.created_at', 'gta.updated_at', 'gcus.customer_name', 'gcus.customer_location')
                ->get();
            
            return $query;
        } 
        //Error 
        else{
            return 2;
        }
    }


    //Fetch Task Based on Id and current Date
    public function fetchTaskList($Task){

        //Fetch All Tasks based on Field Officer Id
        if($Task->type == '1'){

        return DB::table('gpff_task as gta')
            ->join('gpff_customer as gcus','gta.customer_id', '=' , 'gcus.customer_id')
            ->where('gta.field_officer_id', $Task->field_officer_id)
            ->orderBy('gta.created_at', 'DESC')
            ->get(['gta.task_id','gta.created_by','gta.field_officer_id','gta.customer_id','gta.area_manager_id','gta.task_title','gta.task_desc','gta.task_status','gta.task_date_time','gta.task_reassign_status','gta.task_start_date_time','gta.task_end_date_time','gta.task_active_minu','gta.notes','gta.created_at','gta.updated_at','gcus.customer_name','gcus.customer_email','gcus.country_code','gcus.customer_mobile','gcus.customer_city','gcus.customer_country','gcus.customer_fax','gcus.customer_address','gcus.customer_location','gcus.customer_lan','gcus.customer_lat','gcus.customer_cr_id','gcus.website','gcus.avatar','gcus.is_active','gta.branch_id','gta.region_id','gta.area_id']);
        } 
        //Fetch Current Date Tasks
        else if($Task->type == '2'){

        $today = date('Y-m-d');
        $today = Carbon::now()->format('Y-m-d').'%';

        return DB::table('gpff_task as gta')
            ->join('gpff_customer as gcus','gta.customer_id', '=' , 'gcus.customer_id')
            ->where('gta.field_officer_id', $Task->field_officer_id)
            ->where('gta.task_date_time', 'like', $today)
            ->orderBy('gta.created_at', 'ASC')
            ->get(['gta.task_id','gta.created_by','gta.field_officer_id','gta.customer_id','gta.area_manager_id','gta.task_title','gta.task_desc','gta.task_status','gta.task_date_time','gta.task_reassign_status','gta.task_start_date_time','gta.task_end_date_time','gta.task_active_minu','gta.notes','gta.created_at','gta.updated_at','gcus.customer_name','gcus.customer_email','gcus.country_code','gcus.customer_mobile','gcus.customer_city','gcus.customer_country','gcus.customer_fax','gcus.customer_address','gcus.customer_location','gcus.customer_lan','gcus.customer_lat','gcus.customer_cr_id','gcus.website','gcus.avatar','gcus.is_active','gta.branch_id','gta.region_id','gta.area_id']);

        } 
        //Fetch Custom Date Tasks //2020-05-06
        else if($Task->type == '3'){
        
            $date = $Task->date.'%';

            return DB::table('gpff_task as gta')
                ->join('gpff_customer as gcus','gta.customer_id', '=' , 'gcus.customer_id')
                ->where('gta.field_officer_id', $Task->field_officer_id)
                ->where('gta.task_date_time', 'like', $date)
                ->orderBy('gta.created_at', 'ASC')
                ->get(['gta.task_id','gta.created_by','gta.field_officer_id','gta.customer_id','gta.area_manager_id','gta.task_title','gta.task_desc','gta.task_status','gta.task_date_time','gta.task_reassign_status','gta.task_start_date_time','gta.task_end_date_time','gta.task_active_minu','gta.notes','gta.created_at','gta.updated_at','gcus.customer_name','gcus.customer_email','gcus.country_code','gcus.customer_mobile','gcus.customer_city','gcus.customer_country','gcus.customer_fax','gcus.customer_address','gcus.customer_location','gcus.customer_lan','gcus.customer_lat','gcus.customer_cr_id','gcus.website','gcus.avatar','gcus.is_active','gta.branch_id','gta.region_id','gta.area_id']);
        } 
        //Fetch task
        else if($Task->type == '4'){
        
            $today = date('Y-m-d');
            $today = Carbon::now()->format('Y-m-d').'%';

            return DB::table('gpff_task as gta')
                ->join('gpff_customer as gcus','gta.customer_id', '=' , 'gcus.customer_id')
                ->where('gta.field_officer_id', $Task->field_officer_id)
                ->where('gta.task_status', 1)
                ->where('gta.task_date_time', 'like', $today)
                ->orderBy('gta.created_at', 'ASC')
                ->get(['gta.task_id','gta.created_by','gta.field_officer_id','gta.customer_id','gta.area_manager_id','gta.task_title','gta.task_desc','gta.task_status','gta.task_date_time','gta.task_reassign_status','gta.task_start_date_time','gta.task_end_date_time','gta.task_active_minu','gta.notes','gta.created_at','gta.updated_at','gcus.customer_name','gcus.customer_email','gcus.country_code','gcus.customer_mobile','gcus.customer_city','gcus.customer_country','gcus.customer_fax','gcus.customer_address','gcus.customer_location','gcus.customer_lan','gcus.customer_lat','gcus.customer_cr_id','gcus.website','gcus.avatar','gcus.is_active','gta.branch_id','gta.region_id','gta.area_id']);
        
        } else if($Task->type == '5'){

            return DB::table('gpff_task as gta')
            ->join('gpff_customer as gcus','gta.customer_id', '=' , 'gcus.customer_id')
            ->where('gta.field_officer_id', $Task->field_officer_id)
            ->where('gta.task_status', $Task->task_status)
            ->orderBy('gta.created_at', 'DESC')
            ->get(['gta.task_id','gta.created_by','gta.field_officer_id','gta.customer_id','gta.area_manager_id','gta.task_title','gta.task_desc','gta.task_status','gta.task_date_time','gta.task_reassign_status','gta.task_start_date_time','gta.task_end_date_time','gta.task_active_minu','gta.notes','gta.created_at','gta.updated_at','gcus.customer_name','gcus.customer_email','gcus.country_code','gcus.customer_mobile','gcus.customer_city','gcus.customer_country','gcus.customer_fax','gcus.customer_address','gcus.customer_location','gcus.customer_lan','gcus.customer_lat','gcus.customer_cr_id','gcus.website','gcus.avatar','gcus.is_active','gta.branch_id','gta.region_id','gta.area_id']);
        } 
        //Error 
        else{
            return 2;
        }
    }

    //Fetch Area Based Task and current Date
    public function fetchAreaBaseTaskList($Task){

        //Fetch All Tasks based on Area
        if($Task->type == '1'){

        return DB::table('gpff_task as gta')
            ->join('gpff_customer as gcus','gta.customer_id', '=' , 'gcus.customer_id')
            ->join('gpff_users as guse','gta.field_officer_id', '=' , 'guse.user_id')
            ->where('gta.area_id', $Task->area_id)
            ->orderBy('gta.created_at', 'ASC')
            ->get();
        } 
        //Fetch Current Date Tasks
        else if($Task->type == '2'){

        $today = date('Y-m-d');
        $today = Carbon::now()->format('Y-m-d').'%';

        return DB::table('gpff_task as gta')
            ->join('gpff_customer as gcus','gta.customer_id', '=' , 'gcus.customer_id')
            ->join('gpff_users as guse','gta.field_officer_id', '=' , 'guse.user_id')
            ->where('gta.area_id', $Task->area_id)
            ->where('gta.task_date_time', 'like', $today)
            ->orderBy('gta.created_at', 'ASC')
            ->get();

        } 
        //Fetch Custom Date Tasks //2020-05-06
        else if($Task->type == '3'){
        
            $date = $Task->date.'%';

            return DB::table('gpff_task as gta')
                ->join('gpff_customer as gcus','gta.customer_id', '=' , 'gcus.customer_id')
                ->join('gpff_users as guse','gta.field_officer_id', '=' , 'guse.user_id')
                ->where('gta.area_id', $Task->area_id)
                ->where('gta.task_date_time', 'like', $date)
                ->orderBy('gta.created_at', 'ASC')
                ->get();

        } 
        //Error 
        else{
            return 2;
        }

    }

    //Fetch Region Based Task and current Date
    public function fetchRegionBaseTaskList($Task){
        //Fetch All Tasks based on Admin
        if($Task->type == '1'){
            return DB::table('gpff_task as gta')
                ->join('gpff_customer as gcus','gta.customer_id', '=' , 'gcus.customer_id')
                ->join('gpff_users as guse','gta.field_officer_id', '=' , 'guse.user_id')
                ->where('gta.region_id', $Task->region_id)
                ->orderBy('gta.created_at', 'ASC')
                ->get();
        } 
        //Fetch Current Date Tasks
        else if($Task->type == '2'){

            $today = date('Y-m-d');
            $today = Carbon::now()->format('Y-m-d').'%';
            return DB::table('gpff_task as gta')
                ->join('gpff_customer as gcus','gta.customer_id', '=' , 'gcus.customer_id')
                ->join('gpff_users as guse','gta.field_officer_id', '=' , 'guse.user_id')
                ->where('gta.region_id', $Task->region_id)
                ->where('gta.task_date_time', 'like', $today)
                ->orderBy('gta.created_at', 'ASC')
                ->get();

        } 
        //Fetch Custom Date Tasks //2020-05-06
        else if($Task->type == '3'){
        
            $date = $Task->date.'%';
            return DB::table('gpff_task as gta')
                ->join('gpff_customer as gcus','gta.customer_id', '=' , 'gcus.customer_id')
                ->join('gpff_users as guse','gta.field_officer_id', '=' , 'guse.user_id')
                ->where('gta.region_id', $Task->region_id)
                ->where('gta.task_date_time', 'like', $date)
                ->orderBy('gta.created_at', 'ASC')
                ->get();
        } 
        //Error 
        else{
            return 2;
        }
    }

    //Fetch Ind Task Details
    public function fetchIndTaskDetails($Task){

        return DB::table('gpff_task as gta')
            ->join('gpff_customer as gcus','gta.customer_id', '=' , 'gcus.customer_id')
            ->where('gta.task_id', $Task->task_id)
            ->get();
    }

    //Fetch Ind Task All Details Fetch
    public function fetchIndTaskAllDetails($Task){
        $data = [];

        $data['task_details'] =  DB::table('gpff_task as gta')
                ->join('gpff_customer as gcus','gta.customer_id', '=' , 'gcus.customer_id')
                ->where('gta.task_id', $Task->task_id)
                ->First();

        $data['task_revisit_details'] =  DB::table('gpff_Revisit')
                            ->where('task_id', $Task->task_id)
                            ->First();

        //1-NOT AVAILABLE,2-FIRST CALL( LEVEL OF DOCTOR),3-PROMOTION ITEMS DETAILS,4-FEEDBACK,5-COMPLAINT DISTRIBUTION,6-COMPLAINT PRODUCTS,7-COMPLAINT PRICE ,8-COMPETITOR
        $data['task_notavailable_details'] =   DB::table('gpff_feedback')
                            ->where('task_id', $Task->task_id)
                            ->where('type',1)
                            ->get();

        $data['task_firstcall_details'] =   DB::table('gpff_feedback')
                            ->where('task_id', $Task->task_id)
                            ->where('type',2)
                            ->get();

        $data['task_promoted_details'] =   DB::table('gpff_feedback')
                            ->where('task_id', $Task->task_id)
                            ->where('type',3)
                            ->get();

        $data['task_feedback_details'] =   DB::table('gpff_feedback')
                            ->where('task_id', $Task->task_id)
                            ->where('type',4)
                            ->get();

        $data['task_complaintdistribut_details'] =   DB::table('gpff_feedback')
                            ->where('task_id', $Task->task_id)
                            ->where('type',5)
                            ->get();

        $data['task_complaintproduct_details'] =   DB::table('gpff_feedback')
                            ->where('task_id', $Task->task_id)
                            ->where('type',6)
                            ->get();

        $data['task_complaintprice_details'] =   DB::table('gpff_feedback')
                            ->where('task_id', $Task->task_id)
                            ->where('type',7)
                            ->get();

        $data['task_complaintcompetitior_details'] =   DB::table('gpff_feedback')
                            ->where('task_id', $Task->task_id)
                            ->where('type',8)
                            ->get();

        $data['task_gift_details'] =   DB::table('gpff_delivered_gift')
                            ->where('task_id', $Task->task_id)
                            ->get();

        $data['task_sample_details'] =   DB::table('gpff_delivered_samples')
                            ->where('task_id', $Task->task_id)
                            ->get();
        
        return $data;
    }

    //Field Officer Side Fetch Customer Based All Task and Task Based All Order or Feedback or Gift or Revisit Details Fetch
    public function getFieldTaskAllDetails($Task){

        $data =[];

        $taskid =   DB::table('gpff_task')
                    ->where('field_officer_id', $Task->field_officer_id)
                    ->where('customer_id', $Task->customer_id)
                    ->where('task_status', 3)
                    ->orderBy('task_end_date_time', 'DESC')
                    ->First(['task_id']);
        if($taskid){
        $data['taskcustdetails'] =   DB::table('gpff_task as gta')
                                    ->join('gpff_customer as gcus','gta.customer_id', '=' , 'gcus.customer_id')
                                    ->where('gta.task_id', $taskid->task_id)
                                    ->get();

        $data['notavailable']   =   DB::table('gpff_feedback')
                                ->where('task_id', $taskid->task_id)
                                ->where('type',1)
                                ->get();

        $data['firstcall']   =   DB::table('gpff_feedback')
                                ->where('task_id', $taskid->task_id)
                                ->where('type',2)
                                ->get();

        $data['promoted']   =   DB::table('gpff_feedback')
                                ->where('task_id', $taskid->task_id)
                                ->where('type',3)
                                ->get();

        $data['feedback']   =   DB::table('gpff_feedback')
                                ->where('task_id', $taskid->task_id)
                                ->where('type',4)
                                ->get();

        $data['complaintdistribut'] =   DB::table('gpff_feedback')
                            ->where('task_id', $Task->task_id)
                            ->where('type',5)
                            ->get();

        $data['complaintproduct'] =   DB::table('gpff_feedback')
                            ->where('task_id', $Task->task_id)
                            ->where('type',6)
                            ->get();

        $data['complaintprice'] =   DB::table('gpff_feedback')
                            ->where('task_id', $Task->task_id)
                            ->where('type',7)
                            ->get();

        $data['complaintcompetitior'] =   DB::table('gpff_feedback')
                            ->where('task_id', $Task->task_id)
                            ->where('type',8)
                            ->get();

        $data['delivered_gift'] =   DB::table('gpff_delivered_gift')
                                    ->where('task_id', $taskid->task_id)
                                    ->get();

        $data['Revisit']        =   DB::table('gpff_Revisit')
                                    ->where('task_id', $taskid->task_id)
                                    ->get();

        $data['order']      =   DB::table('gpff_order as or')
                                ->join('gpff_order_list as grl','or.order_id', '=' , 'grl.order_id')
                                ->where('or.task_id', $taskid->task_id)
                                ->get();

            return $data;
        } else{
            return 2;
        }

    }
}
