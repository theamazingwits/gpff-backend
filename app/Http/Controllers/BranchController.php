<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use File;
use Mail;

require(base_path().'/app/Http/Middleware/Branch.php');

class BranchController extends Controller
{
////////////////////////////
//Branch Managment Api Calls//
////////////////////////////
    // Add Branch Details 
    public function addBranch(Request $request){
        $branch = new Branch();
        $branch->branch_name    = $request->input('branch_name');
        $branch->branch_description     = $request->input('branch_description');
        $branch->branch_address     = $request->input('branch_address');
        $branch->branch_currency        = $request->input('branch_currency');
        $branch->branch_country      = $request->input('branch_country');
        $branch->branch_contact         = $request->input('branch_contact');
        $branch->country_code         = $request->input('country_code');
        $branch->country         = $request->input('country');
        
        if($request->input('branch_landline')){
        	$branch->branch_landline = $request->input('branch_landline');
        }else{
        	$branch->branch_landline = '';
        }

        $result = $branch->storeBranch($branch);
        if($result){
            $response = response()->json(['status' => 'success', 'message' => 'Branch details stored successfully']);
        }else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Error in storing details....']);
        }
        return $response;
    }
    // Update Users Details
    public function updateBranch(Request $request)
    {
        $branch = new Branch();
        $branch->branch_id = $request->input('branch_id');
        $branch->branch_name    = $request->input('branch_name');
        $branch->branch_description     = $request->input('branch_description');
        $branch->branch_address     = $request->input('branch_address');
        $branch->branch_currency        = $request->input('branch_currency');
        $branch->branch_country      = $request->input('branch_country');
        $branch->branch_contact         = $request->input('branch_contact');
        $branch->country_code         = $request->input('country_code');
        $branch->country         = $request->input('country');

        if($request->input('branch_landline')){
            $branch->branch_landline = $request->input('branch_landline');
        }else{
            $branch->branch_landline = '';
        }

        $result = $branch->updateBranch($branch);
        if($result > 0){
            $response = response()->json(['status' => 'success', 'message' => 'Branch Details Update successfully','data'=> $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Error in Update Brnach Details']);
        }
        return $response;
    }

    // Delete User details
    public function deleteBranch(Request $request){
        $branch = new Branch();
        $branch->branch_id = $request->input('branch_id');

        $result = $branch->deleteBranch($branch);
        if($result > 0){
            $response = response()->json(['status' => 'success' ,'message' => 'Branch details deleted successfully']);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Error in deleting']); 
        }
        return $response;
    }

     //Branch Activate Or Deactivate
    public function branchActiveOrDeactive(Request $request)
    {
        $branch = new Branch();
        $branch->branch_id = $request->input('branch_id');
        $branch->branch_status  = $request->input('branch_status');
        
        $result = $branch->branchActiveOrDeactive($branch);
        if($result > 0){
            if($branch->branch_status == 1){
                $response = response()->json(['status' => 'success', 'message' => 'Branch Actived Successfully']);
            } elseif ($branch->branch_status == 2) {
                $response = response()->json(['status' => 'success', 'message' => 'Branch Deactived Successfully']);
            }
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Error in Updated details.. !']);
        }
        return $response;
    }

    // Fetch (All)Branch Details
    public function getAllBranch()
    {
        $branch = new Branch();

        $data = $branch->getAllBranch();
        if(count($data) > 0 ){
            $response = response()->json(['status' => 'success' , 'message' => 'Branch Data retrived successfully','data'=>$data]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Error in Get details..','data'=>'null']);
        }
        return $response;
    } 

  // Get Indivitual Branch Details
    public function getIndBranch($branch_id)
    {
        $branch = new Branch();
        $branch->branch_id = $branch_id;

        $result = $branch->getIndBranch($branch);
        if(count($result) > 0){
            $response = response()->json(['status' => 'success' , 'message' => '', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Invalid branch details']); 
        }
        return $response;
    }  

    // Get Region Manager based Details
    public function getIdBasedBranch($manager_id)
    {
        $branch = new Branch();
        $branch->manager_id = $manager_id;

        $result = $branch->getIdBasedBranch($branch);
        if(count($result) > 0){
            $response = response()->json(['status' => 'success' , 'message' => '', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Invalid branch details']); 
        }
        return $response;
    }  



    ////////////////////////////
//Region Managment Api Calls//
////////////////////////////
    // Add Region Details 
    public function addRegion(Request $request){
        $branch = new Branch();
        $branch->region_name    = $request->input('region_name');
        $branch->region_address     = $request->input('region_address');
        $branch->region_description     = $request->input('region_description');
        $branch->region_contact      = $request->input('region_contact');
        $branch->branch_id         = $request->input('branch_id');
        $branch->country_code         = $request->input('country_code');
        $branch->country         = $request->input('country');

        
        if($request->input('region_landline')){
        	$branch->region_landline = $request->input('region_landline');
        }else{
        	$branch->region_landline = '';
        }

        $result = $branch->storeRegion($branch);
        if($result){
            $response = response()->json(['status' => 'success', 'message' => 'Region details stored successfully']);
        }else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Error in storing details....']);
        }
        return $response;
    }
    // Update Region Details
    public function updateRegion(Request $request)
    {
        $branch = new Branch();
        $branch->region_id    = $request->input('region_id');
        $branch->region_name    = $request->input('region_name');
        $branch->region_address     = $request->input('region_address');
        $branch->region_description     = $request->input('region_description');
        $branch->region_contact      = $request->input('region_contact');
        $branch->branch_id         = $request->input('branch_id');
        $branch->country_code         = $request->input('country_code');
        $branch->country         = $request->input('country');

        if($request->input('region_landline')){
            $branch->region_landline = $request->input('region_landline');
        }else{
            $branch->region_landline = '';
        }

        $result = $branch->updateRegion($branch);
        if($result > 0){
            $response = response()->json(['status' => 'success', 'message' => 'Region Details Update successfully','data'=> $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Error in Update Region Details']);
        }
        return $response;
    }

    // Delete Region details
    public function deleteRegion(Request $request){
        $branch = new Branch();
        $branch->region_id = $request->input('region_id');

        $result = $branch->deleteRegion($branch);
        if($result > 0){
            $response = response()->json(['status' => 'success' ,'message' => 'Region details deleted successfully']);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Error in deleting']); 
        }
        return $response;
    }

     //Region Activate Or Deactivate
    public function regionActiveOrDeactive(Request $request)
    {
        $branch = new Branch();
        $branch->region_id = $request->input('region_id');
        $branch->region_status  = $request->input('region_status');
        
        $result = $branch->regionActiveOrDeactive($branch);
        if($result > 0){
            if($branch->region_status == 1){
                $response = response()->json(['status' => 'success', 'message' => 'Region Actived Successfully']);
            } elseif ($branch->region_status == 2) {
                $response = response()->json(['status' => 'success', 'message' => 'Region Deactived Successfully']);
            }
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Error in Updated details.. !']);
        }
        return $response;
    }

     // Fetch (All)Region Details
    public function getAllRegion()
    {
        $branch = new Branch();

        $data = $branch->getAllRegion();
        if(count($data) > 0 ){
            $response = response()->json(['status' => 'success' , 'message' => 'Region Data retrived successfully','data'=>$data]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Error in Get details..','data'=>'null']);
        }
        return $response;
    }   

     // Get Indivitual Region Details
    public function getIndRegion($region_id)
    {
        $branch = new Branch();
        $branch->region_id = $region_id;

        $result = $branch->getIndRegion($branch);
        if(count($result) > 0){
            $response = response()->json(['status' => 'success' , 'message' => '', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Invalid region details']); 
        }
        return $response;
    }  

     // Get Region Manager based Region Details
    public function getIdBasedRegion($manager_id)
    {
        $branch = new Branch();
        $branch->manager_id = $manager_id;

        $result = $branch->getIdBasedRegion($branch);
        if(count($result) > 0){
            $response = response()->json(['status' => 'success' , 'message' => '', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Invalid region details']); 
        }
        return $response;
    }  


     ////////////////////////////
//Area Managment Api Calls//
////////////////////////////
    // Add Area Details 
    public function addArea(Request $request){
        $branch = new Branch();
        $branch->area_name          = $request->input('area_name');
        $branch->area_address       = $request->input('area_address');
        $branch->area_description   = $request->input('area_description');
        $branch->area_contact       = $request->input('area_contact');
        $branch->branch_id          = $request->input('branch_id');
        $branch->region_id          = $request->input('region_id');
        $branch->country_code       = $request->input('country_code');
        $branch->country            = $request->input('country');
        $branch->pin_code           = $request->input('pin_code');
        
        if($request->input('area_landline')){
        	$branch->area_landline = $request->input('area_landline');
        }else{
        	$branch->area_landline = '';
        }

        $result = $branch->storeArea($branch);
        if($result){
            $response = response()->json(['status' => 'success', 'message' => 'Area details stored successfully']);
        }else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Error in storing details....']);
        }
        return $response;
    }
    // Update Area Details
    public function updateArea(Request $request)
    {
        $branch = new Branch();
        $branch->area_id         = $request->input('area_id');
        $branch->area_name    = $request->input('area_name');
        $branch->area_address     = $request->input('area_address');
        $branch->area_description     = $request->input('area_description');
        $branch->area_contact        = $request->input('area_contact');
        $branch->branch_id         = $request->input('branch_id');
        $branch->region_id         = $request->input('region_id');
        $branch->country_code         = $request->input('country_code');
        $branch->country         = $request->input('country');
        $branch->pin_code         = $request->input('pin_code');

        if($request->input('area_landline')){
            $branch->area_landline = $request->input('area_landline');
        }else{
            $branch->area_landline = '';
        }

        $result = $branch->updateArea($branch);
        if($result > 0){
            $response = response()->json(['status' => 'success', 'message' => 'Area Details Update successfully','data'=> $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Error in Update Area Details']);
        }
        return $response;
    }

    // Delete Area details
    public function deleteArea(Request $request){
        $branch = new Branch();
        $branch->area_id = $request->input('area_id');

        $result = $branch->deleteArea($branch);
        if($result > 0){
            $response = response()->json(['status' => 'success' ,'message' => 'Area details deleted successfully']);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Error in deleting']); 
        }
        return $response;
    }

     //Area Activate Or Deactivate
    public function areaActiveOrDeactive(Request $request)
    {
        $branch = new Branch();
        $branch->area_id = $request->input('area_id');
        $branch->area_status  = $request->input('area_status');
        
        $result = $branch->areaActiveOrDeactive($branch);
        if($result > 0){
            if($branch->area_status == 1){
                $response = response()->json(['status' => 'success', 'message' => 'Area Actived Successfully']);
            } elseif ($branch->area_status == 2) {
                $response = response()->json(['status' => 'success', 'message' => 'Area Deactived Successfully']);
            }
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Error in Updated details.. !']);
        }
        return $response;
    }

    // Fetch (All)Area Details
    public function getAllArea()
    {
        $branch = new Branch();

        $data = $branch->getAllArea();
        if(count($data) > 0 ){
            $response = response()->json(['status' => 'success' , 'message' => 'Area Data retrived successfully','data'=>$data]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Error in Get details..','data'=>'null']);
        }
        return $response;
    }   

    // Get Indivitual Area Details
    public function getIndArea($area_id)
    {
        $branch = new Branch();
        $branch->area_id = $area_id;

        $result = $branch->getIndArea($branch);
        if(count($result) > 0){
            $response = response()->json(['status' => 'success' , 'message' => '', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Invalid area details']); 
        }
        return $response;
    }  

     // Get Region Manager based Details
    public function getIdBasedArea($manager_id)
    {
        $branch = new Branch();
        $branch->manager_id = $manager_id;

        $result = $branch->getIdBasedArea($branch);
        if(count($result) > 0){
            $response = response()->json(['status' => 'success' , 'message' => '', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Invalid area details']); 
        }
        return $response;
    }  

    //Area Based Field Details Fetch 
    public function getAreaBasedField(Request $request)
    {
        $branch = new Branch();
        $branch->area_id = $request->input('area_id');
        
        $result = $branch->getAreaBasedField($branch);
        if(count($result) > 0){
            $response = response()->json(['status' => 'success' , 'message' => '', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Invalid Field Officer details']);
        }
        return $response;
    }
    //Multi Area Based Field Details Fetch 
    public function getMultiAreaBasedField(Request $request)
    {
        $branch = new Branch();
        $branch->area_id = $request->input('area_id');
        
        $result = $branch->getMultiAreaBasedField($branch);
        if(count($result) > 0){
            $response = response()->json(['status' => 'success' , 'message' => '', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Invalid Field Officer details']); 
        }
        return $response;
    }
    //Area Based Field Details Fetch 
    public function getAreabasFO(Request $request)
    {
        $branch = new Branch();
        $branch->user_id = $request->input('user_id');
        
        $result = $branch->getAreabasFO($branch);
        if(count($result) > 0){
            $response = response()->json(['status' => 'success' , 'message' => '', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Invalid Field Officer details']); 
        }
        return $response;
    }
 ////////////////////////////
//Warehouse Managment Api Calls//
////////////////////////////
    // Add Warehouse Details 
    public function addWarehouse(Request $request){

        $branch = new Branch();
        $branch->warehouse_name    = $request->input('warehouse_name');
        $branch->branch_id         = $request->input('branch_id');
        $branch->region_id         = $request->input('region_id');
        // $branch->area_id       = implode(',', $request->input('area_id'));
        $branch->area_id = $request->input('area_id');
        $branch->warehouse_cr_id     = $request->input('warehouse_cr_id');
        $branch->stockist_id     = $request->input('stockist_id');
        $branch->warehouse_lan        = $request->input('warehouse_lan');
        $branch->warehouse_lat        = $request->input('warehouse_lat');
        $branch->warehouse_address        = $request->input('warehouse_address');
        $branch->country_code         = $request->input('country_code');
        $branch->country         = $request->input('country');
        $branch->warehouse_type    = $request->input('warehouse_type');
        $branch->warehouse_landline    = $request->input('warehouse_landline');
        $branch->main_warehouse_id  = $request->input('main_warehouse_id');
        $branch->main_warehouse_name  = $request->input('main_warehouse_name');

        if($request->input('warehouse_contact')){
        	$branch->warehouse_contact = $request->input('warehouse_contact');
        }else{
        	$branch->warehouse_contact = '';
        }

        $result = $branch->storeWarehouse($branch);
        if($result == 1){
            $response = response()->json(['status' => 'success', 'message' => 'Warehouse details stored successfully']);
        }elseif($result == 2){
            $response = response()->json(['status' => 'failure' , 'message' => 'Stockist Alredy Assign another Warehouse....']);
        }else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Error in Stored Warehouse Details.']);
        }
        return $response;
    }
    // Update Warehouse Details
    public function updateWarehouse(Request $request)
    {
        $branch = new Branch();
        $branch->warehouse_id       = $request->input('warehouse_id');
        $branch->warehouse_name     = $request->input('warehouse_name');
        $branch->branch_id          = $request->input('branch_id');
        $branch->region_id          = $request->input('region_id');
        $branch->area_id            = $request->input('area_id');
        $branch->warehouse_cr_id    = $request->input('warehouse_cr_id');
        $branch->stockist_id        = $request->input('stockist_id');
        $branch->warehouse_lan      = $request->input('warehouse_lan');
        $branch->warehouse_lat      = $request->input('warehouse_lat');
        $branch->warehouse_address  = $request->input('warehouse_address');
        $branch->warehouse_landline    = $request->input('warehouse_landline');
        $branch->country_code       = $request->input('country_code');
        $branch->country            = $request->input('country');
        if($request->input('warehouse_contact')){
            $branch->warehouse_contact = $request->input('warehouse_contact');
        }else{
            $branch->warehouse_contact = '';
        }
        $branch->warehouse_type    = $request->input('warehouse_type');
        $branch->main_warehouse_id  = $request->input('main_warehouse_id');
        $branch->main_warehouse_name  = $request->input('main_warehouse_name');

        $result = $branch->updateWarehouse($branch);
        if($result > 0){
            $response = response()->json(['status' => 'success', 'message' => 'Warehouse Details Update successfully','data'=> $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Error in Update Warehouse Details']);
        }
        return $response;
    }

    // Delete Warehouse details
    public function deleteWarehouse(Request $request){
        $branch = new Branch();
        $branch->warehouse_id = $request->input('warehouse_id');

        $result = $branch->deleteWarehouse($branch);
        if($result > 0){
            $response = response()->json(['status' => 'success' ,'message' => 'Warehouse details deleted successfully']);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Error in deleting']); 
        }
        return $response;
    }

     //Warehouse Activate Or Deactivate
    public function warehouseActiveOrDeactive(Request $request)
    {
        $branch = new Branch();

        $branch->warehouse_id = $request->input('warehouse_id');
        $branch->warehouse_status  = $request->input('warehouse_status');
        
        $result = $branch->warehouseActiveOrDeactive($branch);
        if($result > 0){
            if($branch->warehouse_status == 1){
                $response = response()->json(['status' => 'success', 'message' => 'Warehouse Actived Successfully']);
            } elseif ($branch->warehouse_status == 2) {
                $response = response()->json(['status' => 'success', 'message' => 'Warehouse Deactived Successfully']);
            }
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Error in Updated details.. !']);
        }
        return $response;
    }

    // Fetch (All)Warehouse Details
    public function getAllWarehouse()
    {
        $branch = new Branch();
        
        $data = $branch->getAllWarehouse();
        if(count($data) > 0 ){
            $response = response()->json(['status' => 'success' , 'message' => 'Warehouse Data retrived successfully','data'=>$data]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Error in Get details..','data'=>'null']);
        }
        return $response;
    } 

    // Get Indivitual Warehouse Details
    public function getIndWarehouse($warehouse_id)
    {
        $branch = new Branch();
        $branch->warehouse_id = $warehouse_id;

        $result = $branch->getIndWarehouse($branch);
        if(count($result) > 0){
            $response = response()->json(['status' => 'success' , 'message' => '', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Invalid warehouse details']); 
        }
        return $response;
    }  

    // Get Main Based Sub Warehouse Details
    public function getMainBaseSubWarehouse($warehouse_id)
    {
        $branch = new Branch();
        $branch->warehouse_id = $warehouse_id;

        $result = $branch->getMainBaseSubWarehouse($branch);
        if(count($result) > 0){
            $response = response()->json(['status' => 'success' , 'message' => '', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Invalid warehouse details']); 
        }
        return $response;
    }  

     // Get (All)Stockist Details
    public function getAllStockist()
    {
        $branch = new Branch();

        $result = $branch->getAllStockist();
        if(count($result) > 0){
            $response = response()->json(['status' => 'success' , 'message' => '', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Invalid warehouse details']); 
        }
        return $response;
    }


    // Get Region based Warehouse Details
    public function getRegionBasWare(Request $request)
    {
        $branch = new Branch();
        $branch->region_id = $request->input('region_id');
        
        $result = $branch->getRegionBasWare($branch);
        if(count($result) > 0){
            $response = response()->json(['status' => 'success' , 'message' => '', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Invalid warehouse details']); 
        }
        return $response;
    }

    // Get Region based Main Warehouse Details
    public function getRegionBasMainWare(Request $request)
    {
        $branch = new Branch();
        $branch->region_id = $request->input('region_id');
        
        $result = $branch->getRegionBasMainWare($branch);
        if(count($result) > 0){
            $response = response()->json(['status' => 'success' , 'message' => '', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Invalid warehouse details']); 
        }
        return $response;
    }
    // Get Region based Stock Request Warehouse Details
    public function getRegionBasStockRequestWare(Request $request)
    {
        $branch = new Branch();
        $branch->region_id = $request->input('region_id');
        $branch->warehouse_id = $request->input('warehouse_id');
        
        $result = $branch->getRegionBasStockRequestWare($branch);
        if(count($result) > 0){
            $response = response()->json(['status' => 'success' , 'message' => '', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Invalid warehouse details']); 
        }
        return $response;
    }
    
    // Get Branch based Region Details
    public function getBranchBaseReg(Request $request)
    {
        $branch = new Branch();
        $branch->branch_id = $request->input('branch_id');
        
        $result = $branch->getBranchBaseReg($branch);
        if(count($result) > 0){
            $response = response()->json(['status' => 'success' , 'message' => '', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Invalid Branch details']); 
        }
        return $response;
    }
    
    // Get Region based AREA Details
    public function getRegionBasArea(Request $request)
    {
        $branch = new Branch();
        $branch->region_id = $request->input('region_id');
        
        $result = $branch->getRegionBasArea($branch);
        if(count($result) > 0){
            $response = response()->json(['status' => 'success' , 'message' => '', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Invalid Area details']); 
        }
        return $response;
    }

    // Get Region based Stockist Details
    public function getRegionBasStockist(Request $request)
    {
        $branch = new Branch();
        $branch->region_id = $request->input('region_id');
        
        $result = $branch->getRegionBasStockist($branch);
        if(count($result) > 0){
            $response = response()->json(['status' => 'success' , 'message' => '', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'No Stockist Found']); 
        }
        return $response;
    }

    // Get Region based Stockist Details
    public function getRegionBasAvaiStockist(Request $request)
    {
        $branch = new Branch();
        $branch->region_id = $request->input('region_id');
        
        $result = $branch->getRegionBasAvaiStockist($branch);
        if(count($result) > 0){
            $response = response()->json(['status' => 'success' , 'message' => '', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'No Stockist Found']); 
        }
        return $response;
    }

    //New Calls For (May 27)
    //Branch AdminBased Details Fetch 
    public function getBranchAdminBaseDetails(Request $request)
    {
        $branch = new Branch();
        $branch->user_id = $request->input('user_id');
        $branch->vendor = $request->input('vendor');
        $branch->type = $request->input('type');
        
        $result = $branch->getBranchAdminBaseDetails($branch);
        if(count($result) > 0){
            $response = response()->json(['status' => 'success' , 'message' => '', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'No details Found','data' => $result]); 
        }
        return $response;
    }
    //Region Manager Based Details Fetch 
    public function getRegionMangeBaseDetails(Request $request)
    {
        $branch = new Branch();
        $branch->user_id = $request->input('user_id');
        $branch->vendor = $request->input('vendor');
        $branch->type = $request->input('type');

        if($request->input('start_date')){
            $branch->start_date = $request->input('start_date');
            $branch->end_date = $request->input('end_date');
        }else{
            $branch->start_date = "";
            $branch->end_date = "";
        }

        if($request->input('field_officer_id')){
            // print_r($request->field_officer_id);
            $branch->field_officer_id = $request->input('field_officer_id');
        }else{
            $branch->field_officer_id = "";
        }

        
        $result = $branch->getRegionMangeBaseDetails($branch);
        if(count($result) > 0){
            $response = response()->json(['status' => 'success' , 'message' => '', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Invalid details','data' => $result]); 
        }
        return $response;
    }
    //get Field Officer Based Area List
    public function getFOBaseAreaDetails(Request $request)
    {
        $branch = new Branch();
        $branch->user_id = $request->input('user_id');
        
        $result = $branch->getFOBaseAreaDetails($branch);
        if(count($result) > 0){
            $response = response()->json(['status' => 'success' , 'message' => '', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Invalid details','data' => $result]); 
        }
        return $response;
    }
    //get Area Manager Based Customer List
    public function getAreaManageBaseCusDetails(Request $request)
    {
        $branch = new Branch();
        $branch->user_id = $request->input('user_id');
        $branch->vendor  = $request->input('vendor');
        
        $result = $branch->getAreaManageBaseCusDetails($branch);
        if(count($result) > 0){
            $response = response()->json(['status' => 'success' , 'message' => '', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Invalid details','data' => $result]); 
        }
        return $response;
    }

    public function getAreaManageBaseVendorDetails(Request $request)
    {
        $branch = new Branch();
        $branch->user_id = $request->input('user_id');
        $branch->vendor  = $request->input('vendor');
        
        $result = $branch->getAreaManageBaseVendorDetails($branch);
        if(count($result) > 0){
            $response = response()->json(['status' => 'success' , 'message' => '', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Invalid details','data' => $result]); 
        }
        return $response;
    }

    public function getAreaManagerBasedStockistDetails(Request $request){
        $branch = new Branch();
        $branch->user_id = $request->input('user_id');
        $result = $branch->getAreaManagerBasedStockistDetails($branch);
        if(count($result)>0){
            $response = response()->json(['status' => 'success', 'message' => '', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure', 'message' => 'Invalid details', 'data'=> $result]);
        }
        return $response;
    }

    public function getAllVendors()
    {
        $branch = new Branch();

        $data = $branch->getAllVendors();
        if(count($data) > 0 ){
            $response = response()->json(['status' => 'success' , 'message' => 'Area Data retrived successfully','data'=>$data]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Error in Get details..','data'=>'null']);
        }
        return $response;
    } 
    // Get Area Manager based Warehouse Details
    public function getAreaManageBasWare(Request $request)
    {
        $branch = new Branch();
        $branch->user_id = $request->input('user_id');
        
        $result = $branch->getAreaManageBasWare($branch);
        if(count($result) > 0){
            $response = response()->json(['status' => 'success' , 'message' => '', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Invalid warehouse details']); 
        }
        return $response;
    }

    public function getRegionBasVendor(Request $request)
    {
        $branch = new Branch();
        $branch->area_id = $request->input('area_id');

        $result = $branch->getRegionBasVendor($branch);
        if($result){
            $response = response()->json(['status' => 'success' , 'message' => '', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Invalid vendor details']); 
        }
        return $response;
    }

    public function getAreaManagerBasedWarehouse(Request $request){
        $branch = new Branch();
        $branch->warehouse_cr_id = $request->input('warehouse_cr_id');
        $result = $branch->getAreaManagerBasedWarehouse($branch);
        if($result){
            $response = response()->json(['status'=>'success', 'message'=>'', 'data'=> $result]);
        }
        else{
            $response = response()->json(['status'=>'failure', 'message'=>'Invalid warehouse details']);
        }
        return $response;
    }

    //get Area Manager Based Customer List
    public function getAreaManageBaseCusDetailsRemarkBase(Request $request)
    {
        $branch = new Branch();
        $branch->user_id = $request->input('user_id');
        $branch->vendor  = $request->input('vendor');
        $branch->remarks_type = $request->input('remarks_type');
        
        $result = $branch->getAreaManageBaseCusDetailsRemarkBase($branch);
        if(count($result) > 0){
            $response = response()->json(['status' => 'success' , 'message' => '', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Invalid details','data' => $result]); 
        }
        return $response;
    }
    // Get Branch based Warehouse Details
    public function getBranchBasWare(Request $request)
    {
        $branch = new Branch();
        $branch->branch_id = $request->input('branch_id');
        
        $result = $branch->getBranchBasWare($branch);
        if(count($result) > 0){
            $response = response()->json(['status' => 'success' , 'message' => '', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Invalid warehouse details']); 
        }
        return $response;
    }
}
