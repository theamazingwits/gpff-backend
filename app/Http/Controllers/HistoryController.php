<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use File;
use Mail;

require(base_path().'/app/Http/Middleware/History.php');

class HistoryController extends Controller
{
////////////////////////////////
//History Management Api Calls//
////////////////////////////////
	// Get Stock Add History Details
    public function getStockAddHistory(Request $request)
    {
        $Historys = new History();
        $Historys->region_id 	= $request->input('region_id');
        $Historys->warehouse_id    = $request->input('warehouse_id');
        $Historys->type = $request->input('type');

        $result = $Historys->getStockAddHistory($Historys);
        if(count($result) > 0){
            $response = response()->json(['status' => 'success' , 'message' => '', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Invalid History details']); 
        }
        return $response;
    } 
    // Get Assign Stock History Details
    public function getStockAssignHistory(Request $request)
    {
        $Historys = new History();
        $Historys->id   = $request->input('id');
        $Historys->type = $request->input('type');

        $result = $Historys->getStockAssignHistory($Historys);
        if(count($result) > 0){
            $response = response()->json(['status' => 'success' , 'message' => '', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Invalid History details']); 
        }
        return $response;
    } 


}
