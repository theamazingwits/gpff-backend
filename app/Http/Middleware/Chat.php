<?php

namespace App\Http\Controllers;

use Closure;
use DB;
use Mail;
use Carbon\Carbon;

require(base_path().'/app/Http/Middleware/Common.php');

class Chat
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        return $next($request);
    }

    //////////////////////
    //Chating Api Calls//
    /////////////////////
    //Admin Side Chat
    public function getAdminChatDetails($Chats){
        
        $some_data = array(
                            'user_id' => $Chats->user_id
                        ); 

        $fields_string = http_build_query($some_data);
        $curl_handle = curl_init();
        
        curl_setopt($curl_handle, CURLOPT_POST, 1);
        
        curl_setopt($curl_handle,CURLOPT_URL,env('CHAT_URL'));
        curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl_handle, CURLOPT_POSTFIELDS, $fields_string);
        curl_setopt($curl_handle,CURLOPT_CONNECTTIMEOUT,2);
        curl_setopt($curl_handle,CURLOPT_RETURNTRANSFER,1);
        $buffer = curl_exec($curl_handle);
        curl_close($curl_handle);
    
        $buffer = json_decode($buffer);
        //print_r($buffer);exit;

        $send_id = [];
        $rece_id = [];
        if(!empty($buffer->data)){

            foreach ($buffer->data as $userdetails) {
                $send_id[] = $userdetails->senderUserId; 
                $rece_id[] = $userdetails->receiverId;    
            }

            $result_award = [];

            $details1 = DB::table('gpff_users')
                        ->whereIn('role',[2,3])
                        ->whereIn('user_id',$send_id)
                        ->orwhereIn('user_id',$rece_id)
                        ->where('user_id','!=',$Chats->user_id)
                        ->orderByRaw(\DB::raw("FIELD(user_id, ".implode(",",$rece_id).")"))
                        ->orderByRaw(\DB::raw("FIELD(user_id, ".implode(",",$send_id).")"))
                        ->groupBy('user_id')
                        ->get(['user_id','firstname','lastname','email','avatar','address','phone','country_code','country_flag','role']);

            $details2 = DB::table('gpff_users')
                        ->whereIn('role',[2,3])
                        ->whereNotIn('user_id',$send_id)
                        ->whereNotIn('user_id',$rece_id)
                        ->where('user_id','!=',$Chats->user_id)
                        ->orderBy('role')
                        ->groupBy('user_id')
                        ->get(['user_id','firstname','lastname','email','avatar','address','phone','country_code','country_flag','role']);

            $result_award = array_merge(json_decode($details1, true),json_decode($details2, true));
            $result = array_unique($result_award, SORT_REGULAR);

        } else{
            $Branchadmindetails =  DB::table('gpff_users')
                            ->where('role',2)
                            ->get(['user_id','firstname','lastname','email','avatar','address','phone','country_code','country_flag','role']);

            $RegionManagedetails =  DB::table('gpff_users')
                            ->where('role',3)
                            ->get(['user_id','firstname','lastname','email','avatar','address','phone','country_code','country_flag','role']);

        $result = array_merge(json_decode($Branchadmindetails, true),json_decode($RegionManagedetails, true));
        }
        return $result;
    }

    //Branch Admin Side Chat
    public function getBranchAdminChatDetails($Chats){
        
        $Adminid  = DB::table('gpff_users')
                      ->where('user_id', $Chats->user_id)
                      ->first();
        
        $some_data = array(
                            'user_id' => $Chats->user_id
                        ); 

        $fields_string = http_build_query($some_data);
        $curl_handle = curl_init();
        
        curl_setopt($curl_handle, CURLOPT_POST, 1);
        
        curl_setopt($curl_handle,CURLOPT_URL,env('CHAT_URL'));
        curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl_handle, CURLOPT_POSTFIELDS, $fields_string);
        curl_setopt($curl_handle,CURLOPT_CONNECTTIMEOUT,2);
        curl_setopt($curl_handle,CURLOPT_RETURNTRANSFER,1);
        $buffer = curl_exec($curl_handle);
        curl_close($curl_handle);
    
        $buffer = json_decode($buffer);
        //print_r($buffer);exit;

        $send_id = [];
        $rece_id = [];
        if(!empty($buffer->data)){

            foreach ($buffer->data as $userdetails) {
                $send_id[] = $userdetails->senderUserId; 
                $rece_id[] = $userdetails->receiverId;    
            }

            $result_award = [];

            $details1 =  DB::table('gpff_users')
                        ->orwhereIn('user_id',$send_id)
                        ->orwhereIn('user_id',$rece_id)
                        ->orderByRaw(\DB::raw("FIELD(user_id, ".implode(",",$send_id).implode(",",$rece_id).")"))
                        // /->orderByRaw(\DB::raw("FIELD(user_id, ".implode(",",$rece_id).")"))
                        ->where('user_id','!=',$Chats->user_id)
                        ->groupBy('user_id')
                        ->get(['user_id','firstname','lastname','email','avatar','address','phone','country_code','country_flag','role']);

            /*print_r($details1);
            exit;*/

            $details2 =  DB::table('gpff_users')
                            ->where('user_id',$Adminid->cr_by_id)
                            ->whereNotIn('user_id',$send_id)
                            ->whereNotIn('user_id',$rece_id)
                            ->get(['user_id','firstname','lastname','email','avatar','address','phone','country_code','country_flag','role']);
            
            $details3 =  DB::table('gpff_users')
                            ->where('user_id','!=',$Chats->user_id)
                            ->where('role',2)
                            ->whereNotIn('user_id',$send_id)
                            ->whereNotIn('user_id',$rece_id)
                            ->get(['user_id','firstname','lastname','email','avatar','address','phone','country_code','country_flag','role']);

            $details4 =  DB::table('gpff_users')
                            ->where('cr_by_id',$Chats->user_id)
                            ->where('role',3)
                            ->whereNotIn('user_id',$send_id)
                            ->whereNotIn('user_id',$rece_id)
                            ->get(['user_id','firstname','lastname','email','avatar','address','phone','country_code','country_flag','role']);

            $result_award = array_merge(json_decode($details1, true),json_decode($details2, true),json_decode($details3, true),json_decode($details4, true));
            $result = array_unique($result_award, SORT_REGULAR);

        } else{

            $Admindetails =  DB::table('gpff_users')
                            ->where('user_id',$Adminid->cr_by_id)
                            ->get(['user_id','firstname','lastname','email','avatar','address','phone','country_code','country_flag','role']);
            
            $OtherBranchdetails =  DB::table('gpff_users')
                            ->where('user_id','!=',$Chats->user_id)
                            ->where('role',2)
                            ->get(['user_id','firstname','lastname','email','avatar','address','phone','country_code','country_flag','role']);

            $RegionManagedetails =  DB::table('gpff_users')
                            ->where('cr_by_id',$Chats->user_id)
                            ->where('role',3)
                            ->get(['user_id','firstname','lastname','email','avatar','address','phone','country_code','country_flag','role']);

            $result = array_merge(json_decode($Admindetails, true),json_decode($OtherBranchdetails, true),json_decode($RegionManagedetails, true));
        }
        return $result;
    }

    //Region Admin Side Chat
    public function getRegionManagerChatDetails($Chats){
        
        $branchadminid  = DB::table('gpff_users')
                      ->where('user_id', $Chats->user_id)
                      ->first();

        $myArray = explode(',', $branchadminid->region_id);

        $some_data = array(
                            'user_id' => $Chats->user_id
                        ); 

        $fields_string = http_build_query($some_data);
        $curl_handle = curl_init();
        
        curl_setopt($curl_handle, CURLOPT_POST, 1);
        
        curl_setopt($curl_handle,CURLOPT_URL,env('CHAT_URL'));
        curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl_handle, CURLOPT_POSTFIELDS, $fields_string);
        curl_setopt($curl_handle,CURLOPT_CONNECTTIMEOUT,2);
        curl_setopt($curl_handle,CURLOPT_RETURNTRANSFER,1);
        $buffer = curl_exec($curl_handle);
        curl_close($curl_handle);
    
        $buffer = json_decode($buffer);
        //print_r($buffer);exit;

        $send_id = [];
        $rece_id = [];
        if(!empty($buffer->data)){

            foreach ($buffer->data as $userdetails) {
                $send_id[] = $userdetails->senderUserId; 
                $rece_id[] = $userdetails->receiverId;    
            }

            /*print_r($send_id);
            print_r($rece_id);
            exit;*/

            $result_award = [];

            $details1 =  DB::table('gpff_users')
                            ->whereIn('user_id',$send_id)
                            ->orwhereIn('user_id',$rece_id)
                            ->orderByRaw(\DB::raw("FIELD(user_id, ".implode(",",$rece_id).")"))
                            //->orderByRaw(\DB::raw("FIELD(user_id, ".implode(",",$rece_id).implode(",",$send_id).")"))
                            //->orderByRaw(\DB::raw("FIELD(user_id, ".implode(",",$send_id).")"))
                            ->where('user_id','!=',$Chats->user_id)
                            ->groupBy('user_id')
                            ->get(['user_id','firstname','lastname','email','avatar','address','phone','country_code','country_flag','role']);

            /*print_r($details1);
            exit;*/
            $Branchadmindetails =  DB::table('gpff_users')
                            ->where('user_id',$branchadminid->cr_by_id)
                            ->whereNotIn('user_id',$send_id)
                            ->whereNotIn('user_id',$rece_id)
                            ->get(['user_id','firstname','lastname','email','avatar','address','phone','country_code','country_flag','role']);

            $OtherRegionManagedetails =  DB::table('gpff_users')
                            ->where('user_id','!=',$Chats->user_id)
                            ->where('role',3)
                            ->whereNotIn('user_id',$send_id)
                            ->whereNotIn('user_id',$rece_id)
                            ->get(['user_id','firstname','lastname','email','avatar','address','phone','country_code','country_flag','role']);
            
            $AreaManagedetails =  DB::table('gpff_users')
                         ->whereIn('region_id', $myArray)
                         ->where('role',4)
                         ->whereNotIn('user_id',$send_id)
                         ->whereNotIn('user_id',$rece_id)
                         ->get(['user_id','firstname','lastname','email','avatar','address','phone','country_code','country_flag','role']);

            $FOdetails =  DB::table('gpff_users')
                            ->whereIn('region_id', $myArray)
                            ->where('role',5)
                            ->whereNotIn('user_id',$send_id)
                            ->whereNotIn('user_id',$rece_id)
                            ->get(['user_id','firstname','lastname','email','avatar','address','phone','country_code','country_flag','role']);

            $Stockistdetails =  DB::table('gpff_users')
                            ->whereIn('region_id', $myArray)
                            ->where('role',7)
                            ->whereNotIn('user_id',$send_id)
                            ->whereNotIn('user_id',$rece_id)
                            ->get(['user_id','firstname','lastname','email','avatar','address','phone','country_code','country_flag','role']);

            $result_award = array_merge(json_decode($details1, true),json_decode($Branchadmindetails, true),json_decode($OtherRegionManagedetails, true),json_decode($AreaManagedetails, true),json_decode($FOdetails, true),json_decode($Stockistdetails, true));
            $result = array_unique($result_award, SORT_REGULAR);

        } else{
            $Branchadmindetails =  DB::table('gpff_users')
                            ->where('user_id',$branchadminid->cr_by_id)
                            ->get(['user_id','firstname','lastname','email','avatar','address','phone','country_code','country_flag','role']);

            $OtherRegionManagedetails =  DB::table('gpff_users')
                            ->where('user_id','!=',$Chats->user_id)
                            ->where('role',3)
                            ->get(['user_id','firstname','lastname','email','avatar','address','phone','country_code','country_flag','role']);
            
            $AreaManagedetails =  DB::table('gpff_users')
                         ->where('cr_by_id',$Chats->user_id)
                         ->where('role',4)
                         ->get(['user_id','firstname','lastname','email','avatar','address','phone','country_code','country_flag','role']);

            $FOdetails =  DB::table('gpff_users')
                            ->whereIn('region_id', $myArray)
                            ->where('role',5)
                            ->get(['user_id','firstname','lastname','email','avatar','address','phone','country_code','country_flag','role']);

            $Stockistdetails =  DB::table('gpff_users')
                            ->whereIn('region_id', $myArray)
                            ->where('role',7)
                            ->get(['user_id','firstname','lastname','email','avatar','address','phone','country_code','country_flag','role']);

            $result = array_merge(json_decode($Branchadmindetails, true),json_decode($OtherRegionManagedetails, true),json_decode($AreaManagedetails, true),json_decode($FOdetails, true),json_decode($Stockistdetails, true));
        }
        return $result;
    }

    //Area Manager Side Chat
    public function getManagerChatDetails($Chats){
        
        $regionmanid  = DB::table('gpff_users')
                      ->where('user_id', $Chats->user_id)
                      ->first();

        $some_data = array(
                            'user_id' => $Chats->user_id
                        ); 

        $fields_string = http_build_query($some_data);
        $curl_handle = curl_init();
        
        curl_setopt($curl_handle, CURLOPT_POST, 1);
        
        curl_setopt($curl_handle,CURLOPT_URL,env('CHAT_URL'));
        curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl_handle, CURLOPT_POSTFIELDS, $fields_string);
        curl_setopt($curl_handle,CURLOPT_CONNECTTIMEOUT,2);
        curl_setopt($curl_handle,CURLOPT_RETURNTRANSFER,1);
        $buffer = curl_exec($curl_handle);
        curl_close($curl_handle);
    
        $buffer = json_decode($buffer);
        //print_r($buffer);exit;

        $send_id = [];
        $rece_id = [];
        $createdAt = [];
        if(!empty($buffer->data)){

            foreach ($buffer->data as $userdetails) {
                $send_id[] = $userdetails->senderUserId; 
                $rece_id[] = $userdetails->receiverId;
                $createdAt[] = $userdetails->createdAt;    
            }

            /*print_r($send_id);
            print_r($rece_id);
            print_r($createdAt);
            exit;*/

            $result_award = [];

            $details1 =  DB::table('gpff_users')
                            ->whereIn('user_id',$send_id)
                            ->orwhereIn('user_id',$rece_id)
                            ->orderByRaw(\DB::raw("FIELD(user_id, ".implode(",",$rece_id).")"))
                            ->orderByRaw(\DB::raw("FIELD(user_id, ".implode(",",$send_id).")"))
                            ->where('user_id','!=',$Chats->user_id)
                            ->groupBy('user_id')
                            ->get(['user_id','firstname','lastname','email','avatar','address','phone','country_code','country_flag','role']);

           /* print_r($details1);
            exit;*/


            $regionmanagerdetails =  DB::table('gpff_users')
                            ->where('region_id',$regionmanid->region_id)
                            ->whereNotIn('user_id',$send_id)
                            ->whereNotIn('user_id',$rece_id)
                            ->where('role',3)
                            ->get(['user_id','firstname','lastname','email','avatar','address','phone','country_code','country_flag','role']);
            
            $otherAreaManagedetails =  DB::table('gpff_users')
                         ->where('region_id',$regionmanid->region_id)
                         ->where('user_id','!=',$Chats->user_id)
                         ->where('role',4)
                         ->whereNotIn('user_id',$send_id)
                         ->whereNotIn('user_id',$rece_id)
                         ->get(['user_id','firstname','lastname','email','avatar','address','phone','country_code','country_flag','role']);

            $myArray = explode(',', $regionmanid->area_id);

            $FOdetails =  DB::table('gpff_users')
                            //->whereIn('area_id', $myArray)
                            ->where('area_manager_id',$Chats->user_id)
                            ->where('role',5)
                            ->whereNotIn('user_id',$send_id)
                            ->whereNotIn('user_id',$rece_id)
                            ->get(['user_id','firstname','lastname','email','avatar','address','phone','country_code','country_flag','role']);

            $Stockistdetails =  DB::table('gpff_users')
                            ->where('region_id',$regionmanid->region_id)
                            ->where('role',7)
                            ->whereNotIn('user_id',$send_id)
                            ->whereNotIn('user_id',$rece_id)
                            ->get(['user_id','firstname','lastname','email','avatar','address','phone','country_code','country_flag','role']);
                    
            $result_award = array_merge(json_decode($details1, true),json_decode($regionmanagerdetails, true),json_decode($otherAreaManagedetails, true),json_decode($FOdetails, true),json_decode($Stockistdetails, true));
            $result = array_unique($result_award, SORT_REGULAR);

        } else{

            $regionmanagerdetails =  DB::table('gpff_users')
                            ->where('region_id',$regionmanid->region_id)
                            ->where('role',3)
                            ->get(['user_id','firstname','lastname','email','avatar','address','phone','country_code','country_flag','role']);
            
            $otherAreaManagedetails =  DB::table('gpff_users')
                         ->where('region_id',$regionmanid->region_id)
                         ->where('user_id','!=',$Chats->user_id)
                         ->where('role',4)
                         ->get(['user_id','firstname','lastname','email','avatar','address','phone','country_code','country_flag','role']);

            $myArray = explode(',', $regionmanid->area_id);

            $FOdetails =  DB::table('gpff_users')
                            ->where('area_manager_id',$Chats->user_id)
                            //->whereIn('area_id', $myArray)
                            ->where('role',5)
                            ->get(['user_id','firstname','lastname','email','avatar','address','phone','country_code','country_flag','role']);

            $Stockistdetails =  DB::table('gpff_users')
                            ->where('region_id',$regionmanid->region_id)
                            ->where('role',7)
                            ->get(['user_id','firstname','lastname','email','avatar','address','phone','country_code','country_flag','role']);

            $result = array_merge(json_decode($regionmanagerdetails, true),json_decode($otherAreaManagedetails, true),json_decode($FOdetails, true),json_decode($Stockistdetails, true));
        }
        return $result;
    }

    //FieldOfficer Side Chat
    public function fetchFOChatDetails($Chats){
        
        $managerid  = DB::table('gpff_users')
                      ->where('user_id', $Chats->user_id)
                      ->first();
        $regmanagerid  = DB::table('gpff_users')
                      ->where('user_id', $managerid->cr_by_id)
                      ->first();

        $some_data = array(
                            'user_id' => $Chats->user_id
                        ); 

        $fields_string = http_build_query($some_data);
        $curl_handle = curl_init();
        
        curl_setopt($curl_handle, CURLOPT_POST, 1);
        
        curl_setopt($curl_handle,CURLOPT_URL,env('CHAT_URL'));
        curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl_handle, CURLOPT_POSTFIELDS, $fields_string);
        curl_setopt($curl_handle,CURLOPT_CONNECTTIMEOUT,2);
        curl_setopt($curl_handle,CURLOPT_RETURNTRANSFER,1);
        $buffer = curl_exec($curl_handle);
        curl_close($curl_handle);
    
        $buffer = json_decode($buffer);
        //print_r($buffer);exit;
        
        $send_id = [];
        $rece_id = [];
        if(!empty($buffer->data)){

            foreach ($buffer->data as $userdetails) {
                $send_id[] = $userdetails->senderUserId; 
                $rece_id[] = $userdetails->receiverId;    
            }

            $result_award = [];

           /* print_r($send_id);
            print_r($rece_id);
            exit;*/

            $details1 =  DB::table('gpff_users')
                            ->whereIn('role',[3,4,7])
                            ->whereIn('user_id',$send_id)
                            ->orwhereIn('user_id',$rece_id)
                            ->orderByRaw(\DB::raw("FIELD(user_id, ".implode(",",$rece_id).")"))
                            //->orderByRaw(\DB::raw("FIELD(user_id, ".implode(",",$send_id).")"))
                            ->where('user_id','!=',$Chats->user_id)
                            ->groupBy('user_id')
                            ->get(['user_id','firstname','lastname','email','avatar','address','phone','country_code','country_flag','role']);
            /*print_r($details1);
            exit;*/

            $regionmanagerdetails =  DB::table('gpff_users')
                            ->where('region_id',$managerid->region_id)
                            ->where('role',3)
                            ->whereNotIn('user_id',$send_id)
                            ->whereNotIn('user_id',$rece_id)
                            ->get(['user_id','firstname','lastname','email','avatar','address','phone','country_code','country_flag','role']);

            $manager_details =  DB::table('gpff_users')
                            ->where('user_id',$managerid->area_manager_id)
                            ->where('role',4)
                            ->whereNotIn('user_id',$send_id)
                            ->whereNotIn('user_id',$rece_id)
                            ->get(['user_id','firstname','lastname','email','avatar','address','phone','country_code','country_flag','role']);

            $stockist_details =  DB::table('gpff_users')
                            ->where('region_id',$managerid->region_id)
                            ->where('role',7)
                            ->whereNotIn('user_id',$send_id)
                            ->whereNotIn('user_id',$rece_id)
                            ->get(['user_id','firstname','lastname','email','avatar','address','phone','country_code','country_flag','role']);

            $result = array_merge(json_decode($details1, true),json_decode($regionmanagerdetails, true),json_decode($manager_details, true),json_decode($stockist_details, true));
            $result = array_unique($result, SORT_REGULAR);

        } else{
            $regionmanagerdetails =  DB::table('gpff_users')
                            ->where('region_id',$managerid->region_id)
                            ->where('role',3)
                            ->get(['user_id','firstname','lastname','email','avatar','address','phone','country_code','country_flag','role']);

            $manager_details =  DB::table('gpff_users')
                            ->where('user_id',$managerid->area_manager_id)
                            ->where('role',4)
                            ->get(['user_id','firstname','lastname','email','avatar','address','phone','country_code','country_flag','role']);

            $stockist_details =  DB::table('gpff_users')
                            ->where('region_id',$managerid->region_id)
                            ->where('role',7)
                            ->get(['user_id','firstname','lastname','email','avatar','address','phone','country_code','country_flag','role']);

            $result = array_merge(json_decode($regionmanagerdetails, true),json_decode($manager_details, true),json_decode($stockist_details, true));
        }
        return $result;
    }

    //Stockist Side Chat details fetch
    public function getStockistChatDetails($Chats){
        
        $managerid  = DB::table('gpff_users')
                      ->where('user_id', $Chats->user_id)
                      ->first();
        $regmanagerid  = DB::table('gpff_users')
                      ->where('user_id', $managerid->cr_by_id)
                      ->first();
    
        $some_data = array(
                            'user_id' => $Chats->user_id
                        ); 

        $fields_string = http_build_query($some_data);
        $curl_handle = curl_init();
        
        curl_setopt($curl_handle, CURLOPT_POST, 1);
        
        curl_setopt($curl_handle,CURLOPT_URL,env('CHAT_URL'));
        curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl_handle, CURLOPT_POSTFIELDS, $fields_string);
        curl_setopt($curl_handle,CURLOPT_CONNECTTIMEOUT,2);
        curl_setopt($curl_handle,CURLOPT_RETURNTRANSFER,1);
        $buffer = curl_exec($curl_handle);
        curl_close($curl_handle);
    
        $buffer = json_decode($buffer);
       
        $send_id = [];
        $rece_id = [];
        if(!empty($buffer->data)){

            foreach ($buffer->data as $userdetails) {
                $send_id[] = $userdetails->senderUserId; 
                $rece_id[] = $userdetails->receiverId;    
            }
            $result_award = [];

            $details1 =  DB::table('gpff_users')
                            ->whereIn('role',[3,4,5])
                            ->whereIn('user_id',$send_id)
                            ->orwhereIn('user_id',$rece_id)
                            ->orderByRaw(\DB::raw("FIELD(user_id, ".implode(",",$rece_id).")"))
                            //->orderByRaw(\DB::raw("FIELD(user_id, ".implode(",",$send_id).")"))
                            ->where('user_id','!=',$Chats->user_id)
                            ->groupBy('user_id')
                            ->get(['user_id','firstname','lastname','email','avatar','address','phone','country_code','country_flag','role']);
            

            $regionmanagerdetails =  DB::table('gpff_users')
                            ->where('region_id',$managerid->region_id)
                            ->where('role',3)
                            ->whereNotIn('user_id',$send_id)
                            ->whereNotIn('user_id',$rece_id)
                            ->get(['user_id','firstname','lastname','email','avatar','address','phone','country_code','country_flag','role']);

            $manager_details =  DB::table('gpff_users')
                            ->where('region_id',$managerid->region_id)
                            ->where('role',4)
                            ->whereNotIn('user_id',$send_id)
                            ->whereNotIn('user_id',$rece_id)
                            ->get(['user_id','firstname','lastname','email','avatar','address','phone','country_code','country_flag','role']);

            $FO_details =  DB::table('gpff_users')
                            ->where('region_id',$managerid->region_id)
                            ->where('role',5)
                            ->whereNotIn('user_id',$send_id)
                            ->whereNotIn('user_id',$rece_id)
                            ->get(['user_id','firstname','lastname','email','avatar','address','phone','country_code','country_flag','role']);

            $result = array_merge(json_decode($details1, true),json_decode($regionmanagerdetails, true),json_decode($manager_details, true),json_decode($FO_details, true));
            $result = array_unique($result, SORT_REGULAR);

        } else{

            /*print_r($managerid->region_id);
            exit;*/
            
            $regionmanagerdetails =  DB::table('gpff_users')
                            ->where('region_id',$managerid->region_id)
                            ->where('role',3)
                            ->get(['user_id','firstname','lastname','email','avatar','address','phone','country_code','country_flag','role']);

            $manager_details =  DB::table('gpff_users')
                            ->where('region_id',$managerid->region_id)
                            ->where('role',4)
                            ->get(['user_id','firstname','lastname','email','avatar','address','phone','country_code','country_flag','role']);

            $FO_details =  DB::table('gpff_users')
                            ->where('region_id',$managerid->region_id)
                            ->where('role',5)
                            ->get(['user_id','firstname','lastname','email','avatar','address','phone','country_code','country_flag','role']);

            $result = array_merge(json_decode($regionmanagerdetails, true),json_decode($manager_details, true),json_decode($FO_details, true));
        }
        return $result;
    }

    //Customer Side Chat details fetch
    public function getCustomerChatDetails($Chats){
        
        $customerid  = DB::table('gpff_users')
                      ->where('user_id', $Chats->user_id)
                      ->first();

       /* $regmanagerid  = DB::table('gpff_users')
                      ->where('user_id', $managerid->cr_by_id)
                      ->first();*/
    
        $some_data = array(
                            'user_id' => $Chats->user_id
                        ); 

        $fields_string = http_build_query($some_data);
        $curl_handle = curl_init();
        
        curl_setopt($curl_handle, CURLOPT_POST, 1);
        
        curl_setopt($curl_handle,CURLOPT_URL,env('CHAT_URL'));
        curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl_handle, CURLOPT_POSTFIELDS, $fields_string);
        curl_setopt($curl_handle,CURLOPT_CONNECTTIMEOUT,2);
        curl_setopt($curl_handle,CURLOPT_RETURNTRANSFER,1);
        $buffer = curl_exec($curl_handle);
        curl_close($curl_handle);
    
        $buffer = json_decode($buffer);
       
        $send_id = [];
        $rece_id = [];
        if(!empty($buffer->data)){

            foreach ($buffer->data as $userdetails) {
                $send_id[] = $userdetails->senderUserId; 
                $rece_id[] = $userdetails->receiverId;    
            }
            $result_award = [];

            $details1 =  DB::table('gpff_users')
                            ->whereIn('role',[4,5])
                            ->whereIn('user_id',$send_id)
                            ->orwhereIn('user_id',$rece_id)
                            ->orderByRaw(\DB::raw("FIELD(user_id, ".implode(",",$rece_id).")"))
                            //->orderByRaw(\DB::raw("FIELD(user_id, ".implode(",",$send_id).")"))
                            ->where('user_id','!=',$Chats->user_id)
                            ->groupBy('user_id')
                            ->get(['user_id','firstname','lastname','email','avatar','address','phone','country_code','country_flag','role']);
            

            /*$regionmanagerdetails =  DB::table('gpff_users')
                            ->where('region_id',$managerid->region_id)
                            ->where('role',3)
                            ->whereNotIn('user_id',$send_id)
                            ->whereNotIn('user_id',$rece_id)
                            ->get(['user_id','firstname','lastname','email','avatar','address','phone','country_code','country_flag','role']);*/

            $manager_details =  DB::table('gpff_users')
                            ->where('region_id',$managerid->region_id)
                            ->where('role',4)
                            ->whereNotIn('user_id',$send_id)
                            ->whereNotIn('user_id',$rece_id)
                            ->get(['user_id','firstname','lastname','email','avatar','address','phone','country_code','country_flag','role']);

            $FO_details =  DB::table('gpff_users')
                            ->where('region_id',$managerid->region_id)
                            ->where('role',5)
                            ->whereNotIn('user_id',$send_id)
                            ->whereNotIn('user_id',$rece_id)
                            ->get(['user_id','firstname','lastname','email','avatar','address','phone','country_code','country_flag','role']);

            $result = array_merge(json_decode($details1, true),json_decode($manager_details, true),json_decode($FO_details, true));
            $result = array_unique($result, SORT_REGULAR);

        } else{

            /*$regionmanagerdetails =  DB::table('gpff_users')
                            ->where('region_id',$managerid->region_id)
                            ->where('role',3)
                            ->get(['user_id','firstname','lastname','email','avatar','address','phone','country_code','country_flag','role']);*/

            $manager_details =  DB::table('gpff_users')
                            ->where('region_id',$customerid->region_id)
                            ->where('role',4)
                            ->get(['user_id','firstname','lastname','email','avatar','address','phone','country_code','country_flag','role']);

            $FO_details =  DB::table('gpff_users')
                            ->where('region_id',$customerid->region_id)
                            ->where('role',5)
                            ->get(['user_id','firstname','lastname','email','avatar','address','phone','country_code','country_flag','role']);

            $result = array_merge(json_decode($manager_details, true),json_decode($FO_details, true));
        }
        return $result;
    }
}
