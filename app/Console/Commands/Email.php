<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use Mail;
class Email extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'EmailSend:Notification';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Automated Email Notification';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //Reeport Mail Send
        $result = DB::table('gpff_email_queue')
                ->where('is_sent',0)
                ->get();

        if(count($result)){

            foreach ($result as $data) {

            echo "**********";
            print_r(json_decode($data->email_template_data));
            print_r(json_decode($data->email_template));
            print_r(json_decode($data->email_to));
            echo "########";
                
                Mail::send($data->email_template,json_decode($data->email_template_data, true), function($message) use($data) {

                    $message->to($data->email_to)->subject($data->email_subject);
              
                    $message->from('globaltarget18@gmail.com','GPFF');

                    if(!empty($data->email_file_name)){
                
                        $message->attach($data->email_file_path, [
                            'as' => $data->email_file_name, 
                            'mime' => 'application/pdf'
                        ]);
                    }
                });

               
                 DB::table('gpff_email_queue')
                    ->where('id', $data->id)
                    ->update([
                        'is_sent' => 1, 
                        'updated_date' => date('Y-m-d H:i:s')
                        ]);

            }
        }



    }
}
