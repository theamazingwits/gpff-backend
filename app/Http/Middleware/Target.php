<?php

namespace App\Http\Controllers;

use Closure;
use DB;
use Carbon\Carbon;

require(base_path().'/app/Http/Middleware/Common.php');

class Target
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        return $next($request);
    }
//Target Assign In the Table 
     //Store Task Details 
    //1="Target Based", 2="unit based", 3= "percentage based"
 public function assignTarget($Targets){

        $userrole = DB::table('gpff_users')
                ->where('user_id',$Targets->created_by)
                ->First();
        if($Targets->insentive_type == 2){
            
            foreach ($Targets->product_details as $value) {
                $product = DB::table('gpff_product')
                           ->where('product_id',$value['product_id'])
                           ->first();
                $values = array(
                'fo_id'             => $Targets->fo_id , 
                'fo_name'           => $Targets->fo_name ,
                'created_by'        => $Targets->created_by ,
                'insentive_type'    => $Targets->insentive_type ,
                'product_id'        => $value['product_id'],
                'product_name'      => $product->product_genericname,
                'unit'              => $value['unit'],
                'insentive_amt'     => $value['insentive_amt'],
                'target_amt'        => $Targets->target_amt ,
                'from_date'         => $Targets->from_date ,
                'to_date'           => $Targets->to_date ,
                'branch_id'         => $Targets->branch_id , 
                'region_id'         => $Targets->region_id , 
                // 'area_id'           => implode("," ,$Targets->area_id),

                'area_id'           => $Targets->area_id , 
                'created_at'        => date('Y-m-d H:i:s') , 
                'updated_at'        => date('Y-m-d H:i:s')
                );

                DB::table('gpff_insentive')
                    ->insert($values);            
            }
        }else if($Targets->insentive_type == 1){
            $tg = $Targets->target_amt;
            $tg1 = (80 / 100) * $tg;
            $tg2 = (120 / 100) * $tg;
            $target_amt1 = $tg1 ;
            $insentive_amt1 = 1.5;
            $target_amt2 = $Targets->target_amt;
            $insentive_amt2 = 1.8;
            $target_amt3 = $tg2 ;
            $insentive_amt3 = 2;

            for ($i=0; $i <3 ; $i++) { 
                if($i == 0){
                    $target_amt = $target_amt1;
                    $insentive_amt = $insentive_amt1;
                }else if($i == 1){
                    $target_amt = $target_amt2;
                    $insentive_amt = $insentive_amt2;
                }else{
                    $target_amt = $target_amt3;
                    $insentive_amt = $insentive_amt3;
                }
                $values = array(
                'fo_id'             => $Targets->fo_id , 
                'fo_name'           => $Targets->fo_name ,
                'created_by'        => $Targets->created_by ,
                'insentive_type'    => $Targets->insentive_type ,
                'product_id'        => $Targets->product_id ,
                'target_amt'        => $target_amt ,
                'insentive_amt'     => $insentive_amt,
                'from_date'         => $Targets->from_date ,
                'to_date'           => $Targets->to_date ,
                'branch_id'         => $Targets->branch_id , 
                'region_id'         => $Targets->region_id , 
                'area_id'           => $Targets->area_id , 
                'insentive_status'  => 1 ,
                'created_at'        => date('Y-m-d H:i:s') , 
                'updated_at'        => date('Y-m-d H:i:s')
                );

                DB::table('gpff_insentive')
                ->insert($values);   
            }
        }else if($Targets->insentive_type == 3){

            $target_amt1 = $Targets->target_amt;
            $insentive_amt1 = 0.25;
            $target_amt2 = $Targets->target_amt + 1000000;
            $insentive_amt2 = 0.6;

            for ($i=0; $i < 2 ; $i++) {
                if($i == 0){
                    $target_amt = $target_amt1;
                    $insentive_amt = $insentive_amt1;
                }else{
                    $target_amt = $target_amt2;
                    $insentive_amt = $insentive_amt2;
                }
                $values = array(
                'fo_id'             => $Targets->fo_id , 
                'fo_name'           => $Targets->fo_name ,
                'created_by'        => $Targets->created_by ,
                'insentive_type'    => $Targets->insentive_type ,
                'product_id'        => $Targets->product_id ,
                'target_amt'        => $target_amt ,
                'insentive_amt'     => $insentive_amt,
                'from_date'         => $Targets->from_date ,
                'to_date'           => $Targets->to_date ,
                'branch_id'         => $Targets->branch_id , 
                'region_id'         => $Targets->region_id , 
                'area_id'           => $Targets->area_id , 
                'insentive_status'  => 1 ,
                'created_at'        => date('Y-m-d H:i:s') , 
                'updated_at'        => date('Y-m-d H:i:s')
                );

                DB::table('gpff_insentive')
                    ->insert($values);
            }
        }  
        //Start Notification Entry
        if($userrole->role == 2){
            $message = "Your Branch Admin ".$userrole->firstname."Has Assign Some Target To you.";
        } else if($userrole->role == 3){
            $message = "Your Region Manager ".$userrole->firstname."Has Assign Some Target To you.";
        } else if($userrole->role == 4){
            $message = "Your Manager ".$userrole->firstname." Has Assign Some Target To you.";
        }
        $page_id = 'UNKNOWN';
        $cmn = new Common();
        //$cmn->insertNotification($Targets->target_ass_userid,$userrole->firstname,$Targets->created_by,$message,$page_id);
        //End Notification Entry

        return 1;
    }

    //Delete user details
    public function removeTarget($Targets)
    {
        return  DB::table('gpff_insentive')
                ->where('insentive_id', $Targets->insentive_id)
                ->delete();
    }

    //Fetch Target Details Based On the Users
    public function getMyTargetDetails($Targets){
        if($Targets->target_status == 3){
            return  DB::table('gpff_insentive as gpinc')
                ->leftjoin('gpff_branch as gpbra','gpinc.branch_id','gpbra.branch_id')
                ->leftjoin('gpff_region as gpreg','gpinc.region_id','gpreg.region_id')
                ->where('gpinc.fo_id', $Targets->user_id)
                ->orderBy('gpinc.updated_at','DESC')
                ->get(['gpinc.insentive_id','gpinc.fo_id','gpinc.fo_name','gpinc.created_by','gpinc.insentive_type','gpinc.product_id','gpinc.product_name','gpinc.unit','gpinc.insentive_amt','gpinc.target_amt','gpinc.from_date','gpinc.to_date','gpinc.branch_id','gpbra.branch_name','gpreg.region_name','gpinc.region_id','gpinc.insentive_status']);
        }else{
            return  DB::table('gpff_insentive as gpinc')
                ->leftjoin('gpff_branch as gpbra','gpinc.branch_id','gpbra.branch_id')
                ->leftjoin('gpff_region as gpreg','gpinc.region_id','gpreg.region_id')
                ->where('gpinc.fo_id', $Targets->user_id)
                ->where('gpinc.insentive_status', $Targets->target_status)
                ->orderBy('gpinc.updated_at','DESC')
                ->get(['gpinc.insentive_id','gpinc.fo_id','gpinc.fo_name','gpinc.created_by','gpinc.insentive_type','gpinc.product_id','gpinc.product_name','gpinc.unit','gpinc.insentive_amt','gpinc.target_amt','gpinc.from_date','gpinc.to_date','gpinc.branch_id','gpbra.branch_name','gpreg.region_name','gpinc.region_id','gpinc.insentive_status']);
        }
    }
    //Fetch Target Details Based On the Assigned Users
    public function getAssignToTargetDetails($Targets){
        if($Targets->target_status == 3){
            return  DB::table('gpff_insentive as gpinc')
                ->leftjoin('gpff_branch as gpbra','gpinc.branch_id','gpbra.branch_id')
                ->leftjoin('gpff_region as gpreg','gpinc.region_id','gpreg.region_id')
                ->where('gpinc.created_by', $Targets->user_id)
                ->orderBy('gpinc.updated_at','DESC')
                ->get(['gpinc.insentive_id','gpinc.fo_id','gpinc.fo_name','gpinc.created_by','gpinc.insentive_type','gpinc.product_id','gpinc.product_name','gpinc.unit','gpinc.insentive_amt','gpinc.target_amt','gpinc.from_date','gpinc.to_date','gpinc.branch_id','gpbra.branch_name','gpreg.region_name','gpinc.region_id','gpinc.insentive_status']);
        }else{
            return  DB::table('gpff_insentive as gpinc')
                ->leftjoin('gpff_branch as gpbra','gpinc.branch_id','gpbra.branch_id')
                ->leftjoin('gpff_region as gpreg','gpinc.region_id','gpreg.region_id')
                ->where('gpinc.created_by', $Targets->user_id)
                ->where('gpinc.insentive_status', $Targets->target_status)
                ->orderBy('gpinc.updated_at','DESC')
                ->get(['gpinc.insentive_id','gpinc.fo_id','gpinc.fo_name','gpinc.created_by','gpinc.insentive_type','gpinc.product_id','gpinc.product_name','gpinc.unit','gpinc.insentive_amt','gpinc.target_amt','gpinc.from_date','gpinc.to_date','gpinc.branch_id','gpbra.branch_name','gpreg.region_name','gpinc.region_id','gpinc.insentive_status']);
        }
    }

    //Fetch Incentive Target Details 
    public function getIncentiveTargetDetails($Targets){
        $incentive_data = array();
        $incentive_data = DB::table('gpff_insentive as gpins')
                        ->join('gpff_branch as gpbra','gpins.branch_id','gpbra.branch_id')
                        ->join('gpff_region as gpreg','gpins.region_id','gpreg.region_id')
                        ->where('gpins.created_by', $Targets->user_id)
                        ->orderBy('gpins.updated_at','DESC')
                        ->get(['gpins.insentive_id', 'gpins.insentive_type','gpins.fo_id','gpins.fo_name', 'gpins.product_id',
                        'gpins.unit', 'gpins.insentive_amt', 'gpins.target_amt', 'gpins.from_date', 'gpins.to_date', 'gpins.achived_at_incentive_amt',
                        'gpins.achived_unit', 'gpins.incentive_config', 'gpins.incentive_desc', 'gpins.insentive_status',
                        'gpins.branch_id', 'gpins.region_id', 'gpins.area_id', 'gpins.created_at','gpins.updated_at','gpbra.branch_name',
                        'gpreg.region_name', 'gpins.insentive_type as target_type']);

        foreach ($incentive_data as $key => $value) {
            if($value->product_id == 'all') {
                $incentive_data[$key]->product_name = ['All'];
            } else {
                $incentive_data[$key]->product_name = DB::table('gpff_product')
                                ->where('product_id', $value->product_id)
                                ->get()
                                ->pluck('product_genericname');
            }
            if($value->insentive_type == '1') {
                $incentive_data[$key]->insentive_type = 'Target Based';
            } else if($value->insentive_type == '2') {
                $incentive_data[$key]->insentive_type = 'Unit Based';
            } else {
                $incentive_data[$key]->insentive_type = 'Percentage Based';
            }
        }

        return $incentive_data;
    }

    //Fetch Previous Incentive Target Details 
    public function getPreviousTargetDetails($Targets){

        $incentive_data = DB::table('gpff_insentive')
                        ->where('fo_id',$Targets->user_id)
                        ->where('insentive_type', $Targets->type)
                        ->whereMonth('from_date', '=', Carbon::now()->subMonth()->month)
                        ->get();
        if(count($incentive_data) == 0){
            $incentive_data = DB::table('gpff_insentive')
                        ->where('fo_id',$Targets->user_id)
                        ->where('insentive_type', $Targets->type)
                        ->whereMonth('from_date', '<=', Carbon::now()->subMonth()->month)
                        ->whereMonth('to_date', '>=', Carbon::now()->subMonth()->month)
                        ->get();
        }

        return $incentive_data;
    }

     //Delete user details
     public function deleteIncentiveTarget($Targets)
     {
         return  DB::table('gpff_insentive')
                 ->where('insentive_id', $Targets->insentive_id)
                 ->delete();
     }

    

    
}
