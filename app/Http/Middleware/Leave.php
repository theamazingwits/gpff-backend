<?php

namespace App\Http\Controllers;

use Closure;
use DB;
use Mail;
use Carbon\Carbon;

require(base_path().'/app/Http/Middleware/Common.php');

class Leave
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        return $next($request);
    }

    //Field Officer Add Our Leave Application
    public function addLeave($Leaves) 
    {  
        $values = array(
            'user_id'           => $Leaves->user_id , 
            'leave_start_date'  => $Leaves->leave_start_date ,
            'leave_end_date'    => $Leaves->leave_end_date , 
            'leave_detaile'     => $Leaves->leave_detaile ,
            'area_manager_id'   => $Leaves->area_manager_id ,
            'area_id'           => $Leaves->area_id ,
            'leave_type'        => $Leaves->leave_type ,
            'leave_status'      => 0,
            'created_at'        => date('Y-m-d H:i:s') , 
            'updated_at'        => date('Y-m-d H:i:s')
        );
          DB::table('gpff_leave')
                ->insert($values);

        //Notification Entry
        $name = DB::table('gpff_users')
                ->where('user_id', $Leaves->user_id)
                ->First();
            
            $message = "Field Officer ".$name->firstname." has request for the leave Application.";
            $page_id = 'UNKNOWN';
            $cmn = new Common();
            $cmn->insertNotification($Leaves->user_id,$name->firstname,$Leaves->area_manager_id,$message,$page_id);
        //End Notification Entry
        return 1;
    }
    //Manager or admin Accept Or Reject Leave Application
    public function leaveAcceptOrReject($Leaves) 
    {
        $values = array(
                'leave_status'          => $Leaves->leave_status ,
                'leave_reject_reason'   => $Leaves->leave_reject_reason ,  
                'updated_at'            => date('Y-m-d H:i:s')
             );

        //Notification Entry
        $leave = DB::table('gpff_leave')
                ->where('leave_id', $Leaves->leave_id)
                ->First();
        $name = DB::table('gpff_users')
                ->where('user_id', $leave->area_manager_id)
                ->First();
            if($Leaves->leave_status == 1){
                $message = "Manager Has Approve Your Leave.";
            } else if($Leaves->leave_status == 2){
                $message = "Manager Has Reject Your Leave.";
            }
            $page_id = 'UNKNOWN';

            $cmn = new Common();
            $cmn->insertNotification($leave->area_manager_id,$name->firstname,$leave->user_id,$message,$page_id);
        //End Notification Entry

        return  DB::table('gpff_leave')
                ->where('leave_id', $Leaves->leave_id)
                ->update($values);  
    }
    // Fetch Leave Request Based Manager
    public function getManagerBasedLeaveRequest($Leaves)
    {  
        return  DB::table('gpff_leave as gle')
                ->join('gpff_users as guse','gle.user_id', '=' , 'guse.user_id')
                ->where('gle.area_manager_id', $Leaves->area_manager_id)
                ->where('gle.leave_status', $Leaves->leave_status)
                ->orderBy('gle.updated_at','DESC')
                ->get();
    }
    // Fetch Leave History Based User_id
    public function getUserLeaveHistory($Leaves)
    {   
        return  DB::table('gpff_leave')
                ->where('user_id', $Leaves->user_id)
                ->orderBy('updated_at','DESC')
                ->get();
    }

//Attendance
    //Field Officer Attendance 
    public function addAttendance($Leaves) 
    {   
        if($Leaves->type == 1){

            $values = array(
                'user_id'       => $Leaves->user_id , 
                'user_name'     => $Leaves->user_name ,
                'date'          => $Leaves->date , 
                'in_time'       => $Leaves->in_time ,
                'created_at'    => date('Y-m-d H:i:s') , 
                'updated_at'    => date('Y-m-d H:i:s')
            );

            DB::table('gpff_attendance')
            ->insert($values);

            $values1 = array(
                'user_attendance_status'   => 1, 
                'updated_at'    => date('Y-m-d H:i:s')
            );

            DB::table('gpff_users')
                ->where('user_id', $Leaves->user_id)
                ->update($values1);  

            //Notification Entry
            $name = DB::table('gpff_users')
                    ->where('user_id', $Leaves->user_id)
                    ->First();
                
                $message = "Field Officer ".$Leaves->user_name." is On duty at ".$Leaves->in_time;
                $page_id = 'UNKNOWN';

                $cmn = new Common();
                $cmn->insertNotification($Leaves->user_id,$Leaves->user_name,$name->area_manager_id,$message,$page_id);
            //End Notification Entry
            return 1;

        } else if($Leaves->type == 2){

            $attendanceid =   DB::table('gpff_attendance')
                            ->where('user_id', $Leaves->user_id)
                            ->where('date', $Leaves->date)
                            ->where('out_time', NULL)
                            ->First();

            $values = array(
                'user_id'       => $Leaves->user_id , 
                'user_name'     => $Leaves->user_name ,
                'date'          => $Leaves->date , 
                'out_time'      => $Leaves->out_time , 
                'updated_at'    => date('Y-m-d H:i:s')
            );

              DB::table('gpff_attendance')
                ->where('attendance_id', $attendanceid->attendance_id)
                ->update($values);  

            $values1 = array(
                'user_attendance_status'   => 0, 
                'updated_at'    => date('Y-m-d H:i:s')
            );

            DB::table('gpff_users')
                ->where('user_id', $Leaves->user_id)
                ->update($values1);

            //Notification Entry
            $name = DB::table('gpff_users')
                    ->where('user_id', $Leaves->user_id)
                    ->First();
                
                $message = "Field Officer ".$Leaves->user_name." is Off duty at ".$Leaves->out_time;
                $page_id = 'UNKNOWN';

                $cmn = new Common();
                $cmn->insertNotification($Leaves->user_id,$Leaves->user_name,$name->area_manager_id,$message,$page_id);
            //End Notification Entry
            return 1;
        }
    }
}

