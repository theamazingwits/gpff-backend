<?php

namespace App\Http\Controllers;

use Closure;
use DB;
use Mail;
use Carbon\Carbon;
use PDF;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Arr;


class Report
{
  /**
   * Handle an incoming request.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  \Closure  $next
   * @return mixed
   */
  public function handle($request, Closure $next)
  {
    return $next($request);
  }


  ////////////////////////////
  //Report Managment Api Calls//
  ////////////////////////////
  //Get Current Year Sales Report
  public function getCurrentYearSalesReport()
  {
    $orders = DB::table('gpff_order as gorder')
      ->join('gpff_order_list_batchwise as gorderlist', 'gorderlist.order_id', 'gorder.order_id')
      ->join('gpff_product as gp', 'gorderlist.product_id', 'gp.product_id')
      ->join('gpff_branch as gbranch', 'gorder.branch_id', 'gbranch.branch_id')
      ->join('gpff_region as gregion', 'gorder.region_id', 'gregion.region_id')
      ->join('gpff_area as garea', 'gorder.area_id', 'garea.area_id')
      ->whereYear('gorder.invoice_date', date('Y'))
      ->whereNotIn('gorder.order_status', [0,5])
      // ->where('gorder.invoice_type', 1)
      ->orderBy('gorder.created_at', 'DESC')
      ->get(['gorder.order_id', 'gorder.order_date', 'gorder.customer_name', 'gp.product_genericname', 'gorderlist.product_qty', 'gorder.field_officer_name', 'gorderlist.product_type', 'gbranch.branch_name', 'gregion.region_name', 'garea.area_name', 'gorderlist.product_tot_price']);

    //Promotional Product Chart Count
    $promotional = [];
    for ($i = 1; $i <= 12; $i++) {

      $res_promotional = DB::table('gpff_order_list_batchwise as gorderlist')
        ->join('gpff_order as gpor', 'gorderlist.order_id', 'gpor.order_id')
        ->whereYear('invoice_date', date('Y'))
        ->whereMonth('invoice_date', $i)
        ->where('product_type', '1')
        ->sum('product_tot_price');

      array_push($promotional, $res_promotional);
    }

    //Generic Product Chart Count
    $generic = [];
    for ($i = 1; $i <= 12; $i++) {

      $res_generic = DB::table('gpff_order_list_batchwise as gorderlist')
        ->join('gpff_order as gpor', 'gorderlist.order_id', 'gpor.order_id')
        ->whereYear('invoice_date', date('Y'))
        ->whereMonth('invoice_date', $i)
        ->where('product_type', '2')
        ->sum('product_tot_price');

      array_push($generic, $res_generic);
    }

    // print_r(json_encode($generic));
    // exit;

    //Promotional Product Total Amount
    $promotional_amt = DB::table('gpff_order_list_batchwise as gorderlist')
      ->join('gpff_order as gpor', 'gorderlist.order_id', 'gpor.order_id')
      ->whereYear('invoice_date', date('Y'))
      ->where('product_type', '1')
      ->sum('product_tot_price');

    //General Product Total Amount
    $generic_amt = DB::table('gpff_order_list_batchwise as gorderlist')
      ->join('gpff_order as gpor', 'gorderlist.order_id', 'gpor.order_id')
      ->whereYear('invoice_date', date('Y'))
      ->where('product_type', '2')
      ->sum('product_tot_price');

    $total_sale_amt = DB::table('gpff_order as gorder')
      ->whereYear('gorder.invoice_date', date('Y'))
      ->whereNotIn('gorder.order_status', [0,5])
      ->sum('or_tot_price');

    //Promotional Product Chart Details    
    $promotional_prod_count = [];
    $promotional_prod = DB::table('gpff_order_list_batchwise as gorderlist')
      ->join('gpff_order as gpor', 'gorderlist.order_id', 'gpor.order_id')
      ->join('gpff_product as gp', 'gorderlist.product_id', 'gp.product_id')
      ->whereYear('gpor.invoice_date', date('Y'))
      ->where('gorderlist.product_type', '1')
      ->groupBy('gorderlist.product_id','gp.product_genericname')
      ->get(['gorderlist.product_id',DB::Raw('SUM(product_qty) as product_qty'),DB::Raw('SUM(product_tot_price) as product_tot_price'),'gp.product_genericname']);

    foreach ($promotional_prod as $value) {

      $promo = [];
      $promo['product_genericname'] = $value->product_genericname;
      $promo['product_qty'] = $value->product_qty;
      $promo['product_price'] = $value->product_tot_price;
      array_push($promotional_prod_count, $promo);
      // $name =     DB::table('gpff_product')
      //   ->where('product_id', $value->product_id)
      //   ->First();
      // if($name){
      //   $promo['product_genericname'] = $name->product_genericname;

      //   $promo['product_qty'] = DB::table('gpff_order_list')
      //     ->where('product_id', $value->product_id)
      //     ->sum('product_qty');

      //   $promo['product_price'] = DB::table('gpff_order_list')
      //     ->where('product_id', $value->product_id)
      //     ->sum('product_tot_price');

      //   array_push($promotional_prod_count, $promo);
      // }
    }


    //Generic Product Chart Details    
    $generic_prod_count = [];
    $generic_prod = DB::table('gpff_order_list_batchwise as gorderlist')
      ->join('gpff_order as gpor', 'gorderlist.order_id', 'gpor.order_id')
      ->join('gpff_product as gp', 'gorderlist.product_id', 'gp.product_id')
      ->whereYear('gpor.invoice_date', date('Y'))
      ->where('gorderlist.product_type', '2')
      ->groupBy('gorderlist.product_id','gp.product_genericname')
      ->get(['gorderlist.product_id',DB::Raw('SUM(product_qty) as product_qty'),DB::Raw('SUM(product_tot_price) as product_tot_price'),'gp.product_genericname']);

    foreach ($generic_prod as $value) {

      $generic_chart = [];
      $generic_chart['product_genericname'] = $value->product_genericname;
      $generic_chart['product_qty'] = $value->product_qty;
      $generic_chart['product_price'] = $value->product_tot_price;
      array_push($generic_prod_count, $generic_chart);
      // $name =     DB::table('gpff_product')
      //   ->where('product_id', $value->product_id)
      //   ->First();
      // if($name){
      // $generic['product_genericname'] = $name->product_genericname;

      // $generic['product_qty'] = DB::table('gpff_order_list')
      //   ->where('product_id', $value->product_id)
      //   ->sum('product_qty');

      // $generic['product_price'] = DB::table('gpff_order_list')
      //   ->where('product_id', $value->product_id)
      //   ->sum('product_tot_price');

      // array_push($generic_prod_count, $generic);
      // }
    }

    $result = array_merge(
      ['order_details' => $orders->toArray()],
      ['promotional_chart_count' => $promotional],
      ['generic_chart_count' => $generic],
      ['promotional_total_amount' => $promotional_amt],
      ['generic_total_amount' => $generic_amt],
      ['total_sale_amt' => $total_sale_amt],
      ['promotional_prod_det' => $promotional_prod_count],
      ['generic_prod_det' => $generic_prod_count]
    );

    return $result;
  }



  //Get Filter Based Sales Report
  public function getFilterSalesReport($report)
  {
    $orders = DB::table('gpff_order as gorder')
      ->join('gpff_order_list_batchwise as gorderlist', 'gorderlist.order_id', 'gorder.order_id')
      ->join('gpff_product as gp', 'gorderlist.product_id', 'gp.product_id')
      ->join('gpff_branch as gbranch', 'gorder.branch_id', 'gbranch.branch_id')
      ->join('gpff_region as gregion', 'gorder.region_id', 'gregion.region_id')
      ->join('gpff_area as garea', 'gorder.area_id', 'garea.area_id')
      // ->where('gorder.warehouse_id', 2)
      // ->where('gorder.invoice_type', 1)
      ->whereNotIn('gorder.order_status', [0,5])
      ->orderBy('gorder.created_at', 'DESC');



    $promotional_res = [];
    $generic_res = [];

    //Promotional Product Chart Details
    $promotional_prod =  DB::table('gpff_order as gorder')
      ->join('gpff_order_list_batchwise as gorderlist', 'gorderlist.order_id', 'gorder.order_id')
      ->join('gpff_product as gp', 'gorderlist.product_id', 'gp.product_id')
      ->whereNotIn('gorder.order_status', [0,5]);

    //Generic Product Chart Details
      $generic_prod =  DB::table('gpff_order as gorder')
      ->join('gpff_order_list_batchwise as gorderlist', 'gorderlist.order_id', 'gorder.order_id')
      ->join('gpff_product as gp', 'gorderlist.product_id', 'gp.product_id')
      ->whereNotIn('gorder.order_status', [0,5]);


    if ($report->branch_id != '') {
      $orders = $orders->whereIn('gorder.branch_id', $report->branch_id);
      $promotional_prod = $promotional_prod->whereIn('gorder.branch_id', $report->branch_id);
      $generic_prod = $generic_prod->whereIn('gorder.branch_id', $report->branch_id);
    }
    if ($report->region_id != '') {
      $orders = $orders->whereIn('gorder.region_id', $report->region_id);
      $promotional_prod = $promotional_prod->whereIn('gorder.region_id', $report->region_id);
      $generic_prod = $generic_prod->whereIn('gorder.region_id', $report->region_id);
    }
    if ($report->area_id != '') {
      $orders = $orders->whereIn('gorder.area_id', $report->area_id);
      $promotional_prod = $promotional_prod->whereIn('gorder.area_id', $report->area_id);
      $generic_prod = $generic_prod->whereIn('gorder.area_id', $report->area_id);
    }
    if ($report->customer_id != '') {
      $orders = $orders->whereIn('gorder.customer_id', $report->customer_id);
      $promotional_prod = $promotional_prod->whereIn('gorder.customer_id', $report->customer_id);
      $generic_prod = $generic_prod->whereIn('gorder.customer_id', $report->customer_id);
    }
    if ($report->field_officer_id != '') {
      $orders = $orders->whereIn('gorder.field_officer_id', $report->field_officer_id);
      $promotional_prod = $promotional_prod->whereIn('gorder.field_officer_id', $report->field_officer_id);
      $generic_prod = $generic_prod->whereIn('gorder.field_officer_id', $report->field_officer_id);
    }
    if ($report->product_id != '') {
      $orders = $orders->whereIn('gorderlist.product_id', $report->product_id);
      $promotional_prod = $promotional_prod->whereIn('gorderlist.product_id', $report->product_id);
      $generic_prod = $generic_prod->whereIn('gorderlist.product_id', $report->product_id);
    }
    if ($report->year != '') {
      $orders = $orders->whereYear("gorder.invoice_date", $report->year);
      $promotional_prod = $promotional_prod->whereYear("gorder.invoice_date", $report->year);
      $generic_prod = $generic_prod->whereYear("gorder.invoice_date", $report->year);
    }
    if ($report->months != '') {
      $orders = $orders->whereIn(DB::raw('MONTH(gorder.invoice_date)'), $report->months);
      $promotional_prod = $promotional_prod->whereIn(DB::raw('MONTH(gorder.invoice_date)'), $report->months);
      $generic_prod = $generic_prod->whereIn(DB::raw('MONTH(gorder.invoice_date)'), $report->months);
    }

    //$res = ['1','2','3','4','5','6','7','8','9','10','11','12'];

    foreach ($report->months as $value) {
      $promotional['Month'] = $value;
      $promotional['count'] =  DB::table('gpff_order as gorder')
        ->join('gpff_order_list_batchwise as gorderlist', 'gorderlist.order_id', 'gorder.order_id')
        ->where(
          DB::raw('MONTH(gorder.invoice_date)'),
          $value
        )
        ->whereNotIn('gorder.order_status', [0,5])
        ->where('gorderlist.product_type', '1')
        ->where(function ($query) use ($report) {
          if ($report->branch_id != '') {
            $query->whereIn('gorder.branch_id', $report->branch_id);
          }
        })
        ->where(function ($query) use ($report) {
          if ($report->region_id != '') {
            $query->whereIn('gorder.region_id', $report->region_id);
          }
        })
        ->where(function ($query) use ($report) {
          if ($report->area_id != '') {
            $query->whereIn('gorder.area_id', $report->area_id);
          }
        })
        ->where(function ($query) use ($report) {
          if ($report->customer_id != '') {
            $query->whereIn('gorder.customer_id', $report->customer_id);
          }
        })
        ->where(function ($query) use ($report) {
          if ($report->field_officer_id != '') {
            $query->whereIn('gorder.field_officer_id', $report->field_officer_id);
          }
        })
        ->where(function ($query) use ($report) {
          if ($report->product_id != '') {
            $query->whereIn('gorderlist.product_id', $report->product_id);
          }
        })
        ->where(function ($query) use ($report) {
          if ($report->year != '') {
            $query->whereYear("gorder.invoice_date", $report->year);
          }
        })
        ->sum('gorderlist.product_tot_price');

      array_push($promotional_res, $promotional);
    }


    foreach ($report->months as $value) {
      $generic['Month'] = $value;
      $generic['count'] =  DB::table('gpff_order as gorder')
        ->join('gpff_order_list_batchwise as gorderlist', 'gorderlist.order_id', 'gorder.order_id')
        ->where(
          DB::raw('MONTH(gorder.invoice_date)'),
          $value
        )
        ->whereNotIn('gorder.order_status', [0,5])
        ->where('gorderlist.product_type', '2')
        ->where(function ($query) use ($report) {
          if ($report->branch_id != '') {
            $query->whereIn('gorder.branch_id', $report->branch_id);
          }
        })
        ->where(function ($query) use ($report) {
          if ($report->region_id != '') {
            $query->whereIn('gorder.region_id', $report->region_id);
          }
        })
        ->where(function ($query) use ($report) {
          if ($report->area_id != '') {
            $query->whereIn('gorder.area_id', $report->area_id);
          }
        })
        ->where(function ($query) use ($report) {
          if ($report->customer_id != '') {
            $query->whereIn('gorder.customer_id', $report->customer_id);
          }
        })
        ->where(function ($query) use ($report) {
          if ($report->field_officer_id != '') {
            $query->whereIn('gorder.field_officer_id', $report->field_officer_id);
          }
        })
        ->where(function ($query) use ($report) {
          if ($report->product_id != '') {
            $query->whereIn('gorderlist.product_id', $report->product_id);
          }
        })
        ->where(function ($query) use ($report) {
          if ($report->year != '') {
            $query->whereYear("gorder.invoice_date", $report->year);
          }
        })
        ->sum('gorderlist.product_tot_price');

      array_push($generic_res, $generic);
    }

    //Promotional Product Total Amount
    $promotional_amt =  DB::table('gpff_order as gorder')
      ->join('gpff_order_list_batchwise as gorderlist', 'gorderlist.order_id', 'gorder.order_id')
      ->whereNotIn('gorder.order_status', [0,5])
      ->whereIn(DB::raw('MONTH(gorder.invoice_date)'), $report->months)
      ->where('gorderlist.product_type', '1')
      ->where(function ($query) use ($report) {
        if ($report->branch_id != '') {
          $query->whereIn('gorder.branch_id', $report->branch_id);
        }
      })
      ->where(function ($query) use ($report) {
        if ($report->region_id != '') {
          $query->whereIn('gorder.region_id', $report->region_id);
        }
      })
      ->where(function ($query) use ($report) {
        if ($report->area_id != '') {
          $query->whereIn('gorder.area_id', $report->area_id);
        }
      })
      ->where(function ($query) use ($report) {
        if ($report->customer_id != '') {
          $query->whereIn('gorder.customer_id', $report->customer_id);
        }
      })
      ->where(function ($query) use ($report) {
        if ($report->field_officer_id != '') {
          $query->whereIn('gorder.field_officer_id', $report->field_officer_id);
        }
      })
      ->where(function ($query) use ($report) {
        if ($report->product_id != '') {
          $query->whereIn('gorderlist.product_id', $report->product_id);
        }
      })
      ->where(function ($query) use ($report) {
        if ($report->year != '') {
          $query->whereYear("gorder.invoice_date", $report->year);
        }
      })
      ->sum('gorderlist.product_tot_price');

    //Generic Product Total Amount
    $generic_amt =  DB::table('gpff_order as gorder')
      ->join('gpff_order_list_batchwise as gorderlist', 'gorderlist.order_id', 'gorder.order_id')
      ->whereNotIn('gorder.order_status', [0,5])
      ->whereIn(DB::raw('MONTH(gorder.invoice_date)'), $report->months)
      ->where('gorderlist.product_type', '2')
      ->where(function ($query) use ($report) {
        if ($report->branch_id != '') {
          $query->whereIn('gorder.branch_id', $report->branch_id);
        }
      })
      ->where(function ($query) use ($report) {
        if ($report->region_id != '') {
          $query->whereIn('gorder.region_id', $report->region_id);
        }
      })
      ->where(function ($query) use ($report) {
        if ($report->area_id != '') {
          $query->whereIn('gorder.area_id', $report->area_id);
        }
      })
      ->where(function ($query) use ($report) {
        if ($report->customer_id != '') {
          $query->whereIn('gorder.customer_id', $report->customer_id);
        }
      })
      ->where(function ($query) use ($report) {
        if ($report->field_officer_id != '') {
          $query->whereIn('gorder.field_officer_id', $report->field_officer_id);
        }
      })
      ->where(function ($query) use ($report) {
        if ($report->product_id != '') {
          $query->whereIn('gorderlist.product_id', $report->product_id);
        }
      })
      ->where(function ($query) use ($report) {
        if ($report->year != '') {
          $query->whereYear("gorder.invoice_date", $report->year);
        }
      })
      ->sum('gorderlist.product_tot_price');

//    $total_sale_amt = $orders->sum('or_tot_price');

    $order_det = $orders->get(['gorder.order_id', 'gorder.order_date', 'gorder.customer_name', 'gp.product_genericname', 'gorderlist.product_qty', 'gorder.field_officer_name', 'gorderlist.product_type', 'gbranch.branch_name', 'gregion.region_name', 'garea.area_name', 'gorderlist.product_tot_price','gorder.or_tot_price']);
    $orderId = [];
    foreach($order_det as $value){
      $orderId[] = $value->order_id;
    }
    // echo "--".count($order_det)."--";
    // echo "--".count($orderId)."--";
    $or_id = array_unique($orderId);
    // echo "--Uni".count($or_id)."--";
    // echo json_encode($or_id);
    $total_sale_amt = DB::table('gpff_order')
                      ->whereIn('order_id',$or_id)
                      ->sum('or_tot_price');
    // $orderRes = $order_det->unique('order_id');
    // $orderRes = array_slice($orderRes->values()->all(),true);
    // $total_sale_amt = 0;
    // foreach($orderRes as $value){
    //   $total_sale_amt = $total_sale_amt + (int)$value->or_tot_price;
    // }


    //Promotional Product Chart Details    
    $promotional_prod_count = [];
    $promotional_prod = $promotional_prod
      ->where('gorderlist.product_type', '1')
      ->groupBy('gorderlist.product_id','gp.product_genericname')
      ->get(['gorderlist.product_id',DB::Raw('SUM(product_qty) as product_qty'),DB::Raw('SUM(product_tot_price) as product_tot_price'),'gp.product_genericname']);

    foreach ($promotional_prod as $value) {

      $promo = [];
      $promo['product_genericname'] = $value->product_genericname;
      $promo['product_qty'] = $value->product_qty;
      $promo['product_price'] = $value->product_tot_price;
      array_push($promotional_prod_count, $promo);
      // $name =     DB::table('gpff_product')
      //   ->where('product_id', $value->product_id)
      //   ->First();
      // if($name){
      //   $promo['product_genericname'] = $name->product_genericname;

      //   $promo['product_qty'] = DB::table('gpff_order_list_batchwise')
      //     ->where('product_id', $value->product_id)
      //     ->sum('product_qty');

      //   $promo['product_price'] = DB::table('gpff_order_list_batchwise')
      //     ->where('product_id', $value->product_id)
      //     ->sum('product_tot_price');

      //   array_push($promotional_prod_count, $promo);
      // }
    }


    //Generic Product Chart Details    
    $generic_prod_count = [];
    $generic_prod = $generic_prod
      ->where('gorderlist.product_type', '2')
      ->groupBy('gorderlist.product_id','gp.product_genericname')
      ->get(['gorderlist.product_id',DB::Raw('SUM(product_qty) as product_qty'),DB::Raw('SUM(product_tot_price) as product_tot_price'),'gp.product_genericname']);

    foreach ($generic_prod as $value) {

      $generic = [];
      $generic['product_genericname'] = $value->product_genericname;
      $generic['product_qty'] = $value->product_qty;
      $generic['product_price'] = $value->product_tot_price;
      array_push($generic_prod_count, $generic);
      // $name =     DB::table('gpff_product')
      //   ->where('product_id', $value->product_id)
      //   ->First();
      // if($name){
      //   $generic['product_genericname'] = $name->product_genericname;

      //   $generic['product_qty'] = DB::table('gpff_order_list_batchwise')
      //     ->where('product_id', $value->product_id)
      //     ->sum('product_qty');

      //   $generic['product_price'] = DB::table('gpff_order_list_batchwise')
      //     ->where('product_id', $value->product_id)
      //     ->sum('product_tot_price');

      //   array_push($generic_prod_count, $generic);
      // }
    }


    $result = array_merge(
      ['order_details' => $order_det->toArray()],
      ['promotional_chart_count' => $promotional_res],
      ['generic_chart_count' => $generic_res],
      ['promotional_total_amount' => $promotional_amt],
      ['generic_total_amount' => $generic_amt],
      ['total_sale_amt' => $total_sale_amt],
      ['promotional_prod_det' => $promotional_prod_count],
      ['generic_prod_det' => $generic_prod_count]
    );

    return $result;
  }

  //Get Filter Based Sales Report
  public function getFilterSalesDatewiseReport($report)
  {
    $orders = DB::table('gpff_order as gorder')
      ->join('gpff_order_list_batchwise as gorderlist', 'gorderlist.order_id', 'gorder.order_id')
      ->join('gpff_product as gp', 'gorderlist.product_id', 'gp.product_id')
      ->join('gpff_branch as gbranch', 'gorder.branch_id', 'gbranch.branch_id')
      ->join('gpff_region as gregion', 'gorder.region_id', 'gregion.region_id')
      ->join('gpff_area as garea', 'gorder.area_id', 'garea.area_id')
      ->join('gpff_warehouse as gwar', 'gorder.warehouse_id', 'gwar.warehouse_id')
      ->orderBy('gorder.invoice_date', 'DESC');

    $vendor_orders = DB::table('gpff_vendor_order as gvorder')
      ->join('gpff_vendor_order_list_batchwise as gvorderlist', 'gvorderlist.vendor_order_id', 'gvorder.vendor_order_id')
      ->join('gpff_product as gp', 'gvorderlist.product_id', 'gp.product_id')
      ->join('gpff_branch as gbranch', 'gvorder.branch_id', 'gbranch.branch_id')
      ->join('gpff_region as gregion', 'gvorder.region_id', 'gregion.region_id')
      ->join('gpff_area as garea', 'gvorder.area_id', 'garea.area_id')
      ->join('gpff_warehouse as gwar', 'gvorder.warehouse_id', 'gwar.warehouse_id')
      ->join('gpff_users as guser', 'gvorder.area_manager_id', 'guser.user_id')
      ->orderBy('gvorder.created_at', 'DESC');

    

    $promotional_res = [];
    $generic_res = [];
    if ($report->branch_id != '') {
      $orders = $orders->whereIn('gorder.branch_id', $report->branch_id);
      $vendor_orders = $vendor_orders->whereIn('gvorder.branch_id', $report->branch_id);
    }
    if ($report->region_id != '') {
      $orders = $orders->whereIn('gorder.region_id', $report->region_id);
      $vendor_orders = $vendor_orders->whereIn('gvorder.region_id', $report->region_id);
    }
    if ($report->area_id != '') {
      $orders = $orders->whereIn('gorder.area_id', $report->area_id);
      $vendor_orders = $vendor_orders->whereIn('gvorder.area_id', $report->area_id);
    }
    if ($report->customer_id != '') {
      $orders = $orders->whereIn('gorder.customer_id', $report->customer_id);
    }
    if ($report->field_officer_id != '') {
      $orders = $orders->whereIn('gorder.field_officer_id', $report->field_officer_id);
    }
    if ($report->product_id != '') {
      $orders = $orders->whereIn('gorderlist.product_id', $report->product_id);    
      $vendor_orders = $vendor_orders->whereIn('gvorderlist.product_id', $report->product_id);
    }
    $orders = $orders->whereBetween(DB::RAW('date(gorder.invoice_date)'), [date($report->start_date), date($report->end_date)]);
    $vendor_orders = $vendor_orders->whereBetween(DB::RAW('date(gvorder.created_at)'), [date($report->start_date), date($report->end_date)]);

    $customer_order_det = $orders->get(['gorder.order_id', 'gorder.order_date', 'gorder.customer_name', 'gp.product_genericname', 'gorderlist.product_qty', 'gorder.field_officer_name', 'gorderlist.product_type', 'gbranch.branch_name', 'gregion.region_name', 'garea.area_name', 'gorderlist.product_tot_price','gorder.or_tot_price','gwar.warehouse_name']);

    $vendor_order_det = $vendor_orders->get(['gvorder.vendor_order_id', 'gvorder.order_date', 'gvorder.vendor_name', 'gp.product_genericname', 'gvorderlist.product_qty', 'gvorderlist.product_type', 'gbranch.branch_name', 'gregion.region_name', 'garea.area_name', 'gvorderlist.product_tot_price','gvorder.or_tot_price','gwar.warehouse_name','guser.firstname','guser.lastname']);
    $orderId = [];
    $customer_prom_prod_amt = 0;
    $customer_gen_prod_amt = 0;
    foreach($customer_order_det as $value){
      $orderId[] = $value->order_id;
      if($value->product_type == 1){
        $customer_prom_prod_amt = $customer_prom_prod_amt + (float)$value->product_tot_price;
      }else if($value->product_type == 2){
        $customer_gen_prod_amt = $customer_gen_prod_amt + (float)$value->product_tot_price;
      }
    }
    $or_id = array_unique($orderId);
    // $orid = "";
    // foreach($or_id as $value){
    //   $orid = $orid.$value.",";
    // }
    // print_r($orid);
    // exit;
    $customer_total_sale_amt = DB::table('gpff_order')
                      ->whereIn('order_id',$or_id)
                      ->get([DB::raw('SUM(or_tot_price) as or_tot_price'),DB::raw('SUM(order_discount) as order_discount'),DB::raw('SUM(spl_discount) as spl_discount')]);
    $cus_warehouse_wise = DB::table('gpff_order')
                      ->join('gpff_warehouse','gpff_warehouse.warehouse_id','gpff_order.warehouse_id')
                      ->whereIn('order_id',$or_id)
                      ->groupBy('gpff_order.warehouse_id','warehouse_name')
                      ->get(['gpff_order.warehouse_id',DB::Raw('SUM(or_tot_price) as or_tot_price'),'warehouse_name']);
    $vendor_orderId = [];
    $vendor_prom_prod_amt = 0;
    $vendor_gen_prod_amt = 0;
    foreach($vendor_order_det as $value){
      $vendor_orderId[] = $value->vendor_order_id;
      if($value->product_type == 1){
        $vendor_prom_prod_amt = $vendor_prom_prod_amt + (float)$value->product_tot_price;
      }else if($value->product_type == 2){
        $vendor_gen_prod_amt = $vendor_gen_prod_amt + (float)$value->product_tot_price;
      }
    }
    // $orid = "";
    // foreach($vendor_orderId as $value){
    //   $orid = $orid.$value.",";
    // }
    // print_r($orid);
    // exit;
    // $ven_or_id = $vendor_orderId;
    $ven_or_id = array_unique($vendor_orderId);
    $vendor_total_sale_amt = DB::table('gpff_vendor_order')
                      ->whereIn('vendor_order_id',$ven_or_id)
                      ->get([DB::raw('SUM(or_tot_price) as or_tot_price'),DB::raw('SUM(order_discount) as order_discount'),DB::raw('SUM(spl_discount) as spl_discount')]);
    $ven_warehouse_wise = DB::table('gpff_vendor_order')
                      ->join('gpff_warehouse','gpff_warehouse.warehouse_id','gpff_vendor_order.warehouse_id')
                      ->whereIn('vendor_order_id',$ven_or_id)
                      ->groupBy('gpff_vendor_order.warehouse_id','warehouse_name')
                      ->get(['gpff_vendor_order.warehouse_id',DB::Raw('SUM(or_tot_price) as or_tot_price'),'warehouse_name']);
    $promotional_prod_amt = $customer_prom_prod_amt + $vendor_prom_prod_amt;
    $generic_prod_amt = $customer_gen_prod_amt + $vendor_gen_prod_amt;
    $total_sale_amt = $customer_total_sale_amt[0]->or_tot_price + $vendor_total_sale_amt[0]->or_tot_price;
    $cus_dis_amt = $customer_total_sale_amt[0]->order_discount + $customer_total_sale_amt[0]->spl_discount;
    $ven_dis_amt = $vendor_total_sale_amt[0]->order_discount + $vendor_total_sale_amt[0]->spl_discount;
    $total_dis_amt = $cus_dis_amt + $ven_dis_amt;
    //CustomerProduct Chart Details    
    // $customer_prod = $customer_prod
    //   ->groupBy('gorderlist.product_id','gp.product_genericname','gorderlist.product_type')
    //   ->get(['gorderlist.product_id',DB::Raw('SUM(product_qty) as product_qty'),DB::Raw('SUM(product_tot_price) as product_tot_price'),'gp.product_genericname','gorderlist.product_type']);
    // $cus_promotional_prod_count = [];
    // $cus_generic_prod_count = [];
    // $cus_pro
    // foreach ($customer_prod as $value) {

    //   $product = [];
    //   $product['product_genericname'] = $value->product_genericname;
    //   $product['product_qty'] = $value->product_qty;
    //   $product['product_price'] = $value->product_tot_price;
    //   if($value->product_type == 1){
    //     array_push($cus_promotional_prod_count, $product);
    //   }else{
    //     array_push($cus_generic_prod_count, $product);
    //   }
    // }
    // //vendor Product Chart Details    
    // $vendor_prod = $vendor_prod
    //   ->groupBy('gvorderlist.product_id','gp.product_genericname','gvorderlist.product_type')
    //   ->get(['gvorderlist.product_id',DB::Raw('SUM(product_qty) as product_qty'),DB::Raw('SUM(product_tot_price) as product_tot_price'),'gp.product_genericname','gvorderlist.product_type']);
    // $ven_promotional_prod_count = [];
    // $ven_generic_prod_count = [];
    // foreach ($vendor_prod as $value) {

    //   $product = [];
    //   $product['product_genericname'] = $value->product_genericname;
    //   $product['product_qty'] = $value->product_qty;
    //   $product['product_price'] = $value->product_tot_price;
    //   if($value->product_type == 1){
    //     array_push($ven_promotional_prod_count, $product);
    //   }else{
    //     array_push($ven_generic_prod_count, $product);
    //   }
    // }
    if($report->type == 1){
      $order_det = $customer_order_det->toArray();
    }else if($report->type == 2){
      $order_det = $vendor_order_det->toArray();
    }

    $result = array_merge(
      ['cus_warehouse_wise' => $cus_warehouse_wise],
      ['ven_warehouse_wise' => $ven_warehouse_wise],
      ['total_sale_amt' => $total_sale_amt],
      ['cus_total_sale_amt' => $customer_total_sale_amt[0]->or_tot_price],
      ['ven_total_sale_amt' => $vendor_total_sale_amt[0]->or_tot_price],
      ['cus_dis_amt' => $cus_dis_amt],
      ['ven_dis_amt' => $ven_dis_amt],
      ['total_dis_amt' => $total_dis_amt],
      ['promotional_prod_amt' => $promotional_prod_amt],
      ['generic_prod_amt' => $generic_prod_amt],
      ['customer_prom_prod_amt' => $customer_prom_prod_amt],
      ['customer_gen_prod_amt' => $customer_gen_prod_amt],
      ['vendor_prom_prod_amt' => $vendor_prom_prod_amt],
      ['vendor_gen_prod_amt' => $vendor_gen_prod_amt],
      // ['cus_promotional_prod_det' => $cus_promotional_prod_count],
      // ['cus_generic_prod_det' => $cus_generic_prod_count],
      // ['ven_promotional_prod_det' => $ven_promotional_prod_count],
      // ['ven_generic_prod_det' => $ven_generic_prod_count],
      ['order_details' => $order_det]
    );

    return $result;
  }

  public function CustomercountsalesFilterReport($report)
  {
    $orders = DB::table('gpff_order as gorder')
      ->join('gpff_order_list_batchwise as gorderlist', 'gorderlist.order_id', 'gorder.order_id')
      ->orderBy('gorder.invoice_date', 'DESC');

    $promotional_res = [];
    $generic_res = [];
    if ($report->branch_id != '') {
      $orders = $orders->whereIn('gorder.branch_id', $report->branch_id);
    }
    if ($report->region_id != '') {
      $orders = $orders->whereIn('gorder.region_id', $report->region_id);
    }
    if ($report->area_id != '') {
      $orders = $orders->whereIn('gorder.area_id', $report->area_id);
    }
    if ($report->customer_id != '') {
      $orders = $orders->whereIn('gorder.customer_id', $report->customer_id);
    }
    if ($report->field_officer_id != '') {
      $orders = $orders->whereIn('gorder.field_officer_id', $report->field_officer_id);
    }
    if ($report->product_id != '') {
      $orders = $orders->whereIn('gorderlist.product_id', $report->product_id);    
    }
    $orders = $orders->whereBetween(DB::RAW('date(gorder.invoice_date)'), [date($report->start_date), date($report->end_date)]);

    $customer_order_det = $orders->get(['gorder.order_id']);

    $orderId = [];
    foreach($customer_order_det as $value){
      $orderId[] = $value->order_id;
    }
    $or_id = array_unique($orderId);

    $customer_total_sale_amt = DB::table('gpff_order')
                      ->whereIn('order_id',$or_id)
                      ->get([DB::raw('SUM(or_tot_price) as or_tot_price'),DB::raw('SUM(order_discount) as order_discount'),DB::raw('SUM(spl_discount) as spl_discount')]);
    $cus_warehouse_wise = DB::table('gpff_order')
                      ->join('gpff_warehouse','gpff_warehouse.warehouse_id','gpff_order.warehouse_id')
                      ->whereIn('order_id',$or_id)
                      ->groupBy('gpff_order.warehouse_id','warehouse_name')
                      ->get(['gpff_order.warehouse_id',DB::Raw('SUM(or_tot_price) as or_tot_price'),'warehouse_name']);

    $result = array_merge(
      ['cus_warehouse_wise' => $cus_warehouse_wise],
      ['cus_total_sale_amt' => $customer_total_sale_amt[0]->or_tot_price]
    );

    return $result;
  }

  public function VendorcountsalesFilterReport($report)
  {
    $vendor_orders = DB::table('gpff_vendor_order as gvorder')
      ->join('gpff_vendor_order_list_batchwise as gvorderlist', 'gvorderlist.vendor_order_id', 'gvorder.vendor_order_id')
      ->orderBy('gvorder.created_at', 'DESC'); 

    $promotional_res = [];
    $generic_res = [];
    if ($report->branch_id != '') {
      $vendor_orders = $vendor_orders->whereIn('gvorder.branch_id', $report->branch_id);
    }
    if ($report->region_id != '') {
      $vendor_orders = $vendor_orders->whereIn('gvorder.region_id', $report->region_id);
    }
    if ($report->area_id != '') {
      $vendor_orders = $vendor_orders->whereIn('gvorder.area_id', $report->area_id);
    }
    if ($report->product_id != '') {
      $vendor_orders = $vendor_orders->whereIn('gvorderlist.product_id', $report->product_id);
    }
    $vendor_orders = $vendor_orders->whereBetween(DB::RAW('date(gvorder.created_at)'), [date($report->start_date), date($report->end_date)]);
 
    $vendor_order_det = $vendor_orders->get(['gvorder.vendor_order_id']);

    $vendor_orderId = [];
    foreach($vendor_order_det as $value){
      $vendor_orderId[] = $value->vendor_order_id;
    }

    $ven_or_id = array_unique($vendor_orderId);
    $vendor_total_sale_amt = DB::table('gpff_vendor_order')
                      ->whereIn('vendor_order_id',$ven_or_id)
                      ->get([DB::raw('SUM(or_tot_price) as or_tot_price'),DB::raw('SUM(order_discount) as order_discount'),DB::raw('SUM(spl_discount) as spl_discount')]);
    $ven_warehouse_wise = DB::table('gpff_vendor_order')
                      ->join('gpff_warehouse','gpff_warehouse.warehouse_id','gpff_vendor_order.warehouse_id')
                      ->whereIn('vendor_order_id',$ven_or_id)
                      ->groupBy('gpff_vendor_order.warehouse_id','warehouse_name')
                      ->get(['gpff_vendor_order.warehouse_id',DB::Raw('SUM(or_tot_price) as or_tot_price'),'warehouse_name']);

    $result = array_merge(
      ['ven_warehouse_wise' => $ven_warehouse_wise],
      ['ven_total_sale_amt' => $vendor_total_sale_amt[0]->or_tot_price]
    );

    return $result;
  }

  public function CustomersalesOrderFilterReport($report)
 {
   $orders = DB::table('gpff_order as gorder')
     ->join('gpff_order_list_batchwise as gorderlist', 'gorderlist.order_id', 'gorder.order_id')
     ->join('gpff_product as gp', 'gorderlist.product_id', 'gp.product_id')
     ->join('gpff_branch as gbranch', 'gorder.branch_id', 'gbranch.branch_id')
     ->join('gpff_region as gregion', 'gorder.region_id', 'gregion.region_id')
     ->join('gpff_area as garea', 'gorder.area_id', 'garea.area_id')
     ->join('gpff_warehouse as gwar', 'gorder.warehouse_id', 'gwar.warehouse_id')
     ->orderBy('gorder.invoice_date', 'DESC');

   $promotional_res = [];
   $generic_res = [];
   if ($report->branch_id != '') {
     $orders = $orders->whereIn('gorder.branch_id', $report->branch_id);
   }
   if ($report->region_id != '') {
     $orders = $orders->whereIn('gorder.region_id', $report->region_id);
   }
   if ($report->area_id != '') {
     $orders = $orders->whereIn('gorder.area_id', $report->area_id);
   }
   if ($report->customer_id != '') {
     $orders = $orders->whereIn('gorder.customer_id', $report->customer_id);
   }
   if ($report->field_officer_id != '') {
     $orders = $orders->whereIn('gorder.field_officer_id', $report->field_officer_id);
   }
   if ($report->product_id != '') {
     $orders = $orders->whereIn('gorderlist.product_id', $report->product_id);    
   }
   $orders = $orders->whereBetween(DB::RAW('date(gorder.invoice_date)'), [date($report->start_date), date($report->end_date)]);

   $customer_order_det = $orders->get(['gorder.order_id', 'gorder.order_date', 'gorder.customer_name', 'gp.product_genericname', 'gorderlist.product_qty', 'gorder.field_officer_name', 'gorderlist.product_type', 'gbranch.branch_name', 'gregion.region_name', 'garea.area_name', 'gorderlist.product_tot_price','gorder.or_tot_price','gwar.warehouse_name']);

   // if($report->type == 1){
   //   $order_det = $customer_order_det->toArray();
   // }else if($report->type == 2){
   //   $order_det = $vendor_order_det->toArray();
   // }
   $order_det = $customer_order_det->toArray();

   $result = array_merge(
     ['order_details' => $order_det]
   );

   return $result;
 }

 public function VendorsalesOrderFilterReport($report)
 {
   $vendor_orders = DB::table('gpff_vendor_order as gvorder')
     ->join('gpff_vendor_order_list_batchwise as gvorderlist', 'gvorderlist.vendor_order_id', 'gvorder.vendor_order_id')
     ->join('gpff_product as gp', 'gvorderlist.product_id', 'gp.product_id')
     ->join('gpff_branch as gbranch', 'gvorder.branch_id', 'gbranch.branch_id')
     ->join('gpff_region as gregion', 'gvorder.region_id', 'gregion.region_id')
     ->join('gpff_area as garea', 'gvorder.area_id', 'garea.area_id')
     ->join('gpff_warehouse as gwar', 'gvorder.warehouse_id', 'gwar.warehouse_id')
     ->join('gpff_users as guser', 'gvorder.area_manager_id', 'guser.user_id')
     ->orderBy('gvorder.created_at', 'DESC');

   

   $promotional_res = [];
   $generic_res = [];
   if ($report->branch_id != '') {
     $vendor_orders = $vendor_orders->whereIn('gvorder.branch_id', $report->branch_id);
   }
   if ($report->region_id != '') {
     $vendor_orders = $vendor_orders->whereIn('gvorder.region_id', $report->region_id);
   }
   if ($report->area_id != '') {
     $vendor_orders = $vendor_orders->whereIn('gvorder.area_id', $report->area_id);
   }
   if ($report->product_id != '') {
     $vendor_orders = $vendor_orders->whereIn('gvorderlist.product_id', $report->product_id);
   }
   $vendor_orders = $vendor_orders->whereBetween(DB::RAW('date(gvorder.created_at)'), [date($report->start_date), date($report->end_date)]);

   $vendor_order_det = $vendor_orders->get(['gvorder.vendor_order_id', 'gvorder.order_date', 'gvorder.vendor_name', 'gp.product_genericname', 'gvorderlist.product_qty', 'gvorderlist.product_type', 'gbranch.branch_name', 'gregion.region_name', 'garea.area_name', 'gvorderlist.product_tot_price','gvorder.or_tot_price','gwar.warehouse_name','guser.firstname','guser.lastname']);

   // if($report->type == 1){
   //   $order_det = $customer_order_det->toArray();
   // }else if($report->type == 2){
   //   $order_det = $vendor_order_det->toArray();
   // }
   $order_det = $vendor_order_det->toArray();

   $result = array_merge(
     ['order_details' => $order_det]
   );

   return $result;
 }


  //Get Current Year Coverage Report
  public function getCurrentYearCoverageReport()
  {
    $task = DB::table('gpff_task as gtask')

      ->join('gpff_customer as gcustomer', 'gcustomer.customer_id', 'gtask.customer_id')
      ->join('gpff_users as gusers', 'gtask.field_officer_id', 'gusers.user_id')
      ->join('gpff_branch as gbranch', 'gbranch.branch_id', 'gtask.branch_id')
      ->join('gpff_region as gregion', 'gtask.region_id', 'gregion.region_id')
      ->join('gpff_area as garea', 'gtask.area_id', 'garea.area_id')
      ->whereYear('gtask.task_date_time', date('Y'))
      ->orderBy('gtask.task_date_time', 'DESC')
      ->get(['gtask.task_id', 'gtask.task_title', 'gcustomer.customer_name', 'gtask.task_date_time', 'gtask.task_status', 'gusers.firstname', 'gusers.lastname', 'gbranch.branch_name', 'gregion.region_name', 'garea.area_name', 'gtask.customer_type', 'gcustomer.customer_location']);

    //Covered Task List Count
    $covered_task = DB::table('gpff_task')
      ->whereYear('created_at', date('Y'))
      ->where('task_status', '3')
      ->count('task_id');

    //UnCovered Task List count
    $uncovered_task = DB::table('gpff_task')
      ->whereYear('created_at', date('Y'))
      ->where('task_status', '2')
      ->count('task_id');

    //Inprogress Task List count
    $inprogress_task = DB::table('gpff_task')
      ->whereYear('created_at', date('Y'))
      ->where('task_status', '1')
      ->count('task_id');

    //Not Started Task List count
    $notstarted_task = DB::table('gpff_task')
      ->whereYear('created_at', date('Y'))
      ->where('task_status', '0')
      ->count('task_id');

    //Total Pharamacy Visited Count
    $totalpharamact_visit = DB::table('gpff_task')
      ->whereYear('created_at', date('Y'))
      ->where('task_status', '3')
      ->where('customer_type', 2)
      ->groupBy('customer_id')
      ->count('task_id');

    //Unvisited Pharamacy Visited Count
    $totalpharamact_unvisit = DB::table('gpff_task')
      ->whereYear('created_at', date('Y'))
      ->where('task_status', '2')
      ->where('customer_type', 2)
      ->groupBy('customer_id')
      ->count('task_id');


    //Doctor Visited in Bar Chart Count
    $DoctorBarvisit = [];
    for ($i = 1; $i <= 12; $i++) {

      $totalDocvisiCount = DB::table('gpff_task')
        ->whereYear('created_at', date('Y'))
        ->whereMonth('created_at', $i)
        ->where('task_status', 3)
        ->where('customer_type', 1)
        ->count();

      array_push($DoctorBarvisit, $totalDocvisiCount);
    }
    //Pharmacy Visited in Bar Chart Count
    $PharmacyBarvisit = [];
    for ($i = 1; $i <= 12; $i++) {

      $totalPharvisiCount = DB::table('gpff_task')
        ->whereYear('created_at', date('Y'))
        ->whereMonth('created_at', $i)
        ->where('task_status', 3)
        ->where('customer_type', 2)
        ->count();

      array_push($PharmacyBarvisit, $totalPharvisiCount);
    }
    //Hospital Visited in Bar Chart Count
    $HospitalBarvisit = [];
    for ($i = 1; $i <= 12; $i++) {

      $totalHospivisiCount = DB::table('gpff_task')
        ->whereYear('created_at', date('Y'))
        ->whereMonth('created_at', $i)
        ->where('task_status', 3)
        ->where('customer_type', 3)
        ->count();

      array_push($HospitalBarvisit, $totalHospivisiCount);
    }

    //Doctor Un Visited in Bar Chart Count
    $DoctorBarunvisit = [];
    for ($i = 1; $i <= 12; $i++) {

      $totalDocvisiCount = DB::table('gpff_task')
        ->whereYear('created_at', date('Y'))
        ->whereMonth('created_at', $i)
        ->where('task_status', 0)
        ->where('customer_type', 1)
        ->count();

      array_push($DoctorBarunvisit, $totalDocvisiCount);
    }
    //Pharmacy Un Visited in Bar Chart Count
    $PharmacyBarunvisit = [];
    for ($i = 1; $i <= 12; $i++) {

      $totalPharvisiCount = DB::table('gpff_task')
        ->whereYear('created_at', date('Y'))
        ->whereMonth('created_at', $i)
        ->where('task_status', 0)
        ->where('customer_type', 2)
        ->count();

      array_push($PharmacyBarunvisit, $totalPharvisiCount);
    }
    //Hospital Un Visited in Bar Chart Count
    $HospitalBarunvisit = [];
    for ($i = 1; $i <= 12; $i++) {

      $totalHospivisiCount = DB::table('gpff_task')
        ->whereYear('created_at', date('Y'))
        ->whereMonth('created_at', $i)
        ->where('task_status', 0)
        ->where('customer_type', 3)
        ->count();

      array_push($HospitalBarunvisit, $totalHospivisiCount);
    }


    //Doctor Based Visited in Pie Chart Count
    $DoctorBasvisit = [];
    $DoctorBas = DB::table('gpff_task')
      ->whereYear('created_at', date('Y'))
      ->where('task_status', 3)
      ->where('customer_type', 1)
      ->distinct('customer_id')
      ->get(['customer_id']);

    foreach ($DoctorBas as $value) {

      $Doctor = [];

      $name =     DB::table('gpff_customer')
        ->where('customer_id', $value->customer_id)
        ->First();

      $Doctor['Name'] = $name->customer_name;

      $Doctor['Count'] = DB::table('gpff_task')
        ->where('customer_id', $value->customer_id)
        ->count();

      array_push($DoctorBasvisit, $Doctor);
    }

    //Pharmachy Based Visited in Pie Chart Count
    $PharmachyBasvisit = [];
    $PharmachyBas = DB::table('gpff_task')
      ->whereYear('created_at', date('Y'))
      ->where('task_status', 3)
      ->where('customer_type', 2)
      ->distinct('customer_id')
      ->get(['customer_id']);

    foreach ($PharmachyBas as $value) {

      $Pharmacy = [];

      $name =     DB::table('gpff_customer')
        ->where('customer_id', $value->customer_id)
        ->First();

      $Pharmacy['Name'] = $name->customer_name;

      $Pharmacy['Count'] = DB::table('gpff_task')
        ->where('customer_id', $value->customer_id)
        ->count();

      array_push($PharmachyBasvisit, $Pharmacy);
    }

    //Hospital Based Visited in Pie Chart Count
    $HospitalBasvisit = [];
    $HospitalBas = DB::table('gpff_task')
      ->whereYear('created_at', date('Y'))
      ->where('task_status', 3)
      ->where('customer_type', 3)
      ->distinct('customer_id')
      ->get(['customer_id']);

    foreach ($HospitalBas as $value) {

      $Hospital = [];

      $name =     DB::table('gpff_customer')
        ->where('customer_id', $value->customer_id)
        ->First();

      $Hospital['Name'] = $name->customer_name;

      $Hospital['Count'] = DB::table('gpff_task')
        ->where('customer_id', $value->customer_id)
        ->count();

      array_push($HospitalBasvisit, $Hospital);
    }

    $result = array_merge(
      ['task_details' => $task->toArray()],
      ['covered_chart_count' => $covered_task],
      ['uncovered_chart_count' => $uncovered_task],
      ['inprogress_chart_count' => $inprogress_task],
      ['notstarted_chart_count' => $notstarted_task],
      ['totalpharamacy_visit' => $totalpharamact_visit],
      ['totalpharamacy_unvisit' => $totalpharamact_unvisit],
      ['doctorbased_visit' => $DoctorBasvisit],
      ['pharmacybased_visit' => $PharmachyBasvisit],
      ['hospitalbased_visit' => $HospitalBasvisit],
      ['doctorbased_visit_chart_count' => $DoctorBarvisit],
      ['pharmacybased_visit_chart_count' => $PharmacyBarvisit],
      ['hospitalbased_visit_chart_count' => $HospitalBarvisit],
      ['doctorbased_unvisit_chart_count' => $DoctorBarunvisit],
      ['pharmacybased_unvisit_chart_count' => $PharmacyBarunvisit],
      ['hospitalbased_unvisit_chart_count' => $HospitalBarunvisit]
    );

    return $result;
  }


  //Get Coverage Filter Based Report
  public function getCoverageFilterReport($report)
  {
    $task = DB::table('gpff_task as gtask')
      ->join('gpff_customer as gcustomer', 'gcustomer.customer_id', 'gtask.customer_id')
      ->join('gpff_users as gusers', 'gtask.field_officer_id', 'gusers.user_id')
      ->join('gpff_branch as gbranch', 'gbranch.branch_id', 'gtask.branch_id')
      ->join('gpff_region as gregion', 'gtask.region_id', 'gregion.region_id')
      ->join('gpff_area as garea', 'gtask.area_id', 'garea.area_id');
      if ($report->branch_id != '') {
        $task = $task->whereIn('gtask.branch_id', $report->branch_id);
      }
      if ($report->region_id != '') {
        $task = $task->whereIn('gtask.region_id', $report->region_id);
      }
      if ($report->area_id != '') {
        $task = $task->whereIn('gtask.area_id', $report->area_id);
      }
      if ($report->field_officer_id != '') {
        $task = $task->whereIn('gtask.field_officer_id', $report->field_officer_id);
      }
      if ($report->customer_id != '') {
        $task = $task->whereIn('gtask.customer_id', $report->customer_id);
      }
      if ($report->year != '') {
        $task = $task->whereYear("gtask.task_date_time", $report->year);
      }
      if ($report->months != '') {
        $task = $task->whereIn(DB::raw('MONTH(gtask.task_date_time)'), $report->months);
      }

    switch($report->type){
      case 1:
        $Doctorvisit_res = [];
        $Pharmacyvisit_res = [];
        $Hospitalvisit_res = [];
        $Doctorunvisit_res = [];
        $Pharmacyunvisit_res = [];
        $Hospitalunvisit_res = [];
        foreach ($report->months as $value) {
          if($report->cType != '' && $report->cType ==1){
            $task_d =  DB::table('gpff_task')
            ->whereNotIn('task_status',[0,2]);
            // ->where('task_status', 3);
          }else if($report->cType != '' && $report->cType ==2){
            $task_d =  DB::table('gpff_task')
            ->whereIn('task_status',[0,2]);
            // ->whereIn('task_status', [0,2]);
          }else{
            $task_d =  DB::table('gpff_task')
            ->whereIn('task_status', [0,3]);
          }
          $task_detail =  $task_d
            // ->whereIn('task_status', [0,3])
            // ->where('customer_type', 1)
            ->whereMonth('task_date_time', $value)
            ->groupBy('customer_type','task_status')
            ->where(function ($query) use ($report) {
              if ($report->branch_id != '') {
                $query->whereIn('branch_id', $report->branch_id);
              }
            })
            ->where(function ($query) use ($report) {
              if ($report->region_id != '') {
                $query->whereIn('region_id', $report->region_id);
              }
            })
            ->where(function ($query) use ($report) {
              if ($report->area_id != '') {
                $query->whereIn('area_id', $report->area_id);
              }
            })
            ->where(function ($query) use ($report) {
              if ($report->field_officer_id != '') {
                $query->whereIn('field_officer_id', $report->field_officer_id);
              }
            })
            ->where(function ($query) use ($report) {
              if ($report->customer_id != '') {
                $query->whereIn('customer_id', $report->customer_id);
              }
            })
            ->where(function ($query) use ($report) {
              if ($report->year != '') {
                $query->whereYear("task_date_time", $report->year);
              }
            })
            ->select('customer_type','task_status',DB::raw('count(*) as total'))
            ->get();
            // print(json_encode($task_detail));
            // exit;
            foreach($task_detail as $value1){
              if($value1->task_status == 0){
                switch($value1->customer_type){
                  case 1:
                    $totalDocunvist['Month'] = $value;
                    $totalDocunvist['count'] = $value1->total;
                    array_push($Doctorunvisit_res, $totalDocunvist);
                  break;
                  case 2:
                    $totalPharunvist['Month'] = $value;
                    $totalPharunvist['count'] =  $value1->total;
                    array_push($Pharmacyunvisit_res, $totalPharunvist);
                  break;
                  case 3:
                    $totalHospiunvist['Month'] = $value;
                    $totalHospiunvist['count'] = $value1->total;
                    array_push($Hospitalunvisit_res, $totalHospiunvist);
                  break;
                }
              }else if($value1->task_status == 3){
                switch($value1->customer_type){
                  case 1:
                    $totalDocvist['Month'] = $value;
                    $totalDocvist['count'] = $value1->total;
                    array_push($Doctorvisit_res, $totalDocvist);
                  break;
                  case 2:
                    $totalPharvist['Month'] = $value;
                    $totalPharvist['count'] =  $value1->total;
                    array_push($Pharmacyvisit_res, $totalPharvist);
                  break;
                  case 3:
                    $totalHospivist['Month'] = $value;
                    $totalHospivist['count'] = $value1->total;
                    array_push($Hospitalvisit_res, $totalHospivist);
                  break;
                }
              }
            }
        }
          $result = array_merge(
            ['doctorbased_visit_chart_count' => $Doctorvisit_res],
            ['pharmacybased_visit_chart_count' => $Pharmacyvisit_res],
            ['hospitalbased_visit_chart_count' => $Hospitalvisit_res],
            ['doctorbased_unvisit_chart_count' => $Doctorunvisit_res],
            ['pharmacybased_unvisit_chart_count' => $Pharmacyunvisit_res],
            ['hospitalbased_unvisit_chart_count' => $Hospitalunvisit_res]
          );
      break;
      case 2:
        //Doctor Based Visited in Pie Chart Count
        $DoctorBasvisit = [];
        $DoctorBas = $task
          ->where('task_status', 3)
          ->where('gcustomer.customer_type', 1)
          ->groupBy('gtask.customer_id','gcustomer.customer_name')
          ->orderBy('gtask.customer_id', 'DESC')
          ->get(['gtask.customer_id','gcustomer.customer_name',DB::raw('count(*) as total')]);
        // print(json_encode($DoctorBas));
        // exit;
        foreach ($DoctorBas as $value) {
          $Doctor = [];
          $Doctor['name'] = $value->customer_name;
          $Doctor['value'] = $value->total;
          array_push($DoctorBasvisit, $Doctor);
        }
        $result = array_merge(
          ['doctorbased_visit' => $DoctorBasvisit]
        );
      break;
      case 3:
        //Pharmachy Based Visited in Pie Chart Count
        $PharmachyBasvisit = [];
        $PharmachyBas_res = $task
          ->where('task_status', 3)
          ->where('gtask.customer_type', 2)
          ->groupBy('gtask.customer_id')
          ->get(['gtask.customer_id','gcustomer.customer_name',DB::raw('count(*) as total')]);
        foreach ($PharmachyBas_res as $value) {
          $Pharmacy = [];
          $Pharmacy['name'] = $value->customer_name;
          $Pharmacy['value'] = $value->total;
          array_push($PharmachyBasvisit, $Pharmacy);
        }
        $result = array_merge(
          ['pharmacybased_visit' => $PharmachyBasvisit]
        );
      break;
      case 4:
        //Hospital Based Visited in Pie Chart Count
        $HospitalBasvisit = [];
        $HospitalBas = $task
          ->where('task_status', 3)
          ->where('gtask.customer_type', 3)
          ->groupBy('gtask.customer_id')
          ->orderBy('gtask.customer_id', 'ASC')
          ->get(['gtask.customer_id','gcustomer.customer_name',DB::raw('count(*) as total')]);
        foreach ($HospitalBas as $value) {
          $Hospital = [];
          $Hospital['name'] = $value->customer_name;
          $Hospital['value'] = $value->total;
          array_push($HospitalBasvisit, $Hospital);
        }
        $result = array_merge(
          ['hospitalbased_visit' => $HospitalBasvisit]
        );
      break;
      case 5:
        if($report->cType != '' && $report->cType ==1){
          $res_task = $task->whereNotIn('task_status',[0,2])->orderBy('gtask.task_date_time', 'DESC')->get(['gtask.task_id', 'gtask.task_title', 'gcustomer.customer_name', 'gtask.task_date_time', 'gtask.task_status', 'gusers.firstname', 'gusers.lastname', 'gbranch.branch_name', 'gregion.region_name', 'garea.area_name', 'gtask.customer_type', 'gcustomer.customer_location']);
          $result = array_merge(
            ['task_details' => $res_task->toArray()]
          );
        break;  
        }else if($report->cType != '' && $report->cType ==2){
          $res_task = $task->whereIn('task_status',[0,2])->orderBy('gtask.task_date_time', 'DESC')->get(['gtask.task_id', 'gtask.task_title', 'gcustomer.customer_name', 'gtask.task_date_time', 'gtask.task_status', 'gusers.firstname', 'gusers.lastname', 'gbranch.branch_name', 'gregion.region_name', 'garea.area_name', 'gtask.customer_type', 'gcustomer.customer_location']);
          $result = array_merge(
            ['task_details' => $res_task->toArray()]
          );
        break;  
        }else{
          $res_task = $task->orderBy('gtask.task_date_time', 'DESC')->get(['gtask.task_id', 'gtask.task_title', 'gcustomer.customer_name', 'gtask.task_date_time', 'gtask.task_status', 'gusers.firstname', 'gusers.lastname', 'gbranch.branch_name', 'gregion.region_name', 'garea.area_name', 'gtask.customer_type', 'gcustomer.customer_location']);
          $result = array_merge(
            ['task_details' => $res_task->toArray()]
          );
        break;
        }

    }
    return $result;
  }


  public function dailyCallReport($report)
  {

    $today = date('Y-m-d');
    $today = Carbon::now()->format('Y-m-d') . '%';

    if ($report->type == 1) {
      $orders = DB::table('gpff_task as gta')
        ->join('gpff_customer as gcus', 'gta.customer_id', '=', 'gcus.customer_id')
        ->join('gpff_users as guse', 'gta.field_officer_id', '=', 'guse.user_id')
        ->join('gpff_branch as gpbr', 'gta.branch_id', '=', 'gpbr.branch_id')
        ->join('gpff_region as greg', 'gta.region_id', '=', 'greg.region_id')
        ->join('gpff_area as gpar', 'gta.area_id', '=', 'gpar.area_id')
        ->whereBetween(DB::RAW('date(gta.task_date_time)'), [date($report->start_date), date($report->end_date)]);      
      if ($report->branch_id != '') {
        $orders = $orders->whereIn('gta.branch_id', $report->branch_id);
      }
      if ($report->region_id != '') {
        $orders = $orders->whereIn('gta.region_id', $report->region_id);
      }
      if ($report->area_id != '') {
        $orders = $orders->whereIn('gta.area_id', $report->area_id);
      }

      if ($report->customer_id != '') {
        $orders = $orders->whereIn('gta.customer_id', $report->customer_id);
      }
      if ($report->task_status != '') {
        $orders = $orders->whereIn('gta.task_status', $report->task_status);
      }
      if ($report->field_officer_id != '') {
        $orders = $orders->whereIn('gta.field_officer_id', $report->field_officer_id);
      }

      switch($report->report_type){
        case 1: //Data tables data
          $order_det = $orders->orderBy('gta.task_date_time', 'DESC')->get(['gcus.customer_type','gta.task_id', 'gta.created_by', 'gta.field_officer_id', 'guse.firstname', 'gta.customer_id', 'gta.area_manager_id', 'gta.task_title', 'gta.task_desc', 'gta.task_status','gta.feedback', 'gta.task_date_time', 'gta.task_reassign_status', 'gta.task_start_date_time', 'gta.task_end_date_time', 'gta.task_active_minu', 'gta.notes', 'gta.created_at', 'gta.updated_at', 'gcus.customer_name', 'gcus.customer_email', 'gcus.country_code', 'gcus.customer_mobile', 'gcus.customer_city', 'gcus.customer_country', 'gcus.customer_fax', 'gcus.customer_address', 'gcus.customer_location', 'gcus.customer_lan', 'gcus.customer_lat', 'gcus.customer_cr_id', 'gcus.website', 'gcus.avatar', 'gcus.is_active', 'gta.branch_id', 'gta.region_id', 'gta.area_id', 'gpbr.branch_name', 'greg.region_name', 'gpar.area_name']);

          $result = array_merge(
            ['DailyCall_details' => $order_det->toArray()]
          );
        break;
        case 2:
          $total_count = $orders->count();
          $orders = $orders->groupBy('gta.customer_id')                   
                    ->get(['gcus.customer_type','gta.customer_id','gcus.customer_name',DB::raw('count(gta.task_id) as task_count')]);
          $cus_data = [];
          foreach($orders as $customer)
          {
            $cus_data[] =  (array) $customer;
          }
          $customer_wise = usort($cus_data, function($a, $b) {
                         return ($a['task_count'] > $b['task_count']? -1 : 1);
                       });
          $result = array_merge(
            ['customer_wise' => $cus_data,
            'total_count' => $total_count]
          );
        break;
        case 3:
          $total_count = $orders->count();
          $orders = $orders->groupBy('gta.field_officer_id')                   
                    ->get(['gcus.customer_type','gta.field_officer_id','guse.firstname',DB::raw('count(gta.task_id) as task_count')]);
          $fo_data = [];
          foreach($orders as $fo)
          {
            $fo_data[] =  (array) $fo;
          }
          $fo_wise = usort($fo_data, function($a, $b) {
                         return ($a['task_count'] > $b['task_count']? -1 : 1);
                       });
          $result = array_merge(
            ['fo_wise' => $fo_data,
          'total_count' => $total_count]
          );
        break;
      }
      return $result;
    } else {

      $orders = DB::table('gpff_task as gta')
        ->join('gpff_customer as gcus', 'gta.customer_id', '=', 'gcus.customer_id')
        ->join('gpff_users as guse', 'gta.field_officer_id', '=', 'guse.user_id')
        ->join('gpff_branch as gpbr', 'gta.branch_id', '=', 'gpbr.branch_id')
        ->join('gpff_region as greg', 'gta.region_id', '=', 'greg.region_id')
        ->join('gpff_area as gpar', 'gta.area_id', '=', 'gpar.area_id')
        ->whereBetween(DB::RAW('date(gta.task_date_time)'), [date($report->start_date), date($report->end_date)])
        ->where('gta.task_status', 0)
        ->orderBy('gta.created_at', 'DESC');

      if ($report->branch_id != '') {
        $orders = $orders->whereIn('gta.branch_id', $report->branch_id);
      }
      if ($report->region_id != '') {
        $orders = $orders->whereIn('gta.region_id', $report->region_id);
      }
      if ($report->area_id != '') {
        $orders = $orders->whereIn('gta.area_id', $report->area_id);
      }
      if ($report->customer_id != '') {
        $orders = $orders->whereIn('gta.customer_id', $report->customer_id);
      }
      if ($report->field_officer_id != '') {
        $orders = $orders->whereIn('gta.field_officer_id', $report->field_officer_id);
      }

      $order_det = $orders->get(['gcus.customer_type','gta.task_id', 'gta.created_by', 'gta.field_officer_id', 'guse.firstname', 'gta.customer_id', 'gta.area_manager_id', 'gta.task_title', 'gta.task_desc', 'gta.task_status', 'gta.task_date_time', 'gta.task_reassign_status', 'gta.task_start_date_time', 'gta.task_end_date_time', 'gta.task_active_minu', 'gta.notes', 'gta.created_at', 'gta.updated_at', 'gcus.customer_name', 'gcus.customer_email', 'gcus.country_code', 'gcus.customer_mobile', 'gcus.customer_city', 'gcus.customer_country', 'gcus.customer_fax', 'gcus.customer_address', 'gcus.customer_location', 'gcus.customer_lan', 'gcus.customer_lat', 'gcus.customer_cr_id', 'gcus.website', 'gcus.avatar', 'gcus.is_active', 'gta.branch_id', 'gta.region_id', 'gta.area_id', 'gpbr.branch_name', 'greg.region_name', 'gpar.area_name']);

      $result = array_merge(
        ['pending_task_details' => $order_det->toArray()]
      );

      return $result;
    }
  }
  ////////////////////
  ///Visited Report///
  ////////////////////
  //Current Year Visited Report
  public function getCurrentYearVisitedReport()
  {
    $visited = DB::table('gpff_task as gta')
      ->join('gpff_customer as gcus', 'gta.customer_id', '=', 'gcus.customer_id')
      ->join('gpff_users as guse', 'gta.field_officer_id', '=', 'guse.user_id')
      ->join('gpff_branch as gpbr', 'gta.branch_id', '=', 'gpbr.branch_id')
      ->join('gpff_region as greg', 'gta.region_id', '=', 'greg.region_id')
      ->join('gpff_area as gpar', 'gta.area_id', '=', 'gpar.area_id')
      ->whereYear('gta.created_at', date('Y'))
      ->where('gta.task_status', 3)
      ->orderBy('gta.created_at', 'DESC')
      ->get(['gta.task_id', 'gta.created_by', 'gta.field_officer_id', 'guse.firstname', 'gta.customer_id', 'gta.area_manager_id', 'gta.task_title', 'gta.task_desc', 'gta.task_status', 'gta.task_date_time', 'gta.task_reassign_status', 'gta.task_start_date_time', 'gta.task_end_date_time', 'gta.task_active_minu', 'gta.notes', 'gta.created_at', 'gta.updated_at', 'gcus.customer_name', 'gcus.customer_email', 'gcus.country_code', 'gcus.customer_mobile', 'gcus.customer_city', 'gcus.customer_country', 'gcus.customer_fax', 'gcus.customer_address', 'gcus.customer_location', 'gcus.customer_lan', 'gcus.customer_lat', 'gcus.customer_cr_id', 'gcus.website', 'gcus.avatar', 'gcus.customer_type', 'gcus.is_active', 'gta.branch_id', 'gta.region_id', 'gta.area_id', 'gpbr.branch_name', 'greg.region_name', 'gpar.area_name']);

    //Total Visited in Par Chart Count
    $totalvist = [];
    for ($i = 1; $i <= 12; $i++) {

      $totalvisiCount = DB::table('gpff_task')
        ->whereYear('created_at', date('Y'))
        ->whereMonth('created_at', $i)
        ->where('task_status', 3)
        ->count();

      array_push($totalvist, $totalvisiCount);
    }
    //Doctor Visited in Par Chart Count
    $Doctorvisit = [];
    for ($i = 1; $i <= 12; $i++) {

      $totalDocvisiCount = DB::table('gpff_task')
        ->whereYear('created_at', date('Y'))
        ->whereMonth('created_at', $i)
        ->where('task_status', 3)
        ->where('customer_type', 1)
        ->count();

      array_push($Doctorvisit, $totalDocvisiCount);
    }
    //Pharmacy Visited in Par Chart Count
    $Pharmacyvisit = [];
    for ($i = 1; $i <= 12; $i++) {

      $totalPharvisiCount = DB::table('gpff_task')
        ->whereYear('created_at', date('Y'))
        ->whereMonth('created_at', $i)
        ->where('task_status', 3)
        ->where('customer_type', 2)
        ->count();

      array_push($Pharmacyvisit, $totalPharvisiCount);
    }
    //Hospital Visited in Par Chart Count
    $Hospitalvisit = [];
    for ($i = 1; $i <= 12; $i++) {

      $totalHospivisiCount = DB::table('gpff_task')
        ->whereYear('created_at', date('Y'))
        ->whereMonth('created_at', $i)
        ->where('task_status', 3)
        ->where('customer_type', 3)
        ->count();

      array_push($Hospitalvisit, $totalHospivisiCount);
    }
    //Totoal Visited Totoal Count
    $totvisitotCount = DB::table('gpff_task')
      ->whereYear('created_at', date('Y'))
      ->where('task_status', 3)
      ->count();

    //Doctor Visited Totoal Count
    $DocvisitotCount = DB::table('gpff_task')
      ->whereYear('created_at', date('Y'))
      ->where('task_status', 3)
      ->where('customer_type', 1)
      ->count();

    //Pharmacy Visited Totoal Count
    $PhavisitotCount = DB::table('gpff_task')
      ->whereYear('created_at', date('Y'))
      ->where('task_status', 3)
      ->where('customer_type', 2)
      ->count();

    //Hospital Visited Totoal Count
    $HospivisitotCount = DB::table('gpff_task')
      ->whereYear('created_at', date('Y'))
      ->where('task_status', 3)
      ->where('customer_type', 3)
      ->count();

    //Doctor Based Visited in Pie Chart Count
    $DoctorBasvisit = [];
    $DoctorBas = DB::table('gpff_task')
      ->whereYear('created_at', date('Y'))
      ->where('task_status', 3)
      ->where('customer_type', 1)
      ->distinct('customer_id')
      ->get(['customer_id']);

    foreach ($DoctorBas as $value) {

      $Doctor = [];

      $name =     DB::table('gpff_customer')
        ->where('customer_id', $value->customer_id)
        ->First();

      $Doctor['Name'] = $name->customer_name;

      $Doctor['Count'] = DB::table('gpff_task')
        ->where('customer_id', $value->customer_id)
        ->count();

      array_push($DoctorBasvisit, $Doctor);
    }

    //Pharmachy Based Visited in Pie Chart Count
    $PharmachyBasvisit = [];
    $PharmachyBas = DB::table('gpff_task')
      ->whereYear('created_at', date('Y'))
      ->where('task_status', 3)
      ->where('customer_type', 2)
      ->distinct('customer_id')
      ->get(['customer_id']);

    foreach ($PharmachyBas as $value) {

      $Pharmacy = [];

      $name =     DB::table('gpff_customer')
        ->where('customer_id', $value->customer_id)
        ->First();

      $Pharmacy['Name'] = $name->customer_name;

      $Pharmacy['Count'] = DB::table('gpff_task')
        ->where('customer_id', $value->customer_id)
        ->count();

      array_push($PharmachyBasvisit, $Pharmacy);
    }
    //Hospital Based Visited in Pie Chart Count
    $HospitalBasvisit = [];
    $HospitalBas = DB::table('gpff_task')
      ->whereYear('created_at', date('Y'))
      ->where('task_status', 3)
      ->where('customer_type', 3)
      ->distinct('customer_id')
      ->get(['customer_id']);

    foreach ($HospitalBas as $value) {

      $Hospital = [];

      $name =     DB::table('gpff_customer')
        ->where('customer_id', $value->customer_id)
        ->First();

      $Hospital['Name'] = $name->customer_name;

      $Hospital['Count'] = DB::table('gpff_task')
        ->where('customer_id', $value->customer_id)
        ->count();

      array_push($HospitalBasvisit, $Hospital);
    }

    $result = array_merge(
      ['visited_details' => $visited->toArray()],
      ['TotalVisit_count' => $totalvist],
      ['TotalDocVisit_count' => $Doctorvisit],
      ['TotalPharmacyVisit_count' => $Pharmacyvisit],
      ['TotalHospitalVisit_count' => $Hospitalvisit],
      ['TotalVisitTotal' => $totvisitotCount],
      ['TotalDocVisitTotal' => $DocvisitotCount],
      ['TotalPharmacyVisitTotal' => $PhavisitotCount],
      ['TotalHospitalVisitTotal' => $HospivisitotCount],
      ['DoctorBasedvisit_count' => $DoctorBasvisit],
      ['PharmachyBasedvisit_count' => $PharmachyBasvisit],
      ['HospitalBasedvisit_count' => $HospitalBasvisit]
    );

    return $result;
  }
  ///Visited Filter based Report
  public function getVisitedFilterReport($report)
  {
    $visited = DB::table('gpff_task as gta')
      ->join('gpff_customer as gcus', 'gta.customer_id', '=', 'gcus.customer_id')
      ->join('gpff_users as guse', 'gta.field_officer_id', '=', 'guse.user_id')
      ->join('gpff_branch as gpbr', 'gta.branch_id', '=', 'gpbr.branch_id')
      ->join('gpff_region as greg', 'gta.region_id', '=', 'greg.region_id')
      ->join('gpff_area as gpar', 'gta.area_id', '=', 'gpar.area_id')
      ->where('gta.task_status', 3)
      ->orderBy('gta.created_at', 'DESC');

    if ($report->branch_id != '') {
      $visited = $visited->whereIn('gta.branch_id', $report->branch_id);
    }
    if ($report->region_id != '') {
      $visited = $visited->whereIn('gta.region_id', $report->region_id);
    }
    if ($report->area_id != '') {
      $visited = $visited->whereIn('gta.area_id', $report->area_id);
    }
    if ($report->field_officer_id != '') {
      $visited = $visited->whereIn('gta.field_officer_id', $report->field_officer_id);
    }
    if ($report->year != '') {
      $visited = $visited->whereYear("gta.created_at", $report->year);
    }
    if ($report->months != '') {
      $visited = $visited->whereIn(DB::raw('MONTH(gta.created_at)'), $report->months);
    }

    $totalvist_res = [];
    $Doctorvisit_res = [];
    $Pharmacyvisit_res = [];
    $Hospitalvisit_res = [];

    //Total Visited in Par Chart Count  
    foreach ($report->months as $value) {
      $totalvist['Month'] = $value;
      $totalvist['count'] =  DB::table('gpff_task')
        ->where('task_status', 3)
        ->whereMonth('created_at', $value)
        ->where(function ($query) use ($report) {
          if ($report->branch_id != '') {
            $query->whereIn('branch_id', $report->branch_id);
          }
        })
        ->where(function ($query) use ($report) {
          if ($report->region_id != '') {
            $query->whereIn('region_id', $report->region_id);
          }
        })
        ->where(function ($query) use ($report) {
          if ($report->area_id != '') {
            $query->whereIn('area_id', $report->area_id);
          }
        })
        ->where(function ($query) use ($report) {
          if ($report->field_officer_id != '') {
            $query->whereIn('field_officer_id', $report->field_officer_id);
          }
        })
        ->where(function ($query) use ($report) {
          if ($report->year != '') {
            $query->whereYear("created_at", $report->year);
          }
        })
        ->count();

      array_push($totalvist_res, $totalvist);
    }
    //Doctor Visited in Par Chart Count
    foreach ($report->months as $value) {
      $totalDocvist['Month'] = $value;
      $totalDocvist['count'] =  DB::table('gpff_task')
        ->where('task_status', 3)
        ->where('customer_type', 1)
        ->whereMonth('created_at', $value)
        ->where(function ($query) use ($report) {
          if ($report->branch_id != '') {
            $query->whereIn('branch_id', $report->branch_id);
          }
        })
        ->where(function ($query) use ($report) {
          if ($report->region_id != '') {
            $query->whereIn('region_id', $report->region_id);
          }
        })
        ->where(function ($query) use ($report) {
          if ($report->area_id != '') {
            $query->whereIn('area_id', $report->area_id);
          }
        })
        ->where(function ($query) use ($report) {
          if ($report->field_officer_id != '') {
            $query->whereIn('field_officer_id', $report->field_officer_id);
          }
        })
        ->where(function ($query) use ($report) {
          if ($report->year != '') {
            $query->whereYear("created_at", $report->year);
          }
        })
        ->count();

      array_push($Doctorvisit_res, $totalDocvist);
    }
    //Pharmacy Visited in Par Chart Count
    foreach ($report->months as $value) {
      $totalPharvist['Month'] = $value;
      $totalPharvist['count'] =  DB::table('gpff_task')
        ->where('task_status', 3)
        ->where('customer_type', 2)
        ->whereMonth('created_at', $value)
        ->where(function ($query) use ($report) {
          if ($report->branch_id != '') {
            $query->whereIn('branch_id', $report->branch_id);
          }
        })
        ->where(function ($query) use ($report) {
          if ($report->region_id != '') {
            $query->whereIn('region_id', $report->region_id);
          }
        })
        ->where(function ($query) use ($report) {
          if ($report->area_id != '') {
            $query->whereIn('area_id', $report->area_id);
          }
        })
        ->where(function ($query) use ($report) {
          if ($report->field_officer_id != '') {
            $query->whereIn('field_officer_id', $report->field_officer_id);
          }
        })
        ->where(function ($query) use ($report) {
          if ($report->year != '') {
            $query->whereYear("created_at", $report->year);
          }
        })
        ->count();

      array_push($Pharmacyvisit_res, $totalPharvist);
    }
    //Pharmacy Visited in Par Chart Count
    foreach ($report->months as $value) {
      $totalHospivist['Month'] = $value;
      $totalHospivist['count'] =  DB::table('gpff_task')
        ->where('task_status', 3)
        ->where('customer_type', 3)
        ->whereMonth('created_at', $value)
        ->where(function ($query) use ($report) {
          if ($report->branch_id != '') {
            $query->whereIn('branch_id', $report->branch_id);
          }
        })
        ->where(function ($query) use ($report) {
          if ($report->region_id != '') {
            $query->whereIn('region_id', $report->region_id);
          }
        })
        ->where(function ($query) use ($report) {
          if ($report->area_id != '') {
            $query->whereIn('area_id', $report->area_id);
          }
        })
        ->where(function ($query) use ($report) {
          if ($report->field_officer_id != '') {
            $query->whereIn('field_officer_id', $report->field_officer_id);
          }
        })
        ->where(function ($query) use ($report) {
          if ($report->year != '') {
            $query->whereYear("created_at", $report->year);
          }
        })
        ->count();

      array_push($Hospitalvisit_res, $totalHospivist);
    }

    //Totoal Visited Totoal Count
    $totvisitotCount = DB::table('gpff_task')
      ->where('task_status', 3)
      ->whereIn(DB::raw('MONTH(created_at)'), $report->months)
      ->where(function ($query) use ($report) {
        if ($report->branch_id != '') {
          $query->whereIn('branch_id', $report->branch_id);
        }
      })
      ->where(function ($query) use ($report) {
        if ($report->region_id != '') {
          $query->whereIn('region_id', $report->region_id);
        }
      })
      ->where(function ($query) use ($report) {
        if ($report->area_id != '') {
          $query->whereIn('area_id', $report->area_id);
        }
      })
      ->where(function ($query) use ($report) {
        if ($report->field_officer_id != '') {
          $query->whereIn('field_officer_id', $report->field_officer_id);
        }
      })
      ->where(function ($query) use ($report) {
        if ($report->year != '') {
          $query->whereYear("created_at", $report->year);
        }
      })
      ->count();

    //Doctor Visited Totoal Count
    $DocvisitotCount = DB::table('gpff_task')
      ->where('task_status', 3)
      ->where('customer_type', 1)
      ->whereIn(DB::raw('MONTH(created_at)'), $report->months)
      ->where(function ($query) use ($report) {
        if ($report->branch_id != '') {
          $query->whereIn('branch_id', $report->branch_id);
        }
      })
      ->where(function ($query) use ($report) {
        if ($report->region_id != '') {
          $query->whereIn('region_id', $report->region_id);
        }
      })
      ->where(function ($query) use ($report) {
        if ($report->area_id != '') {
          $query->whereIn('area_id', $report->area_id);
        }
      })
      ->where(function ($query) use ($report) {
        if ($report->field_officer_id != '') {
          $query->whereIn('field_officer_id', $report->field_officer_id);
        }
      })
      ->where(function ($query) use ($report) {
        if ($report->year != '') {
          $query->whereYear("created_at", $report->year);
        }
      })
      ->where(function ($query) use ($report) {
        if ($report->months != '') {
          $query->whereIn(DB::raw('MONTH(created_at)'), $report->months);
        }
      })
      ->count();

    //Pharmacy Visited Totoal Count
    $PhavisitotCount = DB::table('gpff_task')
      ->where('task_status', 3)
      ->where('customer_type', 2)
      ->whereIn(DB::raw('MONTH(created_at)'), $report->months)
      ->where(function ($query) use ($report) {
        if ($report->branch_id != '') {
          $query->whereIn('branch_id', $report->branch_id);
        }
      })
      ->where(function ($query) use ($report) {
        if ($report->region_id != '') {
          $query->whereIn('region_id', $report->region_id);
        }
      })
      ->where(function ($query) use ($report) {
        if ($report->area_id != '') {
          $query->whereIn('area_id', $report->area_id);
        }
      })
      ->where(function ($query) use ($report) {
        if ($report->field_officer_id != '') {
          $query->whereIn('field_officer_id', $report->field_officer_id);
        }
      })
      ->where(function ($query) use ($report) {
        if ($report->year != '') {
          $query->whereYear("created_at", $report->year);
        }
      })
      ->where(function ($query) use ($report) {
        if ($report->months != '') {
          $query->whereIn(DB::raw('MONTH(created_at)'), $report->months);
        }
      })
      ->count();
    //Hospital Visited Totoal Count
    $HospivisitotCount = DB::table('gpff_task')
      ->where('task_status', 3)
      ->where('customer_type', 3)
      ->whereIn(DB::raw('MONTH(created_at)'), $report->months)
      ->where(function ($query) use ($report) {
        if ($report->branch_id != '') {
          $query->whereIn('branch_id', $report->branch_id);
        }
      })
      ->where(function ($query) use ($report) {
        if ($report->region_id != '') {
          $query->whereIn('region_id', $report->region_id);
        }
      })
      ->where(function ($query) use ($report) {
        if ($report->area_id != '') {
          $query->whereIn('area_id', $report->area_id);
        }
      })
      ->where(function ($query) use ($report) {
        if ($report->field_officer_id != '') {
          $query->whereIn('field_officer_id', $report->field_officer_id);
        }
      })
      ->where(function ($query) use ($report) {
        if ($report->year != '') {
          $query->whereYear("created_at", $report->year);
        }
      })
      ->where(function ($query) use ($report) {
        if ($report->months != '') {
          $query->whereIn(DB::raw('MONTH(created_at)'), $report->months);
        }
      })
      ->count();

    //Doctor Based Visited in Pie Chart Count
    $DoctorBasvisit = [];

    $DoctorBas =  DB::table('gpff_task')
      ->where('task_status', 3)
      ->where('customer_type', 1)
      ->where(function ($query) use ($report) {
        if ($report->branch_id != '') {
          $query->whereIn('branch_id', $report->branch_id);
        }
      })
      ->where(function ($query) use ($report) {
        if ($report->region_id != '') {
          $query->whereIn('region_id', $report->region_id);
        }
      })
      ->where(function ($query) use ($report) {
        if ($report->area_id != '') {
          $query->whereIn('area_id', $report->area_id);
        }
      })
      ->where(function ($query) use ($report) {
        if ($report->field_officer_id != '') {
          $query->whereIn('field_officer_id', $report->field_officer_id);
        }
      })
      ->where(function ($query) use ($report) {
        if ($report->year != '') {
          $query->whereYear("created_at", $report->year);
        }
      })
      ->where(function ($query) use ($report) {
        if ($report->months != '') {
          $query->whereIn(DB::raw('MONTH(created_at)'), $report->months);
        }
      })
      ->distinct('customer_id')
      ->get(['customer_id']);

    foreach ($DoctorBas as $value) {

      $Doctor = [];

      $name =     DB::table('gpff_customer')
        ->where('customer_id', $value->customer_id)
        ->First();
      if ($name) {
        $Doctor['Name'] = $name->customer_name;
      }

      $Doctor['Count'] = DB::table('gpff_task')
        ->where('customer_id', $value->customer_id)
        ->where('task_status', 3)
        ->where(function ($query) use ($report) {
          if ($report->branch_id != '') {
            $query->whereIn('branch_id', $report->branch_id);
          }
        })
        ->where(function ($query) use ($report) {
          if ($report->region_id != '') {
            $query->whereIn('region_id', $report->region_id);
          }
        })
        ->where(function ($query) use ($report) {
          if ($report->area_id != '') {
            $query->whereIn('area_id', $report->area_id);
          }
        })
        ->where(function ($query) use ($report) {
          if ($report->field_officer_id != '') {
            $query->whereIn('field_officer_id', $report->field_officer_id);
          }
        })
        ->where(function ($query) use ($report) {
          if ($report->year != '') {
            $query->whereYear("created_at", $report->year);
          }
        })
        ->where(function ($query) use ($report) {
          if ($report->months != '') {
            $query->whereIn(DB::raw('MONTH(created_at)'), $report->months);
          }
        })
        ->count();

      array_push($DoctorBasvisit, $Doctor);
    }
    //Pharmachy Based Visited in Pie Chart Count
    $PharmachyBasvisit = [];

    $PharmachyBas =  DB::table('gpff_task')
      ->where('task_status', 3)
      ->where('customer_type', 2)
      ->where(function ($query) use ($report) {
        if ($report->branch_id != '') {
          $query->whereIn('branch_id', $report->branch_id);
        }
      })
      ->where(function ($query) use ($report) {
        if ($report->region_id != '') {
          $query->whereIn('region_id', $report->region_id);
        }
      })
      ->where(function ($query) use ($report) {
        if ($report->area_id != '') {
          $query->whereIn('area_id', $report->area_id);
        }
      })
      ->where(function ($query) use ($report) {
        if ($report->field_officer_id != '') {
          $query->whereIn('field_officer_id', $report->field_officer_id);
        }
      })
      ->where(function ($query) use ($report) {
        if ($report->year != '') {
          $query->whereYear("created_at", $report->year);
        }
      })
      ->where(function ($query) use ($report) {
        if ($report->months != '') {
          $query->whereIn(DB::raw('MONTH(created_at)'), $report->months);
        }
      })
      ->distinct('customer_id')
      ->get(['customer_id']);

    foreach ($PharmachyBas as $value) {

      $Pharmacy = [];

      $name =     DB::table('gpff_customer')
        ->where('customer_id', $value->customer_id)
        ->First();
      if ($name) {
        $Pharmacy['Name'] = $name->customer_name;
      }

      $Pharmacy['Count'] = DB::table('gpff_task')
        ->where('customer_id', $value->customer_id)
        ->where('task_status', 3)
        ->where(function ($query) use ($report) {
          if ($report->branch_id != '') {
            $query->whereIn('branch_id', $report->branch_id);
          }
        })
        ->where(function ($query) use ($report) {
          if ($report->region_id != '') {
            $query->whereIn('region_id', $report->region_id);
          }
        })
        ->where(function ($query) use ($report) {
          if ($report->area_id != '') {
            $query->whereIn('area_id', $report->area_id);
          }
        })
        ->where(function ($query) use ($report) {
          if ($report->field_officer_id != '') {
            $query->whereIn('field_officer_id', $report->field_officer_id);
          }
        })
        ->where(function ($query) use ($report) {
          if ($report->year != '') {
            $query->whereYear("created_at", $report->year);
          }
        })
        ->where(function ($query) use ($report) {
          if ($report->months != '') {
            $query->whereIn(DB::raw('MONTH(created_at)'), $report->months);
          }
        })
        ->count();

      array_push($PharmachyBasvisit, $Pharmacy);
    }

    //Hospital Based Visited in Pie Chart Count
    $HospitalBasvisit = [];

    $HospitalBas =  DB::table('gpff_task')
      ->where('task_status', 3)
      ->where('customer_type', 3)
      ->where(function ($query) use ($report) {
        if ($report->branch_id != '') {
          $query->whereIn('branch_id', $report->branch_id);
        }
      })
      ->where(function ($query) use ($report) {
        if ($report->region_id != '') {
          $query->whereIn('region_id', $report->region_id);
        }
      })
      ->where(function ($query) use ($report) {
        if ($report->area_id != '') {
          $query->whereIn('area_id', $report->area_id);
        }
      })
      ->where(function ($query) use ($report) {
        if ($report->field_officer_id != '') {
          $query->whereIn('field_officer_id', $report->field_officer_id);
        }
      })
      ->where(function ($query) use ($report) {
        if ($report->year != '') {
          $query->whereYear("created_at", $report->year);
        }
      })
      ->where(function ($query) use ($report) {
        if ($report->months != '') {
          $query->whereIn(DB::raw('MONTH(created_at)'), $report->months);
        }
      })
      ->distinct('customer_id')
      ->get(['customer_id']);

    foreach ($HospitalBas as $value) {

      $Hospital = [];

      $name =     DB::table('gpff_customer')
        ->where('customer_id', $value->customer_id)
        ->First();
      if ($name) {
        $Hospital['Name'] = $name->customer_name;
      }

      $Hospital['Count'] = DB::table('gpff_task')
        ->where('customer_id', $value->customer_id)
        ->where('task_status', 3)
        ->where(function ($query) use ($report) {
          if ($report->branch_id != '') {
            $query->whereIn('branch_id', $report->branch_id);
          }
        })
        ->where(function ($query) use ($report) {
          if ($report->region_id != '') {
            $query->whereIn('region_id', $report->region_id);
          }
        })
        ->where(function ($query) use ($report) {
          if ($report->area_id != '') {
            $query->whereIn('area_id', $report->area_id);
          }
        })
        ->where(function ($query) use ($report) {
          if ($report->field_officer_id != '') {
            $query->whereIn('field_officer_id', $report->field_officer_id);
          }
        })
        ->where(function ($query) use ($report) {
          if ($report->year != '') {
            $query->whereYear("created_at", $report->year);
          }
        })
        ->where(function ($query) use ($report) {
          if ($report->months != '') {
            $query->whereIn(DB::raw('MONTH(created_at)'), $report->months);
          }
        })
        ->count();

      array_push($HospitalBasvisit, $Hospital);
    }

    $visited_de = $visited->get(['gta.task_id', 'gta.created_by', 'gta.field_officer_id', 'guse.firstname', 'gta.customer_id', 'gta.area_manager_id', 'gta.task_title', 'gta.task_desc', 'gta.task_status', 'gta.task_date_time', 'gta.task_reassign_status', 'gta.task_start_date_time', 'gta.task_end_date_time', 'gta.task_active_minu', 'gta.notes', 'gta.created_at', 'gta.updated_at', 'gcus.customer_name', 'gcus.customer_email', 'gcus.country_code', 'gcus.customer_mobile', 'gcus.customer_city', 'gcus.customer_country', 'gcus.customer_fax', 'gcus.customer_address', 'gcus.customer_location', 'gcus.customer_lan', 'gcus.customer_lat', 'gcus.customer_cr_id', 'gcus.website', 'gcus.avatar', 'gcus.customer_type', 'gcus.is_active', 'gta.branch_id', 'gta.region_id', 'gta.area_id', 'gpbr.branch_name', 'greg.region_name', 'gpar.area_name']);

    $result = array_merge(
      ['visited_details' => $visited_de->toArray()],
      ['TotalVisit_count' => $totalvist_res],
      ['TotalDocVisit_count' => $Doctorvisit_res],
      ['TotalPharmacyVisit_count' => $Pharmacyvisit_res],
      ['TotalHospitalVisit_count' => $Hospitalvisit_res],
      ['TotalVisitTotal' => $totvisitotCount],
      ['TotalDocVisitTotal' => $DocvisitotCount],
      ['TotalPharmacyVisitTotal' => $PhavisitotCount],
      ['TotalHospitalVisitTotal' => $HospivisitotCount],
      ['DoctorBasedvisit_count' => $DoctorBasvisit],
      ['PharmachyBasedvisit_count' => $PharmachyBasvisit],
      ['HospitalBasedvisit_count' => $HospitalBasvisit]
    );

    return $result;
  }

  ////////////////////
  ///Target Report////
  ////////////////////

  ////////////////////
  ///Target Report////
  ////////////////////
  public function targetDetailReport($req)
  {
    $months = $req->months;
    //$months = explode(',', $req->months);
    $array_ret = [];
    $array_val = [];

    for ($i = 0; $i < count($months); $i++) {
      $array_val['month'] = $months[$i];
      $array_val['year'] = $req->year;
      switch($req->type){
        case 1:
          $array_val['target'] = $this->getTargetBasedInsentive($req, $months[$i]);
          $array_val['unit'] = [];
          $array_val['percentage'] = [];
        break;
        case 2:
          $array_val['target'] = [];
          $array_val['unit'] = [];
          $array_val['percentage'] = $this->getPercentageBasedInsentive($req, $months[$i]);
        break;
        case 3:
          $array_val['target'] = [];
          $array_val['unit'] = $this->getUnitBasedInsentive($req, $months[$i]); // NO Changes
          $array_val['percentage'] = [];
        break;
      }
      array_push($array_ret, $array_val);
    }

    return $array_ret;
    /*echo json_encode($array_ret);
    exit;*/
  }

  private function getUnitBasedInsentive($req, $month)
  {
    $array_ret = [];
    $prdObject = [];
    // $datas = [];
    $year = $req->year;
    $filedOfficer = $req->field_officer_id;
    $insResult = DB::table('gpff_insentive as ins')
      ->join('gpff_product as gp', 'ins.product_id', 'ins.product_id')
      ->where('ins.fo_id', $filedOfficer)
      ->where('ins.insentive_type', 2)
      ->where('ins.insentive_status', 1)
      ->whereRaw('year(ins.from_date) = ' . $year . ' AND month(ins.from_date) = ' . $month . ' order by cast(ins.product_id as unsigned) DESC')
      ->select('ins.insentive_id', 'ins.product_id', 'ins.unit', 'ins.insentive_amt', 'ins.from_date', 'ins.to_date', 'ins.product_name','gp.product_netprice')
      // ->get();
      // ->orderBy('created_at', 'DESC')
      ->First();
      // print_r($insResult);
      // print_r($insResult->unit);
      // exit();

    $not_sale_products = [];
    if ($insResult) {
      $insentive_details =  DB::table('gpff_insentive as ins')
                       ->where('fo_id',$filedOfficer)
                       ->where('insentive_type',2)
                       ->whereRaw('year(ins.from_date) = ' . $year . ' AND month(ins.from_date) = ' . $month . ' order by cast(ins.product_id as unsigned) DESC')
                       ->get();
      $insentive_id = [];
      foreach ($insentive_details as $value) {
        $insentive_id[] = $value->insentive_id;
      }
      $orderRes = DB::table('gpff_order_list_batchwise as ol')
        ->join('gpff_order as o', 'o.order_id', 'ol.order_id')
        //->join('gpff_order_list_batchwise as ol', 'o.order_id', 'ol.order_id')
        ->join('gpff_product as p', 'ol.product_id', 'p.product_id')
        ->join('gpff_insentive as in', 'in.product_id', 'ol.product_id')
        ->whereNotIn('o.order_status',[0])
        ->where('field_officer_id', $filedOfficer)
        //->where('p.product_type', 1)
        ->where('in.insentive_type', 2)
        ->whereIn('in.insentive_id',$insentive_id)
        // ->whereRaw($prdIdQry . ' and DATE(o.created_at) >= DATE("'. $row->from_date .'") AND DATE(o.created_at) <= DATE("'. $row->to_date.'")')
        ->whereRaw('DATE(o.created_at) >= DATE("' . $insResult->from_date . '") AND DATE(o.created_at) <= DATE("' . $insResult->to_date . '")')
        ->select('o.order_id', 'ol.order_list_batchwise_id','o.field_officer_id', 'o.or_tot_price', 'ol.product_id', 'ol.product_qty', 'o.created_at', 'p.product_type', 'p.product_netprice', 'p.product_genericname', 'p.product_id','in.unit','in.insentive_amt')
        ->distinct('ol.order_list_batchwise_id')
        ->get();
        //print_r(json_encode($orderRes));
        $orderRes = $orderRes->unique('order_list_batchwise_id');
        $orderRes = array_slice($orderRes->values()->all(),true);

        $product_id = [];
        foreach($orderRes as $value){
          $product_id[] = $value->product_id;
        }

        $not_sale_products = DB::table('gpff_insentive as ins')
                            ->join('gpff_product as gp', 'ins.product_id', 'ins.product_id')
                            ->whereIn('ins.insentive_id',$insentive_id)
                            ->whereNotIn('ins.product_id',$product_id)
                            ->select('ins.insentive_id', 'ins.product_id', 'ins.unit', 'ins.insentive_amt', 'ins.from_date', 'ins.to_date', 'ins.product_name','gp.product_netprice')
                            ->get();

        // print_r(json_encode($orderRes));
        // exit;
      if (count($orderRes)) {
        foreach ($orderRes as $repRow) {

          if (isset($prdObject[$repRow->product_id])) {
            $prdObject[$repRow->product_id]['sale_unit'] = (int)$prdObject[$repRow->product_id]['sale_unit'] + (int)$repRow->product_qty;
            if($repRow->unit < $prdObject[$repRow->product_id]['sale_unit']){
              $qty = $prdObject[$repRow->product_id]['sale_unit'] - $repRow->unit;
              $prdObject[$repRow->product_id]['achived_incentive'] =$repRow->insentive_amt * $qty; 

            }
            $prdObject[$repRow->product_id]['total_sale_price'] = (int)$prdObject[$repRow->product_id]['sale_unit'] * $repRow->product_netprice;
              // print_r("expression");
              // exit;
          } else {
            // print_r("expression1");
            if($repRow->unit < $repRow->product_qty){
              $qty = $repRow->product_qty - $repRow->unit;
              $prdObject[$repRow->product_id] = [
                'pid' => $repRow->product_id,
                'pname' => $repRow->product_genericname,
                'sale_unit' => $repRow->product_qty,
                'unit_target' => $repRow->unit,
                'insentive_amt' => $repRow->insentive_amt,
                'pprice' => $repRow->product_netprice,
                'achived_incentive' => $repRow->insentive_amt * $qty,
                'total_sale_price' => $repRow->product_netprice * $repRow->product_qty
              ];
            }else{
              $prdObject[$repRow->product_id] = [
                'pid' => $repRow->product_id,
                'pname' => $repRow->product_genericname,
                'sale_unit' => $repRow->product_qty,
                'unit_target' => $repRow->unit,
                'insentive_amt' => $repRow->insentive_amt,
                'pprice' => $repRow->product_netprice,
                'achived_incentive' => 0,
                'total_sale_price' => $repRow->product_netprice * $repRow->product_qty
              ];
            }

            
          }
        }
        // print_r($prdObject);
        //     exit;

      } 
    }
    foreach($not_sale_products as $value){
        $prdObject[$value->product_id] = [
              'pid' => $value->product_id,
              'pname' => $value->product_name,
              'sale_unit' => 0,
              'unit_target' => $value->unit,
              'insentive_amt' => $value->insentive_amt,
              'pprice' => $value->product_netprice,
              'achived_incentive' => 0,
              'target_achived_price' => 0,
              'total_sale_price' => 0
            ];  
    }
    
    // return [$insResult];
    foreach ($prdObject as $key => $value) {
      array_push($array_ret, $value);
    }
    return $array_ret;
    //return $prdObject;

    foreach ($prdObject as $key => $value) {
      array_push($array_ret, $value);
    }
    return $array_ret;
  }

  private function getTargetBasedInsentive($req, $month)
  {
    $array_ret = [];
    $prdObject = [];
    // $datas = [];
    $totalSaleAmt = 0;
    $year = $req->year;
    $filedOfficer = $req->field_officer_id;

    // $insResult = DB::table('gpff_insentive')
    //   ->where('fo_id', $filedOfficer)
    //   ->where('insentive_type', 1)
    //   ->where('insentive_status', 1)
    //   ->whereRaw('year(from_date) = ' . $year . ' AND month(from_date) = ' . $month . ' order by cast(target_amt as unsigned) DESC')
    //   ->select('insentive_id', 'product_id', 'unit', 'insentive_amt', 'target_amt', 'from_date', 'to_date')
    //   ->First();

    $insResult = DB::table('gpff_insentive as ins')
      ->join('gpff_product as gp', 'ins.product_id', 'ins.product_id')
      ->where('ins.fo_id', $filedOfficer)
      ->where('ins.insentive_type', 1)
      ->where('ins.insentive_status', 1)
      ->whereRaw('year(ins.from_date) = ' . $year . ' AND month(ins.from_date) = ' . $month . ' order by cast(ins.product_id as unsigned) DESC')
      ->select('ins.insentive_id', 'ins.product_id', 'ins.unit','ins.insentive_amt', 'ins.from_date', 'ins.to_date','ins.target_amt',DB::Raw($totalSaleAmt . ' as totalsaleAmt1'), DB::RAW('(' . $totalSaleAmt . '/100) * ins.insentive_amt as totalsaleAmt'))
      // ->get();
      // ->orderBy('created_at', 'DESC')
      ->First();
    // print_r($insResult);

    if ($insResult) {
      //foreach($insResult as $row) {
      $orderRes = DB::table('gpff_order as o')
        ->join('gpff_order_list as ol', 'o.order_id', 'ol.order_id')
        ->join('gpff_product as p', 'ol.product_id', 'p.product_id')
        ->where('o.field_officer_id', $filedOfficer)
        ->where('ol.product_type', 1)
        ->whereRaw('DATE(o.created_at) >= DATE("' . $insResult->from_date . '") AND DATE(o.created_at) <= DATE("' . $insResult->to_date . '")')
        ->select('o.order_id', 'o.field_officer_id', 'o.or_tot_price', 'ol.product_id', 'ol.product_qty', 'o.created_at', 'p.product_type', 'p.product_netprice', 'p.product_genericname', 'p.product_id', 'ol.product_tot_price')
        ->get();

      if (count($orderRes)) {
        foreach ($orderRes as $repRow) {
          $totalSaleAmt = $totalSaleAmt + (int)$repRow->product_tot_price;
        }
      }
      //}
    }

    $condResult = DB::table('gpff_insentive')
      ->where('fo_id', $filedOfficer)
      ->where('insentive_type', 1)
      ->where('insentive_status', 1)
      ->where('target_amt', '<', $totalSaleAmt)
      ->whereRaw('year(from_date) = ' . $year . ' AND month(from_date) = ' . $month . ' order by cast(target_amt as unsigned) DESC limit 0,1')
      ->select('insentive_id', 'insentive_amt', 'target_amt', DB::Raw($totalSaleAmt . ' as totalsaleAmt1'), DB::RAW('(' . $totalSaleAmt . '/100) * insentive_amt as totalsaleAmt'))
      ->get();
    if (count($condResult)) {
      return (array)$condResult;
    } else {
      if(isset($insResult)){
          $prdObject[$insResult->product_id] = [
          'pid' => $insResult->product_id,
          'insentive_id' => $insResult->insentive_id,
          'target_amt' => $insResult->target_amt,
          'totalsaleAmt' => $insResult->totalsaleAmt,
          'totalsaleAmt1'=>0,
          'insentive_amt' => $insResult->insentive_amt
        ];
        // return [$insResult];
        foreach ($prdObject as $key => $value) {
          array_push($array_ret, $value);
        }
        return $array_ret;
      }else{
        return [];
      }
      // $ins = (array)$insResult;
      // // print_r($ins);
      // return $ins;
    }
  }

  private function getPercentageBasedInsentive($req, $month)
  {

    $array_ret = [];
    $prdObject = [];
    $totalSaleAmt = 0;
    $year = $req->year;
    $filedOfficer = $req->field_officer_id;

    // $insResult = DB::table('gpff_insentive')
    //   ->where('fo_id', $filedOfficer)
    //   ->where('insentive_type', 3)
    //   ->where('insentive_status', 1)
    //   ->whereRaw('year(from_date) = ' . $year . ' AND month(from_date) = ' . $month . ' order by cast(target_amt as unsigned) DESC')
    //   ->select('insentive_id', 'product_id', 'unit', 'insentive_amt', 'target_amt', 'from_date', 'to_date')
    //   //->get();
    //   ->First();

    $insResult = DB::table('gpff_insentive as ins')
      ->join('gpff_product as gp', 'ins.product_id', 'ins.product_id')
      ->where('ins.fo_id', $filedOfficer)
      ->where('ins.insentive_type', 3)
      ->where('ins.insentive_status', 1)
      ->whereRaw('year(ins.from_date) = ' . $year . ' AND month(ins.from_date) = ' . $month . ' order by cast(ins.product_id as unsigned) DESC')
      ->select('ins.insentive_id', 'ins.product_id', 'ins.unit', 'ins.insentive_amt', 'ins.from_date', 'ins.to_date','ins.target_amt','ins.product_name','gp.product_netprice',DB::RAW('(' . $totalSaleAmt . '/100) * ins.insentive_amt as totalsaleAmt'))
      // ->get();
      // ->orderBy('created_at', 'DESC')
      ->First();

    if ($insResult) {
      //foreach($insResult as $row) {
      $orderRes = DB::table('gpff_order as o')
        ->join('gpff_order_list as ol', 'o.order_id', 'ol.order_id')
        ->join('gpff_product as p', 'ol.product_id', 'p.product_id')
        ->where('o.field_officer_id', $filedOfficer)
        ->where('ol.product_type', 2)
        ->whereRaw('DATE(o.created_at) >= DATE("' . $insResult->from_date . '") AND DATE(o.created_at) <= DATE("' . $insResult->to_date . '")')
        ->select('o.order_id', 'o.field_officer_id', 'o.or_tot_price', 'ol.product_id', 'ol.product_qty', 'o.created_at', 'p.product_type', 'p.product_netprice', 'p.product_genericname', 'p.product_id', 'ol.product_tot_price')
        ->get();

      if (count($orderRes)) {
        foreach ($orderRes as $repRow) {
          $totalSaleAmt = $totalSaleAmt + (int)$repRow->product_tot_price;
        }
      }
      //}
    }

    $condResult = DB::table('gpff_insentive')
      ->where('fo_id', $filedOfficer)
      ->where('insentive_type', 3)
      ->where('insentive_status', 1)
      ->where('target_amt', '<', $totalSaleAmt)
      ->whereRaw('year(from_date) = ' . $year . ' AND month(from_date) = ' . $month . ' order by cast(target_amt as unsigned) DESC limit 0,1')
      ->select('insentive_id', 'insentive_amt', 'target_amt', DB::Raw($totalSaleAmt . ' as totalsaleAmt1'), DB::RAW('(' . $totalSaleAmt . '/100) * insentive_amt as totalsaleAmt'))
      ->get();
    if (count($condResult)) {
      return (array)$condResult;
    } else {
        if(isset($insResult)){
            $prdObject[$insResult->product_id] = [
              'pid' => $insResult->product_id,
              'pname' => $insResult->product_name,
              'insentive_id' => $insResult->insentive_id, 
              'insentive_amt' => $insResult->insentive_amt,
              'totalsaleAmt1' => 0,
              'totalsaleAmt' =>$insResult->totalsaleAmt,
              'target_amt' => $insResult->target_amt,
              'pprice' => $insResult->product_netprice

          ];
          // return [$insResult];
          foreach ($prdObject as $key => $value) {
            array_push($array_ret, $value);
          }
          return $array_ret;
      }else{
        return [];
      }
      // return (array)$insResult;
          //       'pid' => $insResult->product_id,
          // 'insentive_id' => $insResult->insentive_id,
          // 'target_amt' => $insResult->target_amt,
          // 'totalsaleAmt' => $insResult->totalsaleAmt,
          // 'totalsaleAmt1'=>0,
          // 'insentive_amt' => $insResult->insentive_amt
    }
  }


  // public function removeNull($var){
  //   return ($var !== NULL && $var !== FALSE && $var !== "");
  // }
  //Get Current Year Samples Report
  public function getCurrentYearSamplesReport()
  {
    $samples = DB::table('gpff_delivered_samples as gdeliversample')
      ->join('gpff_samples as gsample', 'gsample.samples_id', 'gdeliversample.samples_id')
      ->join('gpff_customer as gcustomer', 'gdeliversample.customer_id', 'gcustomer.customer_id')
      ->join('gpff_branch as gbranch', 'gsample.branch_id', 'gbranch.branch_id')
      ->join('gpff_region as gregion', 'gsample.region_id', 'gregion.region_id')
      ->whereYear('gdeliversample.created_at', date('Y'))
      ->orderBy('gdeliversample.created_at', 'DESC')
      ->get(['gdeliversample.samples_id', 'gdeliversample.created_at', 'gdeliversample.samples_name', 'gdeliversample.samples_qty', 'gdeliversample.field_officer_name', 'gdeliversample.field_officer_name', 'gcustomer.customer_name', 'gcustomer.customer_type', 'gbranch.branch_name', 'gregion.region_name', 'gsample.product_type']);

    //Promotional Product Chart Count
    $promotional = [];
    for ($i = 1; $i <= 12; $i++) {

      $res_promotional = DB::table('gpff_delivered_samples as gdeliversample')
        ->join('gpff_samples as gsample', 'gsample.samples_id', 'gdeliversample.samples_id')
        ->whereYear('gdeliversample.created_at', date('Y'))
        ->whereMonth('gdeliversample.created_at', $i)
        ->where('gsample.product_type', '1')
        ->sum('gdeliversample.samples_qty');

      array_push($promotional, $res_promotional);
    }

    //Generic Product Chart Count
    $generic = [];
    for ($i = 1; $i <= 12; $i++) {

      $res_generic = DB::table('gpff_delivered_samples as gdeliversample')
        ->join('gpff_samples as gsample', 'gsample.samples_id', 'gdeliversample.samples_id')
        ->whereYear('gdeliversample.created_at', date('Y'))
        ->whereMonth('gdeliversample.created_at', $i)
        ->where('gsample.product_type', '2')
        ->sum('gdeliversample.samples_qty');

      array_push($generic, $res_generic);
    }

    //Doctor Based Promotional Product in Pie Chart Count
    $DoctorBasPromo = [];
    $DoctorBasGeneric = [];
    $DoctorBas = DB::table('gpff_delivered_samples')
      ->whereYear('created_at', date('Y'))
      ->distinct('customer_id')
      ->get(['customer_id']);

    foreach ($DoctorBas as $value) {

      $Doctor = [];

      $name =     DB::table('gpff_customer')
        ->where('customer_id', $value->customer_id)
        ->First();
      if ($name) {
        $Doctor['Name'] = $name->customer_name;
      }

      $Doctor['Promo_Count'] = DB::table('gpff_delivered_samples as gdeliversample')
        ->join('gpff_samples as gsample', 'gsample.samples_id', 'gdeliversample.samples_id')
        ->where('gsample.product_type', '1')
        ->where('gdeliversample.customer_id', $value->customer_id)
        ->sum('gdeliversample.samples_qty');

      array_push($DoctorBasPromo, $Doctor);
    }

    foreach ($DoctorBas as $value) {

      $Doctor = [];

      $name =     DB::table('gpff_customer')
        ->where('customer_id', $value->customer_id)
        ->First();

      if ($name) {
        $Doctor['Name'] = $name->customer_name;
      }

      $Doctor['Promo_Count'] = DB::table('gpff_delivered_samples as gdeliversample')
        ->join('gpff_samples as gsample', 'gsample.samples_id', 'gdeliversample.samples_id')
        ->where('gsample.product_type', '2')
        ->where('gdeliversample.customer_id', $value->customer_id)
        ->sum('gdeliversample.samples_qty');

      array_push($DoctorBasGeneric, $Doctor);
    }

    $total_samples_deliveried =  DB::table('gpff_delivered_samples')
      ->whereYear('created_at', date('Y'))
      ->sum('samples_qty');

    $total_promotional_samples_deliveried =  DB::table('gpff_delivered_samples as gdeliversample')
      ->join('gpff_samples as gsample', 'gsample.samples_id', 'gdeliversample.samples_id')
      ->where('gsample.product_type', '1')
      ->whereYear('gdeliversample.created_at', date('Y'))
      ->sum('gdeliversample.samples_qty');

    $total_generic_samples_deliveried =  DB::table('gpff_delivered_samples as gdeliversample')
      ->join('gpff_samples as gsample', 'gsample.samples_id', 'gdeliversample.samples_id')
      ->where('gsample.product_type', '2')
      ->whereYear('gdeliversample.created_at', date('Y'))
      ->sum('gdeliversample.samples_qty');

    $result = array_merge(
      ['sample_details' => $samples->toArray()],
      ['promotional_chart_count' => $promotional],
      ['generic_chart_count' => $generic],
      ['doctorbased_promo_product' => $DoctorBasPromo],
      ['doctorbased_generic_product' => $DoctorBasGeneric],
      ['total_samples_deliveried' => $total_samples_deliveried],
      ['promotion_samples_delivered' => $total_promotional_samples_deliveried],
      ['generic_samples_delivered' => $total_generic_samples_deliveried]
    );

    return $result;
  }


  //Get Filter Based Sales Report
  public function getFilterSamplesReport($report)
  {
    $samples = DB::table('gpff_delivered_samples as gdeliversample')
      ->join('gpff_customer as gcustomer', 'gdeliversample.customer_id', 'gcustomer.customer_id')
      ->join('gpff_branch as gbranch', 'gcustomer.branch_id', 'gbranch.branch_id')
      ->join('gpff_region as gregion', 'gcustomer.region_id', 'gregion.region_id')
      ->whereBetween(DB::RAW('date(gdeliversample.created_at)'), [date($report->start_date), date($report->end_date)]);

    if ($report->branch_id != '') {
      $samples = $samples->whereIn('gcustomer.branch_id', $report->branch_id);
    }
    if ($report->region_id != '') {
      $samples = $samples->whereIn('gcustomer.region_id', $report->region_id);
    }
    // echo $report->area_id; 
    if ($report->area_id != '') {
      $samples = $samples->whereIn('gcustomer.area_id', $report->area_id);
    }

    if ($report->customer_id != '') {
      $samples = $samples->whereIn('gdeliversample.customer_id', $report->customer_id);
    }
    if ($report->field_officer_id != '') {
      $samples = $samples->whereIn('gdeliversample.field_officer_id', $report->field_officer_id);
    }

    if ($report->sample_id != '') {
      $samples = $samples->whereIn('gdeliversample.product_id', $report->sample_id);
    }

    switch($report->report_type){
      case 1: //Data table list
        $samples_det = $samples->orderBy('gdeliversample.created_at', 'DESC')->get(['gdeliversample.fo_received_id', 'gdeliversample.created_at', 'gdeliversample.samples_name', 'gdeliversample.samples_qty','gdeliversample.batch_id', 'gdeliversample.field_officer_name','gcustomer.customer_name', 'gcustomer.customer_type', 'gbranch.branch_name', 'gregion.region_name']);
      break;
      case 2: // Product Wise Data
        $total_qty = $samples->sum('gdeliversample.samples_qty');
        $samples_det = $samples->join('gpff_product as gp','gdeliversample.product_id','gp.product_id')
          ->groupBy('gdeliversample.product_id')
          ->get(['gdeliversample.product_id','gp.product_genericname',DB::raw('sum(gdeliversample.samples_qty) as sample_qty')]);
        $product_data = [];
        foreach($samples_det as $product)
        {
          $product_data[] =  (array) $product;
        }
        $product_wise = usort($product_data, function($a, $b) {
                       return ($a['sample_qty'] > $b['sample_qty']? -1 : 1);
                     });
        $result = array(
          "product_wise"=>$product_data,
          "total_qty"=> $total_qty
        );
        $samples_det = $result;
      break;
      case 3: // Doctor Wise Data
        $total_qty = $samples->sum('gdeliversample.samples_qty');
        $samples_det = $samples->where('gcustomer.customer_type',1)
        ->groupBy('gdeliversample.customer_id')->get(['gdeliversample.customer_id','gcustomer.customer_name',DB::raw('sum(gdeliversample.samples_qty) as sample_qty')]);
        $cus_data = [];
        foreach($samples_det as $customer)
        {
          $cus_data[] =  (array) $customer;
        }
        $customer_wise = usort($cus_data, function($a, $b) {
                       return ($a['sample_qty'] > $b['sample_qty']? -1 : 1);
                     });
        $result = array(
          "customer_wise"=>$cus_data,
          "total_qty"=> $total_qty
        );
        $samples_det = $result;
      break;
      case 4: // Pharmacy Wise Data
        $total_qty = $samples->sum('gdeliversample.samples_qty');
        $samples_det = $samples->where('gcustomer.customer_type',2)->groupBy('gdeliversample.customer_id')->get(['gdeliversample.customer_id','gcustomer.customer_name',DB::raw('sum(gdeliversample.samples_qty) as sample_qty')]);
        $cus_data = [];
        foreach($samples_det as $customer)
        {
          $cus_data[] =  (array) $customer;
        }
        $customer_wise = usort($cus_data, function($a, $b) {
                       return ($a['sample_qty'] > $b['sample_qty']? -1 : 1);
                     });
        $result = array(
          "customer_wise"=>$cus_data,
          "total_qty"=> $total_qty
        );
        $samples_det = $result;
      break;
      case 5: // Hospital Wise Data
        $total_qty = $samples->sum('gdeliversample.samples_qty');
        $samples_det = $samples->where('gcustomer.customer_type',3)->groupBy('gdeliversample.customer_id')->get(['gdeliversample.customer_id','gcustomer.customer_name',DB::raw('sum(gdeliversample.samples_qty) as sample_qty')]);
        $cus_data = [];
        foreach($samples_det as $customer)
        {
          $cus_data[] =  (array) $customer;
        }
        $customer_wise = usort($cus_data, function($a, $b) {
                       return ($a['sample_qty'] > $b['sample_qty']? -1 : 1);
                     });
        $result = array(
          "customer_wise"=>$cus_data,
          "total_qty"=> $total_qty
        );
        $samples_det = $result;
      break;
      case 6: // FO Wise Data
        $total_qty = $samples->sum('gdeliversample.samples_qty');
        $samples_det = $samples->join('gpff_users as gu','gdeliversample.field_officer_id','gu.user_id')
          ->groupBy('gdeliversample.field_officer_id')
          ->get(['gdeliversample.field_officer_id','gu.firstname',DB::raw('sum(gdeliversample.samples_qty) as sample_qty')]);
        $fo_data = [];
        foreach($samples_det as $fo)
        {
          $fo_data[] =  (array) $fo;
        }
        $fo_wise = usort($fo_data, function($a, $b) {
                       return ($a['sample_qty'] > $b['sample_qty']? -1 : 1);
                     });
        $result = array(
          "fo_wise"=>$fo_data,
          "total_qty"=> $total_qty
        );
        $samples_det = $result;
      break;
      case 7: // Samples Data
        $samples_det = $samples->join('gpff_product as gp','gdeliversample.product_id','gp.product_id')
          ->groupBy('gdeliversample.product_id')
          ->get(['gdeliversample.product_id','gp.product_genericname']);
      break;
    }
    return $samples_det;
  }


  //Get Current Year Gift Report
  public function getCurrentYearGiftReport()
  {
    $gift = DB::table('gpff_delivered_gift as gdeliveredgift')
      ->join('gpff_gift as ggift', 'ggift.gift_id', 'gdeliveredgift.gift_id')
      ->join('gpff_customer as gcustomer', 'gdeliveredgift.customer_id', 'gcustomer.customer_id')
      ->join('gpff_branch as gbranch', 'ggift.branch_id', 'gbranch.branch_id')
      ->join('gpff_region as gregion', 'ggift.region_id', 'gregion.region_id')
      ->whereYear('gdeliveredgift.created_at', date('Y'))
      ->orderBy('gdeliveredgift.created_at', 'DESC')
      ->get(['gdeliveredgift.gift_id', 'gdeliveredgift.created_at', 'gdeliveredgift.gift_name', 'gdeliveredgift.gift_qty', 'gdeliveredgift.field_officer_name', 'gcustomer.customer_name', 'gcustomer.customer_type', 'gbranch.branch_name', 'gregion.region_name']);

    //Delivered Gift Bar Chart Count
    $gift_delivered = [];
    for ($i = 1; $i <= 12; $i++) {

      $res_gift = DB::table('gpff_delivered_gift as gdeliveredgift')
        ->join('gpff_gift as ggift', 'ggift.gift_id', 'gdeliveredgift.gift_id')
        ->whereYear('gdeliveredgift.created_at', date('Y'))
        ->whereMonth('gdeliveredgift.created_at', $i)
        ->sum('gdeliveredgift.gift_qty');

      array_push($gift_delivered, $res_gift);
    }

    //Doctor Based Gift in Pie Chart Count
    $DoctorBasGift = [];
    $DoctorBas = DB::table('gpff_delivered_gift')
      ->whereYear('created_at', date('Y'))
      ->distinct('customer_id')
      ->get(['customer_id']);

    foreach ($DoctorBas as $value) {

      $Doctor = [];
      $name =     DB::table('gpff_customer')
        ->where('customer_id', $value->customer_id)
        ->where('customer_type', 1)
        ->First();

      if (isset($name->customer_name)) {

        $Doctor['Name'] = $name->customer_name;

        $Doctor['Promo_Count'] = DB::table('gpff_delivered_gift')
          ->where('customer_id', $value->customer_id)
          ->sum('gift_qty');

        array_push($DoctorBasGift, $Doctor);
      }
    }
    //Pharma Based Gift in Pie Chart Count
    $PharmaBasGift = [];
    $PharmaBas = DB::table('gpff_delivered_gift')
      ->whereYear('created_at', date('Y'))
      ->distinct('customer_id')
      ->get(['customer_id']);


    foreach ($PharmaBas as $value) {

      $Pharmacy = [];

      $name =     DB::table('gpff_customer')
        ->where('customer_id', $value->customer_id)
        ->where('customer_type', 2)
        ->First();

      if (isset($name->customer_name)) {

        $Pharmacy['Name'] = $name->customer_name;

        $Pharmacy['Promo_Count'] = DB::table('gpff_delivered_gift')
          ->where('customer_id', $value->customer_id)
          ->sum('gift_qty');

        array_push($PharmaBasGift, $Pharmacy);
      }
    }
    //Hospital Based Gift in Pie Chart Count
    $HospitalBasGift = [];
    $HospitalBas = DB::table('gpff_delivered_gift')
      ->whereYear('created_at', date('Y'))
      ->distinct('customer_id')
      ->get(['customer_id']);

    foreach ($HospitalBas as $value) {

      $Hospital = [];

      $name =     DB::table('gpff_customer')
        ->where('customer_id', $value->customer_id)
        ->where('customer_type', 3)
        ->First();

      if (isset($name->customer_name)) {

        $Hospital['Name'] = $name->customer_name;

        $Hospital['Promo_Count'] = DB::table('gpff_delivered_gift')
          ->where('customer_id', $value->customer_id)
          ->sum('gift_qty');

        array_push($HospitalBasGift, $Hospital);
      }
    }


    $total_gift_deliveried =  DB::table('gpff_delivered_gift')
      ->whereYear('created_at', date('Y'))
      ->sum('gift_qty');

    $result = array_merge(
      ['gift_details' => $gift->toArray()],
      ['gift_delivered_chart_count' => $gift_delivered],
      ['doctorbased_gift' => $DoctorBasGift],
      ['pharmacybased_gift' => $PharmaBasGift],
      ['hospitalbased_gift' => $HospitalBasGift],
      ['total_gift_deliveried' => $total_gift_deliveried]
    );

    return $result;
  }


  //Get Filter Based Gift Report
  public function getFilterGiftReport($report)
  {
    $gift = DB::table('gpff_delivered_gift as gdeliveredgift')
      ->join('gpff_gift as ggift', 'ggift.gift_id', 'gdeliveredgift.gift_id')
      ->join('gpff_customer as gcustomer', 'gdeliveredgift.customer_id', 'gcustomer.customer_id')
      ->join('gpff_branch as gbranch', 'ggift.branch_id', 'gbranch.branch_id')
      ->join('gpff_region as gregion', 'ggift.region_id', 'gregion.region_id')
      ->whereBetween(DB::RAW('date(gdeliveredgift.created_at)'), [date($report->start_date), date($report->end_date)]);
    if ($report->branch_id != '') {
      $gift = $gift->whereIn('ggift.branch_id', $report->branch_id);
    }
    if ($report->region_id != '') {
      $gift = $gift->whereIn('ggift.region_id', $report->region_id);
    }
    if ($report->customer_id != '') {
      $gift = $gift->whereIn('gdeliveredgift.customer_id', $report->customer_id);
    }
    if ($report->field_officer_id != '') {
      $gift = $gift->whereIn('gdeliveredgift.field_officer_id', $report->field_officer_id);
    }
    if ($report->gift_id != '') {
      $gift = $gift->whereIn('gdeliveredgift.gift_id', $report->gift_id);
    }
    switch($report->type){
      case 1:
        $gift_det = $gift->orderBy('gdeliveredgift.created_at', 'DESC')->get(['gdeliveredgift.gift_id', 'gdeliveredgift.created_at', 'gdeliveredgift.gift_name', 'gdeliveredgift.gift_qty', 'gdeliveredgift.field_officer_name', 'gcustomer.customer_name', 'gcustomer.customer_type', 'gbranch.branch_name', 'gregion.region_name']);
        $result = array_merge(
          ['gift_details' => $gift_det->toArray()]
        );
      break;
      case 2: // Product Wise Data
        $total_qty = $gift->sum('gdeliveredgift.gift_qty');
        $samples_det = $gift->groupBy('gdeliveredgift.gift_id')
          ->get(['gdeliveredgift.gift_id','ggift.gift_name',DB::raw('sum(gdeliveredgift.gift_qty) as gift_qty')]);
        $product_data = [];
        foreach($samples_det as $product)
        {
          $product_data[] =  (array) $product;
        }
        $product_wise = usort($product_data, function($a, $b) {
                       return ($a['gift_qty'] > $b['gift_qty']? -1 : 1);
                     });
        $result = array(
          "product_wise"=>$product_data,
          "total_qty"=> $total_qty
        );
      break;
      case 3: // Doctor Wise Data
        $total_qty = $gift->sum('gdeliveredgift.gift_qty');
        $gift_det = $gift->where('gcustomer.customer_type',1)
        ->groupBy('gdeliveredgift.customer_id')->get(['gdeliveredgift.customer_id','gcustomer.customer_name',DB::raw('sum(gdeliveredgift.gift_qty) as gift_qty')]);
        $cus_data = [];
        foreach($gift_det as $customer)
        {
          $cus_data[] =  (array) $customer;
        }
        $customer_wise = usort($cus_data, function($a, $b) {
                       return ($a['gift_qty'] > $b['gift_qty']? -1 : 1);
                     });
        $result = array(
          "customer_wise"=>$cus_data,
          "total_qty"=> $total_qty
        );
      break;
      case 4: // Pharmacy Wise Data
        $total_qty = $gift->sum('gdeliveredgift.gift_qty');
        $gift_det = $gift->where('gcustomer.customer_type',2)
        ->groupBy('gdeliveredgift.customer_id')->get(['gdeliveredgift.customer_id','gcustomer.customer_name',DB::raw('sum(gdeliveredgift.gift_qty) as gift_qty')]);
        $cus_data = [];
        foreach($gift_det as $customer)
        {
          $cus_data[] =  (array) $customer;
        }
        $customer_wise = usort($cus_data, function($a, $b) {
                       return ($a['gift_qty'] > $b['gift_qty']? -1 : 1);
                     });
        $result = array(
          "customer_wise"=>$cus_data,
          "total_qty"=> $total_qty
        );
      break;
      case 5: // Hospital Wise Data
        $total_qty = $gift->sum('gdeliveredgift.gift_qty');
        $gift_det = $gift->where('gcustomer.customer_type',3)
        ->groupBy('gdeliveredgift.customer_id')->get(['gdeliveredgift.customer_id','gcustomer.customer_name',DB::raw('sum(gdeliveredgift.gift_qty) as gift_qty')]);
        $cus_data = [];
        foreach($gift_det as $customer)
        {
          $cus_data[] =  (array) $customer;
        }
        $customer_wise = usort($cus_data, function($a, $b) {
                       return ($a['gift_qty'] > $b['gift_qty']? -1 : 1);
                     });
        $result = array(
          "customer_wise"=>$cus_data,
          "total_qty"=> $total_qty
        );
      break;
      case 6: // FO Wise Data
        $total_qty = $gift->sum('gdeliveredgift.gift_qty');
        $gift_det = $gift->join('gpff_users as gu','gdeliveredgift.field_officer_id','gu.user_id')
          ->groupBy('gdeliveredgift.field_officer_id')
          ->get(['gdeliveredgift.field_officer_id','gu.firstname',DB::raw('sum(gdeliveredgift.gift_qty) as gift_qty')]);
        $fo_data = [];
        foreach($gift_det as $fo)
        {
          $fo_data[] =  (array) $fo;
        }
        $fo_wise = usort($fo_data, function($a, $b) {
                       return ($a['gift_qty'] > $b['gift_qty']? -1 : 1);
                     });
        $result = array(
          "fo_wise"=>$fo_data,
          "total_qty"=> $total_qty
        );
      break;
      case 7: // Samples Data
        $gift_det = $gift->groupBy('gdeliveredgift.gift_id')
          ->get(['gdeliveredgift.gift_id','ggift.gift_name']);
        $result = array(
          "gift_det"=>$gift_det
        );
      break;
      
    }
    return $result;
  }
  //APP SIDE Report WITH PDF GENERATION
  //1-Daily Call Report
  public function appDailyCallReport($report)
  {

    $today = date('Y-m-d');
    $today1 = date('Y-m-d');
    $today = Carbon::now()->format('Y-m-d') . '%';

    $orders = DB::table('gpff_task as gta')
      ->join('gpff_customer as gcus', 'gta.customer_id', '=', 'gcus.customer_id')
      ->join('gpff_users as guse', 'gta.field_officer_id', '=', 'guse.user_id')
      ->join('gpff_branch as gpbr', 'gta.branch_id', '=', 'gpbr.branch_id')
      ->join('gpff_region as greg', 'gta.region_id', '=', 'greg.region_id')
      ->join('gpff_area as gpar', 'gta.area_id', '=', 'gpar.area_id')
      ->whereBetween(DB::RAW('date(gta.created_at)'), [date($report->start_date), date($report->end_date)])
      //->where('gta.task_date_time', 'like', $today)
      ->orderBy('gta.created_at', 'DESC');

    if ($report->branch_id != '') {
      $orders = $orders->whereIn('gta.branch_id', $report->branch_id);
    }
    if ($report->region_id != '') {
      $orders = $orders->whereIn('gta.region_id', $report->region_id);
    }
    if ($report->area_id != '') {
      $orders = $orders->whereIn('gta.area_id', $report->area_id);
    }
    if ($report->field_officer_id != '') {
      $orders = $orders->whereIn('gta.field_officer_id', $report->field_officer_id);
    }

    $order_det = $orders->get(['gta.task_id', 'gta.created_by', 'gta.field_officer_id', 'guse.firstname', 'gta.customer_id', 'gta.area_manager_id', 'gta.task_title', 'gta.task_desc', 'gta.task_status', 'gta.task_date_time', 'gta.task_reassign_status', 'gta.task_start_date_time', 'gta.task_end_date_time', 'gta.task_active_minu', 'gta.notes', 'gta.created_at', 'gta.updated_at', 'gcus.customer_name', 'gcus.customer_email', 'gcus.country_code', 'gcus.customer_mobile', 'gcus.customer_city', 'gcus.customer_country', 'gcus.customer_fax', 'gcus.customer_address', 'gcus.customer_location', 'gcus.customer_lan', 'gcus.customer_lat', 'gcus.customer_cr_id', 'gcus.website', 'gcus.avatar', 'gcus.is_active', 'gcus.customer_type', 'gta.branch_id', 'gta.region_id', 'gta.area_id', 'gpbr.branch_name', 'greg.region_name', 'gpar.area_name']);

    if (count($order_det) > 0) {
      $customPaper = array(0, 0, 767.00, 883.80);

      view()->share('datas', $order_det);
      view()->share('Start_date', $today1);
      view()->share('End_date', $today1);
      view()->share('Title', 'Daily Call Report');
      $pdf = PDF::loadView('DailyCallReport')
        ->setPaper('a4', 'landscape');
      $pdf->save(public_path('Report/DailyCallReport.pdf'));

      $file_name = 'DailyCallReport.pdf';
      $name = $file_name;
      $filePath = 'Report/DailyCallReport/' . $name;

      Storage::disk('s3')->put($filePath, file_get_contents(public_path('Report') . "/" . $file_name));

      $this->sendReportToEmail($report->email_id, $file_name, $filePath);

      return 1;
    } else {
      return 2;
    }
  }
  //2-App Visited Report
  public function appVisitedReport($report)
  {
    $visited = DB::table('gpff_task as gta')
      ->join('gpff_customer as gcus', 'gta.customer_id', '=', 'gcus.customer_id')
      ->join('gpff_users as guse', 'gta.field_officer_id', '=', 'guse.user_id')
      ->join('gpff_branch as gpbr', 'gta.branch_id', '=', 'gpbr.branch_id')
      ->join('gpff_region as greg', 'gta.region_id', '=', 'greg.region_id')
      ->join('gpff_area as gpar', 'gta.area_id', '=', 'gpar.area_id')
      ->where('gta.task_status', 3)
      ->orderBy('gta.created_at', 'DESC');

    if ($report->branch_id != '') {
      $visited = $visited->whereIn('gta.branch_id', $report->branch_id);
    }
    if ($report->region_id != '') {
      $visited = $visited->whereIn('gta.region_id', $report->region_id);
    }
    if ($report->area_id != '') {
      $visited = $visited->whereIn('gta.area_id', $report->area_id);
    }
    if ($report->field_officer_id != '') {
      $visited = $visited->whereIn('gta.field_officer_id', $report->field_officer_id);
    }
    if ($report->year != '') {
      $visited = $visited->whereYear("gta.created_at", $report->year);
    }
    if ($report->months != '') {
      $visited = $visited->whereIn(DB::raw('MONTH(gta.created_at)'), $report->months);
    }

    $visited_de = $visited->get(['gta.task_id', 'gta.created_by', 'gta.field_officer_id', 'guse.firstname', 'gta.customer_id', 'gta.area_manager_id', 'gta.task_title', 'gta.task_desc', 'gta.task_status', 'gta.task_date_time', 'gta.task_reassign_status', 'gta.task_start_date_time', 'gta.task_end_date_time', 'gta.task_active_minu', 'gta.notes', 'gta.created_at', 'gta.updated_at', 'gcus.customer_name', 'gcus.customer_email', 'gcus.country_code', 'gcus.customer_mobile', 'gcus.customer_city', 'gcus.customer_country', 'gcus.customer_fax', 'gcus.customer_address', 'gcus.customer_location', 'gcus.customer_lan', 'gcus.customer_lat', 'gcus.customer_cr_id', 'gcus.website', 'gcus.avatar', 'gcus.customer_type', 'gcus.is_active', 'gta.branch_id', 'gta.region_id', 'gta.area_id', 'gpbr.branch_name', 'greg.region_name', 'gpar.area_name']);

    /*print_r($visited_de);
      exit;*/
    if (count($visited_de) > 0) {
      $customPaper = array(0, 0, 767.00, 883.80);

      view()->share('datas', $visited_de);
      view()->share('Month', $report->year);
      view()->share('Title', 'Visited Report');
      $pdf = PDF::loadView('VisitedReport')
        ->setPaper('a4', 'landscape');
      $pdf->save(public_path('Report/VisitedReport.pdf'));

      $file_name = 'VisitedReport.pdf';
      $name = $file_name;
      $filePath = 'Report/VisitedReport/' . $name;

      Storage::disk('s3')->put($filePath, file_get_contents(public_path('Report') . "/" . $file_name));

      $this->sendReportToEmail($report->email_id, $file_name, $filePath);

      return 1;
    } else {
      return 2;
    }
  }

  /// 3-App Sales Report
  public function appSalesReport($report)
  {
    $orders = DB::table('gpff_order as gorder')
      ->join('gpff_order_list as gorderlist', 'gorderlist.order_id', 'gorder.order_id')
      ->join('gpff_branch as gbranch', 'gorder.branch_id', 'gbranch.branch_id')
      ->join('gpff_region as gregion', 'gorder.region_id', 'gregion.region_id')
      ->join('gpff_area as garea', 'gorder.area_id', 'garea.area_id')
      ->orderBy('gorder.created_at', 'DESC');

    if ($report->branch_id != '') {
      $orders = $orders->whereIn('gorder.branch_id', $report->branch_id);
    }
    if ($report->region_id != '') {
      $orders = $orders->whereIn('gorder.region_id', $report->region_id);
    }
    if ($report->area_id != '') {
      $orders = $orders->whereIn('gorder.area_id', $report->area_id);
    }
    if ($report->customer_id != '') {
      $orders = $orders->whereIn('gorder.customer_id', $report->customer_id);
    }
    if ($report->field_officer_id != '') {
      $orders = $orders->whereIn('gorder.field_officer_id', $report->field_officer_id);
    }
    if ($report->product_id != '') {
      $orders = $orders->whereIn('gorderlist.product_id', $report->product_id);
    }
    if ($report->year != '') {
      $orders = $orders->whereYear("gorder.created_at", $report->year);
    }
    if ($report->months != '') {
      $orders = $orders->whereIn(DB::raw('MONTH(gorder.created_at)'), $report->months);
    }

    $order_det = $orders->get(['gorder.order_id', 'gorder.order_date', 'gorder.customer_name', 'gorderlist.product_genericname', 'gorderlist.product_qty', 'gorder.field_officer_name', 'gorderlist.product_type', 'gbranch.branch_name', 'gregion.region_name', 'garea.area_name', 'gorderlist.product_tot_price']);

    if (count($order_det) > 0) {
      $customPaper = array(0, 0, 767.00, 883.80);

      view()->share('datas', $order_det);
      view()->share('Month', $report->year);
      view()->share('Title', 'Sales Report');
      $pdf = PDF::loadView('SalesReport')
        ->setPaper('a4', 'landscape');
      $pdf->save(public_path('Report/SalesReport.pdf'));

      $file_name = 'SalesReport.pdf';
      $name = $file_name;
      $filePath = 'Report/SalesReport/' . $name;

      Storage::disk('s3')->put($filePath, file_get_contents(public_path('Report') . "/" . $file_name));

      $this->sendReportToEmail($report->email_id, $file_name, $filePath);

      return 1;
    } else {
      return 2;
    }
  }

  ///4-App coverage Report
  public function appCoverageReport($report)
  {
    $task = DB::table('gpff_task as gtask')
      ->join('gpff_customer as gcustomer', 'gcustomer.customer_id', 'gtask.customer_id')
      ->join('gpff_users as gusers', 'gtask.field_officer_id', 'gusers.user_id')
      ->join('gpff_branch as gbranch', 'gbranch.branch_id', 'gtask.branch_id')
      ->join('gpff_region as gregion', 'gtask.region_id', 'gregion.region_id')
      ->join('gpff_area as garea', 'gtask.area_id', 'garea.area_id')
      ->orderBy('gtask.task_date_time', 'DESC');

    if ($report->branch_id != '') {
      $task = $task->whereIn('gtask.branch_id', $report->branch_id);
    }
    if ($report->region_id != '') {
      $task = $task->whereIn('gtask.region_id', $report->region_id);
    }
    if ($report->area_id != '') {
      $task = $task->whereIn('gtask.area_id', $report->area_id);
    }
    if ($report->customer_id != '') {
      $task = $task->whereIn('gtask.customer_id', $report->customer_id);
    }
    if ($report->field_officer_id != '') {
      $task = $task->whereIn('gtask.field_officer_id', $report->field_officer_id);
    }
    if ($report->year != '') {
      $task = $task->whereYear("gtask.task_date_time", $report->year);
    }
    if ($report->months != '') {
      $task = $task->whereIn(DB::raw('MONTH(gtask.task_date_time)'), $report->months);
    }

    $res_task = $task->get(['gtask.task_id', 'gtask.task_title', 'gcustomer.customer_name', 'gtask.task_date_time', 'gtask.task_status', 'gusers.firstname', 'gusers.lastname', 'gbranch.branch_name', 'gregion.region_name', 'garea.area_name', 'gtask.customer_type', 'gcustomer.customer_location']);

    if (count($res_task) > 0) {
      $customPaper = array(0, 0, 767.00, 883.80);

      view()->share('datas', $res_task);
      view()->share('Month', $report->year);
      view()->share('Title', 'Coverage Report');
      $pdf = PDF::loadView('CoverageReport')
        ->setPaper('a4', 'landscape');
      $pdf->save(public_path('Report/CoverageReport.pdf'));

      $file_name = 'CoverageReport.pdf';
      $name = $file_name;
      $filePath = 'Report/CoverageReport/' . $name;

      Storage::disk('s3')->put($filePath, file_get_contents(public_path('Report') . "/" . $file_name));

      $this->sendReportToEmail($report->email_id, $file_name, $filePath);

      return 1;
    } else {
      return 2;
    }
  }

  ///5-App Samples Report
  public function appSamplesReport($report)
  {
    $samples = DB::table('gpff_delivered_samples as gdeliversample')
      ->join('gpff_samples as gsample', 'gsample.samples_id', 'gdeliversample.samples_id')
      ->join('gpff_customer as gcustomer', 'gdeliversample.customer_id', 'gcustomer.customer_id')
      ->join('gpff_branch as gbranch', 'gsample.branch_id', 'gbranch.branch_id')
      ->join('gpff_region as gregion', 'gsample.region_id', 'gregion.region_id')
      ->orderBy('gdeliversample.created_at', 'DESC');

    if ($report->branch_id != '') {
      $samples = $samples->whereIn('gsample.branch_id', $report->branch_id);
    }
    if ($report->region_id != '') {
      $samples = $samples->whereIn('gsample.region_id', $report->region_id);
    }
    if ($report->customer_id != '') {
      $samples = $samples->whereIn('gdeliversample.customer_id', $report->customer_id);
    }
    if ($report->field_officer_id != '') {
      $samples = $samples->whereIn('gdeliversample.field_officer_id', $report->field_officer_id);
    }
    if ($report->year != '') {
      $samples = $samples->whereYear("gdeliversample.created_at", $report->year);
    }
    if ($report->months != '') {
      $samples = $samples->whereIn(DB::raw('MONTH(gdeliversample.created_at)'), $report->months);
    }

    $samples_det = $samples->get(['gdeliversample.samples_id', 'gdeliversample.created_at', 'gdeliversample.samples_name', 'gdeliversample.samples_qty', 'gdeliversample.field_officer_name', 'gdeliversample.field_officer_name', 'gcustomer.customer_name', 'gcustomer.customer_type', 'gbranch.branch_name', 'gregion.region_name', 'gsample.product_type']);

    if (count($samples_det) > 0) {
      $customPaper = array(0, 0, 767.00, 883.80);

      view()->share('datas', $samples_det);
      view()->share('Month', $report->year);
      view()->share('Title', 'Samples Report');
      $pdf = PDF::loadView('SamplesReport')
        ->setPaper('a4', 'landscape');
      $pdf->save(public_path('Report/SamplesReport.pdf'));

      $file_name = 'SamplesReport.pdf';
      $name = $file_name;
      $filePath = 'Report/SamplesReport/' . $name;

      Storage::disk('s3')->put($filePath, file_get_contents(public_path('Report') . "/" . $file_name));

      $this->sendReportToEmail($report->email_id, $file_name, $filePath);

      return 1;
    } else {
      return 2;
    }
  }

  ///6-App Gift Report
  public function appGiftReport($report)
  {
    $gift = DB::table('gpff_delivered_gift as gdeliveredgift')
      ->join('gpff_gift as ggift', 'ggift.gift_id', 'gdeliveredgift.gift_id')
      ->join('gpff_customer as gcustomer', 'gdeliveredgift.customer_id', 'gcustomer.customer_id')
      ->join('gpff_branch as gbranch', 'ggift.branch_id', 'gbranch.branch_id')
      ->join('gpff_region as gregion', 'ggift.region_id', 'gregion.region_id')
      ->orderBy('gdeliveredgift.created_at', 'DESC');

    if ($report->branch_id != '') {
      $gift = $gift->whereIn('ggift.branch_id', $report->branch_id);
    }
    if ($report->region_id != '') {
      $gift = $gift->whereIn('ggift.region_id', $report->region_id);
    }
    if ($report->customer_id != '') {
      $gift = $gift->whereIn('gdeliveredgift.customer_id', $report->customer_id);
    }
    if ($report->field_officer_id != '') {
      $gift = $gift->whereIn('gdeliveredgift.field_officer_id', $report->field_officer_id);
    }
    if ($report->year != '') {
      $gift = $gift->whereYear("gdeliveredgift.created_at", $report->year);
    }
    if ($report->months != '') {
      $gift = $gift->whereIn(DB::raw('MONTH(gdeliveredgift.created_at)'), $report->months);
    }

    $gift_det = $gift->get(['gdeliveredgift.gift_id', 'gdeliveredgift.created_at', 'gdeliveredgift.gift_name', 'gdeliveredgift.gift_qty', 'gdeliveredgift.field_officer_name', 'gcustomer.customer_name', 'gcustomer.customer_type', 'gbranch.branch_name', 'gregion.region_name']);

    if (count($gift_det) > 0) {
      $customPaper = array(0, 0, 767.00, 883.80);

      view()->share('datas', $gift_det);
      view()->share('Month', $report->year);
      view()->share('Title', 'Gifts Report');
      $pdf = PDF::loadView('GiftsReport')
        ->setPaper('a4', 'landscape');
      $pdf->save(public_path('Report/GiftsReport.pdf'));

      $file_name = 'GiftsReport.pdf';
      $name = $file_name;
      $filePath = 'Report/GiftsReport/' . $name;

      Storage::disk('s3')->put($filePath, file_get_contents(public_path('Report') . "/" . $file_name));

      $this->sendReportToEmail($report->email_id, $file_name, $filePath);

      return 1;
    } else {
      return 2;
    }
  }

  ///7-App Target Report
  public function appTargetReport($report)
  {
    $target = DB::table('gpff_insentive as instype')
      ->leftjoin('gpff_branch as gpbr', 'instype.branch_id', '=', 'gpbr.branch_id')
      ->leftjoin('gpff_region as greg', 'instype.region_id', '=', 'greg.region_id')
      ->orderBy('instype.created_at', 'DESC');

    if ($report->branch_id != '') {
      $target = $target->whereIn('instype.branch_id', $report->branch_id);
    }
    if ($report->region_id != '') {
      $target = $target->whereIn('instype.region_id', $report->region_id);
    }
    /*      if($report->area_id != ''){
          $target = $target->whereIn('instype.area_id',$report->area_id);
        }*/

    if ($report->field_officer_id != '') {
      $target = $target->whereIn('instype.fo_id', $report->field_officer_id);
    }
    if ($report->year != '') {
      $target = $target->whereYear("instype.from_date", $report->year);
    }
    if ($report->months != '') {
      $target = $target->whereIn(DB::raw('MONTH(instype.from_date)'), $report->months);
    }

    $target_det = $target->get(['instype.insentive_id', 'instype.created_by', 'instype.insentive_type', 'instype.fo_id', 'instype.fo_name', 'instype.product_id', 'instype.product_name', 'instype.unit', 'instype.insentive_amt', 'instype.target_amt', 'instype.from_date', 'instype.to_date', 'instype.insentive_status', 'instype.branch_id', 'instype.region_id', 'instype.created_at', 'instype.updated_at', 'gpbr.branch_name', 'greg.region_name']);

    if (count($target_det) > 0) {
      $customPaper = array(0, 0, 767.00, 883.80);

      view()->share('datas', $target_det);
      view()->share('Month', $report->year);
      view()->share('Title', 'Target Report');
      $pdf = PDF::loadView('TargetReport')
        ->setPaper('a4', 'landscape');
      $pdf->save(public_path('Report/TargetReport.pdf'));

      $file_name = 'TargetReport.pdf';
      $name = $file_name;
      $filePath = 'Report/TargetReport/' . $name;

      Storage::disk('s3')->put($filePath, file_get_contents(public_path('Report') . "/" . $file_name));

      $this->sendReportToEmail($report->email_id, $file_name, $filePath);

      return 1;
    } else {
      return 2;
    }
  }

  //7-App Side Target Chart Data
  public function appTargetDetailChart($report)
  {

    $target = DB::table('gpff_insentive as gpinc')
      ->leftjoin('gpff_branch as gpbr', 'gpinc.branch_id', '=', 'gpbr.branch_id')
      ->leftjoin('gpff_region as greg', 'gpinc.region_id', '=', 'greg.region_id')
      ->where('gpinc.fo_id', $report->field_officer_id)
      ->orderBy('gpinc.from_date', 'DESC');
    if ($report->year != '') {
      $target = $target->whereYear("gpinc.from_date", $report->year);
    }
    if ($report->months != '') {
      $target = $target->whereIn(DB::raw('MONTH(gpinc.from_date)'), $report->months);
    }

    $target_det = $target->get(['gpinc.insentive_id', 'gpinc.fo_id', 'gpinc.fo_name', 'gpinc.created_by', 'gpinc.insentive_type', 'gpinc.product_id', 'gpinc.product_name', 'gpinc.unit', 'gpinc.insentive_amt', 'gpinc.target_amt', 'gpinc.from_date', 'gpinc.to_date', 'gpinc.branch_id', 'gpbr.branch_name', 'greg.region_name', 'gpinc.region_id', 'gpinc.insentive_status']);

    $pricedata = [];
    foreach ($report->months as $value) {
      $price['Month'] = $value;
      $pricede =  DB::table('gpff_insentive')
        ->where(DB::raw('MONTH(from_date)'), $value)
        ->where('insentive_type', 1)
        ->where('fo_id', $report->field_officer_id)
        ->where(function ($query) use ($report) {
          if ($report->year != '') {
            $query->whereYear("from_date", $report->year);
          }
        });

      $price['Tot_price'] = $pricede->sum('target_amt');
      /*            $price['Achived_price'] = $pricede->sum('target_achived_price');
            $price['Pending_price'] = $pricede->sum('target_pending_price');
*/
      array_push($pricedata, $price);
    }

    $productdata = [];
    foreach ($report->months as $value) {
      $product['Month'] = $value;
      $productde =  DB::table('gpff_insentive')
        ->where(DB::raw('MONTH(from_date)'), $value)
        ->where('insentive_type', 2)
        ->where('fo_id', $report->field_officer_id)
        ->where(function ($query) use ($report) {
          if ($report->year != '') {
            $query->whereYear("from_date", $report->year);
          }
        });

      $product['Tot_product'] = $productde->sum('unit');
      /*            $product['Achived_product'] = $productde->sum('target_product_achived_qty');
            $product['Pending_product'] = $productde->sum('target_product_pending_qty');
*/
      array_push($productdata, $product);
    }

    $Percentage_data = [];
    foreach ($report->months as $value) {
      $price['Month'] = $value;
      $pricede =  DB::table('gpff_insentive')
        ->where(DB::raw('MONTH(from_date)'), $value)
        ->where('insentive_type', 3)
        ->where('fo_id', $report->field_officer_id)
        ->where(function ($query) use ($report) {
          if ($report->year != '') {
            $query->whereYear("from_date", $report->year);
          }
        });

      $price['Tot_price'] = $pricede->sum('target_amt');
      /*            $price['Achived_price'] = $pricede->sum('target_achived_price');
            $price['Pending_price'] = $pricede->sum('target_pending_price');
*/
      array_push($Percentage_data, $price);
    }

    $result = array_merge(
      ['Target_details' => $target_det->toArray()],
      ['Amount_data' => $pricedata],
      ['Product_data' => $productdata],
      ['Percentage_data' => $Percentage_data]
    );

    return $result;
  }

  //9-Field Officer Task Missed Call Report
  public function appMissedTaskCallReport($report)
  {

    $today = date('Y-m-d');
    $today1 = date('Y-m-d');
    $today = Carbon::now()->format('Y-m-d') . '%';

    $orders = DB::table('gpff_task as gta')
      ->join('gpff_customer as gcus', 'gta.customer_id', '=', 'gcus.customer_id')
      ->join('gpff_users as guse', 'gta.field_officer_id', '=', 'guse.user_id')
      ->join('gpff_branch as gpbr', 'gta.branch_id', '=', 'gpbr.branch_id')
      ->join('gpff_region as greg', 'gta.region_id', '=', 'greg.region_id')
      ->join('gpff_area as gpar', 'gta.area_id', '=', 'gpar.area_id')
      ->whereBetween(DB::RAW('date(gta.created_at)'), [date($report->start_date), date($report->end_date)])
      //->where('gta.task_date_time', 'like', $today)
      ->orderBy('gta.created_at', 'DESC');

    if ($report->branch_id != '') {
      $orders = $orders->whereIn('gta.branch_id', $report->branch_id);
    }
    if ($report->region_id != '') {
      $orders = $orders->whereIn('gta.region_id', $report->region_id);
    }
    if ($report->area_id != '') {
      $orders = $orders->whereIn('gta.area_id', $report->area_id);
    }
    if ($report->field_officer_id != '') {
      $orders = $orders->whereIn('gta.field_officer_id', $report->field_officer_id);
    }

    $order_det = $orders->get(['gta.task_id', 'gta.created_by', 'gta.field_officer_id', 'guse.firstname', 'gta.customer_id', 'gta.area_manager_id', 'gta.task_title', 'gta.task_desc', 'gta.task_status', 'gta.task_date_time', 'gta.task_reassign_status', 'gta.task_start_date_time', 'gta.task_end_date_time', 'gta.task_active_minu', 'gta.notes', 'gta.created_at', 'gta.updated_at', 'gcus.customer_name', 'gcus.customer_email', 'gcus.country_code', 'gcus.customer_mobile', 'gcus.customer_city', 'gcus.customer_country', 'gcus.customer_fax', 'gcus.customer_address', 'gcus.customer_location', 'gcus.customer_lan', 'gcus.customer_lat', 'gcus.customer_cr_id', 'gcus.website', 'gcus.avatar', 'gcus.is_active', 'gcus.customer_type', 'gta.branch_id', 'gta.region_id', 'gta.area_id', 'gpbr.branch_name', 'greg.region_name', 'gpar.area_name']);

    if (count($order_det) > 0) {
      $customPaper = array(0, 0, 767.00, 883.80);

      view()->share('datas', $order_det);
      view()->share('Start_date', $today1);
      view()->share('End_date', $today1);
      view()->share('Title', 'Task Missed Report');
      $pdf = PDF::loadView('MissedTaskCallReport')
        ->setPaper('a4', 'landscape');
      $pdf->save(public_path('Report/MissedTaskCallReport.pdf'));

      $file_name = 'MissedTaskCallReport.pdf';
      $name = $file_name;
      $filePath = 'Report/MissedTaskCallReport/' . $name;

      Storage::disk('s3')->put($filePath, file_get_contents(public_path('Report') . "/" . $file_name));

      $this->sendReportToEmail($report->email_id, $file_name, $filePath);

      return 1;
    } else {
      return 2;
    }
  }
  //Reports Send To The Mail
  public function sendReportToEmail($report, $file_name, $filePath)
  {
    $pathToFile = 'https://tgetsfa-live.s3.ap-south-1.amazonaws.com/' . $filePath;
    // amazingwits-bucket.s3.ap-south-1.amazonaws.com

    $emailObject = array(
      'user_id' => 0,
      'email_to' => $report,
      'email_cc' => '',
      'email_bcc' => '',
      'email_subject' => 'You Have New Notification from GPFF',
      'email_content' => '',
      'email_template' => 'sample',
      'email_template_data' => json_encode(array('data' => 'data')),
      'email_file_path' => $pathToFile,
      'email_file_name' => $file_name,
      'created_date' => date('Y-m-d H:i:s'),
      'updated_date' => date('Y-m-d H:i:s')
    );

    $cmn = new Common();
    $cmn->insertEmailQueue($emailObject);

    return "Basic Email Sent. Check your inbox.";
  }
  public function productSalesReport($report)
  {

    // $product = $report->product_id;

    $orders = DB::table('gpff_order_list as gorderlist')
      // ->join('gpff_order_list as gorderlist','gorderlist.order_id','gorder.order_id')
      ->join('gpff_region as gregion', 'gorder.region_id', 'gregion.region_id')
      ->join('gpff_area as garea', 'gorder.area_id', 'garea.area_id')
      ->whereBetween(DB::RAW('date(gorder.created_at)'), [date($report->start_date), date($report->end_date)])
      ->where('order_status', 3)
      // ->where('product_id',$report->product_id)
      ->orderBy('gorder.created_at', 'DESC')
      ->get();

    if ($report->product_id != '') {
      $orders = $orders->whereIn('gorder.vendor_id', $report->product_id);
    }
    // print_r($orders);
    // exit();
  }
  public function customerReportForProductIdBased($Orders)
  {
    $results = DB::table('gpff_order as gorder')
      ->join('gpff_order_list_batchwise as gorderlist', 'gorder.order_id', 'gorderlist.order_id')
      ->join('gpff_product as gproduct', 'gorderlist.product_id', 'gproduct.product_id')
      ->join('gpff_region as gpregi', 'gpregi.region_id', 'gorder.region_id')
      ->join('gpff_warehouse as gpwah', 'gpwah.warehouse_id', 'gorder.warehouse_id')
      ->join('gpff_users as gpusr', 'gpusr.user_id', 'gorder.field_officer_id')
      ->whereBetween(DB::RAW('date(gorder.invoice_date)'), [date($Orders->start_date), date($Orders->end_date)])
      // ->where('gorder.or_type', 1)
      ->whereNotIn('gorder.order_status', [0,5])
      ->select('gorder.payment_type','gorder.invoice_date', 'gorder.customer_name', 'gproduct.product_genericname', 'gpwah.warehouse_name', 'gorder.order_id', DB::raw('CONCAT(gpusr.firstname, \' \',gpusr.lastname) AS field_officer_name'))
      ->selectRaw(('SUM(gorderlist.product_qty) as product_quantity'))
      ->selectRaw(('SUM(gorderlist.product_tot_price) as product_tot_price'))
      ->orderBy('gorder.invoice_date', 'DESC')
      ->groupBy('gorder.customer_name', 'gproduct.product_genericname', 'gpwah.warehouse_name', 'gorder.order_id', 'field_officer_name');

    if ($Orders->region_id != '') {
      $orders = $results->whereIn('gorder.region_id', $Orders->region_id);
    }
    if ($Orders->area_id != '') {
      $orders = $results->whereIn('gorder.area_id', $Orders->area_id);
    }
    if ($Orders->warehouse_id != '') {
      $orders = $results->whereIn('gorder.warehouse_id', $Orders->warehouse_id);
    }
    if ($Orders->product_id != '') {
      $orders = $results->whereIn('gorderlist.product_id', $Orders->product_id);
    }
    // print_r($Orders->field_officer_id);
    if ($Orders->field_officer_id != '') {
      $orders = $results->whereIn('gorder.field_officer_id', $Orders->field_officer_id);
    }

    if ($Orders->stockist_id != '') {
      $orders = $results->whereIn('gorder.order_created_by', $Orders->stockist_id);
    }

    // if ($Orders->cType != '') {
    //   $orders = $results->where('gorder.payment_type', $Orders->cType);
    // }

    $order_det = $results->get();

    $result = array_merge(
      ['order_details' => $order_det->toArray()]
    );

    return $result;
  }
  public function customerOrderReportForProductIdBased($Orders)
  {
    $results = DB::table('gpff_order as gorder')
      ->join('gpff_order_list as gorderlist', 'gorder.order_id', 'gorderlist.order_id')
      ->join('gpff_product as gproduct', 'gorderlist.product_id', 'gproduct.product_id')
      ->join('gpff_region as gpregi', 'gpregi.region_id', 'gorder.region_id')
      ->join('gpff_warehouse as gpwah', 'gpwah.warehouse_id', 'gorder.warehouse_id')
      ->join('gpff_users as gpusr', 'gpusr.user_id', 'gorder.field_officer_id')
      ->join('gpff_area as gp_area','gorder.area_id',  'gp_area.area_id')
      ->whereBetween(DB::RAW('date(gorder.order_date)'), [date($Orders->start_date), date($Orders->end_date)])
      // ->where('gorder.or_type', 1)
      ->whereIn('gorder.order_status', [0,5])
      ->select('gp_area.area_name','gorder.order_id','gorder.payment_type','gorder.order_date', 'gorder.customer_name', 'gproduct.product_genericname', 'gpwah.warehouse_name', 'gorder.order_id', DB::raw('CONCAT(gpusr.firstname, \' \',gpusr.lastname) AS field_officer_name'))
      ->selectRaw(('SUM(gorderlist.product_qty) as product_quantity'))
      ->selectRaw(('SUM(gorderlist.product_tot_price) as product_tot_price'))
      ->orderBy('gorder.order_date', 'DESC')
      ->groupBy('gorder.customer_name', 'gproduct.product_genericname', 'gpwah.warehouse_name', 'gorder.order_id', 'field_officer_name');

    if ($Orders->region_id != '') {
      $orders = $results->whereIn('gorder.region_id', $Orders->region_id);
    }
    if ($Orders->area_id != '') {
      $orders = $results->whereIn('gorder.area_id', $Orders->area_id);
    }
    if ($Orders->warehouse_id != '') {
      $orders = $results->whereIn('gorder.warehouse_id', $Orders->warehouse_id);
    }
    if ($Orders->product_id != '') {
      $orders = $results->whereIn('gorderlist.product_id', $Orders->product_id);
    }
    // print_r($Orders->field_officer_id);
    if ($Orders->field_officer_id != '') {
      $orders = $results->whereIn('gorder.field_officer_id', $Orders->field_officer_id);
    }

    if ($Orders->stockist_id != '') {
      $orders = $results->whereIn('gorder.order_created_by', $Orders->stockist_id);
    }

    // if ($Orders->cType != '') {
    //   $orders = $results->where('gorder.payment_type', $Orders->cType);
    // }

    $order_det = $results->get();

    $result = array_merge(
      ['order_details' => $order_det->toArray()]
    );

    return $result;
  }
  public function discountReportSummary($report)
  {

    if ($report->type == 1) {
      $details = DB::table('gpff_order')

        ->whereBetween(DB::RAW('date(created_at)'), [date($report->start_date), date($report->end_date)])
        ->select('customer_name')
        ->selectRaw(('SUM(order_discount) as order_discount'))
        ->groupBy('customer_name');

      if ($report->customer_id != '') {
        $details = $details->whereIn('customer_id', $report->customer_id);
      }

      $result = $details->get();

      return $result;
    } else {
      $details = DB::table('gpff_vendor_order')
        ->whereBetween(DB::RAW('date(created_at)'), [date($report->start_date), date($report->end_date)]);

      if ($report->customer_id != '') {
        $details = $details->whereIn('vendor_id', $report->customer_id);
      }

      $result = $details->get();

      return $result;
    }
  }

  public function discountReport($report)
  {

    if ($report->type == 1) {
      $details = DB::table('gpff_order as gpor')
        ->join('gpff_users as gpusr', 'gpusr.user_id', 'gpor.order_created_by')
        ->join('gpff_warehouse as gwarehouse', 'gwarehouse.warehouse_id', 'gpor.warehouse_id')
        ->where('gpor.order_status', 3)
        ->whereNotIn('gpor.order_discount', [0])
        ->whereBetween(DB::RAW('date(gpor.invoice_date)'), [date($report->start_date), date($report->end_date)]);

      if ($report->customer_id != '') {
        $details = $details->whereIn('gpor.customer_id', $report->customer_id);
      }
      if ($report->branch_id != '') {
        $details = $details->whereIn('gpor.branch_id', $report->branch_id);
      }
      if ($report->region_id != '') {
        $details = $details->whereIn('gpor.region_id', $report->region_id);
      }
      if ($report->area_id != '') {
        $details = $details->whereIn('gpor.area_id', $report->area_id);
      }
      if ($report->billed_by != '') {
        $details = $details->whereIn('gpor.order_created_by', $report->billed_by);
      }
      switch($report->report_type){
        case 1:
          $results = $details->get(['gpor.customer_name as customer_name', 'gpor.order_id', 'gpor.invoice_date', 'gpor.order_discount', 'gpor.or_gross_total','gpusr.firstname', 'gwarehouse.warehouse_name']);
        break;
        case 2:
          $total_price = $details->sum('gpor.order_discount');
          $discount_det = $details->join('gpff_customer as gcustomer','gcustomer.customer_id','gpor.customer_id')->groupBy('gpor.customer_id')->get(['gpor.customer_id','gcustomer.customer_name',DB::raw('sum(gpor.order_discount) as total_price')]);
          $cus_data = [];
          foreach($discount_det as $customer)
          {
            $cus_data[] =  (array) $customer;
          }
          $customer_wise = usort($cus_data, function($a, $b) {
                 return ($a['total_price'] > $b['total_price']? -1 : 1);
               });
          $result = array(
            "customer_wise"=>$cus_data,
            "total_price"=> $total_price
          );
          $results = $result;
        break;
        case 3:
          $total_price = $details->sum('gpor.order_discount');
          $discount_det = $details->groupBy('gpor.order_created_by')->get(['gpor.order_created_by','gpusr.firstname',DB::raw('sum(gpor.order_discount) as total_price')]);
          $cus_data = [];
          foreach($discount_det as $fo)
          {
            $fo_data[] =  (array) $fo;
          }
          $fo_wise = usort($fo_data, function($a, $b) {
                 return ($a['total_price'] > $b['total_price']? -1 : 1);
               });
          $result = array(
            "fo_wise"=>$fo_data,
            "total_price"=> $total_price
          );
          $results = $result;
        break;
        case 4:
          $discount_det = $details->groupBy('gpor.order_created_by')->get(['gpor.order_created_by','gpusr.firstname']);
          $results = $discount_det;
        break;
      }
      return $results;
    } else {
      $details = DB::table('gpff_vendor_order as gpvor')
        ->join('gpff_users as gpusr', 'gpusr.user_id', 'gpvor.area_manager_id')
        ->join('gpff_warehouse as gwarehouse', 'gwarehouse.warehouse_id', 'gpvor.warehouse_id')
        ->where('gpvor.vendor_order_status', 3)
        ->whereNotIn('gpvor.order_discount', [0])
        ->whereBetween(DB::RAW('date(gpvor.created_at)'), [date($report->start_date), date($report->end_date)]);

      if ($report->customer_id != '') {
        $details = $details->whereIn('vendor_id', $report->customer_id);
      }
      if ($report->branch_id != '') {
        $details = $details->whereIn('gpvor.branch_id', $report->branch_id);
      }
      if ($report->region_id != '') {
        $details = $details->whereIn('gpvor.region_id', $report->region_id);
      }
      if ($report->area_id != '') {
        $details = $details->whereIn('gpvor.area_id', $report->area_id);
      }
      if ($report->billed_by != '') {
        $details = $details->whereIn('gpvor.area_manager_id', $report->billed_by);
      }
      switch($report->report_type){
        case 1:
          $results = $details->get(['gpvor.vendor_name as customer_name', 'gpvor.vendor_order_id as order_id', 'gpvor.created_at as invoice_date', 'gpvor.order_discount', 'gpvor.or_gross_total','gpusr.firstname', 'gwarehouse.warehouse_name']);
        break;
        case 2:
          $total_price = $details->sum('gpvor.order_discount');
          $discount_det = $details->join('gpff_customer as gcustomer','gcustomer.customer_id','gpvor.vendor_id')->groupBy('gpvor.vendor_id')->get(['gpvor.vendor_id','gcustomer.customer_name',DB::raw('sum(gpvor.order_discount) as total_price')]);
          $cus_data = [];
          foreach($discount_det as $customer)
          {
            $cus_data[] =  (array) $customer;
          }
          $customer_wise = usort($cus_data, function($a, $b) {
                 return ($a['total_price'] > $b['total_price']? -1 : 1);
               });
          $result = array(
            "customer_wise"=>$cus_data,
            "total_price"=> $total_price
          );
          $results = $result;
        break;
        case 3:
          $total_price = $details->sum('gpvor.order_discount');
          $discount_det = $details->groupBy('gpvor.area_manager_id')->get(['gpvor.area_manager_id','gpusr.firstname',DB::raw('sum(gpvor.order_discount) as total_price')]);
          $cus_data = [];
          foreach($discount_det as $fo)
          {
            $fo_data[] =  (array) $fo;
          }
          $fo_wise = usort($fo_data, function($a, $b) {
                 return ($a['total_price'] > $b['total_price']? -1 : 1);
               });
          $result = array(
            "fo_wise"=>$fo_data,
            "total_price"=> $total_price
          );
          $results = $result;
        break;
        case 4:
          $discount_det = $details->groupBy('gpvor.area_manager_id')->get(['gpvor.area_manager_id as order_created_by','gpusr.firstname']);
          $results = $discount_det;
        break;
      }
      return $results;
    }
  }

  public function getMainWarehouse()
  {

    $data = DB::table('gpff_warehouse')
      ->where('warehouse_type', 1)
      ->get();

    return $data;
  }
  public function subWarehouseInStocksReport($report)
  {
    if ($report->type == 1) {
      $details = DB::table('gpff_product_recived_history_list as hl')
        ->join('gpff_product_recived_history as h', 'h.product_recived_history_id', 'hl.product_recived_history_id')
        ->join('gpff_warehouse as gpwar', 'h.warehouse_id', 'gpwar.warehouse_id')
        ->whereBetween(DB::RAW('date(hl.created_at)'), [date($report->start_date), date($report->end_date)])
        ->where('h.warehouse_id', $report->warehouse_id)
        ->distinct();
      if ($report->product_id != '') {
        $details = $details->whereIn('hl.product_id', $report->product_id);
      }

      $result = $details->get(['hl.category_name', 'hl.product_genericname', 'gpwar.warehouse_name', 'hl.product_qty', 'hl.product_netprice','hl.product_grossprice', 'hl.batch_id','hl.created_at','h.supplier_name','hl.exp_date']);

      return $result;
    } elseif ($report->type == 2) {
      $details = DB::table('gpff_product_request_accept_list as gpral')
        ->join('gpff_product_request as gpr','gpr.product_request_id', 'gpral.product_request_id')
        ->join('gpff_warehouse as gpwar', 'gpwar.warehouse_id', 'gpr.warehouse_id')
        ->where('gpr.sub_ware_id', $report->warehouse_id)
        ->whereBetween(DB::RAW('date(gpral.created_at)'), [date($report->start_date), date($report->end_date)])
        // ->orderBy('gpr.created_at', 'DESC');
        ->distinct();
      // ->get();

      if ($report->sub_ware_id != '') {
        $details = $details->whereIn('gpr.warehouse_id', $report->sub_ware_id);
      }

      if ($report->product_id != '') {
        $details = $details->whereIn('gpral.product_id', $report->product_id);
      }
      $result = $details->get(['gpral.category_name', 'gpral.product_genericname', 'gpwar.warehouse_name', 'gpral.product_qty', 'gpral.product_netprice','gpral.product_grossprice', 'gpral.batch_no as batch_id','gpral.created_at','gpral.exp_date']);
      return $result;
    }
  }
  public function subWarehouseItemMovingReport($report)
  {
    if ($report->type == 1) {
      // print_r($report->start_date);
      $details = DB::table('gpff_order as gpor')
        ->join('gpff_order_list as gporl', 'gporl.order_id', 'gpor.order_id')
        ->join('gpff_order_list_batchwise as gporlb', 'gporl.order_list_id', 'gporlb.order_list_id')
        ->join('gpff_warehouse as gpwar', 'gpor.warehouse_id', 'gpwar.warehouse_id')
        ->where('gpor.invoice_type',1)
        // ->where('gpor.order_status', 3)
        ->whereBetween(DB::RAW('date(gpor.created_at)'), [date($report->start_date), date($report->end_date)])
        ->whereIn('gpor.warehouse_id', $report->warehouse_id)
        // ->orderBy('gpor.created_at', 'DESC');
        ->distinct();
      // ->orderBy('gpor.created_at', 'DESC')
      if ($report->product_id != '') {
        $details = $details->whereIn('gporl.product_id', $report->product_id);
      }

      $result = $details->get(['gporl.category_name', 'gporl.product_genericname', 'gpwar.warehouse_name', 'gporlb.product_qty', 'gporlb.product_tot_price', 'gporlb.batch_no','gporlb.FOC','gpor.updated_at','gpor.customer_id','gpor.order_id', 'gpor.customer_name','gpor.invoice_no']);

      return $result;
    } elseif ($report->type == 2) {

      $details = DB::table('gpff_vendor_order as gpvor')
        ->join('gpff_vendor_order_list as gpvorl', 'gpvorl.vendor_order_id', 'gpvor.vendor_order_id')
        ->join('gpff_vendor_order_list_batchwise as gpvorlb', 'gpvorl.vendor_order_list_id', 'gpvorlb.vendor_order_list_id')
        ->join('gpff_warehouse as gpwar', 'gpvor.warehouse_id', 'gpwar.warehouse_id')
        // ->where('gpvor.vendor_order_status', 3)
        ->whereBetween(DB::RAW('date(gpvor.created_at)'), [date($report->start_date), date($report->end_date)])
        ->whereIn('gpvor.warehouse_id', $report->warehouse_id)
        // ->orderBy('gpvor.created_at', 'DESC');
        ->distinct();

      if ($report->product_id != '') {
        $details = $details->whereIn('gpvorl.product_id', $report->product_id);
      }

      $result = $details->get(['gpvorl.category_name', 'gpvorl.product_genericname', 'gpwar.warehouse_name', 'gpvorlb.product_qty', 'gpvorlb.product_tot_price', 'gpvorlb.batch_no','gpvorlb.FOC', 'gpvor.updated_at', 'gpvor.vendor_order_id as order_id', 'gpvor.vendor_name as customer_name','gpvor.invoice_no']);

      return $result;
    } else {

      $details = DB::table('gpff_product_request as gpr')
        ->join('gpff_product_request_accept_list as gpral', 'gpr.product_request_id', 'gpral.product_request_id')
        ->join('gpff_warehouse as gpwar', 'gpwar.warehouse_id', 'gpr.warehouse_id')
        ->whereIn('gpr.warehouse_id', $report->warehouse_id)
        ->whereBetween(DB::RAW('date(gpral.created_at)'), [date($report->start_date), date($report->end_date)])
        // ->orderBy('gpr.created_at', 'DESC');
        ->distinct();
      // ->get();

      if ($report->sub_ware_id != '') {
        $details = $details->whereIn('gpr.sub_ware_id', $report->sub_ware_id);
      }

      if ($report->product_id != '') {
        $details = $details->whereIn('gpral.product_id', $report->product_id);
      }

      $result = $details->get(['gpr.sub_ware_name', 'gpral.product_genericname', 'gpral.category_name', 'gpral.product_qty', 'gpral.batch_no', 'gpral.created_at']);

      return $result;
    }
  }

  public function subWarehouseItemMovingReportSumm($report)
  {
    if ($report->type == 1) {
      $orderId = [];
      $oreder_details = DB::table('gpff_order as gpor')
                        ->whereIn('gpor.warehouse_id', $report->warehouse_id)
                        ->where('gpor.invoice_type',1)
                        ->whereBetween(DB::RAW('date(gpor.created_at)'), [date($report->start_date), date($report->end_date)])
                        ->get();
      foreach ($oreder_details as $order) {
          $orderId[] = $order->order_id;
      }
      $details = DB::table('gpff_order_list as gporl')
        ->join('gpff_order_list_batchwise as gporlb', 'gporl.order_list_id', 'gporlb.order_list_id')
        ->join('gpff_order as gpor', 'gpor.order_id', 'gporlb.order_id')
        ->join('gpff_warehouse as gpwar', 'gpor.warehouse_id', 'gpwar.warehouse_id')
        ->whereIn('gporl.order_id', $orderId)                
        ->select('gporl.category_name','gporl.product_id','gporl.product_genericname','gpwar.warehouse_name')        
        ->selectRaw(('SUM(gporlb.product_qty) as product_quantity'))
        ->selectRaw(('SUM(gporlb.FOC) as foc_quantity'))
        ->selectRaw(('SUM(gporlb.spl_FOC) as sp_foc_quantity'))
        ->selectRaw(('SUM(gporlb.product_tot_price) as product_tot_price'))
        ->groupBy('gporl.category_name', 'gporl.product_id','gporl.product_genericname','gpwar.warehouse_name');

      // $details = DB::table('gpff_order as gpor')
      //   ->join('gpff_order_list as gporl', 'gporl.order_id', 'gpor.order_id')
      //   ->join('gpff_order_list_batchwise as gporlb', 'gpor.order_id', 'gporlb.order_id')
      //   ->join('gpff_warehouse as gpwar', 'gpor.warehouse_id', 'gpwar.warehouse_id')
      //   ->whereIn('gpor.warehouse_id', $report->warehouse_id)
      //   // ->where('gpor.order_status', 3)
      //   ->whereBetween(DB::RAW('date(gpor.created_at)'), [date($report->start_date), date($report->end_date)])
      //   ->select('gporl.category_name', 'gporl.product_genericname', 'gpwar.warehouse_name')
      //   ->selectRaw(('SUM(gporlb.product_qty) as product_quantity'))
      //   ->selectRaw(('SUM(gporlb.product_tot_price) as product_tot_price'))
      //   ->groupBy('gporl.category_name', 'gporl.product_genericname', 'gpwar.warehouse_name');

      if ($report->product_id != '') {
        $details = $details->whereIn('gporl.product_id', $report->product_id);
      }

      $result = $details->get();

      return $result;
    } elseif ($report->type == 2) {
      $orderId = [];
      $oreder_details = DB::table('gpff_vendor_order as gpvor')
                        ->whereIn('gpvor.warehouse_id', $report->warehouse_id)
                        ->whereBetween(DB::RAW('date(gpvor.created_at)'), [date($report->start_date), date($report->end_date)])
                        ->get();
      foreach ($oreder_details as $order) {
          $orderId[] = $order->vendor_order_id;
      }
      $details = DB::table('gpff_vendor_order_list as gpvorl')
        ->join('gpff_vendor_order_list_batchwise as gpvorlb', 'gpvorl.vendor_order_list_id', 'gpvorlb.vendor_order_list_id')
        ->join('gpff_vendor_order as gpvor', 'gpvor.vendor_order_id', 'gpvorlb.vendor_order_id')
        ->join('gpff_warehouse as gpwar', 'gpvor.warehouse_id', 'gpwar.warehouse_id')
        ->whereIn('gpvorl.vendor_order_id', $orderId)                
        ->select('gpvorl.category_name','gpvorl.product_id','gpvorl.product_genericname','gpwar.warehouse_name')        
        ->selectRaw(('SUM(gpvorlb.product_qty) as product_quantity'))
        ->selectRaw(('SUM(gpvorlb.FOC) as foc_quantity'))
        ->selectRaw(('SUM(gpvorlb.spl_FOC) as sp_foc_quantity'))
        ->selectRaw(('SUM(gpvorlb.product_tot_price) as product_tot_price'))
        ->groupBy('gpvorl.category_name', 'gpvorl.product_id','gpvorl.product_genericname','gpwar.warehouse_name');
      // $details = DB::table('gpff_vendor_order as gpvor')
      //   ->join('gpff_vendor_order_list as gpvorl', 'gpvorl.vendor_order_id', 'gpvor.vendor_order_id')
      //   ->join('gpff_vendor_order_list_batchwise as gpvorlb', 'gpvor.vendor_order_id', 'gpvorlb.vendor_order_id')
      //   ->join('gpff_warehouse as gpwar', 'gpvor.warehouse_id', 'gpwar.warehouse_id')
      //   ->whereIn('gpvor.warehouse_id', $report->warehouse_id)
      //   ->where('gpvor.vendor_order_status', 3)
      //   ->whereBetween(DB::RAW('date(gpvor.created_at)'), [date($report->start_date), date($report->end_date)])
      //   ->select('gpvorl.category_name', 'gpwar.warehouse_name','gpvorl.product_id','gpvorl.product_genericname')        
      //   ->selectRaw(('SUM(gpvorlb.product_qty) as product_quantity'))
      //   ->selectRaw(('SUM(gpvorlb.product_tot_price) as product_tot_price'))
      //   ->groupBy('gpvorl.category_name', 'gpvorl.product_id','gpvorl.product_genericname','gpwar.warehouse_name');

      if ($report->product_id != '') {
        $details = $details->whereIn('gpvorl.product_id', $report->product_id);
      }

      $result = $details->get();

      return $result;
    } else {

      $details = DB::table('gpff_product_request as gpr')
        ->join('gpff_product_request_accept_list as gpral', 'gpr.product_request_id', 'gpral.product_request_id')
        ->join('gpff_warehouse as gpwar', 'gpwar.warehouse_id', 'gpr.warehouse_id')
        // ->where('gprl.approve_status', 1)
        ->whereIn('gpr.warehouse_id', $report->warehouse_id)
        ->whereBetween(DB::RAW('date(gpral.created_at)'), [date($report->start_date), date($report->end_date)])
        ->select('gpr.sub_ware_name', 'gpral.category_name', 'gpral.product_genericname')
        ->selectRaw(('SUM(gpral.product_qty) as product_qty'))
        ->groupBy('gpr.sub_ware_name', 'gpral.category_name', 'gpral.product_genericname');

      if ($report->sub_ware_id != '') {
        $details = $details->whereIn('gpr.sub_ware_id', $report->sub_ware_id);
      }

      if ($report->product_id != '') {
        $details = $details->whereIn('gpral.product_id', $report->product_id);
      }

      $result = $details->get();
      return $result;
    }
  }

  public function stockReport($report)
  {

    $details = DB::table('gpff_product_stock as gpstock')
      ->join('gpff_product_batch_stock as gpbstok', 'gpstock.product_stock_id', 'gpbstok.product_stock_id')
      ->join('gpff_warehouse as gpwar', 'gpwar.warehouse_id', 'gpstock.warehouse_id')
      ->whereBetween(DB::RAW('date(gpstock.created_at)'), [date($report->start_date), date($report->end_date)])
      ->orderBy('gpbstok.updated_at', 'DESC');

    if ($report->warehouse_id != '') {
      $details = $details->whereIn('gpstock.warehouse_id', $report->warehouse_id);
    }

    $results = $details->get(['gpstock.product_id', 'gpstock.product_genericname', 'gpwar.warehouse_name', 'gpbstok.batch_id', 'gpbstok.product_blc_qty', 'gpbstok.product_netprice', 'gpbstok.product_grossprice', 'gpbstok.product_sales_qty']);

    return $results;
  }

  public function dailySaleReport($report)
  {

    $details = DB::table('gpff_order as gorder')
      ->join('gpff_order_list as gorderlist', 'gorderlist.order_id', 'gorder.order_id')
      ->join('gpff_branch as gbranch', 'gorder.branch_id', 'gbranch.branch_id')
      ->join('gpff_region as gregion', 'gorder.region_id', 'gregion.region_id')
      ->join('gpff_area as garea', 'gorder.area_id', 'garea.area_id')
      ->orderBy('gorder.created_at', 'DESC');

    if ($report->region_id != '') {
      $details = $details->whereIn('gorder.region_id', $report->region_id);
    }

    $results = $details->get(['gorder.order_id', 'gorder.order_date', 'gorder.customer_name', 'gorderlist.product_genericname', 'gorderlist.product_qty', 'gorderlist.accepted_qty', 'gorder.field_officer_name', 'gorderlist.product_type', 'gbranch.branch_name', 'gregion.region_name', 'garea.area_name', 'gorderlist.product_tot_price']);

    return $results;
  }

  // public function mainWarehouseItemMovingRequest($report){

  //   $details = DB::table('gpff_product_request as gpr')
  //   ->join('gpff_product_request_list as gprl', 'gpr.product_request_id', 'gprl.product_request_id')
  //   ->join('gpff_product_request_accept_list as gpral','gpr.product_request_id','gpral.product_request_id')
  //   ->join('gpff_warehouse as gpwar', 'gpwar.warehouse_id', 'gpr.warehouse_id')
  //   ->where('gpr.warehouse_id', $report->warehouse_id)
  //   ->where('gprl.approve_status',1)
  //   ->whereBetween(DB::RAW('date(gprl.updated_at)'), [date($report->start_date), date($report->end_date)]);
  //           // ->get();

  //   if($report->sub_ware_id != ''){
  //     $details = $details->whereIn('gpr.sub_ware_id',$report->sub_ware_id);
  //   }

  //   if($report->product_id != ''){
  //     $details = $details->whereIn('gprl.product_id',$report->product_id);
  //   }

  //   $result = $details->get(['gpr.sub_ware_name','gpral.product_genericname','gpral.category_name', 'gpral.product_qty']);

  //   return $result;
  // }

  //   public function mainWarehouseItemMovingRequestSumm($report){

  //     $details = DB::table('gpff_product_request as gpr')
  //     ->join('gpff_product_request_list as gprl', 'gpr.product_request_id', 'gprl.product_request_id')
  //     ->join('gpff_product_request_accept_list as gpral','gpr.product_request_id','gpral.product_request_id')
  //     ->join('gpff_warehouse as gpwar', 'gpwar.warehouse_id', 'gpr.warehouse_id')

  //     ->where('gprl.approve_status',1)
  //     ->where('gpr.warehouse_id', $report->warehouse_id)
  //     ->whereBetween(DB::RAW('date(gprl.updated_at)'), [date($report->start_date), date($report->end_date)])
  //     ->select('gpr.sub_ware_name','gprl.category_name', 'gprl.product_genericname')
  //     ->selectRaw(('SUM(gprl.issued_quantity) as product_qty'))
  //     ->groupBy('gpr.sub_ware_name','gprl.category_name', 'gprl.product_genericname');

  //     if($report->sub_ware_id != ''){
  //       $details = $details->whereIn('gpr.sub_ware_id',$report->sub_ware_id);
  //     }

  //     if($report->product_id != ''){
  //       $details = $details->whereIn('gprl.product_id',$report->product_id);
  //     }

  //     $result = $details->get();
  //     return $result;
  //   }

  public function schemeReport($report){
    //Type 1 = Customer , 2 = Vendor
    if($report->type == 1){

      $scheme_report = DB::table('gpff_order_list_batchwise as olb')
                      ->join('gpff_order as or','or.order_id','olb.order_id')
                      ->join('gpff_product as gp','gp.product_id','olb.product_id')
                      ->join('gpff_order_scheme as os','os.scheme_value','olb.scheme')
                      ->join('gpff_users as gu','gu.user_id','or.order_created_by')
                      ->whereNotIn('or.order_status', [0,5]);
      if($report->bill_no != ''){
        $scheme_report = $scheme_report->where('olb.order_id',$report->bill_no);
      }
      if($report->start_date != ''){
        $scheme_report = $scheme_report->whereBetween(DB::RAW('date(or.invoice_date)'), [date($report->start_date), date($report->end_date)]);
      }
      if($report->branch != ''){
        $scheme_report = $scheme_report->whereIn('or.branch_id',$report->branch);
      }
      if($report->region != ''){
        $scheme_report = $scheme_report->whereIn('or.region_id',$report->region);
      }
      if($report->zone != ''){
        $scheme_report = $scheme_report->whereIn('or.area_id',$report->zone);
      }
      if($report->customer != ''){
        $scheme_report = $scheme_report->whereIn('or.customer_id',$report->customer);
      }
      if($report->scheme != ''){
        $scheme_report = $scheme_report->whereIn('olb.scheme',$report->scheme);
      }
      if($report->product_id != ''){
        $scheme_report = $scheme_report->whereIn('olb.product_id',$report->product_id);
      }
      if($report->billed_by != ''){
        $scheme_report = $scheme_report->whereIn('or.order_created_by',$report->billed_by);
      }
      switch($report->report_type){
        case 1:
          $results = $scheme_report->get(['or.invoice_date as created_at','or.order_id','or.customer_name','gp.product_genericname','olb.batch_no','olb.product_qty','olb.product_netprice','olb.product_grossprice','olb.pro_amt_cal','olb.product_discount','olb.product_tot_price','os.scheme_name','gu.firstname','gu.lastname']);
        break;
        case 2:
          $total_price = $scheme_report->sum('olb.product_discount');
          $discount_det = $scheme_report->groupBy('olb.product_id')->get(['olb.product_id','gp.product_genericname',DB::raw('sum(olb.product_discount) as total_price')]);
          $cus_data = [];
          foreach($discount_det as $customer)
          {
            $cus_data[] =  (array) $customer;
          }
          $customer_wise = usort($cus_data, function($a, $b) {
                 return ($a['total_price'] > $b['total_price']? -1 : 1);
               });
          $result = array(
            "product_wise"=>$cus_data,
            "total_price"=> $total_price
          );
          $results = $result;
        break;
        case 3:
          $total_price = $scheme_report->sum('olb.product_discount');
          $discount_det = $scheme_report->join('gpff_customer as gcustomer','gcustomer.customer_id','or.customer_id')->groupBy('or.customer_id')->get(['or.customer_id','gcustomer.customer_name',DB::raw('sum(olb.product_discount) as total_price')]);
          $cus_data = [];
          foreach($discount_det as $customer)
          {
            $cus_data[] =  (array) $customer;
          }
          $customer_wise = usort($cus_data, function($a, $b) {
                 return ($a['total_price'] > $b['total_price']? -1 : 1);
               });
          $result = array(
            "customer_wise"=>$cus_data,
            "total_price"=> $total_price
          );
          $results = $result;
        break;
        case 4:
          $total_price = $scheme_report->sum('olb.product_discount');
          $discount_det = $scheme_report->groupBy('or.order_created_by')->get(['or.order_created_by','gu.firstname',DB::raw('sum(olb.product_discount) as total_price')]);
          $cus_data = [];
          foreach($discount_det as $fo)
          {
            $fo_data[] =  (array) $fo;
          }
          $fo_wise = usort($fo_data, function($a, $b) {
                 return ($a['total_price'] > $b['total_price']? -1 : 1);
               });
          $result = array(
            "fo_wise"=>$fo_data,
            "total_price"=> $total_price
          );
          $results = $result;
        break;
        case 5:
          $discount_det = $scheme_report->groupBy('or.order_created_by')->get(['or.order_created_by','gu.firstname']);
          $results = $discount_det;
        break;
      }      
      return $results;

    }else if($report->type == 2){
      $scheme_report = DB::table('gpff_vendor_order_list_batchwise as olb')
                      ->join('gpff_vendor_order as or','or.vendor_order_id','olb.vendor_order_id')
                      ->join('gpff_product as gp','gp.product_id','olb.product_id')
                      ->leftjoin('gpff_order_scheme as os','os.scheme_value','olb.scheme')
                      ->join('gpff_users as gu','gu.user_id','or.area_manager_id')
                      ->whereNotIn('or.vendor_order_status', [0,5]);
      if($report->bill_no != ''){
        $scheme_report = $scheme_report->where('olb.vendor_order_id',$report->bill_no);
      }
      if($report->start_date != ''){
        $scheme_report = $scheme_report->whereBetween(DB::RAW('date(or.created_at)'), [date($report->start_date), date($report->end_date)]);
      }
      if($report->branch != ''){
        $scheme_report = $scheme_report->whereIn('or.branch_id',$report->branch);
      }
      if($report->region != ''){
        $scheme_report = $scheme_report->whereIn('or.region_id',$report->region);
      }
      if($report->zone != ''){
        $scheme_report = $scheme_report->whereIn('or.area_id',$report->zone);
      }
      if($report->vendor != ''){
        $scheme_report = $scheme_report->whereIn('or.vendor_id',$report->vendor);
      }
      if($report->scheme != ''){
        $scheme_report = $scheme_report->whereIn('olb.scheme',$report->scheme);
      }
      if($report->product_id != ''){
        $scheme_report = $scheme_report->whereIn('olb.product_id',$report->product_id);
      }
      if($report->billed_by != ''){
        $scheme_report = $scheme_report->whereIn('or.area_manager_id',$report->billed_by);
      }
      switch($report->report_type){
        case 1:
          $results = $scheme_report->get(['or.created_at','or.vendor_order_id as order_id','or.vendor_name as customer_name','gp.product_genericname','olb.batch_no','olb.product_qty','olb.product_netprice','olb.product_grossprice','olb.pro_amt_cal','olb.product_discount','olb.product_tot_price','os.scheme_name','gu.firstname','gu.lastname']);
        break;
        case 2:
          $total_price = $scheme_report->sum('olb.product_discount');
          $discount_det = $scheme_report->groupBy('olb.product_id')->get(['olb.product_id','gp.product_genericname',DB::raw('sum(olb.product_discount) as total_price')]);
          $cus_data = [];
          foreach($discount_det as $customer)
          {
            $cus_data[] =  (array) $customer;
          }
          $customer_wise = usort($cus_data, function($a, $b) {
                 return ($a['total_price'] > $b['total_price']? -1 : 1);
               });
          $result = array(
            "product_wise"=>$cus_data,
            "total_price"=> $total_price
          );
          $results = $result;
        break;
        case 3:
          $total_price = $scheme_report->sum('olb.product_discount');
          $discount_det = $scheme_report->join('gpff_customer as gcustomer','gcustomer.customer_id','or.vendor_id')->groupBy('or.vendor_id')->get(['or.vendor_id','gcustomer.customer_name',DB::raw('sum(olb.product_discount) as total_price')]);
          $cus_data = [];
          foreach($discount_det as $customer)
          {
            $cus_data[] =  (array) $customer;
          }
          $customer_wise = usort($cus_data, function($a, $b) {
                 return ($a['total_price'] > $b['total_price']? -1 : 1);
               });
          $result = array(
            "customer_wise"=>$cus_data,
            "total_price"=> $total_price
          );
          $results = $result;
        break;
        case 4:
          $total_price = $scheme_report->sum('olb.product_discount');
          $discount_det = $scheme_report->groupBy('or.area_manager_id')->get(['or.area_manager_id','gu.firstname',DB::raw('sum(olb.product_discount) as total_price')]);
          $cus_data = [];
          foreach($discount_det as $fo)
          {
            $fo_data[] =  (array) $fo;
          }
          $fo_wise = usort($fo_data, function($a, $b) {
                 return ($a['total_price'] > $b['total_price']? -1 : 1);
               });
          $result = array(
            "fo_wise"=>$fo_data,
            "total_price"=> $total_price
          );
          $results = $result;
        break;
        case 5:
          $discount_det = $scheme_report->groupBy('or.area_manager_id')->get(['or.area_manager_id','gu.firstname']);
          $results = $discount_det;
        break;
      }  
      return $results;
    }
  }

  //Get Filter Based Sales Report
  public function getCustomerSalesAnalysisReport($report)
  {
    $orders = DB::table('gpff_order as gorder')
      ->join('gpff_order_list_batchwise as gorderlist', 'gorderlist.order_id', 'gorder.order_id')
      ->join('gpff_product as gp', 'gorderlist.product_id', 'gp.product_id')
      ->join('gpff_branch as gbranch', 'gorder.branch_id', 'gbranch.branch_id')
      ->join('gpff_region as gregion', 'gorder.region_id', 'gregion.region_id')
      ->join('gpff_area as garea', 'gorder.area_id', 'garea.area_id')
      ->join('gpff_warehouse as gwar', 'gorder.warehouse_id', 'gwar.warehouse_id')
      ->orderBy('gorder.invoice_date', 'DESC');
    if ($report->branch_id != '') {
      $orders = $orders->whereIn('gorder.branch_id', $report->branch_id);
    }
    if ($report->region_id != '') {
      $orders = $orders->whereIn('gorder.region_id', $report->region_id);
    }
    if ($report->area_id != '') {
      $orders = $orders->whereIn('gorder.area_id', $report->area_id);
    }
    if ($report->customer_id != '') {
      $orders = $orders->whereIn('gorder.customer_id', $report->customer_id);
    }
    if ($report->field_officer_id != '') {
      $orders = $orders->whereIn('gorder.field_officer_id', $report->field_officer_id);
    }
    if ($report->product_id != '') {
      $orders = $orders->whereIn('gorderlist.product_id', $report->product_id);    
    }
    $orders = $orders->whereBetween(DB::RAW('date(gorder.invoice_date)'), [date($report->start_date), date($report->end_date)]);

    $customer_order_det = $orders->get(['gorder.order_id', 'gp.product_genericname', 'gorderlist.product_qty', 'gorderlist.product_tot_price','gorderlist.product_type']);
    $orderId = [];
    foreach($customer_order_det as $value){
      $orderId[] = $value->order_id;
    }
    $or_id = array_unique($orderId);
    if ($report->product_id != '') {
      $customer_wise = DB::table('gpff_order as gorder')
                ->join('gpff_customer as gcustomer','gcustomer.customer_id','gorder.customer_id')
                ->join('gpff_order_list_batchwise as gorderlist', 'gorderlist.order_id', 'gorder.order_id')
                ->whereIn('gorder.order_id',$or_id)
                ->whereIn('gorderlist.product_id',$report->product_id)
                ->groupBy('customer_id')
                ->select('gorder.customer_id','gcustomer.customer_name',DB::Raw('SUM(product_tot_price) as total_price'),DB::Raw('SUM(gorderlist.product_qty) as product_qty'))
                ->get();
      $fo_wise = DB::table('gpff_order as gorder')
                    ->join('gpff_users as gusers','gusers.user_id','gorder.field_officer_id')
                    ->join('gpff_order_list_batchwise as gorderlist', 'gorderlist.order_id', 'gorder.order_id')
                    ->whereIn('gorder.order_id',$or_id)
                    ->whereIn('gorderlist.product_id',$report->product_id)
                    ->groupBy('field_officer_id')
                    ->get(['field_officer_id as sales_person_id','gusers.firstname as sales_person_name',DB::Raw('SUM(product_tot_price) as total_price'),DB::Raw('SUM(gorderlist.product_qty) as product_qty')]);
      $total_sales = DB::table('gpff_order as gorder')
                    ->join('gpff_order_list_batchwise as gorderlist', 'gorderlist.order_id', 'gorder.order_id')
                    ->whereIn('gorderlist.product_id',$report->product_id)
                    ->whereIn('gorder.order_id',$or_id)
                    ->groupBy(DB::raw("DATE_FORMAT(invoice_date, '%d-%m-%Y')"))
                    ->orderBy('invoice_date','ASC')
                    ->get([DB::raw("(DATE_FORMAT(invoice_date, '%d-%m-%Y')) as invoice_date"),DB::Raw('SUM(product_tot_price) as total_price')]);
      $customer_total_sale_amt = DB::table('gpff_order as gorder')
                      ->join('gpff_order_list_batchwise as gorderlist', 'gorderlist.order_id', 'gorder.order_id')
                      ->whereIn('gorderlist.product_id',$report->product_id)
                      ->whereIn('gorder.order_id',$or_id)
                      ->get([DB::raw('SUM(product_tot_price) as or_tot_price'),DB::raw('SUM(gorder.or_gross_total) as or_gross_price'),DB::raw('SUM(gorder.order_discount) as order_discount'),DB::raw('SUM(gorder.spl_discount) as spl_discount')]);
      $total_product_sales = DB::table('gpff_order as gorder')
                    ->join('gpff_order_list_batchwise as gorderlist', 'gorderlist.order_id', 'gorder.order_id')
                    ->whereIn('gorderlist.product_id',$report->product_id)
                    ->whereIn('gorder.order_id',$or_id)
                    ->groupBy('gorderlist.product_type')
                    ->get(['gorderlist.product_type',DB::Raw('SUM(product_tot_price) as total_price'),DB::Raw('SUM(gorderlist.product_qty) as product_qty')]);
    }
    else{  
      $customer_wise = DB::table('gpff_order as gorder')
                ->join('gpff_customer as gcustomer','gcustomer.customer_id','gorder.customer_id')
                ->join('gpff_order_list_batchwise as gorderlist', 'gorderlist.order_id', 'gorder.order_id')
                ->whereIn('gorder.order_id',$or_id)
                ->groupBy('customer_id')
                ->select('gorder.customer_id','gcustomer.customer_name',DB::Raw('SUM(or_tot_price) as total_price'),DB::Raw('SUM(gorderlist.product_qty) as product_qty'))
                ->orderBy('total_price','DESC')
                ->get();
      $fo_wise = DB::table('gpff_order as gorder')
                    ->join('gpff_users as gusers','gusers.user_id','gorder.field_officer_id')
                    ->join('gpff_order_list_batchwise as gorderlist', 'gorderlist.order_id', 'gorder.order_id')
                    ->whereIn('gorder.order_id',$or_id)
                    ->groupBy('field_officer_id')
                    ->get(['field_officer_id as sales_person_id','gusers.firstname as sales_person_name',DB::Raw('SUM(or_tot_price) as total_price'),DB::Raw('SUM(gorderlist.product_qty) as product_qty')]);
      $total_sales = DB::table('gpff_order as gorder')
                    ->whereIn('gorder.order_id',$or_id)
                    ->groupBy(DB::raw("DATE_FORMAT(invoice_date, '%d-%m-%Y')"))
                    ->orderBy('invoice_date','ASC')
                    ->get([DB::raw("(DATE_FORMAT(invoice_date, '%d-%m-%Y')) as invoice_date"),DB::Raw('SUM(or_tot_price) as total_price')]);
      $customer_total_sale_amt = DB::table('gpff_order as gorder')
                      ->join('gpff_order_list_batchwise as gorderlist', 'gorderlist.order_id', 'gorder.order_id')
                      ->whereIn('gorder.order_id',$or_id)
                      ->get([DB::raw('SUM(product_tot_price) as or_tot_price'),DB::raw('SUM(gorder.or_gross_total) as or_gross_price'),DB::raw('SUM(gorder.order_discount) as order_discount'),DB::raw('SUM(gorder.spl_discount) as spl_discount')]);
      $total_product_sales = DB::table('gpff_order as gorder')
                    ->join('gpff_order_list_batchwise as gorderlist', 'gorderlist.order_id', 'gorder.order_id')
                    ->whereIn('gorder.order_id',$or_id)
                    ->groupBy('gorderlist.product_type')
                    ->get(['gorderlist.product_type',DB::Raw('SUM(product_tot_price) as total_price'),DB::Raw('SUM(gorderlist.product_qty) as product_qty')]);
    }
    
    $cus_data = [];
    foreach($customer_wise as $customer)
    {
      $cus_data[] =  (array) $customer;
    }
    $customer_wise = usort($cus_data, function($a, $b) {
                   return ($a['total_price'] > $b['total_price']? -1 : 1);
                 });
    $fo_data = [];
    foreach($fo_wise as $fo)
    {
      $fo_data[] =  (array) $fo;
    }
    $fo_wise = usort($fo_data, function($a, $b) {
                   return ($a['total_price'] > $b['total_price']? -1 : 1);
                 });
    $sales_data = [];
    foreach($total_sales as $value){
      $sales_data[] = array(
                           "date"=> $value->invoice_date,
                           "price"=>$value->total_price
                        );  
    }
    $data =  usort($sales_data, function($a, $b) {
                   return (strtotime($a['date']) < strtotime($b['date'])? -1 : 1);
                 });
    $product_data = DB::table('gpff_order as gorder')
                    ->join('gpff_order_list_batchwise as gorderlist', 'gorderlist.order_id', 'gorder.order_id')
                    ->join('gpff_product as gp', 'gorderlist.product_id', 'gp.product_id')
                    ->whereIn('gorder.order_id',$or_id)
                    ->groupBy('gorderlist.product_id','gp.product_genericname','gorderlist.product_type');
    if ($report->product_id != '') {
      $product_data = $product_data->whereIn('gorderlist.product_id',$report->product_id);
    }
    $product_data = $product_data->get(['gorderlist.product_id',DB::Raw('SUM(product_qty) as product_qty'),DB::Raw('SUM(product_tot_price) as product_tot_price'),'gp.product_genericname','gorderlist.product_type']);
    $promotional_prod = [];
    $generic_prod = [];
    foreach ($product_data as $value) {
      $product = [];
      $product['product_id'] = $value->product_id;
      $product['product_genericname'] = $value->product_genericname;
      $product['product_qty'] = $value->product_qty;
      $product['product_price'] = $value->product_tot_price;
      if($value->product_type == 1){
        array_push($promotional_prod, $product);
      }else{
        array_push($generic_prod, $product);
      }
    }
    $prom_data = usort($promotional_prod, function($a, $b) {
                   return ($a['product_price'] > $b['product_price']? -1 : 1);
                 });
    $generic_data = usort($generic_prod, function($a, $b) {
                   return ($a['product_price'] > $b['product_price']? -1 : 1);
                 });
    $result = array_merge(
      ['total_sales_price' => $customer_total_sale_amt],
      ['total_product_sales' => $total_product_sales],
      ['total_sales' => $sales_data],
      ['customer_wise' => $cus_data],
      ['fo_wise' => $fo_data],
      ['promotional' => $promotional_prod],
      ['generic' => $generic_prod]      
    );

    return $result;
  }

  //Get Filter Based Sales Report
  public function getVendorSalesAnalysisReport($report)
  {
    $vendor_orders = DB::table('gpff_vendor_order as gvorder')
      ->join('gpff_vendor_order_list_batchwise as gvorderlist', 'gvorderlist.vendor_order_id', 'gvorder.vendor_order_id')
      ->join('gpff_product as gp', 'gvorderlist.product_id', 'gp.product_id')
      ->join('gpff_branch as gbranch', 'gvorder.branch_id', 'gbranch.branch_id')
      ->join('gpff_region as gregion', 'gvorder.region_id', 'gregion.region_id')
      ->join('gpff_area as garea', 'gvorder.area_id', 'garea.area_id')
      ->join('gpff_warehouse as gwar', 'gvorder.warehouse_id', 'gwar.warehouse_id')
      ->join('gpff_users as guser', 'gvorder.area_manager_id', 'guser.user_id')
      ->orderBy('gvorder.created_at', 'DESC');

    if ($report->branch_id != '') {
      $vendor_orders = $vendor_orders->whereIn('gvorder.branch_id', $report->branch_id);
    }
    if ($report->region_id != '') {
      $vendor_orders = $vendor_orders->whereIn('gvorder.region_id', $report->region_id);
    }
    if ($report->area_id != '') {
      $vendor_orders = $vendor_orders->whereIn('gvorder.area_id', $report->area_id);
    }
    if ($report->customer_id != '') {
      $orders = $vendor_orders->whereIn('gvorder.vendor_id', $report->customer_id);
    }
    if ($report->area_manager_id != '') {
      $orders = $vendor_orders->whereIn('gvorder.area_manager_id', $report->area_manager_id);
    }
    if ($report->product_id != '') {
      $vendor_orders = $vendor_orders->whereIn('gvorderlist.product_id', $report->product_id);
    }
    $vendor_orders = $vendor_orders->whereBetween(DB::RAW('date(gvorder.created_at)'), [date($report->start_date), date($report->end_date)]);

    $vendor_order_det = $vendor_orders->get(['gvorder.vendor_order_id', 'gvorder.order_date', 'gp.product_genericname', 'gvorderlist.product_qty', 'gvorderlist.product_type']);
    $orderId = [];
    foreach($vendor_order_det as $value){
      $orderId[] = $value->vendor_order_id;
    }
    $or_id = array_unique($orderId);
    if ($report->product_id != '') {
      $customer_wise = DB::table('gpff_vendor_order as gorder')
                    ->join('gpff_customer as gcustomer','gcustomer.customer_id','gorder.vendor_id')
                    ->join('gpff_vendor_order_list_batchwise as gvorderlist', 'gvorderlist.vendor_order_id', 'gorder.vendor_order_id')
                    ->whereIn('gvorderlist.product_id', $report->product_id)
                    ->whereIn('gorder.vendor_order_id',$or_id)
                    ->groupBy('vendor_id')
                    ->select('vendor_id as customer_id','gcustomer.customer_name as customer_name',DB::Raw('SUM(product_tot_price) as total_price'),DB::Raw('SUM(gvorderlist.product_qty) as product_qty'))
                    ->get();
      $fo_wise = DB::table('gpff_vendor_order as gorder')
                    ->join('gpff_vendor_order_list_batchwise as gvorderlist', 'gvorderlist.vendor_order_id', 'gorder.vendor_order_id')
                    ->join('gpff_users as guser', 'gorder.area_manager_id', 'guser.user_id')
                    ->whereIn('gvorderlist.product_id', $report->product_id)
                    ->whereIn('gorder.vendor_order_id',$or_id)
                    ->groupBy('gorder.area_manager_id')
                    ->get(['gorder.area_manager_id as sales_person_id','guser.firstname as sales_person_name',DB::Raw('SUM(product_tot_price) as total_price'),DB::Raw('SUM(gvorderlist.product_qty) as product_qty')]);
      $total_sales = DB::table('gpff_vendor_order as gorder')
                    ->join('gpff_vendor_order_list_batchwise as gvorderlist', 'gvorderlist.vendor_order_id', 'gorder.vendor_order_id')
                    ->whereIn('gvorderlist.product_id', $report->product_id)
                    ->whereIn('gorder.vendor_order_id',$or_id)
                    ->groupBy(DB::raw("DATE_FORMAT(gorder.created_at, '%d-%m-%Y')"))
                    ->orderBy('created_at','ASC')
                    ->get([DB::raw("(DATE_FORMAT(gorder.created_at, '%d-%m-%Y')) as created_at"),DB::Raw('SUM(product_tot_price) as total_price')]);
      $customer_total_sale_amt = DB::table('gpff_vendor_order as gorder')
                      ->join('gpff_vendor_order_list_batchwise as gvorderlist', 'gvorderlist.vendor_order_id', 'gorder.vendor_order_id')
                      ->whereIn('gvorderlist.product_id', $report->product_id)
                      ->whereIn('gorder.vendor_order_id',$or_id)
                      ->get([DB::raw('SUM(product_tot_price) as or_tot_price'),DB::raw('SUM(gorder.or_gross_total) as or_gross_price'),DB::raw('SUM(gorder.order_discount) as order_discount'),DB::raw('SUM(gorder.spl_discount) as spl_discount')]);
      $total_product_sales = DB::table('gpff_vendor_order as gorder')
                      ->join('gpff_vendor_order_list_batchwise as gvorderlist', 'gvorderlist.vendor_order_id', 'gorder.vendor_order_id')
                      ->whereIn('gvorderlist.product_id', $report->product_id)
                      ->whereIn('gorder.vendor_order_id',$or_id)
                    ->groupBy('gvorderlist.product_type')
                    ->get(['gvorderlist.product_type',DB::Raw('SUM(product_tot_price) as total_price'),DB::Raw('SUM(gvorderlist.product_qty) as product_qty')]);

    }else{
      $customer_wise = DB::table('gpff_vendor_order as gorder')
                    ->join('gpff_customer as gcustomer','gcustomer.customer_id','gorder.vendor_id')
                    ->join('gpff_vendor_order_list_batchwise as gvorderlist', 'gvorderlist.vendor_order_id', 'gorder.vendor_order_id')
                    ->whereIn('gorder.vendor_order_id',$or_id)
                    ->groupBy('gorder.vendor_id')
                    ->select('vendor_id as customer_id','gcustomer.customer_name as customer_name',DB::Raw('SUM(or_tot_price) as total_price'),DB::Raw('SUM(gvorderlist.product_qty) as product_qty'))
                    ->get();
      $fo_wise = DB::table('gpff_vendor_order as gorder')
                    ->join('gpff_users as guser', 'gorder.area_manager_id', 'guser.user_id')
                    ->join('gpff_vendor_order_list_batchwise as gvorderlist', 'gvorderlist.vendor_order_id', 'gorder.vendor_order_id')
                    ->whereIn('gorder.vendor_order_id',$or_id)
                    ->groupBy('gorder.area_manager_id')
                    ->get(['gorder.area_manager_id as sales_person_id','guser.firstname as sales_person_name',DB::Raw('SUM(or_tot_price) as total_price'),DB::Raw('SUM(gvorderlist.product_qty) as product_qty')]);
      $total_sales = DB::table('gpff_vendor_order as gorder')
                    ->whereIn('gorder.vendor_order_id',$or_id)
                    ->groupBy(DB::raw("DATE_FORMAT(created_at, '%d-%m-%Y')"))
                    ->orderBy('created_at','ASC')
                    ->get([DB::raw("(DATE_FORMAT(created_at, '%d-%m-%Y')) as created_at"),DB::Raw('SUM(or_tot_price) as total_price')]);
      $customer_total_sale_amt = DB::table('gpff_vendor_order as gorder')
                      ->join('gpff_vendor_order_list_batchwise as gvorderlist', 'gvorderlist.vendor_order_id', 'gorder.vendor_order_id')
                      ->whereIn('gorder.vendor_order_id',$or_id)
                      ->get([DB::raw('SUM(product_tot_price) as or_tot_price'),DB::raw('SUM(gorder.or_gross_total) as or_gross_price'),DB::raw('SUM(gorder.order_discount) as order_discount'),DB::raw('SUM(gorder.spl_discount) as spl_discount')]);
      $total_product_sales = DB::table('gpff_vendor_order as gorder')
                      ->join('gpff_vendor_order_list_batchwise as gvorderlist', 'gvorderlist.vendor_order_id', 'gorder.vendor_order_id')
                      ->whereIn('gorder.vendor_order_id',$or_id)
                    ->groupBy('gvorderlist.product_type')
                    ->get(['gvorderlist.product_type',DB::Raw('SUM(product_tot_price) as total_price'),DB::Raw('SUM(gvorderlist.product_qty) as product_qty')]);

    }
    $cus_data = [];
    foreach($customer_wise as $customer)
    {
      $cus_data[] =  (array) $customer;
    }
    $customer_wise = usort($cus_data, function($a, $b) {
                   return ($a['total_price'] > $b['total_price']? -1 : 1);
                 });
    
    $fo_data = [];
    foreach($fo_wise as $fo)
    {
      $fo_data[] =  (array) $fo;
    }
    $fo_wise = usort($fo_data, function($a, $b) {
                   return ($a['total_price'] > $b['total_price']? -1 : 1);
                 });
    $sales_data = [];
    foreach($total_sales as $value){
      $sales_data[] = array(
                           "date"=> $value->created_at,
                           "price"=>$value->total_price
                        );  
    }
    $data =  usort($sales_data, function($a, $b) {
                   return (strtotime($a['date']) < strtotime($b['date'])? -1 : 1);
                 });
    $product_data = DB::table('gpff_vendor_order as gorder')
                    ->join('gpff_vendor_order_list_batchwise as gorderlist', 'gorderlist.vendor_order_id', 'gorder.vendor_order_id')
                    ->join('gpff_product as gp', 'gorderlist.product_id', 'gp.product_id')
                    ->whereIn('gorder.vendor_order_id',$or_id)
                    ->groupBy('gorderlist.product_id','gp.product_genericname','gorderlist.product_type');
    if ($report->product_id != '') {
      $product_data = $product_data->whereIn('gorderlist.product_id',$report->product_id);
    }
    $product_data = $product_data->get(['gorderlist.product_id',DB::Raw('SUM(product_qty) as product_qty'),DB::Raw('SUM(product_tot_price) as product_tot_price'),'gp.product_genericname','gorderlist.product_type']);
    $promotional_prod = [];
    $generic_prod = [];
    foreach ($product_data as $value) {
      $product = [];
      $product['product_id'] = $value->product_id;
      $product['product_genericname'] = $value->product_genericname;
      $product['product_qty'] = $value->product_qty;
      $product['product_price'] = $value->product_tot_price;
      if($value->product_type == 1){
        array_push($promotional_prod, $product);
      }else{
        array_push($generic_prod, $product);
      }
    }
    $prom_data = usort($promotional_prod, function($a, $b) {
                   return ($a['product_price'] > $b['product_price']? -1 : 1);
                 });
    $generic_data = usort($generic_prod, function($a, $b) {
                   return ($a['product_price'] > $b['product_price']? -1 : 1);
                 });
    $result = array_merge(
      ['total_sales_price' => $customer_total_sale_amt],
      ['total_product_sales' => $total_product_sales],
      ['total_sales' => $sales_data],
      ['customer_wise' => $cus_data],
      ['fo_wise' => $fo_data],
      ['promotional' => $promotional_prod],
      ['generic' => $generic_prod]      
    );

    return $result;
  }
}
