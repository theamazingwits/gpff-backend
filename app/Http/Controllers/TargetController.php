<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use File;

require(base_path().'/app/Http/Middleware/Target.php');

class TargetController extends Controller
{
//Target API Calls
    //Add Task

     public function assignTarget(Request $request){
        
        //Target Details
        $Targets = new Target();
        $Targets->fo_id         = $request->input('fo_id');
        $Targets->fo_name       = $request->input('fo_name');
        $Targets->created_by    = $request->input('created_by');
        $Targets->insentive_type= $request->input('insentive_type');
        $Targets->product_id    = $request->input('product_id');
        $Targets->unit          = $request->input('unit');
        $Targets->insentive_amt = $request->input('insentive_amt');
        $Targets->target_amt    = $request->input('target_amt');
        $Targets->from_date     = $request->input('from_date');
        $Targets->to_date       = $request->input('to_date');
        $Targets->branch_id     = $request->input('branch_id');
        $Targets->region_id     = $request->input('region_id');
        $Targets->area_id       = $request->input('area_id');
        $Targets->product_details = $request->input('product_details');
       
        $result = $Targets->assignTarget($Targets);
        if($result == 1){
            $response = response()->json(['status' => 'success' , 'message' => 'Target assigned successfully']);
        } else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Target assigned failed']);
        }
        return $response;
    }

    // // Remove User Target
    public function removeTarget(Request $request){
        $Targets = new Target();
        $Targets->insentive_id = $request->input('insentive_id');

        $result = $Targets->removeTarget($Targets);
        if($result > 0){
            $response = response()->json(['status' => 'success' ,'message' => 'Target details deleted successfully']);
        } else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Error in deleting']); 
        }
        return $response;
    }
    //Fetch Target Details Based On the Users
    public function getMyTargetDetails(Request $request){
        
        $Targets = new Target();
        $Targets->user_id    	= $request->input('user_id');
        $Targets->target_status = $request->input('target_status');

        $result = $Targets->getMyTargetDetails($Targets);
        if($result){
            $response = response()->json(['status' => 'success' , 'message' => '' ,'data' => $result]);
        }else{
            $response = response()->json(['status' => 'failure' , 'message' =>  '' ,'data' => $result]);
        }
        return $response;
    }
    //Fetch Target Details Based On the Assigned User
    public function getAssignToTargetDetails(Request $request){
        
        $Targets = new Target();
        $Targets->user_id       = $request->input('user_id');
        $Targets->target_status = $request->input('target_status');

        $result = $Targets->getAssignToTargetDetails($Targets);
        if($result){
            $response = response()->json(['status' => 'success' , 'message' => '' ,'data' => $result]);
        }else{
            $response = response()->json(['status' => 'failure' , 'message' =>  '' ,'data' => $result]);
        }
        return $response;
    }


    //Fetch Incentive Target Details 
    public function getIncentiveTargetDetails(Request $request){
        $Targets = new Target();
        $Targets->user_id       = $request->input('user_id');

        $result = $Targets->getIncentiveTargetDetails($Targets);
        if($result){
            $response = response()->json(['status' => 'success' , 'message' => '' ,'data' => $result]);
        }else{
            $response = response()->json(['status' => 'failure' , 'message' =>  '' ,'data' => $result]);
        }
        return $response;
    }

    //Fetch Previous Incentive Target Details 
    public function getPreviousTargetDetails(Request $request){
        $Targets = new Target();
        $Targets->user_id       = $request->input('user_id');
        $Targets->type       = $request->input('type');

        $result = $Targets->getPreviousTargetDetails($Targets);
        if($result){
            $response = response()->json(['status' => 'success' , 'message' => '' ,'data' => $result]);
        }else{
            $response = response()->json(['status' => 'failure' , 'message' =>  '' ,'data' => $result]);
        }
        return $response;
    }

    public function deleteIncentiveTarget(Request $request){

        $Targets = new Target();
        $Targets->insentive_id = $request->input('insentive_id');

        $result = $Targets->deleteIncentiveTarget($Targets);
        if($result > 0){
            $response = response()->json(['status' => 'success' ,'message' => 'Incentive Details deleted successfully']);
        } else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Error in deleting']); 
        }
        return $response;
        
      }
}
