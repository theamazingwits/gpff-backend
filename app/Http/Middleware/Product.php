<?php

namespace App\Http\Controllers;

use Closure;
use DB;
use \PDF;
use Mail;
use Carbon\Carbon;
use App\Imports\UsersImport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;

use DOMDocument;

require(base_path().'/app/Http/Middleware/Common.php');
// require(base_path().'/app/Http/Middleware/Order.php');

class Product 
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        return $next($request);
    }
////////////////////////////////
//Product Management Api Calls//
////////////////////////////////
//Category
    //Admin Or Sub Admin Add the Category 
    public function addCategory($Products) 
    {   
        $exit = DB::table('gpff_category')
                ->where('category_name', $Products->category_name)
                ->where('region_id', $Products->region_id)
                ->where('branch_id', $Products->branch_id)
                ->get();

        if(count($exit) > 0){
            return 2;
        } else{
             $values = array(
                'category_name'     => $Products->category_name , 
                'region_id'         => $Products->region_id,
                'branch_id'         => $Products->branch_id,
                'created_at'        => date('Y-m-d H:i:s') , 
                'updated_at'        => date('Y-m-d H:i:s')
            );
            DB::table('gpff_category')
            ->insert($values);
            return 1;
        }
    }
    // Update Category Details
    public function updateCategory($Products) 
    {
        $values = array(
                'category_name'     => $Products->category_name ,
                'region_id'         => $Products->region_id,
                'branch_id'         => $Products->branch_id,
                'updated_at'        => date('Y-m-d H:i:s')
            );
        return  DB::table('gpff_category')
                ->where('category_id', $Products->category_id)
                ->update($values);
    }
    // Delete Category details
    public function removeCategory($Products)
    {
        return  DB::table('gpff_category')
                ->where('category_id', $Products->category_id)
                ->delete();
    }
    //Category Activate Or Deactivate
    public function categoryActiveOrDeactive($Products) 
    {
        $values = array(
                'category_status'   => $Products->category_status ,  
                'updated_at'        => date('Y-m-d H:i:s')
             );
        return DB::table('gpff_category')
        ->where('category_id', $Products->category_id)
        ->update($values);  
    }
    // Fetch (All)Category Details
    public function getAllCategory() 
    {
        $data =[];
        $data['totaldata']= DB::table('gpff_category')
                            ->orderBy('updated_at','DESC')
                            ->get();
         return $data;   
    }

    // Fetch Region based Category Details
    public function getRegionBasedCategory($Products) 
    {
        $data =[];
        $data['totaldata']= DB::table('gpff_category')
                            ->where('region_id',$Products->region_id)
                            ->orderBy('updated_at','DESC')
                            ->get();
         return $data;   
    }
//Product
    //Admin Or Sub Admin Add the Product 
    public function addProduct($Products) 
    {   
        if($Products->type == 1)
        {   
            //product_brand == product_genericname,
            //product_generic == product_description
            //incentive_type == product_type
            $extension = $Products->productall->getClientOriginalExtension();
            if($extension == "xlsx" || $extension == "xls" || $extension == "csv") 
            {   
                $path = $Products->productpath;

                $import = new UsersImport;
                Excel::import($import, $Products->productall);
                $data = $import->data;

                if(!empty($data) && count($data))
                {
                    $values = [];
                    foreach ($data as $value) 
                    {   
                        /*if($value['product_foc']."" != ""){
                            print_r($value['product_foc']."");
                        exit;
                        }else{
                            print_r($value);
                        exit;
                        }*/
                        

                        if($value['category_id'] != "" && $value['category_name'] != "" && $value['brand_name'] != "" && $value['generic_composition']."" != "" && $value['product_packingsize']."" != "" && $value['incentive_type'] != "" && $value['product_netprice']."" != "" && $value['product_grossprice']."" != "" && $value['product_discount']."" != "" && $value['region_id'] != "" && $value['company_id'] != "" && $value['region_name'] != "" && $value['product_foc']."" != "")
                        {
                            

                            $result = DB::table('gpff_product')
                                    ->where('category_name', $value['category_name'])
                                    ->where('product_genericname', $value['brand_name'])
                                    ->where('product_packingsize', $value['product_packingsize'])
                                    ->where('product_type', $value['incentive_type'])
                                    ->where('region_id', $value['region_id'])
                                    ->where('branch_id', $value['company_id'])
                                    ->get();

                            if (count($result) == 0) {

                                $branch =  DB::table('gpff_branch')
                                            ->where('branch_id', $value['company_id'])
                                            ->First();

                                $values[] = [
                                'category_id'         => $value['category_id'] ,
                                'category_name'       => $value['category_name'] ,
                                'product_genericname' => $value['brand_name'] ,
                                'product_description' => $value['generic_composition'] ,
                                'product_packingsize' => $value['product_packingsize'] ,
                                'product_type'        => $value['incentive_type'] ,
                                'product_netprice'    => $value['product_netprice'] ,
                                'product_grossprice'  => $value['product_grossprice'] ,
                                'product_discount'    => $value['product_discount'] ,
                                'product_foc'         => $value['product_foc'] ,
                                'region_id'           => $value['region_id'],
                                'branch_id'           => $value['company_id'],
                                'region_name'         => $value['region_name'],
                                'branch_name'         => $branch->branch_name,
                                'created_at'          => date('Y-m-d H:i:s') , 
                                'updated_at'          => date('Y-m-d H:i:s')
                                ];
                            } else{
                                return 2;
                            }
                        } else{
                             print_r($value);
                        exit;
                            return 4;
                        }
                    }
                    if(count($values) > 0){
                         DB::table('gpff_product')->insert($values);
                         return 1;
                    }else{
                        return 0;
                    }
                }
            }else{
                return 3;
            }
        }else{
            $exit = DB::table('gpff_product')
                ->where('category_name', $Products->category_name)
                ->where('product_genericname', $Products->product_genericname)
                ->where('product_packingsize', $Products->product_packingsize)
                ->where('product_type', $Products->product_type)
                ->where('region_id', $Products->region_id)
                ->where('branch_id', $Products->branch_id)
                ->get();

            if(count($exit) > 0){
                return 2;
            } else{
                $branch =  DB::table('gpff_branch')
                            ->where('branch_id', $Products->branch_id)
                            ->First();

                $values = array(
                    'category_id'         => $Products->category_id ,
                    'category_name'       => $Products->category_name ,
                    'product_genericname' => $Products->product_genericname ,
                    'product_description' => $Products->product_description ,
                    'product_packingsize' => $Products->product_packingsize ,
                    'product_type'        => $Products->product_type ,
                    'product_netprice'    => $Products->product_netprice ,
                    'product_grossprice'  => $Products->product_grossprice ,
                    'product_discount'    => $Products->product_discount ,
                    'product_foc'         => $Products->product_foc ,
                    'product_img'         => $Products->product_img , 
                    'region_id'           => $Products->region_id,
                    'branch_id'           => $Products->branch_id,
                    'region_name'         => $Products->region_name,
                    'branch_name'         => $branch->branch_name,
                    'created_at'          => date('Y-m-d H:i:s') , 
                    'updated_at'          => date('Y-m-d H:i:s')
                );

                $productid   =   DB::table('gpff_product')
                                ->insertGetId($values);
                return 1;
            }
        }
    }
    // Update Product Details
    public function updateProduct($Products) 
    {
        $values = array(
                'category_id'         => $Products->category_id ,
                'category_name'       => $Products->category_name ,
                'product_genericname' => $Products->product_genericname ,
                'product_description' => $Products->product_description ,
                'product_packingsize' => $Products->product_packingsize ,
                'product_type'        => $Products->product_type ,
                'product_netprice'    => $Products->product_netprice ,
                'product_grossprice'  => $Products->product_grossprice ,
                'product_discount'    => $Products->product_discount ,
                'product_foc'         => $Products->product_foc ,
                'product_img'         => $Products->product_img ,
                'region_id'           => $Products->region_id,
                'branch_id'           => $Products->branch_id,
                'region_name'         => $Products->region_name,
                'updated_at'          => date('Y-m-d H:i:s')
            );
        if($Products->product_old_netprice != $Products->product_netprice || $Products->product_old_grossprice != $Products->product_grossprice){
            $warehouse = DB::table('gpff_warehouse')
                        ->where('region_id',$Products->region_id)
                        ->get();
            $warehouses = [];
            foreach ($warehouse as $value) {
                $warehouses[] = $value->warehouse_id;
            }
            $values1 = array(
                'product_netprice'    => $Products->product_netprice ,
                'product_grossprice'  => $Products->product_grossprice ,
                'updated_at'          => date('Y-m-d H:i:s')
            );
            DB::table('gpff_product_batch_stock')
            ->where('product_id', $Products->product_id)
            ->whereIn('warehouse_id',$warehouses)
            ->update($values1);

            //Product Price Logs
            $values2 = array(
                'category_id'         => $Products->category_id ,
                'category_name'       => $Products->category_name ,
                'product_id'  => $Products->product_id,
                'product_genericname' => $Products->product_genericname ,
                'old_net_price'    => $Products->product_old_netprice ,
                'old_gross_price'  => $Products->product_old_grossprice ,
                'net_price'    => $Products->product_netprice ,
                'gross_price'  => $Products->product_grossprice ,
                'region_id'           => $Products->region_id,
                'branch_id'           => $Products->branch_id,
                'updated_by' => $Products->user_id,
                'created_at'          => date('Y-m-d H:i:s'),
                'updated_at'          => date('Y-m-d H:i:s')
            );
            DB::table('product_price_logs')
            ->insert($values2);
        }

        return  DB::table('gpff_product')
                ->where('product_id', $Products->product_id)
                ->update($values);
    }

    // Delete Product details
    public function removeProduct($Products)
    {
        return  DB::table('gpff_product')
                ->where('product_id', $Products->product_id)
                ->delete();
    }
    //Get Indivitual Product Details
    public function getIndProduct($Products)
    {   
        return  DB::table('gpff_product')
                ->where('product_id', $Products->product_id)
                ->get();
    }
    //product Activate Or Deactivate
    public function productActiveOrDeactive($Products) 
    {
        $values = array(
                'product_status'   => $Products->product_status ,  
                'updated_at'        => date('Y-m-d H:i:s')
             );
        return DB::table('gpff_product')
        ->where('product_id', $Products->product_id)
        ->update($values);  
    }
    // Fetch (All)product Details
    public function getAllProduct() 
    {
        $data =[];
        $data['totaldata']= DB::table('gpff_product')
                            ->orderBy('updated_at','DESC')
                            ->get();
         return $data;   
    }
    // Fetch Region based Products Details
    public function getRegionBasedProducts($Products) 
    {
        $data =[];
        $data['totaldata']= DB::table('gpff_product')
                            ->where('region_id',$Products->region_id)
                            ->orderBy('updated_at','DESC')
                            ->get();
         return $data;   
    }
    // Fetch Category Based (all)Product Details
    public function getCategoryBasedProduct($Products)
    {   
        return  DB::table('gpff_product')
                ->where('category_id', $Products->category_id)
                ->get();
    }
    
    // Fetch Category Based (all)Product Details
    public function getCategoryBasedProductPagi($Products)
    {   
        $data = DB::table('gpff_product as gpro')
                ->where('category_id', $Products->category_id)
                ->orderBy('gpro.product_id','DESC')
                ->select(DB::RAW('SQL_CALC_FOUND_ROWS gpro.product_id'),'gpro.category_id','gpro.category_name','gpro.product_genericname','gpro.product_description','gpro.product_img','gpro.product_packingsize','gpro.product_price','gpro.product_netprice','gpro.product_grossprice','gpro.product_discount','gpro.product_type','gpro.product_foc','gpro.branch_id','gpro.branch_name','gpro.region_id','gpro.region_name','gpro.area_id','gpro.product_status');

        $total_count = $data->count();      
        
        $data->offset($Products->start);
        $data->limit($Products->length);
        $data = $data->get();

        $total_filter_count = DB::select(DB::Raw('SELECT FOUND_ROWS() AS    totalfiltercount;'));

        return array(
                0 => $total_count,
                1 => $total_filter_count[0]->totalfiltercount,
                2 => $data
            );
    }
    // Fetch all Product Details Pagination
    public function getAllProductPagi($Products)
    {   
        $data = DB::table('gpff_product as gpro')
                ->where('region_id', $Products->region_id)
                ->orderBy('gpro.product_id','DESC')
                ->select(DB::RAW('SQL_CALC_FOUND_ROWS gpro.product_id'),'gpro.category_id','gpro.category_name','gpro.product_genericname','gpro.product_description','gpro.product_img','gpro.product_packingsize','gpro.product_price','gpro.product_netprice','gpro.product_grossprice','gpro.product_discount','gpro.product_type','gpro.product_foc','gpro.branch_id','gpro.branch_name','gpro.region_id','gpro.region_name','gpro.area_id','gpro.product_status');

        $total_count = $data->count();      
        
        $data->offset($Products->start);
        $data->limit($Products->length);
        $data = $data->get();

        $total_filter_count = DB::select(DB::Raw('SELECT FOUND_ROWS() AS    totalfiltercount;'));

        return array(
                0 => $total_count,
                1 => $total_filter_count[0]->totalfiltercount,
                2 => $data
            );
    }

    //Product Stock
    //Stockist Add the Product Stock
    public function addProductStock($Products) 
    {   
    //Stroed In Product Recived History
        $values = array(
                'warehouse_id'      => $Products->warehouse_id ,
                'stockist_id'       => $Products->stockist_id,
                'supplier_name'     => $Products->supplier_name ,
                'invoice_no'        => $Products->invoice_no ,
                'invoice_date'      => $Products->invoice_date ,
                'grand_tot_amt'     => $Products->grand_tot_amt , 
                'payment_type'      => $Products->payment_type , 
                'region_id'         => $Products->region_id,
                'branch_id'         => $Products->branch_id,
                'created_at'        => date('Y-m-d H:i:s') , 
                'updated_at'        => date('Y-m-d H:i:s')
            );

        $prod_re_his_id   =   DB::table('gpff_product_recived_history')
                            ->insertGetId($values);

        $pdf_name = "stock_".$Products->invoice_no.'.pdf';

        $values = array(
            "invoice_no"=> $pdf_name,
            "updated_at"=> date('Y-m-d H:i:s')
        );

        DB::table('gpff_product_recived_history')
        ->where('product_recived_history_id', $prod_re_his_id)
        ->update($values);
       
        foreach($Products->prduct_details as $prduct_lists)
        {
            $list_value = array(
                'product_recived_history_id' => $prod_re_his_id,
                'category_id'           => $prduct_lists['category_id'],
                'category_name'         => $prduct_lists['category_name'],
                'product_id'            => $prduct_lists['product_id'],
                'product_genericname'   => $prduct_lists['product_genericname'],
                'product_type'          => $prduct_lists['product_type'],
                'batch_id'              => $prduct_lists['batch_id'],
                'exp_date'              => $prduct_lists['exp_date'],
                'UOM'                   => $prduct_lists['UOM'],
                'no_of_item'            => $prduct_lists['no_of_item'],
                'product_qty'           => $prduct_lists['product_qty'],
                'product_netprice'      => $prduct_lists['product_netprice'],
                'product_grossprice'    => $prduct_lists['product_grossprice'],
                'product_discount'      => $prduct_lists['product_discount'],
                'product_tot_amt'       => $prduct_lists['product_tot_amt'],
                'created_at'            => date('Y-m-d H:i:s') ,
                'updated_at'            => date('Y-m-d H:i:s')
            );

            DB::table('gpff_product_recived_history_list')
            ->insert($list_value);
        }

        //Stored in Stock Table
        foreach($Products->prduct_details as $prduct_lists)
        {   
            $exit = DB::table('gpff_product_stock')
                ->where('category_id', $prduct_lists['category_id'])
                ->where('product_id', $prduct_lists['product_id'])
                ->where('warehouse_id', $Products->warehouse_id)
                ->get();

            $details = DB::table('gpff_product_batch_stock')
                    ->where('category_id', $prduct_lists['category_id'])
                    ->where('product_id', $prduct_lists['product_id'])
                    ->where('batch_id', $prduct_lists['batch_id'])
                    ->where('warehouse_id', $Products->warehouse_id)
                    ->get();      

            $war = DB::table('gpff_warehouse')
                ->where('warehouse_id', $Products->warehouse_id)
                ->First();

            if(count($exit)== 0 && count($details)==0){

                $productvalue = array(
                'category_id'           => $prduct_lists['category_id'],
                'category_name'         => $prduct_lists['category_name'],
                'product_id'            => $prduct_lists['product_id'],
                'product_genericname'   => $prduct_lists['product_genericname'],
                'product_type'          => $prduct_lists['product_type'],
                'warehouse_id'          => $Products->warehouse_id ,
                'warehouse_name'        => $war->warehouse_name ,
                'stockist_id'           => $Products->stockist_id,
                'region_id'             => $Products->region_id,
                'branch_id'             => $Products->branch_id,
                'created_at'            => date('Y-m-d H:i:s') ,
                'updated_at'            => date('Y-m-d H:i:s')
                );

                $prod_sto_id = DB::table('gpff_product_stock')
                ->insertGetId($productvalue);

                $batch_stock_list_value = array(
                'product_stock_id'      => $prod_sto_id ,
                'category_id'           => $prduct_lists['category_id'],
                'warehouse_id'          => $Products->warehouse_id ,
                'stockist_id'           => $Products->stockist_id,
                'product_id'            => $prduct_lists['product_id'],
                'batch_id'              => $prduct_lists['batch_id'],
                'product_genericname'   => $prduct_lists['product_genericname'],
                'product_netprice'      => $prduct_lists['product_netprice'],
                'product_grossprice'    => $prduct_lists['product_grossprice'],
                'product_discount'      => $prduct_lists['product_discount'],
                'exp_date'              => $prduct_lists['exp_date'],
                'product_tot_qty'       => $prduct_lists['no_of_item'],
                'product_blc_qty'       => $prduct_lists['no_of_item'],
                'created_at'            => date('Y-m-d H:i:s') ,
                'updated_at'            => date('Y-m-d H:i:s')
                );

                DB::table('gpff_product_batch_stock')
                ->insert($batch_stock_list_value);

            }elseif(count($details)!=0 && count($exit)!=0){

                $prstid = DB::table('gpff_product_stock')
                ->where('category_id', $prduct_lists['category_id'])
                ->where('product_id', $prduct_lists['product_id'])
                ->where('warehouse_id', $Products->warehouse_id)
                ->First();

                $details = DB::table('gpff_product_batch_stock')
                    ->where('category_id', $prduct_lists['category_id'])
                    ->where('product_id', $prduct_lists['product_id'])
                    ->where('batch_id', $prduct_lists['batch_id'])
                    ->where('warehouse_id', $Products->warehouse_id)
                    ->First();

                $productstock = DB::table('gpff_product_batch_stock')
                            ->where('product_stock_id',$prstid->product_stock_id)
                            ->First();
                if($details->batch_id == $prduct_lists['batch_id']){
                    $prtotquantity = $details->product_tot_qty + $prduct_lists['no_of_item'];
                    $prblcqty = $details->product_blc_qty + $prduct_lists['no_of_item'];
                    
                    $values = array(
                        'product_tot_qty'   => $prtotquantity,
                        'product_blc_qty'   => $prblcqty,
                        'product_netprice'      => $prduct_lists['product_netprice'],
                        'product_grossprice'    => $prduct_lists['product_grossprice'],
                        'product_status'    => 1,
                        'updated_at'        => date('Y-m-d H:i:s')
                    );

                        DB::table('gpff_product_batch_stock')
                        ->where('product_batch_stock_id', $details->product_batch_stock_id)
                        ->update($values);

                }
            }else{

                $prstid = DB::table('gpff_product_stock')
                ->where('category_id', $prduct_lists['category_id'])
                ->where('product_id', $prduct_lists['product_id'])
                ->where('warehouse_id', $Products->warehouse_id)
                ->First();

                $details = DB::table('gpff_product_batch_stock')
                    ->where('category_id', $prduct_lists['category_id'])
                    ->where('product_id', $prduct_lists['product_id'])
                    ->where('batch_id', $prduct_lists['batch_id'])
                    ->where('warehouse_id', $Products->warehouse_id)
                    ->First();

                $batch_stock_list_value = array(
                'product_stock_id'      => $prstid->product_stock_id ,
                'category_id'           => $prduct_lists['category_id'],
                'warehouse_id'          => $Products->warehouse_id ,
                'stockist_id'           => $Products->stockist_id,
                'product_id'            => $prduct_lists['product_id'],
                'batch_id'              => $prduct_lists['batch_id'],
                'product_genericname'   => $prduct_lists['product_genericname'],
                'product_netprice'      => $prduct_lists['product_netprice'],
                'product_grossprice'    => $prduct_lists['product_grossprice'],
                'product_discount'      => $prduct_lists['product_discount'],
                'exp_date'              => $prduct_lists['exp_date'],
                'product_tot_qty'       => $prduct_lists['no_of_item'],
                'product_blc_qty'       => $prduct_lists['no_of_item'],
                'created_at'            => date('Y-m-d H:i:s') ,
                'updated_at'            => date('Y-m-d H:i:s')
                );

                DB::table('gpff_product_batch_stock')
                ->insert($batch_stock_list_value);
            }
        }

        $com_details = DB::table('gpff_branch')
                        ->where('branch_id',$Products->branch_id)
                        ->First();

        $cus_de = DB::table('gpff_users')
                        ->where('user_id',$Products->stockist_id)
                        ->First();

        $war = DB::table('gpff_warehouse')
                ->where('warehouse_id', $Products->warehouse_id)
                ->First();

        // print_r($war);
        if($Products->payment_type == 1){
            $payment_method = "Cash";
        }else{
            $payment_method = "Credit";
        }

        $invoice = $Products->invoice_no;

        $total = $Products->grand_tot_amt;

        $number = $this->numbertostring($total);

        view()->share('datas',$Products->prduct_details);
        view()->share('cus_de',$cus_de);
        view()->share('invoice',$invoice);
        view()->share('war',$war);
        view()->share('com_details',$com_details);
        view()->share('value',$number);
        view()->share('payment_method',$payment_method);
        view()->share('total',$total);
        view()->share('date',Carbon::now());

        $pdf = \PDF::loadView('stock_updates')
        ->setPaper('a4', 'landscape');
        $pdf->save(public_path('stock_invoice/'.$pdf_name));
        $file_name = $pdf_name;
        $name = $file_name;
        $filePath = 'stock_invoice/'.$name; 
  
        Storage::disk('s3')->put($filePath, file_get_contents(public_path('stock_invoice')."/".$file_name));

        return 1;
    }

    public function addBulkProductStock($Products) 
    {   
        $extension = $Products->productall->getClientOriginalExtension();
        if($extension == "xlsx" || $extension == "xls" || $extension == "csv") 
        {
            $path = $Products->productpath;
            $import = new UsersImport;
            Excel::import($import, $Products->productall);
            $data = $import->data;
            if(!empty($data) && count($data))
            {
                $values = [];
                $insert_values = array(
                    'warehouse_id'      => $Products->warehouse_id ,
                    'stockist_id'       => $Products->stockist_id,
                    'supplier_name'     => $Products->supplier_name ,
                    'invoice_date'      => date('Y-m-d H:i:s'),
                    'payment_type'      => $Products->payment_type , 
                    'region_id'         => $Products->region_id,
                    'branch_id'         => $Products->branch_id,
                    'upload_type'       => '2',
                    'created_at'        => date('Y-m-d H:i:s'), 
                    'updated_at'        => date('Y-m-d H:i:s')
                );
                $prod_re_his_id   =   DB::table('gpff_product_recived_history')
                            ->insertGetId($insert_values);
                foreach ($data as $value) 
                {

                    // print_r($value);
                    // exit();
                    // print_r($value['category_id']);
                    
                    if($value['category_id'] != "" && $value['category_name'] != "" && $value['product_id'] != "" && $value['product_name'] != "" && $value['product_type'] != "" && $value['warehouse_id'] != "" && $value['stockist_id'] != "" && $value['region_id'] != "" && $value['branch_id'] != "" && $value['batch_id'] != "" && $value['product_netprice'] != "" && $value['product_grossprice'] != "" && $value['exp_date'] != "" && $value['qty'] != "" && (int)$value['qty'] >= 0)
                    {
                        $exit = DB::table('gpff_product_stock')
                        ->where('category_id', $value['category_id'])
                        ->where('product_id', $value['product_id'])
                        ->where('warehouse_id', $value['warehouse_id'])
                        ->get();

                        $details = DB::table('gpff_product_batch_stock')
                        ->where('category_id', $value['category_id'])
                        ->where('product_id', $value['product_id'])
                        ->where('batch_id', $value['batch_id'])
                        ->where('warehouse_id', $value['warehouse_id'])
                        ->get();

                        $war = DB::table('gpff_warehouse')
                        ->where('warehouse_id', $value['warehouse_id'])
                        ->First();

                        if(count($exit)== 0 && count($details)==0){

                            $productvalue = array(
                                'category_id'           => $value['category_id'],
                                'category_name'         => $value['category_name'],
                                'product_id'            => $value['product_id'],
                                'product_genericname'   => $value['product_name'],
                                'product_type'          => $value['product_type'],
                                'warehouse_id'          => $value['warehouse_id'] ,
                                'warehouse_name'        => $war->warehouse_name ,
                                'stockist_id'           => $value['stockist_id'],
                                'region_id'             => $value['region_id'],
                                'branch_id'             => $value['branch_id'],
                                'product_status'        => 1,
                                'created_at'            => date('Y-m-d H:i:s') ,
                                'updated_at'            => date('Y-m-d H:i:s')
                            );

                            $prod_sto_id =  DB::table('gpff_product_stock')
                            ->insertGetId($productvalue);

                            $batch_stock_list_value = array(
                                'product_stock_id'      => $prod_sto_id ,
                                'category_id'           => $value['category_id'],
                                'warehouse_id'          => $value['warehouse_id'],
                                'stockist_id'           => $value['stockist_id'],
                                'product_id'            => $value['product_id'],
                                'batch_id'              => $value['batch_id'],
                                'product_genericname'   => $value['product_name'],
                                'product_netprice'      => $value['product_netprice'],
                                'product_grossprice'    => $value['product_grossprice'],
                                'exp_date'              => $value['exp_date'],
                                'product_tot_qty'       => $value['qty'],
                                'product_blc_qty'       => $value['qty'],
                                'created_at'            => date('Y-m-d H:i:s') ,
                                'updated_at'            => date('Y-m-d H:i:s')
                            );

                            DB::table('gpff_product_batch_stock')
                            ->insert($batch_stock_list_value);
                        }elseif(count($details)!=0 && count($exit)!=0){
                            $prstid = DB::table('gpff_product_stock')
                            ->where('category_id', $value['category_id'])
                            ->where('product_id', $value['product_id'])
                            ->where('warehouse_id',  $value['warehouse_id'])
                            ->First();

                            $details = DB::table('gpff_product_batch_stock')
                            ->where('category_id', $value['category_id'])
                            ->where('product_id', $value['product_id'])
                            ->where('batch_id', $value['batch_id'])
                            ->where('warehouse_id', $value['warehouse_id'])
                            ->First();

                            $productstock = DB::table('gpff_product_batch_stock')
                            ->where('product_stock_id',$prstid->product_stock_id)
                            ->First();

                            if($details->batch_id == $value['batch_id']){
                                $prtotquantity = $details->product_tot_qty + $value['qty'];
                                $prblcqty = $details->product_blc_qty + $value['qty'];

                                $values = array(
                                    'product_tot_qty'   => $prtotquantity,
                                    'product_blc_qty'   => $prblcqty,
                                    'product_status'    => 1,
                                    'updated_at'        => date('Y-m-d H:i:s')
                                );

                                DB::table('gpff_product_batch_stock')
                                ->where('product_batch_stock_id', $details->product_batch_stock_id)
                                ->update($values);

                            }
                        }else{

                            $prstid = DB::table('gpff_product_stock')
                            ->where('category_id', $value['category_id'])
                            ->where('product_id', $value['product_id'])
                            ->where('warehouse_id', $value['warehouse_id'])
                            ->First();

                            $details = DB::table('gpff_product_batch_stock')
                            ->where('category_id', $value['category_id'])
                            ->where('product_id', $value['product_id'])
                            ->where('batch_id', $value['batch_id'])
                            ->where('warehouse_id', $value['warehouse_id'])
                            ->First();

                            $batch_stock_list_value = array(
                                'product_stock_id'      => $prstid->product_stock_id ,
                                'category_id'           => $value['category_id'],
                                'warehouse_id'          => $value['warehouse_id'] ,
                                'stockist_id'           => $value['stockist_id'],
                                'product_id'            => $value['product_id'],
                                'batch_id'              => $value['batch_id'],
                                'product_genericname'   => $value['product_name'],
                                'product_netprice'      => $value['product_netprice'],
                                'product_grossprice'    => $value['product_grossprice'],
                            // 'product_discount'      => $value['product_discount'],
                                'exp_date'              => $value['exp_date'],
                                'product_tot_qty'       => $value['qty'],
                                'product_blc_qty'       => $value['qty'],
                                'created_at'            => date('Y-m-d H:i:s') ,
                                'updated_at'            => date('Y-m-d H:i:s')
                            );

                            DB::table('gpff_product_batch_stock')
                            ->insert($batch_stock_list_value);
                        }
                        $list_value = array(
                            'product_recived_history_id' => $prod_re_his_id,
                            'category_id'           => $value['category_id'],
                            'category_name'         => $value['category_name'],
                            'product_id'            => $value['product_id'],
                            'product_genericname'   => $value['product_name'],
                            'product_type'          => $value['product_type'],
                            'batch_id'              => $value['batch_id'],
                            'exp_date'              => $value['exp_date'],
                            'UOM'                   => 'UOM',
                            'no_of_item'            => $value['qty'],
                            'product_qty'           => $value['qty'],
                            'product_netprice'      => $value['product_netprice'],
                            'product_grossprice'    => $value['product_grossprice'],
                            'created_at'        => date('Y-m-d H:i:s'), 
                            'updated_at'        => date('Y-m-d H:i:s')
                        );

                        DB::table('gpff_product_recived_history_list')
                        ->insert($list_value);
                    }else{
                        return 4;
                    }
                }
                return 1;
            } else {
                return 2;   
            }
            
        }else{
            return 3;
        }
    }
    
    //Stock EXP And Batch edit
    public function updateProductsBatchExp($Products)
    {
        $values = array(
                'exp_date'   => $Products->new_exp_date ,
                'updated_at' => date('Y-m-d H:i:s')
             );

        return DB::table('gpff_product_batch_stock')
        ->where('product_id', $Products->product_id)
        ->where('category_id', $Products->category_id)
        ->where('batch_id', $Products->batch_id)
        ->where('exp_date', $Products->exp_date)
        ->update($values);  
    }
    // Fetch Warehouse Based Product Stock Recived Details
    public function getGRHistory($Products)
    {   
        return  DB::table('gpff_product_recived_history')
                ->where('region_id', $Products->region_id)
                ->where('warehouse_id', $Products->warehouse_id)
                ->orderBy('created_at','DESC')
                ->get();
    }
    // Fetch Ind Based Stock Details Fetch
    public function getIndGRBatch($Products) 
    {
       return   DB::table('gpff_product_recived_history_list as gprhl')
                ->leftjoin('gpff_product_recived_history as gprh','gprhl.product_recived_history_id','gprh.product_recived_history_id')
                ->where('gprh.product_recived_history_id',$Products->product_recived_history_id)
                ->orderBy('gprh.updated_at','DESC')
                ->get(['gprh.supplier_name','gprh.invoice_no','gprh.invoice_date','gprh.payment_type','gprh.grand_tot_amt','gprh.warehouse_id','gprh.stockist_id','gprhl.category_id','gprhl.category_name','gprhl.product_id','gprhl.product_genericname','gprhl.product_type','gprhl.batch_id','gprhl.exp_date','gprhl.UOM','gprhl.no_of_item','gprhl.product_qty','gprhl.product_netprice','gprhl.product_grossprice','gprhl.product_discount','gprhl.product_tot_amt','gprhl.created_at','gprhl.updated_at']);
    }

    // Fetch Warehouse Based Product Stock Details
    public function getWarehouseBasedProductStock($Products)
    {   
        return  DB::table('gpff_product_stock')
                ->where('region_id', $Products->region_id)
                ->where('warehouse_id', $Products->warehouse_id)
                ->orderBy('updated_at','DESC')
                ->get();
    }
    // Fetch Warehouse Based Product Stock Batch Details
    public function getWarehouseBasedProductBatch($Products)
    {   
        return  DB::table('gpff_product_batch_stock')
                ->where('product_id', $Products->product_id)
                ->where('warehouse_id', $Products->warehouse_id)
                ->where('product_status', 1)
                ->where('product_blc_qty','!=', 0)
                ->orderBy('exp_date','ASC')
                ->get(['batch_id']);
    }
    // Fetch Batch Based Product Qty Details
    public function getBatchBasedQty($Products)
    {   
        return  DB::table('gpff_product_batch_stock')
                ->where('product_id', $Products->product_id)
                ->where('warehouse_id', $Products->warehouse_id)
                ->where('batch_id', $Products->batch_id)
                ->where('product_status', 1)
                ->where('product_blc_qty','!=', 0)
                ->get(['batch_id','product_blc_qty','product_netprice','product_grossprice','exp_date']);
    }
    // Fetch Warehouse Based Product Stock Stock List
    public function getWarehouseBasedProductStockList($Products)
    {   
        if($Products->type == 1){
            return  DB::table('gpff_product_batch_stock')
                    ->where('product_status', 1)
                    ->where('warehouse_id', $Products->warehouse_id)
                    //->where('product_blc_qty','!=', 0)
                    ->orderBy('exp_date','ASC')
                    ->get();
        }else{
            return  DB::table('gpff_product_batch_stock')
                    ->where('product_id', $Products->product_id)
                    ->where('warehouse_id', $Products->warehouse_id)
                    ->where('product_status', 1)
                    //->where('product_blc_qty','!=', 0)
                    ->orderBy('exp_date','ASC')
                    ->get();
        }
    }
    //End Product Stock

    // Fetch Multiple Warehouse Based Product Stock List
    public function getMultipleWarehouseBasedProductStockList($Products)
    {   
        $stock_details = DB::table('gpff_product_batch_stock as pbs')
                    ->join('gpff_warehouse as gw','gw.warehouse_id','pbs.warehouse_id')
                    ->where('product_status', 1)
                    ->select(['product_batch_stock_id','product_stock_id','category_id','pbs.warehouse_id','pbs.stockist_id','product_id','batch_id','product_genericname','product_sales_qty','product_tot_qty','product_blc_qty','product_sale_FOC_qty','product_sale_spl_FOC_qty','product_price','product_netprice','product_grossprice','product_discount','pbs.branch_id','pbs.region_id','pbs.area_id','exp_date','product_status','pbs.created_at','pbs.updated_at','warehouse_name'])
                    //->where('product_blc_qty','!=', 0)
                    ->orderBy('exp_date','ASC');
        if($Products->branch_id != ''){
            $stock_details = $stock_details->whereIn('gw.branch_id', $Products->branch_id);
        }
        if($Products->region_id != ''){
            $stock_details = $stock_details->whereIn('gw.region_id', $Products->region_id);
        }
        if($Products->warehouse_id != ''){
            $stock_details = $stock_details->whereIn('pbs.warehouse_id', $Products->warehouse_id);
        }
        if($Products->product_id != ''){
            $stock_details = $stock_details->whereIn('pbs.product_id', $Products->product_id);
        }
        return  $stock_details->get();
    }
    //End Product Stock

    //Get Indivitual Product Qty Details
    public function getIndProductQty($Products)
    {   
        return  DB::table('gpff_product_stock')
                ->where('product_id', $Products->product_id)
                ->get();
    }
    //Get Indivitual Product Qty Details
    public function getIndProductsQtyDetails($Products)
    {   
        $data =[];
        $data['qty'] =  DB::table('gpff_product_batch_stock')
                ->where('product_id', $Products->product_id)
                ->where('warehouse_id', $Products->warehouse_id)
                ->where('product_status',1)
                ->sum('product_blc_qty');

        $data['details'] = DB::table('gpff_product_stock')
                ->where('product_id', $Products->product_id)
                ->where('warehouse_id', $Products->warehouse_id)
                ->get(['category_id','category_name','product_id','product_genericname','product_type']);

        return $data;
    }
    
    //Region Based Field Details Fetch 
    public function getRegionBasedField($Products)
    {   
        return  DB::table('gpff_users')
                ->whereIn('region_id', [$Products->region_id])
                ->where('role', 5)
                ->orderBy('firstname','ASC')
                ->get();
    }
     //Region Based Area Manager Details Fetch 
    public function getRegionBasedAreaManager($Products)
    {   
        return  DB::table('gpff_users')
                ->whereIn('region_id', [$Products->region_id])
                ->where('role', 4)
                ->get();
    }
    //Admin Or Sub Admin Product Assign to the field Officer 
    public function productAssign($Products) 
    {   
        if($Products->area_manager_id){
            $areamanage_id  =  $Products->area_manager_id;
        } else{
            $areamanage_id ="";
        }

        if($Products->region_manager_id){
            $regionmanage_id  =  $Products->region_manager_id;
        } else{
            $regionmanage_id ="";
        }

        if($Products->type == 2){
                DB::table('gpff_product_assign')
                ->where('field_officer_id', $Products->field_officer_id)
                ->where('category_id', $Products->category_id)
                ->delete();
        } 
        foreach ($Products->product as  $productid) {
        
            $values = array(
                'product_id'        => $productid['product_id'],
                'category_id'       => $Products->category_id ,
                'field_officer_id'  => $Products->field_officer_id ,
                'area_manager_id'   => $areamanage_id ,
                'region_manager_id' => $regionmanage_id ,
                'region_id'         => $Products->region_id ,
                'created_at'        => date('Y-m-d H:i:s') ,
                'updated_at'        => date('Y-m-d H:i:s')
            );
            DB::table('gpff_product_assign')
            ->insert($values);
        }        
        
        //Notification Entry
        if($Products->area_manager_id){
            $name = DB::table('gpff_users')
                ->where('user_id', $Products->area_manager_id)
                ->First();
            $message = "Area Manager ".$name->firstname." has assign some products To you.";

            $page_id = 'UNKNOWN';

            $cmn = new Common();
            $cmn->insertNotification($Products->area_manager_id,$name->firstname,$Products->field_officer_id,$message,$page_id);

        } else if($Products->region_manager_id){
            $name = DB::table('gpff_users')
                ->where('user_id', $Products->region_manager_id)
                ->First();
            $message = "Region Manager ".$name->firstname." has assign some products To you.";

            $page_id = 'UNKNOWN';

            $cmn = new Common();
            $cmn->insertNotification($Products->region_manager_id,$name->firstname,$Products->field_officer_id,$message,$page_id);

        }
        //End Notification Entry

        return 1;
    }
    // Fetch field officer Based Assign Product Details
    public function getfieldBasedAssignProduct($Products)
    {   
        return  DB::table('gpff_product_assign as gpa')
                ->join('gpff_product as gpr','gpa.product_id', '=' , 'gpr.product_id')
                ->where('gpa.field_officer_id', $Products->field_officer_id)
                ->get();
    }
    // Fetch field officer Cate Based Assign Product Details
    public function getFOCateBasedAssignProduct($Products)
    {   
        return  DB::table('gpff_product_assign as gpa')
                ->join('gpff_product as gpr','gpa.product_id', '=' , 'gpr.product_id')
                ->where('gpa.field_officer_id', $Products->field_officer_id)
                ->where('gpa.category_id', $Products->category_id)
                ->get();
    }
    // Fetch (All)product Details
    public function getAllAssignProduct() 
    {
        $data =[];
        $data['totaldata']= DB::table('gpff_product_assign as gpa')
                            ->join('gpff_product as gpr','gpa.product_id', '=' , 'gpr.product_id')
                            ->join('gpff_users as gus','gpa.field_officer_id', '=' , 'gus.user_id')
                            ->get();
         return $data;   
    }
     //Area Manager Assign Product List Details Fetch
    public function getAreaManageAssignProduct($Products)
    {   
        return  DB::table('gpff_product_assign')
                ->where('area_manager_id', $Products->user_id)
                ->orderBy('updated_at','DESC')
                ->get();
    }
    //Region Manager Assign Product List Details Fetch
    public function getRegionManageAssignProduct($Products)
    {   
        $data = DB::table('gpff_users')
                    ->where('user_id', $Products->user_id)
                    ->First(['region_id']);

        $myArray = explode(',', $data->region_id);
        
        return  DB::table('gpff_product_assign')
                ->whereIn('region_id', $myArray)
                ->orderBy('updated_at','DESC')
                ->get();
    }
    
//Others
    //public file path
    public function getFilePath($file_name){   
        return $file_name->getClientOriginalName();
    }

    // Fetch Region Based Promotional & Generic Product
    public function getRegionandTypeBasedProducts($Products) 
    {
        $data =[];
        $data['totaldata']= DB::table('gpff_product')
                            ->where('region_id',$Products->region_id)
                            ->where('product_type',$Products->product_type)
                            ->orderBy('updated_at','DESC')
                            ->get();
         return $data;   
    }

//Stock Maintain
    //Product Stock Request Sub to Main
    public function storeRequest($Products)
    {   
        $war_name =     DB::table('gpff_warehouse')
                        ->where('warehouse_id',$Products->sub_ware_id)
                        ->First();

        $values = array(
                'sub_ware_id'          => $Products->sub_ware_id ,
                'sub_ware_name'        => $war_name->warehouse_name ,
                'warehouse_id'          => $Products->warehouse_id ,
                'warehouse_name'        => $Products->warehouse_name ,
                'region_id'             => $Products->region_id,
                'branch_id'             => $Products->branch_id,
                'created_at'            => date('Y-m-d H:i:s') , 
                'updated_at'            => date('Y-m-d H:i:s')
            );
           $req_id =  DB::table('gpff_product_request')
            ->insertGetId($values);

        foreach ($Products->purchase_product_list as $value) {
            $values = array(
                'product_request_id'    => $req_id ,
                'product_id'            => $value['product_id'] ,
                'product_genericname'   => $value['product_genericname'] ,
                'category_id'           => $value['category_id'] ,
                'category_name'         => $value['category_name'] ,
                'product_type'          => $value['product_type'],
                'req_quantity'          => $value['quantity'],
                'issued_quantity'       => 0,
                'pending_quantity'      => $value['quantity'],
                'created_at'            => date('Y-m-d H:i:s') , 
                'updated_at'            => date('Y-m-d H:i:s')
            );
            DB::table('gpff_product_request_list')
            ->insert($values);
        }
            return 1;
    }
    // Fetch Stock Request Details
    public function getStockRequest($Products) 
    {   
        //1-Main,2-sub
        if($Products->type == 1){
            return  DB::table('gpff_product_request')
                ->where('warehouse_id',$Products->warehouse_id)
                ->orderBy('created_at','DESC')
                ->get();
        }else{
            return  DB::table('gpff_product_request')
                ->where('sub_ware_id',$Products->warehouse_id)
                ->orderBy('created_at','DESC')
                ->get();
        }
    }
    // Fetch Stock Request Details List
    public function getStockRequestList($Products) 
    {   
        return  DB::table('gpff_product_request_list')
                ->where('product_request_id',$Products->product_request_id)
                ->orderBy('created_at','ASC')
                ->get();
    }
    // Fetch Stock Request Details List
    public function getStockRequestAcceptList($Products) 
    {   
        return  DB::table('gpff_product_request_accept_list')
                ->where('product_request_list_id',$Products->product_request_list_id)
                ->orderBy('created_at','ASC')
                ->get();
    }
    // Fetch Stock Request Details List
    public function getStockRequestBaseAcceptList($Products) 
    {   
        return  DB::table('gpff_product_request_accept_list')
                ->where('product_request_id',$Products->product_request_id)
                ->orderBy('created_at','ASC')
                ->get();
    }
    // Fetch Stock Request Details List
    public function getStockRequestListInd($Products) 
    {   
        return  DB::table('gpff_product_request_list')
                ->where('product_request_id',$Products->product_request_id)
                ->where('product_id',$Products->product_id)
                ->orderBy('created_at','ASC')
                ->get();
    }
    // Fetch Stock Request Details
    public function stockRequestAcceptOrReject($Products) 
    {   
        $war_details =  DB::table('gpff_product_request')
                        ->where('product_request_id',$Products->product_request_id)
                        ->First();

        $prod_details =  DB::table('gpff_product_request_list')
                        ->where('product_request_id',$Products->product_request_id)
                        ->where('product_id',$Products->product_id)
                        ->First();
        $cal_details =  DB::table('gpff_product_request_list')
                        ->where('product_request_id',$Products->product_request_id)
                        ->where('product_request_list_id',$Products->product_request_list_id)
                        ->where('product_id',$Products->product_id)
                        ->First();

        $req_qty = $cal_details->req_quantity;
        $iss_qty = $cal_details->issued_quantity;
        $pen_qty = $cal_details->pending_quantity;

        if($Products->product_qty > $pen_qty){
            return 2;
        }
        $issued_qty = $iss_qty + $Products->product_qty;
        $pendin_qty = $pen_qty - $Products->product_qty;

        $values1 = array(
                'product_request_id'         => $Products->product_request_id ,
                'product_request_list_id'    => $Products->product_request_list_id ,
                'category_id'                => $Products->category_id ,
                'category_name'              => $Products->category_name ,
                'product_id'                 => $Products->product_id ,
                'product_genericname'        => $Products->product_genericname ,
                'batch_no'                   => $Products->batch_no,
                'product_qty'                => $Products->product_qty,
                'exp_date'                   => $Products->exp_date,
                'product_netprice'           => $Products->product_netprice,
                'product_grossprice'         => $Products->product_grossprice,
                'product_type'               => $Products->product_type,
                'price_type'               => $Products->price_type,
                'total_price'               => $Products->total_price,
                'created_at'                 => date('Y-m-d H:i:s') , 
                'updated_at'                 => date('Y-m-d H:i:s')
            );
        DB::table('gpff_product_request_accept_list')
        ->insert($values1);
        
        

        if($req_qty == $issued_qty){
            $product_status = 1;
        }else{
            $product_status = 0;
        }
        $value = array(
            'issued_quantity'   => $issued_qty ,
            'pending_quantity'  => $pendin_qty ,
            'product_status'    => $product_status ,
            'approve_status'    => 1 ,
            'updated_at'        => date('Y-m-d H:i:s')
        );

        DB::table('gpff_product_request_list')
        ->where('product_request_id',$Products->product_request_id)
        ->where('product_request_list_id',$Products->product_request_list_id)
        ->where('product_id',$Products->product_id)
        ->update($value);

        $penOrcom =  DB::table('gpff_product_request_list')
                        ->where('product_request_id',$Products->product_request_id)
                        ->where('product_status',0)
                        ->get();
        $total_price = DB::table('gpff_product_request_accept_list')
                        ->where('product_request_id',$Products->product_request_id)
                        ->sum('total_price');
        if(count($penOrcom) == 0){
            $value = array(
                'product_status'    => 1 ,
                'approve_status'    => 1 ,
                'total_price'       => $total_price,
                'updated_at'        => date('Y-m-d H:i:s')
            );

            DB::table('gpff_product_request')
            ->where('product_request_id',$Products->product_request_id)
            ->update($value);
        }else{
            $value = array(
                'product_status'    => 0 ,
                'approve_status'    => 1 ,
                'total_price'       => $total_price,
                'updated_at'        => date('Y-m-d H:i:s')
            );

            DB::table('gpff_product_request')
            ->where('product_request_id',$Products->product_request_id)
            ->update($value);
        }
        //Main stock reduce
        $stock_details =  DB::table('gpff_product_batch_stock')
                        ->where('product_id',$Products->product_id)
                        ->where('batch_id',$Products->batch_no)
                        ->where('warehouse_id',$Products->warehouse_id)
                        ->First();

        $blc_stock_qty = $stock_details->product_blc_qty - $Products->product_qty;
        $sal_stock_qty = $stock_details->product_sales_qty + $Products->product_qty;

        if($blc_stock_qty <= 0){
             $product_status = 2;
        }else{
            $product_status = 1;
        }

        $stock_value = array(
            'product_blc_qty'   => $blc_stock_qty,
            'product_sales_qty' => $sal_stock_qty,
            'product_status'    => $product_status,
            'updated_at'        => date('Y-m-d H:i:s')
        );

        DB::table('gpff_product_batch_stock')
        ->where('product_id',$Products->product_id)
        ->where('batch_id',$Products->batch_no)
        ->where('warehouse_id',$Products->warehouse_id)
        ->update($stock_value);
        //Main stock reduce

        return 1;
    }

    // Stock acknowledge
    public function stockAcceptAcknowledge($Products) 
    {   
        $exit = DB::table('gpff_product_stock')
                ->where('category_id', $Products->category_id)
                ->where('product_id', $Products->product_id)
                ->where('warehouse_id', $Products->warehouse_id)
                ->First();

        $prstid = DB::table('gpff_product_batch_stock')
                ->where('category_id', $Products->category_id)
                ->where('product_id', $Products->product_id)
                ->where('batch_id', $Products->batch_no)
                ->where('warehouse_id', $Products->warehouse_id)
                ->First();
        
        $product_details = DB::table('gpff_product')                
                           ->where('product_id',$Products->product_id)
                           ->first();
        // print_r($exit);
        // print_r($exit->product_stock_id);
        // print_r($prstid);
        // exit();

        $war = DB::table('gpff_warehouse')
                ->where('warehouse_id', $Products->warehouse_id)
                ->First();

        // if(count($exit) == 0 && count($prstid) == 0){
        if($exit == '' && $prstid == ''){

                $productvalue = array(
                'category_id'           => $Products->category_id,
                'category_name'         => $Products->category_name,
                'product_id'            => $Products->product_id,
                'product_genericname'   => $Products->product_genericname,
                'product_type'          => $Products->product_type,
                'warehouse_id'          => $Products->warehouse_id ,
                'stockist_id'           => $Products->stockist_id,
                'warehouse_name'        => $war->warehouse_name ,
                'region_id'             => $war->region_id,
                'branch_id'             => $war->branch_id,
                'created_at'            => date('Y-m-d H:i:s') ,
                'updated_at'            => date('Y-m-d H:i:s')
                );

                $prod_sto_id = DB::table('gpff_product_stock')
                ->insertGetId($productvalue);

                $batch_stock_list_value = array(
                'product_stock_id'      => $prod_sto_id ,
                'category_id'           => $Products->category_id,
                'warehouse_id'          => $Products->warehouse_id ,
                'product_id'            => $Products->product_id,
                'batch_id'              => $Products->batch_no,
                'stockist_id'           => $Products->stockist_id,
                'product_genericname'   => $Products->product_genericname,
                'product_netprice'      => $product_details->product_netprice,
                'product_grossprice'    => $product_details->product_grossprice,
                'exp_date'              => $Products->exp_date,
                'product_tot_qty'       => $Products->product_qty,
                'product_blc_qty'       => $Products->product_qty,
                'created_at'            => date('Y-m-d H:i:s') ,
                'updated_at'            => date('Y-m-d H:i:s')
                );

                DB::table('gpff_product_batch_stock')
                ->insert($batch_stock_list_value);
            // }else if(count($exit) > 0 && count($prstid) == 0){
            }else if($exit != '' && $prstid == ''){

                // $prstid = DB::table('gpff_product_stock')
                // ->where('category_id', $Products->category_id)
                // ->where('product_id', $Products->product_id)
                // ->where('batch_id', $Products->batch_no)
                // ->where('warehouse_id', $Products->warehouse_id)
                // ->First();

                $batch_stock_list_value = array(
                'product_stock_id'      => $exit->product_stock_id,
                'category_id'           => $Products->category_id,
                'warehouse_id'          => $Products->warehouse_id ,
                'product_id'            => $Products->product_id,
                'batch_id'              => $Products->batch_no,
                'stockist_id'           => $Products->stockist_id,
                'product_genericname'   => $Products->product_genericname,
                'product_netprice'      => $product_details->product_netprice,
                'product_grossprice'    => $product_details->product_grossprice,
                'exp_date'              => $Products->exp_date,
                'product_tot_qty'       => $Products->product_qty,
                'product_blc_qty'       => $Products->product_qty,
                'created_at'            => date('Y-m-d H:i:s') ,
                'updated_at'            => date('Y-m-d H:i:s')
                );

                DB::table('gpff_product_batch_stock')
                ->insert($batch_stock_list_value);
            }else{

                $blc_qty  =  DB::table('gpff_product_batch_stock')
                            ->where('category_id', $Products->category_id)
                            ->where('product_id', $Products->product_id)
                            ->where('batch_id', $Products->batch_no)
                            ->where('warehouse_id', $Products->warehouse_id)
                            ->sum('product_blc_qty');

                $tot_qty  =  DB::table('gpff_product_batch_stock')
                            ->where('category_id', $Products->category_id)
                            ->where('product_id', $Products->product_id)
                            ->where('batch_id', $Products->batch_no)
                            ->where('warehouse_id', $Products->warehouse_id)
                            ->sum('product_tot_qty');

                $qty = $Products->product_qty;

                $blc    = $blc_qty + $qty;
                $total  = $tot_qty + $qty;

                $value = array(
                    'product_blc_qty'       => $blc ,
                    'product_tot_qty'       => $total,
                    'product_status'        => 1,
                    'product_netprice'      => $product_details->product_netprice,
                    'product_grossprice'    => $product_details->product_grossprice,
                    'updated_at'            => date('Y-m-d H:i:s')
                );

                DB::table('gpff_product_batch_stock')
                ->where('category_id', $Products->category_id)
                ->where('product_id', $Products->product_id)
                ->where('batch_id', $Products->batch_no)
                ->where('warehouse_id', $Products->warehouse_id)
                ->update($value);
            }

        $accept_li_value = array(
            'status'   => 1,
            'updated_at'        => date('Y-m-d H:i:s')
        );

        DB::table('gpff_product_request_accept_list')
        ->where('product_request_accept_list_id',$Products->product_request_accept_list_id)
        ->update($accept_li_value);

        return 1;
    }

    // Fetch Stock Request Details
    public function getStockCompare($Products) 
    {   
        if($Products->type == 1){
            
            $now = Carbon::now();

            if(count($Products->product_id) > 0){
            return  DB::table('gpff_product_batch_stock as gpbs')
                ->leftjoin('gpff_category as gc','gpbs.category_id','gc.category_id')
                ->leftjoin('gpff_warehouse as gw','gpbs.warehouse_id','gw.warehouse_id')
                ->leftjoin('gpff_users as gus','gpbs.stockist_id','gus.user_id')
                ->where('gpbs.product_status','1')
                ->whereIn('gpbs.warehouse_id',$Products->warehouse_id)
                ->whereIn('gpbs.product_id',$Products->product_id)
                ->where('gpbs.exp_date', '>=', $now)
                ->orderBy('gpbs.exp_date','ASC')
                ->distinct()
                //->groupBy('product_id')
                ->get(['gpbs.product_stock_id','gpbs.category_id','gc.category_name','gpbs.warehouse_id','gw.warehouse_name','gpbs.stockist_id','gus.firstname','gpbs.product_id','gpbs.batch_id','gpbs.product_genericname','gpbs.product_blc_qty','gpbs.product_netprice','gpbs.product_grossprice','gpbs.exp_date','gpbs.product_status']);
            }else{
                return  DB::table('gpff_product_batch_stock as gpbs')
                ->leftjoin('gpff_category as gc','gpbs.category_id','gc.category_id')
                ->leftjoin('gpff_warehouse as gw','gpbs.warehouse_id','gw.warehouse_id')
                ->leftjoin('gpff_users as gus','gpbs.stockist_id','gus.user_id')
                ->where('gpbs.product_status','1')
                ->whereIn('gpbs.warehouse_id',$Products->warehouse_id)
                ->where('gpbs.exp_date', '>=', $now)
                ->orderBy('gpbs.exp_date','ASC')
                ->distinct()
                //->groupBy('product_id')
                ->get(['gpbs.product_stock_id','gpbs.category_id','gc.category_name','gpbs.warehouse_id','gw.warehouse_name','gpbs.stockist_id','gus.firstname','gpbs.product_id','gpbs.batch_id','gpbs.product_genericname','gpbs.product_blc_qty','gpbs.product_netprice','gpbs.product_grossprice','gpbs.exp_date','gpbs.product_status']);
            }
        }if($Products->type == 2){
            $warehouse_id = implode("," ,$Products->warehouse_id);
            $now = Carbon::now();
            // print_r($now);
            if(count($Products->product_id) > 0){
                $product_id = implode(",",$Products->product_id);
                $data = DB::table('gpff_product_batch_stock as pbs')
                        ->leftjoin('gpff_warehouse as gw','pbs.warehouse_id','gw.warehouse_id')
                        ->leftjoin('gpff_category as gc','pbs.category_id','gc.category_id')
                        ->whereIn('pbs.product_id', $Products->product_id)
                        ->whereIn('pbs.warehouse_id', $Products->warehouse_id)
                        ->where('pbs.product_status','1')
                        ->where('pbs.exp_date', '>=', $now)
                        // ->sum('pbs.product_tot_qty');
                        ->select('pbs.product_id','pbs.product_genericname','gc.category_name',DB::raw('SUM(pbs.product_blc_qty) as qty'))
                        ->groupBy('pbs.product_id','pbs.product_genericname','gc.category_name')
                        // ->groupBy('pbs.warehouse_id')
                        ->get();
                // print_r($data);
                // exit();

                // return DB::select('select sum(pbs.product_tot_qty) as qty, pbs.product_id, max(ps.product_genericname) as product_genericname,max(ps.category_name) as category_name,max(ps.warehouse_name) as warehouse_name FROM gpff_product_stock as ps, gpff_product_batch_stock as pbs where ps.product_id in ('.$product_id.') and ps.warehouse_id in ('.$warehouse_id.')and ps.product_id = pbs.product_id and ps.warehouse_id = pbs.warehouse_id GROUP BY pbs.product_id');

                // return DB::select('select sum(pbs.product_tot_qty) as qty, pbs.product_id,pbs.exp_date, max(ps.product_genericname) as product_genericname,max(ps.category_name) as category_name,max(ps.warehouse_name) as warehouse_name FROM gpff_product_stock as ps, gpff_product_batch_stock as pbs where ps.product_id in ('.$product_id.') and ps.warehouse_id in ('.$warehouse_id.') and ps.product_id = pbs.product_id and ps.warehouse_id = pbs.warehouse_id and where pbs.exp_date >= "$now" GROUP BY pbs.product_id,pbs.exp_date');
                return $data;

            }else{

                $data = DB::table('gpff_product_batch_stock as pbs')
                        ->leftjoin('gpff_warehouse as gw','pbs.warehouse_id','gw.warehouse_id')
                        ->leftjoin('gpff_category as gc','pbs.category_id','gc.category_id')
                        ->whereIn('pbs.warehouse_id', $Products->warehouse_id)
                        ->where('pbs.product_status','1')
                        ->where('pbs.exp_date', '>=', $now)
                        // ->sum('pbs.product_tot_qty');
                        ->select('pbs.product_id','pbs.product_genericname','gc.category_name',DB::raw('SUM(pbs.product_blc_qty) as qty'))
                        ->groupBy('pbs.product_id','pbs.product_genericname','gc.category_name')
                        // ->groupBy('pbs.warehouse_id')
                        ->get();
                // return DB::select('select sum(pbs.product_tot_qty) as qty, pbs.product_id,pbs.exp_date, max(ps.product_genericname) as product_genericname,max(ps.category_name) as category_name,max(ps.warehouse_name) as warehouse_name FROM gpff_product_stock as ps, gpff_product_batch_stock as pbs where ps.warehouse_id in ('.$warehouse_id.') and ps.product_id = pbs.product_id and ps.warehouse_id = pbs.warehouse_id GROUP BY pbs.product_id,pbs.exp_date');
                // $data = DB::table('gpff_product_stock as ps')
                //         ->join('gpff_product_batch_stock as pbs', 'ps.product_stock_id', 'pbs.product_stock_id')
                //         // ->whereIn('ps.product_id', $product_id)
                //         ->where('pbs.warehouse_id', $warehouse_id)
                //         ->sum('pbs.product_tot_qty')
                //         ->select('ps.product_genericname','ps.category_name','ps.warehouse_name','pbs.product_tot_qty')
                //         // ->get();
                //         ->groupBy('ps.product_genericname','ps.category_name','ps.warehouse_name','pbs.product_tot_qty');
                return $data;
            }
        }else{
            if($Products->date == ""){
                $now = Carbon::now();
            }else{
                $now = $Products->date;
            }
            if(count($Products->product_id) > 0){
            return  DB::table('gpff_product_batch_stock as gpbs')
                ->leftjoin('gpff_category as gc','gpbs.category_id','gc.category_id')
                ->leftjoin('gpff_warehouse as gw','gpbs.warehouse_id','gw.warehouse_id')
                ->leftjoin('gpff_users as gus','gpbs.stockist_id','gus.user_id')
                ->whereIn('gpbs.warehouse_id',$Products->warehouse_id)
                ->whereIn('gpbs.product_id',$Products->product_id)
                ->where('gpbs.exp_date', '<=', $now)
                ->orderBy('gpbs.exp_date','DESC')
                //->groupBy('product_id')
                ->get(['gpbs.product_stock_id','gpbs.category_id','gc.category_name','gpbs.warehouse_id','gw.warehouse_name','gpbs.stockist_id','gus.firstname','gpbs.product_id','gpbs.batch_id','gpbs.product_genericname','gpbs.product_blc_qty','gpbs.product_netprice','gpbs.product_grossprice','gpbs.exp_date','gpbs.product_status']);
            }else{
                return  DB::table('gpff_product_batch_stock as gpbs')
                ->leftjoin('gpff_category as gc','gpbs.category_id','gc.category_id')
                ->leftjoin('gpff_warehouse as gw','gpbs.warehouse_id','gw.warehouse_id')
                ->leftjoin('gpff_users as gus','gpbs.stockist_id','gus.user_id')
                ->whereIn('gpbs.warehouse_id',$Products->warehouse_id)
                ->where('gpbs.exp_date', '<=', $now)
                ->orderBy('gpbs.exp_date','ASC')
                //->groupBy('product_id')
                ->get(['gpbs.product_stock_id','gpbs.category_id','gc.category_name','gpbs.warehouse_id','gw.warehouse_name','gpbs.stockist_id','gus.firstname','gpbs.product_id','gpbs.batch_id','gpbs.product_genericname','gpbs.product_blc_qty','gpbs.product_netprice','gpbs.product_grossprice','gpbs.exp_date','gpbs.product_status']);
            }
        }
    }

    public function numbertostring($number)
    {   
    //     $ones = array(
    //         0 =>"ZERO",
    //         1 => "ONE",
    //         2 => "TWO",
    //         3 => "THREE",
    //         4 => "FOUR",
    //         5 => "FIVE",
    //         6 => "SIX",
    //         7 => "SEVEN",
    //         8 => "EIGHT",
    //         9 => "NINE",
    //         10 => "TEN",
    //         11 => "ELEVEN",
    //         12 => "TWELVE",
    //         13 => "THIRTEEN",
    //         14 => "FOURTEEN",
    //         15 => "FIFTEEN",
    //         16 => "SIXTEEN",
    //         17 => "SEVENTEEN",
    //         18 => "EIGHTEEN",
    //         19 => "NINETEEN",
    //         "014" => "FOURTEEN"
    //         );
    //         $tens = array( 
    //         0 => "ZERO",
    //         1 => "TEN",
    //         2 => "TWENTY",
    //         3 => "THIRTY", 
    //         4 => "FORTY", 
    //         5 => "FIFTY", 
    //         6 => "SIXTY", 
    //         7 => "SEVENTY", 
    //         8 => "EIGHTY", 
    //         9 => "NINETY" 
    //         ); 
    //         $hundreds = array( 
    //         "HUNDRED", 
    //         "THOUSAND", 
    //         "MILLION", 
    //         "BILLION", 
    //         "TRILLION", 
    //         "QUARDRILLION" 
    //         ); /*limit t quadrillion */
    //         $num = number_format($num,2,".",","); 
    //         $num_arr = explode(".",$num); 
    //         $wholenum = $num_arr[0]; 
    //         $decnum = $num_arr[1]; 
    //         $whole_arr = array_reverse(explode(",",$wholenum)); 
    //         krsort($whole_arr,1); 
    //         $rettxt = ""; 
    //         foreach($whole_arr as $key => $i){
                
    //         while(substr($i,0,1)=="0")
    //                 $i=substr($i,1,5);
    //         if($i < 20){ 
    //         /* echo "getting:".$i; */
    //         $rettxt .= $ones[$i]; 
    //         }elseif($i < 100){ 
    //         if(substr($i,0,1)!="0")  $rettxt .= $tens[substr($i,0,1)]; 
    //         if(substr($i,1,1)!="0") $rettxt .= " ".$ones[substr($i,1,1)]; 
    //         }else{ 
    //         if(substr($i,0,1)!="0") $rettxt .= $ones[substr($i,0,1)]." ".$hundreds[0]; 
    //         if(substr($i,1,1)!="0")$rettxt .= " ".$tens[substr($i,1,1)]; 
    //         if(substr($i,2,1)!="0")$rettxt .= " ".$ones[substr($i,2,1)]; 
    //         } 
    //         if($key > 0){ 
    //         $rettxt .= " ".$hundreds[$key]." "; 
    //         }
    //         } 
    //         if($decnum > 0){
    //         $rettxt .= " and ";
    //         if($decnum < 20){
    //         $rettxt .= $ones[$decnum];
    //         }elseif($decnum < 100){
    //         $rettxt .= $tens[substr($decnum,0,1)];
    //         $rettxt .= " ".$ones[substr($decnum,1,1)];
    //         }
    //         }
    //         return $rettxt;
    // }    
        $decimal = round($number - ($no = floor($number)), 2) * 100;
        $decimal_part = $decimal;
        $hundred = null;
        $hundreds = null;
        $digits_length = strlen($no);
        $decimal_length = strlen($decimal);
        $i = 0;
        $str = array();
        $str2 = array();
        $words = array(0 => '', 1 => 'one', 2 => 'two',
            3 => 'three', 4 => 'four', 5 => 'five', 6 => 'six',
            7 => 'seven', 8 => 'eight', 9 => 'nine',
            10 => 'ten', 11 => 'eleven', 12 => 'twelve',
            13 => 'thirteen', 14 => 'fourteen', 15 => 'fifteen',
            16 => 'sixteen', 17 => 'seventeen', 18 => 'eighteen',
            19 => 'nineteen', 20 => 'twenty', 30 => 'thirty',
            40 => 'forty', 50 => 'fifty', 60 => 'sixty',
            70 => 'seventy', 80 => 'eighty', 90 => 'ninety');
        $digits = array('', 'hundred','thousand','lakh', 'crore');

        while( $i < $digits_length ) {
            $divider = ($i == 2) ? 10 : 100;
            $number = floor($no % $divider);
            $no = floor($no / $divider);
            $i += $divider == 10 ? 1 : 2;
            if ($number) {
                $plural = (($counter = count($str)) && $number > 9) ? 's' : null;
                $hundred = ($counter == 1 && $str[0]) ? ' and ' : null;
                $str [] = ($number < 21) ? $words[$number].' '. $digits[$counter]. $plural.' '.$hundred:$words[floor($number / 10) * 10].' '.$words[$number % 10]. ' '.$digits[$counter].$plural.' '.$hundred;
            } else $str[] = null;
        }

        $d = 0;
        while( $d < $decimal_length ) {
            $divider = ($d == 2) ? 10 : 100;
            $decimal_number = floor($decimal % $divider);
            $decimal = floor($decimal / $divider);
            $d += $divider == 10 ? 1 : 2;
            if ($decimal_number) {
                $plurals = (($counter = count($str2)) && $decimal_number > 9) ? 's' : null;
                $hundreds = ($counter == 1 && $str2[0]) ? ' and ' : null;
                @$str2 [] = ($decimal_number < 21) ? $words[$decimal_number].' '. $digits[$decimal_number]. $plural.' '.$hundred:$words[floor($decimal_number / 10) * 10].' '.$words[$decimal_number % 10]. ' '.$digits[$counter].$plural.' '.$hundred;
            } else $str2[] = null;
        }

        $Rupees = implode('', array_reverse($str));
        $paise = implode('', array_reverse($str2));
        $paise = ($decimal_part > 0) ? $paise . ' Kyat' : '';
        return ($Rupees ? $Rupees . 'Kyat ' : '') . $paise;
    }

    public function getBranchBasedCategory($product) 
    {
        return DB::table('gpff_category')
                ->where('branch_id',$product->branch_id)
                ->orderBy('updated_at','DESC')
                ->get();
    }

    public function updateProductsBatchQty($Products)
    {
        $values = array(
                'product_tot_qty'   => $Products->new_tot_qty ,
                'product_blc_qty'   => $Products->new_avl_qty ,
                'updated_at' => date('Y-m-d H:i:s')
             );
        
        $product_batch_data = DB::table('gpff_product_batch_stock')
        ->where('product_batch_stock_id',$Products->product_batch_stock_id)
        ->first();

        $insert_values = array(
            'product_batch_stock_id'=>$Products->product_batch_stock_id,
            'batch_id'=>$product_batch_data->batch_id,
            'old_tot_qty'=>$Products->tot_qty,
            'old_avl_qty'=>$Products->avl_qty,
            'new_tot_qty'=>$Products->new_tot_qty,
            'new_avl_qty'=>$Products->new_avl_qty,
            'created_by' => $Products->user_id,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        );
        $update = DB::table('gpff_product_batch_stock')
        ->where('product_batch_stock_id', $Products->product_batch_stock_id)
        ->update($values);

        DB::table('product_batch_stock_qty_log')
        ->insert($insert_values);
        return $update;  
    }

    public function getStockBalanceSheet($Products) {
        $opening_balance = $this->getOpeningBalance($Products);
        $product_details = DB::table('gpff_product_batch_stock')
                        ->where('product_id',$Products->product_id)
                        ->where('batch_id',$Products->batch_id)
                        ->where('warehouse_id',$Products->warehouse_id)
                        ->First();
        // print_r($opening_balance);
        $customer_sales = DB::table('gpff_order_list_batchwise as gporlb')
                        ->join('gpff_order as gpor','gpor.order_id','gporlb.order_id')
                        ->whereBetween(DB::RAW('date(gporlb.created_at)'), [date($Products->start_date), date($Products->end_date)])
                        ->where('product_id',$Products->product_id)
                        ->where('batch_no',$Products->batch_id)
                        ->where('warehouse_id',$Products->warehouse_id)
                        ->groupBy('gporlb.created_at','gporlb.order_id')
                        ->get(['gporlb.order_id','gporlb.created_at',DB::raw('SUM(product_qty) as product_qty'),DB::raw('SUM(FOC) as FOC'),DB::raw('SUM(spl_FOC) as spl_FOC')]);
        $report_data=[];
        //Customer Sales
        foreach($customer_sales as $value){
            $report_data[] = array(
               "date"=> $value->created_at,
               "des"=>"Customer Sales (Bill No : ".$value->order_id." )",
               "type"=>"2",
               "qty"=>$value->product_qty
            );
        }
        //Customer Sales FOC
        foreach($customer_sales as $value){
            if($value->FOC != null && $value->FOC != "0"){
                $report_data[] = array(
                   "date"=> $value->created_at,
                   "des"=>"Give FOC to customers",
                   "type"=>"2",
                   "qty"=>$value->FOC
                );
            }
        }
        //Customer Sales SPL FOC
        foreach($customer_sales as $value){
            if($value->spl_FOC != null && $value->spl_FOC != "0"){
                $report_data[] = array(
                   "date"=> $value->created_at,
                   "des"=>"Give SPL FOC to customers",
                   "type"=>"2",
                   "qty"=>$value->spl_FOC
                );
            }
        }
        $vendor_sales = DB::table('gpff_vendor_order_list_batchwise as gpvorlb')
                        ->join('gpff_vendor_order as gpvor','gpvor.vendor_order_id','gpvorlb.vendor_order_id')
                        ->whereBetween(DB::RAW('date(gpvorlb.created_at)'), [date($Products->start_date), date($Products->end_date)])
                        ->where('product_id',$Products->product_id)
                        ->where('batch_no',$Products->batch_id)
                        ->where('warehouse_id',$Products->warehouse_id)
                        ->groupBy('gpvorlb.created_at','gpvorlb.vendor_order_id')
                        ->get(['gpvorlb.vendor_order_id','gpvorlb.created_at',DB::raw('SUM(product_qty) as product_qty'),DB::raw('SUM(FOC) as FOC'),DB::raw('SUM(spl_FOC) as spl_FOC')]);
        //Vendor Sales
        foreach($vendor_sales as $value){
            $report_data[] = array(
               "date"=> $value->created_at,
               "des"=>"Vendor Sales (Bill No : ".$value->vendor_order_id." )",
               "type"=>"2",
               "qty"=>$value->product_qty
            );
        }
        //Vendor Sales FOC
        foreach($vendor_sales as $value){
            if($value->FOC != null && $value->FOC != "0"){
                $report_data[] = array(
                   "date"=> $value->created_at,
                   "des"=>"Give FOC to vendors",
                   "type"=>"2",
                   "qty"=>$value->FOC
                );
            }
        }
        //Vendor Sales SPL FOC
        foreach($vendor_sales as $value){
            if($value->spl_FOC != null && $value->spl_FOC != "0"){
                $report_data[] = array(
                                   "date"=> $value->created_at,
                                   "des"=>"Give SPL FOC to vendors",
                                   "type"=>"2",
                                   "qty"=>$value->spl_FOC
                                );
            }
        }
        $warehouse_transfer_out = DB::table('gpff_product_request_accept_list as gprl')
                        ->join('gpff_product_request as gpr','gpr.product_request_id','gprl.product_request_id')
                        ->join('gpff_warehouse as gw','gw.warehouse_id','gpr.sub_ware_id')
                        ->whereBetween(DB::RAW('date(gprl.created_at)'), [date($Products->start_date), date($Products->end_date)])
                        ->where('product_id',$Products->product_id)
                        ->where('batch_no',$Products->batch_id)
                        ->where('gpr.warehouse_id',$Products->warehouse_id)
                        ->groupBy('gprl.created_at','gpr.sub_ware_id')
                        ->get(['gpr.sub_ware_id','gw.warehouse_name','gprl.created_at',DB::raw('SUM(product_qty) as product_qty')]);
        // print_r(json_encode($warehouse_transfer_out));
        // exit;
        foreach($warehouse_transfer_out as $value){
            $report_data[] = array(
                               "date"=> $value->created_at,
                               "des"=>"Stocks transfer to ".$value->warehouse_name,
                               "type"=>"2",
                               "qty"=>$value->product_qty
                            );            
        }
        $samples = DB::table('gppf_fo_received_sample')
                        ->whereBetween(DB::RAW('date(created_at)'), [date($Products->start_date), date($Products->end_date)])
                        ->where('product_id',$Products->product_id)
                        ->where('batch_id',$Products->batch_id)
                        ->where('warehouse_id',$Products->warehouse_id)
                        ->groupBy('created_at')
                        ->get(['created_at',DB::raw('SUM(tot_qty) as product_qty')]);
        foreach($samples as $value){
            $report_data[] = array(
                               "date"=> $value->created_at,
                               "des"=>"Give Samples",
                               "type"=>"2",
                               "qty"=>$value->product_qty
                            );            
        }
        //Cargo Stocks Add
        $cargo_stacks = DB::table('gpff_product_recived_history_list as ghl')
                        ->join('gpff_product_recived_history as gh','gh.product_recived_history_id','ghl.product_recived_history_id')
                        ->whereBetween(DB::RAW('date(ghl.created_at)'), [date($Products->start_date), date($Products->end_date)])
                        ->where('product_id',$Products->product_id)
                        ->where('batch_id',$Products->batch_id)
                        ->where('warehouse_id',$Products->warehouse_id)
                        ->groupBy('ghl.created_at')
                        ->get(['ghl.created_at',DB::raw('SUM(product_qty) as product_qty')]);

        foreach($cargo_stacks as $value){
            $report_data[] = array(
                               "date"=> $value->created_at,
                               "des"=>"Stocks added by Cargo",
                               "type"=>"1",
                               "qty"=>$value->product_qty
                            );            
        }
        $po_stocks = DB::table('gpff_purchase_invoice_products as gpip')
                        ->join('gpff_purchase_invoice','gpff_purchase_invoice.invoice_id','gpip.purchase_invoice_id')
                        ->whereBetween(DB::RAW('date(gpip.created_at)'), [date($Products->start_date), date($Products->end_date)])
                        ->where('product_id',$Products->product_id)
                        ->where('batch_no',$Products->batch_id)
                        ->where('gpff_purchase_invoice.warehouse_id',$Products->warehouse_id)
                        ->groupBy('gpip.created_at')
                        ->get(['gpip.created_at',DB::raw('SUM(no_of_items) as product_qty')]);

        foreach($po_stocks as $value){
            $report_data[] = array(
                               "date"=> $value->created_at,
                               "des"=>"Stocks added by PO",
                               "type"=>"1",
                               "qty"=>$value->product_qty
                            );            
        }
        $warehouse_transfer_in = DB::table('gpff_product_request_accept_list as gprl')
                        ->join('gpff_product_request as gpr','gpr.product_request_id','gprl.product_request_id')
                        ->join('gpff_warehouse as gw','gw.warehouse_id','gpr.warehouse_id')
                        ->whereBetween(DB::RAW('date(gprl.created_at)'), [date($Products->start_date), date($Products->end_date)])
                        ->where('product_id',$Products->product_id)
                        ->where('batch_no',$Products->batch_id)
                        ->where('sub_ware_id',$Products->warehouse_id)
                        ->where('gprl.status',1)
                        ->groupBy('gprl.created_at','gpr.warehouse_id')
                        ->get(['gpr.warehouse_id','gw.warehouse_name','gprl.created_at',DB::raw('SUM(product_qty) as product_qty')]);
        foreach($warehouse_transfer_in as $value){
            $report_data[] = array(
                               "date"=> $value->created_at,
                               "des"=>"Stocks received from ".$value->warehouse_name,
                               "type"=>"1",
                               "qty"=>$value->product_qty
                            );            
        }
        $sales_return = DB::table('gpff_sale_return_batchwise_list as gpsrlb')
                        ->join('gpff_sale_return as gpsr','gpsr.sale_return_id','gpsrlb.sale_return_id')
                        ->whereBetween(DB::RAW('date(gpsrlb.created_at)'), [date($Products->start_date), date($Products->end_date)])
                        ->where('product_id',$Products->product_id)
                        ->where('batch_no',$Products->batch_id)
                        ->where('warehouse_id',$Products->warehouse_id)
                        ->whereNotNull('return_qty')
                        ->groupBy('gpsrlb.created_at','gpsrlb.order_id','gpsr.or_return_type')
                        ->get(['gpsrlb.order_id','gpsr.or_return_type','gpsrlb.created_at',DB::raw('SUM(return_qty) as product_qty'),DB::raw('SUM(return_FOC) as FOC')]);
        foreach($sales_return as $value){
            if($value->or_return_type == 1){
                $des = "Customer sales return (Bill No :".$value->order_id." )";
            }else if($value->or_return_type == 2){
                $des = "Vendor sales return (Bill No :".$value->order_id." )";
            }
            $report_data[] = array(
                               "date"=> $value->created_at,
                               "des"=>$des,
                               "type"=>"1",
                               "qty"=>$value->product_qty
                            );            
        }
        foreach($sales_return as $value){
            if($value->FOC != null && $value->FOC != "0"){
                if($value->or_return_type == 1){
                    $des = "Customer sales return FOC (Bill No :".$value->order_id." )";
                }else if($value->or_return_type == 2){
                    $des = "Vendor sales return FOC(Bill No :".$value->order_id." )";
                }
                $report_data[] = array(
                                   "date"=> $value->created_at,
                                   "des"=>$des,
                                   "type"=>"1",
                                   "qty"=>$value->FOC
                                );            
            }
        }
        // print_r(json_encode($report_data));
        $data =  usort($report_data, function($a, $b) {
                   return (strtotime($a['date']) < strtotime($b['date'])? -1 : 1);
                 });
        //print_r(json_encode($report_data));
        $balance_sheet = [];
        foreach($report_data as $data){
            if($data['type'] == "1"){
                $opening_balance = $opening_balance + (int)$data['qty'];
            }else if($data['type'] == "2"){
                $opening_balance = $opening_balance - (int)$data['qty'];
            }
            $balance_sheet[] = array(
                               "date"=> $data['date'],
                               "product_name"=>$product_details->product_genericname,
                               "batch_id"=>$product_details->batch_id,
                               "des"=>$data['des'],
                               "type"=>$data['type'],
                               "qty"=>$data['qty'],
                               "balance"=>$opening_balance
                            );
        }
        return $balance_sheet;
        // print("ENTER");
        // print_r(json_encode($balance_sheet));
        // print_r(json_encode($customer_sales));
        // print_r(json_encode($vendor_sales));
        // print_r(json_encode($warehouse_transfer_out));
        // print_r(json_encode($samples));
        //exit;
    }
    public function getOpeningBalance($Products){
        $cargo_stacks = DB::table('gpff_product_recived_history_list as ghl')
                        ->join('gpff_product_recived_history as gh','gh.product_recived_history_id','ghl.product_recived_history_id')
                        ->whereDate('ghl.created_at' ,'<' ,$Products->start_date)
                        ->where('product_id',$Products->product_id)
                        ->where('batch_id',$Products->batch_id)
                        ->where('warehouse_id',$Products->warehouse_id)
                        ->get([DB::raw('SUM(product_qty) as product_qty')]);
        $warehouse_transfer = DB::table('gpff_product_request_accept_list as gprl')
                        ->join('gpff_product_request as gpr','gpr.product_request_id','gprl.product_request_id')
                        ->where('gprl.created_at' ,'<' ,$Products->start_date)
                        ->where('product_id',$Products->product_id)
                        ->where('batch_no',$Products->batch_id)
                        ->where('sub_ware_id',$Products->warehouse_id)
                        ->get([DB::raw('SUM(product_qty) as product_qty')]);
        $warehouse_transfer_in = DB::table('gpff_product_request_accept_list as gprl')
                        ->join('gpff_product_request as gpr','gpr.product_request_id','gprl.product_request_id')
                        ->where('gprl.created_at' ,'<' ,$Products->start_date)
                        ->where('product_id',$Products->product_id)
                        ->where('batch_no',$Products->batch_id)
                        ->where('sub_ware_id',$Products->warehouse_id)
                        ->where('gprl.status',1)
                        ->get([DB::raw('SUM(product_qty) as product_qty')]);
        $sales_return = DB::table('gpff_sale_return_batchwise_list as gpsrlb')
                        ->join('gpff_sale_return as gpsr','gpsr.sale_return_id','gpsrlb.sale_return_id')
                        ->where('gpsrlb.created_at' ,'<' ,$Products->start_date)
                        ->where('product_id',$Products->product_id)
                        ->where('batch_no',$Products->batch_id)
                        ->where('warehouse_id',$Products->warehouse_id)
                        ->whereNotNull('return_qty')
                        ->get([DB::raw('SUM(return_qty) as product_qty'),DB::raw('SUM(return_FOC) as FOC')]);
        $customer_sales = DB::table('gpff_order_list_batchwise as gporlb')
                        ->join('gpff_order as gpor','gpor.order_id','gporlb.order_id')
                        ->where('gporlb.created_at' ,'<' ,$Products->start_date)
                        ->where('product_id',$Products->product_id)
                        ->where('batch_no',$Products->batch_id)
                        ->where('warehouse_id',$Products->warehouse_id)
                        ->get([DB::raw('SUM(product_qty) as product_qty'),DB::raw('SUM(FOC) as FOC'),DB::raw('SUM(spl_FOC) as spl_FOC')]);
        $vendor_sales = DB::table('gpff_vendor_order_list_batchwise as gpvorlb')
                        ->join('gpff_vendor_order as gpvor','gpvor.vendor_order_id','gpvorlb.vendor_order_id')
                        ->where('gpvorlb.created_at' ,'<' ,$Products->start_date)
                        ->where('product_id',$Products->product_id)
                        ->where('batch_no',$Products->batch_id)
                        ->where('warehouse_id',$Products->warehouse_id)
                        ->get([DB::raw('SUM(product_qty) as product_qty'),DB::raw('SUM(FOC) as FOC'),DB::raw('SUM(spl_FOC) as spl_FOC')]);
        $warehouse_transfer_out = DB::table('gpff_product_request_accept_list as gprl')
                        ->join('gpff_product_request as gpr','gpr.product_request_id','gprl.product_request_id')
                        ->where('gprl.created_at' ,'<' ,$Products->start_date)
                        ->where('product_id',$Products->product_id)
                        ->where('batch_no',$Products->batch_id)
                        ->where('warehouse_id',$Products->warehouse_id)
                        ->get([DB::raw('SUM(product_qty) as product_qty')]);
        $samples = DB::table('gppf_fo_received_sample')
                        ->where('created_at' ,'<' ,$Products->start_date)
                        ->where('product_id',$Products->product_id)
                        ->where('batch_id',$Products->batch_id)
                        ->where('warehouse_id',$Products->warehouse_id)
                        ->get([DB::raw('SUM(tot_qty) as product_qty')]);
        $openig_balance = 0;
        $tot_stocks = 0;
        if($cargo_stacks[0]->product_qty != null){
            $openig_balance = $openig_balance + (int)$cargo_stacks[0]->product_qty;
            $tot_stocks = $tot_stocks + (int)$cargo_stacks[0]->product_qty;
        }
        if($warehouse_transfer_in[0]->product_qty != null){
            $openig_balance = $openig_balance + (int)$warehouse_transfer_in[0]->product_qty;
            $tot_stocks = $tot_stocks + (int)$warehouse_transfer_in[0]->product_qty;
        }
        if($sales_return[0]->product_qty != null){
            $openig_balance = $openig_balance + (int)$sales_return[0]->product_qty;
            if($sales_return[0]->FOC != null){
                $openig_balance = $openig_balance + (int)$sales_return[0]->FOC;    
            }
            
        }
        if($customer_sales[0]->product_qty != null){
            $openig_balance = $openig_balance - (int)$customer_sales[0]->product_qty;
        }
        if($customer_sales[0]->FOC != null){
            $openig_balance = $openig_balance - (int)$customer_sales[0]->FOC;
        }
        if($customer_sales[0]->spl_FOC != null){
            $openig_balance = $openig_balance - (int)$customer_sales[0]->spl_FOC;
        }
        if($vendor_sales[0]->product_qty != null){
            $openig_balance = $openig_balance - (int)$vendor_sales[0]->product_qty;
        }
        if($vendor_sales[0]->FOC != null){
            $openig_balance = $openig_balance - (int)$vendor_sales[0]->FOC;
        }
        if($vendor_sales[0]->spl_FOC != null){
            $openig_balance = $openig_balance - (int)$vendor_sales[0]->spl_FOC;
        }
        if($warehouse_transfer_out[0]->product_qty != null){
            $openig_balance = $openig_balance - (int)$warehouse_transfer_out[0]->product_qty;
        }
        if($samples[0]->product_qty != null){
            $openig_balance = $openig_balance - (int)$samples[0]->product_qty;
        }
        $product_batch_stock = DB::table('gpff_product_batch_stock')
                             ->where('product_id',$Products->product_id)
                             ->where('batch_id',$Products->batch_id)
                             ->where('warehouse_id',$Products->warehouse_id)
                             ->First();
        // $grand_tot_stocs = (int)$product_batch_stock->product_tot_qty - $tot_stocks;
        // $openig_balance = $openig_balance + $grand_tot_stocs;
        // print_r($openig_balance);
        // print_r(json_encode($cargo_stacks));
        // print_r(json_encode($warehouse_transfer_in));
        // print_r(json_encode($sales_return));
        // print_r(json_encode($customer_sales));
        // print_r(json_encode($vendor_sales));
        // print_r(json_encode($warehouse_transfer_out));
        // print_r(json_encode($samples));
        // exit;
        return $openig_balance;
    }
    public function uploadGoodsRecivedStock($Products) 
    {   
        $extension = $Products->productall->getClientOriginalExtension();
        if($extension == "xlsx" || $extension == "xls" || $extension == "csv") 
        {
            $path = $Products->productpath;
            $import = new UsersImport;
            Excel::import($import, $Products->productall);
            $data = $import->data;
            if(!empty($data) && count($data))
            {
                $values = array(
                    'warehouse_id'      => $Products->warehouse_id ,
                    'stockist_id'       => $Products->stockist_id,
                    'supplier_name'     => $Products->supplier_name ,
                    'invoice_date'      => $Products->invoice_date ,
                    'payment_type'      => $Products->payment_type , 
                    'region_id'         => $Products->region_id,
                    'branch_id'         => $Products->branch_id,
                    'upload_type'       => '2',
                    'created_at'        => $Products->created_at, 
                    'updated_at'        => $Products->updated_at
                );
                $prod_re_his_id   =   DB::table('gpff_product_recived_history')
                            ->insertGetId($values);
                foreach ($data as $value) 
                {
                    if($value['category_id'] != "" && $value['category_name'] != "" && $value['product_id'] != "" && $value['product_name'] != "" && $value['product_type'] != "" && $value['warehouse_id'] != "" && $value['stockist_id'] != "" && $value['region_id'] != "" && $value['branch_id'] != "" && $value['batch_id'] != "" && $value['product_netprice'] != "" && $value['product_grossprice'] != "" && $value['exp_date'] != "" && $value['qty'] != "")
                    {
                        $list_value = array(
                            'product_recived_history_id' => $prod_re_his_id,
                            'category_id'           => $value['category_id'],
                            'category_name'         => $value['category_name'],
                            'product_id'            => $value['product_id'],
                            'product_genericname'   => $value['product_name'],
                            'product_type'          => $value['product_type'],
                            'batch_id'              => $value['batch_id'],
                            'exp_date'              => $value['exp_date'],
                            'UOM'                   => 'UOM',
                            'no_of_item'            => $value['qty'],
                            'product_qty'           => $value['qty'],
                            'product_netprice'      => $value['product_netprice'],
                            'product_grossprice'    => $value['product_grossprice'],
                            'created_at'        => $Products->created_at, 
                            'updated_at'        => $Products->updated_at
                        );

                        DB::table('gpff_product_recived_history_list')
                        ->insert($list_value);

                    }else{
                        return 4;
                    }
                }
                return 1;
            } else {
                return 2;   
            }
            
        }else{
            return 3;
        }
    }

    public function getBatchDetails($Products)
    {   
        return  DB::table('gpff_product_batch_stock')
                ->where('product_id', $Products->product_id)
                ->where('warehouse_id', $Products->warehouse_id)
                ->orderBy('exp_date','ASC')
                ->get(['batch_id']);
    }

    //Stockist Adjustment Add the Product Stock
    public function stockAdjustAddProduct($Products) 
    {   
    //Stroed In Product Recived History
        $values = array(
                'warehouse_id'      => $Products->warehouse_id ,
                'stockist_id'       => $Products->stockist_id,
                'supplier_name'     => $Products->supplier_name ,
                'invoice_no'        => $Products->invoice_no ,
                'invoice_date'      => $Products->invoice_date ,
                'grand_tot_amt'     => $Products->grand_tot_amt , 
                'payment_type'      => $Products->payment_type , 
                'region_id'         => $Products->region_id,
                'branch_id'         => $Products->branch_id,
                'created_at'        => $Products->created_at , 
                'updated_at'        => $Products->updated_at
            );

        $prod_re_his_id   =   DB::table('gpff_product_recived_history')
                            ->insertGetId($values);

        $pdf_name = "stock_".$Products->invoice_no.'.pdf';

        $values = array(
            "invoice_no"=> $pdf_name,
            "updated_at"=> date('Y-m-d H:i:s')
        );

        DB::table('gpff_product_recived_history')
        ->where('product_recived_history_id', $prod_re_his_id)
        ->update($values);
       
        foreach($Products->prduct_details as $prduct_lists)
        {
            $list_value = array(
                'product_recived_history_id' => $prod_re_his_id,
                'category_id'           => $prduct_lists['category_id'],
                'category_name'         => $prduct_lists['category_name'],
                'product_id'            => $prduct_lists['product_id'],
                'product_genericname'   => $prduct_lists['product_genericname'],
                'product_type'          => $prduct_lists['product_type'],
                'batch_id'              => $prduct_lists['batch_id'],
                'exp_date'              => $prduct_lists['exp_date'],
                'UOM'                   => $prduct_lists['UOM'],
                'no_of_item'            => $prduct_lists['no_of_item'],
                'product_qty'           => $prduct_lists['product_qty'],
                'product_netprice'      => $prduct_lists['product_netprice'],
                'product_grossprice'    => $prduct_lists['product_grossprice'],
                'product_discount'      => $prduct_lists['product_discount'],
                'product_tot_amt'       => $prduct_lists['product_tot_amt'],
                'created_at'        => $Products->created_at , 
                'updated_at'        => $Products->updated_at
            );

            DB::table('gpff_product_recived_history_list')
            ->insert($list_value);
        }

        $com_details = DB::table('gpff_branch')
                        ->where('branch_id',$Products->branch_id)
                        ->First();

        $cus_de = DB::table('gpff_users')
                        ->where('user_id',$Products->stockist_id)
                        ->First();

        $war = DB::table('gpff_warehouse')
                ->where('warehouse_id', $Products->warehouse_id)
                ->First();

        // print_r($war);
        if($Products->payment_type == 1){
            $payment_method = "Cash";
        }else{
            $payment_method = "Credit";
        }

        $invoice = $Products->invoice_no;

        $total = $Products->grand_tot_amt;

        $number = $this->numbertostring($total);

        view()->share('datas',$Products->prduct_details);
        view()->share('cus_de',$cus_de);
        view()->share('invoice',$invoice);
        view()->share('war',$war);
        view()->share('com_details',$com_details);
        view()->share('value',$number);
        view()->share('payment_method',$payment_method);
        view()->share('total',$total);
        view()->share('date',$Products->created_at);

        $pdf = \PDF::loadView('stock_updates')
        ->setPaper('a4', 'landscape');
        $pdf->save(public_path('stock_invoice/'.$pdf_name));
        $file_name = $pdf_name;
        $name = $file_name;
        $filePath = 'stock_invoice/'.$name; 
  
        Storage::disk('s3')->put($filePath, file_get_contents(public_path('stock_invoice')."/".$file_name));

        return 1;
    }
    //Purchase Order
    public function addPORequest($Products) 
    {   
        $warehouse_details = DB::table('gpff_warehouse')
                            ->where('warehouse_id',$Products->warehouse_id)
                            ->first();
        $values = array(
                'supplier_id'      => $Products->supplier_id ,
                'supplier_name'      => $Products->supplier_name ,
                'warehouse_id'      => $Products->warehouse_id ,
                'warehouse_name'      => $warehouse_details->warehouse_name ,
                'delivery_schedule'      => $Products->delivery_schedule ,
                'delivery_address'      => $Products->delivery_address ,
                'grand_total'      => $Products->grand_total ,
                'payment_terms'      => $Products->payment_terms ,
                'freight_terms'      => $Products->freight_terms ,
                'stockist_id'       => $Products->stockist_id,
                'region_id'         => $Products->region_id,
                'branch_id'         => $Products->branch_id,
                'created_at'        => date('Y-m-d H:i:s') , 
                'updated_at'        => date('Y-m-d H:i:s')
            );

        $po_id   =   DB::table('gpff_purchase_order')
                            ->insertGetId($values);
        $list_value = [];
        foreach($Products->product_details as $prduct_lists)
        {
            $list_value[] = array(
                'purchase_order_id' => $po_id,
                'category_id'           => $prduct_lists['category_id'],
                'category_name'         => $prduct_lists['category_name'],
                'product_id'            => $prduct_lists['product_id'],
                'product_name'   => $prduct_lists['product_name'],
                'no_of_items'            => $prduct_lists['no_of_items'],
                'balance_items'         => $prduct_lists['no_of_items'],
                'net_price'      => $prduct_lists['net_price'],
                'gross_price'    => $prduct_lists['gross_price'],
                'total_amount'       => $prduct_lists['total_amount'],
                'created_at'            => date('Y-m-d H:i:s') ,
                'updated_at'            => date('Y-m-d H:i:s')
            );

        }
        if(count($list_value) > 0){
            DB::table('gpff_purchase_order_products')
            ->insert($list_value);
        }
        return 1;
    }

    public function getAllPORequest($data) {
        $result = DB::table('gpff_purchase_order')
                  ->join('gpff_branch','gpff_branch.branch_id','gpff_purchase_order.branch_id')
                  ->join('gpff_region','gpff_region.region_id','gpff_purchase_order.region_id')
                ->orderBy('gpff_purchase_order.updated_at','DESC');
        if($data->branch_id != ''){
            $result = $result->whereIn('gpff_purchase_order.branch_id',$data->branch_id);
        }
        if($data->region_id != ''){
            $result = $result->whereIn('gpff_purchase_order.region_id',$data->region_id);
        }
        if($data->status != ''){
            $result = $result->whereIn('po_status',$data->status);
        }
        if($data->stockist_id != ''){
            $result = $result->whereIn('stockist_id',$data->stockist_id);
        }
        if($data->warehouse_id != ''){
            $result = $result->whereIn('warehouse_id',$data->warehouse_id);
        }
        return $result->get();
    }

    public function getPODetails($data) {
        $po_details = DB::table('gpff_purchase_order')
                      ->where('purchase_order_id',$data->purchase_order_id)
                      ->first();
        $supplier_details = DB::table('gpff_supplier')
                            ->where('supplier_id',$po_details->supplier_id)
                            ->first();
        $po_products = DB::table('gpff_purchase_order_products')
                        ->where('purchase_order_id',$po_details->purchase_order_id)
                        ->get();

        $result = array(
            'purchase_order_details' => $po_details,
            'supplier_details' => $supplier_details,
            'purchase_order_products' => $po_products
        );
        return $result;
    }

    public function getPOInvoiceDetails($data) {
        $po_details = DB::table('gpff_purchase_order')
                      ->where('purchase_order_id',$data->purchase_order_id)
                      ->first();
        $supplier_details = DB::table('gpff_supplier')
                            ->where('supplier_id',$po_details->supplier_id)
                            ->first();
        $po_products = DB::table('gpff_purchase_order_products')
                        ->where('purchase_order_id',$po_details->purchase_order_id)
                        ->where('status',0)
                        ->get();

        $result = array(
            'purchase_order_details' => $po_details,
            'supplier_details' => $supplier_details,
            'purchase_order_products' => $po_products
        );
        return $result;
    }

    public function acceptPORequest($data) {
        $updated_values = array(
            'po_status' => 1,
            'updated_at'        => date('Y-m-d H:i:s')
        );
        $result = DB::table('gpff_purchase_order')
                      ->where('purchase_order_id',$data->purchase_order_id)
                      ->update($updated_values);

        return $result;
    }

    public function generatePOInvoice($Products) 
    {   
        $values = array(
                'purchase_order_id' => $Products->purchase_order_id ,
                'receipt_date'      => $Products->receipt_date ,
                'supplier_id'      => $Products->supplier_id ,
                'supplier_name'      => $Products->supplier_name ,
                'warehouse_id'      => $Products->warehouse_id ,
                'warehouse_name'      => $Products->warehouse_name  ,
                'delivery_address'      => $Products->delivery_address ,
                'grand_total'       => $Products->grand_total ,
                'balance_amount'    => $Products->grand_total ,
                'payment_terms'     => $Products->payment_terms ,
                'freight_terms'     => $Products->freight_terms ,
                'region_id'         => $Products->region_id,
                'branch_id'         => $Products->branch_id,
                'created_by_id'     => $Products->created_by_id,
                'created_by_name'   => $Products->created_by_name,
                'created_at'        => date('Y-m-d H:i:s') , 
                'updated_at'        => date('Y-m-d H:i:s')
            );

        $invoice_id   =   DB::table('gpff_purchase_invoice')
                            ->insertGetId($values);
        $list_value = [];
        foreach($Products->product_details as $prduct_lists)
        {

            $product_update = $this->updatePO_Product($prduct_lists['purchase_order_product_id'],$prduct_lists['no_of_items']);
            $list_value[] = array(
                'purchase_invoice_id' => $invoice_id,
                'category_id'           => $prduct_lists['category_id'],
                'category_name'         => $prduct_lists['category_name'],
                'product_id'            => $prduct_lists['product_id'],
                'product_name'   => $prduct_lists['product_name'],
                'purchase_price'   => $prduct_lists['purchase_price'],
                'no_of_items'            => $prduct_lists['no_of_items'],
                'batch_no'            => $prduct_lists['batch_no'],
                'manufacturing_date'            => $prduct_lists['manufacturing_date'],
                'expiry_date'            => $prduct_lists['expiry_date'],
                'total_amount'       => $prduct_lists['total_amount'],
                'created_at'            => date('Y-m-d H:i:s') ,
                'updated_at'            => date('Y-m-d H:i:s')
            );
        }
        if(count($list_value) > 0){
            DB::table('gpff_purchase_invoice_products')
            ->insert($list_value);
        }

        $values1 = array(
                'purchase_invoice_id' => $invoice_id,
                'supplier_id'      => $Products->supplier_id ,
                'supplier_name'      => $Products->supplier_name ,
                'amount'      => $Products->grand_total ,
                'type' => 1,
                'created_by_id'         => $Products->created_by_id,
                'created_by_name'         => $Products->created_by_name,
                'created_at'        => date('Y-m-d H:i:s') , 
                'updated_at'        => date('Y-m-d H:i:s')
            );
        DB::table('gpff_dealer_payments')
        ->insert($values1);
        $products = DB::table('gpff_purchase_order_products')
                    ->where('purchase_order_id',$Products->purchase_order_id)
                    ->where('status',0)
                    ->get();
        if(count($products) == 0){
            $values2 = array(
                'invoice_status' => 1
            );
            DB::table('gpff_purchase_order')
            ->where('purchase_order_id',$Products->purchase_order_id)
            ->update($values2);
        }
        return 1;
    }

    public function updatePO_Product($purchase_order_product_id,$no_of_items){
        $po_product_details = DB::table('gpff_purchase_order_products')
        ->where('purchase_order_product_id',$purchase_order_product_id)
        ->first();
        $balance_items = ((int)$po_product_details->balance_items) - ((int)$no_of_items);
        $received_items = (int)$po_product_details->received_items + (int)$no_of_items;
        $no_of_items = (int)$po_product_details->no_of_items;
        if($no_of_items == $received_items){
            $update_values = array(
                'balance_items' => $balance_items,
                'received_items' => $received_items,
                'status' => 1
            );
        }else{
            $update_values = array(
                'balance_items' => $balance_items,
                'received_items' => $received_items
            );
        }
        $update = DB::table('gpff_purchase_order_products')
        ->where('purchase_order_product_id',$purchase_order_product_id)
        ->update($update_values);
        return $update;
    }

    public function getAllPOInvoice($data) {
        $result = DB::table('gpff_purchase_invoice')
                  ->join('gpff_branch','gpff_branch.branch_id','gpff_purchase_invoice.branch_id')
                  ->join('gpff_region','gpff_region.region_id','gpff_purchase_invoice.region_id')
                ->orderBy('gpff_purchase_invoice.updated_at','DESC');
        if($data->start_date != ''){
            $result = $result->whereBetween(DB::RAW('date(gpff_purchase_invoice.created_at)'), [date($data->start_date), date($data->end_date)]);
        }
        if($data->invoice_id != ''){
            $result = $result->where('gpff_purchase_invoice.invoice_id',$data->invoice_id);
        }
        if($data->branch_id != ''){
            $result = $result->whereIn('gpff_purchase_invoice.branch_id',$data->branch_id);
        }
        if($data->region_id != ''){
            $result = $result->whereIn('gpff_purchase_invoice.region_id',$data->region_id);
        }
        if($data->payment_status != ''){
            $result = $result->whereIn('payment_status',$data->payment_status);
        }
        if($data->product_status != ''){
            $result = $result->whereIn('product_status',$data->product_status);
        }
        if($data->warehouse_id != ''){
            $result = $result->whereIn('warehouse_id',$data->warehouse_id);
        }
        if($data->supplier_id != ''){
            $result = $result->whereIn('supplier_id',$data->supplier_id);
        }
        return $result->get(['invoice_id','gpff_purchase_invoice.created_at','supplier_id','supplier_name','warehouse_id','warehouse_name','grand_total','balance_amount','payment_status']);
    }

    public function getPOInvoiceProducts($data) {
        $result = DB::table('gpff_purchase_invoice_products')
                ->where('purchase_invoice_id',$data->invoice_no)
                ->whereIn('product_status',[$data->product_status])
                ->orderBy('updated_at','DESC');
        return $result->get();
    }

    // Stock acknowledge
    public function poStockAcknowledge($Products) 
    {   
        $exit = DB::table('gpff_product_stock')
                ->where('category_id', $Products->category_id)
                ->where('product_id', $Products->product_id)
                ->where('warehouse_id', $Products->warehouse_id)
                ->First();

        $prstid = DB::table('gpff_product_batch_stock')
                ->where('category_id', $Products->category_id)
                ->where('product_id', $Products->product_id)
                ->where('batch_id', $Products->batch_no)
                ->where('warehouse_id', $Products->warehouse_id)
                ->First();
        
        $product_details = DB::table('gpff_product')                
                           ->where('product_id',$Products->product_id)
                           ->first();

        $war = DB::table('gpff_warehouse')
                ->where('warehouse_id', $Products->warehouse_id)
                ->First();
        if($exit == '' && $prstid == ''){

                $productvalue = array(
                'category_id'           => $Products->category_id,
                'category_name'         => $Products->category_name,
                'product_id'            => $Products->product_id,
                'product_genericname'   => $Products->product_genericname,
                'product_type'          => $product_details->product_type,
                'warehouse_id'          => $Products->warehouse_id ,
                'stockist_id'           => $Products->stockist_id,
                'warehouse_name'        => $war->warehouse_name ,
                'region_id'             => $war->region_id,
                'branch_id'             => $war->branch_id,
                'created_at'            => date('Y-m-d H:i:s') ,
                'updated_at'            => date('Y-m-d H:i:s')
                );

                $prod_sto_id = DB::table('gpff_product_stock')
                ->insertGetId($productvalue);

                $batch_stock_list_value = array(
                'product_stock_id'      => $prod_sto_id ,
                'category_id'           => $Products->category_id,
                'warehouse_id'          => $Products->warehouse_id ,
                'product_id'            => $Products->product_id,
                'batch_id'              => $Products->batch_no,
                'stockist_id'           => $Products->stockist_id,
                'product_genericname'   => $Products->product_genericname,
                'product_netprice'      => $product_details->product_netprice,
                'product_grossprice'    => $product_details->product_grossprice,
                'exp_date'              => $Products->exp_date,
                'product_tot_qty'       => $Products->product_qty,
                'product_blc_qty'       => $Products->product_qty,
                'created_at'            => date('Y-m-d H:i:s') ,
                'updated_at'            => date('Y-m-d H:i:s')
                );

                DB::table('gpff_product_batch_stock')
                ->insert($batch_stock_list_value);
            }else if($exit != '' && $prstid == ''){

                $batch_stock_list_value = array(
                'product_stock_id'      => $exit->product_stock_id,
                'category_id'           => $Products->category_id,
                'warehouse_id'          => $Products->warehouse_id ,
                'product_id'            => $Products->product_id,
                'batch_id'              => $Products->batch_no,
                'stockist_id'           => $Products->stockist_id,
                'product_genericname'   => $Products->product_genericname,
                'product_netprice'      => $product_details->product_netprice,
                'product_grossprice'    => $product_details->product_grossprice,
                'exp_date'              => $Products->exp_date,
                'product_tot_qty'       => $Products->product_qty,
                'product_blc_qty'       => $Products->product_qty,
                'created_at'            => date('Y-m-d H:i:s') ,
                'updated_at'            => date('Y-m-d H:i:s')
                );

                DB::table('gpff_product_batch_stock')
                ->insert($batch_stock_list_value);
            }else{

                $blc_qty  =  DB::table('gpff_product_batch_stock')
                            ->where('category_id', $Products->category_id)
                            ->where('product_id', $Products->product_id)
                            ->where('batch_id', $Products->batch_no)
                            ->where('warehouse_id', $Products->warehouse_id)
                            ->sum('product_blc_qty');

                $tot_qty  =  DB::table('gpff_product_batch_stock')
                            ->where('category_id', $Products->category_id)
                            ->where('product_id', $Products->product_id)
                            ->where('batch_id', $Products->batch_no)
                            ->where('warehouse_id', $Products->warehouse_id)
                            ->sum('product_tot_qty');

                $qty = $Products->product_qty;

                $blc    = $blc_qty + $qty;
                $total  = $tot_qty + $qty;

                $value = array(
                    'product_blc_qty'       => $blc ,
                    'product_tot_qty'       => $total,
                    'product_status'        => 1,
                    'product_netprice'      => $product_details->product_netprice,
                    'product_grossprice'    => $product_details->product_grossprice,
                    'updated_at'            => date('Y-m-d H:i:s')
                );

                DB::table('gpff_product_batch_stock')
                ->where('category_id', $Products->category_id)
                ->where('product_id', $Products->product_id)
                ->where('batch_id', $Products->batch_no)
                ->where('warehouse_id', $Products->warehouse_id)
                ->update($value);
            }

        $accept_li_value = array(
            'product_status'   => 1,
            'updated_at'        => date('Y-m-d H:i:s')
        );

        DB::table('gpff_purchase_invoice_products')
        ->where('purchase_invoice_product_id',$Products->purchase_invoice_product_id)
        ->update($accept_li_value);

        $products = DB::table('gpff_purchase_invoice_products')
                    ->where('purchase_invoice_id',$Products->purchase_invoice_id)
                    ->where('product_status',0)
                    ->get();
        if(count($products) == 0){
            $values2 = array(
                'product_status' => 1
            );
            DB::table('gpff_purchase_invoice')
            ->where('invoice_id',$Products->purchase_invoice_id)
            ->update($values2);
        }
        return 1;
    }

    public function addPOPaymentDetails($Products){
        $values = array(
            "purchase_invoice_id"   =>$Products->invoice_id,
            "supplier_id"           =>$Products->supplier_id,
            "supplier_name"         =>$Products->supplier_name,
            "amount"                =>$Products->payment_amount,
            "payment_type"          =>$Products->payment_type,
            "bank_name"             =>$Products->bank_name,
            "card_type"             =>$Products->card_type,
            "card_no"               =>$Products->card_no,
            "account_holder_name"   =>$Products->holder_name,
            "cheque_no"             =>$Products->cheque_no,
            "account_no"            =>$Products->account_no,
            "transaction_id"        =>$Products->transaction_id,
            "type"                  =>2,
            "created_by_id"         =>$Products->created_by_id,
            "created_by_name"       =>$Products->created_by_name,
            "created_at"            =>date('Y-m-d H:i:s'),
            "updated_at"            =>date('Y-m-d H:i:s')
        );

        $colectionid = DB::table('gpff_dealer_payments')
        ->insertGetId($values);
        $balance_amount = (int)$Products->balance_amount - (int)$Products->payment_amount;
        if($balance_amount == 0){
            $payment = array(
                'balance_amount'   => 0,
                'payment_status' => 1,
                'updated_at'     => date('Y-m-d H:i:s')
            );
        }else{
            $payment = array(
                'balance_amount'   => $balance_amount,
                'updated_at'     => date('Y-m-d H:i:s')
            );
        }


        DB::table('gpff_purchase_invoice')
        ->where('invoice_id',$Products->invoice_id)
        ->update($payment);
        
        return 1;
    }

    public function getPOPaymentTransaction($data) {
        $result = DB::table('gpff_dealer_payments')
                ->where('purchase_invoice_id',$data->invoice_id)
                ->where('type',2)
                ->orderBy('updated_at','DESC');
        return $result->get();
    }
}

