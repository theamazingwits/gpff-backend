<?php

namespace App\Http\Controllers;

use Closure;
use DB;
use \PDF;
use Mail;
use Carbon\Carbon;
use App\Imports\UsersImport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;


require(base_path().'/app/Http/Middleware/Common.php');
// 
class Telecalling
{
        /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
     
    public function handle($request, Closure $next)
    {
        return $next($request);
    }

    public function addTelecalling($Telecallings){

    	$values = array(

    	'field_officer_id' 		=> $Telecallings->field_officer_id,
		'field_officer_name'	=> $Telecallings->field_officer_name,
		'customer_id'			=> $Telecallings->customer_id,
		'customer_name'			=> $Telecallings->customer_name,
        'area_id'               => $Telecallings->area_id,
        'region_id'             => $Telecallings->region_id,
        'branch_id'             => $Telecallings->branch_id,
		'tele_date'			    => date('Y-m-d H:i:s'),
        'created_at'            => date('Y-m-d H:i:s'),
        'updated_at'            => date('Y-m-d H:i:s')
	);

    	DB::table('gpff_telecalling')
    	->insert($values);

        return 1;

    }

    public function fieldOfficerBaseTeleCalling($Telecallings){

        if($Telecallings->type ==1){
            $results = DB::table('gpff_telecalling')
            ->whereDate('tele_date', Carbon::today())
            ->where('field_officer_id', $Telecallings->field_officer_id)
            ->orderBy('created_at', 'DESC')
            ->get();
        }else if($Telecallings->type == 3){
            $results = DB::table('gpff_telecalling')
            ->where('field_officer_id', $Telecallings->field_officer_id)
            ->whereDate('tele_date', $Telecallings->date)
            ->orderBy('created_at', 'DESC')
            ->get();
        }else{
            $results = DB::table('gpff_telecalling')
            ->where('field_officer_id', $Telecallings->field_officer_id)
            ->orderBy('created_at', 'DESC')
            ->get();
        }
        return $results;
    }

    public function telecallingReport($Telecallings){

        $details = DB::table('gpff_telecalling')
        ->join('gpff_customer as gcus', 'gpff_telecalling.customer_id', '=', 'gcus.customer_id')
        ->join('gpff_users as guse', 'gpff_telecalling.field_officer_id', '=', 'guse.user_id')
        ->whereBetween(DB::RAW('date(gpff_telecalling.created_at)'), [date($Telecallings->start_date), date($Telecallings->end_date)]);

        if($Telecallings->branch_id != ''){
            $details = $details->whereIn('gpff_telecalling.branch_id', $Telecallings->branch_id);
        }
        if($Telecallings->region_id != ''){
            $details = $details->whereIn('gpff_telecalling.region_id', $Telecallings->region_id);
        }
        if($Telecallings->area_id != ''){
            $details = $details->whereIn('gpff_telecalling.area_id', $Telecallings->area_id);
        }
        if($Telecallings->field_officer_id != ''){
            $details = $details->whereIn('gpff_telecalling.field_officer_id', $Telecallings->field_officer_id);
        }

        if($Telecallings->customer_id != ''){   
            $details = $details->whereIn('gpff_telecalling.customer_id', $Telecallings->customer_id);
        }
        if ($Telecallings->task_status != '') {
        $details = $details->whereIn('gpff_telecalling.status', $Telecallings->task_status);
      }
        switch($Telecallings->report_type){
        case 1: //Data tables data
          $results = $details->orderBy('gpff_telecalling.updated_at','DESC')->get();
        break;
        case 2:
          $total_count = $details->count();
          $details = $details->groupBy('gpff_telecalling.customer_id')                   
                    ->get(['gpff_telecalling.customer_id','gcus.customer_name',DB::raw('count(gpff_telecalling.telecalling_id) as task_count')]);
          $cus_data = [];
          foreach($details as $customer)
          {
            $cus_data[] =  (array) $customer;
          }
          $customer_wise = usort($cus_data, function($a, $b) {
                         return ($a['task_count'] > $b['task_count']? -1 : 1);
                       });
          $result = array_merge(
            ['customer_wise' => $cus_data,
            'total_count' => $total_count]
          );
          $results = $result;
        break;
        case 3:
          $total_count = $details->count();
          $details = $details->groupBy('gpff_telecalling.field_officer_id')                   
                    ->get(['gpff_telecalling.field_officer_id','guse.firstname',DB::raw('count(gpff_telecalling.telecalling_id) as task_count')]);
          $fo_data = [];
          foreach($details as $fo)
          {
            $fo_data[] =  (array) $fo;
          }
          $fo_wise = usort($fo_data, function($a, $b) {
                         return ($a['task_count'] > $b['task_count']? -1 : 1);
                       });
          $result = array_merge(
            ['fo_wise' => $fo_data,
          'total_count' => $total_count]
          );
          $results = $result;
        break;

      }        


        return $results;

    }

    public function addRecalling($Telecallings){

        $datas = DB::table('gpff_recalling')
        ->where('telecalling_id', $Telecallings->telecalling_id)
        ->where('status', 0)
        ->First(['status', 'recalling_id']);

        if($datas){
            if($datas->status == 0){
                $values = array(
                    "status" => 1,
                    "updated_at" => date('Y-m-d H:i:s')
                );
                DB::table('gpff_recalling')
                ->where('recalling_id', $datas->recalling_id)
                ->update($values);

                $recal_value = array(
                    "telecalling_id"  => $Telecallings->telecalling_id,
                    "customer_name"   => $Telecallings->customer_name,
                    "customer_id"     => $Telecallings->customer_id,
                    "field_officer_id" => $Telecallings->field_officer_id,
                    "field_officer_name" => $Telecallings->field_officer_name,
                    "telecalling_date"  => $Telecallings->telecalling_date,
                    "recalling_date"  => $Telecallings->recalling_date,
                    "status"        => 0,
                    "message"       => $Telecallings->message,
                    "created_at"    => date('Y-m-d H:i:s'),
                    "updated_at"    => date('Y-m-d H:i:s')
                );


                DB::table('gpff_recalling')
                ->insert($recal_value);

            }else{
                $recal_value = array(
                    "telecalling_id"  => $Telecallings->telecalling_id,
                    "customer_name"   => $Telecallings->customer_name,
                    "customer_id"     => $Telecallings->customer_id,
                    "field_officer_id" => $Telecallings->field_officer_id,
                    "field_officer_name" => $Telecallings->field_officer_name,
                    "telecalling_date"  => $Telecallings->telecalling_date,
                    "recalling_date"  => $Telecallings->recalling_date,
                    "status"        => 0,
                    "message"       => $Telecallings->message,
                    "created_at"    => date('Y-m-d H:i:s'),
                    "updated_at"    => date('Y-m-d H:i:s')
                );

                $re_call = DB::table('gpff_recalling')
                ->insertGetId($recal_value);
                $feedback = "Re Call at : ".date('d-m-Y h:i A',strtotime($Telecallings->recalling_date))." Reason :".$Telecallings->message;
                $updates = array(
                    "recalling_id"=>$re_call,
                    "telecalling_type"=>4,
                    "feedback" => $feedback,
                    "status"=>1,
                    "updated_at"=>date('Y-m-d H:i:s')
                );

                DB::table('gpff_telecalling')
                ->where('telecalling_id', $Telecallings->telecalling_id)
                ->update($updates);
            }
        }else{
            $recal_value = array(
                "telecalling_id"  => $Telecallings->telecalling_id,
                "customer_name"   => $Telecallings->customer_name,
                "customer_id"     => $Telecallings->customer_id,
                "field_officer_id" => $Telecallings->field_officer_id,
                "field_officer_name" => $Telecallings->field_officer_name,
                "telecalling_date"  => $Telecallings->telecalling_date,
                "recalling_date"  => $Telecallings->recalling_date,
                "status"        => 0,
                "message"       => $Telecallings->message,
                "created_at"    => date('Y-m-d H:i:s'),
                "updated_at"    => date('Y-m-d H:i:s')
            );

            $re_call = DB::table('gpff_recalling')
            ->insertGetId($recal_value);
             $feedback = "Re Call at : ".date('d-m-Y h:i A',strtotime($Telecallings->recalling_date))." Reason :".$Telecallings->message;
            $updates = array(
                "recalling_id"=>$re_call,
                "telecalling_type"=>4,
                "feedback" => $feedback,
                "status"=>1,
                "updated_at"=>date('Y-m-d H:i:s')
            );

            DB::table('gpff_telecalling')
            ->where('telecalling_id', $Telecallings->telecalling_id)
            ->update($updates);
        }
        return 1;
    }

    public function fieldOfficerBasedRecalling($Telecallings){
        // print_r($Telecallings->field_officer_id);
        $details = DB::table('gpff_telecalling as gptel')
                ->join('gpff_recalling as gprec', 'gptel.telecalling_id', 'gprec.telecalling_id')
                ->where('gptel.field_officer_id', $Telecallings->field_officer_id)
                ->whereDate('gprec.recalling_date', Carbon::today())
                ->get(['gptel.telecalling_type','gptel.field_officer_id','gptel.field_officer_name'  ,'gptel.customer_id','gptel.customer_name', 'gptel.area_id','gptel.region_id', 'gptel.tele_date','gptel.order_id','gptel.feedback_id','gptel.not_available_id','gprec.recalling_id','gprec.telecalling_date','gprec.recalling_date','gprec.status','gprec.message']);

        $results = $details;
        return $results;
    }

    public function telecallingBasedAllDetails($Telecallings){
        $details = [];

        $details['order_details'] = DB::table('gpff_order')
                                ->where('telecalling_id', $Telecallings->telecalling_id)
                                ->where('telecalling_type',1)
                                ->get();

        $details['feed_back'] = DB::table('gpff_feedback')
                                ->where('telecalling_id', $Telecallings->telecalling_id)
                                ->where('telecalling_type',2)
                                ->get();

        $details['not_available'] = DB::table('gpff_feedback')
                                ->where('telecalling_id', $Telecallings->telecalling_id)
                                ->where('telecalling_type',3)
                                ->get();

        return $details;
    }

    public function telecallingStatusUpdate($Telecallings){
        
        $value = array(
            "telecalling_id" => $Telecallings->telecalling_id,
            "status"    => 1,
            "updated_at" => date('Y-m-d H:i:s')
        );

        DB::table('gpff_telecalling')
        ->where('telecalling_id', $Telecallings->telecalling_id)
        ->update($value);

        return 1;
    }

}   