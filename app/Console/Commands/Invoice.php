<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use \PDF;
use Mail;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;

class Invoice extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Invoice:Generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Automated Invoice Generation';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //Reeport Mail Send
        $result = DB::table('gpff_invoice_log')
                ->where('invoice_log_status',0)
                ->first();

        if($result){
            if($result->order_type == 1){
                   $order_id = $result->order_id;
                   $order_details = DB::table('gpff_order')
                        ->where('order_id',$order_id)
                        ->First();

                    $order_list_detail = DB::table('gpff_order as gor')
                                        ->join('gpff_order_list as gol', 'gol.order_id', 'gor.order_id')
                                        ->join('gpff_order_list_batchwise as gprl','gol.order_list_id','gprl.order_list_id')
                                        ->where('gol.order_id',$order_id)
                                        ->get();

                    $warehouse = DB::table('gpff_warehouse')
                                ->where('warehouse_id', $order_details->warehouse_id)
                                ->First();

                    $cus_de = DB::table('gpff_customer')
                                    ->where('customer_id',$order_details->customer_id)
                                    ->First();

                    $com_details = DB::table('gpff_branch')
                                    ->where('branch_id',$order_details->branch_id)
                                    ->First();

                    $number = $this->numbertostring($order_details->or_tot_price);
                    $pdf_name = "GPFF_OR_".$order_id;;
                    //Invoice
                    view()->share('datas',$order_list_detail);
                    view()->share('order_details',$order_details);
                    view()->share('cus_de',$cus_de);
                    view()->share('war', $warehouse);
                    view()->share('com_details',$com_details);
                    view()->share('invoice',$pdf_name);
                    view()->share('value',$number);
                    view()->share('date',Carbon::now());

                    $pdf = \PDF::loadView('or_invoice')
                    ->setPaper('a4', 'landscape');
                    $pdf->save(public_path('Order_invoice/'.$pdf_name.'.pdf'));
                    $file_name = $pdf_name.'.pdf';
                    $name = $file_name;
                    $filePath = 'Order_invoice/'.$name; 

                    Storage::disk('s3')->put($filePath, file_get_contents(public_path('Order_invoice')."/".$file_name));
                    $log = DB::table('gpff_invoice_log')
                    ->where('invoice_log_id', $result->invoice_log_id)
                    ->update([
                        'invoice_log_status' => 1, 
                        'updated_at' => date('Y-m-d H:i:s')
                        ]);
                    print_r($log);

            }
        }



    }
    public function numbertostring($number)
    {  
        $decimal = round($number - ($no = floor($number)), 2) * 100;
        $decimal_part = $decimal;
        $hundred = null;
        $hundreds = null;
        $digits_length = strlen($no);
        $decimal_length = strlen($decimal);
        $i = 0;
        $str = array();
        $str2 = array();
        $words = array(0 => '', 1 => 'one', 2 => 'two',
            3 => 'three', 4 => 'four', 5 => 'five', 6 => 'six',
            7 => 'seven', 8 => 'eight', 9 => 'nine',
            10 => 'ten', 11 => 'eleven', 12 => 'twelve',
            13 => 'thirteen', 14 => 'fourteen', 15 => 'fifteen',
            16 => 'sixteen', 17 => 'seventeen', 18 => 'eighteen',
            19 => 'nineteen', 20 => 'twenty', 30 => 'thirty',
            40 => 'forty', 50 => 'fifty', 60 => 'sixty',
            70 => 'seventy', 80 => 'eighty', 90 => 'ninety');
        $digits = array('', 'hundred','thousand','lakh', 'crore');

        while( $i < $digits_length ) {
            $divider = ($i == 2) ? 10 : 100;
            $number = floor($no % $divider);
            $no = floor($no / $divider);
            $i += $divider == 10 ? 1 : 2;
            if ($number) {
                $plural = (($counter = count($str)) && $number > 9) ? 's' : null;
                $hundred = ($counter == 1 && $str[0]) ? ' and ' : null;
                $str [] = ($number < 21) ? $words[$number].' '. $digits[$counter]. $plural.' '.$hundred:$words[floor($number / 10) * 10].' '.$words[$number % 10]. ' '.$digits[$counter].$plural.' '.$hundred;
            } else $str[] = null;
        }

        $d = 0;
        while( $d < $decimal_length ) {
            $divider = ($d == 2) ? 10 : 100;
            $decimal_number = floor($decimal % $divider);
            $decimal = floor($decimal / $divider);
            $d += $divider == 10 ? 1 : 2;
            if ($decimal_number) {
                $plurals = (($counter = count($str2)) && $decimal_number > 9) ? 's' : null;
                $hundreds = ($counter == 1 && $str2[0]) ? ' and ' : null;
                @$str2 [] = ($decimal_number < 21) ? $words[$decimal_number].' '. $digits[$decimal_number]. $plural.' '.$hundred:$words[floor($decimal_number / 10) * 10].' '.$words[$decimal_number % 10]. ' '.$digits[$counter].$plural.' '.$hundred;
            } else $str2[] = null;
        }

        $Rupees = implode('', array_reverse($str));
        $paise = implode('', array_reverse($str2));
        $paise = ($decimal_part > 0) ? $paise . ' Kyat' : '';
        return ($Rupees ? $Rupees . 'Kyat ' : '') . $paise;
    }
}
