<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use File;
use Mail;

require(base_path().'/app/Http/Middleware/Collection.php');

class CollectionController extends Controller{

	public function addCollectionDetails(Request $request){
		$Collections = new Collection();

		// $Collections->user_id				= $request->input('user_id');
		$Collections->invoice_no 			= $request->input('invoice_no');
		$Collections->order_id	 			= $request->input('order_id');
		$Collections->customer_id 			= $request->input('customer_id');
		$Collections->customer_name 		= $request->input('customer_name');
		$Collections->collection_date 		= $request->input('collection_date');
		$Collections->collection_type		= $request->input('collection_type');
		$Collections->collection_status 	= $request->input('collection_status');
		$Collections->collection_mode 		= $request->input('collection_mode');
		$Collections->collection_amount 	= $request->input('collection_amount');
		$Collections->status 				= $request->input('status');
		$Collections->collection_for		= $request->input('collection_for');
		// $Collections->created_by 			= $request->input('created_by');
		$Collections->collection_cr_id		= $request->input('collection_cr_id');
		$Collections->collection_cr_name 	= $request->input('collection_cr_name');
		$Collections->total_price 			= $request->input('total_price');
		$Collections->collection_cr_date 	= $request->input('collection_cr_date');
		$Collections->collect_approve_id 	= $request->input('collect_approve_id');
		$Collections->collect_approve_name 	= $request->input('collect_approve_name');
		$Collections->collect_approve_date  = $request->input('collect_approve_date');
		$Collections->col_type 				= $request->input('col_type');
		$Collections->finance_user_id 		= $request->input('finance_user_id');
		$Collections->area_id 				= $request->input('area_id');
		$Collections->region_id 			= $request->input('region_id');
		$Collections->warehouse_id 			= $request->input('warehouse_id');
		$Collections->doer_id 				= $request->input('doer_id');
		$Collections->doer_name 			= $request->input('doer_name');
		$Collections->checker_id 				= $request->input('checker_id');
		$Collections->checker_name 			= $request->input('checker_name');
		// $Collections->doer_date 			= $request->input('doer_date');
		// $Collections->doer_status 			= $request->input('doer_status');
		// $Collections->approve_status		= $request->input()
		if($request->input('bank_name')){
			$Collections->bank_name = $request->input('bank_name');
		}else{
			$Collections->bank_name = "";
		}
		if($request->input('card_type')){
			$Collections->card_type = $request->input('card_type');
		}else{
			$Collections->card_type = "";
		}
		if($request->input('card_no')){
			$Collections->card_no = $request->input('card_no');
		}else{
			$Collections->card_no = "";
		}
		if($request->input('holder_name')){
			$Collections->holder_name = $request->input('holder_name');
		}else{
			$Collections->holder_name = "";
		}
		if($request->input('account_no')){
			$Collections->account_no = $request->input('account_no');
		}else{
			$Collections->account_no = "";
		}
		if($request->input('transaction_id')){
			$Collections->transaction_id = $request->input('transaction_id');
		}else{
			$Collections->transaction_id = "";
		}
		if($request->input('cheque_no')){
			$Collections->cheque_no = $request->input('cheque_no');
		}else{
			$Collections->cheque_no = "";
		}

		$results = $Collections->addCollectionDetails($Collections);

		if($results == 1){
			$response = response()->json(['status'=>'success', 'message'=>"Collection details stored successfully"]);
		}else{
			$response = response()->json(['status'=>'failure', 'message'=>"Something went wrong"]);
		}
		return $response;
	}

	public function doerStatusUpdate(Request $request){
		$Collections = new Collection();

		$Collections->collection_id = $request->input('collection_id');
		$Collections->doer_id = $request->input('doer_id');
		$Collections->doer_name = $request->input('doer_name');

		$results = $Collections->doerStatusUpdate($Collections);
		if($results == 1){
			$response = response()->json(['status'=>'success', 'message'=>"collection accepted"]);
		}else{
			$response = response()->json(['status'=>'failure', 'message'=>'Something went wrong']);
		}
		return $response;
	}

	public function doerCollectionDetails(Request $request){
		$Collections = new Collection();

		$Collections->doer_id = $request->input('doer_id');

		$results = $Collections->doerCollectionDetails($Collections);
		if($results){
			$response = response()->json(['status'=>'success', 'message'=>"collection accepted details", 'data'=>$results]);
		}else{
			$response = response()->json(['status'=>'failure', 'message'=>'Something went wrong']);
		}
		return $response;

	}

	public function fetchCollectionDateBased(Request $request){

		$Collections = new Collection();
		$Collections->collection_date = $request->input("collection_date");

		$results = $Collections->fetchCollectionDateBased($Collections);

		if(count($results)>0){
			$response = response()->json(['status'=>'success', 'message'=>"Collection details retrive successfully", 'data'=>$results]);
		}else{
			$response = response()->json(['status'=>'failure', 'message'=>"Something went wrong"]);
		}

		return $response;
	}

	public function fetchInvoice_noBasedDetails(Request $request){

		$Collections = new Collection();
		$Collections->invoice_no = $request->input("invoice_no");

		$results = $Collections->fetchInvoice_noBasedDetails($Collections);

		if(count($results)>0){
			$response = response()->json(['status'=>'success', 'message'=>"Collection details retrive successfully", 'data'=>$results]);
		}else{
			$response = response()->json(['status'=>'failure', 'message'=>"Something went wrong"]);
		}
		
		return $response;
	}

	public function addSalereturn(Request $request){


		$Collections = new Collection();
		$Collections->finance_user_id		= $request->input('finance_user_id');
		$Collections->return_qty 			= $request->input('return_qty');
		$Collections->or_gross_total		= $request->input('or_gross_total');
		$Collections->invoice_no 			= $request->input('invoice_no');
		$Collections->order_id				= $request->input('order_id');
		$Collections->return_grand_total	= $request->input('return_grand_total');
		$Collections->customer_id			= $request->input('customer_id');
		$Collections->customer_name			= $request->input('customer_name');
		$Collections->order_return_date		= $request->input('order_return_date');
		$Collections->return_reason			= $request->input('return_reason');
		$Collections->or_tot_price 			= $request->input('or_tot_price');
		$Collections->branch_id				= $request->input('branch_id');
		$Collections->region_id				= $request->input('region_id');
		$Collections->area_id				= $request->input('area_id');
		$Collections->stockist_id			= $request->input('stockist_id');
		$Collections->warehouse_id			= $request->input('warehouse_id');
		$Collections->payment_type			= $request->input('payment_type');
		$Collections->or_type 				= $request->input('or_type');
		$Collections->payment_status		= $request->input('payment_status');
		$Collections->payment_name			= $request->input('payment_name');
		$Collections->return_list			= $request->input('return_list');
		$Collections->or_return_type 		= $request->input('or_return_type');

		// print_r($request->input('finance_user_id'));
		// print_r("expression");

		$results = $Collections->addSalereturn($Collections);

		if($results==1){
			$response = response()->json(['status' => 'success' , 'message' => 'Order return successfully']);
		}else{
			$response = response()->json(['status' => 'failure' , 'message' => 'Order return failed']);
		}

		return $response;
	}

	public function saleReturnIdBasedDetails(Request $request){

		$Collections = new Collection();
		// $Collections->invoice_no = $request->input('invoice_no');
		$Collections->sale_return_id = $request->input('sale_return_id');

		$results = $Collections->saleReturnIdBasedDetails($Collections);

		if(count($results)>0){
			$response = response()->json(['status'=>'success', 'message'=>'sale return details', 'data'=>$results]);
		}else{
			$response = response()->json(['status'=>'failure', 'message'=> 'Something went wrong to retrive the data']);
		}
		return $response;
	}

	public function acceptCollectionDetail(Request $request){

		$Collections = new Collection();
		$Collections->collection_id = $request->input('collection_id');

		$results = $Collections->acceptCollectionDetail($Collections);

		if($results == 1){
			$response = response()->json(['status'=>'success', 'message'=>'Collection accepted', 'data'=>$results]);
		}else{
			$response = response()->json(['status'=>'failure', 'message'=>"Something went wrong"]);
		}
		return $response;
	}

	public function fetchVendorOrderList(Request $request){
		$Collections = new Collection();
		$Collections->area_manager_id = $request->input('area_manager_id');
		$Collections->type = $request->input('type');
		$results = $Collections->fetchVendorOrderList($Collections);

		if(count($results)>0){
			$response = response()->json(['status'=>'success', 'message'=>'invoice list successfully fetched', 'data'=>$results]);
		}else{
			$response = response()->json(['status'=>'failure', 'message'=> 'No data found']);
		}
		return $response;
	}

	// public function vendorCustomerCollectionDetails(Request $request){
	// 	$Collections = new Collection();
	// 	$Collections->collection_for = $request->input('collection_for');

	// 	$results = $Collections->vendorCustomerCollectionDetails($Collections);
	// 	if(count($results)>0){
	// 		if($Collections->collection_id == 1){
	// 			$response = response()->json(['status'=>'success', 'messages'=>'customer collection details']);
	// 		}else{
	// 			$response = response()->json(['status'=>'success', 'messages'=>'customer collection details']);
	// 	}else{
	// 		$response = response()->json(['status'=>'failure', 'messages'=> 'No data found']);
	// 	}

	// }

	public function fetchCollectionDetailsBasedId(Request $request){

		$Collections = new Collection();

		$Collections->collection_id = $request->input('collection_id');
		$Collections->type = $request->input('type');

		$results = $Collections->fetchCollectionDetailsBasedId($Collections);
		// prin

		if($results){
			$response = response()->json(['status'=>'success', 'messages'=>'Transaction details', 'data'=>$results]);
		}else{
			$response = response()->json(['status'=>'failure', 'messages'=>'No data found']);
		}
		return $response;
	}


	public function deleteCollectionDetails(Request $request){

		$Collections = new Collection();

		$Collections->collection_id = $request->input('collection_id');

		$results = $Collections->deleteCollectionDetails($Collections);

		if($results == 1){
			$response = response()->json(['status'=>'success', 'messages'=>'Transaction details successfully deleted ']);
		}else{
			$response = response()->json(['status'=>'failure', 'messages'=>'No data found']);
		}
		return $response;
	}

	// public function updateCollectionDetails(Request $request){
	// 	$Collections = new Collection();

	// 	$Collections->

	// }

	public function fetchCollectionDetailsOrderBase(Request $request){

		$Collections = new Collection();

		$Collections->order_id = $request->input('order_id');
		$Collections->type 	   = $request->input('type');

		$results = $Collections->fetchCollectionDetailsOrderBase($Collections);
		// prin

		if($results){
			$response = response()->json(['status'=>'success', 'messages'=>'Transaction details', 'data'=>$results]);
		}else{
			$response = response()->json(['status'=>'failure', 'messages'=>'No data found']);
		}
		return $response;
	}

	public function fetchFinanceUserBasedCollections(Request $request){
		$Collections = new Collection();
		
		$Collections->finance_user_id = $request->input('finance_user_id');
		$Collections->collection_for = $request->input('collection_for');
		$Collections->start_date = $request->input('start_date');
		$Collections->end_date = $request->input('end_date');
		if($request->input('customer_id')){
			$Collections->customer_id = $request->input('customer_id');

		}else{
			$Collections->customer_id = '';
		}

		if($request->input('order_id')){
			$Collections->order_id = $request->input('order_id');
		}else{
			$Collections->order_id = '';
		}

		if($request->input('checker_id')){
			$Collections->checker_id = $request->input('checker_id');
		}else{
			$Collections->checker_id = '';
		}
		// print_r($Collections->finance_user_id);

		$results = $Collections->fetchFinanceUserBasedCollections($Collections);

		if($results){
			$response = response()->json(['status'=>'success', 'messages'=>'Collections details', 'data'=>$results]);
		}else{
			$response = response()->json(['status'=>'failure', 'messages'=>'No data found']);
		}
		return $response;


	}

	public function updateCollectionUser(Request $request){
		$Collections = new Collection();

		$Collections->collection_id = $request->input('collection_id');
		$Collections->finance_user_id = $request->input('finance_user_id');

		$results = $Collections->updateCollectionUser($Collections);

		if($results == 1){
			$response = response()->json(['status'=>'success', 'messages'=>'Collections details updated successfully']);
		}else{
			$response = response()->json(['status'=>'failure', 'messages'=>'Something went wrong']);
		}
		return $response;
	}

	public function fetchRegionBaseCusOrderVenOrderDet(Request $request){
		$Collections = new Collection();
		$Collections->type = $request->input('type');
		$Collections->region_id = $request->input('region_id');


		// $Collections->start_date = $request->input('start_date');
		// $Collections->end_date =$request->input('end_date');

		if($request->input('customer_id')){
			$Collections->customer_id = $request->input('customer_id');
		}else{
			$Collections->customer_id = '';
		}

		if($request->input('order_id')){
			$Collections->order_id = $request->input('order_id');
		}else{
			$Collections->order_id = '';
		}

		if($request->input('start_date')){
			$Collections->start_date = $request->input('start_date');
			$Collections->end_date 	 = $request->input('end_date');
		}else{
			$Collections->start_date = '';
			$Collections->end_date = '';
		}

		// if($request->input('order_status')){
		// 	$Collections->order_status = $request->input('order_status');
		// }else{
		// 	$Collections->order_status = '';
		// }
		if($request->input('warehouse_id')){
			$Collections->warehouse_id = $request->input('warehouse_id');
		}else{
			$Collections->warehouse_id = '';
		}

		if($request->input('payment_status')){
			$Collections->payment_status = $request->input('payment_status');
		}else{
			$Collections->payment_status = '';
		}

		if($request->input('area_id')){
			$Collections->area_id = $request->input('area_id');
		}else{
			$Collections->area_id = '';
		}

		if($request->input('field_officer_id')){
			$Collections->field_officer_id = $request->input('field_officer_id');
		}else{
			$Collections->field_officer_id = '';
		}

		$results = $Collections->fetchRegionBaseCusOrderVenOrderDet($Collections);

		if($results){
			$response = response()->json(['status'=>'success', 'messages'=>'Orders details', 'data'=>$results]);
		}else{
			$response = response()->json(['status'=>'failure', 'messages'=>'No data found']);
		}
		return $response;
	}

	public function regionBaseCusOrVendorDetails(Request $request){
		$Collections = new Collection();
		$Collections->region_id = $request->input('region_id');
		$Collections->type 		= $request->input('type');

		$results = $Collections->regionBaseCusOrVendorDetails($Collections);
		if($results){
			$response = response()->json(['status'=>'success', 'messages'=>'Vendor details', 'data'=>$results]);
		}else{
			$response = response()->json(['status'=>'success', 'messages'=>'No data found']);
		}
		return $response;
	}

	public function checkerStatusUpdate(Request $request){
		$Collections = new Collection();
		$Collections->collection_id = $request->input('collection_id');
		$Collections->checker_id = $request->input('checker_id');
		$Collections->checker_name = $request->input('checker_name');
		$Collections->finance_user_id = $request->input('finance_user_id');
		// $Collections->check_status = $request->input('check_status');

		$results = $Collections->checkerStatusUpdate($Collections);

		if($results==1){
			$response = response()->json(['status'=>'success', 'message'=>"Check status accepted"]);
		}else{
			$response = response()->json(['status'=>'success', 'messages'=>'Something went wrong']);
		}
		return $response;
	}

	public function checkerStateReject(Request $request){
		$Collections = new Collection();
		$Collections->collection_id = $request->input('collection_id');
		$Collections->checker_id = $request->input('checker_id');
		$Collections->checker_name = $request->input('checker_name');
		// $Collections->check_status = $request->input('check_status');

		$results = $Collections->checkerStateReject($Collections);

		if($results==1){
			$response = response()->json(['status'=>'success', 'message'=>"Check status rejected"]);
		}else{
			$response = response()->json(['status'=>'success', 'messages'=>'Something went wrong']);
		}
		return $response;
	}

	public function regionBasedCollectionDetails(Request $request){
		$Collections = new Collection();

		$Collections->region_id = $request->input('region_id');
		$Collections->collection_for = $request->input('collection_for');
		if($request->input('checker_id')){
            $Collections->checker_id = $request->input('checker_id');
        }else{
            $Collections->checker_id = "";
        }

		$results = $Collections->regionBasedCollectionDetails($Collections);

		if($results){
			$response = response()->json(['status'=>'success', 'message'=>'', 'data'=>$results]);
		}else{
			$response = response()->json(['status'=>'failure', 'message'=>"Something went wrong"]);
		}
		return $response;
	}


	public function fetchRegionBasedCollect(Request $request){
		$Collections = new Collection();
		$Collections->region_id = $request->input('region_id');
		$Collections->check_status = $request->input('check_status');

		$results = $Collections->fetchRegionBasedCollect($Collections);

		if(count($results)>0){
			$response = response()->json(['status'=>'success', 'message'=>"region based collection details fetched successfully", 'data'=>$results]);
		}else{
			$response = response()->json(['status'=>'failure', 'message'=>'No data found']);
		}
		return $response;
	}

	public function fetchCheckedCollectionDetails(Request $request){
		$Collections = new Collection();
		$Collections->checker_id = $request->input('checker_id');
		$Collections->collection_for = $request->input('collection_for');
		// $Collections->
		$results = $Collections->fetchCheckedCollectionDetails($Collections);
		if($results){
			$response = response()->json(['status'=>'success', 'message'=>'Checked collections details', 'data'=> $results]);
		}else{
			$response = response()->json(['status'=>'failure', 'message'=>'No data found']);
		}
		return $response;
	}

	public function fetchFieldOfficerCollection(Request $request){
		$Collections = new Collection();
		$Collections->warehouse_id = $request->input('warehouse_id');

		$results = $Collections->fetchFieldOfficerCollection	($Collections);

		if($results){
			$response = response()->json(['status'=>'success', 'message'=>'', 'data'=> $results]);
		}else{
			$response = response()->json(['status'=>'failure', 'message'=>'No data found']);
		}
		return $response;
	}

	public function fetchAreaBasedCollect(Request $request){
		$Collections = new Collection();
		$Collections->area_id = $request->input('area_id');
		$Collections->check_status = $request->input('check_status');

		$results = $Collections->fetchAreaBasedCollect($Collections);

		if(count($results)>0){
			$response = response()->json(['status'=>'success', 'message'=>"Area based collection details fetched successfully", 'data'=>$results]);
		}else{
			$response = response()->json(['status'=>'failure', 'message'=>'No data found']);
		}
		return $response;
	}

	public function expireDateReport(Request $request){

		// print_r("1");
		$Collections = new Collection();

		$Collections->type = $request->input('type');

		if($request->input('start_date')){
			$Collections->start_date = $request->input('start_date');
			$Collections->end_date = $request->input('end_date');
		}else{
			$Collections->start_date = '';
			$Collections->end_date = '';
		}

		if($request->input('branch_id')){
			$Collections->branch_id = $request->input('branch_id');
		}else{
			$Collections->branch_id = '';
		}

		if($request->input('product_id')){
			$Collections->product_id = $request->input('product_id');
		}else{
			$Collections->product_id = '';
		}

		if($request->input('region_id')){
			$Collections->region_id = $request->input('region_id');
		}else{
			$Collections->region_id = '';
		}

		if($request->input('warehouse_id')){
			$Collections->warehouse_id = $request->input('warehouse_id');
		}else{
			$Collections->warehouse_id = '';
		}

		$results = $Collections->expireDateReport($Collections);

		if(count($results)>0){
			$response = response()->json(['status'=>'success', 'message'=>"Expire date based details fetched successfully", 'data'=>$results]);
		}else{
			$response = response()->json(['status'=>'failure', 'message'=>'No data found']);
		}
		return $response;
	}

	public function dailyCollectionReport(Request $request){
		$Collections = new Collection();

		$Collections->type = $request->input('type');//1.customer, 2.vendor
		$Collections->start_date = $request->input('start_date');
		$Collections->end_date = $request->input('end_date');
		if($request->input('branch_id')){
			$Collections->branch_id = $request->input('branch_id');

		}else{
			$Collections->branch_id = '';
		}
		if($request->input('region_id')){
			$Collections->region_id = $request->input('region_id');

		}else{
			$Collections->region_id = '';
		}

		if($request->input('customer_id')){
			$Collections->customer_id = $request->input('customer_id');

		}else{
			$Collections->customer_id = '';
		}

		if($request->input('invoice_no')){
			$Collections->invoice_no = $request->input('invoice_no');
		}else{
			$Collections->invoice_no = '';
		}

		if($request->input('collection_cr_id')){
			$Collections->collection_cr_id = $request->input('collection_cr_id');
		}else{
			$Collections->collection_cr_id = '';
		}

		if($request->input('start_date')){
			$Collections->start_date = $request->input('start_date');
		}else{
			$Collections->start_date = '';
		}

		if($request->input('report_type')){
			$Collections->report_type = $request->input('report_type');
		}else{
			$Collections->report_type = '';
		}
		$results = $Collections->dailyCollectionReport($Collections);
		if(count($results)>0){
			$response = response()->json(['status'=>'success', 'message'=> 'daily collection details fetched successfully', 'data'=>$results]);
		}else{
			$response = response()->json(['status'=>'failure', 'message'=>'No data found']);
		}

		return $response;
	}

	public function invoiceBasedReturnDetails(Request $request){

		$Collections = new Collection();
		$Collections->invoice_no = $request->input('invoice_no');
		// $Collections->sale_return_id = $request->input('sale_return_id');

		$results = $Collections->invoiceBasedReturnDetails($Collections);

		if(count($results)>0){
			$response = response()->json(['status'=>'success', 'message'=>'Invoice based details', 'data'=>$results]);
		}else{
			$response = response()->json(['status'=>'failure', 'message'=> 'Something went wrong to retrive the data']);
		}
		return $response;
	}

	public function orderReturnReport(Request $request){

		$Collections = new Collection();
		$Collections->or_return_type = $request->input('or_return_type');
		$Collections->start_date = $request->input('start_date');
		$Collections->end_date = $request->input('end_date');

		if($request->input('invoice_no')){
			$Collections->invoice_no = $request->input('invoice_no');
		}else{
			$Collections->invoice_no = '';
		}

		if($request->input('start_date')){
			$Collections->start_date = $request->input('start_date');
		}else{
			$Collections->start_date = '';
		}

		if($request->input('customer_id')){
			$Collections->customer_id = $request->input('customer_id');
		}else{
			$Collections->customer_id = '';
		}

		if($request->input('branch_id')){
			$Collections->branch_id = $request->input('branch_id');
		}else{
			$Collections->branch_id = '';
		}
		if($request->input('region_id')){
			$Collections->region_id = $request->input('region_id');
		}else{
			$Collections->region_id = '';
		}
		if($request->input('warehouse_id')){
			$Collections->warehouse_id = $request->input('warehouse_id');
		}else{
			$Collections->warehouse_id = '';
		}
		if($request->input('field_officer_id')){
			$Collections->field_officer_id = $request->input('field_officer_id');
		}else{
			$Collections->field_officer_id = '';
		}
		if($request->input('area_manager_id')){
			$Collections->area_manager_id = $request->input('area_manager_id');
		}else{
			$Collections->area_manager_id = '';
		}
		$results = $Collections->orderReturnReport($Collections);

		if($results){
			$response = response()->json(['status'=>'success', 'message'=>'sale return details', 'data'=>$results]);
		}else{
			$response = response()->json(['status'=>'failure', 'message'=> 'Something went wrong to retrive the data']);
		}
		return $response;
	}

	public function getCollectionCrDetails(){
		$Collections = new Collection();

		$data = $Collections->getCollectionCrDetails();
		if(count($data)>0){
            $response = response()->json(['status' => 'success' , 'message' => 'Data retrived successfully','data'=>$data]);
        }else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Error in Get details..','data'=>'null']);
        }
        return $response;
	}

	public function getRegionBasedWarehouses(Request $request){
		$Collections = new Collection();

		$Collections->region_id = $request->input('region_id');

		$results = $Collections->getRegionBasedWarehouses($Collections);

		if(count($results)>0){
			$response = response()->json(['status' => 'success' , 'message' => 'Data retrived successfully','data'=>$results]);
		}else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Error in Get details..','data'=>'null']);
		}
		return $response;
	}

	public function financeUsrBasedReturnDetails(Request $request){

		$Collections = new Collection();
		$Collections->or_return_type = $request->input('or_return_type');
		$Collections->finance_user_id = $request->input('finance_user_id');
		// $Collections->sale_return_id = $request->input('sale_return_id');

		$results = $Collections->financeUsrBasedReturnDetails($Collections);

		if(count($results)>0){
			$response = response()->json(['status'=>'success', 'message'=>'Finance user based details', 'data'=>$results]);
		}else{
			$response = response()->json(['status'=>'failure', 'message'=> 'Something went wrong to retrive the data']);
		}
		return $response;
	}

	public function orderIdBasedReturnDetails(Request $request){

		$Collections = new Collection();
		$Collections->or_return_type = $request->input('or_return_type');
		$Collections->order_id = $request->input('order_id');
		// $Collections->sale_return_id = $request->input('sale_return_id');

		$results = $Collections->orderIdBasedReturnDetails($Collections);

		if(count($results)>0){
			$response = response()->json(['status'=>'success', 'message'=>'Finance user based details', 'data'=>$results]);
		}else{
			$response = response()->json(['status'=>'failure', 'message'=> 'Something went wrong to retrive the data']);
		}
		return $response;
	}

	public function checkerDailyCollectionReport(Request $request){
		$Collections = new Collection();

		$Collections->type = $request->input('type');
		$Collections->start_date = $request->input('start_date');
		$Collections->end_date = $request->input('end_date');
		$Collections->checker_id = $request->input('checker_id');

		$results = $Collections->checkerDailyCollectionReport($Collections);

		if(count($results)>0){
			$response = response()->json(['status'=>'success', 'message'=> 'Checker based daily collection details fetched successfully', 'data'=>$results]);
		}else{
			$response = response()->json(['status'=>'failure', 'message'=>'No data found']);
		}

		return $response;
	}

	public function doer_list(Request $request){
		$Collections = new Collection();
		$Collections->checker_id = $request->input('checker_id');
		$results = $Collections->doer_list($Collections);
		if($results){
			$response = response()->json(['status'=>'success', 'message'=>'doer details', 'data'=>$results]);
		}else{
			$response = response()->json(['status'=>'failure', 'message'=> 'Something went wrong to retrive the data']);
		}
		return $response;

	}
	
	public function checker_list(){
		$Collections = new Collection();
		// $Collections->?
		$results = $Collections->checker_list($Collections);
		if($results){
			$response = response()->json(['status'=>'success', 'message'=>'Checker details', 'data'=>$results]);
		}else{
			$response = response()->json(['status'=>'failure', 'message'=> 'Something went wrong to retrive the data']);
		}
		return $response;

	}

	public function checkerReport(Request $request){

		$Collections = new Collection();
		$Collections->region_id = $request->input('region_id');
		$Collections->start_date = $request->input('start_date');
		$Collections->end_date = $request->input('end_date');
		$Collections->checker_id =$request->input('checker_id');

		if($request->input('doer_id')){
			$Collections->doer_id = $request->input('doer_id');
		}else{
			$Collections->doer_id = '';
		}

		$results = $Collections->checkerReport($Collections);

		if($results){
			$response = response()->json(['status'=>'success', 'message'=>'', 'data'=>$results]);
		}else{
			$response = response()->json(['status'=>'failure', 'message'=> 'Something went wrong to retrive the data']);
		}
		return $response;
	}

	public function financeUserReport(Request $request){

		$Collections = new Collection();
		$Collections->region_id = $request->input('region_id');
		$Collections->start_date = $request->input('start_date');
		$Collections->end_date = $request->input('end_date');
		$Collections->finance_user_id =$request->input('finance_user_id');

		if($request->input('checker_id')){
			$Collections->checker_id = $request->input('checker_id');
		}else{
			$Collections->checker_id = '';
		}

		$results = $Collections->financeUserReport($Collections);

		if($results){
			$response = response()->json(['status'=>'success', 'message'=>'', 'data'=>$results]);
		}else{
			$response = response()->json(['status'=>'failure', 'message'=> 'Something went wrong to retrive the data']);
		}
		return $response;
	}

	public function regionBaseCusOrderDetails(Request $request){
		$Collections = new Collection();
		$Collections->region_id = $request->input('region_id');
		// print_r($Collections->region_id);

		$results = $Collections->regionBaseCusOrderDetails($Collections);
		if($results){
			$response = response()->json(['status'=>'success', 'message'=>'Region based customer order details', 'data'=>$results]);
		}else{
			$response = response()->json(['status'=>'failure', 'message'=> 'Something went wrong to retrive the data']);
		}
		return $response;
	}

	public function financeUserAcceptedAndPendingReport(Request $request){

		$Collections = new Collection();
		// $Collections->region_id = $request->input('region_id');
		$Collections->start_date = $request->input('start_date');
		$Collections->end_date = $request->input('end_date');
		$Collections->type = $request->input('type');

		if($request->input('finance_user_id')){
			$Collections->finance_user_id = $request->input('finance_user_id');
		}else{
			$Collections->finance_user_id = '';
		}

		if($request->input('region_id')){
			$Collections->region_id = $request->input('region_id');
		}else{
			$Collections->region_id = '';
		}

		$results = $Collections->financeUserAcceptedAndPendingReport($Collections);

		if($results){
			$response = response()->json(['status'=>'success', 'message'=>'', 'data'=>$results]);
		}else{
			$response = response()->json(['status'=>'failure', 'message'=> 'Something went wrong to retrive the data']);
		}
		return $response;
	}

	public function financeUserPendingReport(Request $request){

		$Collections = new Collection();

		$Collections->start_date = $request->input('start_date');
		$Collections->end_date = $request->input('end_date');

		if($request->input('finance_user_id')){
			$Collections->finance_user_id = $request->input('finance_user_id');
		}else{
			$Collections->finance_user_id = '';
		}

		$results = $Collections->financeUserPendingReport($Collections);

		if($results){
			$response = response()->json(['status'=>'success', 'message'=>'', 'data'=>$results]);
		}else{
			$response = response()->json(['status'=>'failure', 'message'=> 'Something went wrong to retrive the data']);
		}
		return $response;
	}

	public function getAllFinanceUser(){
		$Collections = new Collection();

		$results = $Collections->getAllFinanceUser();

		if($results){
			$response = response()->json(['status'=>'success', 'message'=>'', 'data'=>$results]);
		}else{
			$response = response()->json(['status'=>'failure', 'message'=> 'Something went wrong to retrive the data']);
		}
		return $response;

	}

	public function collectionPendingReport(Request $request){
		$Collections = new Collection();
		$Collections->type = $request->input('type');
		$Collections->report_type = $request->input('report_type');
		if($request->input('branch_id')){
			$Collections->branch_id = $request->input('branch_id');
		}else{
			$Collections->branch_id = '';
		}
		if($request->input('region_id')){
			$Collections->region_id = $request->input('region_id');
		}else{
			$Collections->region_id = '';
		}
		// $Collections->start_date = $request->input('start_date');
		// $Collections->end_date =$request->input('end_date');

		if($request->input('customer_id')){
			$Collections->customer_id = $request->input('customer_id');
		}else{
			$Collections->customer_id = '';
		}

		if($request->input('order_id')){
			$Collections->order_id = $request->input('order_id');
		}else{
			$Collections->order_id = '';
		}

		if($request->input('start_date')){
			$Collections->start_date = $request->input('start_date');
			$Collections->end_date 	 = $request->input('end_date');
		}else{
			$Collections->start_date = '';
			$Collections->end_date = '';
		}

		// if($request->input('order_status')){
		// 	$Collections->order_status = $request->input('order_status');
		// }else{
		// 	$Collections->order_status = '';
		// }
		if($request->input('warehouse_id')){
			$Collections->warehouse_id = $request->input('warehouse_id');
		}else{
			$Collections->warehouse_id = '';
		}

		if($request->input('payment_status')){
			$Collections->payment_status = $request->input('payment_status');
		}else{
			$Collections->payment_status = '';
		}

		if($request->input('area_id')){
			$Collections->area_id = $request->input('area_id');
		}else{
			$Collections->area_id = '';
		}

		if($request->input('field_officer_id')){
			$Collections->field_officer_id = $request->input('field_officer_id');
		}else{
			$Collections->field_officer_id = '';
		}

		$results = $Collections->collectionPendingReport($Collections);

		if($results){
			$response = response()->json(['status'=>'success', 'messages'=>'Orders details', 'data'=>$results]);
		}else{
			$response = response()->json(['status'=>'failure', 'messages'=>'No data found']);
		}
		return $response;
	}
	//Multiple collections approve
	public function acceptMultipleCollectionDetail(Request $request){

		$Collections = new Collection();
		$Collections->collection_id = $request->input('collection_id');
		$Collections->finance_user_id = $request->input('finance_user_id');
		$Collections->order_id = $request->input('order_id');
		$Collections->collection_for = $request->input('collection_for');

		$results = $Collections->acceptMultipleCollectionDetail($Collections);

		if($results == 1){
			$response = response()->json(['status'=>'success', 'message'=>'Collection accepted', 'data'=>$results]);
		}else{
			$response = response()->json(['status'=>'failure', 'message'=>"Something went wrong"]);
		}
		return $response;
	}

	public function getCustomerCollections(Request $request){
		$Collections = new Collection();
		$Collections->type = $request->input('type');
		$Collections->region_id = $request->input('region_id');

		if($request->input('customer_id')){
			$Collections->customer_id = $request->input('customer_id');
		}else{
			$Collections->customer_id = '';
		}

		if($request->input('order_id')){
			$Collections->order_id = $request->input('order_id');
		}else{
			$Collections->order_id = '';
		}

		if($request->input('start_date')){
			$Collections->start_date = $request->input('start_date');
			$Collections->end_date 	 = $request->input('end_date');
		}else{
			$Collections->start_date = '';
			$Collections->end_date = '';
		}

		if($request->input('area_id')){
			$Collections->area_id = $request->input('area_id');
		}else{
			$Collections->area_id = '';
		}


		$results = $Collections->getCustomerCollections($Collections);

		if($results){
			$response = response()->json(['status'=>'success', 'messages'=>'Orders details', 'data'=>$results]);
		}else{
			$response = response()->json(['status'=>'failure', 'messages'=>'No data found']);
		}
		return $response;
	}

	public function addDoerCollectionDetails(Request $request){
		$Collections = new Collection();

		$Collections->invoice_no 			= $request->input('invoice_no');
		$Collections->order_id	 			= $request->input('order_id');
		$Collections->customer_id 			= $request->input('customer_id');
		$Collections->customer_name 		= $request->input('customer_name');
		$Collections->collection_date 		= $request->input('collection_date');
		$Collections->collection_type		= $request->input('collection_type');
		$Collections->collection_status 	= $request->input('collection_status');
		$Collections->collection_mode 		= $request->input('collection_mode');
		$Collections->collection_amount 	= $request->input('collection_amount');
		$Collections->status 				= $request->input('status');
		$Collections->collection_for		= $request->input('collection_for');

		$Collections->collection_cr_id		= $request->input('collection_cr_id');
		$Collections->collection_cr_name 	= $request->input('collection_cr_name');
		$Collections->total_price 			= $request->input('total_price');
		$Collections->collection_cr_date 	= $request->input('collection_cr_date');
		$Collections->checker_id 	= $request->input('checker_id');
		$Collections->checker_name 	= $request->input('checker_name');
		$Collections->col_type 				= $request->input('col_type');
		$Collections->area_id 				= $request->input('area_id');
		$Collections->region_id 			= $request->input('region_id');
		$Collections->warehouse_id 			= $request->input('warehouse_id');
		// $Collections->doer_id 				= $request->input('doer_id');
		$Collections->doer_name 			= $request->input('doer_name');
		// $Collections->doer_date 			= $request->input('doer_date');
		// $Collections->doer_status 			= $request->input('doer_status');
		// $Collections->approve_status		= $request->input()
		if($request->input('bank_name')){
			$Collections->bank_name = $request->input('bank_name');
		}else{
			$Collections->bank_name = "";
		}
		if($request->input('card_type')){
			$Collections->card_type = $request->input('card_type');
		}else{
			$Collections->card_type = "";
		}
		if($request->input('card_no')){
			$Collections->card_no = $request->input('card_no');
		}else{
			$Collections->card_no = "";
		}
		if($request->input('holder_name')){
			$Collections->holder_name = $request->input('holder_name');
		}else{
			$Collections->holder_name = "";
		}
		if($request->input('account_no')){
			$Collections->account_no = $request->input('account_no');
		}else{
			$Collections->account_no = "";
		}
		if($request->input('transaction_id')){
			$Collections->transaction_id = $request->input('transaction_id');
		}else{
			$Collections->transaction_id = "";
		}
		if($request->input('cheque_no')){
			$Collections->cheque_no = $request->input('cheque_no');
		}else{
			$Collections->cheque_no = "";
		}

		$results = $Collections->addDoerCollectionDetails($Collections);

		if($results == 1){
			$response = response()->json(['status'=>'success', 'message'=>"Collection details stored successfully"]);
		}else{
			$response = response()->json(['status'=>'failure', 'message'=>"Something went wrong"]);
		}
		return $response;
	}

	public function doerCollectionReport(Request $request){
		$Collections = new Collection();
		$Collections->start_date = $request->input('start_date');
		$Collections->end_date = $request->input('end_date');
		if($request->input('collection_for')){
			$Collections->collection_for = $request->input('collection_for');
		}else{
			$Collections->collection_for = '';
		}
		if($request->input('customer_id')){
			$Collections->customer_id = $request->input('customer_id');
		}else{
			$Collections->customer_id = '';
		}
		if($request->input('region_id')){
			$Collections->region_id = $request->input('region_id');
		}else{
			$Collections->region_id = '';
		}
		if($request->input('doer_id')){
			$Collections->doer_id = $request->input('doer_id');
		}else{
			$Collections->doer_id = '';
		}
		if($request->input('checker_status')){
			$Collections->checker_status = $request->input('checker_status');
		}else{
			$Collections->checker_status = '';
		}
		if($request->input('order_id')){
			$Collections->order_id = $request->input('order_id');
		}else{
			$Collections->order_id = '';
		}
		if($request->input('checker_id')){
			$Collections->checker_id = $request->input('checker_id');
		}else{
			$Collections->checker_id = '';
		}

		$results = $Collections->doerCollectionReport($Collections);

		if($results){
			$response = response()->json(['status'=>'success', 'messages'=>'Collection details', 'data'=>$results]);
		}else{
			$response = response()->json(['status'=>'failure', 'messages'=>'No data found']);
		}
		return $response;

	}

	public function financeCollectionReport(Request $request){

		$Collections = new Collection();
		$Collections->start_date = $request->input('start_date');
		$Collections->end_date = $request->input('end_date');
		if($request->input('collection_for')){
			$Collections->collection_for = $request->input('collection_for');
		}else{
			$Collections->collection_for = '';
		}

		if($request->input('finance_user_id')){
			$Collections->finance_user_id = $request->input('finance_user_id');
		}else{
			$Collections->finance_user_id = '';
		}
		if($request->input('status')){
			$Collections->status = $request->input('status');
		}else{
			$Collections->status = '';
		}
		if($request->input('checker_id')){
			$Collections->checker_id = $request->input('checker_id');
		}else{
			$Collections->checker_id = '';
		}

		if($request->input('region_id')){
			$Collections->region_id = $request->input('region_id');
		}else{
			$Collections->region_id = '';
		}
		if($request->input('order_id')){
			$Collections->order_id = $request->input('order_id');
		}else{
			$Collections->order_id = '';
		}

		$results = $Collections->financeCollectionReport($Collections);

		if($results){
			$response = response()->json(['status'=>'success', 'message'=>'', 'data'=>$results]);
		}else{
			$response = response()->json(['status'=>'failure', 'message'=> 'Something went wrong to retrive the data']);
		}
		return $response;
	}
}     