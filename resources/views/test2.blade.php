<!DOCTYPE html>
<html>
<head>
	<title>Invoice</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
	<style>
		body {
			width: 100%;
			padding: 20px;
			display: block;
		}

		h1 {
			font-size: 12px;
			line-height: 3px;
		}
		h1 {
			font-size: 12px;
			line-height: 3px;
		}

		p {
			font-size: 11px;
			line-height: 20px;
		}
		
		tr {
			font-size: 12px;
			line-height: 35px;
		}
		#second-table {
			text-align: center;
			border-collapse: collapse;
			width: 100%;
		}
		#table-a{
			text-align: left;
		}
		#table-b{
			text-align: left;
		}
		h6{
			font-size: 11px;
		}
		#table-c{
			text-align: center;
		}
		#table-d{
			text-align: left;
		}
		#second-table {
			border: 1px solid black;
 			border-collapse: collapse;
		}
		#nd-rows, #nd-rows > th, #nd-rows > td{
			border: 1px solid black;
			text-align: center;
		}
		#table-a > p{
			font-size: 11px;
		}
		#table-ab{
			font-size: 12px;
		}


	</style>
</head>
<body>
<center>
    <h3><b>{{$com_details->branch_name}}</b></h3>
	<p>{{$com_details->branch_address}}<br/> 
	   {{$com_details->branch_country}}.<b>PH: +</b>{{$com_details->country_code}} {{$com_details->branch_contact}}</p>
		<p></p>
	<h4><b>Sales Voucher</b></h4>  <br>
</center>
	<div class="row">
		<div class="col-md-12">
			<table style="width:100%" id="table-ab" class="col-md-12">
				<tr>
						<th >Customer name:</th>
						<td>{{$cus_de->customer_name}}</td>
						<th align="right">Invoice no:</th>
						<td>{{$invoice}}</td>
				</tr>
				<tr>
						<th><b>Address: </b></th>
						<td>{{$cus_de->customer_address}}</td>
						<th align="right">Invoice date:</th>
						<td>{{$date}}</td>
				</tr>
				<tr>
						<th>Location:</th>
						<td>{{$cus_de->customer_location}}</td>
						<th align="right">Customer Id:</th>
						<td>{{$cus_de->customer_id}}</td>
				</tr>
			</table>
		</div>
	</div>
	<center>
		<table id = "second-table" class="col-md-12" style="width:100%">
			<tr id="nd-rows">
				<th>S.No</th>
				<th>Product Name</th>
				<th>Scheme</th>
				<th>Batch.No</th>
				<th>Net_amt</th>
				<th>Gross_amt</th>
				<th>Qty</th>
				<th>Exp.Date</th>
				<th>FOC</th>
				<th>FOC Amt</th>
				<th>Spl.FOC</th>
				<th>Discount</th>
				<th>Amount</th>
			</tr>
			@foreach($datas  as $key=> $data_details)
			<tr id="nd-rows">
				<td>{{ ++$key }}</td>
				<td>{{  $data_details->product_genericname }}</td>
				<td>{{  $data_details->scheme }}</td>
				<td>{{  $data_details->batch_no }}</td>
				<td>{{  $data_details->product_netprice }}</td>
				<td>{{  $data_details->product_grossprice }}</td>
				<td>{{  $data_details->product_qty }}</td>
				<td>{{  $data_details->batch_emp_date }}</td>
				<td>{{  $data_details->FOC }}</td>
				<td>{{  $data_details->FOC_amt }}</td>
				<td>{{  $data_details->spl_FOC }}</td>
				<td>{{  $data_details->product_discount }}</td>
				<td>{{  $data_details->product_tot_price }}</td>
			</tr>
			$key++;
			@endforeach
		</table>

		<div class="row">
			<div class="col-md-12">
				<table style="width:100%" id="table-ab" class="col-md-6">
					<tr>
						<th>Total box:</th>
						<td>0</td>
						<th align="right">Net value:</th>
						<td>{{$order_details->or_gross_total}}</td>
					</tr>
					<tr>
						<th><b>Cash: </b></th>
						<td>{{$order_details->or_tot_price}}Kyat</td>
						<th align="right">Gross value:</th>
						<td>{{$order_details->or_gross_total}}</td>
					</tr>
					<tr>
						<th></th>
						<td>{{$value}}</td>
						<th align="right">Discount:</th>
						<td>{{$order_details->order_discount}}</td>
					</tr>
					<tr>
						<th>Due Date:</th>
						<td>-</td>
						<th align="right">Spl.Discount:</th>
						<td>{{$order_details->spl_discount}}</td>
					</tr>
					<tr>
						<th></th>
						<td></td>
						<th align="right">Balance:</th>
						<td>{{$order_details->or_tot_price}}</td>
					</tr>
					<tr>
						<th></th>
						<td></td>
						<th align="right">Bill amount:</th>
						<td>{{$order_details->or_tot_price}}</td>
					</tr>
				</table>
			</div>
		</div>
	</center>
	<center>
		<div class="row">
			<div class="col-md-12">
				<p align="left">Received the above goods is good order & conditions</p><br>
				<table style="width:100%" id="table-ab" class="col-md-6">
					<tr>
						<th >This is computer generated bill.No need of signature.</th>
						<td></td>
						<th align="right"></th>
						<td><b>Signature & Stamp of Buyer</b></td>
					</tr>
					<tr>
						<th >မှတ်ချက်-</th>
						<td>ဘောက်ချာအဝါပါမှသာလျှင်ငွေရှင်းပေးပါရန်။</td>
						<th align="right"></th>
						<td></td>
					</tr>
					<tr>
						<th ></th>
						<td>ငွေရှင်းရက်ပြည့်သည့်နေတွင်ငွေရှင်းပေးပါရန်မေတ္တာရပ်ခံပါသည်။</td>
						<th align="right"></th>
						<td></td>
					</tr>
					<tr>
						<th></th>
						<td></td>
						<th></th>
						<td></td>
					</tr>
				</table>
			</div>
		</div>
	</center>
</body>
</html>