<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Carbon\Carbon;
use DB;
use Mail;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
        '\App\Console\Commands\Email',
        '\App\Console\Commands\Invoice',
        '\App\Console\Commands\TargetExpire',
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')->hourly();
        //Send the email to users
        //  $schedule->command('EmailSend:Notification')
        //  ->everyMinute()
        //  -> appendOutputTo (storage_path().'/logs/laravel_output.log'); 
        // //Generate order invoices
        //  $schedule->command('Invoice:Generate')
        //  ->everyMinute()
        //  -> appendOutputTo (storage_path().'/logs/invoice_output.log'); 

        //  //Send the email to users
        //  $schedule->command('Target:Expired')
        //  ->everyMinute()
        //  //->dailyAt('10:00')
        //  -> appendOutputTo (storage_path().'/logs/tarexp_output.log');
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
