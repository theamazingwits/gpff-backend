<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;

require(base_path().'/app/Http/Middleware/Tracking.php');

class TrackingController extends Controller
{
   ////////////////////////////////
//Tracking Management Api Calls//
////////////////////////////////
	public function taskLiveTracking(Request $request)
	{	
		$Tracking = new Tracking();
        $Tracking->livelat  = $request->input('livelat');
        $Tracking->livelang  = $request->input('livelang');
        $Tracking->task_id  = $request->input('task_id');
        $Tracking->field_officer_id  = $request->input('field_officer_id');

        $result = $Tracking->taskLiveTracking($Tracking);
        if($result == 1){
            $response = response()->json(['status' => 'success' , 'message' => 'Field Officer Reached the Customer Destination']);
        } else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Not Reached']);
        }
        return $response;
	}

}
