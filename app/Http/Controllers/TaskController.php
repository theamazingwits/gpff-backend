<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use File;

require(base_path().'/app/Http/Middleware/Task.php');

class TaskController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
//Task API Calls
    //Add Task
    public function addTask(Request $request){
        
        //Task Details
        $Tasks = new Task();
        $Tasks->created_by    = $request->input('created_by');
        $Tasks->field_officer_id     = $request->input('field_officer_id');
        $Tasks->area_manager_id    = $request->input('area_manager_id');
        $Tasks->region_manager_id  = $request->input('region_manager_id');
        $Tasks->region_id       = $request->input('region_id');
        $Tasks->branch_id       = $request->input('branch_id');
        $Tasks->area_id       = $request->input('area_id');
        $Tasks->customer_id     = $request->input('customer_id');
        $Tasks->customer_type     = $request->input('customer_type');
        $Tasks->task_title      = $request->input('task_title');
        $Tasks->task_desc     = $request->input('task_desc');
        $Tasks->task_date_time      = $request->input('task_date_time');   
       
        $result = $Tasks->storeTask($Tasks);
        if($result){
            $response = response()->json(['status' => 'success' , 'message' => 'Task added successfully']);
        } else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Task creation failed']);
        }

        return $response;

    }

         //Add multi Task
         public function addmultiTask(Request $request){
            //Task Details
            $Tasks = new Task();
            $Tasks->created_by    = $request->input('created_by');
            $Tasks->field_officer_id     = $request->input('field_officer_id');
            $Tasks->area_manager_id    = $request->input('area_manager_id');
            $Tasks->region_manager_id  = $request->input('region_manager_id');
            $Tasks->region_id       = $request->input('region_id');
            $Tasks->branch_id       = $request->input('branch_id');
            $Tasks->area_id       = $request->input('area_id');
            $Tasks->customer_list       = $request->input('customer_list'); 
           
            $result = $Tasks->storemultitask($Tasks);
            if($result){
                $response = response()->json(['status' => 'success' , 'message' => 'Task added successfully']);
            } else{
                $response = response()->json(['status' => 'failure' , 'message' => 'Task creation failed']);
            }
    
            return $response;
    
        }

    //update Task
    public function updateTask(Request $request){
        
        //Task Details
        $Tasks = new Task();
        $Tasks->task_id    = $request->input('task_id');
        $Tasks->created_by    = $request->input('created_by');
        $Tasks->field_officer_id     = $request->input('field_officer_id');
        $Tasks->area_manager_id    = $request->input('area_manager_id');
        $Tasks->region_id       = $request->input('region_id');
        $Tasks->branch_id       = $request->input('branch_id');
        $Tasks->area_id       = $request->input('area_id');
        $Tasks->customer_id     = $request->input('customer_id');
        $Tasks->customer_type     = $request->input('customer_type');
        $Tasks->task_title      = $request->input('task_title');
        $Tasks->task_desc     = $request->input('task_desc');
        $Tasks->task_date_time      = $request->input('task_date_time');   
       
        $result = $Tasks->updateTask($Tasks);
        if($result){
            $response = response()->json(['status' => 'success' , 'message' => 'Task updated successfully']);
        } else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Task updation failed']);
        }

        return $response;
    }

    //update Task Status
    public function updateTaskStatus(Request $request){

        //Task Details
        $Tasks = new Task();
        $Tasks->task_id    = $request->input('task_id');
        $Tasks->task_status     = $request->input('task_status');

        $result = $Tasks->updateTaskStatus($Tasks);
        if($result){
            $response = response()->json(['status' => 'success' , 'message' => 'Task status updated']);
        } else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Task status updation failed']);
        }

        return $response;
        
    }

    //Reassign task
    public function reassignTask(Request $request){

        //Task Reassign Details
        $Tasks = new Task();
        $Tasks->task_id    = $request->input('task_id');
        $Tasks->created_by    = $request->input('created_by');
        $Tasks->task_reassign_field_officer_id     = $request->input('task_reassign_field_officer_id');
        $Tasks->task_reassign_status     = '1';
        
        $result = $Tasks->reassignTask($Tasks);
        if($result == 1){
            $response = response()->json(['status' => 'success' , 'message' => 'Task reassigned successfully']);
        } else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Task reassign request failed']);
        }

        return $response;
    }

    //Remove Task
    public function removeTask(Request $request){

        //Task Reassign Details
        $Tasks = new Task();
        $Tasks->task_id    = $request->input('task_id');
       
        $result = $Tasks->removeTask($Tasks);
        if($result){
            $response = response()->json(['status' => 'success' , 'message' => 'Task removed successfully']);
        } else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Task removed failed']);
        }

        return $response;
    }

    //Task notes
    public function addNotes(Request $request){

         //Task Notes Details
        $Tasks = new Task();
        $Tasks->task_id    = $request->input('task_id');
        $Tasks->notes    = $request->input('notes');
       
        $result = $Tasks->addNotes($Tasks);
        if($result){
            $response = response()->json(['status' => 'success' , 'message' => 'Task notes added successfully']);
        } else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Task notes added failed']);
        }

        return $response;
    }

    //Admin Fetch Task All and current Date
    public function fetchAdminTaskList(Request $request){

        $Tasks = new Task();
        $Tasks->type = $request->input('type');

        $result = $Tasks->fetchAdminTaskList($Tasks);
        if($result == '2'){
            $response = response()->json(['status' => 'failure' , 'message' => 'Error to fetch data! invaild parameters']);
        } else if($result){
            $response = response()->json(['status' => 'success' , 'message' => '' ,'data' => $result]);
        }else{
            $response = response()->json(['status' => 'failure' , 'message' =>  '' ,'data' => $result]);
        }
        return $response;
    }

    //Area Manager Fetch Task All and current Date
    public function fetchAreaManageTaskList(Request $request){

        $Tasks = new Task();
        $Tasks->type = $request->input('type');
        $Tasks->user_id = $request->input('user_id');

        $result = $Tasks->fetchAreaManageTaskList($Tasks);
        if($result == '2'){
            $response = response()->json(['status' => 'failure' , 'message' => 'Error to fetch data! invaild parameters']);
        } else if($result){
            $response = response()->json(['status' => 'success' , 'message' => '' ,'data' => $result]);
        }else{
            $response = response()->json(['status' => 'failure' , 'message' =>  '' ,'data' => $result]);
        }
        return $response;
    }

    //Fetch Task Based on Id and current Date
    public function fetchTaskList(Request $request){
        
        //Task Reassign Details
        $Tasks = new Task();
        $Tasks->field_officer_id    = $request->input('field_officer_id');
        $Tasks->date    = $request->input('date');
        $Tasks->task_status = $request->input('task_status');
        $Tasks->type = $request->input('type');

        $result = $Tasks->fetchTaskList($Tasks);
        if($result == '2'){
            $response = response()->json(['status' => 'failure' , 'message' => 'Error to fetch data! invaild parameters']);
        } else if($result){
            $response = response()->json(['status' => 'success' , 'message' => '' ,'data' => $result]);
        }else{
            $response = response()->json(['status' => 'failure' , 'message' =>  '' ,'data' => $result]);
        }
        return $response;
    }

    //Fetch Area Based Task and current Date
    public function fetchAreaBaseTaskList(Request $request){
        
        $Tasks = new Task();
        //$Tasks->area_manager_id    = $request->input('area_manager_id');
        $Tasks->area_id    = $request->input('area_id');
        $Tasks->date    = $request->input('date');
        $Tasks->type = $request->input('type');

        $result = $Tasks->fetchAreaBaseTaskList($Tasks);
        if($result == '2'){
            $response = response()->json(['status' => 'failure' , 'message' => 'Error to fetch data! invaild parameters']);
        } else if($result){
            $response = response()->json(['status' => 'success' , 'message' => '' ,'data' => $result]);
        }else{
            $response = response()->json(['status' => 'failure' , 'message' =>  '' ,'data' => $result]);
        }
        return $response;
    }

    //Fetch Region Based Task and current Date
    public function fetchRegionBaseTaskList(Request $request){
        
        $Tasks = new Task();
        $Tasks->region_id    = $request->input('region_id');
        $Tasks->date    = $request->input('date');
        $Tasks->type = $request->input('type');

        $result = $Tasks->fetchRegionBaseTaskList($Tasks);
        if($result == '2'){
            $response = response()->json(['status' => 'failure' , 'message' => 'Error to fetch data! invaild parameters']);
        } else if($result){
            $response = response()->json(['status' => 'success' , 'message' => '' ,'data' => $result]);
        }else{
            $response = response()->json(['status' => 'failure' , 'message' =>  '' ,'data' => $result]);
        }
        return $response;
    }

    //Fetch Ind Task Details
    public function fetchIndTaskDetails(Request $request){
        
        //Task Reassign Details
        $Tasks = new Task();
        $Tasks->task_id    = $request->input('task_id');

        $result = $Tasks->fetchIndTaskDetails($Tasks);
        if($result){
            $response = response()->json(['status' => 'success' , 'message' => '' ,'data' => $result]);
        }else{
            $response = response()->json(['status' => 'failure' , 'message' =>  '' ,'data' => $result]);
        }
        return $response;
    }

    //Fetch Ind Task All Details Fetch
    public function fetchIndTaskAllDetails(Request $request){
        
        //Task Reassign Details
        $Tasks = new Task();
        $Tasks->task_id    = $request->input('task_id');

        $result = $Tasks->fetchIndTaskAllDetails($Tasks);
        if($result){
            $response = response()->json(['status' => 'success' , 'message' => '' ,'data' => $result]);
        }else{
            $response = response()->json(['status' => 'failure' , 'message' =>  '' ,'data' => $result]);
        }
        return $response;
    }

    //Field Officer Side Fetch Customer Based All Task and Task Based All Order or Feedback or Gift or Revisit Details Fetch
    public function getFieldTaskAllDetails(Request $request){
        
        //Task Reassign Details
        $Tasks = new Task();
        $Tasks->field_officer_id    = $request->input('field_officer_id');
        $Tasks->customer_id    = $request->input('customer_id');

        $result = $Tasks->getFieldTaskAllDetails($Tasks);
        if($result == 2){
            $response = response()->json(['status' => 'success' , 'message' => 'No Data' ,'data' => $result]);
        }else if($result){
            $response = response()->json(['status' => 'success' , 'message' => '' ,'data' => $result]);
        }else{
            $response = response()->json(['status' => 'failure' , 'message' =>  '' ,'data' => $result]);
        }
        return $response;
    }
   
}
