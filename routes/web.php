<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/purchasedproducts', function () {
    return view('purchasedproducts');
});

//Login Process
////////////////////////////
//User Managment Api Calls//
////////////////////////////
Route::post('/addUser', 'UserController@addUser');
Route::post('/updateUsers', 'UserController@updateUsers');
Route::post('/userProfileUpdate', 'UserController@userProfileUpdate');
Route::post('/deleteUsers', 'UserController@deleteUsers');
Route::post('/userBasedPriceUpdate', 'UserController@userBasedPriceUpdate');
Route::get('/getIndUsers/{user_id}', 'UserController@getIndUsers');
Route::get('/getRoleBasedUsers/{role}', 'UserController@getRoleBasedUsers');
Route::post('/getManagerBasedUserDetails', 'UserController@getManagerBasedUserDetails');
Route::get('/getAllUsers', 'UserController@getAllUsers');
Route::post('/userActiveOrDeactive', 'UserController@userActiveOrDeactive');
Route::post('/userChangePassword', 'UserController@userChangePassword');
Route::post('/userForgetPassword', 'UserController@userForgetPassword');
Route::post('/login', 'UserController@login');
Route::post('/logout' , 'UserController@logout');
Route::post('/addCustomers', 'UserController@addCustomers');
Route::post('/addAllCustomers', 'UserController@addAllCustomers');
Route::post('/updateCustomers', 'UserController@updateCustomers');
Route::post('/deleteCustomers', 'UserController@deleteCustomers');
Route::get('/getIndCustomers/{customer_id}', 'UserController@getIndCustomers');
Route::get('/getEmpBaseCustomers/{customer_id}', 'UserController@getEmpBaseCustomers');
Route::get('/getAllCustomers', 'UserController@getAllCustomers');
Route::post('/getUserBasedCustomers', 'UserController@getUserBasedCustomers');
Route::post('/customerActiveOrDeactive', 'UserController@customerActiveOrDeactive');
Route::post('/getBranchBasRegManage', 'UserController@getBranchBasRegManage');
Route::post('/getBranchBaseFO', 'UserController@getBranchBaseFO');
Route::post('/getRegionBasAreaManage', 'UserController@getRegionBasAreaManage');
Route::post('/getRegionBasCustomer', 'UserController@getRegionBasCustomer');
Route::post('/getAreaBasCustomer', 'UserController@getAreaBasCustomer');
Route::post('/getMultiAreaBasCustomer', 'UserController@getMultiAreaBasCustomer');
Route::get('/getUserBasedDetails/{user_id}', 'UserController@getUserBasedDetails');
Route::get('/getWarehBasedDetails/{warehouse_id}', 'UserController@getWarehBasedDetails');
/////////////////////////////
//Task Management Api Calls//
/////////////////////////////
Route::post('/addTask', 'TaskController@addTask');
Route::post('/addmultiTask', 'TaskController@addmultiTask');
Route::post('/updateTask', 'TaskController@updateTask');
Route::post('/updateTaskStatus', 'TaskController@updateTaskStatus');
Route::post('/reassignTask', 'TaskController@reassignTask');
Route::post('/removeTask', 'TaskController@removeTask');
Route::post('/fetchAdminTaskList', 'TaskController@fetchAdminTaskList');
Route::post('/fetchAreaManageTaskList', 'TaskController@fetchAreaManageTaskList');
Route::post('/addNotes', 'TaskController@addNotes');
Route::post('/fetchTaskList', 'TaskController@fetchTaskList');
Route::post('/fetchAreaBaseTaskList', 'TaskController@fetchAreaBaseTaskList');
Route::post('/fetchRegionBaseTaskList', 'TaskController@fetchRegionBaseTaskList');
Route::post('/fetchIndTaskDetails', 'TaskController@fetchIndTaskDetails');
Route::post('/fetchIndTaskAllDetails', 'TaskController@fetchIndTaskAllDetails');
Route::post('/getFieldTaskAllDetails', 'TaskController@getFieldTaskAllDetails');
//////////////////////////////
//Leave Management Api Calls//
//////////////////////////////
Route::post('/addLeave', 'LeaveController@addLeave');
Route::post('/leaveAcceptOrReject', 'LeaveController@leaveAcceptOrReject');
Route::post('/getManagerBasedLeaveRequest', 'LeaveController@getManagerBasedLeaveRequest');
Route::get('/getUserLeaveHistory/{user_id}', 'LeaveController@getUserLeaveHistory');
Route::post('/addAttendance', 'LeaveController@addAttendance');
///////////////////////////////////////
//Gift Inventory Management Api Calls//
///////////////////////////////////////
Route::post('/addGift', 'GiftController@addGift');
Route::post('/updateGift', 'GiftController@updateGift');
Route::post('/removeGift', 'GiftController@removeGift');
Route::get('/getIndGift/{gift_id}', 'GiftController@getIndGift');
Route::get('/getAllGift', 'GiftController@getAllGift');
Route::get('/getRegionBasedGift/{region_id}', 'GiftController@getRegionBasedGift');

Route::post('/addGiftQty', 'GiftController@addGiftQty');
Route::post('/updateGiftQty', 'GiftController@updateGiftQty');
Route::post('/getWarehouseBasedGiftStock', 'GiftController@getWarehouseBasedGiftStock');
////////////////////////////////////
//Gift Assign Management Api Calls//
////////////////////////////////////
Route::post('/giftAssign', 'GiftController@giftAssign');
Route::post('/giveGifttoFO', 'GiftController@giveGifttoFO');
Route::post('/getReceivedGiftDet', 'GiftController@getReceivedGiftDet');
Route::post('/getMyAssignGifts', 'GiftController@getMyAssignGifts');
Route::post('/getManagerAssignGifts', 'GiftController@getManagerAssignGifts');
Route::get('/getAllAssignGifts', 'GiftController@getAllAssignGifts');
Route::post('/giftToCustomer', 'GiftController@giftToCustomer');
Route::post('/getFOGift', 'GiftController@getFOGift');

Route::post('/getRegionManageAssignGifts', 'GiftController@getRegionManageAssignGifts');
////////////////////////////////
//Product Management Api Calls//
////////////////////////////////
//Category
Route::post('/addCategory', 'ProductController@addCategory');
Route::post('/updateCategory', 'ProductController@updateCategory');
Route::post('/removeCategory', 'ProductController@removeCategory');
Route::post('/categoryActiveOrDeactive', 'ProductController@categoryActiveOrDeactive');
Route::get('/getAllCategory', 'ProductController@getAllCategory');
Route::get('/getRegionBasedCategory/{region_id}', 'ProductController@getRegionBasedCategory');
Route::get('/getRegionBasedProducts/{region_id}', 'ProductController@getRegionBasedProducts');

Route::post('/getRegionandTypeBasedProducts', 'ProductController@getRegionandTypeBasedProducts');
//Product
Route::post('/addProduct', 'ProductController@addProduct');
Route::post('/updateProduct', 'ProductController@updateProduct');
Route::post('/removeProduct', 'ProductController@removeProduct');
Route::post('/getIndProduct', 'ProductController@getIndProduct');
Route::post('/productActiveOrDeactive', 'ProductController@productActiveOrDeactive');
Route::get('/getAllProduct', 'ProductController@getAllProduct');
Route::post('/getAllProductPagi', 'ProductController@getAllProductPagi');

Route::post('/addGoodsRecivedStock', 'ProductController@addGoodsRecivedStock');
Route::post('/updateProductsBatchExp', 'ProductController@updateProductsBatchExp');
Route::post('/getGRHistory', 'ProductController@getGRHistory');
Route::get('/getIndGRBatch/{region_id}', 'ProductController@getIndGRBatch');
Route::post('/getWarehouseBasedProductStock', 'ProductController@getWarehouseBasedProductStock');
Route::post('/getWarehouseBasedProductBatch', 'ProductController@getWarehouseBasedProductBatch');
Route::post('/getWarehouseBasedProductStockList', 'ProductController@getWarehouseBasedProductStockList');
Route::post('/getMultipleWarehouseBasedProductStockList', 'ProductController@getMultipleWarehouseBasedProductStockList');
Route::post('/getBatchBasedQty', 'ProductController@getBatchBasedQty');
Route::post('/getIndProductQty', 'ProductController@getIndProductQty');
Route::post('/getIndProductsQtyDetails', 'ProductController@getIndProductsQtyDetails');

Route::post('/getCategoryBasedProduct', 'ProductController@getCategoryBasedProduct');
Route::post('/getCategoryBasedProductPagi', 'ProductController@getCategoryBasedProductPagi');

Route::post('/getRegionBasedField', 'ProductController@getRegionBasedField');
Route::post('/getRegionBasedAreaManager', 'ProductController@getRegionBasedAreaManager');
//Assign Product
Route::post('/productAssign', 'ProductController@productAssign');
Route::post('/getfieldBasedAssignProduct', 'ProductController@getfieldBasedAssignProduct');
Route::post('/getFOCateBasedAssignProduct', 'ProductController@getFOCateBasedAssignProduct');
Route::get('/getAllAssignProduct', 'ProductController@getAllAssignProduct');

Route::get('/getAreaManageAssignProduct', 'ProductController@getAreaManageAssignProduct');
Route::get('/getRegionManageAssignProduct', 'ProductController@getRegionManageAssignProduct');

//Stock Request
Route::post('/makeRequest' , 'ProductController@makeRequest');
Route::post('/getStockRequest' , 'ProductController@getStockRequest');
Route::get('/getStockRequestList/{product_request_id}', 'ProductController@getStockRequestList');
Route::get('/getStockRequestAcceptList/{product_request_list_id}', 'ProductController@getStockRequestAcceptList');
Route::get('/getStockRequestBaseAcceptList/{product_request_id}', 'ProductController@getStockRequestBaseAcceptList');
Route::get('/getStockRequestListInd/{product_request_id}/{product_id}', 'ProductController@getStockRequestListInd');
Route::post('/stockRequestAcceptOrReject' , 'ProductController@stockRequestAcceptOrReject');
Route::post('/stockAcceptAcknowledge' , 'ProductController@stockAcceptAcknowledge');
Route::post('/warehouseStockCompare' , 'ProductController@warehouseStockCompare');


////////////////////////////////////////////
//Normal Notification Management Api Calls//
///////////////////////////////////////////
Route::post('/getNotification' , 'UserController@getNotification');
Route::get('/getAppNotification/{notification_user_id}' , 'UserController@getAppNotification');
Route::post('/notificationStatusChange' , 'UserController@notificationStatusChange');
Route::post('/deleteNotification' , 'UserController@deleteNotification');
Route::post('/deleteAllNotification' , 'UserController@deleteAllNotification');
Route::get('/getIndBasedNotiDetails/{notification_id}' , 'UserController@getIndBasedNotiDetails');

//////////////////////////////
//Order Management Api Calls//
//////////////////////////////
Route::post('/addOrderDetails' , 'OrderController@addOrderDetails');
Route::post('/getFieldOfficerOrder' , 'OrderController@getFieldOfficerOrder');
Route::post('/getOrderBasedList' , 'OrderController@getOrderBasedList');
Route::post('/getOrderBasedBatchList' , 'OrderController@getOrderBasedBatchList');

Route::post('/getOrderBasedEventsList' , 'OrderController@getOrderBasedEventsList');
Route::post('/getManagerOrder' , 'OrderController@getManagerOrder');
Route::post('/getManagerOrderBasedList' , 'OrderController@getManagerOrderBasedList');
Route::get('/getAllOrder', 'OrderController@getAllOrder');
Route::post('/getAllOrderDashboard', 'OrderController@getAllOrderDashboard');
Route::post('/getWarAllOrder' , 'OrderController@getWarAllOrder');
Route::post('/getBranchAllOrder' , 'OrderController@getBranchAllOrder');
Route::post('/updateOrderStatus' , 'OrderController@updateOrderStatus');
Route::post('/getPlacedOrder' , 'OrderController@getPlacedOrder');
Route::post('/getwarehouseOrder' , 'OrderController@getwarehouseOrder');
Route::post('/cusOverlimitOrderReq' , 'OrderController@cusOverlimitOrderReq');
Route::get('/getLimitOverData' , 'OrderController@getLimitOverData');
Route::post('/getHistoryLimitOverData' , 'OrderController@getHistoryLimitOverData');
Route::post('/order/Accept' , 'OrderController@orderAccept'); //Mani order accept

Route::get('/getApprovalOrderData' , 'OrderController@getApprovalOrderData');
Route::post('/orderFOCDiscountAccept' , 'OrderController@orderFOCDiscountAccept');
//////////////////////////////////////////
//FeedBack & Review Management Api Calls//
/////////////////////////////////////////
Route::post('/addFeedBack' , 'FeedAndReviewController@addFeedBack');
////////////////////
//Revist Api Calls//
////////////////////
Route::post('/addRevisit' , 'FeedAndReviewController@addRevisit');
//////////////////////
//Chating Api Calls//
/////////////////////
Route::post('/getAdminChatDetails' , 'ChatController@getAdminChatDetails');
Route::post('/getBranchAdminChatDetails' , 'ChatController@getBranchAdminChatDetails');
Route::post('/getRegionManagerChatDetails' , 'ChatController@getRegionManagerChatDetails');
Route::post('/getManagerChatDetails' , 'ChatController@getManagerChatDetails');
Route::post('/getFieldOfficerChatDetails' , 'ChatController@getFieldOfficerChatDetails');
Route::post('/getStockistChatDetails' , 'ChatController@getStockistChatDetails');
Route::post('/getCustomerChatDetails' , 'ChatController@getCustomerChatDetails');

///////////////////////
//Dashboard Api Calls//
//////////////////////
Route::post('/getAdminDashCount' , 'DashboardController@getAdminDashCount');
Route::post('/getAreaDashCount' , 'DashboardController@getAreaDashCount');
Route::post('/getRegionDashCount' , 'DashboardController@getRegionDashCount');
Route::post('/getBranchDashCount' , 'DashboardController@getBranchDashCount');
Route::post('/getStockistDashCount' , 'DashboardController@getStockistDashCount');


Route::post('/getDashTaskCount' , 'DashboardController@getDashTaskCount');
Route::post('/getAdminDashGraph' , 'DashboardController@getAdminDashGraph');
Route::post('/getBranchDashTaskCount' , 'DashboardController@getBranchDashTaskCount');
Route::post('/getBranchDashGraph' , 'DashboardController@getBranchDashGraph');
Route::post('/getRegionDashTaskCount' , 'DashboardController@getRegionDashTaskCount');
Route::post('/getRegionDashGraph' , 'DashboardController@getRegionDashGraph');
Route::post('/getAreaDashTaskCount' , 'DashboardController@getAreaDashTaskCount');
Route::post('/getManagDashCount' , 'DashboardController@getManagDashCount');
Route::post('/getManagDashGraph' , 'DashboardController@getManagDashGraph');

//////////////////////////////////////////
//Samples Inventory Management Api Calls//
//////////////////////////////////////////
Route::post('/addSamples', 'SamplesController@addSamples');
Route::post('/updateSamplesQty', 'SamplesController@updateSamplesQty');
Route::post('/getWarehouseBasedSampleStock', 'SamplesController@getWarehouseBasedSampleStock');

Route::post('/updateSamples', 'SamplesController@updateSamples');
Route::post('/removeSamples', 'SamplesController@removeSamples');
Route::get('/getIndSamples/{samples_id}', 'SamplesController@getIndSamples');
Route::get('/getAllSamples', 'SamplesController@getAllSamples');
///////////////////////////////////////
//Samples Assign Management Api Calls//
///////////////////////////////////////
Route::post('/samplesAssign', 'SamplesController@samplesAssign');
Route::post('/getMyAssignSamples', 'SamplesController@getMyAssignSamples');
Route::post('/sampleToCustomer', 'SamplesController@sampleToCustomer');
Route::post('/getManagerAssignSamples', 'SamplesController@getManagerAssignSamples');
Route::get('/getAllAssignSamples', 'SamplesController@getAllAssignSamples');
Route::get('/getRegionBasedSamples/{region_id}', 'SamplesController@getRegionBasedSamples');
Route::post('/getReceivedSampleDet', 'SamplesController@getReceivedSampleDet');
Route::post('/giveSampletoFO', 'SamplesController@giveSampletoFO');
Route::post('/getFOSamples', 'SamplesController@getFOSamples');

Route::post('/getRegionManageAssignSamples', 'SamplesController@getRegionManageAssignSamples');
////////////////////////////////
//History Management Api Calls//
////////////////////////////////
Route::post('/getStockAddHistory', 'HistoryController@getStockAddHistory');
Route::post('/getStockAssignHistory', 'HistoryController@getStockAssignHistory');
////////////////////////////
//File Managment Api Calls//
////////////////////////////
Route::post('/addFolder', 'FileController@addFolder');
Route::post('/updateFolder', 'FileController@updateFolder');
Route::post('/deleteFolder', 'FileController@deleteFolder');
Route::post('/addFile', 'FileController@addFile');
Route::post('/updateFile', 'FileController@updateFile');
Route::post('/getFolderFiles', 'FileController@getFolderFiles');
Route::post('/getAllFiles', 'FileController@getAllFiles');
////////////////////////////
//Branch Managment Api Calls//
////////////////////////////
Route::post('/addBranch', 'BranchController@addBranch');
Route::post('/updateBranch', 'BranchController@updateBranch');
Route::post('/deleteBranch', 'BranchController@deleteBranch');
Route::post('/branchActiveOrDeactive', 'BranchController@branchActiveOrDeactive');
Route::get('/getAllBranch', 'BranchController@getAllBranch');
Route::get('/getIndBranch/{branch_id}', 'BranchController@getIndBranch');
Route::get('/getIdBasedBranch/{manager_id}', 'BranchController@getIdBasedBranch');
////////////////////////////
//Region Managment Api Calls//
////////////////////////////
Route::post('/addRegion', 'BranchController@addRegion');
Route::post('/updateRegion', 'BranchController@updateRegion');
Route::post('/deleteRegion', 'BranchController@deleteRegion');
Route::post('/regionActiveOrDeactive', 'BranchController@regionActiveOrDeactive');
Route::get('/getAllRegion', 'BranchController@getAllRegion');
Route::get('/getIndRegion/{region_id}', 'BranchController@getIndRegion');
Route::post('/getBranchBaseReg', 'BranchController@getBranchBaseReg');
Route::get('/getIdBasedRegion/{manager_id}', 'BranchController@getIdBasedRegion');
////////////////////////////
//Area Managment Api Calls//
////////////////////////////
Route::post('/addArea', 'BranchController@addArea');
Route::post('/updateArea', 'BranchController@updateArea');
Route::post('/deleteArea', 'BranchController@deleteArea');
Route::post('/areaActiveOrDeactive', 'BranchController@areaActiveOrDeactive');
Route::get('/getAllArea', 'BranchController@getAllArea');
Route::get('/getIndArea/{area_id}', 'BranchController@getIndArea');
Route::post('/getRegionBasArea', 'BranchController@getRegionBasArea');
Route::get('/getIdBasedArea/{manager_id}', 'BranchController@getIdBasedArea');
Route::post('/getAreaBasedField', 'BranchController@getAreaBasedField');
Route::post('/getMultiAreaBasedField', 'BranchController@getMultiAreaBasedField');
Route::post('/getAreabasFO', 'BranchController@getAreabasFO');
////////////////////////////
//Warehouse Managment Api Calls//
////////////////////////////
Route::post('/addWarehouse', 'BranchController@addWarehouse');
Route::post('/updateWarehouse', 'BranchController@updateWarehouse');
Route::post('/deleteWarehouse', 'BranchController@deleteWarehouse');
Route::post('/warehouseActiveOrDeactive', 'BranchController@warehouseActiveOrDeactive');
Route::get('/getAllWarehouse', 'BranchController@getAllWarehouse');
Route::get('/getIndWarehouse/{warehouse_id}', 'BranchController@getIndWarehouse');
Route::get('/getMainBaseSubWarehouse/{warehouse_id}', 'BranchController@getMainBaseSubWarehouse');
Route::get('/getAllStockist', 'BranchController@getAllStockist');
Route::post('/getRegionBasStockist','BranchController@getRegionBasStockist');
Route::post('/getRegionBasAvaiStockist','BranchController@getRegionBasAvaiStockist');
Route::post('/getRegionBasWare','BranchController@getRegionBasWare');
Route::post('/getRegionBasMainWare','BranchController@getRegionBasMainWare');
Route::post('/getRegionBasStockRequestWare','BranchController@getRegionBasStockRequestWare');

//
Route::post('/getBranchAdminBaseDetails','BranchController@getBranchAdminBaseDetails');
Route::post('/getRegionMangeBaseDetails','BranchController@getRegionMangeBaseDetails');
Route::post('/getFOBaseAreaDetails','BranchController@getFOBaseAreaDetails');
Route::post('/getAreaManageBaseCusDetails','BranchController@getAreaManageBaseCusDetails');
Route::post('/getAreaManageBaseCusDetailsRemarkBase', 'BranchController@getAreaManageBaseCusDetailsRemarkBase');



//////////////////////////////
//Target Managment Api Calls//
//////////////////////////////
Route::post('/assignTarget','TargetController@assignTarget');
Route::post('/removeTarget','TargetController@removeTarget');
Route::post('/getMyTargetDetails','TargetController@getMyTargetDetails');
Route::post('/getAssignToTargetDetails','TargetController@getAssignToTargetDetails');

//////////////////////////////
//Report Managment Api Calls///
//////////////////////////////
	//sales Report
Route::post('/salesCurrentYearReport','ReportController@salesCurrentYearReport');
Route::post('/salesFilterReport','ReportController@salesFilterReport');
Route::post('/CustomercountsalesFilterReport','ReportController@CustomercountsalesFilterReport');
Route::post('/VendorcountsalesFilterReport','ReportController@VendorcountsalesFilterReport');
Route::post('/CustomersalesOrderFilterReport','ReportController@CustomersalesOrderFilterReport');
Route::post('/VendorsalesOrderFilterReport','ReportController@VendorsalesOrderFilterReport');
Route::post('/salesAnalysisReport','ReportController@salesAnalysisReport');
	//Coverage Report
Route::post('/coverageCurrentYearReport','ReportController@coverageCurrentYearReport');
Route::post('/coverageFilterReport','ReportController@coverageFilterReport');
	//Samples Report
Route::post('/samplesCurrentYearReport','ReportController@samplesCurrentYearReport');	
Route::post('/samplesFilterReport', 'ReportController@samplesFilterReport');
	//Gift Report
Route::post('/giftCurrentYearReport', 'ReportController@giftCurrentYearReport');
Route::post('/giftFilterReport', 'ReportController@giftFilterReport');
	//Daily Call Report
Route::post('/dailyCallReport','ReportController@dailyCallReport');
	//Visited Report
Route::post('/visitedCurrentYearReport','ReportController@visitedCurrentYearReport');
Route::post('/visitedFilterReport','ReportController@visitedFilterReport');
//Target Report
Route::post('/targetDetailReport','ReportController@targetDetailReport');
Route::post('/productSalesReport', 'ReportController@productSalesReport');

///////////////////////////////////
//APP Report Management Api Calls//
///////////////////////////////////
Route::post('/appDailyCallReport','ReportController@appDailyCallReport');
Route::post('/appVisitedReport','ReportController@appVisitedReport');
Route::post('/appSalesReport','ReportController@appSalesReport');
Route::post('/appCoverageReport','ReportController@appCoverageReport');
Route::post('/appSamplesReport','ReportController@appSamplesReport');
Route::post('/appGiftReport','ReportController@appGiftReport');
Route::post('/appTargetReport','ReportController@appTargetReport');
Route::post('/appTargetDetailChart','ReportController@appTargetDetailChart');
Route::post('/appMissedTaskCallReport','ReportController@appMissedTaskCallReport');

///////////////////////////////////
//APP Side Pagination Api Calls//
///////////////////////////////////
Route::post('/getAllCustomersForApp','UserController@getAllCustomersForApp');

////////////////////////
//INVENTORY API Routes//
////////////////////////
Route::post('/supplier/create','InventoryController@createSupplier');
Route::post('/supplier/list', 'InventoryController@getAllSupplier');
Route::post('/supplier/status', 'InventoryController@updateSupplierStatus');
Route::post('/supplier/details', 'InventoryController@getSupplier');
Route::post('/supplier/delete', 'InventoryController@deleteSupplier');
Route::post('/supplier/update', 'InventoryController@updateSupplier');

Route::post('/sms/smscampign', 'UserController@sendSMSCampaign');
Route::post('/email/emailcampign', 'UserController@sendEMAILCampaign');
Route::post('/notification/noticampign', 'UserController@sendNotiCampaign');
Route::get('/getAppPromoNotification/{notification_user_id}' , 'UserController@getAppPromoNotification');

////////////////////////////
//PURCHASE API call routes//
///////////////////////////

Route::post('/purchase/addPurchase', 'PurchaseController@addPurchase');
Route::get('/purchase/getAllPurchase ', 'PurchaseController@getAllPurchase');
Route::get('/purchase/getIndividualPurchase/{purchase_id}', 'PurchaseController@getIndividualPurchase');


//  ======  ** STOCKIST == SALE MODULE (MANI)  ** =========
Route::post('/sales/addSale', 'SaleController@addSale');
Route::get('/sales/getAllProductSale', 'SaleController@getAllProductSale');
Route::get('/sales/getIndividualProductSale/{sale_id}', 'SaleController@getIndividualProductSale');

//UPDATE SOLED PRODUCTS STATUS
Route::post('/sales/updateStatus', 'SaleController@UpdateSaleStatus');


// New
Route::post('/getIncentiveTargetInfo', 'TargetController@getIncentiveTargetDetails');
Route::post('/getPreviousTargetDetails', 'TargetController@getPreviousTargetDetails');
Route::post('/deleteIncentiveTarget', 'TargetController@deleteIncentiveTarget');

////////////////////////////
//Cart Management Api Call//
///////////////////////////

Route::post('/customer/addToCart', 'CustomerController@addToCart');
Route::post('/customer/getCartDetails', 'CustomerController@getCartDetails');
Route::post('/customer/deleteCartDetails', 'CustomerController@deleteCartDetails');
Route::post('/customer/getCusOrderDetailsPagi', 'CustomerController@getCusOrderDetailsPagi');
Route::post('/customer/getCusPrevOrderDetails', 'CustomerController@getCusPrevOrderDetails');

//Complain
Route::post('/customer/addComplain', 'CustomerController@addComplain');
Route::post('/customer/getCusBasedComplain', 'CustomerController@getCusBasedComplain');
Route::post('/customer/updateComplain', 'CustomerController@updateComplain');
Route::post('/customer/getUserBasedComplain', 'CustomerController@getUserBasedComplain');
Route::get('/customer/getIndComplain/{complain_id}', 'CustomerController@getIndComplain');
Route::get('/customer/deleteComplain/{complain_id}', 'CustomerController@deleteComplain');

Route::post('/getAreaManageBasWare','BranchController@getAreaManageBasWare');
// VendorManagement
Route::post('/getAreaManagerBasedStockistDetails', 'BranchController@getAreaManagerBasedStockistDetails');
Route::get('/getAllVendors', 'BranchController@getAllVendors');
Route::get('/getRegionBasVendor', 'BranchController@getRegionBasVendor');
Route::post('/getAreaManagerBasedWarehouse', 'BranchController@getAreaManagerBasedWarehouse');
Route::post('/getAreaManageBaseVendorDetails', 'BranchController@getAreaManageBaseVendorDetails');
Route::post('/getIdBasedVendorList', 'VendorController@getIdBasedVendorList');
Route::post('/addVendorOrder','VendorController@addVendorOrder');
Route::post('/getAreaManagerBasedOrder', 'VendorController@getAreaManagerBasedOrder');
Route::post('/getAreaManagerBasedVendorOrder', 'VendorController@getAreaManagerBasedVendorOrder');
Route::post('/fetchRegionBasedVendorList', 'VendorController@fetchRegionBasedVendorList');
Route::post('/getOrderIdBasedList', 'VendorController@getOrderIdBasedList');
Route::post('/getWarehouseIdBasedOrderDetails', 'VendorController@getWarehouseIdBasedOrderDetails');
Route::post('/fetchAreaManageBasedVendorOrderDetails', 'VendorController@fetchAreaManageBasedVendorOrderDetails');
Route::post('/reportForAreaManager', 'VendorController@reportForAreaManager');
Route::post('/reportForWarehouse','VendorController@reportForWarehouse');
Route::post('/VendorOrderAccept', 'VendorController@VendorOrderAccept');
Route::post('/getOrderIdDetails', 'VendorController@getOrderIdDetails');
Route::post('/getWhIdBaseVenOrdDetails', 'VendorController@getWhIdBaseVenOrdDetails');
Route::post('/getVendorOrderBasedEventsList', 'VendorController@getVendorOrderBasedEventsList');
Route::post('/updateVendorOrderStatus', 'VendorController@updateVendorOrderStatus');
Route::post('/getPlacedVendorOrder', 'VendorController@getPlacedVendorOrder');
Route::get('/getApprovalVendorOrderData','VendorController@getApprovalVendorOrderData');
Route::post('/getVendorOrderByAdmin','VendorController@getVendorOrderByAdmin');
Route::post('/vendorOrderFOCDiscountAccept','VendorController@vendorOrderFOCDiscountAccept');
Route::post('/getAreaIdBasedPriceType','VendorController@getAreaIdBasedPriceType');
Route::post('/acceptCustomerOrderdetails','OrderController@acceptCustomerOrderdetails');
Route::post('/getOrderBeforeAdminApproval','OrderController@getOrderBeforeAdminApproval');
Route::post('/getOrderBasedFOCDetails', 'OrderController@getOrderBasedFOCDetails');
Route::post('/vendorReportForProductIdBased','VendorController@vendorReportForProductIdBased');
Route::post('/getOrderBatchFOCDetails', 'OrderController@getOrderBatchFOCDetails');
Route::post('/customerReportForProductIdBased', 'ReportController@customerReportForProductIdBased');
Route::post('/customerOrderReportForProductIdBased', 'ReportController@customerOrderReportForProductIdBased');
Route::get('/getOrderBasedOrderList/{order_id}' , 'OrderController@getOrderBasedOrderList');
Route::post('/getAllFieldOfficerAndStockistInOrder', 'VendorController@getAllFieldOfficerAndStockistInOrder');



///////Collection////////

Route::post('/addCollectionDetails', 'CollectionController@addCollectionDetails');
Route::post('/fetchCollectionDateBased','CollectionController@fetchCollectionDateBased');
Route::post('/fetchInvoice_noBasedDetails', 'CollectionController@fetchInvoice_noBasedDetails');
Route::post('/vendorReportForRegionManageBased', 'VendorController@vendorReportForRegionManageBased');
Route::post('/addSalereturn','CollectionController@addSalereturn');
Route::post('/saleReturnIdBasedDetails', 'CollectionController@saleReturnIdBasedDetails');
Route::post('/acceptCollectionDetail','CollectionController@acceptCollectionDetail');
Route::post('/fetchVendorOrderList','CollectionController@fetchVendorOrderList');
Route::post('/fetchCollectionDetailsBasedId', 'CollectionController@fetchCollectionDetailsBasedId');
Route::post('/deleteCollectionDetails', 'CollectionController@deleteCollectionDetails');
Route::post('/fetchCollectionDetailsOrderBase','CollectionController@fetchCollectionDetailsOrderBase');
Route::post('/fetchFinanceUserBasedCollections','CollectionController@fetchFinanceUserBasedCollections');
Route::post('/updateCollectionUser', 'CollectionController@updateCollectionUser');
Route::post('/fetchRegionBaseCusOrderVenOrderDet', 'CollectionController@fetchRegionBaseCusOrderVenOrderDet');
Route::post('/collectionPendingReport', 'CollectionController@collectionPendingReport');
Route::post('/regionBaseCusOrVendorDetails', 'CollectionController@regionBaseCusOrVendorDetails');
Route::post('/checkerStatusUpdate', 'CollectionController@checkerStatusUpdate');
Route::post('/fetchRegionBasedCollect', 'CollectionController@fetchRegionBasedCollect');
Route::post('/fetchAreaBasedCollect', 'CollectionController@fetchAreaBasedCollect');
Route::post('/expireDateReport', 'CollectionController@expireDateReport');
Route::post('/dailyCollectionReport', 'CollectionController@dailyCollectionReport');
Route::post('/checkerDailyCollectionReport','CollectionController@checkerDailyCollectionReport');
Route::post('/invoiceBasedReturnDetails', 'CollectionController@invoiceBasedReturnDetails');
Route::post('/orderReturnReport', 'CollectionController@orderReturnReport');
Route::post('/discountReportSummary','ReportController@discountReportSummary');
Route::post('/discountReport', 'ReportController@discountReport');
Route::post('/subWarehouseInStocksReport','ReportController@subWarehouseInStocksReport');
Route::post('/subWarehouseItemMovingReport','ReportController@subWarehouseItemMovingReport');
Route::post('/subWarehouseItemMovingReportSumm', 'ReportController@subWarehouseItemMovingReportSumm');
Route::post('/mainWarehouseItemMovingRequest', 'ReportController@mainWarehouseItemMovingRequest');
Route::post('/mainWarehouseItemMovingRequestSumm', 'ReportController@mainWarehouseItemMovingRequestSumm');
Route::get('/getCollectionCrDetails', 'CollectionController@getCollectionCrDetails');
Route::post('/getRegionBasedWarehouses','CollectionController@getRegionBasedWarehouses');
Route::post('/financeUsrBasedReturnDetails', 'CollectionController@financeUsrBasedReturnDetails');
Route::post('/orderIdBasedReturnDetails','CollectionController@orderIdBasedReturnDetails');
Route::post('/checkerStateReject', 'CollectionController@checkerStateReject');
Route::post('/doerStatusUpdate', 'CollectionController@doerStatusUpdate');
Route::post('/regionBasedCollectionDetails', 'CollectionController@regionBasedCollectionDetails');
Route::post('/fetchCheckedCollectionDetails', 'CollectionController@fetchCheckedCollectionDetails');
Route::post('/fetchFieldOfficerCollection', 'CollectionController@fetchFieldOfficerCollection');
Route::post('/doerCollectionDetails', 'CollectionController@doerCollectionDetails');
Route::post('/doer_list', 'CollectionController@doer_list');
Route::get('/checker_list', 'CollectionController@checker_list');
Route::post('/checkerReport','CollectionController@checkerReport');
Route::post('/financeUserReport','CollectionController@financeUserReport');
Route::post('/regionBaseCusOrderDetails', 'CollectionController@regionBaseCusOrderDetails');
Route::post('/financeUserAcceptedAndPendingReport', 'CollectionController@financeUserAcceptedAndPendingReport');
Route::get('/getAllFinanceUser', 'CollectionController@getAllFinanceUser');

/////Telecalling/////

Route::post('/addTelecalling', 'TelecallingController@addTelecalling');
Route::post('/fieldOfficerBaseTeleCalling', 'TelecallingController@fieldOfficerBaseTeleCalling');
Route::post('/telecallingReport', 'TelecallingController@telecallingReport');
Route::post('/addRecalling', 'TelecallingController@addRecalling');
Route::post('/fieldOfficerBasedRecalling', 'TelecallingController@fieldOfficerBasedRecalling');
Route::post('/telecallingBasedAllDetails', 'TelecallingController@telecallingBasedAllDetails');
Route::post('/telecallingStatusUpdate','TelecallingController@telecallingStatusUpdate');

///////Auditor\\\\\\\
Route::post('/stockReport', 'ReportController@stockReport');
Route::post('/financeUserPendingReport', 'CollectionController@financeUserPendingReport');
Route::post('/dailySaleReport', 'ReportController@dailySaleReport');
Route::get('/getMainWarehouse', 'ReportController@getMainWarehouse');
Route::post('/getBranchBasWare','BranchController@getBranchBasWare');
Route::post('/getBranchBasedCategory','ProductController@getBranchBasedCategory');
Route::post('/updateProductsBatchQty', 'ProductController@updateProductsBatchQty');

Route::post('/schemeReport', 'ReportController@schemeReport');
Route::get('/getOrderScheme', 'OrderController@getOrderScheme');

//Balance sheet for stocks
Route::post('/getStockBalanceSheet', 'ProductController@getStockBalanceSheet');
Route::post('/getBatchDetails', 'ProductController@getBatchDetails');
Route::post('/uploadGoodsRecivedStock', 'ProductController@uploadGoodsRecivedStock');

Route::post('/generateOrderPDF' , 'OrderController@generateOrderPDF');
Route::post('/updateOrderInvoiceDate' , 'OrderController@updateOrderInvoiceDate');

//Biller Management
Route::post('/addBiller', 'UserController@addBiller');
Route::post('/updateBiller', 'UserController@updateBiller');
Route::post('/updateBillerStatus', 'UserController@updateBillerStatus');
Route::post('/deleteBiller', 'UserController@deleteBiller');
Route::post('/getBillerDetails', 'UserController@getBillerDetails');

Route::post('/stockAdjustAddProduct', 'ProductController@stockAdjustAddProduct');

Route::post('/acceptMultipleCollectionDetail','CollectionController@acceptMultipleCollectionDetail');
Route::post('/getDoerList', 'UserController@getDoerList');
Route::post('/getCustomerCollections', 'CollectionController@getCustomerCollections');
Route::post('/getCheckerList', 'UserController@getCheckerList');
Route::post('/addDoerCollectionDetails', 'CollectionController@addDoerCollectionDetails');
Route::post('/doerCollectionReport', 'CollectionController@doerCollectionReport');
Route::post('/financeCollectionReport', 'CollectionController@financeCollectionReport');
Route::post('/getFinanceUserList', 'UserController@getFinanceUserList');
Route::post('/getCustomersList', 'UserController@getCustomersList');

//Purchase Order
Route::post('/addPORequest', 'ProductController@addPORequest');
Route::post('/getAllPORequest', 'ProductController@getAllPORequest');
Route::post('/getPODetails', 'ProductController@getPODetails');
Route::post('/getPOInvoiceDetails', 'ProductController@getPOInvoiceDetails');
Route::post('/acceptPORequest', 'ProductController@acceptPORequest');
Route::post('/generatePOInvoice', 'ProductController@generatePOInvoice');
Route::post('/getAllPOInvoice', 'ProductController@getAllPOInvoice');
Route::post('/getPOInvoiceProducts', 'ProductController@getPOInvoiceProducts');
Route::post('/poStockAcknowledge' , 'ProductController@poStockAcknowledge');
Route::post('/addPOPaymentDetails', 'ProductController@addPOPaymentDetails');
Route::post('/getPOPaymentTransaction', 'ProductController@getPOPaymentTransaction');


Route::post('/demo', 'OrderController@demo');

