<!DOCTYPE html>
<html>
<head>
	<title>Invoice</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
	<style type="text/css">
		@page { size: 21cm 15cm; margin : 5px; }
		/*@page { size: 595.276pt 425.197pt; margin: 25px;  }*/
		@font-face {
			font-family:'Myanmar3';
			src:local('Myanmar3'), url('https://www.mmwebfonts.com/fonts/myanmar3.woff') format('woff'), url('https://www.mmwebfonts.com/fonts/myanmar3.ttf') format('ttf');
			}

		h4 {
			font-size: 12px;
			line-height: 2px;
		}
		h3 {
			font-size: 15px;
			line-height: 3px;
		}

		p {
			font-size: 10px;
			line-height: 10px;
		}
		
		tr {
			font-size: 11px;
			line-height: 18px;
		}
		#second-table {
			text-align: center;
			border-collapse: collapse;
			width: 100%;
		}
		#table-a{
			text-align: left;
		}
		#table-b{
			text-align: left;
		}
		h6{
			font-size: 11px;
		}
		#table-c{
			text-align: center;
		}
		#table-d{
			text-align: left;
		}
		#second-table {
			border: 1px solid black;
 			border-collapse: collapse;
		}
		#nd-rows, #nd-rows > th, #nd-rows > td{
			border: 1px solid black;
			text-align: center;
		}
		#table-a > p{
			font-size: 11px;
		}
		#table-ab{
			font-size: 11px;
		}
		.unicode{
			font-family:Myanmar3,Yunghkio,'Masterpiece Uni Sans';
		}


	</style>
</head>
<body>
<center>
    <h3><b>{{$com_details->branch_name}}</b></h3>
	<p>{{$com_details->branch_address}}<br/> 
	   {{$com_details->branch_country}}.<b>PH: </b> 01-386672, 01-387623<!-- <b>PH: +</b>{{$com_details->country_code}} {{$com_details->branch_contact}} -->
	</p>
		<p></p>
	<h4><b>Sales Voucher</b></h4>
</center>
	<div class="row">
		<div class="col-md-12">
<!-- 			<table style="width:100%" id="table-ab" class="col-md-12">
				<tr>
						<th >Customer name:</th>
						<td>{{$cus_de->customer_name}}</td>
						<th align="right">Invoice no:</th>
						<td>{{$invoice}}</td>
				</tr>
				<tr>
						<th><b>Address: </b></th>
						<td>{{$cus_de->customer_address}}</td>
						<th align="right">Invoice date:</th>
						<td>{{$date}}</td>
				</tr>
				<tr>
						<th>Location:</th>
						<td>{{$cus_de->customer_location}}</td>
						<th align="right">Customer Id:</th>
						<td>{{$cus_de->customer_id}}</td>
				</tr>
			</table> -->
			<table style="width:100%" id="table-ab" class="col-md-12">
                <tr>
                        <th >Customer name:</th>
                        <td>{{$cus_de->customer_name}}</td>
                        <th align="right">Invoice no:</th>
                        <td>{{$invoice}}</td>
                </tr>
                <tr>
                        <th >Customer Mobile:</th>
                        <td>{{$cus_de->customer_mobile}}</td>
                        <th >Warehouse Name:</th>
                        <td>{{$war->warehouse_name}}</td>
                </tr>
                <tr>
                        <th><b>Address: </b></th>
                        <td>{{$cus_de->customer_address}} , {{$cus_de->customer_location}}</td>
                        <th align="right">Invoice date:</th>
                        <td>{{$date}}</td>
                </tr>
                <tr>
                        <!-- <th>Location:</th>
                        <td>{{$cus_de->customer_location}}</td> -->
                        <th >Customer Id:</th>
                        <td>{{$cus_de->customer_emp_id}}</td>
                         <th align="right">Field Officer name:</th>
                        <td>{{$order_details->field_officer_name}}</td>
                </tr>
                <tr>
                        <th ></th>
                        <td></td>
                         <th align="right">Biller:</th>
                        <td>{{$order_details->biller_name}}</td>
                </tr>

                <!-- <tr>
                        <th align="right">Field Officer name:</th>
                        <td>{{$order_details->field_officer_name}}</td>
                </tr> -->
            </table>
		</div>
	</div>
	<br>
	<center>
		<table id = "second-table" class="col-md-12" style="width:100%">
			<tr id="nd-rows">
				<th>S.No</th>
				<th>Product Name</th>
				<th>Scheme</th>
				<th>Batch.No</th>
				<th>Net Amount</th>
				<th>Gross Amount</th>
				<th>Qty</th>
				<th>Exp.Date</th>
				<th>FOC</th>
				<th>FOC Amt</th>
				<th>SPL FOC</th>
				<th>DIS Amount</th>
				<th>Amount</th>
			</tr>
			@foreach($datas  as $key=> $data_details)
			<tr id="nd-rows">
				<td>{{ ++$key }}</td>
				<td>{{  $data_details->product_genericname }}</td>
                @if($data_details->scheme == 0)
                <td>-</td>
                @else
                <?php 
                	$scheme_details = DB::table('gpff_order_scheme')
                					->where('scheme_value',$data_details->scheme)
                					->first();
                ?>
                <td>{{$scheme_details->scheme_name}}</td>
                @endif
				<td>{{  $data_details->batch_no }}</td>
				@if($data_details->pro_amt_cal == 1) 
				<td style="text-align:right">{{  number_format($data_details->product_netprice,2) }}</td>
				<td>-</td>
				@endif
				@if($data_details->pro_amt_cal == 2)
				<td>-</td>
				<td style="text-align:right">{{  number_format($data_details->product_grossprice,2) }}</td>
				@endif
				<td>{{  $data_details->product_qty }}</td>
				<td>{{  $data_details->batch_emp_date }}</td>
				<td>{{  $data_details->FOC }}</td>
				<td style="text-align:right">{{  number_format($data_details->FOC_amt,2) }}</td>
				<td>{{  $data_details->spl_FOC }}</td>
				<td>{{  $data_details->product_discount }}</td>
				<td style="text-align:right">{{  number_format($data_details->product_tot_price,2) }}</td>
			</tr>
			$key++;
			@endforeach
		</table>

		<div class="row">
			<div class="col-md-12">
				<table style="width:100%" id="table-ab" class="col-md-6">
					<tr>
						<th>Total box:</th>
						<td>{{$order_details->tot_box}}</td>
						<th align="right">Net value:</th>
						<td style="text-align:right">{{number_format($order_details->or_gross_total,2)}}</td>
					</tr>
					<tr>
						<th><b>Cash: </b></th>
						<td>{{number_format($order_details->or_tot_price,2)}}Kyat</td>
						<th align="right">Gross value:</th>
						<td style="text-align:right">{{number_format($order_details->or_gross_total,2)}}</td>
					</tr>
					<tr>
						<th></th>
						<td>{{$value}}</td>
						<th align="right">Discount:</th>
						<td style="text-align:right">{{number_format($order_details->order_discount,2)}}</td>
					</tr>
					<tr>
						<th>Due Date:</th>
						<td>{{$order_details->credit_valid_to}}</td>
						<th align="right">Spl.Discount:</th>
						<td style="text-align:right">{{number_format($order_details->spl_discount,2)}}</td>
					</tr>
					<tr>
						<th></th>
						<td></td>
						<th align="right">Balance:</th>
						<td style="text-align:right">{{number_format($order_details->or_tot_price,2)}}</td>
					</tr>
					<tr>
						<th></th>
						<td></td>
						<th align="right">Bill amount:</th>
						<td style="text-align:right">{{number_format($order_details->or_tot_price,2)}}</td>
					</tr>
				</table>
			</div>
		</div>
	</center>
	<center>
		<!-- <div class="row">
			<div class="col-md-12">
				<p align="left">Received the above goods is good order & conditions</p><br>
				<table style="width:100%" id="table-ab" class="col-md-6">
					<tr>
						<th >This is computer generated bill.No need of signature.</th>
						<td></td>
						<th align="right"></th>
						<td><b>Signature & Stamp of Buyer</b></td>
					</tr>
					<tr>
						<th></th>
						<td></td>
						<th></th>
						<td></td>
					</tr>
				</table>
			</div>
		</div> -->
		<div class="row">
			<div class="col-md-12">
				<p align="left">Received the above goods is good order & conditions</p>
				<img align="left" style="width:50%;height:50px;margin-left:0%;" src="https://tgetsfa-live.s3.ap-south-1.amazonaws.com/website/burmese_invoice.png"><br>
			</div><br><br>
			<div class="col-md-12">
				<table style="width:100%" id="table-ab" class="col-md-6">
					<tr>
						<th >This is computer generated bill.No need of signature.</th>
						<td></td>
						<th align="right"></th>
						<td><b>Signature & Stamp of Buyer</b></td>
					</tr>
					<tr>
						<th></th>
						<td></td>
						<th></th>
						<td></td>
					</tr>
				</table>
			</div>
		</div>
	</center>
</body>
</html>