<?php

namespace App\Http\Controllers;

use Closure;
use DB;
use Mail;
use Carbon\Carbon;
use App\Imports\UsersImport;
use Maatwebsite\Excel\Facades\Excel;

require(base_path().'/app/Http/Middleware/Common.php');

class User
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        return $next($request);
    }

////////////////////////////
//User Managment Api Calls//
////////////////////////////
    //1-superadmin ,2-Brachadmin,3-Regionadmin,4-Areamanager,5-fieldofficer,6-salesref,7-stockist,8-customer 
    //Insert user Data
    public function storeUsers($users) 
    {   
        $pass = substr(str_shuffle("abcdef@0123456789"), 0, 8);
        //$pass = "12345678";
        $password = md5($pass);

        $values = array(
            'firstname'         => ucwords($users->firstname) , 
            'lastname'          => ucwords($users->lastname) ,
            'username'          => $users->username , 
            'email'             => $users->email ,
            'mobile'            => $users->mobile ,
            'country_code'      => $users->country_code,
            'address'           => $users->address ,
            // 'vendor'            => $users->vendor,
            'country'           => $users->country , 
            'country_flag'      => $users->country_flag,
            'city'              => $users->city ,
            'role'              => $users->role ,
            'area_manager_id'   => $users->area_manager_id , 
            'gender'            => $users->gender ,
            'dob'               => $users->dob , 
            'password'          => $password , 
            'permission'        => $users->permission , 
            // 'remarks'           => $users->remarks,
            // 'remarks_type'      => $users->remarks_type,
            'branch_id'         => $users->branch_id , 
            'region_id'         => $users->region_id , 
            'area_id'           => $users->area_id , 
            'special_permission'=> $users->special_permission , 
            'branch_admin_id'   => $users->branch_admin_id , 
            'region_manager_id' => $users->region_manager_id , 
            'warehouse_id'      => $users->warehouse_id , 
            'avatar'            => $users->avatar ,
            'cr_by_id'          => $users->cr_by_id , 
            'created_at'        => date('Y-m-d H:i:s') , 
            'updated_at'        => date('Y-m-d H:i:s')
        );
        //Email Function
        $template = 'communication';
        $this->sendUserEmail($users->email,$users->username,$pass,$template,$users->firstname);
        //Notification Entry
        if($users->area_manager_id){
            
            $managname  =   DB::table('gpff_users')
                            ->where('user_id',$users->area_manager_id)
                            ->first();

            if($users->role == 5){
                $message = "Your Manager ".$managname->firstname." has Add One New Field Officer";
            } else if($users->role == 6){
                $message = "Your Manager ".$managname->firstname." has Add One New Sales Ref";
            } else if($users->role == 7){
                $message = "Your Manager ".$managname->firstname." has Add One New Stockist";
            }   
            $page_id = 'UNKNOWN';

            $cmn = new Common();
            $cmn->insertNotification($users->area_manager_id,$managname->firstname,$managname->cr_by_id,$message,$page_id);
        }
        if($users->region_manager_id){
            
            $managname  =   DB::table('gpff_users')
                            ->where('user_id',$users->region_manager_id)
                            ->first();
 
            if($users->role == 5){
                $message = "Your Manager ".$managname->firstname." has Add One New Field Officer";
            } else if($users->role == 6){
                $message = "Your Manager ".$managname->firstname." has Add One New Sales Ref";
            } else if($users->role == 7){
                $message = "Your Manager ".$managname->firstname." has Add One New Stockist";
            } else if($users->role == 4){
                $message = "Your Manager ".$managname->firstname." has Add One Area Manager";
            }   
            $page_id = 'UNKNOWN';
            $cmn = new Common();
            $cmn->insertNotification($users->region_manager_id,$managname->firstname,$managname->cr_by_id,$message,$page_id);
        }
        if($users->branch_admin_id){
            
            $managname  =   DB::table('gpff_users')
                            ->where('user_id',$users->branch_admin_id)
                            ->first();

            if($users->role == 5){
                $message = "Your Branch Admin ".$managname->firstname." has Add One New Field Officer";
            } else if($users->role == 6){
                $message = "Your Branch Admin ".$managname->firstname." has Add One New Sales Ref";
            } else if($users->role == 7){
                $message = "Your Branch Admin ".$managname->firstname." has Add One New Stockist";
            } else if($users->role == 4){
                $message = "Your Branch Admin ".$managname->firstname." has Add One Area Manager";
            }   
            $page_id = 'UNKNOWN';
            $cmn = new Common();
            $cmn->insertNotification($users->region_manager_id,$managname->firstname,$managname->cr_by_id,$message,$page_id);
        }
        //End Notification Entry
        return  DB::table('gpff_users')
                ->insert($values);
    }
    //Update user details
    public function updateUsers($users) 
    {
        $values = array(
                'firstname' => ucwords($users->firstname) , 
                'lastname'  => ucwords($users->lastname) ,
                'username'  => $users->username , 
                'email'     => $users->email ,
                'mobile'    => $users->mobile ,
                'country_code' =>$users->country_code,
                'country_flag'=>$users->country_flag,
                'address'   => $users->address , 
                'city'      => $users->city ,
                'country'   => $users->country , 
                'role'      => $users->role ,
                'area_manager_id'=> $users->area_manager_id ,  
                'gender'    => $users->gender ,
                'dob'       => $users->dob ,
                'permission'  => $users->permission , 
                'avatar'    => $users->avatar , 
                'branch_id'  => $users->branch_id , 
                'region_id'  => $users->region_id , 
                'area_id'       => $users->area_id , 
                'special_permission'=> $users->special_permission ,
                'branch_admin_id'  => $users->branch_admin_id , 
                'region_manager_id'  => $users->region_manager_id , 
                'warehouse_id'  => $users->warehouse_id , 
                'updated_at'    => date('Y-m-d H:i:s')
            );
        return  DB::table('gpff_users')
                ->where('user_id', $users->user_id)
                ->update($values);
    }
    //  Users Profile Update Details
    public function userProfileUpdate($users) 
    {
        $values = array(
                'firstname' => ucwords($users->firstname) , 
                'lastname'  => ucwords($users->lastname) ,
                'username'  => $users->username , 
                'email'     => $users->email ,
                'mobile'    => $users->mobile ,
                'country_code' =>$users->country_code,
                'country_flag'=>$users->country_flag,
                'address'   => $users->address , 
                'city'      => $users->city ,
                'country'   => $users->country , 
                'gender'    => $users->gender ,
                'dob'       => $users->dob ,
                'avatar'    => $users->avatar , 
                'updated_at'    => date('Y-m-d H:i:s')
            );
        return  DB::table('gpff_users')
                ->where('user_id', $users->user_id)
                ->update($values);
    }
    public function userBasedPriceUpdate($users) 
    {
        $values = array(
                'price_type'    => $users->price_type , 
                'updated_at'    => date('Y-m-d H:i:s')
            );
        return  DB::table('gpff_users')
                ->where('user_id', $users->user_id)
                ->update($values);
    }
    //Delete user details
    public function deleteUsers($users)
    {
        return  DB::table('gpff_users')
                ->where('user_id', $users->user_id)
                ->delete();
    }
    //Get Indivitual Users Details
    public function getIndUsers($users)
    {   
        return  DB::table('gpff_users')
                ->where('user_id', $users->user_id)
                ->get();
    }
    //Get Indivitual Users Details
    public function getRoleBasedUsers($users)
    {   
        //Role 3 : branchname,
        //Role 4 : branchname,regionname
        //Role 5 : branchname,regionname,areaname
        //Role 7 : branchname,regionname
        //1-superadmin ,2-Brachadmin,3-Regionadmin,4-Areamanager,5-fieldofficer,6-salesref,7-stockist,8-customer
        if($users->role == 3){
            return  DB::table('gpff_users as guse')
                    ->join('gpff_branch as gbran','guse.branch_id','gbran.branch_id')
                    ->where('guse.role', $users->role)
                    ->orderBy('guse.updated_at','DESC')
                    ->get();
        } else if($users->role == 4 || $users->role == 7){
            return  DB::table('gpff_users as guse')
                    ->join('gpff_branch as gbran','guse.branch_id','gbran.branch_id')
                    ->join('gpff_region as gregi','guse.region_id','gregi.region_id')
                    ->where('guse.role', $users->role)
                    ->orderBy('guse.updated_at','DESC')
                    ->get();
        } else if($users->role == 5){

            return  DB::table('gpff_users as guse')
                    ->join('gpff_branch as gbran','guse.branch_id','gbran.branch_id')
                    ->join('gpff_region as gregi','guse.region_id','gregi.region_id')
                    //->join('gpff_area as gare','guse.area_id','gare.area_id')
                    ->where('guse.role', $users->role)
                    ->orderBy('guse.updated_at','DESC')
                    ->get();

            /*$data = DB::table('gpff_users')
                    ->where('role', $users->role)
                    ->get();

            $tot = [];
            foreach ($data as $value) {
                $myArray = explode(',', $value->area_id);
                $arr = DB::table('gpff_users as guse')
                    ->join('gpff_branch as gbran','guse.branch_id','gbran.branch_id')
                    ->join('gpff_region as gregi','guse.region_id','gregi.region_id')
                    ->join('gpff_area as gare','guse.region_id','gare.region_id')
                    ->whereIn('gare.area_id', $myArray)
                    ->where('guse.user_id', $value->user_id)
                    ->orderBy('guse.updated_at','DESC')
                    ->get();
                array_push($tot, $arr);
            }

            return $tot;*/
        } else {
            return  DB::table('gpff_users')
                ->where('role', $users->role)
                ->orderBy('updated_at','DESC')
                ->get();
        }
    }
    // Fetch Manager Based User Details
    public function getManagerBasedUserDetails($users)
    {   
        return  DB::table('gpff_users')
                ->where('area_manager_id', $users->area_manager_id)
                ->whereNotIn('role',[8])
                ->orderBy('updated_at','DESC')
                ->get();
    }
    public function getAllUsers() 
    {
        $data =[];
        $data['totaldata']= DB::table('gpff_users')
                            ->whereNotIn('role',[1])
                            ->orderBy('updated_at','DESC')
                            ->get();
         return $data;   
    }
    public function userActiveOrDeactive($users) 
    {
        $values = array(
                'is_active'  => $users->is_active ,  
                'updated_at' => date('Y-m-d H:i:s')
             );
        //Notification Entry
        $admin = DB::table('gpff_users')
                ->where('role', 1)
                ->First();
            if($users->is_active == 1){
                $message = "Admin Has Activated Your Account.";
            } else if($users->is_active == 2){
                $message = "Admin Has Deactivated Your Account.";
            }
            $page_id = 'UNKNOWN';
            $cmn = new Common();
            $cmn->insertNotification($admin->user_id,$admin->firstname,$users->user_id,$message,$page_id);
        //End Notification Entry

        return DB::table('gpff_users')
        ->where('user_id', $users->user_id)
        ->update($values);  
    }
    //Check User mobile Number Already Exist
    public function getUsermobilenumber($users) 
    {
        return DB::table('gpff_users')
        ->where('mobile', $users->mobile)
        ->orderBy('updated_at', 'DESC')
        ->get();
    }
    //Check User Email Already Exist
    public function getUserEmail($users) 
    {
        return DB::table('gpff_users')
        ->where('email', $users->email)
        ->orderBy('updated_at', 'DESC')
        ->get();
    }
    //Check User Name Already Exist
    public function getUserName($users) 
    {
        return DB::table('gpff_users')
        ->where('username', $users->username)
        ->orderBy('updated_at', 'DESC')
        ->get();
    }
    //User Login 
    public function user_login($users)
    {
        $login = DB::table('gpff_users')
                 ->where('username', $users->username)
                 ->where('password', $users->password)
                 ->First(['user_id','is_active']);

        if($login){
            if($login->is_active == 1){
                if($users->login_tag == '1'){
                    if($users->push_token != ''){
                        $value = array(
                                'app_push_token' => $users->push_token,
                                'os_type' => $users->os_type,
                                'online_status' => '1',
                                'updated_at' => date('Y-m-d H:i:s')       
                            );
                    } else{
                        $value = array(
                                'online_status' => '1',
                                'updated_at' => date('Y-m-d H:i:s')
                            );
                    }
                } else if($users->login_tag == '2'){
                    if($users->push_token != ''){
                        $value = array(
                                'web_push_token' => $users->push_token,
                                'online_status' => '1',
                                'updated_at' => date('Y-m-d H:i:s')
                            );
                    } else{
                        $value = array(
                                'online_status' => '1',
                                'updated_at' => date('Y-m-d H:i:s')
                            );
                    }
                } 


                DB::table('gpff_users')
                ->where('user_id', $login->user_id)            
                ->update($value);

                $login_value = DB::table('gpff_users')
                 ->where('user_id', $login->user_id)
                 ->First();
                

                return array(
                        'login_details' => $login_value
                        );
    
            } else{
                return 1;
            } 
        }else{
            return 2;
        }
    }
    //Logout
    public function user_logout($users)
    {
        if($users->login_tag == 1){
            $values = array(
                'updated_at' =>  date('Y-m-d H:i:s'),
                'online_status' => '0',
                'app_push_token'=> Null
            );
        } else{
            $values = array(
                'updated_at' =>  date('Y-m-d H:i:s'),
                'online_status' => '0',
                'web_push_token'=> Null
            );
        }
        
        return  DB::table('gpff_users')
                ->where('user_id',$users->user_id)
                ->update($values);
    }
    //User Change our Password
    public function userChangePassword($users) 
    {   
        $login = DB::table('gpff_users')
                 ->where('user_id', $users->user_id)
                 ->where('password', $users->old_password)
                 ->First();
        if($login){
            $values = array(
                'password'  => $users->new_password ,  
                'updated_at' => date('Y-m-d H:i:s')
             );
            DB::table('gpff_users')
            ->where('user_id', $users->user_id)
            ->update($values);
            return 1;  
        } else{
            return 2;
        }
        
    }
    //User Forget our Password
    public function userForgetPassword($users) 
    {   
        $login = DB::table('gpff_users')
                 ->where('email', $users->email)
                 ->First();

        if($login){
            $pass = substr(str_shuffle("abcdef@0123456789"), 0, 8);
            $password = md5($pass);

            $values = array(
                'password'  => $password ,
                'updated_at' => date('Y-m-d H:i:s')
             );

            $template = 'forgetPassword';
            $this->sendUserEmail($users->email,$login->username,$pass,$template,$login->firstname);

            DB::table('gpff_users')
            ->where('email', $users->email)
            ->update($values);
            return 1;  
        } else{
            return 2;
        }
        
    }
    //Insert Customer Data
    public function storeCustomers($users) 
    {   

        $pass = substr(str_shuffle("abcdef@0123456789"), 0, 8);
        //$pass = "12345678";
        $password = md5($pass);

        $values = array(
            'customer_name'     => ucwords($users->customer_name) , 
            'customer_email'    => $users->customer_email , 
            'customer_mobile'   => $users->customer_mobile ,
            'customer_city'     => $users->customer_city ,
            'customer_country'  =>$users->customer_country,
            'vendor'            => $users->vendor,
            'customer_fax'      => $users->customer_fax , 
            'country_code'      => $users->country_code ,
            'customer_address'  => $users->customer_address ,
            'customer_location' => $users->customer_location ,
            'customer_lat'      => $users->customer_lat ,
            'customer_lan'      => $users->customer_lan , 
            'website'           => $users->website ,
            'branch_id'         => $users->branch_id ,
            'region_id'         => $users->region_id ,
            'remarks'           => $users->remarks,
            'remarks_type'      => $users->remarks_type,
            'area_id'           => $users->area_id , 
            'avatar'            => $users->avatar ,
            'customer_cr_id'    => $users->customer_cr_id,
            'area_manager_id'   => $users->area_manager_id,
            'region_manager_id' => $users->region_manager_id,
            'customer_type'     => $users->customer_type,
            'pin_code'          => $users->pin_code,
            'credit_limit'      => $users->credit_limit, //NEW CHANGES
            'customer_emp_id'   => $users->customer_emp_id,
            'created_at'        => date('Y-m-d H:i:s') , 
            'updated_at'        => date('Y-m-d H:i:s')
        );
        
        //Email Function
        // $template = 'communication';
        // $this->sendUserEmail($users->customer_email,$users->customer_mobile,$pass,$template,$users->customer_name);

        if($users->area_manager_id)
        {    
            $managname  =   DB::table('gpff_users')
                            ->where('user_id',$users->area_manager_id)
                            ->first();

            $message = "Your Manager ".$managname->firstname." has Add One New Customer";
            $page_id = 'UNKNOWN';

            $cmn = new Common();
            $cmn->insertNotification($users->area_manager_id,$managname->firstname,$managname->cr_by_id,$message,$page_id);
        }
        if($users->region_manager_id)
        {    
            $managname  =   DB::table('gpff_users')
                            ->where('user_id',$users->region_manager_id)
                            ->first();

            $message = "Your Region Manager ".$managname->firstname." has Add One New Customer";
            $page_id = 'UNKNOWN';

            $cmn = new Common();
            $cmn->insertNotification($users->region_manager_id,$managname->firstname,$managname->cr_by_id,$message,$page_id);
        }

        $customerid   = DB::table('gpff_customer')
                    ->insertGetId($values);

        $value = array(
            'firstname' => ucwords($users->customer_name) , 
            'username'  => $users->customer_mobile , 
            'email'     => $users->customer_email ,
            'mobile'    => $users->customer_mobile ,
            'country_code' =>$users->country_code,
            'address'   => $users->customer_address , 
            'country'   => $users->customer_country ,
            'branch_id'   => $users->branch_id ,
            'region_id'   => $users->region_id ,
            'area_id'   => $users->area_id , 
            'role'      => 8 ,
            'area_manager_id'=> $users->area_manager_id , 
            'region_manager_id'=> $users->region_manager_id , 
            'cr_by_id'    => $users->customer_cr_id,
            'password'  => $password , 
            'avatar'    => $users->avatar ,
            'customerid'    => $users->customer_emp_id ,
            'created_at'=> date('Y-m-d H:i:s') , 
            'updated_at'=> date('Y-m-d H:i:s')
        );
        return  DB::table('gpff_users')
                ->insert($value);
    }

    public function addAllCustomers($users){

        $extension = $users->customerall->getClientOriginalExtension();
        if($extension == "xlsx" || $extension == "xls" || $extension == "csv") 
        {   
            $path = $users->customerpath;

            $import = new UsersImport;
            Excel::import($import, $users->customerall);
            $data = $import->data;

            if(!empty($data) && count($data))
            {
                $values = [];
                foreach ($data as $value) 
                {
                    // if($value['customer_name'] != "" && $value['customer_email'] != "" && $value['country_code'] != "" && $value['customer_mobile'] != "" && $value['customer_city'] != "" && $value['customer_address'] != "" && $value['customer_type'] != "" && $value['region_id'] != "" && $value['company_id'] != "" && $value['area_id'] != "" && $value['pin_code'] != "" && $value['customer_id'] != "")
                    // {
                    if($value['customer_name'] != "" && $value['country_code'] != "" && $value['customer_mobile'] != "" && $value['customer_city'] != "" && $value['customer_address'] != "" && $value['customer_type'] != "" && $value['region_id'] != "" && $value['company_id'] != "" && $value['area_id'] != "" && $value['customer_id'] != "")
                    {
                        $result = DB::table('gpff_customer')
                                // ->where('vendor', 2)
                                ->where('customer_mobile', $value['customer_mobile'])
                                ->where('customer_email', $value['customer_email'])
                                ->where('customer_emp_id', $value['customer_id'])
                                ->get();

                        if (count($result) == 0) {
                            $values[] = [
                                'customer_name'     => ucwords($value['customer_name']) , 
                                'customer_email'    => $value['customer_email'],
                                'customer_mobile'   => $value['customer_mobile'],
                                'customer_city'     => $value['customer_city'],
                                'country_code'      => $value['country_code'],
                                'remarks_type'      => $value['remarks_type'],
                                'remarks'           => $value['remarks'],
                                'customer_address'  => $value['customer_address'], 
                                'branch_id'         => $value['company_id'],
                                'customer_type'     => $value['customer_type'],
                                'region_id'         => $value['region_id'],
                                'area_id'           => $value['area_id'],
                                'pin_code'          => $value['pin_code'],
                                'customer_emp_id'   => $value['customer_id'],
                                'created_at'        => date('Y-m-d H:i:s') , 
                                'updated_at'        => date('Y-m-d H:i:s')
                            ];

                            $password = md5($value['customer_mobile']);

                            $values2[] = [
                                'firstname'  => $value['customer_name'] , 
                                'email'      => $value['customer_email'],
                                'mobile'     => $value['customer_mobile'],
                                'username'   => $value['customer_name'],
                                'country_code'  => $value['country_code'],
                                'address'    => $value['customer_address'], 
                                'branch_id'  => $value['company_id'],
                                'region_id'  => $value['region_id'],
                                'area_id'    => $value['area_id'],
                                'role'       => 8 ,
                                'password'   => $password ,
                                'customerid' => $value['customer_id'],
                                'created_at' => date('Y-m-d H:i:s') , 
                                'updated_at' => date('Y-m-d H:i:s')
                            ];

                        } else{
                            return 2;
                        }
                    } else{
                        return 4;
                    }
                }
                if(count($values) > 0){
                            
                    DB::table('gpff_customer')
                    ->insert($values);

                    DB::table('gpff_users')
                    ->insert($values2);
                    
                    return 1;
                }else{
                    return 0;
                }
            }
        }else{
            return 3;
        }
    }
    //Update Customer details
    public function updateCustomers($users) 
    {   

        /*$value = array(
            'firstname' => ucwords($users->customer_name) , 
            'username'  => $users->customer_mobile , 
            'email'     => $users->customer_email ,
            'mobile'    => $users->customer_mobile ,
            'country_code' =>$users->country_code,
            'country'   => $users->customer_country , 
            'address'   => $users->customer_address , 
            'role'      => 8 ,
            'branch_id'   => $users->branch_id ,
            'region_id'   => $users->region_id ,
            'area_id'   => $users->area_id , 
            'area_manager_id'=> $users->area_manager_id , 
            'avatar'    => $users->avatar ,
            'created_at'=> date('Y-m-d H:i:s') , 
            'updated_at'=> date('Y-m-d H:i:s')
        );

        DB::table('gpff_users')
        ->where('user_id', $users->customer_login_id)
        ->update($value);*/


        $values = array(
            'customer_login_id' => $users->customer_login_id ,
            'customer_name'     => ucwords($users->customer_name) , 
            'customer_email'    => $users->customer_email , 
            'customer_mobile'   => $users->customer_mobile ,
            'customer_city'     => $users->customer_city ,
            'customer_country'  => $users->customer_country,
            'vendor'            => $users->vendor,
            'customer_fax'      => $users->customer_fax , 
            'country_code'      => $users->country_code ,
            'customer_address'  => $users->customer_address , 
            'customer_location' => $users->customer_location ,
            'customer_lat'      => $users->customer_lat ,
            'customer_lan'      => $users->customer_lan , 
            'remarks'           => $users->remarks,
            'remarks_type'      => $users->remarks_type,
            'dob'               => $users->dob,
            'marriage_status'   => $users->marriage_status,
            'facebook'          => $users->facebook,
            'twitter'           => $users->twitter,
            'instagram'         => $users->instagram,
            'alternate_mobile_number' => $users->alternate_mobile_number,
            'website'           => $users->website ,
            'branch_id'         => $users->branch_id ,
            'region_id'         => $users->region_id ,
            'area_id'           => $users->area_id , 
            'pin_code'          => $users->pin_code , 
            'credit_limit'      => $users->credit_limit, //NEW CHANGES
            'customer_cr_id'    => $users->customer_cr_id,
            'area_manager_id'   => $users->area_manager_id,
            'avatar'            => $users->avatar ,
            'customer_type'     => $users->customer_type,
            'customer_emp_id'   => $users->customer_emp_id,
            'updated_at'    => date('Y-m-d H:i:s')
            );
        return  DB::table('gpff_customer')
                // ->where('vendor', 2)
                ->where('customer_id', $users->customer_id)
                ->update($values);
    }
    //Delete Customer details
    public function deleteCustomers($users)
    {   
        $cusdetails =   DB::table('gpff_customer')
                        // ->where('vendor', 2)
                        ->where('customer_id', $users->customer_id)
                        ->First();

        DB::table('gpff_users')
        ->where('user_id', $cusdetails->customer_login_id)
        ->delete();

        return  DB::table('gpff_customer')
                // ->where('vendor', 2)
                ->where('customer_id', $users->customer_id)
                ->delete();
    }
    //Get Indivitual Customer Details
    public function getIndCustomers($users)
    {   
        return  DB::table('gpff_customer')
                // ->where('vendor', 2)
                ->where('customer_id', $users->customer_id)
                ->get();
    }
    //Get Indivitual Customer Details
    public function getEmpBaseCustomers($users)
    {   
        return  DB::table('gpff_customer')
                ->where('customer_emp_id', $users->customer_id)
                ->get();
    }
    public function getAllCustomers() 
    {
        $data =[];
        $data['totaldata']= DB::table('gpff_customer as gcus')
                            ->join('gpff_branch as gbran','gcus.branch_id','gbran.branch_id')
                            ->join('gpff_region as gregi','gcus.region_id','gregi.region_id')
                            ->where('vendor', 2)
                            ->orderBy('gcus.updated_at','DESC')
                            ->get();
         return $data;   
    }
    // Get User Based Customers Details
    public function getUserBasedCustomers($users)
    {   
        return  DB::table('gpff_customer')
                ->where('vendor', 2)
                ->where('customer_cr_id', $users->customer_cr_id)
                ->orderBy('updated_at','DESC')
                ->get();
    }
    public function customerActiveOrDeactive($users) 
    {   
        $cusdetails =   DB::table('gpff_customer')
                        // ->where('vendor', 2)
                        ->where('customer_id', $users->customer_id)
                        ->First();

        $value = array(
                'is_active'  => $users->is_active ,  
                'updated_at' => date('Y-m-d H:i:s')
             );
        DB::table('gpff_users')
        ->where('user_id', $cusdetails->customer_login_id)
        ->update($value);

        $values = array(
                'is_active'  => $users->is_active ,  
                'updated_at' => date('Y-m-d H:i:s')
             );
        return DB::table('gpff_customer')
        // ->where('vendor', 2)
        ->where('customer_id', $users->customer_id)
        ->update($values);  
    }
    //Check Customer mobile Number Already Exist
    public function getCustomermobilenumber($users) 
    {
        return DB::table('gpff_customer')
        // ->where('vendor', 2)
        ->where('customer_mobile', $users->customer_mobile)
        ->orderBy('updated_at', 'DESC')
        ->get();
    }
    //Check Customer Email Already Exist
    public function getCustomerEmail($users) 
    {
        return DB::table('gpff_customer')
        // ->where('vendor', 2)
        ->where('customer_email', $users->customer_email)
        ->orderBy('updated_at', 'DESC')
        ->get();
    }
    // Fetch Region Based Region Manager List GEt
    public function getBranchBasRegManage($users){
        return  DB::table('gpff_users')
                ->where('branch_id', $users->branch_id)
                ->where('role', 3)
                ->get(['user_id','firstname','lastname','username','email','role','mobile','cr_by_id','region_id','branch_id','user_attendance_status']);
    }
// Fetch Branch Based Field Officer List GEt
    public function getBranchBaseFO($users){
        return  DB::table('gpff_users')
                ->where('branch_id', $users->branch_id)
                ->where('role', 5)
                ->get(['user_id','firstname','lastname','username','email','role','mobile','cr_by_id','region_id','branch_id','user_attendance_status']);
    }
    // Fetch Area Based Area Manager List GEt
    public function getRegionBasAreaManage($users){
        return  DB::table('gpff_users')
                ->where('region_id', $users->region_id)
                ->where('role', 4)
                ->get(['user_id','firstname','lastname','username','email','role','mobile','cr_by_id','region_id','branch_id','area_id','user_attendance_status']);
    }
    // Fetch Customer Based Region
    public function getRegionBasCustomer($users){
        return  DB::table('gpff_customer')
                ->where('vendor', 2)
                ->where('region_id', $users->region_id)
                ->orderBy('customer_name','ASC')
                ->get();
    }
    // Fetch Customer Based Area
    public function getAreaBasCustomer($users){
        return  DB::table('gpff_customer')
                ->where('vendor', 2)
                ->where('area_id', $users->area_id)
                ->get();
    }
    // Fetch Customer Based Area ALL
    public function getMultiAreaBasCustomer($users){

        // print_r([$users->area_id]);
        // exit;

// 25d55ad283aa400af464c76d713c07ad

        return  DB::table('gpff_customer')
                ->where('vendor', 2)
                ->whereIn('area_id',$users->area_id)
                ->orderBy('customer_name','ASC')
                ->get();
    }
//End User Details
////////////////////////////////////////////
//Normal Notification Management Api Calls//
///////////////////////////////////////////
    //Fetch All Notifcation
    public function fetchNotification($users){

        return DB::table('gpff_notification')
                ->where('isread','0')
                ->where('isdelete','0')
                ->where('notification_user_id',$users->notification_user_id)
                ->orderBy('notification_id', 'DESC')
                ->get(['notification_id','user_name','message','page_id','created_at']);
    }
    //Fetch All Notifcation
    public function getAppNotification($users){

        return DB::table('gpff_notification')
                ->where('isread','0')
                ->where('isdelete','0')
                ->where('page_id', 'UNKNOWN')
                ->where('notification_user_id',$users->notification_user_id)
                ->orderBy('notification_id', 'DESC')
                ->select('notification_id','user_name','message','page_id','created_at')
                ->paginate(10);
    }
    //Fetch All Notifcation
    public function notificationStatus($users){

        $values = array(
            'isread' => '1',
            'updated_at' => date('Y-m-d H:i:s')
        );
        return DB::table('gpff_notification')
            ->where('notification_id',$users->notification_id)
            ->update($values);
    }
    //Delete Notifcation
    public function deleteNotification($users){

             DB::table('gpff_notification')
            ->where('notification_id', $users->notification_id)
            ->delete();

            return 1;
    }
    //Delete All Notifcation
    public function deleteAllNotification($users){

        $values = array(
            'isread' => '1',
            'updated_at' => date('Y-m-d H:i:s')
        );
            DB::table('gpff_notification')
            ->where('notification_user_id',$users->notification_user_id)
            ->where('isread','0')
            ->update($values);
        return 1;
    }
    // Get Indivitual Id Based Notification Fetch
    public function getIndBasedNotiDetails($users){

        return DB::table('gpff_notification')
                ->where('notification_id',$users->notification_id)
                ->get();
    }

    //Get Users Based Details
    public function getUserBasedDetails($users)
    {   
        $userdata = DB::table('gpff_users')
                    ->where('user_id', $users->user_id)
                    ->First();

        if($userdata->role == 2){

            $myArray = explode(',', $userdata->branch_id);

            $branch_details = DB::table('gpff_branch')
                              ->whereIn('branch_id', $myArray)
                              ->get();
            $region_details = '';
            $area_details = '';
        } else if ($userdata->role == 3) {

            $myArray = explode(',', $userdata->region_id);

            $branch_details = DB::table('gpff_branch')
                              ->where('branch_id', $userdata->branch_id)
                              ->get();
            $region_details = DB::table('gpff_region')
                              ->whereIn('region_id', $myArray)
                              ->get();
            $area_details = '';
        } else if ($userdata->role == 4) {

            $myArray = explode(',', $userdata->area_id);

            $branch_details = DB::table('gpff_branch')
                              ->where('branch_id', $userdata->branch_id)
                              ->get();
            $region_details = DB::table('gpff_region')  
                              ->where('region_id', $userdata->region_id)
                              ->get();
            $area_details = DB::table('gpff_area')
                              ->whereIn('area_id', $myArray)
                              ->get();
        } else if ($userdata->role == 7 || $userdata->role == 9) {

            $branch_details = DB::table('gpff_branch')
                              ->where('branch_id', $userdata->branch_id)
                              ->get();
            $region_details = DB::table('gpff_region') 
                              ->where('region_id', $userdata->region_id)
                              ->get();
            $area_details = '';
        }

        $result = array(
            'branch_details'    =>  $branch_details,
            'region_details'    =>  $region_details,
            'area_details'      =>  $area_details
            );

        return $result;
    }

        //Get warehouse Based Details
    public function getWarehBasedDetails($users)
    {   
        $waredata = DB::table('gpff_warehouse')
                    ->where('warehouse_id', $users->warehouse_id)
                    ->First();

        if($waredata){
            $branch_details = DB::table('gpff_branch')
                          ->where('branch_id', $waredata->branch_id)
                          ->get();
            $region_details = DB::table('gpff_region')
                          ->where('region_id', $waredata->region_id)
                          ->get();
        
            $result = array(
                'branch_details'    =>  $branch_details,
                'region_details'    =>  $region_details,
                );
        }else{
            $result = "";
        }
        return $result;
    }

//Others
    //public file path
    public function getFilePath($file_name){   
        return $file_name->getClientOriginalName();
    }

    //Password Send To the Email Notification
    public function sendUserEmail($user,$username,$pass,$template,$name){
        $data = array('pass'=>$pass,'username'=>$username,'name'=>$name);

        $emailObject = array(
             'user_id' => 0,
             'email_to' => $user,
             'email_cc' => '',
             'email_bcc' => '',
             'email_subject' => 'You Have New Notification from GPFF',
             'email_content' => '',
             'email_template' => $template,
             'email_template_data' => json_encode($data),
             'email_file_path' => '',
             'email_file_name' => '',
             'created_date' => date('Y-m-d H:i:s'),
             'updated_date' => date('Y-m-d H:i:s')
            ); 

            $cmn = new Common();
            $cmn->insertEmailQueue($emailObject);
            $cmn->sendEMAIL($emailObject);


        return "Basic Email Sent. Check your inbox."; 
    }


//Pagination Call
    // Fetch (All)Customers Details
    /*public function getAllCustomersForApp($users) 
    {   
        $data = DB::table('gpff_customer as gcus')
                ->join('gpff_branch as gbran','gcus.branch_id','gbran.branch_id')
                ->join('gpff_region as gregi','gcus.region_id','gregi.region_id')
                ->orderBy('gcus.customer_id','DESC');
              
        $total_count = $data->count();      

        $data->offset($users->start);
        $data->limit($users->length);
        $data = $data->get();

        $total_filter_count = DB::select(DB::Raw('SELECT FOUND_ROWS() AS    totalfiltercount;'));

            return array(
                    0 => $total_count,
                    1 => $total_filter_count[0]->totalfiltercount,
                    2 => $data
                );
    }*/
    public function getAllCustomersForApp($users) 
    {   
        $col = array(
            0 => 'gcus.customer_emp_id',
            1 => 'gbran.branch_name',
            2 => 'gregi.region_name',
            3 => 'gcus.customer_type',
            4 => 'gcus.customer_name',
            5 => 'gcus.customer_email',
            6 => 'gcus.customer_mobile',
            7 => 'gcus.customer_city',
            8 => 'gcus.customer_id',
        );

        $data = DB::table('gpff_customer as gcus')
                ->join('gpff_branch as gbran','gcus.branch_id','gbran.branch_id')
                ->join('gpff_region as gregi','gcus.region_id','gregi.region_id')
                ->orderBy('gcus.customer_id','DESC')
                ->orderBy($col[$users->orderColumn], $users->orderDir)
                ->select(DB::RAW('SQL_CALC_FOUND_ROWS gcus.customer_emp_id'),'gbran.branch_name','gregi.region_name','gcus.customer_type','gcus.customer_name','gcus.customer_email','gcus.customer_mobile','gcus.customer_city','gcus.customer_id');
              
        $total_count = $data->count();      
        if(!empty($users->searchKey)) {
                $qry = '(gcus.customer_emp_id like "%'.$users->searchKey.'%" OR 
                        gbran.branch_name like "%'.$users->searchKey.'%" OR 
                        gregi.region_name like "%'.$users->searchKey.'%" OR 
                        gcus.customer_type like "%'.$users->searchKey.'%" OR 
                        gcus.customer_name like "%'.$users->searchKey.'%" OR 
                        gcus.customer_email like "%'.$users->searchKey.'%" OR 
                        gcus.customer_mobile like "%'.$users->searchKey.'%" OR 
                        gcus.customer_city like "%'.$users->searchKey.'%" OR 
                        gcus.customer_id like "%'.$users->searchKey.'%")';
                        $data->whereRaw($qry);
                        $data->offset($users->start);
                        $data->limit($users->length);
                        $data = $data->get();

                $total_filter_count = DB::select(DB::Raw('SELECT FOUND_ROWS() AS    totalfiltercount;'));
            } else {
                $data->offset($users->start);
                $data->limit($users->length);
                $data = $data->get();

                $total_filter_count = DB::select(DB::Raw('SELECT FOUND_ROWS() AS    totalfiltercount;'));
        }
            return array(
                    0 => $total_count,
                    1 => $total_filter_count[0]->totalfiltercount,
                    2 => $data
                );
    }

    public function sendSMSCampaign($request) {
        $cmn = new Common();

        $result = '';
        
        $cusDetails = $request->customers;
        $message = $request->message;

        foreach ($cusDetails as $key => $value) {
            $reqBody = array();
           /* $reqBody['tag'] = 'sendSms';
            $reqBody['projectcode'] = 'gpff';
            $reqBody['type'] = 'BOOMSMS';
            $reqBody['from'] = 'BOOM SMS';*/
            $reqBody['message'] = $message;
            $reqBody['to'] = $value['customer_mobile'];

            $result = $cmn->sendSMS($reqBody);
        }

        return $result;

    }

    public function sendEMAILCampaign($request) {
        $cmn = new Common();

        $result = '';
        $cusDetails = $request->customers;
        $message = $request->message;
        $subject = $request->subject;

        foreach ($cusDetails as $key => $value) {
            if($value['customer_email'] != null){
                $reqBody = array();
                //$reqBody['tag'] = 'sendEmail';
                // $reqBody['projectcode'] = 'gpff';
                $reqBody['subject'] = $subject;
                $reqBody['message'] = $message;
                $reqBody['body'] = $message;
                $reqBody['bcc'] = '';
                $reqBody['cc'] = '';
                $reqBody['email_to'] = $value['customer_email'];
    
               $cmn->sendEMAILall($reqBody);
            }
        }
        $result = 1;
        return $result;

    }

    //Notification
    public function sendNotiCampaign($request) {
        $cmn = new Common();
        $result = '';
        $user = DB::table('gpff_users')
                        ->where('user_id',$request->user_id)
                        ->first();

        $cusDetails = $request->customers;
        foreach ($cusDetails as $key => $value) {
            
            $customerid = DB::table('gpff_users')
                        ->where('customerid',$value['customer_emp_id'])
                        ->first();

            if($customerid){
                $notify_user_id = $customerid->user_id;
                $message = $request->message;
                $file = $request->file;
                $title = $request->title;
                $page_id = 'PRNOT';
                $user_id = $request->user_id;
                $user_name = $user->firstname;
                $result = $cmn->insertNotification($user_id,$user_name,$notify_user_id,$message,$page_id,$file,$title);
            }
            $result = 1;
        }
        return $result;

    }

    //Fetch All Notifcation
    public function getAppPromoNotification($users){

        return DB::table('gpff_notification')
                ->where('isread','0')
                ->where('isdelete','0')
                ->where('page_id','PRNOT')
                ->where('notification_user_id',$users->notification_user_id)
                ->orderBy('notification_id', 'DESC')
                //->select('notification_id','user_name','message','page_id','created_at')
                ->paginate(10);
    }

    public function storeBiller($users) 
    {   
        $values = array(
            'biller_name'        => ucwords($users->biller_name) , 
            'email_id'          => $users->email_id ,
            'mobile'            => $users->mobile ,
            'country_code'      => $users->country_code,
            'country'           => $users->country,
            'warehouse_id'      => $users->warehouse_id,
            'cr_by_id'          => $users->cr_by_id , 
            'created_at'        => date('Y-m-d H:i:s') , 
            'updated_at'        => date('Y-m-d H:i:s')
        );
        return  DB::table('gpff_biller')
                ->insert($values);
    }
    public function updateBiller($users) 
    {   
        $values = array(
            'biller_name'        => ucwords($users->biller_name) , 
            'email_id'          => $users->email_id ,
            'mobile'            => $users->mobile ,
            'country_code'      => $users->country_code,
            'country'           => $users->country,
            'updated_at'        => date('Y-m-d H:i:s')
        );
        return  DB::table('gpff_biller')
                ->where('biller_id',$users->biller_id)
                ->update($values);
    }
    public function getBillerDetails($users) 
    {   

        $biller_details = DB::table('gpff_biller')
                         ->where('warehouse_id',$users->warehouse_id)
                         ->orderBy('updated_at','DESC');
        if($users->biller_id != ''){
            $biller_details = $biller_details->where('biller_id',$users->biller_id);
        }
        return $biller_details->get();
    }
    //Delete Biller details
    public function deleteBiller($users)
    {
        return  DB::table('gpff_biller')
                ->where('biller_id', $users->biller_id)
                // ->where('warehouse_id', $users->warehouse_id)
                ->delete();
    }

    //Active Deactive Biller Details
    public function updateBillerStatus($users) 
    {
        $values = array(
                'biller_status'  => $users->biller_status ,  
                'updated_at' => date('Y-m-d H:i:s')
             );

        return DB::table('gpff_biller')
        ->where('biller_id', $users->biller_id)
        ->update($values);  
    }

    public function getDoerList($users) 
    {   

        $user_details = DB::table('gpff_users as guse')
                        ->join('gpff_branch as gbran','guse.branch_id','gbran.branch_id')
                        ->join('gpff_region as gregi','guse.region_id','gregi.region_id')
                        ->where('guse.role','12')
                         ->orderBy('guse.updated_at','DESC');
        if($users->branch_id != ''){
            $user_details = $user_details->whereIn('guse.branch_id',$users->branch_id);
        }
        if($users->region_id != ''){
            $user_details = $user_details->whereIn('guse.region_id',$users->region_id);
        }
        return $user_details->get();
    }

    public function getCheckerList($users) 
    {   

        $user_details = DB::table('gpff_users as guse')
                        ->join('gpff_branch as gbran','guse.branch_id','gbran.branch_id')
                        ->join('gpff_region as gregi','guse.region_id','gregi.region_id')
                        ->where('guse.role','10')
                         ->orderBy('guse.updated_at','DESC');
        if($users->branch_id != ''){
            $user_details = $user_details->whereIn('guse.branch_id',$users->branch_id);
        }
        if($users->region_id != ''){
            $user_details = $user_details->whereIn('guse.region_id',$users->region_id);
        }
        return $user_details->get();
    }

    public function getFinanceUserList($users) 
    {   

        $user_details = DB::table('gpff_users as guse')
                        ->join('gpff_branch as gbran','guse.branch_id','gbran.branch_id')
                        ->join('gpff_region as gregi','guse.region_id','gregi.region_id')
                        ->where('guse.role','9')
                         ->orderBy('guse.updated_at','DESC');
        if($users->branch_id != ''){
            $user_details = $user_details->whereIn('guse.branch_id',$users->branch_id);
        }
        if($users->region_id != ''){
            $user_details = $user_details->whereIn('guse.region_id',$users->region_id);
        }
        return $user_details->get();
    }

    public function getCustomersList($users) 
    {   

        $user_details = DB::table('gpff_customer as gcus')
                            ->join('gpff_branch as gbran','gcus.branch_id','gbran.branch_id')
                            ->join('gpff_region as gregi','gcus.region_id','gregi.region_id')
                            ->join('gpff_area as garea','gcus.area_id','garea.area_id')
                            ->where('vendor', 2)
                            ->orderBy('gcus.updated_at','DESC');
        if($users->branch_id != ''){
            $user_details = $user_details->whereIn('gcus.branch_id',$users->branch_id);
        }
        if($users->region_id != ''){
            $user_details = $user_details->whereIn('gcus.region_id',$users->region_id);
        }
        if($users->area_id != ''){
            $user_details = $user_details->whereIn('gcus.area_id',$users->area_id);
        }
        // print_r("Hii");
        // print_r(json_encode($user_details->get()));
        // exit;
        return $user_details->get();
    }

}

