<?php

namespace App\Http\Controllers;

use Closure;
use DB;
use Mail;
use Carbon\Carbon;

require(base_path().'/app/Http/Middleware/Common.php');

class Dashboard
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        return $next($request);
    }

    // Admin Dashboard Count
    //  1-superadmin ,2-Brachadmin,3-Regionadmin,4-Areamanager,5-fieldofficer,6-salesref,7-stockist,8-customer
    public function getAdminDashCount($Dashboards)
    {
        $userList = DB::table('gpff_users')
                        ->whereIn('role',['2','3','4'])
                        ->where('is_active',1)
                        ->count();

        $fieldofficer = DB::table('gpff_users')
                        ->where('role',5)
                        ->where('is_active',1)
                        ->count();

        $customer   =   DB::table('gpff_customer')
                        ->where('vendor', 2)
                        //->where('is_active',1)
                        ->count();

        $task       =   DB::table('gpff_task')
                        ->count();

        $stockist = DB::table('gpff_users')
                        ->where('role',7)
                        ->where('is_active',1)
                        ->count();

        $product = DB::table('gpff_product')
                        ->where('product_status',1)
                        ->count();

        $branch = DB::table('gpff_branch')
                        ->where('branch_status',1)
                        ->count();

        $region = DB::table('gpff_region')
                        ->where('region_status',1)
                        ->count();

        $area = DB::table('gpff_area')
                        ->where('area_status',1)
                        ->count();

        $warehouse = DB::table('gpff_warehouse')
                        ->where('warehouse_status',1)
                        ->count();

        $gift = DB::table('gpff_gift')
                        ->where('gift_status',1)
                        ->count();

        $samples = DB::table('gpff_samples')
                        ->where('samples_status',1)
                        ->count();

        $order = DB::table('gpff_order')
                        ->count();

        $result = array(

            'userList_count'        =>  $userList,
            'fieldofficer_count'    =>  $fieldofficer,
            'customer_count'        =>  $customer,
            'task_count'            =>  $task,
            'stockist_count'        =>  $stockist,
            'product_count'         =>  $product,
            'branch_count'          =>  $branch,
            'region_count'          =>  $region,
            'area_count'            =>  $area,
            'warehouse_count'       =>  $warehouse,
            'gift_count'            =>  $gift,
            'samples_count'         =>  $samples,
            'order_count'           =>  $order
            );

        return $result;
    }
    // Manager Dashboard Count
    public function getAreaDashCount($Dashboards)
    {   
        $data = DB::table('gpff_users')
                    ->where('user_id', $Dashboards->area_manager_id)
                    ->First(['area_id','region_id']);

        $myArray = explode(',', $data->area_id);

        $fieldofficer = DB::table('gpff_users')
                        ->where('area_manager_id', $Dashboards->area_manager_id)
                        ->where('role', 5)->count();
                        // ->get();

        $task       = DB::table('gpff_task')
                        ->whereIn('area_id', $myArray)->count();
                        // ->get();


        $customer   =   DB::table('gpff_customer')
                        ->where('vendor', 2)
                        ->whereIn('area_id', $myArray)->count();
                        // ->get();

        $leave      =   DB::table('gpff_leave')
                        ->where('area_manager_id', $Dashboards->area_manager_id)->count();
                        // ->get();

        $order      =   DB::table('gpff_order')
                        ->whereIn('area_id', $myArray)->count();
                        // ->get();

        $product    =   DB::table('gpff_product')
                        ->where('region_id', $data->region_id)->count();
                        // ->get();

        $gift    =      DB::table('gpff_gift')
                        ->where('region_id', $data->region_id)->count();
                        // ->get();

        $samples    =   DB::table('gpff_samples')
                        ->where('region_id', $data->region_id)->count();
                        // ->get();

        $result = array(

            'fieldofficer_count'    =>  $fieldofficer,
            'task_count'            =>  $task,
            'customer_count'        =>  $customer,
            'leave_count'           =>  $leave,
            'order_count'           =>  $order,
            'product_count'         =>  $product,
            'gift_count'            =>  $gift,
            'samples_count'         =>  $samples


            // 'fieldofficer_count'    =>  count($fieldofficer),
            // 'task_count'            =>  count($task),
            // 'customer_count'        =>  count($customer),
            // 'leave_count'           =>  count($leave),
            // 'order_count'           =>  count($order),
            // 'product_count'         =>  count($product),
            // 'gift_count'            =>  count($gift),
            // 'samples_count'         =>  count($samples)

            );

        return $result;
    }
    //Region Manager Dashboard Count
    public function getRegionDashCount($Dashboards)
    {   
        $data = DB::table('gpff_users')
                    ->where('user_id', $Dashboards->user_id)
                    ->First(['region_id']);

        $myArray = explode(',', $data->region_id);

        $areamanager   =   DB::table('gpff_users')
                        ->whereIn('region_id', $myArray)
                        ->where('role', 4)->count();
                        // ->get();

        $fieldofficer = DB::table('gpff_users')
                        ->whereIn('region_id', $myArray)
                        ->where('role', 5)->count();
                        // ->get();

        $customer   =   DB::table('gpff_customer')
                        ->where('vendor', 2)
                        ->whereIn('region_id', $myArray)->count();
                        // ->get();

        $task       =   DB::table('gpff_task')
                        ->whereIn('region_id', $myArray)->count();
                        // ->get();

        $order      =   DB::table('gpff_order')
                        ->whereIn('region_id', $myArray)->count();
                        // ->get();

        $product    =   DB::table('gpff_product')
                        ->whereIn('region_id', $myArray)->count();
                        // ->get();

        $gift    =      DB::table('gpff_gift')
                        ->whereIn('region_id', $myArray)->count();
                        // ->get();

        $samples    =   DB::table('gpff_samples')
                        ->whereIn('region_id', $myArray)->count();
                        // ->get();

        $result = array(

            // 'areamanager_count'     =>  count($areamanager),
            // 'fieldofficer_count'    =>  count($fieldofficer),
            // 'customer_count'        =>  count($customer),
            // 'task_count'            =>  count($task),
            // 'order_count'           =>  count($order),
            // 'product_count'         =>  count($product),
            // 'gift_count'            =>  count($gift),
            // 'samples_count'         =>  count($samples)
            
            'areamanager_count'     =>  $areamanager,
            'fieldofficer_count'    =>  $fieldofficer,
            'customer_count'        =>  $customer,
            'task_count'            =>  $task,
            'order_count'           =>  $order,
            'product_count'         =>  $product,
            'gift_count'            =>  $gift,
            'samples_count'         =>  $samples

            );

        return $result;
    }
    //Branch Manager Dashboard Count
    public function getBranchDashCount($Dashboards)
    {   
        $data = DB::table('gpff_users')
                    ->where('user_id', $Dashboards->user_id)
                    ->First(['branch_id']);

        $myArray = explode(',', $data->branch_id);

        $region   =     DB::table('gpff_region')
                        ->whereIn('branch_id', $myArray)
                        ->count();

        $regionmanager  = DB::table('gpff_users')
                        ->whereIn('branch_id', $myArray)
                        ->where('role', 3)
                        ->count();

        $area   =       DB::table('gpff_area')
                        ->whereIn('branch_id', $myArray)
                        ->count();

        $areamanager   =   DB::table('gpff_users')
                        ->whereIn('branch_id', $myArray)
                        ->where('role', 4)
                        ->count();

        $fieldofficer = DB::table('gpff_users')
                        ->whereIn('branch_id', $myArray)
                        ->where('role', 5)
                        ->count();

        $customer   =   DB::table('gpff_customer')
                        ->where('vendor',2)
                        ->whereIn('branch_id', $myArray)
                        ->count();

        $warehouse   =   DB::table('gpff_warehouse')
                        ->whereIn('branch_id', $myArray)
                        ->count();

        $stockist = DB::table('gpff_users')
                        ->whereIn('branch_id', $myArray)
                        ->where('role', 7)
                        ->count();

        $task       =   DB::table('gpff_task')
                        ->whereIn('branch_id', $myArray)
                        ->count();

        $order      =   DB::table('gpff_order')
                        ->whereIn('branch_id', $myArray)
                        ->count();

        $product    =   DB::table('gpff_product')
                        ->whereIn('branch_id', $myArray)
                        ->count();

        $gift    =      DB::table('gpff_gift')
                        ->whereIn('branch_id', $myArray)
                        ->count();

        $samples    =   DB::table('gpff_samples')
                        ->whereIn('branch_id', $myArray)
                        ->count();

        $result = array(
            'region_count'          =>  $region,
            'regionmanager_count'   =>  $regionmanager,
            'area_count'            =>  $area,
            'areamanager_count'     =>  $areamanager,
            'fieldofficer_count'    =>  $fieldofficer,
            'customer_count'        =>  $customer,
            'warehouse_count'       =>  $warehouse,
            'stockist_count'        =>  $stockist,
            'task_count'            =>  $task,
            'order_count'           =>  $order,
            'product_count'         =>  $product,
            'gift_count'            =>  $gift,
            'samples_count'         =>  $samples

            );

        return $result;
    }
    //Stockist Dashboard Count
    public function getStockistDashCount($Dashboards)
    {   
        $data = DB::table('gpff_users')
                    ->where('user_id', $Dashboards->user_id)
                    ->First(['warehouse_id']);

        //$myArray = explode(',', $data->warehouse_id);

        $giftreq   =     DB::table('gpff_giftassign')
                        ->where('warehouse_id', $data->warehouse_id)
                        ->where('giftassign_status', 0)
                        ->get();
                        
        $samplesreq   =   DB::table('gpff_samplesassign')
                        ->where('warehouse_id', $data->warehouse_id)
                        ->where('samplesassign_status', 0)
                        ->get();

        $orderreq      =   DB::table('gpff_order')
                        ->where('warehouse_id', $data->warehouse_id)
                        ->where('order_status', 0)
                        ->get();

        $gift    =      DB::table('gpff_gift_stock')
                        ->where('warehouse_id', $data->warehouse_id)
                        ->get();

        $product    =      DB::table('gpff_product_stock')
                        ->where('warehouse_id', $data->warehouse_id)
                        ->get();

        $samples    =   DB::table('gpff_samples')
                        ->where('warehouse_id', $data->warehouse_id)
                        ->get();

        $result = array(
            'giftreq_count'      =>  count($giftreq),
            'samplesreq_count'   =>  count($samplesreq),
            'orderreq_count'     =>  count($orderreq),
            'product_count'      =>  count($product),
            'samples_count'      =>  count($samples),
            'gift_count'         =>  count($gift)
            );

        return $result;
    }


//Web SIde Dashboard
    // Admin aand Manager Dashboard Count
    public function getDashTaskCount($Dashboards)
    {   
            $db = DB::table('gpff_task');
            // 0-Not Started, 1-In Progress, 2-Hold, 3-Completed
            $result = $db->selectRaw('
                COUNT(*) AS alltask_count,
                SUM(task_status = 0) AS notstarted_count,
                SUM(task_status = 1) AS inprogress_count,
                SUM(task_status = 2) AS hold_count,
                SUM(task_status = 3) AS completed_count
            ')->first();

            return [
                'alltask_count' => $result->alltask_count,
                'notstarted_count' => $result->notstarted_count,
                'inprogress_count' => $result->inprogress_count,
                'hold_count' => $result->hold_count,
                'completed_count' => $result->completed_count,
            ];

    }
    // Admin Dashboard Graph
    public function getAdminDashGraph($Dashboards)
    {   
        $result =   DB::table('gpff_product_stock')
                    ->get(['product_id','category_name','product_genericname','product_sales_qty']);

        return $result;
    }

    // Branch Manager Dashboard Count
    public function getBranchDashTaskCount($Dashboards)
    {   

        $data =     DB::table('gpff_users')
                    ->where('user_id', $Dashboards->user_id)
                    ->First(['branch_id']);

        $myArray = explode(',', $data->branch_id);

        //0-Not Started 1- Inprogress 2-Hold 3-Completed
        $alltask    =   DB::table('gpff_task')
                        ->whereIn('branch_id', $myArray)
                        ->count();

        $notstarted =   DB::table('gpff_task')
                        ->whereIn('branch_id', $myArray)
                        ->where('task_status',0)
                        ->count();

        $inprogress =   DB::table('gpff_task')
                        ->whereIn('branch_id', $myArray)
                        ->where('task_status',1)
                        ->count();

        $hold       =   DB::table('gpff_task')
                        ->whereIn('branch_id', $myArray)
                        ->where('task_status',2)
                        ->count();

        $completed  =   DB::table('gpff_task')
                        ->whereIn('branch_id', $myArray)
                        ->where('task_status',3)
                        ->count();

        $result = array(

            'alltask_count'     =>  $alltask,
            'notstarted_count'  =>  $notstarted,
            'inprogress_count'  =>  $inprogress,
            'hold_count'        =>  $hold,
            'completed_count'   =>  $completed,
            );

        return $result;
    }
    // Branch Dashboard Graph
    public function getBranchDashGraph($Dashboards)
    {   
        $data =     DB::table('gpff_users')
                    ->where('user_id', $Dashboards->user_id)
                    ->First(['branch_id']);

        $myArray = explode(',', $data->branch_id);

        $result =   DB::table('gpff_product_stock')
                    ->whereIn('branch_id', $myArray)
                    ->get(['product_id','category_name','product_genericname','product_sales_qty']);

        return $result;
    }

    // Region Manager Dashboard Count
    public function getRegionDashTaskCount($Dashboards)
    {   

        $data =     DB::table('gpff_users')
                    ->where('user_id', $Dashboards->user_id)
                    ->First(['region_id']);

        $myArray = explode(',', $data->region_id);

        // $db = DB::table('gpff_task')->whereIn('region_id', $myArray)->get(['task_status']);
        // //0-Not Started 1- Inprogress 2-Hold 3-Completed
        // $alltask    =  $db->count();

        // $notstarted =  $db->where('task_status',0)->count();

        // $inprogress =  $db->where('task_status',1)->count();

        // $hold       =  $db->where('task_status',2)->count();

        // $completed  =  $db->where('task_status',3)->count();

        $db = DB::table('gpff_task')
                ->whereIn('region_id', $myArray)
                ->selectRaw('COUNT(*) as total, SUM(task_status = 0) as notstarted, SUM(task_status = 1) as inprogress, SUM(task_status = 2) as hold, SUM(task_status = 3) as completed')
                ->first();

        $result = array(

            'alltask_count'     =>  $db->total,
            'notstarted_count'  =>  $db->notstarted,
            'inprogress_count'  =>  $db->inprogress,
            'hold_count'        =>  $db->hold,
            'completed_count'   =>  $db->completed,
            );

        return $result;
    }
    // Region Dashboard Graph
    public function getRegionDashGraph($Dashboards)
    {   
        $data =     DB::table('gpff_users')
                    ->where('user_id', $Dashboards->user_id)
                    ->First(['region_id']);

        $myArray = explode(',', $data->region_id);

        $result =   DB::table('gpff_product_stock')
                    ->whereIn('region_id', $myArray)
                    ->get(['product_id','category_name','product_genericname','product_sales_qty']);

        return $result;
    }

    // Area Manager Dashboard Count
    public function getAreaDashTaskCount($Dashboards)
    {   
        $db = DB::table('gpff_task')->where('area_manager_id', $Dashboards->user_id);

        $result = $db->selectRaw('
            COUNT(*) AS alltask_count,
            SUM(task_status = 0) AS notstarted_count,
            SUM(task_status = 1) AS inprogress_count,
            SUM(task_status = 2) AS hold_count,
            SUM(task_status = 3) AS completed_count
        ')->first();

        return [
            'alltask_count' => $result->alltask_count,
            'notstarted_count' => $result->notstarted_count,
            'inprogress_count' => $result->inprogress_count,
            'hold_count' => $result->hold_count,
            'completed_count' => $result->completed_count,
        ];

        // $db = DB::table('gpff_task')->where('area_manager_id', $Dashboards->user_id)->get(['task_status']);
        // //0-Not Started 1- Inprogress 2-Hold 3-Completed
        // $alltask    =  $db->count();

        // $notstarted =  $db->where('task_status',0)->count();

        // $inprogress =  $db->where('task_status',1)->count();

        // $hold       =  $db->where('task_status',2)->count();

        // $completed  =  $db->where('task_status',3)->count();

        // $result = array(

        //     'alltask_count'     =>  $alltask,
        //     'notstarted_count'  =>  $notstarted,
        //     'inprogress_count'  =>  $inprogress,
        //     'hold_count'        =>  $hold,
        //     'completed_count'   =>  $completed,

        //     );

        // return $result;
    }
    // Manager Dashboard Graph
    public function getManagDashGraph($Dashboards)
    {   
        $data = DB::table('gpff_users')
                ->where('area_manager_id', $Dashboards->area_manager_id)
                ->First(['region_id']);

        if($data){
            $result =   DB::table('gpff_product_stock')
                    ->where('region_id', $data->region_id)
                    ->get(['product_id','category_name','product_genericname','product_sales_qty']);
        }else{
            $result = "";
        }
        return $result;
    }


    // Manager Dashboard Count
    public function getManagDashCount($Dashboards)
    {   
        // $data = DB::table('gpff_users')
        //     ->where('user_id', $Dashboards->area_manager_id)
        //     ->first(['area_id']);

        // $myArray = explode(',', $data->area_id);

        // $fieldofficerCount = DB::table('gpff_users')
        //     ->whereIn('area_id', $myArray)
        //     ->where('role', 5)
        //     ->count();

        // $customerCount = DB::table('gpff_customer')
        //     ->where(function ($query) use ($Dashboards) {
        //         $query->where('vendor', 2)
        //             ->where('area_manager_id', $Dashboards->area_manager_id)
        //             ->orWhere('area_manager_id', NULL);
        //     })
        //     ->count();

        // $orderCount = DB::table('gpff_order')
        //     ->where('area_manager_id', $Dashboards->area_manager_id)
        //     ->count();

        // $pieCustomerCount = DB::table('gpff_Revisit')
        //     ->where('area_manager_id', $Dashboards->area_manager_id)
        //     ->distinct('customer_id')
        //     ->count();

        // $result = [
        //     'fieldofficer_count' => $fieldofficerCount,
        //     'customer_count' => $customerCount,
        //     'order_count' => $orderCount,
        //     'pie_customer_count' => $pieCustomerCount,
        // ];

        // return $result;
        $data = DB::table('gpff_users')
                    ->where('user_id', $Dashboards->area_manager_id)
                    ->First(['area_id']);

        $myArray = explode(',', $data->area_id);

        //print_r();
        $fieldofficer = DB::table('gpff_users')
                        ->whereIn('area_id', $myArray)
                        ->where('role', 5)->count();
                        // ->get();

        $customer   =   DB::table('gpff_customer')
                        ->where('vendor', 2)
                        ->where('area_manager_id', $Dashboards->area_manager_id)->count();
                        // ->get();

        $customer1   =   DB::table('gpff_customer')
                        ->where('vendor', 2)
                        ->where('area_manager_id', NULL)->count();
                        // ->get();

        $order      =   DB::table('gpff_order')
                        ->where('area_manager_id', $Dashboards->area_manager_id)->count();
                        // ->get();

        $pie_customer    =   DB::table('gpff_Revisit')
                            ->where('area_manager_id', $Dashboards->area_manager_id)
                            ->distinct('customer_id')->count();
                            // ->get();

        $result = array(
            'fieldofficer_count'    =>  $fieldofficer,
            'customer_count'        =>  $customer + $customer1,
            'order_count'           =>  $order,
            'pie_customer_count'    =>  $pie_customer
            // 'fieldofficer_count'    =>  count($fieldofficer),
            // 'customer_count'        =>  count($customer) + count($customer1),
            // 'order_count'           =>  count($order),
            // 'pie_customer_count'    =>  count($pie_customer)

            );

        return $result;
    }    
}
