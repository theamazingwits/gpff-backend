<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
// use Illuminate\Http\Request;
use File;
use Mail;
require(base_path().'/app/Http/Middleware/Purchase.php');

class PurchaseController extends Controller{
		private $Purchase;
		function __construct() {
		if (!isset(self::$this->Purchases)) {
			$this->Purchases = new Purchase();
    		
		} 
    	
  	}

	// place the Purchase
	public function addPurchase(Request $request){
		$Purchases = new Purchase();
		$Purchases->warehouse_id 			= $request->input('warehouse_id');
		$Purchases->supplier_id				= $request->input('supplier_id');
		$Purchases->supplier_name 			= $request->input('supplier_name');
		$Purchases->supplier_code			= $request->input('supplier_code');
		$Purchases->date					= $request->input('date');
		$Purchases->purchase_product_list	= $request->input('purchase_product_list');
		$Purchases->amount					= $request->input('amount');
		$Purchases->due 					= $request->input('due');
		$Purchases->payment_type 			= $request->input('payment_type');
		$Purchases->payment_status 			= $request->input('payment_status');
		$Purchases->user_id 				= $request->input('user_id');
		$result = $Purchases->addPurchase($Purchases);
		// $result = $this->$Purchases->addPurchase($request->input())
		if($result == 1){
			$response = response()->json(['status' => 'success', 'message' => 'Purchase added successfully']);
		} else{
			$response = response()->json(['status' => 'success', 'message' => 'Error in Purchase']);
		}

		return $response;

	}
	public function getAllPurchase(){

		$Purchases = new Purchase();

		$result = $Purchases->getAllPurchase();

		if(count($result)>0){
			$response = response()->json(['status'=>'success', 'message'=>"Purchase data", 'result' => $result]);
		}else{
			$response = response()->json(['status'=>'Failed', 'message'=>"No Purchase data"]);
		}
		return $response;
	}
	public function getIndividualPurchase($purchase_id){
		$Purchases = new Purchase();
		$Purchases->purchase_id = $purchase_id;
		$result = $Purchases->getIndividualPurchase($Purchases);

		if(count($result)>0){
			$response = response()->json(['status'=>'success', 'message'=>"Purchase data", 'result' => $result]);
		}else{
			$response = response()->json(['status'=>'Failed', 'message'=>"No Purchase data"]);
		}
		return $response;

	}
}
?>