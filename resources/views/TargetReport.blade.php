<!DOCTYPE html>
<html lang="en">
<head>
	<title>Table V01</title>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css"/>

	<style>
		
			  
			  #logo {
				float: left;
				margin-top: 8px;
				margin-left: 30%;
			  }
			  
			  #logo img {
				height: 70px;
			  }

			  .display-1 {
				.font(@font-size-display-1, @line-height-display-1, @letter-spacing-display-1);
				font-weight: @font-weight-display-1;
				color: @font-color-display-1;
				text-transform: inherit;
				
				}
				
				h2 {
					text-align: center;
					padding-top: 20px 0;
				}
				
				.table-bordered {
					border: 1px solid #ddd !important;
				}
				
				table caption {
					padding: .5em 0;
				}
				
				table tfoot tr td {
					text-align: center !important;
				}
				
				@media (max-width: 39.9375em) {
					.tablesaw-stack tbody tr:not(:last-child) {
						border-bottom: 2px solid #0B0B0D;
					}
				}
				
				.p {
					text-align: center;
					padding-top: 140px;
					font-size: 14px;
				}
	</style>

</head>
<body>
		<header class="clearfix">
				{{--  <div id="logo">

				</div>  --}}
			    <center><img src="https://aw-gpff.s3.ap-south-1.amazonaws.com/TARGETSFA1.png"  height="1" alt="TargetSFA" style="padding-bottom: 5%;width: 25%;height: auto;padding-left: 3%"/></center>
				<!-- <h1 class="display-1" style="padding-top: 1%; padding-left: 17%;">Target Report</h1> -->
			   </header>
			

<div class="container">
	<div class="date" ><h4 align="left" >Report Type: {{ $Title }}</h4> <h5 align="right">Year : {{ $Month }}  </h5></div>
	<h4 align="left"></h4 >
  <div class="row">
    <div class="col-xs-12">
      <table summary="This table shows how to create responsive tables using Tablesaw's functionality" class="table table-bordered table-hover tablesaw tablesaw-stack" data-tablesaw-mode="stack">
        <thead>
          <tr>
							<th> S.NO</th>
							<th> Date</th>
							<th> Field officer Name</th>
							<th> Type</th>
							<th> Product</th>
							<th> Insentive</th>
							<th> Target</th>
							<th> Branch</th>
							<th> Region</th>
							
          </tr>
        </thead>
				<tbody>
					@foreach($datas  as $key=> $data_details)

				<tr>
				<td class="column0"> {{ ++$key }} </td>
                <td class="column2"> {{  $data_details->created_at }} </td>
                <td class="column2"> {{  $data_details->fo_name }} </td>
            @if ($data_details->insentive_type == '1')
            	<td class="column3"> Target Based </td>
            	<td class="column3"> All </td>
                <td class="column2"> {{  $data_details->insentive_amt }} </td>
                <td class="column2"> {{  $data_details->target_amt }} </td>
                <td class="column2"> {{  $data_details->branch_name }} </td>
                <td class="column2"> {{  $data_details->region_name }} </td>

            @elseif ($data_details->insentive_type == '2')
            	<td class="column3"> Unit based </td>
            	<td class="column3"> {{  $data_details->product_name }} </td>
                <td class="column2"> {{  $data_details->insentive_amt }} </td>
                <td class="column2"> {{  $data_details->unit }} </td>
                <td class="column2"> {{  $data_details->branch_name }} </td>
                <td class="column2"> {{  $data_details->region_name }} </td>

        	@else
        		<td class="column3"> Percentage </td>
            	<td class="column3"> All </td>
                <td class="column2"> {{  $data_details->insentive_amt }} </td>
                <td class="column2"> {{  $data_details->target_amt }} </td>
                <td class="column2"> {{  $data_details->branch_name }} </td>
                <td class="column2"> {{  $data_details->region_name }} </td>
            @endif
				</tr>
				 $key++;
				 @endforeach
				</tbody>
      </table>
    </div>
  </div>
</div>



</body>
</html>