<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use File;
use Mail;
use DB;
use App\Imports\UsersImport;
use Maatwebsite\Excel\Facades\Excel;

require(base_path().'/app/Http/Middleware/User.php');

class UserController extends Controller
{

////////////////////////////
//User Managment Api Calls//
////////////////////////////
    // Add User Details ( Role 1-admin,2-user,3-feild officer,4-stockist)
    public function addUser(Request $request){
        $users = new User();
        $users->firstname    = $request->input('firstname');
        $users->lastname     = $request->input('lastname');
        $users->username     = $request->input('username');
        $users->email        = $request->input('email');
        $users->address      = $request->input('address');
        // $users->vendor       = $request->input('vendor');
        $users->city         = $request->input('city');
        $users->country      = $request->input('country');
        $users->country_code = $request->input('country_code');
        $users->mobile       = $request->input('mobile');
        $users->gender       = $request->input('gender');
        $users->dob          = $request->input('dob');
        $users->area_manager_id   = $request->input('area_manager_id');
        $users->role         = $request->input('role');
        $users->country_flag = $request->input('country_flag');
        $users->avatar       = $request->input('avatar');
        // $users->special_permission       = $request->input('special_permission');

        if($request->input('special_permission')){
            $users->special_permission       = $request->input('special_permission');
        }else{
            $users->special_permission       = 0;
        }
        if($users->role == 2){
            $users->branch_id = implode(',', $request->input('branch_id'));
            $users->region_id = '';
            $users->area_id = '';
            $users->permission = '';
        } else if ($users->role == 3) {
            $users->branch_id = $request->input('branch_id');
            $users->region_id = implode(',', $request->input('region_id'));
            $users->permission = implode(',', $request->input('permission'));
            $users->area_id = '';
        } else if ($users->role == 4 || $users->role == 6) {
            $users->branch_id = $request->input('branch_id');
            $users->region_id = $request->input('region_id');
            $users->area_id = implode(',', $request->input('area_id'));
            $users->permission = implode(',', $request->input('permission'));
        } else if ($users->role == 5 || $users->role == 12) {
            $users->branch_id = $request->input('branch_id');
            $users->region_id = $request->input('region_id');
            $users->area_id = implode(',', $request->input('area_id'));
            $users->permission = '';
        } else if($users->role == 11){
            $users->branch_id = $request->input('branch_id');
            $users->region_id = implode(',', $request->input('region_id'));
            $users->area_id = $request->input('area_id');
            $users->permission = '';
        }else{
            $users->permission = '';
            if($request->input('branch_id')){
            $users->branch_id = $request->input('branch_id');
            }else{
                $users->branch_id = '';
            }
            if($request->input('region_id')){
                $users->region_id = $request->input('region_id');
            }else{
                $users->region_id = '';
            }
            if($request->input('area_id')){
                $users->area_id = $request->input('area_id');
            }else{
                $users->area_id = '';
            }
        }
        if($request->input('branch_admin_id')){
            $users->branch_admin_id = $request->input('branch_admin_id');
        }else{
            $users->branch_admin_id = '';
        }
        if($request->input('region_manager_id')){
            $users->region_manager_id = $request->input('region_manager_id');
        }else{
            $users->region_manager_id = '';
        }
        if($request->input('warehouse_id')){
            $users->warehouse_id = $request->input('warehouse_id');
        }else{
            $users->warehouse_id = '';
        }
        if($request->input('cr_by_id')){
            $users->cr_by_id = $request->input('cr_by_id');
        }else{
            $users->cr_by_id = '';
        }

        $vaildate_mobilenumber  = $users->getUsermobilenumber($users);
        $vaildate_email         = $users->getUserEmail($users);
        $vaildate_UserName      = $users->getUserName($users);

        if(count($vaildate_mobilenumber) < 1 ){
            if(count($vaildate_email) < 1 ){
                if(count($vaildate_UserName) < 1 ){
                    $result = $users->storeUsers($users);
                    if($result){
                        $response = response()->json(['status' => 'success', 'message' => 'User details stored successfully']);
                    }else{
                        $response = response()->json(['status' => 'failure' , 'message' => 'Error in storing details....']);
                    }
                    return $response;
                } else{
                    $response = response()->json(['status' => 'failure' , 'message' => 'Username already Taken']); 
                }
            } else{
                $response = response()->json(['status' => 'failure' , 'message' => 'Mail Id already exists']); 
            }
        } else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Mobile number already exists']); 
        }
        return $response;
    }
    // Update Users Details
    public function updateUsers(Request $request)
    {
        $users = new User();
        $users->user_id      = $request->input('user_id');
        $users->firstname    = $request->input('firstname');
        $users->lastname     = $request->input('lastname');
        $users->username     = $request->input('username');
        $users->email        = $request->input('email');
        $users->address      = $request->input('address');
        $users->city         = $request->input('city');
        $users->country      = $request->input('country');
        $users->country_code = $request->input('country_code');
        $users->mobile       = $request->input('mobile');
        $users->area_manager_id   = $request->input('area_manager_id');
        $users->gender       = $request->input('gender');
        $users->dob          = $request->input('dob');
        $users->role         = $request->input('role');
        $users->country_flag = $request->input('country_flag');
        $users->avatar       = $request->input('avatar');
        if($request->input('special_permission')){
            $users->special_permission       = $request->input('special_permission');
        }else{
            $users->special_permission       = 0;
        }

        if($users->role == 2){
            $users->branch_id = implode(',', $request->input('branch_id'));
            $users->region_id = '';
            $users->area_id = '';
            $users->permission = '';
        } else if ($users->role == 3) {
            $users->branch_id = $request->input('branch_id');
            $users->region_id = implode(',', $request->input('region_id'));
            $users->area_id = '';
            $users->permission = implode(',', $request->input('permission'));
        } else if ($users->role == 4) {
            $users->branch_id = $request->input('branch_id');
            $users->region_id = $request->input('region_id');
            $users->area_id = implode(',', $request->input('area_id'));
            $users->permission = implode(',', $request->input('permission'));
        }else if ($users->role == 5 || $users->role == 12) {
            $users->branch_id = $request->input('branch_id');
            $users->region_id = $request->input('region_id');
            $users->area_id = implode(',', $request->input('area_id'));
            $users->permission = '';
        } else{
            $users->permission = '';
            if($request->input('branch_id')){
            $users->branch_id = $request->input('branch_id');
            }else{
                $users->branch_id = '';
            }
            if($request->input('region_id')){
                $users->region_id = $request->input('region_id');
            }else{
                $users->region_id = '';
            }
            if($request->input('area_id')){
                $users->area_id = $request->input('area_id');
            }else{
                $users->area_id = '';
            }
        }

        if($request->input('branch_admin_id')){
            $users->branch_admin_id = $request->input('branch_admin_id');
        }else{
            $users->branch_admin_id = '';
        }
        if($request->input('region_manager_id')){
            $users->region_manager_id = $request->input('region_manager_id');
        }else{
            $users->region_manager_id = '';
        }
        if($request->input('warehouse_id')){
            $users->warehouse_id = $request->input('warehouse_id');
        }else{
            $users->warehouse_id = '';
        }

        $result = $users->updateUsers($users);
        if($result > 0){
            $response = response()->json(['status' => 'success', 'message' => 'User Details Update successfully','data'=> $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Error in Update User Details']);
        }
        return $response;
    }
    //  Users Profile Update Details
    public function userProfileUpdate(Request $request)
    {
        $users = new User();
        $users->user_id      = $request->input('user_id');
        $users->firstname    = $request->input('firstname');
        $users->lastname     = $request->input('lastname');
        $users->username     = $request->input('username');
        $users->email        = $request->input('email');
        $users->address      = $request->input('address');
        $users->city         = $request->input('city');
        $users->country      = $request->input('country');
        $users->country_code = $request->input('country_code');
        $users->mobile       = $request->input('mobile');
        //$users->area_manager_id   = $request->input('area_manager_id');
        $users->gender       = $request->input('gender');
        $users->dob          = $request->input('dob');
        $users->role         = $request->input('role');
        $users->country_flag = $request->input('country_flag');
        $users->avatar       = $request->input('avatar');
        /*$users->branch_id = $request->input('branch_id');
        $users->region_id = $request->input('region_id');
        $users->area_id = $request->input('area_id');
        $users->permission = $request->input('permission');
        $users->branch_admin_id = $request->input('branch_admin_id');
        $users->region_manager_id = $request->input('region_manager_id');
        if($request->input('warehouse_id')){
            $users->warehouse_id = $request->input('warehouse_id');
        }else{
            $users->warehouse_id = '';
        }*/

        $result = $users->userProfileUpdate($users);
        if($result > 0){
            $response = response()->json(['status' => 'success', 'message' => 'User Profile Details Update successfully','data'=> $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Error in Update User Details']);
        }
        return $response;
    }
    //  Users Profile Update Price
    public function userBasedPriceUpdate(Request $request)
    {
        $users = new User();
        $users->user_id      = $request->input('user_id');
        $users->price_type   = $request->input('price_type');

        $result = $users->userBasedPriceUpdate($users);
        if($result == 1){
            $response = response()->json(['status' => 'success', 'message' => 'User Price Type Update successfully','data'=> $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Error in Update Price Type']);
        }
        return $response;
    }
    // Delete User details
    public function deleteUsers(Request $request){
        $users = new User();
        $users->user_id = $request->input('user_id');

        $result = $users->deleteUsers($users);
        if($result > 0){
            $response = response()->json(['status' => 'success' ,'message' => 'User details deleted successfully']);
        } else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Error in deleting']); 
        }
        return $response;
    }
    // Get Indivitual Users Details
    public function getIndUsers($user_id)
    {
        $users = new User();
        $users->user_id = $user_id;

        $result = $users->getIndUsers($users);
        if(count($result) > 0){
            $response = response()->json(['status' => 'success' , 'message' => '', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Invalid user details']); 
        }
        return $response;
    } 
    // Fetch Role Based (all)User Details
    public function getRoleBasedUsers($role)
    {
        $users = new User();
        $users->role = $role;

        $result = $users->getRoleBasedUsers($users);
        if(count($result) > 0){
            $response = response()->json(['status' => 'success' , 'message' => '', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Invalid user details']); 
        }
        return $response;
    }
    // Fetch Manager Based User Details
    public function getManagerBasedUserDetails(Request $request)
    {
        $users = new User();
        $users->area_manager_id = $request->input('area_manager_id');

        $result = $users->getManagerBasedUserDetails($users);
        if(count($result) > 0){
            $response = response()->json(['status' => 'success' , 'message' => '', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Invalid user details']); 
        }
        return $response;
    }
    // Fetch (All)User Details
    /*public function getAllUsers()
    {
        $users = new User();

        $data = $users->getAllUsers();
        if(count($data) > 0 ){
            $response = response()->json(['status' => 'success' , 'message' => 'User Data retrived successfully','data'=>$data]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Error in Get details..','data'=>'null']);
        }
        return $response;
    }*/
    public function getAllUsers()
    {
        if (\Cache::has('GETALLUSERS')) {
            $value = \Cache::get('GETALLUSERS');
            return $value;
        }
        
        $users = new User();
        
        $data = $users->getAllUsers();

        if(count($data) > 0 ){
            $response = response()->json(['status' => 'success' , 'message' => 'User Data retrived successfully','data'=>$data]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Error in Get details..','data'=>'null']);
        }
        $this->setCacheDataUserModule('GETALLUSERS', $response, '300');
        return $response;
    }   
    //User Activate Or Deactivate
    public function userActiveOrDeactive(Request $request)
    {
        $users = new User();
        $users->user_id = $request->input('user_id');
        $users->is_active  = $request->input('is_active');
        
        $result = $users->userActiveOrDeactive($users);
        if($result > 0){
            if($users->is_active == 1){
                $response = response()->json(['status' => 'success', 'message' => 'User Actived Successfully']);
            } elseif ($users->is_active == 2) {
                $response = response()->json(['status' => 'success', 'message' => 'User Deactived Successfully']);
            }
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Error in Updated details.. !']);
        }
        return $response;
    }
    //User Login Function
    public function login(Request $request)
    {
        $users = new User();
        $users->username    = $request->input('username');
        $users->password    = $request->input('password');
        $users->push_token  = $request->input('push_token');
        $users->login_tag   = $request->input('login_tag');
        $users->os_type     = $request->input('os_type');

        $result = $users->user_login($users);
        if($result == 1){
            $response = response()->json(['status' => 'failure' , 'message' => 'Your account has been Blocked']);
        } else if($result == 2){
            $response = response()->json(['status' => 'failure' , 'message' => 'Username or password is incorrect.']);
        } else if($result){
            $response = response()->json(['status' => 'success' , 'message' => '' ,'data' => $result]);
        }else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Error in login']);
        }
        return $response;
    }
    //User Logout Function
    public function logout(Request $request)
    {
        $users = new User();
        $users->user_id   = $request->input('user_id');
        $users->login_tag = $request->input('login_tag');

        $result = $users->user_logout($users);
        if($result > 0){
            $response = response()->json(['status' => 'success' , 'message' => 'User logged out successfully']);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Error in logging out']);
        }
        return $response;

    }
    //User Change our Password
    public function userChangePassword(Request $request)
    {
        $users = new User();
        $users->user_id      = $request->input('user_id');
        $users->old_password = $request->input('old_password');
        $users->new_password = $request->input('new_password');
        
        $result = $users->userChangePassword($users);
        if($result == 1){
            $response = response()->json(['status' => 'success', 'message' => 'User Password Change Successfully']);
        } else if($result ==2){
            $response = response()->json(['status' => 'success', 'message' => 'Pls Enter Valid Old Password']);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Error in Password Change Process.. !']);
        }
        return $response;
    }
    //User Forget our Password
    public function userForgetPassword(Request $request)
    {
        $users = new User();
        $users->email   = $request->input('email');
        
        $result = $users->userForgetPassword($users);
        if($result == 1){
            $response = response()->json(['status' => 'success', 'message' => 'New Password send to your Mail.']);
        } else if($result ==2){
            $response = response()->json(['status' => 'failure', 'message' => 'Pls Enter Valid Mail Id']);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Error in Password send Process.. !']);
        }
        return $response;
    }


    // Add Customer Details
    public function addCustomers(Request $request){
        $users = new User();
        $users->customer_name   = $request->input('customer_name');
        $users->customer_email  = $request->input('customer_email');
        $users->customer_mobile = $request->input('customer_mobile');
        $users->customer_city    = $request->input('customer_city');
        $users->customer_country = $request->input('customer_country');
        $users->vendor           = $request->input('vendor');
        $users->customer_fax     = $request->input('customer_fax');
        $users->country_code    = $request->input('country_code');
        $users->customer_address = $request->input('customer_address');
        $users->customer_location = $request->input('customer_location');
        $users->customer_lat    = $request->input('customer_lat');
        $users->customer_lan    = $request->input('customer_lan');
        $users->website         = $request->input('website');
        $users->pin_code         = $request->input('pin_code');
        $users->remarks         = $request->input('remarks');
        $users->remarks_type     = $request->input('remarks_type');
        $users->customer_type = $request->input('customer_type');
        $users->customer_emp_id = $request->input('customer_emp_id');
        $users->credit_limit     = $request->input('credit_limit');

        if($request->input('branch_id')){
            $users->branch_id       = $request->input('branch_id');
        }else{
            $users->branch_id = '';
        }
        if($request->input('region_id')){
            $users->region_id       = $request->input('region_id');
        }else{
            $users->region_id = '';
        }
        if($request->input('area_id')){
            $users->area_id       = $request->input('area_id');
        }else{
            $users->area_id = '';
        }

        $users->customer_cr_id  = $request->input('customer_cr_id');
        $users->area_manager_id  = $request->input('area_manager_id');
        if($request->input('region_manager_id')){
            $users->region_manager_id  = $request->input('region_manager_id');
        }else{
            $users->region_manager_id = '';
        }
        $users->avatar  = $request->input('avatar');

        $vaildate_mobilenumber  = $users->getCustomermobilenumber($users);
        $vaildate_email         = $users->getCustomerEmail($users);

        if(count($vaildate_mobilenumber) < 1 ){
            if($users->customer_email === null ||count($vaildate_email) < 1 ){
                    $result = $users->storeCustomers($users);
                    if($request->input('vendor')==2){
                        if($result){
                            $response = response()->json(['status' => 'success', 'message' => 'Customers details stored successfully']);
                    }else{
                        $response = response()->json(['status' => 'failure' , 'message' => 'Error in storing details....']);
                    }}else{
                        $response = response()->json(['status' => 'success', 'message' => 'Vendor details stored successfully']);
                    }
                    return $response;
            } else{
                $response = response()->json(['status' => 'failure' , 'message' => 'Mail Id already exists']); 
            }
        } else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Mobile number already exists']); 
        }
        return $response;
    }

    
    public function addAllCustomers(Request $request){
        $users = new User();

        $users->customerpath = $users->getFilePath($request->file('customerall'));
        $users->customerall = $request->file('customerall');

        $result = $users->addAllCustomers($users);

        if($result == 1){
            $response = response()->json(['status' => 'success' , 'message' => 'Customers details stored successfully']);
        } else if($result == 2){
            $response = response()->json(['status' => 'success' , 'message' => 'Mail Id OR Mobile number already exists']);
        } else if($result == 3){
            $response = response()->json(['status' => 'failure' , 'message' => 'Please upload excel file only']);
        } else if($result == 4){
            $response = response()->json(['status' => 'failure' , 'message' => 'Please Fill All Field in the excel']);
        } else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Customers added failed']);
        }
        return $response;
    }

    
    // Update Users Details
    public function updateCustomers(Request $request)
    {
        $users = new User();
        $users->customer_login_id = $request->input('customer_login_id');
        $users->customer_id     = $request->input('customer_id');
        $users->customer_name   = $request->input('customer_name');
        $users->customer_email  = $request->input('customer_email');
        $users->customer_mobile = $request->input('customer_mobile');
        $users->customer_city    = $request->input('customer_city');
        $users->vendor           = $request->input('vendor');
        $users->customer_country = $request->input('customer_country');
        $users->customer_fax     = $request->input('customer_fax');
        $users->remarks         = $request->input('remarks');
        $users->remarks_type     = $request->input('remarks_type');
        $users->country_code    = $request->input('country_code');
        $users->customer_address = $request->input('customer_address');
        $users->customer_location = $request->input('customer_location');
        $users->customer_lat = $request->input('customer_lat');
        $users->customer_lan = $request->input('customer_lan');
        $users->website         = $request->input('website');
        $users->customer_type = $request->input('customer_type');
        $users->customer_emp_id = $request->input('customer_emp_id');
        $users->credit_limit     = $request->input('credit_limit');
        $users->dob     = $request->input('dob');
        $users->marriage_status     = $request->input('marriage_status');
        $users->facebook     = $request->input('facebook');
        $users->twitter     = $request->input('twitter');
        $users->instagram     = $request->input('instagram');
        $users->alternate_mobile_number     = $request->input('alternate_mobile_number');        

        if($request->input('branch_id')){
            $users->branch_id       = $request->input('branch_id');
        }else{
            $users->branch_id = '';
        }
        if($request->input('region_id')){
            $users->region_id       = $request->input('region_id');
        }else{
            $users->region_id = '';
        }
        if($request->input('area_id')){
            $users->area_id       = $request->input('area_id');
        }else{
            $users->area_id = '';
        }
        $users->customer_cr_id  = $request->input('customer_cr_id');
        $users->area_manager_id  = $request->input('area_manager_id');
        $users->avatar  = $request->input('avatar');
        $users->pin_code  = $request->input('pin_code');

        $result = $users->updateCustomers($users);
        if($result > 0){
            if($users->vendor= $request->input('vendor') == 2){
            $response = response()->json(['status' => 'success', 'message' => 'Customers Details Update successfully','data'=> $result]);
        }else{
            $response = response()->json(['status' => 'success', 'message' => 'Vendor details updated successfully', 'data' => $result]);
        }
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Error in Update Details']);
        }
        return $response;
    }
    // Delete Customers details
    public function deleteCustomers(Request $request){
        $users = new User();
        $users->customer_id = $request->input('customer_id');
        $users->vendor = $request->input('vendor');

        $result = $users->deleteCustomers($users);
        if($result > 0){
            $response = response()->json(['status' => 'success' ,'message' => 'Deleted successfully']);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Error in deleting']); 
        }
        return $response;
    }
    // Get Indivitual Customers Details
    public function getIndCustomers($customer_id)
    {
        $users = new User();
        $users->customer_id = $customer_id;

        $result = $users->getIndCustomers($users);
        if(count($result) > 0){
            $response = response()->json(['status' => 'success' , 'message' => '', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Invalid details']); 
        }
        return $response;
    } 
    // Get Indivitual Customers Details
    public function getEmpBaseCustomers($customer_id)
    {
        $users = new User();
        $users->customer_id = $customer_id;

        $result = $users->getEmpBaseCustomers($users);
        if(count($result) > 0){
            $response = response()->json(['status' => 'success' , 'message' => '', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Invalid Customer details']); 
        }
        return $response;
    } 
    // Fetch (All)Customers Details
    public function getAllCustomers()
    {
        // if (\Cache::has('GETALLCUSTOMERS')) {
        //     $value = \Cache::get('GETALLCUSTOMERS');
        //     return $value;
        // }

        $users = new User();

        $data = $users->getAllCustomers();
        if(count($data) > 0 ){
            
            $response = response()->json(['status' => 'success' , 'message' => 'Customers Data retrived successfully','data'=>$data]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Error in Get details..','data'=>'null']);
        }
        // $this->setCacheDataUserModule('GETALLCUSTOMERS', $response, '300');
        return $response;
    }   
    // Get User Based Customers Details
    public function getUserBasedCustomers(Request $request)
    {
        $users = new User();
        $users->customer_cr_id  = $request->input('customer_cr_id');

        $result = $users->getUserBasedCustomers($users);
        if(count($result) > 0){
            $response = response()->json(['status' => 'success' , 'message' => '', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Invalid Customer details']); 
        }
        return $response;
    } 
    //Customers Activate Or Deactivate
    public function customerActiveOrDeactive(Request $request)
    {
        $users = new User();
        $users->customer_id = $request->input('customer_id');
        $users->is_active  = $request->input('is_active');
        
        $result = $users->customerActiveOrDeactive($users);
        if($result > 0){
            if($users->is_active == 1){
                $response = response()->json(['status' => 'success', 'message' => 'customer Actived Successfully']);
            } else{
                $response = response()->json(['status' => 'success', 'message' => 'customer Deactived Successfully']);
            }
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Error in Updated details.. !']);
        }
        return $response;
    }
    // Fetch Region Based Region Manager List GEt
    public function getBranchBasRegManage(Request $request)
    {
        $users = new User();
        $users->branch_id = $request->input('branch_id');

        $result = $users->getBranchBasRegManage($users);
        if($result){
            $response = response()->json(['status' => 'success' , 'message' => '', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Invalid user details']); 
        }
        return $response;
    }
    // Fetch Branch Based Field Officer List GEt
    public function getBranchBaseFO(Request $request)
    {
        $users = new User();
        $users->branch_id = $request->input('branch_id');

        $result = $users->getBranchBaseFO($users);
        if($result){
            $response = response()->json(['status' => 'success' , 'message' => '', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Invalid user details']); 
        }
        return $response;
    }
    // Fetch Area Based Area Manager List GEt
    public function getRegionBasAreaManage(Request $request)
    {
        $users = new User();
        $users->region_id = $request->input('region_id');

        $result = $users->getRegionBasAreaManage($users);
        if($result){
            $response = response()->json(['status' => 'success' , 'message' => '', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Invalid user details']); 
        }
        return $response;
    }
    // Fetch Customer Based Region
    public function getRegionBasCustomer(Request $request)
    {
        $users = new User();
        $users->region_id = $request->input('region_id');

        $result = $users->getRegionBasCustomer($users);
        if($result){
            $response = response()->json(['status' => 'success' , 'message' => '', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Invalid Customer details']); 
        }
        return $response;
    }
    // Fetch Customer Based Area
    public function getAreaBasCustomer(Request $request)
    {
        $users = new User();
        $users->area_id = $request->input('area_id');

        $result = $users->getAreaBasCustomer($users);
        if($result){
            $response = response()->json(['status' => 'success' , 'message' => '', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Invalid Customer details']); 
        }
        return $response;
    }
    // Fetch Customer Based Area ALL
    public function getMultiAreaBasCustomer(Request $request)
    {
        $users = new User();
        $users->area_id = $request->input('area_id');

        $result = $users->getMultiAreaBasCustomer($users);
        if($result){
            $response = response()->json(['status' => 'success' , 'message' => '', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Invalid Customer details']); 
        }
        return $response;
    }
//End User
////////////////////////////////////////////
//Normal Notification Management Api Calls//
///////////////////////////////////////////
    //Get all Notification
    public function getNotification(Request $request)
    {
        $users = new User();
        $users->notification_user_id = $request->input('notification_user_id');

        $result = $users->fetchNotification($users);
        
        if(count($result) >0){
            $response = response()->json(['status' => 'success' , 'message' => 'Notification details fetched successfully', 'data' => $result
            ]);
        }else{
            $response = response()->json(['status' => 'failure' , 'message' => 'No Notification found'
            ]);
        }
        return $response;
    }
    //Get all Notification
    public function getAppNotification($notification_user_id)
    {
        $users = new User();
        $users->notification_user_id = $notification_user_id;

        $result = $users->getAppNotification($users);

        if(count($result) >0){
            $response = response()->json(['status' => 'success' , 'message' => 'Notification details fetched successfully', 'data' => $result
            ]);
        }else{
            $response = response()->json(['status' => 'failure' , 'message' => 'No Notification found'
            ]);
        }
        return $response;
    }
    //Notification Status Changes 1- Read
    public function notificationStatusChange(Request $request)
    {
        $users = new User();
        $users->notification_id = $request->input('notification_id');

        $result = $users->notificationStatus($users);
        if($result > 0){
            $response = response()->json(['status' => 'success' ,'message' => 'Notification status updated successfully']);
        }else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Error in updating']); 
        }
        return $response;
    }
    //Notification Delete 
    public function deleteNotification(Request $request)
    {
        $users = new User();
        $users->notification_id = $request->input('notification_id');

        $result = $users->deleteNotification($users);
        if($result == 1){
            $response = response()->json(['status' => 'success' ,'message' => 'Notification successfully deleted ']);
        }else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Error in deleting']); 
        }
        return $response;
    }
    //Notification Delete 
    public function deleteAllNotification(Request $request)
    {
        $users = new User();
        $users->notification_user_id = $request->input('notification_user_id');

        $result = $users->deleteAllNotification($users);
        if($result == 1){
            $response = response()->json(['status' => 'success' ,'message' => 'Notification successfully deleted ']);
        }else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Error in deleting']); 
        }
        return $response;
    }
    // Get Indivitual Id Based Notification Fetch
    public function getIndBasedNotiDetails($notification_id)
    {
        $users = new User();
        $users->notification_id = $notification_id;

        $result = $users->getIndBasedNotiDetails($users);
        if($result){
            $response = response()->json(['status' => 'success' , 'message' => '', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Invalid details']); 
        }
        return $response;
    } 


    // Get Users Based Details
    public function getUserBasedDetails($user_id)
    {
        $users = new User();
        $users->user_id = $user_id;

        $result = $users->getUserBasedDetails($users);
        if($result){
            $response = response()->json(['status' => 'success' , 'message' => '', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Invalid user details']); 
        }
        return $response;
    } 

    // Get warehouse Based Details
    public function getWarehBasedDetails($warehouse_id)
    {
        $users = new User();
        $users->warehouse_id = $warehouse_id;

        $result = $users->getWarehBasedDetails($users);
        if($result){
            $response = response()->json(['status' => 'success' , 'message' => '', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Invalid warehouse details']); 
        }
        return $response;
    } 

//Pagination Call
    // Fetch (All)Customers Details
    /*public function getAllCustomersForApp(Request $request)
    {
        $users = new User();

        if($request->input('draw')){
            $users->draw = $request->input('draw');
        }else{
            $users->draw = 1; 
        }
        if($request->input('start')){
             $users->start = $request->input('start');
        }else{
            $users->start = 0; 
        }
        if($request->input('length')){
            $users->length = $request->input('length');
        }else{
            $users->length = 10; 
        }

        $result = $users->getAllCustomersForApp($users);

        if(count($result[2]) > 0){
            $response = response()->json(['status' => 'success' , 'message' => 'Notification details fetched successfully', 'data' => $result[2],
                'draw' => $users->draw,
                'recordsTotal' => $result[0],
                'recordsFiltered' => $result[1],
            ]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'No Notification found',
                'data' => [],
                'draw' => $users->draw,
                'recordsTotal' => 0,
                'recordsFiltered' => 0]);
        }
        return $response;
    }   */
    public function getAllCustomersForApp(Request $request)
    {
        $users = new User();

        if($request->input('draw')){
            $users->draw = $request->input('draw');
        }else{
            $users->draw = 1; 
        }
        if($request->input('start')){
             $users->start = $request->input('start');
        }else{
            $users->start = 0; 
        }
        if($request->input('length')){
            $users->length = $request->input('length');
        }else{
            $users->length = 10; 
        }
        if($request->order[0]['column']){
            $users->orderColumn = $request->order[0]['column'];
        }else{
            $users->orderColumn = 0; 
        }
        if($request->order[0]['dir']){
            $users->orderDir = $request->order[0]['dir'];
        }else{
            $users->orderDir = 'desc'; 
        }
        if($request->search['value']){
            $users->searchKey = $request->search['value'];
        }else{
            $users->searchKey = ""; 
        }

        $result = $users->getAllCustomersForApp($users);

        if(count($result[2]) > 0){
            $response = response()->json(['status' => 'success' , 'message' => 'Notification details fetched successfully', 'data' => $result[2],
                'draw' => $users->draw,
                'recordsTotal' => $result[0],
                'recordsFiltered' => $result[1],
            ]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'No Notification found',
                'data' => [],
                'draw' => $users->draw,
                'recordsTotal' => 0,
                'recordsFiltered' => 0]);
        }
        return $response;
    }

    /*
    *   Send the SMS to Campaign
    */

    public function sendSMSCampaign(Request $request) {
        $users = new User();
        $result = $users->sendSMSCampaign($request);
        $response = response()->json(['status' => 'success' , 'message' => 'SMS send successfully',$result
                    ]);
        return $response;
    }

        /*
    *   Send the EMAIL to Campaign
    */

    public function sendEMAILCampaign(Request $request) {
        $users = new User();
        $result = $users->sendEMAILCampaign($request);
        $response = response()->json(['status' => 'success' , 'message' => 'EMAIL send successfully'
                    ]);
        return $response;
    }

    /*
    *   Send the Notifi to Campaign
    */

    public function sendNotiCampaign(Request $request) {
        $users = new User();
        $result = $users->sendNotiCampaign($request);
        $response = response()->json(['status' => 'success' , 'message' => 'Notification send successfully'
                    ]);
        return $response;
    }

    //Get all Promotion Notification
    public function getAppPromoNotification($notification_user_id)
    {
        $users = new User();
        $users->notification_user_id = $notification_user_id;

        $result = $users->getAppPromoNotification($users);

        if(count($result) >0){
            $response = response()->json(['status' => 'success' , 'message' => 'Notification details fetched successfully', 'data' => $result
            ]);
        }else{
            $response = response()->json(['status' => 'failure' , 'message' => 'No Notification found'
            ]);
        }
        return $response;
    }


    /* @setCacheDataUserModule
    *
    */
    public function setCacheDataUserModule($cKey, $cResponse, $cRestime = 500) {
        \Cache::put($cKey, $cResponse, $cRestime);
    }

    /* @removeLaravelCache
    * If key is exist, using key remove from the cache, 
    * otherwise remove all cache in server side for existing cache
    */
    public function removeLaravelCache() {
        $cKey = $_GET['key'];
        if (isset($cKey)) {
            \Cache::forget($cKey);
            echo "Cache Successfully Cleared using ". $cKey;
        } else {
            \Cache::flush();    
            echo "All Cache Successfully Cleared. ";
        }

        
        
    }

    public function addBiller(Request $request){
        $users = new User();
        $users->biller_name    = $request->input('biller_name');
        $users->email_id        = $request->input('email_id');
        $users->country_code = $request->input('country_code');
        $users->mobile       = $request->input('mobile');
        $users->country       = $request->input('country');
        $users->warehouse_id = $request->input('warehouse_id');
        $users->cr_by_id = $request->input('cr_by_id');
        $result = $users->storeBiller($users);
        if($result){
            $response = response()->json(['status' => 'success', 'message' => 'Biller details stored successfully']);
        }else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Error in storing details....']);
        }
        return $response;
        
    }
    public function updateBiller(Request $request){
        $users = new User();
        $users->biller_id    = $request->input('biller_id');
        $users->biller_name    = $request->input('biller_name');
        $users->email_id        = $request->input('email_id');
        $users->country_code = $request->input('country_code');
        $users->mobile       = $request->input('mobile');
        $users->country = $request->input('country');
        $result = $users->updateBiller($users);
        if($result > 0){
            $response = response()->json(['status' => 'success', 'message' => 'Biller details updated successfully']);
        }else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Error in storing details....']);
        }
        return $response;
        
    }
    public function updateBillerStatus(Request $request){
        $users = new User();
        $users->biller_id    = $request->input('biller_id');
        $users->biller_status    = $request->input('biller_status');
        $result = $users->updateBillerStatus($users);
        if($result > 0){
            if($users->biller_status == 1){
                $response = response()->json(['status' => 'success', 'message' => 'Biller Actived Successfully']);
            } elseif ($users->biller_status == 2) {
                $response = response()->json(['status' => 'success', 'message' => 'Biller Deactived Successfully']);
            }
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Error in Updated details.. !']);
        }
        return $response;
        
    }
    public function deleteBiller(Request $request){
        $users = new User();
        $users->biller_id    = $request->input('biller_id');
        // $users->warehouse_id    = $request->input('warehouse_id');
        $result = $users->deleteBiller($users);
        if($result > 0){
            $response = response()->json(['status' => 'success', 'message' => 'Biller details deleted successfully']);
        }else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Error in storing details....']);
        }
        return $response;
        
    }
    public function getBillerDetails(Request $request){
        $users = new User();
        if($request->input('biller_id')){
            $users->biller_id        = $request->input('biller_id');
        }else{
            $users->biller_id        = '';
        }
        $users->warehouse_id    = $request->input('warehouse_id');
        $result = $users->getBillerDetails($users);
        if(count($result) > 0){
            $response = response()->json(['status' => 'success', 'message' => '','data'=>$result]);
        }else{
            $response = response()->json(['status' => 'failure' , 'message' => 'No data found']);
        }
        return $response;
        
    }

    // Get User List
    public function getDoerList(Request $request)
    {
        $users = new User();
        if($request->input('branch_id')){
            $users->branch_id        = $request->input('branch_id');
        }else{
            $users->branch_id        = '';
        }
        if($request->input('region_id')){
            $users->region_id        = $request->input('region_id');
        }else{
            $users->region_id        = '';
        }
        $result = $users->getDoerList($users);
        if(count($result) > 0){
            $response = response()->json(['status' => 'success' , 'message' => '', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Invalid user details']); 
        }
        return $response;
    }

    public function getCheckerList(Request $request)
    {
        $users = new User();
        if($request->input('branch_id')){
            $users->branch_id        = $request->input('branch_id');
        }else{
            $users->branch_id        = '';
        }
        if($request->input('region_id')){
            $users->region_id        = $request->input('region_id');
        }else{
            $users->region_id        = '';
        }
        $result = $users->getCheckerList($users);
        if(count($result) > 0){
            $response = response()->json(['status' => 'success' , 'message' => '', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Invalid user details']); 
        }
        return $response;
    }

    public function getFinanceUserList(Request $request)
    {
        $users = new User();
        if($request->input('branch_id')){
            $users->branch_id        = $request->input('branch_id');
        }else{
            $users->branch_id        = '';
        }
        if($request->input('region_id')){
            $users->region_id        = $request->input('region_id');
        }else{
            $users->region_id        = '';
        }
        $result = $users->getFinanceUserList($users);
        if(count($result) > 0){
            $response = response()->json(['status' => 'success' , 'message' => '', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Invalid user details']); 
        }
        return $response;
    }

    // Get User List
    public function getCustomersList(Request $request)
    {
        $users = new User();
        if($request->input('branch_id')){
            $users->branch_id        = $request->input('branch_id');
        }else{
            $users->branch_id        = '';
        }
        if($request->input('region_id')){
            $users->region_id        = $request->input('region_id');
        }else{
            $users->region_id        = '';
        }
        if($request->input('area_id')){
            $users->area_id        = $request->input('area_id');
        }else{
            $users->area_id        = '';
        }
        $result = $users->getCustomersList($users);
        // print_r($result);
        // exit;
        if(count($result) > 0){
            $response = response()->json(['status' => 'success' , 'message' => '', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Invalid user details']); 
        }

        return $response;
    }


}
