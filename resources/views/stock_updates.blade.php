<!DOCTYPE html>
<html>
<head>
	<title>Invoice</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
	<style>
		body {
			width: 100%;
			padding: 10px;
			display: block;
		}

		h4 {
			font-size: 14px;
			line-height: 5px;
		}
		h3 {
			font-size: 20px;
			line-height: 5px;
		}

		p {
			font-size: 12px;
			line-height: 20px;
		}
		
		tr {
			font-size: 12px;
			line-height: 35px;
		}
		#second-table {
			text-align: center;
			border-collapse: collapse;
			width: 100%;
		}
		#table-a{
			text-align: left;
		}
		#table-b{
			text-align: left;
		}
		h6{
			font-size: 12px;
		}
		#table-c{
			text-align: center;
		}
		#table-d{
			text-align: left;
		}
		#second-table {
			border: 1px solid black;
			border-collapse: collapse;
		}
		#nd-rows, #nd-rows > th, #nd-rows > td{
			border: 1px solid black;
			text-align: center;
		}
		#table-a > p{
			font-size: 12px;
		}
		#table-ab{
			font-size: 12px;
		}


	</style>
</head>
<body>
	<center>
		<h3><b>{{$com_details->branch_name}}</b></h3>
		<p>{{$com_details->branch_address}}<br/> 
			{{$com_details->branch_country}}.<b>PH: </b>{{$com_details->country_code}} {{$com_details->branch_contact}}</p>
			<p></p>
			<h4><b>Stock Voucher</b></h4>  <br>
		</center>
		<div class="row">
			<div class="col-md-12">
				<table style="width:100%" id="table-ab" class="col-md-12">
					<tr>
						<th >Stockist name:</th>
						<td>{{$cus_de->firstname." ".$cus_de->lastname}}</td>
						<th align="right">Invoice no:</th>
						<td>{{$invoice}}</td>
					</tr>
					<tr>
						<th >Warehouse Id:</th>
						<td>{{$war->warehouse_id}}</td>
						<th >Warehouse Name:</th>
						<td>{{$war->warehouse_name}}</td>
					</tr>
					<tr>
						<th><b>Payment Method: </b></th>
						<td>{{$payment_method}}</td>
						<th align="right">Date:</th>
						<td>{{$date}}</td>
					</tr>
				</table>
			</div>
		</div>
		<center>
			<table id = "second-table" class="col-md-12" style="width:100%">
				<tr id="nd-rows">
					<th>S.No</th>
					<th>Product Name</th>
					<th>Batch.No</th>
					<th>Exp.Date</th>
					<th>UOM</th>
					<th>No of items</th>
					<th>Qty</th>
					<th>Net_amt</th>
					<th>Gross_amt</th>
					<th>Discount</th>
					<th>Amount</th>
				</tr>
				@foreach($datas  as $key=> $prduct_lists)
				<tr id="nd-rows">
					<td>{{ ++$key }}</td>
					<td>{{  $prduct_lists['product_genericname'] }}</td>
					<td>{{  $prduct_lists['batch_id'] }}</td>
					<td>{{  $prduct_lists['exp_date'] }}</td>
					<td>{{  $prduct_lists['UOM'] }}</td>
					<td>{{  $prduct_lists['no_of_item'] }}</td>
					<td>{{  $prduct_lists['product_qty'] }}</td>
					<td>{{  $prduct_lists['product_netprice'] }}</td>
					<td>{{  $prduct_lists['product_grossprice'] }}</td>
					<td>{{  $prduct_lists['product_discount'] }}</td>
					<td>{{  $prduct_lists['product_tot_amt'] }}</td>
				</tr>
				$key++;
				@endforeach
			</table>

			<div class="row">
				<div class="col-md-12">
					<table style="width:100%" id="table-ab" class="col-md-6">
<!-- 						<tbody style="float: left;">
							<tr>
								<th> </th>
								<td></td>
								<th><b>Cash: </b></th>
								<td>{{$total}}Kyat</td>
							</tr>	
						</tbody>
						<tbody style="float: right;">
							<tr>
								<th> </th>
								<td></td>

							</tr>	
						</tbody> -->
					<tr>
						<th><b>Cash: </b></th>
						<td>{{$total}}Kyat</td>
						<th><b>Total Amount:</b></th>
						<td>{{$total}}</td>
					</tr>
					<tr>
						<th> </th>
						<td>{{$value}}</td>
						<th></th>
						<td></td>
					</tr>

					</table>
				</div>
			</div>
		</center>
		<center>
			<div class="row">
				<div class="col-md-12">
					<table style="width:100%;" id="table-ab" class="col-md-12">
						<tbody style="float: left;">
							<tr>
								<th >This is computer generated bill, No need of signature.</th>
								<td></td>
							</tr>
						</tbody>
						<tbody style="float: right;">
							<tr>
								<td></td>
								<th align="right"></th>
								<td><b>Signature</b></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</center>
	</body>
	</html>