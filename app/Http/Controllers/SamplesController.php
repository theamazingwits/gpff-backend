<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use File;
use Mail;

require(base_path().'/app/Http/Middleware/Samples.php');

class SamplesController extends Controller
{
//////////////////////////////////////////
//Samples Inventory Management Api Calls//
//////////////////////////////////////////
	//Admin Or Sub Admin Add the Samples 
    //Sample Stock
	public function addSamples(Request $request){
    	
        $Sample = new Samples();
        $Sample->samples_name  		= $request->input('samples_name');
        $Sample->product_id       = $request->input('product_id');
        $Sample->category_id       = $request->input('category_id');
        $Sample->category_name       = $request->input('category_name');
        $Sample->product_type       = $request->input('product_type');
        $Sample->samples_qty        = $request->input('samples_qty');
        $Sample->samples_description = $request->input('samples_description');
        $Sample->samples_cr_id  	= $request->input('samples_cr_id');
        $Sample->region_id          = $request->input('region_id');
        $Sample->warehouse_id          = $request->input('warehouse_id');
        $Sample->branch_id      = $request->input('branch_id');
        $Sample->stockist_id = $request->input('stockist_id');

        $result = $Sample->addSamples($Sample);
        if($result == 1){
            $response = response()->json(['status' => 'success' , 'message' => 'Samples added successfully']);
        }else if($result == 2){
            $response = response()->json(['status' => 'success' , 'message' => 'Samples Already In Stock']);
        } else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Samples added failed']);
        }
        return $response;
    }
    // Update Samples Qty Details
    public function updateSamplesQty(Request $request)
    {
        $Sample = new Samples();
        $Sample->samples_id 	= $request->input('samples_id');
        $Sample->product_id       = $request->input('product_id');
        //$Sample->product_type       = $request->input('product_type');
        $Sample->samples_qty 	= $request->input('samples_qty');
        $Sample->warehouse_id  = $request->input('warehouse_id');
        $Sample->region_id  = $request->input('region_id');
        $Sample->branch_id  = $request->input('branch_id');

        $result = $Sample->updateSamplesQty($Sample);
        if($result > 0){
            $response = response()->json(['status' => 'success', 'message' => 'Samples Qty Update successfully','data'=> $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Error in Update Samples Qty']);
        }
        return $response;
    }
    //get Warehouse Based Sample Stock
    public function getWarehouseBasedSampleStock(Request $request)
    {
        $Sample = new Samples();
        $Sample->region_id  = $request->input('region_id');
        $Sample->warehouse_id  = $request->input('warehouse_id');

        $data = $Sample->getWarehouseBasedSampleStock($Sample);

        if(count($data) > 0 ){
            $response = response()->json(['status' => 'success' , 'message' => 'Samples Data retrived successfully','data'=>$data]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Error in Get details..','data'=>'null']);
        }
        return $response;
    }   
    //End Sample Stock


    // Update Samples Details
    public function updateSamples(Request $request)
    {
        $Sample = new Samples();
        $Sample->samples_id             = $request->input('samples_id');
        $Sample->samples_name           = $request->input('samples_name');
        $Sample->samples_description    = $request->input('samples_description');
        $Sample->samples_cr_id          = $request->input('samples_cr_id');
        $Sample->region_id              = $request->input('region_id');
        $Sample->warehouse_id           = $request->input('warehouse_id');
        $Sample->product_id             = $request->input('product_id');
        $Sample->category_id            = $request->input('category_id');


        $result = $Sample->updateSamples($Sample);
        if($result > 0){
            $response = response()->json(['status' => 'success', 'message' => 'Samples Details Update successfully','data'=> $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Error in Update Samples Details']);
        }
        return $response;
    }
    // Delete Samples details
    public function removeSamples(Request $request){
        
        $Sample = new Samples();
        $Sample->samples_id  = $request->input('samples_id');

        $result = $Sample->removeSamples($Sample);
        if($result > 0){
            $response = response()->json(['status' => 'success' ,'message' => 'Samples details deleted successfully']);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Error in deleting']); 
        }
        return $response;
    }
    // Get Indivitual Samples Details
    public function getIndSamples($samples_id)
    {
        $Sample = new Samples();
        $Sample->samples_id = $samples_id;

        $result = $Sample->getIndSamples($Sample);
        if(count($result) > 0){
            $response = response()->json(['status' => 'success' , 'message' => '', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Invalid Samples details']); 
        }
        return $response;
    } 
    // Fetch (All)Samples Details
    public function getAllSamples()
    {
        $Sample = new Samples();

        $data = $Sample->getAllSamples();
        if(count($data) > 0 ){
            $response = response()->json(['status' => 'success' , 'message' => 'Samples Data retrived successfully','data'=>$data]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Error in Get details..','data'=>'null']);
        }
        return $response;
    }  
    // Fetch Region Based Samples Details
    public function getRegionBasedSamples($region_id)
    {
        $Sample = new Samples();

        $Sample->region_id = $region_id;

        $data = $Sample->getRegionBasedSamples($Sample);

        if(count($data) > 0 ){
            $response = response()->json(['status' => 'success' , 'message' => 'Samples Data retrived successfully','data'=>$data]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Error in Get details..','data'=>'null']);
        }
        return $response;
    }   
    ///////////////////////////////////////
    //Samples Assign Management Api Calls//
    ///////////////////////////////////////
    //Admin Or Sub Admin Sample Assign into the field_officer
    public function samplesAssign(Request $request){
        
        $Sample = new Samples();
        // $Sample->samples_id = $request->input('samples_id');
        // $Sample->samples_name = $request->input('samples_name');
        // $Sample->samples_totqty = $request->input('samples_totqty');
        //$Sample->field_officer_id = $request->input('field_officer_id');
        $Sample->fieldofficers = $request->input('fieldofficers');
        $Sample->area_manager_id = $request->input('area_manager_id');
        $Sample->region_manager_id = $request->input('region_manager_id');
        $Sample->warehouse_id = $request->input('warehouse_id');
        //$Sample->stockist_id = $request->input('stockist_id');
        $Sample->region_id = $request->input('region_id');
        $Sample->products = $request->input('products');
        

        $result = $Sample->samplesAssign($Sample);
        if($result == 1){
            $response = response()->json(['status' => 'success' , 'message' => 'Sample can be Assign successfully']);
        } else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Sample Assign failed']);
        }
        return $response;
    }
    //Fetch Completed Assign Pending samples for Stockist
    public function getReceivedSampleDet(Request $request){

        $Sample = new Samples();
        $Sample->warehouse_id = $request->input('warehouse_id');
        $Sample->type = $request->input('type');

        $result = $Sample->getReceivedSampleDet($Sample);
        if(count($result) > 0){
            $response = response()->json(['status' => 'success' , 'message' => 'Fieldofficer received sample successfully', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'No Sample Assign details']); 
        }
        return $response;

    }
    // Get My Assign Sample Details
    public function getMyAssignSamples(Request $request)
    {
        $Sample = new Samples();
        $Sample->field_officer_id = $request->input('user_id');

        $result = $Sample->getMyAssignSamples($Sample);
        if(count($result) > 0){
            $response = response()->json(['status' => 'success' , 'message' => '', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Invalid Sample details']); 
        }
        return $response;
    } 
    //Area Manager Assign Samples List Details Fetch
    public function getManagerAssignSamples(Request $request)
    {
        $Sample = new Samples();
        $Sample->area_manager_id = $request->input('area_manager_id');

        $result = $Sample->getManagerAssignSamples($Sample);
        if(count($result) > 0){
            $response = response()->json(['status' => 'success' , 'message' => '', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Invalid Sample details']); 
        }
        return $response;
    } 
    //Region Manager Assign Samples List Details Fetch
    public function getRegionManageAssignSamples(Request $request)
    {
        $Sample = new Samples();
        $Sample->user_id = $request->input('user_id');

        $result = $Sample->getRegionManageAssignSamples($Sample);
        if(count($result) > 0){
            $response = response()->json(['status' => 'success' , 'message' => '', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Invalid Sample details']); 
        }
        return $response;
    } 

    //Give Sample to Field Officer
    public function giveSampletoFO(Request $request){
        $Sample = new Samples();
        $Sample->samplesassign_id = $request->input('sample_assign_id');
        $Sample->total_qty = $request->input('total_qty');
        $Sample->pre_issued_qty = $request->input('pre_issued_qty');
        $Sample->issue_qty = $request->input('issue_qty');        
        $Sample->products = $request->input('products');

        $result = $Sample->giveSampletoFO($Sample);
        if($result == 1){
            $response = response()->json(['status' => 'success' , 'message' => 'Fieldofficer received sample successfully', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Error Sample details']); 
        }
        return $response;
    }
    // Get My Assign Sample Details
    public function getFOSamples(Request $request)
    {
        $Sample = new Samples();
        $Sample->field_officer_id = $request->input('user_id');

        $result = $Sample->getFOSamples($Sample);
        if(count($result) > 0){
            $response = response()->json(['status' => 'success' , 'message' => '', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Invalid Sample details']); 
        }
        return $response;
    } 
    

    

    

    // Get Admin Assign Sample Details
    public function getAllAssignSamples()
    {
        $Sample = new Samples();
        $result = $Sample->getAllAssignSamples();

        if(count($result) > 0){
            $response = response()->json(['status' => 'success' , 'message' => '', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Invalid Gift details']); 
        }
        return $response;
    } 

    //Field Officer Give the Sample to the Customer
    public function sampleToCustomer(Request $request){
        
        $Sample = new Samples();
        $Sample->fo_received_id          = $request->input('fo_received_id');
        $Sample->samples_name        = $request->input('samples_name');
        $Sample->samples_qty         = $request->input('samples_qty');
        $Sample->product_id        = $request->input('product_id');
        $Sample->batch_id         = $request->input('batch_id');
        //$Sample->Sampledetails          = $request->input('Sampledetails');
        $Sample->field_officer_name = $request->input('field_officer_name');
        $Sample->customer_id         = $request->input('customer_id');
        $Sample->field_officer_id    = $request->input('field_officer_id');
        $Sample->task_id             = $request->input('task_id');
        $Sample->samples_cmt         = $request->input('samples_cmt');

        $result = $Sample->sampleToCustomer($Sample);
        if($result == 1){
            $response = response()->json(['status' => 'success' , 'message' => 'success']);
        }else{
            $response = response()->json(['status' => 'failure' , 'message' => 'failed']);
        }
        return $response;
    }

}
