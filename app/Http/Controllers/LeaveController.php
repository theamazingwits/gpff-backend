<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use File;
use Mail;

require(base_path().'/app/Http/Middleware/Leave.php');


class LeaveController extends Controller
{	
    //Field Officer Add Our Leave Application
	public function addLeave(Request $request){
    	
        $Leaves = new Leave();
        $Leaves->leave_start_date  	= $request->input('leave_start_date');
        $Leaves->leave_end_date  	= $request->input('leave_end_date');
        $Leaves->user_id  			= $request->input('user_id');
        $Leaves->area_manager_id  		= $request->input('area_manager_id');
        $Leaves->leave_type         = $request->input('leave_type');   
        $Leaves->leave_detaile  	= $request->input('leave_detaile');
        $Leaves->area_id      = $request->input('area_id');
       
        $result = $Leaves->addLeave($Leaves);
        if($result == 1){
            $response = response()->json(['status' => 'success' , 'message' => 'Leave added successfully']);
        } else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Leave creation failed']);
        }
        return $response;
    }
    //Manager or admin Accept Or Reject Leave Application
    public function leaveAcceptOrReject(Request $request)
    {
        $Leaves = new Leave();
        $Leaves->leave_id 	  	   	 = $request->input('leave_id');
        $Leaves->leave_status  	   	 = $request->input('leave_status');
        $Leaves->leave_reject_reason = $request->input('leave_reject_reason');

        $result = $Leaves->leaveAcceptOrReject($Leaves);
        if($result > 0){
            if($Leaves->leave_status == 1){
                $response = response()->json(['status' => 'success', 'message' => 'leave Accept Successfully']);
            } elseif($Leaves->leave_status == 2){
                $response = response()->json(['status' => 'success', 'message' => 'leave Reject Successfully']);
            }
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Error in Updated details.. !']);
        }
        return $response;
    }
    // Fetch Leave Request Based Manager
    public function getManagerBasedLeaveRequest(Request $request)
    {
        $Leaves = new Leave();
        $Leaves->area_manager_id 	= $request->input('area_manager_id');
        $Leaves->leave_status 	= $request->input('leave_status');

        $result = $Leaves->getManagerBasedLeaveRequest($Leaves);
        if(count($result) > 0){
            $response = response()->json(['status' => 'success' , 'message' => '', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Invalid user Leave details']); 
        }
        return $response;
    }
    // Fetch User Leave History Based user_id
    public function getUserLeaveHistory($user_id)
    {
        $Leaves = new Leave();
        $Leaves->user_id 	= $user_id;

        $result = $Leaves->getUserLeaveHistory($Leaves);
        if(count($result) > 0){
            $response = response()->json(['status' => 'success' , 'message' => '', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Invalid user Leave details']); 
        }
        return $response;
    }

//Attendance
    //Field Officer Attendance  //Type 1-In 2-Out
    public function addAttendance(Request $request){
        
        $Leaves = new Leave();
        $Leaves->user_id    = $request->input('user_id');
        $Leaves->user_name  = $request->input('user_name');
        $Leaves->date       = $request->input('date');
        $Leaves->in_time    = $request->input('in_time');
        $Leaves->out_time   = $request->input('out_time');
        $Leaves->type       = $request->input('type');   
       
        $result = $Leaves->addAttendance($Leaves);
        if($result == 1){
            if($Leaves->type == 1){
                $response = response()->json(['status' => 'success' , 'message' => 'Attendance In successfully']);
            } else{
                $response = response()->json(['status' => 'success' , 'message' => 'Attendance Out successfully']);
            }
        } else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Leave creation failed']);
        }
        return $response;
    }
}	
