<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use File;
use Mail;

require(base_path().'/app/Http/Middleware/Report.php');

class ReportController extends Controller
{

////////////////////////////
//Report Managment Api Calls//
////////////////////////////
/// Current Year Sales Report
    public function salesCurrentYearReport(Request $request){
        $report = new Report();
  
        $result = $report->getCurrentYearSalesReport();

        if(count($result) > 0){
            $response = response()->json(['status' => 'success' , 'message' => '', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'No Sales data found']); 
        }
        return $response;
    }


    /// Sales report with Filters
    public function salesFilterReport(Request $request){
        $report = new Report();
        $report->type = $request->input('type');  //1 = Month Wise 2 = Date Wise
        //Check Branch Filter
        if($request->input('branch_id')){
        	$report->branch_id = $request->input('branch_id');
        }else{
        	 $report->branch_id = '';
        }
        //Check Year and Month Based
        if($request->input('year')){
        	$report->year = $request->input('year');
        	$report->months = $request->input('months');
        }else{
        	$report->year = '';
        	$report->months = '';
        }
        //Check Date Based
        if($request->input('start_date')){
            $report->start_date = $request->input('start_date');
        }else{
            $report->start_date = '';
        }
        //Check Date Based
        if($request->input('end_date')){
            $report->end_date = $request->input('end_date');
        }else{
            $report->end_date = '';
        }
        //Check Region Based Filter
        if($request->input('region_id')){
        	$report->region_id = $request->input('region_id');
        }else{
        	$report->region_id = '';
        }
        //Check Area Based Filter
        if($request->input('area_id')){
        	$report->area_id = $request->input('area_id');
        }else{
        	$report->area_id = '';
        }
        //Check Product Based Filter
        if($request->input('product_id')){
 			$report->product_id = $request->input('product_id');
        }else{
        	$report->product_id = '';
        }
        //Check Customer Based Filter
        if($request->input('customer_id')){
        	$report->customer_id = $request->input('customer_id');
        }else{
        	$report->customer_id = '';
        }
        //Check Filedofficer Based Filter
        if($request->input('field_officer_id')){
        	$report->field_officer_id = $request->input('field_officer_id');
        }else{
        	$report->field_officer_id = '';
        }
        $result = $report->getFilterSalesDatewiseReport($report);
        // if($report->type == 1){
        //     $result = $report->getFilterSalesReport($report);
        // }else if($report->type == 2){
        //     $result = $report->getFilterSalesDatewiseReport($report);
        // }
        

        if(count($result) > 0){
            $response = response()->json(['status' => 'success' , 'message' => '', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'No Sales data found']); 
        }
        return $response;
    }

    public function CustomersalesOrderFilterReport(Request $request){
        $report = new Report();
        $report->type = $request->input('type');  //1 = Month Wise 2 = Date Wise
        //Check Branch Filter
        if($request->input('branch_id')){
        	$report->branch_id = $request->input('branch_id');
        }else{
        	 $report->branch_id = '';
        }
        //Check Year and Month Based
        if($request->input('year')){
        	$report->year = $request->input('year');
        	$report->months = $request->input('months');
        }else{
        	$report->year = '';
        	$report->months = '';
        }
        //Check Date Based
        if($request->input('start_date')){
            $report->start_date = $request->input('start_date');
        }else{
            $report->start_date = '';
        }
        //Check Date Based
        if($request->input('end_date')){
            $report->end_date = $request->input('end_date');
        }else{
            $report->end_date = '';
        }
        //Check Region Based Filter
        if($request->input('region_id')){
        	$report->region_id = $request->input('region_id');
        }else{
        	$report->region_id = '';
        }
        //Check Area Based Filter
        if($request->input('area_id')){
        	$report->area_id = $request->input('area_id');
        }else{
        	$report->area_id = '';
        }
        //Check Product Based Filter
        if($request->input('product_id')){
 			$report->product_id = $request->input('product_id');
        }else{
        	$report->product_id = '';
        }
        //Check Customer Based Filter
        if($request->input('customer_id')){
        	$report->customer_id = $request->input('customer_id');
        }else{
        	$report->customer_id = '';
        }
        //Check Filedofficer Based Filter
        if($request->input('field_officer_id')){
        	$report->field_officer_id = $request->input('field_officer_id');
        }else{
        	$report->field_officer_id = '';
        }
        $result = $report->CustomersalesOrderFilterReport($report);
        // if($report->type == 1){
        //     $result = $report->getFilterSalesReport($report);
        // }else if($report->type == 2){
        //     $result = $report->getFilterSalesDatewiseReport($report);
        // }
        

        if(count($result) > 0){
            $response = response()->json(['status' => 'success' , 'message' => '', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'No Sales data found']); 
        }
        return $response;
    }

    public function VendorsalesOrderFilterReport(Request $request){
        $report = new Report();
        $report->type = $request->input('type');  //1 = Month Wise 2 = Date Wise
        //Check Branch Filter
        if($request->input('branch_id')){
        	$report->branch_id = $request->input('branch_id');
        }else{
        	 $report->branch_id = '';
        }
        //Check Year and Month Based
        if($request->input('year')){
        	$report->year = $request->input('year');
        	$report->months = $request->input('months');
        }else{
        	$report->year = '';
        	$report->months = '';
        }
        //Check Date Based
        if($request->input('start_date')){
            $report->start_date = $request->input('start_date');
        }else{
            $report->start_date = '';
        }
        //Check Date Based
        if($request->input('end_date')){
            $report->end_date = $request->input('end_date');
        }else{
            $report->end_date = '';
        }
        //Check Region Based Filter
        if($request->input('region_id')){
        	$report->region_id = $request->input('region_id');
        }else{
        	$report->region_id = '';
        }
        //Check Area Based Filter
        if($request->input('area_id')){
        	$report->area_id = $request->input('area_id');
        }else{
        	$report->area_id = '';
        }
        //Check Product Based Filter
        if($request->input('product_id')){
 			$report->product_id = $request->input('product_id');
        }else{
        	$report->product_id = '';
        }
        //Check Customer Based Filter
        if($request->input('customer_id')){
        	$report->customer_id = $request->input('customer_id');
        }else{
        	$report->customer_id = '';
        }
        //Check Filedofficer Based Filter
        if($request->input('field_officer_id')){
        	$report->field_officer_id = $request->input('field_officer_id');
        }else{
        	$report->field_officer_id = '';
        }
        $result = $report->VendorsalesOrderFilterReport($report);
        // if($report->type == 1){
        //     $result = $report->getFilterSalesReport($report);
        // }else if($report->type == 2){
        //     $result = $report->getFilterSalesDatewiseReport($report);
        // }
        

        if(count($result) > 0){
            $response = response()->json(['status' => 'success' , 'message' => '', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'No Sales data found']); 
        }
        return $response;
    } 

    public function CustomercountsalesFilterReport(Request $request){
        $report = new Report();
        $report->type = $request->input('type');  //1 = Month Wise 2 = Date Wise
        //Check Branch Filter
        if($request->input('branch_id')){
        	$report->branch_id = $request->input('branch_id');
        }else{
        	 $report->branch_id = '';
        }
        //Check Year and Month Based
        if($request->input('year')){
        	$report->year = $request->input('year');
        	$report->months = $request->input('months');
        }else{
        	$report->year = '';
        	$report->months = '';
        }
        //Check Date Based
        if($request->input('start_date')){
            $report->start_date = $request->input('start_date');
        }else{
            $report->start_date = '';
        }
        //Check Date Based
        if($request->input('end_date')){
            $report->end_date = $request->input('end_date');
        }else{
            $report->end_date = '';
        }
        //Check Region Based Filter
        if($request->input('region_id')){
        	$report->region_id = $request->input('region_id');
        }else{
        	$report->region_id = '';
        }
        //Check Area Based Filter
        if($request->input('area_id')){
        	$report->area_id = $request->input('area_id');
        }else{
        	$report->area_id = '';
        }
        //Check Product Based Filter
        if($request->input('product_id')){
 			$report->product_id = $request->input('product_id');
        }else{
        	$report->product_id = '';
        }
        //Check Customer Based Filter
        if($request->input('customer_id')){
        	$report->customer_id = $request->input('customer_id');
        }else{
        	$report->customer_id = '';
        }
        //Check Filedofficer Based Filter
        if($request->input('field_officer_id')){
        	$report->field_officer_id = $request->input('field_officer_id');
        }else{
        	$report->field_officer_id = '';
        }
        $result = $report->CustomercountsalesFilterReport($report);
        // if($report->type == 1){
        //     $result = $report->getFilterSalesReport($report);
        // }else if($report->type == 2){
        //     $result = $report->getFilterSalesDatewiseReport($report);
        // }
        

        if(count($result) > 0){
            $response = response()->json(['status' => 'success' , 'message' => '', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'No Sales data found']); 
        }
        return $response;
    }

    public function VendorcountsalesFilterReport(Request $request){
        $report = new Report();
        $report->type = $request->input('type');  //1 = Month Wise 2 = Date Wise
        //Check Branch Filter
        if($request->input('branch_id')){
        	$report->branch_id = $request->input('branch_id');
        }else{
        	 $report->branch_id = '';
        }
        //Check Year and Month Based
        if($request->input('year')){
        	$report->year = $request->input('year');
        	$report->months = $request->input('months');
        }else{
        	$report->year = '';
        	$report->months = '';
        }
        //Check Date Based
        if($request->input('start_date')){
            $report->start_date = $request->input('start_date');
        }else{
            $report->start_date = '';
        }
        //Check Date Based
        if($request->input('end_date')){
            $report->end_date = $request->input('end_date');
        }else{
            $report->end_date = '';
        }
        //Check Region Based Filter
        if($request->input('region_id')){
        	$report->region_id = $request->input('region_id');
        }else{
        	$report->region_id = '';
        }
        //Check Area Based Filter
        if($request->input('area_id')){
        	$report->area_id = $request->input('area_id');
        }else{
        	$report->area_id = '';
        }
        //Check Product Based Filter
        if($request->input('product_id')){
 			$report->product_id = $request->input('product_id');
        }else{
        	$report->product_id = '';
        }
        //Check Customer Based Filter
        if($request->input('customer_id')){
        	$report->customer_id = $request->input('customer_id');
        }else{
        	$report->customer_id = '';
        }
        //Check Filedofficer Based Filter
        if($request->input('field_officer_id')){
        	$report->field_officer_id = $request->input('field_officer_id');
        }else{
        	$report->field_officer_id = '';
        }
        $result = $report->VendorcountsalesFilterReport($report);
        // if($report->type == 1){
        //     $result = $report->getFilterSalesReport($report);
        // }else if($report->type == 2){
        //     $result = $report->getFilterSalesDatewiseReport($report);
        // }
        

        if(count($result) > 0){
            $response = response()->json(['status' => 'success' , 'message' => '', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'No Sales data found']); 
        }
        return $response;
    }

 ////////////////////////
 ///Coverage Analysis///
 ///////////////////////
 /// Current Year Coverage Report
    public function coverageCurrentYearReport(Request $request){
        $report = new Report();
  
        $result = $report->getCurrentYearCoverageReport();

        if(count($result) > 0){
            $response = response()->json(['status' => 'success' , 'message' => '', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'No Coverage data found']); 
        }
        return $response;
    }

     ///Coverage Filter based Report
    public function coverageFilterReport(Request $request){
        $report = new Report();
  	     $report->type = $request->input('type');
  		//Check Year and Month Based
        if($request->input('year')){
        	$report->year = $request->input('year');
        	$report->months = $request->input('months');
        }else{
        	$report->year = '';
        	$report->months = '';
        }
        //Check Doctor Filter
        if($request->input('doctor_id')){
        	$report->doctor_id = $request->input('doctor_id');
        }else{
        	 $report->doctor_id = '';
        }        
        //Check Branch Filter
        if($request->input('branch_id')){
        	$report->branch_id = $request->input('branch_id');
        }else{
        	 $report->branch_id = '';
        }
        //Check Region Based Filter
        if($request->input('region_id')){
        	$report->region_id = $request->input('region_id');
        }else{
        	$report->region_id = '';
        }
        //Check Area Based Filter
        if($request->input('area_id')){
        	$report->area_id = $request->input('area_id');
        }else{
        	$report->area_id = '';
        }
        //Check Filedofficer Based Filter
        if($request->input('field_officer_id')){
        	$report->field_officer_id = $request->input('field_officer_id');
        }else{
        	$report->field_officer_id = '';
        }

		//Check Customer Based Filter
        if($request->input('customer_id')){
        	$report->customer_id = $request->input('customer_id');
        }else{
        	$report->customer_id = '';
        }

        //Check Coverage Filter
        if($request->input('cType')){
        	$report->cType = $request->input('cType');
        }else{
        	 $report->cType = '';
        }  

        $result = $report->getCoverageFilterReport($report);

        if(count($result) > 0){
            $response = response()->json(['status' => 'success' , 'message' => '', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'No Coverage data found']); 
        }
        return $response;
    }

/////////////////////
//Daily Call Report//
/////////////////////
/// Daily Call Report 
    public function dailyCallReport(Request $request){
        $report = new Report();
        
        //Check Branch Filter
        if($request->input('branch_id')){
            $report->branch_id = $request->input('branch_id');
        }else{
             $report->branch_id = '';
        }
        //Check Region Based Filter
        if($request->input('region_id')){
            $report->region_id = $request->input('region_id');
        }else{
            $report->region_id = '';
        }
        //Check Area Based Filter
        if($request->input('area_id')){
            $report->area_id = $request->input('area_id');
        }else{
            $report->area_id = '';
        }
        //Check Customer Based Filter
        if($request->input('customer_id')){
            $report->customer_id = $request->input('customer_id');
        }else{
            $report->customer_id = '';
        }
        //Check Filedofficer Based Filter
        if($request->input('field_officer_id')){
            $report->field_officer_id = $request->input('field_officer_id');

        }else{
            $report->field_officer_id = '';
        }

        //Check Status
        if($request->input('task_status')){
            $report->task_status = $request->input('task_status');

        }else{
            $report->task_status = '';
        }

        $report->start_date = $request->input('start_date');
        $report->end_date = $request->input('end_date');
        $report->type = $request->input('type');
        $report->report_type = $request->input('report_type');

        $result = $report->dailyCallReport($report);

        if(count($result) > 0){
            $response = response()->json(['status' => 'success' , 'message' => '', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'No Daily data found']);
        }
        return $response;
    }

////////////////////
///Visited Report///
////////////////////
 /// Current Year Visited Report
    public function visitedCurrentYearReport(Request $request){
        $report = new Report();
  
        $result = $report->getCurrentYearVisitedReport();

        if($result){
            $response = response()->json(['status' => 'success' , 'message' => '', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'No Visited data found']); 
        }
        return $response;
    }

     ///Visited Filter based Report
    public function visitedFilterReport(Request $request){
        $report = new Report();
    
        //Check Year and Month Based
        if($request->input('year')){
            $report->year = $request->input('year');
            $report->months = $request->input('months');
        }else{
            $report->year = '';
            $report->months = '';
        }
        //Check Branch Filter
        if($request->input('branch_id')){
            $report->branch_id = $request->input('branch_id');
        }else{
             $report->branch_id = '';
        }
        //Check Region Based Filter
        if($request->input('region_id')){
            $report->region_id = $request->input('region_id');
        }else{
            $report->region_id = '';
        }
        //Check Area Based Filter
        if($request->input('area_id')){
            $report->area_id = $request->input('area_id');
        }else{
            $report->area_id = '';
        }
        //Check Filedofficer Based Filter
        if($request->input('field_officer_id')){
            $report->field_officer_id = $request->input('field_officer_id');
        }else{
            $report->field_officer_id = '';
        }

        $result = $report->getVisitedFilterReport($report);

        if(count($result) > 0){
            $response = response()->json(['status' => 'success' , 'message' => '', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'No Visited data found']); 
        }
        return $response;
    }
////////////////////
///Target Report////
////////////////////
    public function targetDetailReport(Request $request){
        $report = new Report();
        $result = $report->targetDetailReport($request);

        if(count($result) > 0){

            $response = response()->json(['status' => 'success' , 'message' => '', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'No Target data found']); 
        }
        return $response;
    }

////////////////////
///Samples Report///
////////////////////
    /// Current Year Samples Report
    public function samplesCurrentYearReport(Request $request){
        $report = new Report();
  
        $result = $report->getCurrentYearSamplesReport();

        if(count($result) > 0){
            $response = response()->json(['status' => 'success' , 'message' => '', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'No Sales data found']); 
        }
        return $response;
    }


    /// Samples report with Filters
    public function samplesFilterReport(Request $request){
        $report = new Report();
        $report->start_date = $request->input('start_date');
        $report->end_date = $request->input('end_date');
        
        //Check Branch Filter
        if($request->input('branch_id')){
        	$report->branch_id = $request->input('branch_id');
        }else{
        	 $report->branch_id = '';
        }
        //Check Region Based Filter
        if($request->input('region_id')){
        	$report->region_id = $request->input('region_id');
        }else{
        	$report->region_id = '';
        }
        //Check Region Based Filter
        if($request->input('area_id')){
            $report->area_id = $request->input('area_id');
        }else{
            $report->area_id = '';
        }
        //Check Customer Based Filter
        if($request->input('customer_id')){
        	$report->customer_id = $request->input('customer_id');
        }else{
        	$report->customer_id = '';
        }
        //Check Filedofficer Based Filter
        if($request->input('field_officer_id')){
        	$report->field_officer_id = $request->input('field_officer_id');
        }else{
        	$report->field_officer_id = '';
        }

        if($request->input('report_type')){
            $report->report_type = $request->input('report_type');
        }else{
            $report->report_type = '';
        }        
        if($request->input('sample_id')){
            $report->sample_id = $request->input('sample_id');
        }else{
            $report->sample_id = '';
        }        
    
        $result = $report->getFilterSamplesReport($report);

        if(count($result) > 0){
            $response = response()->json(['status' => 'success' , 'message' => '', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'No Sales data found']); 
        }
        return $response;
    }


////////////////////
///Gift Report///
////////////////////
    /// Current Year Gift Report
    public function giftCurrentYearReport(Request $request){
        $report = new Report();
  
        $result = $report->getCurrentYearGiftReport();

        if(count($result) > 0){
            $response = response()->json(['status' => 'success' , 'message' => '', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'No Sales data found']); 
        }
        return $response;
    }


    /// Gift report with Filters
    public function giftFilterReport(Request $request){
        $report = new Report();
        //Type 1 = Data, 2= Barchart,3=Doctorwise Chart,4=Pharmacy wise,5=Hospital Wise.
        $report->type = $request->input('type');
        //Check Branch Filter
        if($request->input('branch_id')){
        	$report->branch_id = $request->input('branch_id');
        }else{
        	 $report->branch_id = '';
        }
        $report->start_date = $request->input('start_date');
        $report->end_date = $request->input('end_date');
        //Check Region Based Filter
        if($request->input('region_id')){
        	$report->region_id = $request->input('region_id');
        }else{
        	$report->region_id = '';
        }
        //Check Area Based Filter
        if($request->input('area_id')){
            $report->area_id = $request->input('area_id');
        }else{
            $report->area_id = '';
        }
        //Check Customer Based Filter
        if($request->input('customer_id')){
        	$report->customer_id = $request->input('customer_id');
        }else{
        	$report->customer_id = '';
        }
        //Check Filedofficer Based Filter
        if($request->input('field_officer_id')){
        	$report->field_officer_id = $request->input('field_officer_id');
        }else{
        	$report->field_officer_id = '';
        }
        if($request->input('gift_id')){
            $report->gift_id = $request->input('gift_id');
        }else{
            $report->gift_id = '';
        }

        $result = $report->getFilterGiftReport($report);

        if(count($result) > 0){
            $response = response()->json(['status' => 'success' , 'message' => '', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'No Sales data found']); 
        }
        return $response;
    }

//APP SIDE Report WITH PDF GENERATION
    //1-Daily Call Report
    public function appDailyCallReport(Request $request){
        $report = new Report();
        //Check Branch Filter
        if($request->input('branch_id')){
            $report->branch_id = $request->input('branch_id');
        }else{
             $report->branch_id = '';
        }
        //Check Region Based Filter
        if($request->input('region_id')){
            $report->region_id = $request->input('region_id');
        }else{
            $report->region_id = '';
        }
        //Check Area Based Filter
        if($request->input('area_id')){
            $report->area_id = $request->input('area_id');
        }else{
            $report->area_id = '';
        }
        //Check Filedofficer Based Filter
        if($request->input('field_officer_id')){
            $report->field_officer_id = $request->input('field_officer_id');

        }else{
            $report->field_officer_id = '';
        }

        $report->email_id = $request->input('email_id');
        $report->start_date = $request->input('start_date');
        $report->end_date = $request->input('end_date');

        $result = $report->appDailyCallReport($report);

        if($result == 1){
            $response = response()->json(['status' => 'success' , 'message' => 'Report Send To Your Mail']);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'No Daily data found']); 
        }
        return $response;
    }
    //2-App Visited Report
    public function appVisitedReport(Request $request){
        $report = new Report();
    
        //Check Year and Month Based
        if($request->input('year')){
            $report->year = $request->input('year');
            $report->months = $request->input('months');
        }else{
            $report->year = '';
            $report->months = '';
        }
        //Check Branch Filter
        if($request->input('branch_id')){
            $report->branch_id = $request->input('branch_id');
        }else{
             $report->branch_id = '';
        }
        //Check Region Based Filter
        if($request->input('region_id')){
            $report->region_id = $request->input('region_id');
        }else{
            $report->region_id = '';
        }
        //Check Area Based Filter
        if($request->input('area_id')){
            $report->area_id = $request->input('area_id');
        }else{
            $report->area_id = '';
        }
        //Check Filedofficer Based Filter
        if($request->input('field_officer_id')){
            $report->field_officer_id = $request->input('field_officer_id');
        }else{
            $report->field_officer_id = '';
        }

        $report->email_id = $request->input('email_id');
        $result = $report->appVisitedReport($report);

        if($result == 1){
            $response = response()->json(['status' => 'success' , 'message' => 'Report Send To Your Mail']);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'No Visited data found']); 
        }
        return $response;
    }
    /// 3-App Sales Report
    public function appSalesReport(Request $request){
        $report = new Report();

        //Check Branch Filter
        if($request->input('branch_id')){
            $report->branch_id = $request->input('branch_id');
        }else{
             $report->branch_id = '';
        }
        //Check Year and Month Based
        if($request->input('year')){
            $report->year = $request->input('year');
            $report->months = $request->input('months');
        }else{
            $report->year = '';
            $report->months = '';
        }
        //Check Region Based Filter
        if($request->input('region_id')){
            $report->region_id = $request->input('region_id');
        }else{
            $report->region_id = '';
        }
        //Check Area Based Filter
        if($request->input('area_id')){
            $report->area_id = $request->input('area_id');
        }else{
            $report->area_id = '';
        }
        //Check Product Based Filter
        if($request->input('product_id')){
            $report->product_id = $request->input('product_id');
        }else{
            $report->product_id = '';
        }
        //Check Customer Based Filter
        if($request->input('customer_id')){
            $report->customer_id = $request->input('customer_id');
        }else{
            $report->customer_id = '';
        }
        //Check Filedofficer Based Filter
        if($request->input('field_officer_id')){
            $report->field_officer_id = $request->input('field_officer_id');
        }else{
            $report->field_officer_id = '';
        }
        
        $report->email_id = $request->input('email_id');
        $result = $report->appSalesReport($report);

        if($result == 1){
            $response = response()->json(['status' => 'success' , 'message' => 'Report Send To Your Mail']);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'No Sales data found']); 
        }
        return $response;
    }
    ///4-App coverage Report
    public function appCoverageReport(Request $request){
        $report = new Report();
    
        //Check Year and Month Based
        if($request->input('year')){
            $report->year = $request->input('year');
            $report->months = $request->input('months');
        }else{
            $report->year = '';
            $report->months = '';
        }
        //Check Doctor Filter
        if($request->input('doctor_id')){
            $report->doctor_id = $request->input('doctor_id');
        }else{
             $report->doctor_id = '';
        }        
        //Check Branch Filter
        if($request->input('branch_id')){
            $report->branch_id = $request->input('branch_id');
        }else{
             $report->branch_id = '';
        }
        //Check Region Based Filter
        if($request->input('region_id')){
            $report->region_id = $request->input('region_id');
        }else{
            $report->region_id = '';
        }
        //Check Area Based Filter
        if($request->input('area_id')){
            $report->area_id = $request->input('area_id');
        }else{
            $report->area_id = '';
        }
        //Check Filedofficer Based Filter
        if($request->input('field_officer_id')){
            $report->field_officer_id = $request->input('field_officer_id');
        }else{
            $report->field_officer_id = '';
        }

        //Check Customer Based Filter
        if($request->input('customer_id')){
            $report->customer_id = $request->input('customer_id');
        }else{
            $report->customer_id = '';
        }
        $report->email_id = $request->input('email_id');
        $result = $report->appCoverageReport($report);

        if($result == 1){
            $response = response()->json(['status' => 'success' , 'message' => 'Report Send To Your Mail']);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'No Coverage data found']); 
        }
        return $response;
    }
    /// 5-App Samples Report
    public function appSamplesReport(Request $request){
        $report = new Report();

        //Check Branch Filter
        if($request->input('branch_id')){
            $report->branch_id = $request->input('branch_id');
        }else{
             $report->branch_id = '';
        }
        //Check Year and Month Based
        if($request->input('year')){
            $report->year = $request->input('year');
            $report->months = $request->input('months');
        }else{
            $report->year = '';
            $report->months = '';
        }
        //Check Region Based Filter
        if($request->input('region_id')){
            $report->region_id = $request->input('region_id');
        }else{
            $report->region_id = '';
        }
        //Check Customer Based Filter
        if($request->input('customer_id')){
            $report->customer_id = $request->input('customer_id');
        }else{
            $report->customer_id = '';
        }
        //Check Filedofficer Based Filter
        if($request->input('field_officer_id')){
            $report->field_officer_id = $request->input('field_officer_id');
        }else{
            $report->field_officer_id = '';
        }
        $report->email_id = $request->input('email_id');
        $result = $report->appSamplesReport($report);

        if($result == 1){
            $response = response()->json(['status' => 'success' , 'message' => 'Report Send To Your Mail']);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'No Sales data found']); 
        }
        return $response;
    }
    /// 6-App Gift Report
    public function appGiftReport(Request $request){
        $report = new Report();

        //Check Branch Filter
        if($request->input('branch_id')){
            $report->branch_id = $request->input('branch_id');
        }else{
             $report->branch_id = '';
        }
        //Check Year and Month Based
        if($request->input('year')){
            $report->year = $request->input('year');
            $report->months = $request->input('months');
        }else{
            $report->year = '';
            $report->months = '';
        }
        //Check Region Based Filter
        if($request->input('region_id')){
            $report->region_id = $request->input('region_id');
        }else{
            $report->region_id = '';
        }
        //Check Customer Based Filter
        if($request->input('customer_id')){
            $report->customer_id = $request->input('customer_id');
        }else{
            $report->customer_id = '';
        }
        //Check Filedofficer Based Filter
        if($request->input('field_officer_id')){
            $report->field_officer_id = $request->input('field_officer_id');
        }else{
            $report->field_officer_id = '';
        }
        $report->email_id = $request->input('email_id');
        $result = $report->appGiftReport($report);

        if($result == 1){
            $response = response()->json(['status' => 'success' , 'message' => 'Report Send To Your Mail']);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'No Sales data found']); 
        }
        return $response;
    }
    /// 7-App Target Report
    public function appTargetReport(Request $request){
        $report = new Report();
        
        //Check Year and Month Based
        if($request->input('year')){
            $report->year = $request->input('year');
            $report->months = $request->input('months');
        }else{
            $report->year = '';
            $report->months = '';
        }
        //Check Branch Filter
        if($request->input('branch_id')){
            $report->branch_id = $request->input('branch_id');
        }else{
             $report->branch_id = '';
        }
        //Check Region Based Filter
        if($request->input('region_id')){
            $report->region_id = $request->input('region_id');
        }else{
            $report->region_id = '';
        }
        //Check Area Based Filter
        if($request->input('area_id')){
            $report->area_id = $request->input('area_id');
        }else{
            $report->area_id = '';
        }
        //Check Filedofficer Based Filter
        if($request->input('field_officer_id')){
            $report->field_officer_id = $request->input('field_officer_id');

        }else{
            $report->field_officer_id = '';
        }
        $report->email_id = $request->input('email_id');

        $result = $report->appTargetReport($report);

        if($result == 1){
            $response = response()->json(['status' => 'success' , 'message' => 'Report Send To Your Mail']);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'No Target data found']); 
        }
        return $response;
    }

    //8-App Side Chart Data
    public function appTargetDetailChart(Request $request){
        $report = new Report();
        //Check Year and Month Based
        if($request->input('year')){
            $report->year = $request->input('year');
            $report->months = $request->input('months');
        }else{
            $report->year = '';
            $report->months = '';
        }
        
        $report->field_officer_id = $request->input('field_officer_id');
        $result = $report->appTargetDetailChart($report);

        if(count($result) > 0){
            $response = response()->json(['status' => 'success' , 'message' => '', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'No Target data found']); 
        }
        return $response;
    }
    //9-Field Officer Task Missed Call Report
    public function appMissedTaskCallReport(Request $request){
        $report = new Report();
        //Check Branch Filter
        if($request->input('branch_id')){
            $report->branch_id = $request->input('branch_id');
        }else{
             $report->branch_id = '';
        }
        //Check Region Based Filter
        if($request->input('region_id')){
            $report->region_id = $request->input('region_id');
        }else{
            $report->region_id = '';
        }
        //Check Area Based Filter
        if($request->input('area_id')){
            $report->area_id = $request->input('area_id');
        }else{
            $report->area_id = '';
        }
        //Check Filedofficer Based Filter
        if($request->input('field_officer_id')){
            $report->field_officer_id = $request->input('field_officer_id');

        }else{
            $report->field_officer_id = '';
        }

        $report->email_id = $request->input('email_id');
        $report->start_date = $request->input('start_date');
        $report->end_date = $request->input('end_date');
        
        $result = $report->appMissedTaskCallReport($report);

        if($result == 1){
            $response = response()->json(['status' => 'success' , 'message' => 'Report Send To Your Mail']);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'No Daily data found']); 
        }
        return $response;
    }

    public function productSalesReport(Request $request){

        $report = new Report();

        if($request->input('region_id')){
            $report->region_id = $request->input('region_id');
        }else{
            $report->region_id = '';
        }
        //Check Area Based Filter
        if($request->input('area_id')){
            $report->area_id = $request->input('area_id');
        }else{
            $report->area_id = '';
        }
        // if($request->input('product_id')){
        //     $report->product_id = $request->input('product_id');
        // }else{
        //     $report->area_id = '';
        // }

        $report->start_date = $request->input('start_date');
        $report->end_date = $request->input('end_date');
        $report->product_id = $request->input('product_id');

        $result = $report->productSalesReport($report);

        if($result == 1){
            $response = response()->json(['status' => 'success' , 'message' => 'Report Send To Your Mail']);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'No Daily data found']); 
        }
        return $response;

    }
      public function customerReportForProductIdBased(Request $request){
        $Orders = new Report();

        $Orders->start_date = $request->input('start_date');
        $Orders->end_date = $request->input('end_date');

        if($request->input('region_id')){
            $Orders->region_id = $request->input('region_id');
        }else{
            $Orders->region_id = '';
        }

        if($request->input('area_id')){
            $Orders->area_id = $request->input('area_id');
        }else{
            $Orders->area_id = '';
        }

        if($request->input('warehouse_id')){
            $Orders->warehouse_id = $request->input('warehouse_id');
        }else{
            $Orders->warehouse_id='';
        }

        if($request->input('product_id')){
            $Orders->product_id = $request->input('product_id');
        }else{
            $Orders->product_id = '';
        }

        if($request->input('field_officer_id')){
            $Orders->field_officer_id = $request->input('field_officer_id');
        }else{
            $Orders->field_officer_id = '';
        }

        if($request->input('stockist_id')){
            $Orders->stockist_id = $request->input('stockist_id');
        }else{
            $Orders->stockist_id = '';
        }

        // if($request->input('cType')){
        //     $Orders->cType = $request->input('cType');
        // }else{
        //     $Orders->cType = '';
        // }

        $result = $Orders->customerReportForProductIdBased($Orders);
        
        if(count($result) > 0){
            $response = response()->json(['status' => 'success' , 'message' => '', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'No Sales data found']); 
        }
        return $response;
    }

    public function customerOrderReportForProductIdBased(Request $request){
        $Orders = new Report();

        $Orders->start_date = $request->input('start_date');
        $Orders->end_date = $request->input('end_date');

        if($request->input('region_id')){
            $Orders->region_id = $request->input('region_id');
        }else{
            $Orders->region_id = '';
        }

        if($request->input('area_id')){
            $Orders->area_id = $request->input('area_id');
        }else{
            $Orders->area_id = '';
        }

        if($request->input('warehouse_id')){
            $Orders->warehouse_id = $request->input('warehouse_id');
        }else{
            $Orders->warehouse_id='';
        }

        if($request->input('product_id')){
            $Orders->product_id = $request->input('product_id');
        }else{
            $Orders->product_id = '';
        }

        if($request->input('field_officer_id')){
            $Orders->field_officer_id = $request->input('field_officer_id');
        }else{
            $Orders->field_officer_id = '';
        }

        if($request->input('stockist_id')){
            $Orders->stockist_id = $request->input('stockist_id');
        }else{
            $Orders->stockist_id = '';
        }

        // if($request->input('cType')){
        //     $Orders->cType = $request->input('cType');
        // }else{
        //     $Orders->cType = '';
        // }

        $result = $Orders->customerOrderReportForProductIdBased($Orders);
        
        if(count($result) > 0){
            $response = response()->json(['status' => 'success' , 'message' => '', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'No Sales data found']); 
        }
        return $response;
    }

    public function discountReportSummary(Request $request){
        $report = new Report();

        $report->type = $request->input('type');
        $report->start_date = $request->input('start_date');
        $report->end_date = $request->input('end_date');
        // $report->customer_id = $request->input('customer_id');

        if($request->input('customer_id')){
            $report->customer_id = $request->input('customer_id');
        }else{
            $report->customer_id = '';
        }

        if($request->input('vendor_id')){
            $report->vendor_id = $request->input('vendor_id');
        }else{
            $report->vendor_id ='';
        }

        $result = $report->discountReportSummary($report);

        if($report){
            $response = response()->json(['status'=>'success', 'message'=>'', 'data'=> $result]);
        }else{
            $response = response()->json(['status'=>'failure', 'message'=>'No data found']);
        }
        return $response;
    }

    public function discountReport(Request $request){
        $report = new Report();

        $report->type = $request->input('type');
        $report->report_type = $request->input('report_type');
        $report->start_date = $request->input('start_date');
        $report->end_date = $request->input('end_date');
        // $report->customer_id = $request->input('customer_id');
        if($request->input('branch_id')){
            $report->branch_id = $request->input('branch_id');
        }else{
            $report->branch_id = '';
        }
        if($request->input('region_id')){
            $report->region_id = $request->input('region_id');
        }else{
            $report->region_id = '';
        }
        if($request->input('area_id')){
            $report->area_id = $request->input('area_id');
        }else{
            $report->area_id = '';
        }
        if($request->input('customer_id')){
            $report->customer_id = $request->input('customer_id');
        }else{
            $report->customer_id = '';
        }
        if($request->input('billed_by')){
            $report->billed_by = $request->input('billed_by');
        }else{
            $report->billed_by = '';
        }

        // if($request->input('vendor_id')){
        //     $report->vendor_id = $request->input('vendor_id');
        // }else{
        //     $report->vendor_id ='';
        // }

        $result = $report->discountReport($report);

        if($report){
            $response = response()->json(['status'=>'success', 'message'=>'', 'data'=> $result]);
        }else{
            $response = response()->json(['status'=>'failure', 'message'=>'No data found']);
        }
        return $response; 
    }

    public function getMainWarehouse(){

        $report = new Report();

        $result = $report->getMainWarehouse();

        if($report){
            $response = response()->json(['status'=>'success', 'message'=>'', 'data'=> $result]);
        }else{
            $response = response()->json(['status'=>'failure', 'message'=>'No data found']);
        }
        return $response;

    }
    public function subWarehouseInStocksReport(Request $request){
        
        $report = new Report();

        $report->warehouse_id = $request->input('warehouse_id');
        $report->type      = $request->input('type');
        $report->start_date  = $request->input('start_date');
        // print_r($request->input('start_date'));
        $report->end_date    = $request->input('end_date');

        if($request->input('product_id')){
            $report->product_id = $request->input('product_id');
        }else{
            $report->product_id = '';
        }

        if($request->input('sub_ware_id')){
            $report->sub_ware_id = $request->input('sub_ware_id');
        }else{
            $report->sub_ware_id = '';
        }

        $result = $report->subWarehouseInStocksReport($report);

        if($report){
            $response = response()->json(['status'=>'success', 'message'=>'', 'data'=> $result]);
        }else{
            $response = response()->json(['status'=>'failure', 'message'=>'No data found']);
        }
        return $response; 
    }
    
    public function subWarehouseItemMovingReport(Request $request){
        
        $report = new Report();

        $report->warehouse_id = $request->input('warehouse_id');
        $report->type      = $request->input('type');
        $report->start_date  = $request->input('start_date');
        // print_r($request->input('start_date'));
        $report->end_date    = $request->input('end_date');

        if($request->input('product_id')){
            $report->product_id = $request->input('product_id');
        }else{
            $report->product_id = '';
        }

        if($request->input('sub_ware_id')){
            $report->sub_ware_id = $request->input('sub_ware_id');
        }else{
            $report->sub_ware_id = '';
        }

        $result = $report->subWarehouseItemMovingReport($report);

        if($report){
            $response = response()->json(['status'=>'success', 'message'=>'', 'data'=> $result]);
        }else{
            $response = response()->json(['status'=>'failure', 'message'=>'No data found']);
        }
        return $response; 
    }

    public function subWarehouseItemMovingReportSumm(Request $request){
        
        $report = new Report();

        $report->warehouse_id = $request->input('warehouse_id');
        $report->type      = $request->input('type');
        $report->start_date  = $request->input('start_date');
        $report->end_date    = $request->input('end_date');

        if($request->input('product_id')){
            $report->product_id = $request->input('product_id');
        }else{
            $report->product_id = '';
        }

        if($request->input('sub_ware_id')){
            $report->sub_ware_id = $request->input('sub_ware_id');
        }else{
            $report->sub_ware_id = '';
        }
        
        $result = $report->subWarehouseItemMovingReportSumm($report);

        if($report){
            $response = response()->json(['status'=>'success', 'message'=>'', 'data'=> $result]);
        }else{
            $response = response()->json(['status'=>'failure', 'message'=>'No data found']);
        }
        return $response; 
    }

    public function mainWarehouseItemMovingRequest(Request $request){
        $report = new Report();

        $report->warehouse_id = $request->input('warehouse_id');
        $report->start_date = $request->input('start_date');
        $report->end_date = $request->input('end_date');

        if($request->input('sub_ware_id')){
            $report->sub_ware_id = $request->input('sub_ware_id');
        }else{
            $report->sub_ware_id = "";
        }

        if($request->input('product_id')){
            $report->product_id = $request->input('product_id');
        }else{
            $report->product_id = "";
        }

        $result = $report->mainWarehouseItemMovingRequest($report);

        if($report){
            $response = response()->json(['status'=>'success', 'message'=>'', 'data'=> $result]);
        }else{
            $response = response()->json(['status'=>'failure', 'message'=>'No data found']);
        }
        return $response; 

    }

    public function mainWarehouseItemMovingRequestSumm(Request $request){
        $report = new Report();

        $report->warehouse_id = $request->input('warehouse_id');
        $report->start_date = $request->input('start_date');
        $report->end_date = $request->input('end_date');

        if($request->input('sub_ware_id')){
            $report->sub_ware_id = $request->input('sub_ware_id');
        }else{
            $report->sub_ware_id = "";
        }

        if($request->input('product_id')){
            $report->product_id = $request->input('product_id');
        }else{
            $report->product_id = "";
        }

        $result = $report->mainWarehouseItemMovingRequestSumm($report);

        if($report){
            $response = response()->json(['status'=>'success', 'message'=>'', 'data'=> $result]);
        }else{
            $response = response()->json(['status'=>'failure', 'message'=>'No data found']);
        }
        return $response; 

    }

    public function stockReport(Request $request){

        $report = new Report();
        
        $report->start_date = $request->input('start_date');
        $report->end_date = $request->input('end_date');

        if($request->input('warehouse_id')){
            $report->warehouse_id = $request->input('warehouse_id');
        }else{
            $report->warehouse_id = '';
        }

        $results = $report->stockReport($report);

        if($results){
            $response = response()->json(['status'=>'success', 'message'=>'', 'data'=>$results]);
        }else{
            $response = response()->json(['status'=>'failure', 'message'=> 'Something went wrong to retrive the data']);
        }
        return $response;
    }

    public function dailySaleReport(Request $request){

        $report = new Report();

        $report->start_date = $request->input('start_date');
        $report->end_date = $request->input('end_date');

        if($request->input('region_id')){
            $report->region_id = $request->input('region_id');
        }else{
            $report->region_id = '';
        }

        $results = $report->dailySaleReport($report);

        if($results){
            $response = response()->json(['status'=>'success', 'message'=>'', 'data'=>$results]);
        }else{
            $response = response()->json(['status'=>'failure', 'message'=> 'Something went wrong to retrive the data']);
        }
        return $response;
    }

    public function schemeReport(Request $request){
        $report = new Report();

        $report->type = $request->input('type');
        $report->report_type = $request->input('report_type');
        if($request->input('bill_no')){
            $report->bill_no = $request->input('bill_no');
        }else{
            $report->bill_no = '';
        }
        if($request->input('start_date')){
            $report->start_date = $request->input('start_date');
        }else{
            $report->start_date = '';
        }
        if($request->input('end_date')){
            $report->end_date = $request->input('end_date');
        }else{
            $report->end_date = '';
        }
        if($request->input('branch')){
            $report->branch = $request->input('branch');
        }else{
            $report->branch = '';
        }
        if($request->input('region')){
            $report->region = $request->input('region');
        }else{
            $report->region = '';
        }
        if($request->input('zone')){
            $report->zone = $request->input('zone');
        }else{
            $report->zone = '';
        }
        if($request->input('vendor')){
            $report->vendor = $request->input('vendor');
        }else{
            $report->vendor = '';
        }
        if($request->input('customer')){
            $report->customer = $request->input('customer');
        }else{
            $report->customer = '';
        }
        if($request->input('scheme')){
            $report->scheme = $request->input('scheme');
        }else{
            $report->scheme = '';
        }
        if($request->input('billed_by')){
            $report->billed_by = $request->input('billed_by');
        }else{
            $report->billed_by = '';
        }
        if($request->input('product_id')){
            $report->product_id = $request->input('product_id');
        }else{
            $report->product_id = '';
        }
        $result = $report->schemeReport($report);

        if($report){
            $response = response()->json(['status'=>'success', 'message'=>'', 'data'=> $result]);
        }else{
            $response = response()->json(['status'=>'failure', 'message'=>'No data found']);
        }
        return $response; 
    }


    /// Sales Analysis report with Filters
    public function salesAnalysisReport(Request $request){
        $report = new Report();
        $report->type = $request->input('type');
        //Check Branch Filter
        if($request->input('branch_id')){
            $report->branch_id = $request->input('branch_id');
        }else{
             $report->branch_id = '';
        }
        //Check Year and Month Based
        if($request->input('year')){
            $report->year = $request->input('year');
            $report->months = $request->input('months');
        }else{
            $report->year = '';
            $report->months = '';
        }
        //Check Date Based
        if($request->input('start_date')){
            $report->start_date = $request->input('start_date');
        }else{
            $report->start_date = '';
        }
        //Check Date Based
        if($request->input('end_date')){
            $report->end_date = $request->input('end_date');
        }else{
            $report->end_date = '';
        }
        //Check Region Based Filter
        if($request->input('region_id')){
            $report->region_id = $request->input('region_id');
        }else{
            $report->region_id = '';
        }
        //Check Area Based Filter
        if($request->input('area_id')){
            $report->area_id = $request->input('area_id');
        }else{
            $report->area_id = '';
        }
        //Check Product Based Filter
        if($request->input('product_id')){
            $report->product_id = $request->input('product_id');
        }else{
            $report->product_id = '';
        }
        //Check Customer Based Filter
        if($request->input('customer_id')){
            $report->customer_id = $request->input('customer_id');
        }else{
            $report->customer_id = '';
        }
        //Check Filedofficer Based Filter
        if($request->input('field_officer_id')){
            $report->field_officer_id = $request->input('field_officer_id');
        }else{
            $report->field_officer_id = '';
        }
        //Check Areamanager Based Filter
        if($request->input('area_manager_id')){
            $report->area_manager_id = $request->input('area_manager_id');
        }else{
            $report->area_manager_id = '';
        }
        // $result = $report->getSalesAnalysisReport($report);
        if($report->type == 1){
            $result = $report->getCustomerSalesAnalysisReport($report);
        }else if($report->type == 2){
            $result = $report->getVendorSalesAnalysisReport($report);
        }
        

        if(count($result) > 0){
            $response = response()->json(['status' => 'success' , 'message' => '', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'No Sales data found']); 
        }
        return $response;
    }
    
}
