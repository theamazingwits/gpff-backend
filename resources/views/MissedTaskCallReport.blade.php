<!DOCTYPE html>
<html lang="en">
<head>
	<title>Table V01</title>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css"/>

	<style>
		
			  
			  #logo {
				float: left;
				margin-top: 8px;
				margin-left: 30%;
			  }
			  
			  #logo img {
				height: 70px;
			  }

			  .display-1 {
				.font(@font-size-display-1, @line-height-display-1, @letter-spacing-display-1);
				font-weight: @font-weight-display-1;
				color: @font-color-display-1;
				text-transform: inherit;
				
				}
				
				h2 {
					text-align: center;
					padding-top: 20px 0;
				}
				
				.table-bordered {
					border: 1px solid #ddd !important;
				}
				
				table caption {
					padding: .5em 0;
				}
				
				table tfoot tr td {
					text-align: center !important;
				}
				
				@media (max-width: 39.9375em) {
					.tablesaw-stack tbody tr:not(:last-child) {
						border-bottom: 2px solid #0B0B0D;
					}
				}
				
				.p {
					text-align: center;
					padding-top: 140px;
					font-size: 14px;
				}
	</style>

</head>
<body>
		<header class="clearfix">
				{{--  <div id="logo">

				</div>  --}}
			    <center><img src="https://aw-gpff.s3.ap-south-1.amazonaws.com/TARGETSFA1.png"  height="1" alt="TargetSFA" style="padding-bottom: 5%;width: 25%;height: auto;padding-left: 3%"/></center>
				<!-- <h1 class="display-1" style="padding-top: 1%; padding-left: 17%;">Daily Call Report</h1> -->
				</header>
			

<div class="container">
	<div class="date" ><h4 align="left" >Report Type: {{ $Title }}</h4> <h5 align="right">Date: {{ $Start_date }}  -  {{ $End_date }}</h5></div>
	<h4 align="left"></h4 >
  <div class="row">
    <div class="col-xs-12">
      <table summary="This table shows how to create responsive tables using Tablesaw's functionality" class="table table-bordered table-hover tablesaw tablesaw-stack" data-tablesaw-mode="stack">
        <thead>
          <tr>
							<th> S.NO</th>
							<th> Task Id</th>
							<th> Field officer Id</th>
							<th> Field officer Name</th>
							<th> Customer Id</th>
							<th> Customer Name</th>
							<th> Customer Type</th>
							<th> Branch</th>
							<th> Region</th>
							<th> Area</th>
							
          </tr>
        </thead>
				<tbody>
						@foreach($datas  as $key=> $data_details)

				<tr>
				<td class="column0"> {{ ++$key }} </td>
				<td class="column1"> {{  $data_details->task_id }}  </td>
                <td class="column2"> {{  $data_details->field_officer_id }} </td>
                <td class="column2"> {{  $data_details->firstname }} </td>
                <td class="column3"> {{  $data_details->customer_id }} </td>
                <td class="column3"> {{  $data_details->customer_name }} </td>
                @if ($data_details->customer_type == '1')
                <td class="column3"> Doctor </td>
                @elseif ($data_details->customer_type == '2')
                <td class="column3"> Pharmacy </td>
				@else
				<td class="column3"> Hospital </td>
				@endif
				<td class="column2"> {{  $data_details->branch_name }} </td>
                <td class="column2"> {{  $data_details->region_name }} </td>
                <td class="column2"> {{  $data_details->area_name }} </td>
				</tr>
				 $key++;
				 @endforeach
				</tbody>
      </table>
    </div>
  </div>
</div>



</body>
</html>