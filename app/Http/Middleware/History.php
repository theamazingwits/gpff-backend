<?php

namespace App\Http\Controllers;

use Closure;
use DB;
use Mail;
use Carbon\Carbon;

require(base_path().'/app/Http/Middleware/Common.php');

class History
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        return $next($request);
    }
////////////////////////////////
//History Management Api Calls//
////////////////////////////////
    // Get Stock Add History Details
    public function getStockAddHistory($Historys)
    {   
        if($Historys->type == 1){
            return  DB::table('gpff_stock_add_history as gsah')
                ->join('gpff_product as gpro','gsah.orginal_id','gpro.product_id')
                ->where('gsah.region_id', $Historys->region_id)
                ->where('gsah.warehouse_id', $Historys->warehouse_id)
                ->where('gsah.type', $Historys->type)
                ->orderBy('gsah.updated_at','DESC')
                ->get();
        } elseif ($Historys->type == 2) {
            return  DB::table('gpff_stock_add_history as gsah')
                ->join('gpff_gift as ggit','gsah.orginal_id','ggit.gift_id')
                ->where('gsah.region_id', $Historys->region_id)
                ->where('gsah.warehouse_id', $Historys->warehouse_id)
                ->where('gsah.type', $Historys->type)
                ->orderBy('gsah.updated_at','DESC')
                ->get();
        } elseif ($Historys->type == 3) {
            return  DB::table('gpff_stock_add_history as gsah')
                ->join('gpff_samples as gsam','gsah.id','gsam.samples_id')
                ->where('gsah.region_id', $Historys->region_id)
                ->where('gsah.warehouse_id', $Historys->warehouse_id)
                ->where('gsah.type', $Historys->type)
                ->orderBy('gsah.updated_at','DESC')
                ->get();
        } 
        
    }
    // Get Assign Stock History Details
    public function getStockAssignHistory($Historys)
    {   
        return  DB::table('gpff_stock_assign_history')
                ->where('id', $Historys->id)
                ->where('type', $Historys->type)
                ->orderBy('updated_at','DESC')
                ->get();
    }
}
