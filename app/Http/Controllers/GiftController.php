<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use File;
use Mail;

require(base_path().'/app/Http/Middleware/Gift.php');

class GiftController extends Controller
{
///////////////////////////////////////
//Gift Inventory Management Api Calls//
///////////////////////////////////////
    //Admin Or Sub Admin Add the Gift 
	public function addGift(Request $request){
    	
        $Gifts = new Gift();
        $Gifts->gift_name  			= $request->input('gift_name');
        $Gifts->gift_description    = $request->input('gift_description');
        $Gifts->gift_cr_id  		= $request->input('gift_cr_id');
        $Gifts->region_id           = $request->input('region_id');
        $Gifts->region_name         = $request->input('region_name');
        $Gifts->branch_id           = $request->input('branch_id');
       
        $result = $Gifts->addGift($Gifts);
        print_r($result);
        if($result == 1){
            $response = response()->json(['status' => 'success' , 'message' => 'Gift added successfully']);
        }else if($result == 2){
            $response = response()->json(['status' => 'success' , 'message' => 'Gift Already Exists']);
        } else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Gift added failed']);
        }
        return $response;
    }
    // Update Gift Details
    public function updateGift(Request $request)
    {
        $Gifts = new Gift();
        $Gifts->gift_id      		= $request->input('gift_id');
        $Gifts->gift_name  			= $request->input('gift_name');
        $Gifts->gift_description  	= $request->input('gift_description');
        $Gifts->gift_cr_id  		= $request->input('gift_cr_id');
        $Gifts->region_id           = $request->input('region_id');
        $Gifts->region_name         = $request->input('region_name');
        
        $result = $Gifts->updateGift($Gifts);
        if($result > 0){
            $response = response()->json(['status' => 'success', 'message' => 'Gift Details Update successfully','data'=> $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Error in Update Gift Details']);
        }
        return $response;
    }
    // Delete Gift details
    public function removeGift(Request $request){
        
        $Gifts = new Gift();
        $Gifts->gift_id  = $request->input('gift_id');

        $result = $Gifts->removeGift($Gifts);
        if($result > 0){
            $response = response()->json(['status' => 'success' ,'message' => 'Gift details deleted successfully']);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Error in deleting']); 
        }
        return $response;
    }
    // Get Indivitual Gift Details
    public function getIndGift($gift_id)
    {
        $Gifts = new Gift();
        $Gifts->gift_id = $gift_id;

        $result = $Gifts->getIndGift($Gifts);
        if(count($result) > 0){
            $response = response()->json(['status' => 'success' , 'message' => '', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Invalid Gift details']); 
        }
        return $response;
    } 
    // Fetch (All)Gift Details
    public function getAllGift()
    {
        $Gifts = new Gift();

        $data = $Gifts->getAllGift();
        if(count($data) > 0 ){
            $response = response()->json(['status' => 'success' , 'message' => 'Gift Data retrived successfully','data'=>$data]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Error in Get details..','data'=>'null']);
        }
        return $response;
    }   
    // Fetch Region Based gift Details
    public function getRegionBasedGift($region_id)
    {
        $Gifts = new Gift();
        $Gifts->region_id = $region_id;

        $data = $Gifts->getRegionBasedGift($Gifts);
        if(count($data) > 0 ){
            $response = response()->json(['status' => 'success' , 'message' => 'gift Based Products Data retrived successfully','data'=>$data]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Error in Get details..','data'=>'null']);
        }
        return $response;
    } 



    //Stockist SIde
    //Admin Or Sub Admin Add the Gift 
    public function addGiftQty(Request $request){
        
        $Gifts = new Gift();
        $Gifts->gift_id             = $request->input('gift_id');
        $Gifts->gift_name           = $request->input('gift_name');
        $Gifts->gift_qty            = $request->input('gift_qty');
        $Gifts->gift_description    = $request->input('gift_description');
        $Gifts->region_id           = $request->input('region_id');
        $Gifts->branch_id           = $request->input('branch_id');
        $Gifts->warehouse_id        = $request->input('warehouse_id');
        $Gifts->stockist_id         = $request->input('stockist_id');
       
        $result = $Gifts->addGiftQty($Gifts);
        if($result == 1){
            $response = response()->json(['status' => 'success' , 'message' => 'Gift added successfully']);
        } else if($result == 2){
            $response = response()->json(['status' => 'success' , 'message' => 'Gift Already In Stock']);
        } else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Gift added failed']);
        }
        return $response;
    }
    // Update Gift Qty Details
    public function updateGiftQty(Request $request)
    {
        $Gifts = new Gift();
        $Gifts->gift_stock_id       = $request->input('gift_stock_id');
        $Gifts->gift_id             = $request->input('gift_id');
        $Gifts->gift_qty            = $request->input('gift_qty');
        $Gifts->region_id           = $request->input('region_id');
        $Gifts->branch_id           = $request->input('branch_id');
        $Gifts->warehouse_id        = $request->input('warehouse_id');

        $result = $Gifts->updateGiftQty($Gifts);
        if($result > 0){
            $response = response()->json(['status' => 'success', 'message' => 'Gift Qty Update successfully','data'=> $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Error in Update Gift Qty']);
        }
        return $response;
    }
    // Fetch Warehouse Based gift Details
    public function getWarehouseBasedGiftStock(Request $request)
    {
        $Gifts = new Gift();
        $Gifts->warehouse_id    = $request->input('warehouse_id');
        $Gifts->region_id       = $request->input('region_id');

        $data = $Gifts->getWarehouseBasedGiftStock($Gifts);
        if(count($data) > 0 ){
            $response = response()->json(['status' => 'success' , 'message' => 'Warehouse GiftStock Data retrived successfully','data'=>$data]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Error in Get details..','data'=>'null']);
        }
        return $response;
    } 
  
    //Stockist SIde
    
////////////////////////////////////
//Gift Assign Management Api Calls//
///////////////////////////////////
    //Admin Or Sub Admin Gift Assign into the field_officer
    public function giftAssign(Request $request){
        
        $Gifts = new Gift();
        $Gifts->gift_id = $request->input('gift_id');
        $Gifts->gift_name = $request->input('gift_name');
        $Gifts->gift_totqty = $request->input('gift_totqty');
        //$Gifts->field_officer_id = $request->input('field_officer_id');
        $Gifts->fieldofficers = $request->input('fieldofficers');
        $Gifts->area_manager_id = $request->input('area_manager_id');
        $Gifts->warehouse_id = $request->input('warehouse_id');
        $Gifts->region_manager_id = $request->input('region_manager_id');
        //$Gifts->stockist_id = $request->input('stockist_id');
        $Gifts->region_id = $request->input('region_id');

        $result = $Gifts->giftAssign($Gifts);
        if($result == 1){
            $response = response()->json(['status' => 'success' , 'message' => 'Gift can be Assign successfully']);
        } else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Gift Assign failed']);
        }
        return $response;
    }
    //Fetch Completed Assign Pending Gift for Stockist
    public function getReceivedGiftDet(Request $request){

        $Gifts = new Gift();
        $Gifts->warehouse_id = $request->input('warehouse_id');
        $Gifts->type = $request->input('type');

        $result = $Gifts->getReceivedGiftDet($Gifts);
        if(count($result) > 0){
            $response = response()->json(['status' => 'success' , 'message' => 'Fieldofficer received Gift successfully', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'No Gift Assign details']); 
        }
        return $response;
    }
    // Get My Assign Gift Details
    public function getMyAssignGifts(Request $request)
    {
        $Gifts = new Gift();
        $Gifts->field_officer_id = $request->input('user_id');

        $result = $Gifts->getMyAssignGifts($Gifts);
        if(count($result) > 0){
            $response = response()->json(['status' => 'success' , 'message' => '', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Invalid Gift details']); 
        }
        return $response;
    } 
    //Area Manager Assign Gifts List Details Fetch
    public function getManagerAssignGifts(Request $request)
    {
        $Gifts = new Gift();
        $Gifts->area_manager_id = $request->input('area_manager_id');

        $result = $Gifts->getManagerAssignGifts($Gifts);
        if(count($result) > 0){
            $response = response()->json(['status' => 'success' , 'message' => '', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Invalid Gift details']); 
        }
        return $response;
    } 
    //Region Manager Assign Gifts List Details Fetch
    public function getRegionManageAssignGifts(Request $request)
    {
        $Gifts = new Gift();
        $Gifts->user_id = $request->input('user_id');

        $result = $Gifts->getRegionManageAssignGifts($Gifts);
        if(count($result) > 0){
            $response = response()->json(['status' => 'success' , 'message' => '', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Invalid Gift details']); 
        }
        return $response;
    } 

    //Give Gift to Field Officer
    public function giveGifttoFO(Request $request){

        $Gifts = new Gift();
        $Gifts->giftassign_id = $request->input('giftassign_id');
        $Gifts->field_officer_id = $request->input('field_officer_id');
        $Gifts->giftassign_status = $request->input('giftassign_status');

        $result = $Gifts->giveGifttoFO($Gifts);
        if($result == 1){
            $response = response()->json(['status' => 'success' , 'message' => 'Fieldofficer received Gift successfully', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Error Gift details']); 
        }
        return $response;
    }
    // Get My Assign Gift Details
    public function getFOGift(Request $request)
    {
        $Gifts = new Gift();
        $Gifts->field_officer_id = $request->input('user_id');

        $result = $Gifts->getFOGift($Gifts);
        if(count($result) > 0){
            $response = response()->json(['status' => 'success' , 'message' => '', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Invalid Gift details']); 
        }
        return $response;
    } 
    
    
    
    
    // Get Admin Assign Gift Details
    public function getAllAssignGifts()
    {
        $Gifts = new Gift();
        $result = $Gifts->getAllAssignGifts();

        if(count($result) > 0){
            $response = response()->json(['status' => 'success' , 'message' => '', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Invalid Gift details']); 
        }
        return $response;
    } 
    //Field Officer Give the Gift to the Customer
    public function giftToCustomer(Request $request){
        
        $Gifts = new Gift();
        $Gifts->gift_id             = $request->input('gift_id');
        $Gifts->gift_name           = $request->input('gift_name');
        $Gifts->gift_qty            = $request->input('gift_qty');
        $Gifts->field_officer_name  = $request->input('field_officer_name');
        $Gifts->customer_id         = $request->input('customer_id');
        $Gifts->field_officer_id    = $request->input('field_officer_id');
        $Gifts->task_id             = $request->input('task_id');
        $Gifts->gift_cmt            = $request->input('gift_cmt');

        $result = $Gifts->giftToCustomer($Gifts);
        if($result == 1){
            $response = response()->json(['status' => 'success' , 'message' => 'success']);
        }else if($result == 2){
            $response = response()->json(['status' => 'failure' , 'message' => 'Your Gift Blc Is Zero']);
        } else{
            $response = response()->json(['status' => 'failure' , 'message' => 'failed']);
        }
        return $response;
    }
}
