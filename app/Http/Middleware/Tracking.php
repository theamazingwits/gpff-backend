<?php

namespace App\Http\Controllers;

use Closure;
use GeoFence;
use DB;
use Mail;
use Carbon\Carbon;

class Tracking
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        return $next($request);
    }


////////////////////////////
//Tracking Managment Api Calls//
////////////////////////////
    //Live Tracking
    public function taskLiveTracking($Tracking) 
    {   

        $task = DB::table('gpff_task')
                ->where('task_id', $Tracking->task_id)
                ->First();

        $customer_det = DB::table('gpff_customer')
                ->where('customer_id', $task->customer_id)
                ->First();

        $distance = GeoFence::CalculateDistance($Tracking->livelat, $Tracking->livelang, $customer_det->customer_lat, $customer_det->customer_lan);
    
        if((float)$distance <= 0.2){

        $values = array(
            'task_status'           => 1, 
            'task_start_date_time'  => date('Y-m-d H:i:s'),
            'updated_at'            => date('Y-m-d H:i:s')
        );
    
        return  DB::table('gpff_task')
                ->where('task_id', $Tracking->task_id)
                ->update($values);

        }else{

            return 2;
        }    
    }
}
