<?php

namespace App\Http\Controllers;

use Closure;
use DB;
use Mail;
use Carbon\Carbon;

require(base_path().'/app/Http/Middleware/Common.php');

class Samples
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        return $next($request);
    }
//////////////////////////////////////////
//Samples Inventory Management Api Calls//
//////////////////////////////////////////
    //Admin Or Sub Admin Add the Samples 
    public function addSamples($Sample) 
    {  
        $exit = DB::table('gpff_samples')
                ->where('product_id', $Sample->product_id)
                ->where('category_id', $Sample->category_id)
                ->where('warehouse_id', $Sample->warehouse_id)
                ->get();

        if(count($exit) > 0){
            return 2;
        } else{
            $values = array(
                'samples_name'          => $Sample->samples_name , 
                'samples_blc_qty'       => $Sample->samples_qty ,
                'samples_tot_qty'       => $Sample->samples_qty ,
                'samples_description'   => $Sample->samples_description ,
                'samples_cr_id'         => $Sample->samples_cr_id , 
                'product_id'            => $Sample->product_id , 
                'category_id'           => $Sample->category_id ,
                'category_name'         => $Sample->category_name , 
                'product_type'            => $Sample->product_type ,
                'warehouse_id'          => $Sample->warehouse_id , 
                'region_id'             => $Sample->region_id , 
                'branch_id'             => $Sample->branch_id ,
                'stockist_id'           => $Sample->stockist_id ,
                'samples_status'        => 1,
                'created_at'            => date('Y-m-d H:i:s') , 
                'updated_at'            => date('Y-m-d H:i:s')
            );

            $samplesid   =   DB::table('gpff_samples')
                            ->insertGetId($values);

            //History Maintaintance(type 3 = samples)
            $values1 = array(
                'id'            => $samplesid ,
                'orginal_id'    => $Sample->product_id ,
                //'product_type'  => $Sample->product_type ,
                'qty'           => $Sample->samples_qty ,
                'warehouse_id'  => $Sample->warehouse_id ,
                'date'          => date('Y-m-d H:i:s') , 
                'region_id'     => $Sample->region_id,
                'branch_id'     => $Sample->branch_id,
                'type'          => 3 ,
                'created_at'    => date('Y-m-d H:i:s') , 
                'updated_at'    => date('Y-m-d H:i:s')
            );
            DB::table('gpff_stock_add_history')
            ->insert($values1);
            //End History Maintaintance(type 3 = samples)
            return 1;
        }        
    }
    //Update Samples details
    public function updateSamplesQty($Sample) 
    {   
        //History Maintaintance(type 3 = samples)
        $values1 = array(
            'id'            => $Sample->samples_id ,
            'orginal_id'    => $Sample->product_id ,
            //'product_type'  => $Sample->product_type ,
            'warehouse_id'  => $Sample->warehouse_id,
            'region_id'     => $Sample->region_id,
            'branch_id'     => $Sample->branch_id,
            'qty'           => $Sample->samples_qty ,
            'date'          => date('Y-m-d H:i:s') , 
            'type'          => 3 ,
            'created_at'    => date('Y-m-d H:i:s') , 
            'updated_at'    => date('Y-m-d H:i:s')
        );
        DB::table('gpff_stock_add_history')
        ->insert($values1);
        //End History Maintaintance(type 3 = samples)
        
        $tot_qty =  DB::table('gpff_samples')
                    ->where('samples_id', $Sample->samples_id)
                    ->where('product_id', $Sample->product_id)
                    ->where('region_id', $Sample->region_id)
                    //->where('warehouse_id', $Sample->warehouse_id)
                    ->sum('samples_tot_qty');

        $blc_qty =  DB::table('gpff_samples')
                    ->where('samples_id', $Sample->samples_id)
                    ->where('product_id', $Sample->product_id)
                    ->where('region_id', $Sample->region_id)
                    //->where('warehouse_id', $Sample->warehouse_id)
                    ->sum('samples_blc_qty');

        $qty = $Sample->samples_qty;

        $tot = $tot_qty + $qty;
        $blc = $blc_qty + $qty;

        $values = array(
                'samples_tot_qty'     => $tot ,
                'samples_blc_qty'     => $blc ,
                'updated_at'          => date('Y-m-d H:i:s')
            );
        return  DB::table('gpff_samples')
                ->where('samples_id', $Sample->samples_id)
                ->where('product_id', $Sample->product_id)
                ->where('region_id', $Sample->region_id)
                //->where('warehouse_id', $Sample->warehouse_id)
                ->update($values);
    }
     //get Warehouse Based Sample Stock
    public function getWarehouseBasedSampleStock($Sample) 
    {
        return  DB::table('gpff_samples')
                ->where('region_id', $Sample->region_id)
                ->where('warehouse_id', $Sample->warehouse_id)
                ->orderBy('updated_at','DESC')
                ->get();
    }
    
//End

    //Update Samples details
    public function updateSamples($Sample) 
    {
        $values = array(
                'samples_name'          => $Sample->samples_name , 
                'samples_description'   => $Sample->samples_description ,
                'samples_cr_id'         => $Sample->samples_cr_id , 
                'product_id'            => $Sample->product_id , 
                'category_id'           => $Sample->category_id , 
                'warehouse_id'          => $Sample->warehouse_id , 
                'region_id'             => $Sample->region_id , 
                'updated_at'            => date('Y-m-d H:i:s')
            );
        return  DB::table('gpff_samples')
                ->where('samples_id', $Sample->samples_id)
                ->update($values);
    }
    //Delete Samples details
    public function removeSamples($Sample)
    {
        return  DB::table('gpff_samples')
                ->where('samples_id', $Sample->samples_id)
                ->delete();
    }
    //Get Indivitual Samples Details
    public function getIndSamples($Sample)
    {   
        return  DB::table('gpff_samples')
                ->where('samples_id', $Sample->samples_id)
                ->get();
    }
    //Get All Samples Details
    public function getAllSamples() 
    {
        $data =[];
        $data['totaldata']= DB::table('gpff_samples as gsam')
                            ->leftjoin('gpff_product as gppr','gsam.product_id','gppr.product_id')
                            ->leftjoin('gpff_category as gpca','gsam.category_id','gpca.category_id')
                            ->orderBy('gsam.updated_at','DESC')
                            ->get();
         return $data;   
    }
    //Get Region Based Samples Details
    public function getRegionBasedSamples($Sample) 
    {
        $data =[];
        $data['totaldata']= DB::table('gpff_samples as gsam')
                            ->leftjoin('gpff_product as gppr','gsam.product_id','gppr.product_id')
                            ->leftjoin('gpff_category as gpca','gsam.category_id','gpca.category_id')
                            ->where('gsam.region_id', $Sample->region_id)
                            ->orderBy('gsam.updated_at','DESC')
                            ->get();
         return $data;   
    }
///////////////////////////////////////
//Samples Assign Management Api Calls//
///////////////////////////////////////
    //Admin Or Sub Admin Add the Samples 
    public function samplesAssign($Sample) 
    {  
        if($Sample->area_manager_id){
            $manager  =  DB::table('gpff_users')
                    ->where('user_id', $Sample->area_manager_id)
                    ->First(['firstname','lastname']);
            $managername =  $manager->firstname.' '.$manager->lastname;
        } else{
            $managername ="";
        }
        
        if($Sample->region_manager_id){
            $manager  =  DB::table('gpff_users')
                    ->where('user_id', $Sample->region_manager_id)
                    ->First(['firstname','lastname']);
            $regmanagername =  $manager->firstname.' '.$manager->lastname;
        } else{
            $regmanagername ="";
        }

        foreach ($Sample->products as $product) 
        {
            $field_name  =  DB::table('gpff_users')
            ->where('user_id', $Sample->fieldofficers)
            ->First(['firstname','lastname']);
            $fieldname =  $field_name->firstname.' '.$field_name->lastname;
            $values = array(
                'category_id'        => $product['category_id'],
                'category_name'=> $product['category_name'],
                'product_id'        => $product['product_id'],
                'product_genericname'=> $product['product_name'],
                'requested_qty'     => $product['qty'],
                'field_officer_id'  => $Sample->fieldofficers,
                'field_officer_name'=> $fieldname,
                'area_manager_id'   => $Sample->area_manager_id,
                'region_manager_id' => $Sample->region_manager_id,
                'warehouse_id'      => $Sample->warehouse_id,
                'region_manager_name'=> $regmanagername,
                //'stockist_id'       => $Sample->stockist_id,
                'date'              => date('Y-m-d H:i:s') , 
                'region_id'         => $Sample->region_id,
                'manager_name'      => $managername,
                'created_at'        => date('Y-m-d H:i:s') , 
                'updated_at'        => date('Y-m-d H:i:s')
            );
            
            DB::table('gpff_samplesassign')
            ->insert($values);

            //Notification Entry
            if($Sample->area_manager_id){
                $name = DB::table('gpff_users')
                    ->where('user_id', $Sample->area_manager_id)
                    ->First();
                $message = "Area Manager ".$name->firstname." has assign Sample Product to you.";
                $page_id = 'UNKNOWN';

                $cmn = new Common();
                $cmn->insertNotification($Sample->area_manager_id,$name->firstname,$Sample->fieldofficers,$message,$page_id);
                //Stockist
                $waremessage = "Area Manager ".$name->firstname." has assign Sample in your Warehouse.";
                $page_id = 'UNKNOWN';

                $warename = DB::table('gpff_users')
                    ->where('warehouse_id', $Sample->warehouse_id)
                    ->First();

                $cmn = new Common();
                $cmn->insertNotification($Sample->area_manager_id,$name->firstname,$warename->user_id,$waremessage,$page_id);

            } else if ($Sample->region_manager_id) {
                $name = DB::table('gpff_users')
                    ->where('user_id', $Sample->region_manager_id)
                    ->First();
                $message = "Region Manager ".$name->firstname." has assign Sample Product to you.";
                $page_id = 'UNKNOWN';

                $cmn = new Common();
                $cmn->insertNotification($Sample->region_manager_id,$name->firstname,$Sample->fieldofficers,$message,$page_id);
                //Stockist
                $waremessage = "Region Manager ".$name->firstname." has assign Sample in your Warehouse.";
                $page_id = 'UNKNOWN';

                $warename = DB::table('gpff_users')
                    ->where('warehouse_id', $Sample->warehouse_id)
                    ->First();

                $cmn = new Common();
                $cmn->insertNotification($Sample->region_manager_id,$name->firstname,$warename->user_id,$waremessage,$page_id);

            }
        }

        //End Notification Entry
        return 1;
    }

    //Get Completed Assign Pending samples for Stockist
    public function getReceivedSampleDet($Sample){

        if($Sample->type == 0){ //Waiting 

              return  DB::table('gpff_samplesassign')
                ->where('warehouse_id', $Sample->warehouse_id)
                ->where('samplesassign_status', 0)
                ->orderBy('updated_at','DESC')
                ->get();

        } else if($Sample->type == 1){ //Received

              return  DB::table('gpff_samplesassign as gpsa')
                ->join('gppf_fo_received_sample as gprs','gprs.samplesassign_id','gpsa.samplesassign_id')
                ->where('gpsa.warehouse_id', $Sample->warehouse_id)
                ->where('samplesassign_status', 1)
                ->orderBy('gpsa.updated_at','DESC')
                ->get();
        } 
    }
    // Get My Assign Sample Details
    public function getMyAssignSamples($Sample)
    {   
        return  DB::table('gpff_samplesassign')
                ->where('field_officer_id', $Sample->field_officer_id)
                ->where('samplesassign_status', 0)
                ->orderBy('updated_at','DESC')
                ->get();
    }
    //Area Manager Assign Samples List Details Fetch
    public function getManagerAssignSamples($Sample)
    {   
        return  DB::table('gpff_samplesassign as gsam')
                ->leftjoin('gpff_warehouse as gpwa','gsam.warehouse_id','gpwa.warehouse_id')
                ->leftjoin('gpff_region as gpre','gsam.region_id','gpre.region_id')
                ->where('gsam.area_manager_id', $Sample->area_manager_id)
                ->orderBy('gsam.updated_at','DESC')
                ->get(['gsam.category_name','gsam.category_id','gsam.product_id', 'gsam.created_at', 'gsam.product_genericname','gsam.requested_qty as qty','gsam.field_officer_name','gpre.region_name','gpwa.warehouse_name','gsam.samplesassign_id']);
    }
    //Region Manager Assign Samples List Details Fetch
    public function getRegionManageAssignSamples($Sample)
    {   
        $data = DB::table('gpff_users')
                    ->where('user_id', $Sample->user_id)
                    ->First(['region_id']);

        $myArray = explode(',', $data->region_id);

        return  DB::table('gpff_samplesassign as gpsa')
                ->leftjoin('gpff_warehouse as gpwa','gpwa.warehouse_id','gpsa.warehouse_id')
                ->leftjoin('gpff_region as gpre','gpsa.region_id','gpre.region_id')
                ->whereIn('gpsa.region_id', $myArray)
                ->orderBy('gpsa.updated_at','DESC')
                ->get(['gpsa.samplesassign_id', 'gpsa.created_at','gpsa.qty','gpsa.field_officer_name','gpre.region_name','gpwa.warehouse_name']);
                // ->get(['gpsa.samplesassign_id', 'gpsa.created_at', 'gpsa.samples_name','gpsa.qty','gpsa.field_officer_name','gpre.region_name','gpwa.warehouse_name']);
    }


    //Give Sample to Fieldofficer
    public function giveSampletoFO($Sample){
        //Get Sample Details
        $samplesassign  =  DB::table('gpff_samplesassign')
                    ->where('samplesassign_id', $Sample->samplesassign_id)
                    ->First();
        $req_qty = (int)$samplesassign->requested_qty;
        $total_issued = (int)$samplesassign->qty + (int)$Sample->issue_qty;
        if($req_qty < $total_issued){
            return 0;
        }        
        foreach($Sample->products as $product){
                $values1 = array(
                'samplesassign_id'  => $Sample->samplesassign_id,
                'product_id'        => $product['product_id'],
                'category_id'        => $product['category_id'],
                'batch_id'        => $product['batch_id'],
                'exp_date'        => $product['exp_date'],
                'net_price'        => $product['net_price'],
                'gross_price'        => $product['gross_price'],
                'field_officer_id'  => $samplesassign->field_officer_id,
                'tot_qty'           => $product['qty'],
                'blc_qty'           => $product['qty'],
                'date'              => date('Y-m-d H:i:s'), 
                'status'            => 1,
                'warehouse_id'      => $samplesassign->warehouse_id,
                'created_at'        => date('Y-m-d H:i:s'), 
                'updated_at'        => date('Y-m-d H:i:s')
            );    
            DB::table('gppf_fo_received_sample')
            ->insert($values1);
            $product_stock = DB::table('gpff_product_batch_stock')
                            ->where('product_id',$product['product_id'])
                            ->where('batch_id',$product['batch_id'])
                            ->where('warehouse_id',$samplesassign->warehouse_id)
                            ->First();
            $balance_qty = (int)$product_stock->product_blc_qty - (int)$product['qty'];
            $sample_qty = (int)$product_stock->product_sample_qty + (int)$product['qty'];

            if($balance_qty == 0){
                 $value = array(
                    'product_blc_qty'      => $balance_qty ,
                    'product_sample_qty'    => $sample_qty ,
                    'product_status' => 0,
                    'updated_at'        => date('Y-m-d H:i:s')
                );
            }else{
                $value = array(
                    'product_blc_qty'      => $balance_qty ,
                    'product_sample_qty'    => $sample_qty ,                    
                    'updated_at'        => date('Y-m-d H:i:s')
                );
            }

            DB::table('gpff_product_batch_stock')
            ->where('product_id',$product['product_id'])
            ->where('batch_id',$product['batch_id'])
            ->where('warehouse_id',$samplesassign->warehouse_id)
            ->update($value);


        }
        if($req_qty == $total_issued){
            $val = array(
                'qty' => $total_issued,
                'samplesassign_status'=> 1 ,
                'updated_at'    => date('Y-m-d H:i:s')
            );
        }else{
            $val = array(
                'qty' => $total_issued,
                'updated_at'    => date('Y-m-d H:i:s')
            );
        }
        //Change Sampleassign Status
        

        DB::table('gpff_samplesassign')
        ->where('samplesassign_id',$Sample->samplesassign_id)
        ->where('field_officer_id',$samplesassign->field_officer_id)
        ->update($val);

        return 1;

    }
    // Get My Assign Sample Details
    public function getFOSamples($Sample)
    {   
        return  DB::table('gppf_fo_received_sample as gfrs')
                ->leftjoin('gpff_samplesassign as gpsa','gfrs.samplesassign_id','gpsa.samplesassign_id')
                ->where('gfrs.field_officer_id', $Sample->field_officer_id)
                ->orderBy('gfrs.updated_at','DESC')
                ->get();
    }
    
    
    // Get Admin Assign Sample Details
    public function getAllAssignSamples()
    {   
        return  DB::table('gpff_samplesassign as gsam')
                ->leftjoin('gpff_warehouse as gpwa','gsam.warehouse_id','gpwa.warehouse_id')
                ->leftjoin('gpff_region as gpre','gsam.region_id','gpre.region_id')
                ->where('gsam.samplesassign_status', 1)
                ->orderBy('gsam.updated_at','DESC')
                ->get();
    }
    //Field Officer Give the Sample to the Customer
    public function sampleToCustomer($Sample) 
    {   
        //Get Sample Details
        $blc_qty  =  DB::table('gppf_fo_received_sample')
                     ->where('field_officer_id', $Sample->field_officer_id)
                     ->where('fo_received_id', $Sample->fo_received_id) 
                     ->sum('blc_qty');

        if($blc_qty > 0){
            $values = array(
                'fo_received_id'            => $Sample->fo_received_id , 
                'samples_name'          => $Sample->samples_name ,
                'product_id'            => $Sample->product_id ,
                'batch_id'              => $Sample->batch_id ,
                'samples_qty'           => $Sample->samples_qty ,
                'field_officer_id'      => $Sample->field_officer_id ,
                'field_officer_name'    => $Sample->field_officer_name , 
                'customer_id'           => $Sample->customer_id ,
                'task_id'               => $Sample->task_id ,
                'samples_cmt'           => $Sample->samples_cmt , 
                'created_at'            => date('Y-m-d H:i:s') , 
                'updated_at'            => date('Y-m-d H:i:s')
            );
            DB::table('gpff_delivered_samples')
            ->insert($values);

            //Reduce Sample in FO Total Quantity
            $sal_qty  =  DB::table('gppf_fo_received_sample')
                        ->where('field_officer_id', $Sample->field_officer_id)
                        ->where('fo_received_id', $Sample->fo_received_id) 
                        ->sum('sales_qty');

            $qty = $Sample->samples_qty;

            $sales  = $sal_qty + $qty;
            $blc    = $blc_qty - $qty;

            $assgin_value = array(
                        'blc_qty'       => $blc ,
                        'sales_qty'     => $sales ,
                        'updated_at'    => date('Y-m-d H:i:s')
                    );

            DB::table('gppf_fo_received_sample')
            ->where('field_officer_id', $Sample->field_officer_id)
            ->where('fo_received_id', $Sample->fo_received_id) 
            ->update($assgin_value);
            //End Reduce Sample in Field Officer SIde

            //Notification Entry
            /*$name = DB::table('gpff_users')
                    ->where('user_id', $Sample->field_officer_id)
                    ->First();
                $message = "Field Officer ".$name->firstname." has Sample Product give to the customer.";
                $page_id = 'UNKNOWN';

                $cmn = new Common();
                $cmn->insertNotification($Sample->field_officer_id,$name->firstname,$name->area_manager_id,$message,$page_id);*/
            //End Notification Entry
            return 1;
        }
    }

}
