<?php 

namespace App\Http\Controllers;

use PDF;
use Carbon\Carbon;
use Closure;
use DB;
use Mail;
use AWS;
//require(base_path().'/app/Http/Middleware/Common.php');

use Illuminate\Support\Facades\Storage;

class Purchase{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next){
    	return $next($request);
    }

	public function addPurchase($Purchases){
		$values = array(
			'warehouse_id'			=> $Purchases->warehouse_id,
			'supplier_id'			=> $Purchases->supplier_id,
			'supplier_name'			=> $Purchases->supplier_name,
			'supplier_code'			=> $Purchases->supplier_code,
			'date'			=> $Purchases->date,
			'amount'				=> $Purchases->amount,
			'due'					=> $Purchases->due,
			'payment_type'			=> $Purchases->payment_type,
			'payment_status'		=> $Purchases->payment_status,
			'user_id'				=> $Purchases->user_id,
			'created_at'			=> date('Y-m-d H:i:s'),
			'updated_at'			=> date('Y-m-d H:i:s')
		);
		$purchase_id = DB::table('gpff_purchase')
						->insertGetId($values);	

		foreach ($Purchases->purchase_product_list as $purchase_product_lists) {
			$list_values = array(

				'purchase_id' 			=> $purchase_id,
				'product_genericname' 	=> $purchase_product_lists['product_genericname'],
				'product_id'			=> $purchase_product_lists['product_id'],
		
				'quantity'				=> $purchase_product_lists['quantity'],
				'unit_price'			=> $purchase_product_lists['unit_price'],
				'total_price'			=> $purchase_product_lists['total_price'],
				'created_at'			=> date('Y-m-d H:i:s'),
				'updated_at' 			=> date('Y-m-d H:i:s')
			);
			DB::table('gpff_purchase_product_list')
			->insert($list_values);
		}

		//  PDF GENERATION FOR PRODUCTS PURCHASED
		$purchased_product_details = DB::table('gpff_purchase')
			->where('purchase_id',$purchase_id)
			->first();

		$purchased_product_details_Array = DB::table('gpff_purchase_product_list')
			->where('purchase_id',$purchase_id)
			->get();

		$supplier_details = DB::table('gpff_supplier')
			->where('supplier_id',$Purchases->supplier_id)
			->first();
	
		view()->share('purchase_id',$purchased_product_details->purchase_id);
		view()->share('purchased_date',$purchased_product_details->created_at);
		// supplier details
		view()->share('supplier_code',$purchased_product_details->supplier_code);
		view()->share('supplier_name',$supplier_details->supplier_name);
		view()->share('contact_person',$supplier_details->supplier_contact_person);
		view()->share('contact_number',$supplier_details->supplier_contact_number);
		view()->share('email',$supplier_details->supplier_email);
		view()->share('supplier_licence',$supplier_details->supplier_licence);
		//End supplier details
		view()->share('product_details_array',$purchased_product_details_Array);
		view()->share('Grand_total',$purchased_product_details->amount);
		view()->share('date',Carbon::now());

		$pdf_name = $purchased_product_details->supplier_code;
		$customPaper = array(0,0,767.00,883.80);

		$pdf = PDF::loadView('purchasedproducts')
		->setPaper('a4', 'landscape');
		$pdf->save(public_path('/Invoice/'.$pdf_name.'.pdf'));

		$file_name = $pdf_name.'.pdf';
        $name = $file_name;

        $filePath = 'Invoice/'.$name; 

        //Storage::disk('s3')->put($filePath, file_get_contents(public_path('Invoice')."/".$file_name));
		//unlink(public_path('Invoice')."/".$pdf_name.'.pdf');

		return 1;
	}

	
	public function getAllPurchase(){
		return DB::table('gpff_purchase')
			->orderBy('gpff_purchase.updated_at', 'DESC')
			// return DB::table('gpff_purchase as gp')
			// 	->join('gpff_purchase_product_list as gppl', 'gp.purchase_id', 'gppl.purchase_id')
			// 	->orderBy('gp.updated_at', 'DESC')
			->get();
	}

	public function getIndividualPurchase($Purchases){
		return DB::table('gpff_purchase as gp')
			->join('gpff_purchase_product_list as gppl', 'gp.purchase_id', 'gppl.purchase_id')
			->join('gpff_supplier as gpffsupp', 'gp.supplier_id', 'gpffsupp.supplier_id')
			->where('gp.purchase_id', $Purchases->purchase_id)
			->orderBy('gp.updated_at', 'DESC')
			->get();

	}





}

?>