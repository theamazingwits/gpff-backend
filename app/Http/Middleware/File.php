<?php

namespace App\Http\Controllers;

use Closure;
use DB;
use Mail;
use Carbon\Carbon;

require(base_path().'/app/Http/Middleware/Common.php');

class File
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        return $next($request);
    }
///////////////////////////////////////
//Gift Inventory Management Api Calls//
///////////////////////////////////////
    //Add Folder
    public function storeFolder($Files) 
    {  
        $values = array(
            'user_id'       => $Files->user_id , 
            'folder_name'   => $Files->folder_name ,
            'parent_folder' => $Files->parent_folder ,
            'folder_desc'   => $Files->folder_desc ,
            'region_id'     => $Files->region_id ,
            'branch_id'     => $Files->branch_id ,
            'created_at'    => date('Y-m-d H:i:s') , 
            'updated_at'    => date('Y-m-d H:i:s')
        );

        DB::table('gpff_folders')
        ->insert($values);

        return 1;        
    }
    //Update Folder details
    public function updateFolder($Files) 
    {
        $values = array(
                'user_id'       => $Files->user_id , 
                'folder_name'   => $Files->folder_name ,
                'parent_folder' => $Files->parent_folder ,
                'folder_desc'   => $Files->folder_desc ,
                'updated_at'        => date('Y-m-d H:i:s')
            );
        return  DB::table('gpff_folders')
                ->where('folder_id', $Files->folder_id)
                ->update($values);
    }

    //Delete Folder details
    public function deleteFolder($Files)
    {
        DB::table('gpff_folders')
        ->where('folder_id', $Files->folder_id)
        ->where('region_id', $Files->region_id)
        ->delete();
        return 1;
    }


    //Add File
    public function addFile($Files) 
    {  
        $values = array(
            'user_id'       => $Files->user_id , 
            'folder_id'     => $Files->folder_id ,
            'file_name'     => $Files->file_name ,
            'file_extension'=> $Files->file_extension ,
            'region_id'     => $Files->region_id ,
            'branch_id'     => $Files->branch_id ,
            'created_at'    => date('Y-m-d H:i:s') , 
            'updated_at'    => date('Y-m-d H:i:s')
        );

        DB::table('gpff_files')
        ->insert($values);

        return 1;        
    }
    //Update File 
    public function updateFile($Files) 
    {
        $values = array(
                'user_id'       => $Files->user_id , 
                'folder_id'     => $Files->folder_id ,
                'file_name'     => $Files->file_name ,
                'file_extension'=> $Files->file_extension ,
                'updated_at'    => date('Y-m-d H:i:s')
            );
        return  DB::table('gpff_files')
                ->where('file_id', $Files->file_id)
                ->update($values);
    }
    //Delete File details
    public function deleteFile($Files)
    {
        DB::table('gpff_files')
        ->where('file_id', $Files->file_id)
        ->where('region_id', $Files->region_id)
        ->delete();
        return 1;
    }
    


    // Get Folder And Files Details
    public function getFolderFiles($Files)
        {   

            if($Files->type == 1){

                $folder =   DB::table('gpff_folders as gpfo')
                            ->join('gpff_branch as gpbr','gpfo.branch_id', '=' , 'gpbr.branch_id')
                            ->join('gpff_region as gpre','gpfo.region_id', '=' , 'gpre.region_id')
                            ->where('gpfo.parent_folder', 'NULL')
                            ->where('gpfo.region_id', $Files->region_id)
                           ->get(['gpfo.folder_id','gpfo.user_id','gpfo.folder_name','gpfo.parent_folder','gpfo.folder_desc','gpfo.region_id','gpfo.branch_id','gpfo.created_at','gpbr.branch_name','gpre.region_name']);

                $file   =   DB::table('gpff_files as gpfi')
                            ->join('gpff_branch as gpbr','gpfi.branch_id', '=' , 'gpbr.branch_id')
                            ->join('gpff_region as gpre','gpfi.region_id', '=' , 'gpre.region_id')
                            ->where('gpfi.folder_id', 'NULL')
                            ->where('gpfi.region_id', $Files->region_id)
                            ->get(['gpfi.file_id','gpfi.user_id','gpfi.folder_id','gpfi.file_name','gpfi.file_extension','gpfi.created_at','gpbr.updated_at','gpbr.branch_name','gpre.region_name']);

            } elseif ($Files->type == 2) {

                $folder =   DB::table('gpff_folders as gpfo')
                            ->join('gpff_branch as gpbr','gpfo.branch_id', '=' , 'gpbr.branch_id')
                            ->join('gpff_region as gpre','gpfo.region_id', '=' , 'gpre.region_id')
                            ->where('gpfo.parent_folder', $Files->folder_id)
                            ->where('gpfo.region_id', $Files->region_id)
                            ->get(['gpfo.folder_id','gpfo.user_id','gpfo.folder_name','gpfo.parent_folder','gpfo.folder_desc','gpfo.region_id','gpfo.branch_id','gpfo.created_at','gpre.region_name']);

                $file   =   DB::table('gpff_files as gpfi')
                            ->join('gpff_branch as gpbr','gpfi.branch_id', '=' , 'gpbr.branch_id')
                            ->join('gpff_region as gpre','gpfi.region_id', '=' , 'gpre.region_id')
                            ->where('gpfi.folder_id', $Files->folder_id)
                            ->where('gpfi.region_id', $Files->region_id)
                            ->get(['gpfi.file_id','gpfi.user_id','gpfi.folder_id','gpfi.file_name','gpfi.file_extension','gpfi.created_at','gpbr.updated_at','gpbr.branch_name','gpre.region_name']);
     }

            $result = array(
                    'folders'   =>  $folder,
                    'files'     =>  $file,
            );

            return $result;
}


    // Get Folder And Files Details
    public function getAllFiles($Files)
    {   
        return   DB::table('gpff_files as gpfi')
                ->join('gpff_branch as gpbr','gpfi.branch_id', '=' , 'gpbr.branch_id')
                ->join('gpff_region as gpre','gpfi.region_id', '=' , 'gpre.region_id')
                ->join('gpff_folders as gpfo','gpfi.folder_id', '=' , 'gpfo.folder_id')
                ->where('gpfi.region_id', $Files->region_id)
                ->get(['gpfi.file_id','gpfi.user_id','gpfi.folder_id','gpfi.file_name','gpfi.file_extension','gpfi.created_at','gpbr.updated_at','gpbr.branch_name','gpre.region_name']);
                   //->get();
    }
}
