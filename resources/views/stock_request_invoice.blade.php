<!DOCTYPE html>
<html lang="en">
<head>
	<title>Table V01</title>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css"/>

	<style>
		
			  
			  #logo {
				float: left;
				margin-top: 8px;
				margin-left: 30%;
			  }
			  
			  #logo img {
				height: 70px;
			  }

			  .display-1 {
				.font(@font-size-display-1, @line-height-display-1, @letter-spacing-display-1);
				font-weight: @font-weight-display-1;
				color: @font-color-display-1;
				text-transform: inherit;
				
				}
				
				h2 {
					text-align: center;
					padding-top: 20px 0;
				}
				
				.table-bordered {
					border: 1px solid #ddd !important;
				}
				
				table caption {
					padding: .5em 0;
				}
				
				table tfoot tr td {
					text-align: center !important;
				}
				
				@media (max-width: 39.9375em) {
					.tablesaw-stack tbody tr:not(:last-child) {
						border-bottom: 2px solid #0B0B0D;
					}
				}
				
				.p {
					text-align: center;
					padding-top: 140px;
					font-size: 14px;
				}
	</style>

</head>
<body>
	<header class="clearfix">
		{{--  <div id="logo">
			</div>  --}}
		<center><img src="https://aw-gpff.s3.ap-south-1.amazonaws.com/TARGETSFA1.png"  height="1" alt="TargetSFA" style="padding-bottom: 5%;width: 25%;height: auto;padding-left: 3%"/></center>
				<h1 class="display-1" style="padding-top: 1%; padding-left: 17%;">Order Approved Voucher</h1>

</header>
			
<div class="container">
	<div class="date" ><h4 align="left" >Warehouse From: {{ $war_details->warehouse_name }}</h4> <h5 align="right">Invoice no:{{ $invoice }}</h5><br><h4 align="left" >Warehouse To: {{ $war_details->sub_ware_name }}</h4> <h5 align="right">Invoice Date:{{ $date }}</h5></div>
	<h4 align="left"></h4 >
  <div class="row">
    <div class="col-xs-12">
      <table summary="This table shows how to create responsive tables using Tablesaw's functionality" class="table table-bordered table-hover tablesaw tablesaw-stack" data-tablesaw-mode="stack">
        <thead>
          <tr>
							<th>S.No</th>
							<th>Category Name</th> 
							<th>Product Name</th>
							<th>Batch.No</th>
							<th>Exp.Date</th>
							<th>Quantity</th>
							<th>netprice</th>				
							<th>grossprice</th>
							
          </tr>
        </thead>
				<tbody>
						@foreach($datas  as $key=> $data_details)
			<tr>
				<td> {{ ++$key }} </td>
                <td> {{  $data_details['category_name'] }} </td>
                <td> {{  $data_details['product_genericname'] }} </td>
                <td> {{  $data_details['batch_no'] }} </td>
                <td> {{  $data_details['exp_date'] }} </td>
                <td> {{  $data_details['product_qty'] }} </td>
                <td> {{  $data_details['product_netprice'] }} </td>
                <td> {{  $data_details['product_grossprice'] }} </td>
			</tr>
			$key++;
			@endforeach
				</tbody>
      </table>
    </div>
  </div>
</div>



</body>
</html>