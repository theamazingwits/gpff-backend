<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use File;
use Mail;

require(base_path().'/app/Http/Middleware/Customer.php');

class CustomerController extends Controller
{
/////////////////////////////
//Cart Management Api Calls//
/////////////////////////////
	//Add Product to the Cart
    public function addToCart(Request $request){
        
        $Customers = new Customer();
        $Customers->product_id    	= $request->input('product_id');
        $Customers->customer_id     = $request->input('customer_id');
        $Customers->customer_name     = $request->input('customer_name');
        $Customers->category_id     = $request->input('category_id');
        $Customers->category_name   = $request->input('category_name');
        $Customers->product_genericname = $request->input('product_genericname');
        $Customers->product_qty = $request->input('product_qty');
        $Customers->product_packingsize = $request->input('product_packingsize'); 
        $Customers->product_price   = $request->input('product_price'); 
        $Customers->product_netprice = $request->input('product_netprice');
        $Customers->product_grossprice = $request->input('product_grossprice');
        $Customers->product_discount = $request->input('product_discount');
        $Customers->product_tot_price = $request->input('product_tot_price');
        $Customers->product_type  = $request->input('product_type');
        $Customers->branch_id  = $request->input('branch_id');
        $Customers->region_id  = $request->input('region_id');
        $Customers->area_id  = $request->input('area_id');
        
        $result = $Customers->storeToCart($Customers);
        if($result == 1){
            $response = response()->json(['status' => 'success' , 'message' => 'Product add to Cart']);
        } elseif($result == 2){
             $response = response()->json(['status' => 'failure' , 'message' => 'Already product in your Cart']);
        }else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Product add to Cart failed']);
        }
        return $response;
    }

    // Fetch Cart Details 
    public function getCartDetails(Request $request)
    {
        $Customers = new Customer();
        $Customers->customer_id  = $request->input('customer_id');

        $result = $Customers->getCartDetails($Customers);
        if(count($result)){
            $response = response()->json(['status' => 'success', 'message' => 'Cart Details fetched successfully','data'=> $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'No Data found']);
        }
        return $response;
    }
    // Delete Cart details
    public function deleteCartDetails(Request $request){
        $Customers = new Customer();
        $Customers->gpff_cus_cart_id = $request->input('gpff_cus_cart_id');

        $result = $Customers->deleteCartDetails($Customers);
        if($result > 0){
            $response = response()->json(['status' => 'success' ,'message' => 'Cart details deleted']);
        } else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Error in deleting']); 
        }
        return $response;
    }

    // Fetch Customer Based Order History Details Fetch
    public function getCusOrderDetailsPagi(Request $request)
    {
        $Customers = new Customer();

        if($request->input('draw')){
            $Customers->draw = $request->input('draw');
        }else{
            $Customers->draw = 1; 
        }
        if($request->input('start')){
             $Customers->start = $request->input('start');
        }else{
            $Customers->start = 0; 
        }
        if($request->input('length')){
            $Customers->length = $request->input('length');
        }else{
            $Customers->length = 10; 
        }

        $Customers->customer_id  = $request->input('customer_id');

        $result = $Customers->getCusOrderDetailsPagi($Customers);

        if(count($result[2]) > 0){
            $response = response()->json(['status' => 'success' , 'message' => 'Customers Order details fetched successfully', 'data' => $result[2],
                'draw' => $Customers->draw,
                'recordsTotal' => $result[0],
                'recordsFiltered' => $result[1],
            ]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'No Data found',
                'data' => [],
                'draw' => $Customers->draw,
                'recordsTotal' => 0,
                'recordsFiltered' => 0]);
        }
        return $response;

    }

    // Fetch Customer Based Last 5 Order List
    public function getCusPrevOrderDetails(Request $request)
    {
        $Customers = new Customer();
        $Customers->customer_id  = $request->input('customer_id');

        $result = $Customers->getCusPrevOrderDetails($Customers);
        if(count($result)){
            $response = response()->json(['status' => 'success', 'message' => 'Customers Last Order Details fetched successfully','data'=> $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'No Data found']);
        }
        return $response;
    }

    //Add Complain
    public function addComplain(Request $request){

        $Customers = new Customer();
        $Customers->complain_title    = $request->input('complain_title');
        $Customers->complain_name     = $request->input('complain_name');
        $Customers->complain_text     = $request->input('complain_text');
        $Customers->complain_file     = $request->input('complain_file');
        $Customers->customer_id       = $request->input('customer_id');
        $Customers->customer_name     = $request->input('customer_name');
        $Customers->branch_id     = $request->input('branch_id');
        $Customers->region_id     = $request->input('region_id');
        $Customers->area_id     = $request->input('area_id');
        $result = $Customers->addComplain($Customers);
        if($result == 1){
            $response = response()->json(['status' => 'success' , 'message' => 'Complain added successfully']);
        } else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Complain add failed']);
        }
        return $response;
    }

    //Update Complain Details Status
    public function updateComplain(Request $request){

        $Customers = new Customer();
        $Customers->complain_id    = $request->input('complain_id');
        $Customers->res_msg     = $request->input('res_msg');
        $Customers->status     = $request->input('status');
        $Customers->approve_id     = $request->input('approve_id');
        $Customers->approve_name     = $request->input('approve_name');
        $result = $Customers->updateComplain($Customers);
        if($result == 1){
            $response = response()->json(['status' => 'success' , 'message' => 'Complain Status Update successfully']);
        } else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Complain Status Update failed']);
        }
        return $response;
    }

    // Fetch Complain Details 
    public function getCusBasedComplain(Request $request)
    {
        $Customers = new Customer();
        $Customers->customer_id  = $request->input('customer_id');
        $Customers->status  = $request->input('status');

        $result = $Customers->getCusBasedComplain($Customers);
        if(count($result)){
            $response = response()->json(['status' => 'success', 'message' => 'Complain Details fetched successfully','data'=> $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'No Data found']);
        }
        return $response;
    }

    // Fetch Complain Details For Area and Region Manager
    public function getUserBasedComplain(Request $request)
    {
        $Customers = new Customer();
        $Customers->user_id  = $request->input('user_id');
        $Customers->type  = $request->input('type');

        $result = $Customers->getUserBasedComplain($Customers);
        if($result){
            $response = response()->json(['status' => 'success', 'message' => 'Complain Details fetched successfully','data'=> $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'No Data found']);
        }
        return $response;
    }

    // Fetch Ind Complain Details
    public function getIndComplain($complain_id)
    {
        $Customers = new Customer();
        $Customers->complain_id = $complain_id;

        $result = $Customers->getIndComplain($Customers);
        if(count($result) > 0){
            $response = response()->json(['status' => 'success' , 'message' => '', 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Invalid Complain details']); 
        }
        return $response;
    }

    // Fetch Ind Complain Details
    public function deleteComplain($complain_id)
    {
        $Customers = new Customer();
        $Customers->complain_id = $complain_id;

        $result = $Customers->deleteComplain($Customers);
        if($result > 0){
            $response = response()->json(['status' => 'success' ,'message' => 'Complain details deleted']);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Error To delete']); 
        }
        return $response;
    }
}
