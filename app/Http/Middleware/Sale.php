<?php 

namespace App\Http\Controllers;

use PDF;
use Carbon\Carbon;
use Closure;
use DB;
use Mail;
use AWS;
//require(base_path().'/app/Http/Middleware/Common.php');

use Illuminate\Support\Facades\Storage;

class Sale{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next){
    	return $next($request);
    }

	public function addSale($Sale){
		$values = array(
			'warehouse_id'			=> $Sale->warehouse_id,
			'customer_id'			=> $Sale->customer_id,
			'customer_name'			=> $Sale->customer_name,
			'customer_code'			=> $Sale->customer_code,
			'date'					=> $Sale->date,
			'amount'				=> $Sale->amount,
			'discount_per'			=> $Sale->discount_per,
			'vat'					=> $Sale->vat,
			'due'					=> $Sale->due,
			'payment_type'			=> $Sale->payment_type,
			'payment_status'		=> $Sale->payment_status,
			'region_id'				=> $Sale->region_id,
			'branch_id'				=> $Sale->branch_id,
			'user_id'				=> $Sale->user_id,
			// 'sale_status'			=> $Sale->sale_status,
			'created_at'			=> date('Y-m-d H:i:s'),
			'updated_at'			=> date('Y-m-d H:i:s')
		);
		$sale_id = DB::table('gpff_sale')
						->insertGetId($values);	

		foreach ($Sale->sale_product_list as $sale_product_lists) {
			$list_values = array(

				'sale_id' 			=> $sale_id,
				'product_genericname' 	=> $sale_product_lists['product_genericname'],
				'product_id'			=> $sale_product_lists['product_id'],
				'quantity'				=> $sale_product_lists['quantity'],
				'unit_price'			=> $sale_product_lists['unit_price'],
				'total_price'			=> $sale_product_lists['total_price'],
				'created_at'			=> date('Y-m-d H:i:s'),
				'updated_at' 			=> date('Y-m-d H:i:s')
			);
			DB::table('gpff_sale_product_list')
			->insert($list_values);
		}


		//  PDF GENERATION FOR PRODUCTS PURCHASED

		// $purchased_product_details = DB::table('gpff_purchase')
		// 	->where('purchase_id',$purchase_id)
		// 	->first();

		
		// $purchased_product_details_Array = DB::table('gpff_purchase_product_list')
		// 	->where('purchase_id',$purchase_id)
		// 	->get();


		// $supplier_details = DB::table('gpff_supplier')
		// 	->where('supplier_id',$Purchases->supplier_id)
		// 	->first();


	
		// view()->share('purchase_id',$purchased_product_details->purchase_id);
		// view()->share('purchased_date',$purchased_product_details->created_at);
		// // supplier details
		// view()->share('supplier_code',$purchased_product_details->supplier_code);
		// view()->share('supplier_name',$supplier_details->supplier_name);
		// view()->share('contact_person',$supplier_details->supplier_contact_person);
		// view()->share('contact_number',$supplier_details->supplier_contact_number);
		// view()->share('email',$supplier_details->supplier_email);
		// view()->share('supplier_licence',$supplier_details->supplier_licence);
		// //End supplier details
		// view()->share('product_details_array',$purchased_product_details_Array);
		// view()->share('Grand_total',$purchased_product_details->amount);
		// view()->share('date',Carbon::now());

		// $pdf_name = $purchased_product_details->supplier_code;
		// $customPaper = array(0,0,767.00,883.80);

		// $pdf = PDF::loadView('purchasedproducts')
		// ->setPaper('a4', 'landscape');
		// $pdf->save(public_path('/Invoice/'.$pdf_name.'.pdf'));

		// $file_name = $pdf_name.'.pdf';
  //       $name = $file_name;

  //       $filePath = 'Invoice/'.$name; 

  //       Storage::disk('s3')->put($filePath, file_get_contents(public_path('Invoice')."/".$file_name));
		//unlink(public_path('Invoice')."/".$pdf_name.'.pdf');

		return 1;
	}

	
	public function getAllProductSale(){
		return DB::table('gpff_sale')
			->orderBy('gpff_sale.updated_at', 'DESC')
			->get();
	}

	public function getIndividualProductSale($Sale){
		return DB::table('gpff_sale as gs')
			->join('gpff_sale_product_list as gspl', 'gs.sale_id', 'gspl.sale_id')
			->join('gpff_customer as gpffcus', 'gs.customer_id', 'gpffcus.customer_id')
			->where('gs.sale_id', $Sale->sale_id)
			->orderBy('gs.updated_at', 'DESC')
			->get();
	}


	public function UpdateOrderSaleStatus($Sale){

		if($Sale->sale_status == 1){
			$update_values = array(
                    'sale_status'       => $Sale->sale_status,
                    'order_indate'  => date('Y-m-d H:i:s') ,
                	'updated_at'    => date('Y-m-d H:i:s')
                );

		} else if ($Sale->sale_status == 2) {
            $update_values = array(
                'sale_status'       => $Sale->sale_status,
                'order_opdate'  => date('Y-m-d H:i:s') ,
               	'updated_at'    => date('Y-m-d H:i:s')
            );
        } else if ($Sale->sale_status == 3 ) {
            $update_values = array(
                'sale_status'       => $Sale->sale_status,
                'order_oddate'  => date('Y-m-d H:i:s') ,
               	'updated_at'    => date('Y-m-d H:i:s')
            );
        }
         DB::table('gpff_sale')
			->where('gpff_sale.sale_id', $Sale->sale_id)
			->update($update_values);


			return 1;
	}

}

?>