<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use File;
use Mail;

require(base_path().'/app/Http/Middleware/Chat.php');

class ChatController extends Controller
{
   	//////////////////////
    //Chating Api Calls//
    /////////////////////
    //Admin Side Chat
    public function getAdminChatDetails(Request $request)
    {
        $Chats = new Chat();
        $Chats->user_id = $request->input('user_id');
        
        $result = $Chats->getAdminChatDetails($Chats);
        if(count($result) > 0){
            $response = response()->json(['status' => 'success', 'message' => '' , 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Error in fetching ID', 'data' => '']);
        }
        return $response;
    }
    //Branch Admin Side Chat
    public function getBranchAdminChatDetails(Request $request)
    {
        $Chats = new Chat();
        $Chats->user_id = $request->input('user_id');
        
        $result = $Chats->getBranchAdminChatDetails($Chats);
        if(count($result) > 0){
            $response = response()->json(['status' => 'success', 'message' => '' , 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Error in fetching ID', 'data' => '']);
        }
        return $response;
    }
    //Region Admin Side Chat
    public function getRegionManagerChatDetails(Request $request)
    {
        $Chats = new Chat();
        $Chats->user_id = $request->input('user_id');
        
        $result = $Chats->getRegionManagerChatDetails($Chats);
        if(count($result) > 0){
            $response = response()->json(['status' => 'success', 'message' => '' , 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Error in fetching ID', 'data' => '']);
        }
        return $response;
    }
    //Area Manager Side Chat
    public function getManagerChatDetails(Request $request)
    {
        $Chats = new Chat();
        $Chats->user_id = $request->input('user_id');
        
        $result = $Chats->getManagerChatDetails($Chats);
        if(count($result) > 0){
            $response = response()->json(['status' => 'success', 'message' => '' , 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Error in fetching ID', 'data' => '']);
        }
        return $response;
    }
	//FieldOfficer Side Chat details
	public function getFieldOfficerChatDetails(Request $request)
	{
		$Chats = new Chat();
		$Chats->user_id = $request->input('user_id');
		
		$result = $Chats->fetchFOChatDetails($Chats);
		if(count($result) > 0){
            $response = response()->json(['status' => 'success', 'message' => '' , 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Error in fetching ID', 'data' => '']);
        }
        return $response;
	}
    //Stockist Side Chat
    public function getStockistChatDetails(Request $request)
    {
        $Chats = new Chat();
        $Chats->user_id = $request->input('user_id');
        
        $result = $Chats->getStockistChatDetails($Chats);
        if(count($result) > 0){
            $response = response()->json(['status' => 'success', 'message' => '' , 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Error in fetching ID', 'data' => '']);
        }
        return $response;
    }
    //Customer Side Chat
    public function getCustomerChatDetails(Request $request)
    {
        $Chats = new Chat();
        $Chats->user_id = $request->input('user_id');
        
        $result = $Chats->getCustomerChatDetails($Chats);
        if(count($result) > 0){
            $response = response()->json(['status' => 'success', 'message' => '' , 'data' => $result]);
        }
        else{
            $response = response()->json(['status' => 'failure' , 'message' => 'Error in fetching ID', 'data' => '']);
        }
        return $response;
    }
}
