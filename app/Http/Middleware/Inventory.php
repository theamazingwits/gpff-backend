<?php

namespace App\Http\Controllers;

use Closure;
use DB;
use Mail;
use Carbon\Carbon;
use App\Imports\UsersImport;
use Maatwebsite\Excel\Facades\Excel;

require(base_path().'/app/Http/Middleware/Common.php');

class Inventory {
	private $cmn;

	function __construct() {
    	$this->cmn = new Common();
    	
  	}
	// Save Supplier
	public function createSupplier($data) {  
		$values = array(
			'supplier_name' => $data->supplier_name,
			'supplier_code' => $data->supplier_code,
			'supplier_email' => $data->supplier_email,
			'supplier_contact_number_code' => $data->supplier_contact_number_code,
			'supplier_country' => $data->supplier_country,
			'supplier_contact_number' => $data->supplier_contact_number,
			'branch_id' => $data->branch_id,
			'region_id' => $data->region_id,
			'created_by' => $data->created_by,
			'created_at' => date('Y-m-d H:i:s'),
			'updated_at' => date('Y-m-d H:i:s')
		);
		$result = DB::table('gpff_supplier')
					->insert($values);
		return $result;
    }
    //Active Deactive Supplier Details
    public function updateSupplierStatus($data) 
    {
        $values = array(
                'supplier_active_status'  => $data->supplier_status ,  
                'updated_at' => date('Y-m-d H:i:s')
             );

        return DB::table('gpff_supplier')
        ->where('supplier_id', $data->supplier_id)
        ->update($values);  
    }
    // Update Supplier
    public function updateSupplier($data) {
    	$values = array(
			'supplier_name' => $data->supplier_name,
			'supplier_code' => $data->supplier_code,
			'supplier_email' => $data->supplier_email,
			'supplier_contact_number_code' => $data->supplier_contact_number_code,
			'supplier_country' => $data->supplier_country,
			'supplier_contact_number' => $data->supplier_contact_number,
			'branch_id' => $data->branch_id,
			'region_id' => $data->region_id,
			'updated_by' => $data->updated_by,
			'updated_at' => date('Y-m-d H:i:s')
		);
		$result = DB::table('gpff_supplier')
					->where('supplier_id',$data->supplier_id)
					->update($values);
		return $result;
    }

    // Get the List
    public function getSupplier($colname, $data) {
		return $this->cmn->getQuery('gpff_supplier', $colname, $data);
    }

    public function getSupplierList($data) {
    	$result = DB::table('gpff_supplier')
    			->orderBy('updated_at','DESC');
    	if($data->branch_id != ''){
    		$result = $result->whereIn('branch_id',$data->branch_id);
    	}
    	if($data->region_id != ''){
    		$result = $result->whereIn('region_id',$data->region_id);
    	}
    	if($data->status != ''){
    		$result = $result->where('supplier_active_status',$data->status);
    	}
    	return $result->get();
    }


    public function deleteSupplier($data) {

    	return  DB::table('gpff_supplier')
                ->where('supplier_id', $data->supplier_id)
                ->delete();
    }
}