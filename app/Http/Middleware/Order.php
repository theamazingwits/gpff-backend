<?php

namespace App\Http\Controllers;

use Closure;
use DB;
use \PDF;
use Mail;
use Carbon\Carbon;
use App\Imports\UsersImport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;
use App\Jobs\InvoiceGenerate;


require(base_path().'/app/Http/Middleware/Common.php');

class Order
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        return $next($request);
    }

//////////////////////////////
//Order Management Api Calls//
//////////////////////////////
    //Store Orders Details 
    public function storeOrderDetails($Orders){

        $name = DB::table('gpff_customer')
                ->where('customer_id', $Orders->customer_id)
                ->First(['customer_name','customer_lan','customer_lat','region_id']);
        $balance_check = $this->getStockBalanceCheck($Orders);
        if($balance_check == 2){
            return 3;
        }
        $values = array(
            'customer_id'       => $Orders->customer_id ,
            'customer_name'     => $name->customer_name , 
            'warehouse_id'      => $Orders->warehouse_id,
            'field_officer_id'  => $Orders->field_officer_id,
            'field_officer_name'=> $Orders->field_officer_name,
            'biller_id'  => $Orders->biller_id,
            'biller_name'=> $Orders->biller_name,
            'payment_type'      => $Orders->payment_type,
            'order_date'        => $Orders->order_date , 
            'tot_box'           => $Orders->tot_box,
            'or_gross_total'    => $Orders->or_gross_total ,
            'or_tot_price'      => $Orders->or_tot_price ,
            'or_blc_price'      => $Orders->or_tot_price ,
            'order_discount'    => $Orders->order_discount ,
            'order_created_by'  => $Orders->order_created_by,
            'branch_id'         => $Orders->branch_id ,
            'region_id'         => $Orders->region_id ,
            'area_id'           => $Orders->area_id ,                
            'or_type'           => $Orders->or_type,
            'stokist_id'        => $Orders->order_created_by,
            'credit_lim'        => $Orders->credit_lim,
            'credit_valid_to'   => $Orders->credit_valid_to,
            'spl_discount'       => $Orders->spl_discount ,
            'created_at'        => date('Y-m-d H:i:s') , 
            'updated_at'        => date('Y-m-d H:i:s')
        );

        $order_id   =   DB::table('gpff_order')
                        ->insertGetId($values);

        if($Orders->telecalling_id != ''){

            $tele_value = array(
                // 'events'            => $Orders->events,
                'telecalling_type'  => $Orders->telecalling_type,
                'order_id'          => $order_id,
                'updated_at'        => date('Y-m-d H:i:s')
            );

            DB::table('gpff_telecalling')
            ->where('telecalling_id', $Orders->telecalling_id)
            ->update($tele_value);
        }

        $pdf_name = "GPFF_OR_".$order_id;
        $file_name = $pdf_name.'.pdf';

        $value = array(
                'invoice_no'        => $file_name ,
                'invoice_type'      => 1,
                'invoice_date'      => date('Y-m-d H:i:s'),
                'updated_at'        => date('Y-m-d H:i:s')
            );

        DB::table('gpff_order')
        ->where('order_id',$order_id)
        ->update($value);

        $SP_FOC = 0;
        $list_value = [];
        foreach($Orders->order_list as $order_lists)
        {
            $SP_FOC = $SP_FOC + $order_lists['spl_FOC'];
            $list_value[] = array(
                'order_id'              => $order_id,
                'category_id'           => $order_lists['category_id'],
                'category_name'         => $order_lists['category_name'],
                'product_id'            => $order_lists['product_id'],
                'product_genericname'   => $order_lists['product_genericname'],
                'product_qty'           => $order_lists['product_qty'],
                // 'accepted_qty'          => 0,
                'product_tot_price'     => $order_lists['product_tot_price'],
                'product_type'          => $order_lists['product_type'],
                'created_at'            => date('Y-m-d H:i:s') ,
                'updated_at'            => date('Y-m-d H:i:s')
            );
        }
        DB::table('gpff_order_list')
        ->insert($list_value);
        $order_list_id = DB::table('gpff_order_list')
                        ->where('order_id',$order_id)
                        ->first();                                
        $order_list_id = $order_list_id->order_list_id;
        foreach($Orders->order_list as $order_lists)
        {
            $list_value_batch[] = array(
                'order_id'              => $order_id,
                'order_list_id'         => $order_list_id,
                'category_id'           => $order_lists['category_id'],
                'product_id'            => $order_lists['product_id'],
                'batch_no'              => $order_lists['batch_no'],
                'batch_emp_date'        => $order_lists['batch_emp_date'],    
                'product_qty'           => $order_lists['product_qty'],
                'product_netprice'      => $order_lists['product_netprice'],
                'product_grossprice'    => $order_lists['product_grossprice'],
                'product_discount'      => $order_lists['product_discount'],
                'product_tot_price'     => $order_lists['product_tot_price'],
                'product_type'          => $order_lists['product_type'],
                'scheme'                => $order_lists['scheme'],
                'comments'              => $order_lists['comments'],
                'FOC'                   => $order_lists['FOC'],
                'FOC_amt'               => $order_lists['FOC_amt'],
                'spl_FOC'               => $order_lists['spl_FOC'],
                'pro_amt_cal'           => $order_lists['pro_amt_cal'],
                'created_at'            => date('Y-m-d H:i:s') ,
                'updated_at'            => date('Y-m-d H:i:s')
            );
            $order_list_id = $order_list_id + 1;
        }
        DB::table('gpff_order_list_batchwise')
                ->insert($list_value_batch);

        if($SP_FOC == 0 && $Orders->spl_discount == 0){
            // print_r("expression");

           $orvalue1 = array(
                    'order_status' => 1,
                    'updated_at'  => date('Y-m-d H:i:s')
            );

            DB::table('gpff_order')
                    ->where('order_id', $order_id)
                    ->update($orvalue1);

            $event_values = array(
                'order_id'    => $order_id, 
                'event_name'  => "The order is in-progress" ,
                'event_date'  => now() ,
                'order_status'  => 1 ,
                'created_at'  => date('Y-m-d H:i:s') , 
                'updated_at'  => date('Y-m-d H:i:s')
            );

            DB::table('gpff_order_events')
            ->insert($event_values);
        } else{
            $orvalue1 = array(
                    'order_status'       => 5,
                    'updated_at'        => date('Y-m-d H:i:s')
                );

             DB::table('gpff_order')
            ->where('order_id', $order_id)
            ->update($orvalue1);

            $event_values = array(
                    'order_id'    => $order_id, 
                    'event_name'  => "The order send to the admin" ,
                    'event_date'  => now() ,
                    'order_status'  => 5 ,
                    'created_at'  => date('Y-m-d H:i:s') , 
                    'updated_at'  => date('Y-m-d H:i:s')
                );
                
            DB::table('gpff_order_events')
            ->insert($event_values);
        }

        $this->stockReduceBasedOrder($order_id); 

        // $order_details = DB::table('gpff_order')
        //                 ->where('order_id',$order_id)
        //                 ->First();

        // $order_list_detail = DB::table('gpff_order as gor')
        //                     ->join('gpff_order_list as gol', 'gol.order_id', 'gor.order_id')
        //                     ->join('gpff_order_list_batchwise as gprl','gol.order_list_id','gprl.order_list_id')
        //                     ->where('gol.order_id',$order_id)
        //                     ->get();

        // $warehouse = DB::table('gpff_warehouse')
        //             ->where('warehouse_id', $order_details->warehouse_id)
        //             ->First();

        // $cus_de = DB::table('gpff_customer')
        //                 ->where('customer_id',$order_details->customer_id)
        //                 ->First();

        // $com_details = DB::table('gpff_branch')
        //                 ->where('branch_id',$order_details->branch_id)
        //                 ->First();

        // $number = $this->numbertostring($order_details->or_tot_price);

        // //Invoice
        // view()->share('datas',$order_list_detail);
        // view()->share('order_details',$order_details);
        // view()->share('cus_de',$cus_de);
        // view()->share('war', $warehouse);
        // view()->share('com_details',$com_details);
        // view()->share('invoice',$pdf_name);
        // view()->share('value',$number);
        // view()->share('date',Carbon::now());

        // $pdf = \PDF::loadView('or_invoice')
        // ->setPaper('a4', 'landscape');
        // $pdf->save(public_path('Order_invoice/'.$pdf_name.'.pdf'));
        // $file_name = $pdf_name.'.pdf';
        // $name = $file_name;
        // $filePath = 'Order_invoice/'.$name; 
  
        // Storage::disk('s3')->put($filePath, file_get_contents(public_path('Order_invoice')."/".$file_name));


        $cmn = new Common();
        $invoice_details =  array(
            'order_id' => $order_id,
            'order_type' => 1,
            'invoice_url' => $file_name,
            'invoice_log_status' => 0,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
          );
        $cmn->insertInvoiceQueue($invoice_details);
        
        if($Orders->or_type == 2){

            $waremessage = "Customer ".$name->customer_name." has placed one Order to your WareHouse.";

            $warename = DB::table('gpff_users')
                    ->where('warehouse_id', $war[0]->warehouse_id)
                    ->First();

            $page_id = 'UNKNOWN';

            $cmn->insertNotification($Orders->customer_id,$name->customer_name,$warename->user_id,$waremessage,$page_id);
        }else if($Orders->or_type == 1){

            //Field Officer Target
            $target = DB::table('gpff_insentive')
                    ->where('fo_id', $Orders->field_officer_id)
                    ->where('insentive_status', 1)
                    ->get();
        
            //Notification Entry
            $name = DB::table('gpff_users')
                    ->where('user_id', $Orders->field_officer_id)
                    ->First();
            
            $waremessage = "Field Officer ".$name->firstname." has placed one Order to your WareHouse.";

            $warename = DB::table('gpff_users')
                    ->where('warehouse_id', $Orders->warehouse_id)
                    ->First();

            $message = "Field Officer ".$name->firstname." has placed one to the Order.";
            $page_id = 'UNKNOWN';

            if($Orders->area_manager_id){
                $cmn->insertNotification($Orders->field_officer_id,$name->firstname,$Orders->area_manager_id,$message,$page_id);
                $cmn->insertNotification($Orders->field_officer_id,$name->firstname,$warename->user_id,$waremessage,$page_id);
            } 
             //End Notification Entry
        }
        InvoiceGenerate::dispatch();
        return 1;   
    }

    public function getStockBalanceCheck($Orders){
        foreach($Orders->order_list as $order_lists){
            $blc_qty = DB::table('gpff_product_batch_stock')
                            ->where('category_id', $order_lists['category_id'])
                            ->where('product_id', $order_lists['product_id'])
                            ->where('batch_id',$order_lists['batch_no'])
                            ->where('warehouse_id',$Orders->warehouse_id)
                            ->sum('product_blc_qty');
            $product_qty = $order_lists['product_qty'];
            $foc_qty = $order_lists['FOC'];
            $spl_foc = $order_lists['spl_FOC'];

            $total_qty = (int)$product_qty + (int)$foc_qty + (int)$spl_foc;
            if($blc_qty < $total_qty){
                return 2;
            }

        }
        return 1;
    }
    //Get field officer Order Details
    public function PDFDetails($Orders)
    {   
        $order_id = 1716;
        /*print_r($order_id);
        exit;*/
        
        $pdf_name = "GPFF_OR_".$order_id;

        $order_details = DB::table('gpff_order')
                        ->where('order_id',$order_id)
                        ->First();

        $order_list = DB::table('gpff_order_list as gol')
                        ->leftjoin('gpff_order_list_batchwise as gprl','gol.order_list_id','gprl.order_list_id')
                        ->where('gol.order_id',$order_id)
                        ->get();

        $cus_de = DB::table('gpff_customer')
                        ->where('customer_id',$order_details->customer_id)
                        ->First();

        $com_details = DB::table('gpff_branch')
                        ->where('branch_id',$order_details->branch_id)
                        ->First();

        $number = $this->numbertostring($order_details->or_tot_price);
        //Invoice
        view()->share('datas',$order_list);
        view()->share('order_details',$order_details);
        view()->share('cus_de',$cus_de);
        view()->share('com_details',$com_details);
        view()->share('invoice',$pdf_name);
        view()->share('value',$number);
        view()->share('date',Carbon::now());

        $pdf = \PDF::loadView('test2')
        ->setPaper('a4', 'landscape');
        $pdf->save(public_path('Order_invoice/'.$pdf_name.'.pdf'));
        return 1;   
    }

    public function numbertostring($number)
    {   
    //     $ones = array(
    //         0 =>"ZERO",
    //         1 => "ONE",
    //         2 => "TWO",
    //         3 => "THREE",
    //         4 => "FOUR",
    //         5 => "FIVE",
    //         6 => "SIX",
    //         7 => "SEVEN",
    //         8 => "EIGHT",
    //         9 => "NINE",
    //         10 => "TEN",
    //         11 => "ELEVEN",
    //         12 => "TWELVE",
    //         13 => "THIRTEEN",
    //         14 => "FOURTEEN",
    //         15 => "FIFTEEN",
    //         16 => "SIXTEEN",
    //         17 => "SEVENTEEN",
    //         18 => "EIGHTEEN",
    //         19 => "NINETEEN",
    //         "014" => "FOURTEEN"
    //         );
    //         $tens = array( 
    //         0 => "ZERO",
    //         1 => "TEN",
    //         2 => "TWENTY",
    //         3 => "THIRTY", 
    //         4 => "FORTY", 
    //         5 => "FIFTY", 
    //         6 => "SIXTY", 
    //         7 => "SEVENTY", 
    //         8 => "EIGHTY", 
    //         9 => "NINETY" 
    //         ); 
    //         $hundreds = array( 
    //         "HUNDRED", 
    //         "THOUSAND", 
    //         "MILLION", 
    //         "BILLION", 
    //         "TRILLION", 
    //         "QUARDRILLION" 
    //         ); /*limit t quadrillion */
    //         $num = number_format($num,2,".",","); 
    //         $num_arr = explode(".",$num); 
    //         $wholenum = $num_arr[0]; 
    //         $decnum = $num_arr[1]; 
    //         $whole_arr = array_reverse(explode(",",$wholenum)); 
    //         krsort($whole_arr,1); 
    //         $rettxt = ""; 
    //         foreach($whole_arr as $key => $i){
                
    //         while(substr($i,0,1)=="0")
    //                 $i=substr($i,1,5);
    //         if($i < 20){ 
    //         /* echo "getting:".$i; */
    //         $rettxt .= $ones[$i]; 
    //         }elseif($i < 100){ 
    //         if(substr($i,0,1)!="0")  $rettxt .= $tens[substr($i,0,1)]; 
    //         if(substr($i,1,1)!="0") $rettxt .= " ".$ones[substr($i,1,1)]; 
    //         }else{ 
    //         if(substr($i,0,1)!="0") $rettxt .= $ones[substr($i,0,1)]." ".$hundreds[0]; 
    //         if(substr($i,1,1)!="0")$rettxt .= " ".$tens[substr($i,1,1)]; 
    //         if(substr($i,2,1)!="0")$rettxt .= " ".$ones[substr($i,2,1)]; 
    //         } 
    //         if($key > 0){ 
    //         $rettxt .= " ".$hundreds[$key]." "; 
    //         }
    //         } 
    //         if($decnum > 0){
    //         $rettxt .= " and ";
    //         if($decnum < 20){
    //         $rettxt .= $ones[$decnum];
    //         }elseif($decnum < 100){
    //         $rettxt .= $tens[substr($decnum,0,1)];
    //         $rettxt .= " ".$ones[substr($decnum,1,1)];
    //         }
    //         }
    //         return $rettxt;
    // }
        $decimal = round($number - ($no = floor($number)), 2) * 100;
        $decimal_part = $decimal;
        $hundred = null;
        $hundreds = null;
        $digits_length = strlen($no);
        $decimal_length = strlen($decimal);
        $i = 0;
        $str = array();
        $str2 = array();
        $words = array(0 => '', 1 => 'one', 2 => 'two',
            3 => 'three', 4 => 'four', 5 => 'five', 6 => 'six',
            7 => 'seven', 8 => 'eight', 9 => 'nine',
            10 => 'ten', 11 => 'eleven', 12 => 'twelve',
            13 => 'thirteen', 14 => 'fourteen', 15 => 'fifteen',
            16 => 'sixteen', 17 => 'seventeen', 18 => 'eighteen',
            19 => 'nineteen', 20 => 'twenty', 30 => 'thirty',
            40 => 'forty', 50 => 'fifty', 60 => 'sixty',
            70 => 'seventy', 80 => 'eighty', 90 => 'ninety');
        $digits = array('', 'hundred','thousand','lakh', 'crore');

        while( $i < $digits_length ) {
            $divider = ($i == 2) ? 10 : 100;
            $number = floor($no % $divider);
            $no = floor($no / $divider);
            $i += $divider == 10 ? 1 : 2;
            if ($number) {
                $plural = (($counter = count($str)) && $number > 9) ? 's' : null;
                $hundred = ($counter == 1 && $str[0]) ? ' and ' : null;
                $str [] = ($number < 21) ? $words[$number].' '. $digits[$counter]. $plural.' '.$hundred:$words[floor($number / 10) * 10].' '.$words[$number % 10]. ' '.$digits[$counter].$plural.' '.$hundred;
            } else $str[] = null;
        }

        $d = 0;
        while( $d < $decimal_length ) {
            $divider = ($d == 2) ? 10 : 100;
            $decimal_number = floor($decimal % $divider);
            $decimal = floor($decimal / $divider);
            $d += $divider == 10 ? 1 : 2;
            if ($decimal_number) {
                $plurals = (($counter = count($str2)) && $decimal_number > 9) ? 's' : null;
                $hundreds = ($counter == 1 && $str2[0]) ? ' and ' : null;
                @$str2 [] = ($decimal_number < 21) ? $words[$decimal_number].' '. $digits[$decimal_number]. $plural.' '.$hundred:$words[floor($decimal_number / 10) * 10].' '.$words[$decimal_number % 10]. ' '.$digits[$counter].$plural.' '.$hundred;
            } else $str2[] = null;
        }

        $Rupees = implode('', array_reverse($str));
        $paise = implode('', array_reverse($str2));
        $paise = ($decimal_part > 0) ? $paise . ' Kyat' : '';
        return ($Rupees ? $Rupees . 'Kyat ' : '') . $paise;
    }

    // public function getFieldOfficerOrder($Orders)
    // {   
    //     return  DB::table('gpff_order')
    //             ->where('field_officer_id', $Orders->field_officer_id)
    //             ->orderBy('updated_at','DESC')
    //             ->get();
    // }

public function getFieldOfficerOrder($Orders)
    {   
        if($Orders->type == 1){
            return  DB::table('gpff_order as gpor')
                    ->join('gpff_warehouse as gpwar','gpor.warehouse_id','gpwar.warehouse_id')
                    ->where('gpor.field_officer_id', $Orders->field_officer_id)
                    ->orderBy('gpor.updated_at','DESC')
                    ->get(['gpor.order_id','gpor.task_id','gpor.field_officer_id','gpor.field_officer_name','gpor.area_manager_id','gpor.customer_id','gpor.customer_name','gpor.order_date','gpor.or_tot_price','gpor.or_blc_price','gpor.branch_id','gpor.region_id','gpor.area_id','gpor.or_type','gpor.invoice_type','gpor.warehouse_id','gpwar.warehouse_name','gpor.order_status','gpor.payment_type', 'gpor.payment_status','gpor.invoice_no','gpwar.warehouse_name','gpor.created_at','gpor.updated_at','gpor.reject_reason']);
        }else if($Orders->type == 2){
            return  DB::table('gpff_order as gpor')
                    ->join('gpff_warehouse as gpwar','gpor.warehouse_id','gpwar.warehouse_id')
                    ->where('gpor.field_officer_id', $Orders->field_officer_id)
                    ->where('gpor.order_status', 3)
                    // ->where('gpor.payment_status', 2)
                    ->whereIn('gpor.payment_status',[2,3,4])
                    ->orderBy('gpor.updated_at','DESC')
                    ->get(['gpor.order_id','gpor.task_id','gpor.field_officer_id','gpor.field_officer_name','gpor.area_manager_id','gpor.customer_id','gpor.customer_name','gpor.order_date','gpor.or_tot_price','gpor.or_blc_price','gpor.branch_id','gpor.region_id','gpor.area_id','gpor.or_type','gpor.invoice_type','gpor.warehouse_id','gpwar.warehouse_name','gpor.order_status','gpor.payment_type','gpor.payment_status','gpor.invoice_no', 'gpwar.warehouse_name','gpor.created_at','gpor.updated_at']);
        }else if($Orders->type == 3){
            return  DB::table('gpff_order as gpor')
                    ->join('gpff_warehouse as gpwar','gpor.warehouse_id','gpwar.warehouse_id')
                    ->where('gpor.field_officer_id', $Orders->field_officer_id)
                    ->where('gpor.order_status', 3)
                    ->where('gpor.payment_status', 1)
                    ->orderBy('gpor.updated_at','DESC')
                    ->get(['gpor.order_id','gpor.task_id','gpor.field_officer_id','gpor.field_officer_name','gpor.area_manager_id','gpor.customer_id','gpor.customer_name','gpor.order_date','gpor.or_tot_price','gpor.or_blc_price','gpor.branch_id','gpor.region_id','gpor.area_id','gpor.or_type','gpor.invoice_type','gpor.warehouse_id','gpwar.warehouse_name','gpor.order_status','gpor.payment_type','gpor.payment_status','gpor.invoice_no', 'gpwar.warehouse_name','gpor.created_at','gpor.updated_at']);
        }
    }

    //Get field officer Order List Details
    public function getOrderBasedList($Orders)
    {   
        return  DB::table('gpff_order_list')
                ->where('order_id', $Orders->order_id)
                ->orderBy('updated_at','DESC')
                ->get();
    }
    //Get field officer Order List Details
    public function getOrderBasedBatchList($Orders)
    {   
        if($Orders->vendor == 2){
         $balance =  DB::table('gpff_order')
         ->where('order_id', $Orders->order_id)
         ->First(['or_blc_price']);
         if($balance->or_blc_price != 0){
            return  DB::table('gpff_order_list as gol')
            ->leftjoin('gpff_order_list_batchwise as golb','golb.order_list_id','gol.order_list_id')
            ->join("gpff_order as gpor", 'gpor.order_id', 'gol.order_id')
            ->where('gol.order_id', $Orders->order_id)
            ->where('gpor.order_status', 3)
            ->orderBy('gol.created_at','ASC')
            ->get(['gol.order_id','gol.order_list_id','gol.category_id','gol.category_name','golb.product_id','gol.product_genericname','golb.batch_no','golb.batch_emp_date','golb.product_qty','golb.product_netprice','golb.product_grossprice','golb.or_gross_total','golb.product_discount','golb.product_tot_price','golb.product_type','golb.scheme','golb.comments','golb.FOC','golb.FOC_amt','golb.pro_amt_cal','golb.created_at','golb.updated_at','gpor.or_blc_price','gpor.or_tot_price', 'gpor.acc_or_tot_price', 'gpor.or_gross_total','gpor.spl_discount', 'gpor.order_discount','gpor.customer_id', 'gpor.customer_name','gpor.warehouse_id','gpor.order_status','gpor.payment_type']);
        }
    }else{
        $balance =  DB::table('gpff_vendor_order')
        ->where('vendor_order_id', $Orders->order_id)
        ->First(['or_blc_price']);
        if($balance->or_blc_price != 0){
            return  DB::table('gpff_vendor_order_list_batchwise as gvolb')
            ->leftjoin('gpff_vendor_order_list as gvol','gvolb.vendor_order_list_id','gvol.vendor_order_list_id')
            ->join('gpff_vendor_order as gpvor', 'gpvor.vendor_order_id', 'gvolb.vendor_order_id')
            ->where('gvolb.vendor_order_id', $Orders->order_id)
            ->where('gpvor.vendor_order_status', 3)
            ->orderBy('gvolb.created_at','ASC')
            ->get(['gvolb.vendor_order_id','gvolb.vendor_order_list_id','gvolb.category_id','gvol.category_name','gvolb.product_id','gvol.product_genericname','gvolb.batch_no','gvolb.batch_emp_date','gvolb.product_qty','gvolb.product_netprice','gvolb.product_grossprice','gvolb.or_gross_total','gvolb.product_discount','gvolb.product_tot_price','gvolb.product_type','gvolb.scheme','gvolb.comments','gvolb.FOC','gvolb.FOC_amt','gvolb.pro_amt_cal','gvolb.created_at','gvolb.updated_at','gpvor.or_blc_price','gpvor.or_tot_price', 'gpvor.acc_or_tot_price', 'gpvor.or_gross_total','gpvor.spl_discount', 'gpvor.order_discount','gpvor.vendor_id', 'gpvor.vendor_name', 'gpvor.warehouse_id','gpvor.vendor_order_status','gpvor.payment_type']);
        }
    }
}
    // Fetch Order Based Events details
    public function getOrderBasedEventsList($Orders)
    {   
        return  DB::table('gpff_order_events')
                ->where('order_id', $Orders->order_id)
                ->orderBy('updated_at','ASC')
                ->get();
    }
    //Get manager Order Details
    public function getManagerOrder($Orders)
    {   
        $data = DB::table('gpff_users')
                    ->where('user_id', $Orders->area_manager_id)
                    ->First(['area_id']);
                
        $myArray = explode(',', $data->area_id);

        $details = DB::table('gpff_order as gpor')
                    ->join('gpff_warehouse as gpwar','gpor.warehouse_id','gpwar.warehouse_id')
                    ->whereIn('gpor.area_id', $myArray)
                    ->whereBetween(DB::RAW('date(gpor.created_at)'), [date($Orders->start_date), date($Orders->end_date)])
                    ->orderBy('gpor.updated_at','DESC');

        if($Orders->field_officer_id != ''){
            $details = $details->whereIn('gpor.field_officer_id', $Orders->field_officer_id);
        }

        $details = $details->get(['gpor.order_id','gpor.task_id','gpor.field_officer_id','gpor.field_officer_name','gpor.area_manager_id','gpor.customer_id','gpor.customer_name','gpor.order_date','gpor.or_tot_price','gpor.branch_id','gpor.region_id','gpor.area_id','gpor.warehouse_id','gpwar.warehouse_name','gpor.order_status','gpor.created_at','gpor.updated_at','gpor.payment_type', 'gpor.invoice_type','gpor.or_type' ]);

        return $details;
    }
    //Get field officer Order List Details
    public function getManagerOrderBasedList($Orders)
    {   
        $id =   DB::table('gpff_order')
                ->where('order_id', $Orders->order_id)
                ->First(['customer_id', 'invoice_type','branch_id','warehouse_id']);
        // print_r($id);
        $warehouse =  DB::table('gpff_branch as gb')
        ->join('gpff_warehouse as gw', 'gw.branch_id', 'gb.branch_id')
        ->where('gw.warehouse_id', $id->warehouse_id)
        ->where('gb.branch_id', $id->branch_id)
        ->get(['gw.warehouse_name','gw.warehouse_address','gw.warehouse_contact','gb.branch_name']);

        if($id->invoice_type == 0){

            $customer    =   DB::table('gpff_customer')
                            ->where('customer_id', $id->customer_id)
                            ->get();
            // print_r($customer);

            $product = DB::table('gpff_order_list')
                    ->where('order_id', $Orders->order_id)
                    ->get();

            $status    =    DB::table('gpff_order as gpor')
                            ->join('gpff_users as gpusr', 'gpor.order_created_by', 'gpusr.user_id')
                            ->where('gpor.order_id', $Orders->order_id)
                            ->get(['gpor.order_status','gpor.order_date','gpor.order_rej_date','gpor.reject_reason','gpor.acc_or_tot_price','gpor.or_tot_price','gpor.or_gross_total','gpor.order_discount','gpor.payment_type','gpor.invoice_no','gpor.spl_discount','gpor.or_type', 'gpor.invoice_type','gpor.net_or_gross_price','gpusr.firstname']);



        }else{

            $customer    =   DB::table('gpff_customer')
                            ->where('customer_id', $id->customer_id)
                            ->get();


            $product    =   DB::table('gpff_order_list_batchwise as golb')
                            ->leftjoin('gpff_order_list as gol','golb.order_list_id','gol.order_list_id')
                            ->where('golb.order_id', $Orders->order_id)
                            ->orderBy('golb.created_at','ASC')
                            ->get(['golb.order_id','golb.order_list_id','golb.category_id','gol.category_name','golb.product_id','gol.product_genericname','golb.batch_no','golb.batch_emp_date','golb.product_qty','golb.product_netprice','golb.product_grossprice','golb.or_gross_total','golb.product_discount','golb.product_tot_price','golb.product_type','golb.scheme','golb.comments','golb.FOC','golb.FOC_amt','golb.spl_FOC','golb.pro_amt_cal','golb.created_at','golb.updated_at']);

            $status    =    DB::table('gpff_order as gpor')
                            ->join('gpff_users as gpusr', 'gpor.order_created_by', 'gpusr.user_id')
                            ->where('gpor.order_id', $Orders->order_id)
                            ->get(['gpor.order_status','gpor.order_date','gpor.order_rej_date','gpor.reject_reason','gpor.acc_or_tot_price','gpor.or_tot_price','gpor.or_gross_total','gpor.order_discount','gpor.payment_type','gpor.invoice_no','gpor.spl_discount','gpor.or_type', 'gpor.invoice_type','gpor.net_or_gross_price','gpusr.firstname']);
        }

        $result = array(
                'customer'  =>  $customer,
                'product'   =>  $product,
                'status'    =>  $status,
                'warehouse'  => $warehouse
            );

        return $result;
    }
    public function getAllOrderDashboard($Orders) 
    {
        $data =[];
        $data['totaldata']= DB::table('gpff_order as gpor')
                            ->leftjoin('gpff_warehouse as gpwar','gpor.warehouse_id','gpwar.warehouse_id')
                            ->leftjoin('gpff_region as gpre','gpor.region_id','gpre.region_id')
                            ->leftjoin('gpff_branch as gpba','gpor.branch_id','gpba.branch_id')
                            ->leftjoin('gpff_area as gpar','gpor.area_id','gpar.area_id')
                            ->whereBetween(DB::RAW('date(gpor.created_at)'), [date($Orders->start_date), date($Orders->end_date)])
                            ->orderBy('updated_at','DESC')
                            ->get(['gpor.order_id','gpor.task_id','gpor.field_officer_id','gpor.field_officer_name','gpor.area_manager_id','gpor.customer_id','gpor.customer_name','gpor.order_date','gpor.or_tot_price','gpor.acc_or_tot_price','gpor.branch_id','gpba.branch_name','gpor.region_id','gpre.region_name','gpor.area_id','gpar.area_name','gpor.stokist_id','gpor.warehouse_id','gpwar.warehouse_name','gpor.order_rej_date','gpor.order_status','gpor.invoice_type','gpor.reject_reason','gpor.payment_type','gpor.or_type','gpor.payment_img','gpor.created_at','gpor.updated_at']);
         return $data;   
    }
    // Fetch (All)Order Details //Customer name
    public function getAllOrder() 
    {
        $data =[];
        $data['totaldata']= DB::table('gpff_order as gpor')
                            ->leftjoin('gpff_warehouse as gpwar','gpor.warehouse_id','gpwar.warehouse_id')
                            ->leftjoin('gpff_region as gpre','gpor.region_id','gpre.region_id')
                            ->leftjoin('gpff_branch as gpba','gpor.branch_id','gpba.branch_id')
                            ->leftjoin('gpff_area as gpar','gpor.area_id','gpar.area_id')
                            ->orderBy('updated_at','DESC')
                            ->get(['gpor.order_id','gpor.task_id','gpor.field_officer_id','gpor.field_officer_name','gpor.area_manager_id','gpor.customer_id','gpor.customer_name','gpor.order_date','gpor.or_tot_price','gpor.acc_or_tot_price','gpor.branch_id','gpba.branch_name','gpor.region_id','gpre.region_name','gpor.area_id','gpar.area_name','gpor.stokist_id','gpor.warehouse_id','gpwar.warehouse_name','gpor.order_rej_date','gpor.order_status','gpor.invoice_type','gpor.reject_reason','gpor.payment_type','gpor.or_type','gpor.payment_img','gpor.created_at','gpor.updated_at']);
         return $data;   
    }
    // Fetch (All)Order Details //Customer name
    public function getBranchAllOrder($Orders) 
    {   
        $userdata = DB::table('gpff_users')
                    ->where('user_id', $Orders->user_id)
                    ->First();

        $myArray = explode(',', $userdata->branch_id);

        $data =[];
        $data['totaldata']= DB::table('gpff_order as gpor')
                            ->leftjoin('gpff_warehouse as gpwar','gpor.warehouse_id','gpwar.warehouse_id')
                            ->leftjoin('gpff_region as gpre','gpor.region_id','gpre.region_id')
                            ->leftjoin('gpff_branch as gpba','gpor.branch_id','gpba.branch_id')
                            ->leftjoin('gpff_area as gpar','gpor.area_id','gpar.area_id')
                            ->whereIn('gpor.branch_id', $myArray)
                            ->orderBy('updated_at','DESC')
                            ->get(['gpor.order_id','gpor.task_id','gpor.field_officer_id','gpor.field_officer_name','gpor.area_manager_id','gpor.customer_id','gpor.customer_name','gpor.order_date','gpor.or_tot_price','gpor.acc_or_tot_price','gpor.branch_id','gpba.branch_name','gpor.region_id','gpre.region_name','gpor.area_id','gpar.area_name','gpor.stokist_id','gpor.warehouse_id','gpwar.warehouse_name','gpor.order_rej_date','gpor.order_status','gpor.reject_reason','gpor.payment_type','gpor.or_type','gpor.payment_img','gpor.created_at','gpor.updated_at']);
         return $data;   
    }
    // Fetch (All)Order Details //Customer name
    public function getWarAllOrder($Orders) 
    {   
        if($Orders->type == 1){
            return DB::table('gpff_order as gpor')
                            ->leftjoin('gpff_warehouse as gpwar','gpor.warehouse_id','gpwar.warehouse_id')
                            ->leftjoin('gpff_region as gpre','gpor.region_id','gpre.region_id')
                            ->leftjoin('gpff_branch as gpba','gpor.branch_id','gpba.branch_id')
                            ->leftjoin('gpff_area as gpar','gpor.area_id','gpar.area_id')
                            ->where('gpor.warehouse_id', $Orders->warehouse_id)
                            ->where('gpor.or_type', 3)
                            ->orderBy('updated_at','DESC')
                            ->get(['gpor.order_id','gpor.task_id','gpor.field_officer_id','gpor.field_officer_name','gpor.area_manager_id','gpor.customer_id','gpor.customer_name','gpor.order_date','gpor.or_tot_price','gpor.acc_or_tot_price','gpor.branch_id','gpba.branch_name','gpor.region_id','gpre.region_name','gpor.area_id','gpar.area_name','gpor.stokist_id','gpor.warehouse_id','gpwar.warehouse_name','gpor.order_rej_date','gpor.order_status','gpor.reject_reason','gpor.payment_type','gpor.or_type','gpor.payment_img','gpor.created_at','gpor.updated_at']);
        }else{
            return DB::table('gpff_order as gpor')
                            ->leftjoin('gpff_warehouse as gpwar','gpor.warehouse_id','gpwar.warehouse_id')
                            ->leftjoin('gpff_region as gpre','gpor.region_id','gpre.region_id')
                            ->leftjoin('gpff_branch as gpba','gpor.branch_id','gpba.branch_id')
                            ->leftjoin('gpff_area as gpar','gpor.area_id','gpar.area_id')
                            ->where('gpor.warehouse_id', $Orders->warehouse_id)
                            ->orderBy('updated_at','DESC')
                            ->get(['gpor.order_id','gpor.task_id','gpor.field_officer_id','gpor.field_officer_name','gpor.area_manager_id','gpor.customer_id','gpor.customer_name','gpor.order_date','gpor.or_tot_price','gpor.acc_or_tot_price','gpor.branch_id','gpba.branch_name','gpor.region_id','gpre.region_name','gpor.area_id','gpar.area_name','gpor.stokist_id','gpor.warehouse_id','gpwar.warehouse_name','gpor.order_rej_date','gpor.order_status','gpor.reject_reason','gpor.payment_type','gpor.or_type','gpor.payment_img','gpor.created_at','gpor.updated_at']);
        }
        
    }
    // Update Order Status
    public function updateOrderStatus($Orders) 
    {   
        //Qty Reduce In My Stock
        if($Orders->order_status == 2){

            // $ord_de  =  DB::table('gpff_order_list_batchwise')
            //             ->where('order_id', $Orders->order_id)
            //             ->get(['product_id','product_qty','batch_no', 'FOC', 'spl_FOC']);

            // $ware = DB::table('gpff_order')
            //         ->where('order_id', $Orders->order_id)
            //         ->First();

            
            // foreach ($ord_de as $ord_value) {
                
            //     $blc_qty  =  DB::table('gpff_product_batch_stock')
            //                 ->where('product_id', $ord_value->product_id)
            //                 ->where('batch_id', $ord_value->batch_no)
            //                 ->where('warehouse_id', $ware->warehouse_id)
            //                 ->sum('product_blc_qty');
            //     // print_r($blc_qty);

            //     $sal_qty  =  DB::table('gpff_product_batch_stock')
            //                 ->where('product_id', $ord_value->product_id)
            //                 ->where('batch_id', $ord_value->batch_no)
            //                 ->where('warehouse_id', $ware->warehouse_id)
            //                 ->sum('product_sales_qty');

            //                 // print_r($ord_value);exit;


            //     $qty = $ord_value->product_qty;

            //     $sales  = $sal_qty + $qty;
            //     $blc    = $blc_qty - $qty;

            //     if($ord_value->FOC != 0 && $ord_value->spl_FOC != 0){

            //         $sales = $sales + $ord_value->FOC +$ord_value->spl_FOC;
            //         $blc = $blc - $ord_value->FOC - $ord_value->spl_FOC;

            //     }elseif($ord_value->spl_FOC != 0){

            //         $sales = $sales + $ord_value->spl_FOC;
            //         $blc = $blc - $ord_value->spl_FOC;

            //     }elseif ($ord_value->FOC != 0){

            //         $sales = $sales + $ord_value->FOC;
            //         $blc = $blc - $ord_value->FOC;

            //     }

            //     if($blc <= 0){
            //         $product_status = 2;
            //     }else{
            //         $product_status = 1;
            //     }

            //     $value = array(
            //         'product_blc_qty'       => $blc ,
            //         'product_sales_qty'     => $sales ,
            //         'product_status'        => $product_status ,
            //         'updated_at'            => date('Y-m-d H:i:s')
            //     );

            //     DB::table('gpff_product_batch_stock')
            //     ->where('product_id', $ord_value->product_id)
            //     ->where('batch_id', $ord_value->batch_no)
            //     ->where('warehouse_id', $ware->warehouse_id)
            //     ->update($value);
            // }

            $update_values = array(
                'order_status'  => $Orders->order_status ,
                'updated_at'    => date('Y-m-d H:i:s')
            );

            $event_values = array(
                'order_id'    => $Orders->order_id, 
                'event_name'  => "The order has been Packaged" ,
                'event_date'  => now() ,
                'order_status'  => $Orders->order_status ,
                'created_at'  => date('Y-m-d H:i:s') , 
                'updated_at'  => date('Y-m-d H:i:s')
            );
        } else if ($Orders->order_status == 1) {
            $update_values = array(
                'order_status'  => $Orders->order_status ,
                'updated_at'    => date('Y-m-d H:i:s')
            );

            $event_values = array(
                'order_id'    => $Orders->order_id, 
                'event_name'  => "The order is in-progress" ,
                'event_date'  => now() ,
                'order_status'  => $Orders->order_status ,
                'created_at'  => date('Y-m-d H:i:s') , 
                'updated_at'  => date('Y-m-d H:i:s')
            );
        } else if ($Orders->order_status == 3) {
            $update_values = array(
                'order_status'  => $Orders->order_status ,
                'updated_at'    => date('Y-m-d H:i:s')
            );

            $event_values = array(
                'order_id'    => $Orders->order_id, 
                'event_name'  => "your order has been Delivered" ,
                'event_date'  => now() ,
                'order_status'  => $Orders->order_status ,
                'created_at'  => date('Y-m-d H:i:s') , 
                'updated_at'  => date('Y-m-d H:i:s')
            );
        }else if ($Orders->order_status == 4) {
            $update_values = array(
                'reject_reason' => $Orders->reject_reason,
                'order_status'  => $Orders->order_status ,
                'order_rej_date'  => date('Y-m-d H:i:s') ,
                'updated_at'    => date('Y-m-d H:i:s')
            );

            $event_values = array(
                'order_id'    => $Orders->order_id, 
                'event_name'  => "The order has been Rejected" ,
                'event_date'  => now() ,
                'order_status'  => $Orders->order_status ,
                'created_at'  => date('Y-m-d H:i:s') , 
                'updated_at'  => date('Y-m-d H:i:s')
            );
        }else if ($Orders->order_status == 6) {
            $update_values = array(
                'order_status'  => $Orders->order_status ,
                'updated_at'    => date('Y-m-d H:i:s')
            );


            $update_values1 = array(
                'status'  => 1 ,
                'updated_at'    => date('Y-m-d H:i:s')
            );

            DB::table('gpff_cus_overlimit_order_req')
                ->where('order_id', $Orders->order_id)
                ->update($update_values1);

            $cuslim    =   DB::table('gpff_customer')
                        ->where('customer_id', $Orders->customer_id)
                        ->First('credit_limit');

            $val = $cuslim->credit_limit - $Orders->or_tot_price;

            if($val < 0){
                $update_values2 = array(
                    'credit_limit'  => 0 ,
                    'updated_at'    => date('Y-m-d H:i:s')
                );
            }else{
                $update_values2 = array(
                    'credit_limit'  => $val ,
                    'updated_at'    => date('Y-m-d H:i:s')
                );
            }
            DB::table('gpff_customer')
                ->where('customer_id', $Orders->customer_id)
                ->update($update_values2);

            $event_values = array(
                'order_id'    => $Orders->order_id, 
                'event_name'  => "The order has been Accepted by Admin" ,
                'event_date'  => now() ,
                'order_status'  => $Orders->order_status ,
                'created_at'  => date('Y-m-d H:i:s') , 
                'updated_at'  => date('Y-m-d H:i:s')
            );

        }else if ($Orders->order_status == 7) {

            $update_values = array(
                'reject_reason' => $Orders->reject_reason,
                'order_status'  => $Orders->order_status ,
                'updated_at'    => date('Y-m-d H:i:s')
            );

            $update_values1 = array(
                'remark' => $Orders->reject_reason,
                'status'  => 2 ,
                'updated_at'    => date('Y-m-d H:i:s')
            );

            DB::table('gpff_cus_overlimit_order_req')
                ->where('order_id', $Orders->order_id)
                ->update($update_values1);

            $event_values = array(
                'order_id'    => $Orders->order_id, 
                'event_name'  => "The order has been Rejected by Admin" ,
                'event_date'  => now() ,
                'order_status'  => $Orders->order_status ,
                'created_at'  => date('Y-m-d H:i:s') , 
                'updated_at'  => date('Y-m-d H:i:s')
            );
        }

        DB::table('gpff_order_events')
        ->insert($event_values);

        return  DB::table('gpff_order')
                ->where('order_id', $Orders->order_id)
                ->update($update_values);
    }

    // Fetch WareHouse Based Order Details 
    public function getPlacedOrder($Orders) 
    {
        $data =[];

        if($Orders->type == 0){
            $data['totaldata']= DB::table('gpff_order')
                        ->whereBetween(DB::RAW('date(created_at)'), [date($Orders->start_date), date($Orders->end_date)])
                        ->where('warehouse_id', $Orders->warehouse_id)
                        ->whereIn('order_status', [0,5])
                        ->orderBy('updated_at','DESC')
                        ->get();    
        }else{
            $data['totaldata']= DB::table('gpff_order')
                        ->whereBetween(DB::RAW('date(created_at)'), [date($Orders->start_date), date($Orders->end_date)])
                        ->where('warehouse_id', $Orders->warehouse_id)
                        ->where('order_status', $Orders->type)
                        ->orderBy('updated_at','DESC')
                        ->get();    
        }        
         return $data;   
    }

    // Fetch WareHouse Based  all Order Details 
    // public function getwarehouseOrder($Orders) 
    // {
    //     //Placed Order Details
    //     return  DB::table('gpff_order')
    //                     ->where('warehouse_id', $Orders->warehouse_id)
    //                     ->orderBy('updated_at','DESC')
    //                     ->get();    
    // }

    public function getwarehouseOrder($Orders) 
    {
        //Placed Order Details
        // print_r($Orders->start_date);
        $values =  DB::table('gpff_order')
                    ->where('warehouse_id', $Orders->warehouse_id)
                    ->whereBetween(DB::RAW('date(invoice_date)'), [date($Orders->start_date), date($Orders->end_date)])
                    ->whereNotIn('order_status',[0])
                    ->orderBy('invoice_date','DESC')
                    ->get();

        return $values;
    }


    // ORDER ACCEPT
    public function orderAccept($Orders) 
    {
        $products = $Orders->products;
        $order_id = $Orders->order_id;

        if($Orders->order_status == 1){
            // ORDER STATUS UPDATED IN GPFF_ORDER TABLE
            $value1 = array(
                    'order_status'       => $Orders->order_status,
                    'acc_or_tot_price'       => $Orders->grand_total,
                    'tot_box' => $Orders->tot_box,
                    'updated_at' => date('Y-m-d H:i:s')
                );

             DB::table('gpff_order')
            ->where('order_id', $order_id)
            ->update($value1);
            // print_r($product);
            // exit();

            // ORDER DETAILS ACCEPTED IN GPFF_ORDER_LIST TABLE
            foreach ($products as $product) {
                $value = array(
                    'accepted_qty'       => $product['quantity'] ,
                    'accepted_total_price'     => $product['total_price'] ,
                    'updated_at'            => date('Y-m-d H:i:s')
                );

             DB::table('gpff_order_list')
            ->where('product_id', $product['product_id'])
            ->where('order_id', $Orders->order_id)
            ->update($value);
                
            }

            // $this->stockReduceBasedOrder($order_id);

            return 1;
        }
    }

    public function cusOverlimitOrderReq($Orders){

         $values = array(
            'order_id'           => $Orders->order_id , 
            'customer_id'   => $Orders->customer_id ,
            'or_tot_price'  => $Orders->or_tot_price ,
            'credit_limit'   => $Orders->credit_limit ,
            'stokist_id'       => $Orders->stokist_id ,
            'warehouse_id'      => $Orders->warehouse_id,
            'branch_id'         => $Orders->branch_id ,
            'region_id'         => $Orders->region_id ,
            'area_id'           => $Orders->area_id ,
            'created_at'        => date('Y-m-d H:i:s') , 
            'updated_at'        => date('Y-m-d H:i:s')
        );

        $order_id   =   DB::table('gpff_cus_overlimit_order_req')
                        ->insertGetId($values);

        $value1 = array(
                    'order_status'       => 5,
                    'updated_at'        => date('Y-m-d H:i:s')
                );

         DB::table('gpff_order')
        ->where('order_id', $Orders->order_id)
        ->update($value1);

        $event_values = array(
                'order_id'    => $Orders->order_id, 
                'event_name'  => "The order send to the admin" ,
                'event_date'  => now() ,
                'order_status'  => 5 ,
                'created_at'  => date('Y-m-d H:i:s') , 
                'updated_at'  => date('Y-m-d H:i:s')
            );
            
        DB::table('gpff_order_events')
        ->insert($event_values);

        return 1;
    }

    //Get Limit Overdue Order Request
    public function getLimitOverData()
    {
        return  DB::table('gpff_cus_overlimit_order_req as gcoor')
                ->leftjoin('gpff_customer as cus','gcoor.customer_id','cus.customer_id')
                ->leftjoin('gpff_warehouse as gpwar','gcoor.warehouse_id','gpwar.warehouse_id')
                ->leftjoin('gpff_region as gpre','gcoor.region_id','gpre.region_id')
                ->leftjoin('gpff_branch as gpba','gcoor.branch_id','gpba.branch_id')
                ->leftjoin('gpff_area as gpar','gcoor.area_id','gpar.area_id')
                ->orderBy('gcoor.updated_at','DESC')
                ->where('gcoor.status',0)
                ->get(['gcoor.gpff_cus_overlimit_order_req_id','gcoor.customer_id','cus.customer_name','gcoor.order_id','gcoor.or_tot_price','gcoor.credit_limit','gcoor.stokist_id','gpwar.warehouse_id','gpwar.warehouse_name','gcoor.branch_id','gpba.branch_name','gcoor.region_id','gpre.region_name','gcoor.area_id','gpar.area_name','gcoor.status','gcoor.remark','gcoor.created_at','gcoor.updated_at']);
    }

    //Get Limit Overdue Order Request History
    public function getHistoryLimitOverData($Orders)
    {   
        $userdata = DB::table('gpff_users')
                    ->where('user_id', $Orders->user_id)
                    ->First();

        $myArray = explode(',', $userdata->branch_id);

        return  DB::table('gpff_cus_overlimit_order_req as gcoor')
                ->leftjoin('gpff_customer as cus','gcoor.customer_id','cus.customer_id')
                ->leftjoin('gpff_warehouse as gpwar','gcoor.warehouse_id','gpwar.warehouse_id')
                ->leftjoin('gpff_region as gpre','gcoor.region_id','gpre.region_id')
                ->leftjoin('gpff_branch as gpba','gcoor.branch_id','gpba.branch_id')
                ->leftjoin('gpff_area as gpar','gcoor.area_id','gpar.area_id')
                ->orderBy('gcoor.updated_at','DESC')
                ->whereNotIn('gcoor.status',[0])
                ->whereIn('gcoor.branch_id', $myArray)
                ->get(['gcoor.gpff_cus_overlimit_order_req_id','gcoor.customer_id','cus.customer_name','gcoor.order_id','gcoor.or_tot_price','gcoor.credit_limit','gcoor.stokist_id','gpwar.warehouse_id','gpwar.warehouse_name','gcoor.branch_id','gpba.branch_name','gcoor.region_id','gpre.region_name','gcoor.area_id','gpar.area_name','gcoor.status','gcoor.remark','gcoor.created_at','gcoor.updated_at']);
    }

    //Admin Get Approval Order Details
    public function getApprovalOrderData()
    {
        return  DB::table('gpff_order as gpord')
                ->join('gpff_warehouse as gpwah', 'gpord.warehouse_id', 'gpwah.warehouse_id')
                ->orderBy('created_at','DESC')
                ->where('order_status',5)
                ->get(['gpord.order_id','gpord.task_id', 'gpord.field_officer_id', 'gpord.field_officer_name', 'gpord.area_manager_id', 'gpord.customer_id', 'gpord.customer_name', 'gpord.order_date', 'gpord.or_gross_total', 'gpord.or_tot_price', 'gpord.acc_or_tot_price','gpord.order_discount','gpord.spl_discount','gpord.branch_id','gpord.region_id','gpord.area_id','gpord.stokist_id','gpord.warehouse_id','gpwah.warehouse_name','gpord.order_rej_date','gpord.order_status','gpord.admin_cmt', 'gpord.invoice_type','gpord.invoice_no','gpord.reject_reason','gpord.net_or_gross_price','gpord.payment_type','gpord.payment_img','gpord.payment_name','gpord.credit_lim', 'gpord.credit_valid_to','gpord.or_type','gpord.payment_msg','gpord.created_at','gpord.updated_at']);
    }
    // ORDER Spl Foc or Discount Approve
    public function orderFOCDiscountAccept($Orders) 
    {   
        $pdf_name = "GPFF_OR_".$Orders->order_id.'.pdf';
        $invoice_date = date('Y-m-d H:i:s');
        //SPL FOC Update 
        foreach ($Orders->products_list as $product) {

            $data = DB::table('gpff_order')
                ->where('order_id', $Orders->order_id)
                ->First();

            $value = array(
                'spl_FOC'       => $product['spl_FOC'] ,
                'updated_at'            => date('Y-m-d H:i:s')
            );

            DB::table('gpff_order_list_batchwise')
            ->where('product_id', $product['product_id'])
            ->where('batch_no', $product['batch_no'])
            ->where('order_id', $Orders->order_id)
            ->update($value);

            $blc_qty  =  DB::table('gpff_product_batch_stock')
                        ->where('product_id', $product['product_id'])
                        ->where('batch_id', $product['batch_no'])
                        ->where('warehouse_id', $data->warehouse_id)
                        ->sum('product_blc_qty');
            // print_r($blc_qty);

            $sal_qty  =  DB::table('gpff_product_batch_stock')
                        ->where('product_id', $product['product_id'])
                        ->where('batch_id', $product['batch_no'])
                        ->where('warehouse_id', $data->warehouse_id)
                        ->sum('product_sales_qty');

            $spl_foc_qty  =  DB::table('gpff_product_batch_stock')
                            ->where('product_id', $product['product_id'])
                            ->where('batch_id', $product['batch_no'])
                            ->where('warehouse_id', $data->warehouse_id)
                            ->sum('product_sale_spl_FOC_qty');

            $qty2 = $product['spl_FOC'];

            //$sales = $sal_qty + $product['spl_FOC'];
            $blc = $blc_qty - $product['spl_FOC'];

            $spl_foc = $spl_foc_qty + $qty2;

            $dat_values = array(
                'product_sale_spl_FOC_qty'=> $spl_foc,
                'product_blc_qty'         => $blc ,
                'updated_at'              => date('Y-m-d H:i:s')
            );

            DB::table('gpff_product_batch_stock')
            ->where('product_id', $product['product_id'])
            ->where('batch_id', $product['batch_no'])
            ->where('warehouse_id', $data->warehouse_id)
            ->update($dat_values);

        }

        // $data = DB::table('gpff_order')
        //         ->where('order_id', $Orders->order_id)
        //         ->First();

        $value1 = array(
                    'spl_discount'       => $Orders->spl_discount ,
                    'admin_cmt'         => $Orders->admin_cmt ,
                    'invoice_type'      => 1,
                    'invoice_no'        => $pdf_name,
                    'invoice_date'      => $invoice_date,
                    'order_status'      => 6 ,
                    'updated_at'         => date('Y-m-d H:i:s')
                );

        if($Orders->spl_discount == $data->spl_discount){
            $value1 = array(
                    'admin_cmt'         => $Orders->admin_cmt ,
                    'invoice_type'      => 1,
                    'invoice_no'        => $pdf_name,
                    'invoice_date'      => $invoice_date,
                    'order_status'      => 6 ,
                    'updated_at'        => date('Y-m-d H:i:s')
            );
        }elseif($Orders->spl_discount > $data->spl_discount){
            $dis= $Orders->spl_discount - $data->spl_discount;
            $value1 = array(
                    'or_tot_price'       => $data->or_tot_price - $dis,
                    'spl_discount'       => $Orders->spl_discount ,
                    'invoice_type'      => 1,
                    'invoice_no'        => $pdf_name,
                    'invoice_date'      => $invoice_date,
                    'admin_cmt'         => $Orders->admin_cmt ,
                    'order_status'  => 6 ,
                    'updated_at'         => date('Y-m-d H:i:s')
            );
        }else{
            $dis= $data->spl_discount - $Orders->spl_discount;
            $value1 = array(
                    'or_tot_price'       => $data->or_tot_price + $dis,
                    'spl_discount'       => $Orders->spl_discount ,
                    'admin_cmt'         => $Orders->admin_cmt ,
                    'invoice_type'      => 1,
                    'invoice_no'        => $pdf_name,
                    'invoice_date'      => $invoice_date,
                    'order_status'  => 6 ,
                    'updated_at'         => date('Y-m-d H:i:s')
            );
        }

        //SPL_DISC Update
        DB::table('gpff_order')
        ->where('order_id', $Orders->order_id)
        ->update($value1);

        $event_values = array(
                'order_id'    => $Orders->order_id, 
                'event_name'  => "The order has been Accepted by Admin" ,
                'event_date'  => now() ,
                'order_status'  => 6 ,
                'created_at'  => date('Y-m-d H:i:s') , 
                'updated_at'  => date('Y-m-d H:i:s')
            );

        DB::table('gpff_order_events')
        ->insert($event_values);
        if($data->or_type == 2 || $data->or_type == 1){


            $order_details = DB::table('gpff_order')
                            ->where('order_id',$Orders->order_id)
                            ->First();

            // print_r(expression)

            $cus_de = DB::table('gpff_customer')
                            ->where('customer_id',$order_details->customer_id)
                            ->First();

            $com_details = DB::table('gpff_branch')
                            ->where('branch_id',$order_details->branch_id)
                            ->First();

            $warehouse = DB::table('gpff_warehouse')
                    ->where('warehouse_id', $order_details->warehouse_id)
                    ->First();

            $orders_lists = DB::table('gpff_order_list as golis')
                            ->join('gpff_order_list_batchwise as gpolb', 'gpolb.order_list_id','golis.order_list_id')
                            // ->
                            ->where('golis.order_id',$Orders->order_id)
                            ->get(['golis.category_name', 'golis.product_genericname','gpolb.scheme','gpolb.batch_no','gpolb.product_netprice','gpolb.product_grossprice','gpolb.product_qty','gpolb.batch_emp_date','gpolb.FOC','gpolb.FOC_amt','gpolb.spl_FOC','gpolb.product_discount','gpolb.product_tot_price','gpolb.comments','gpolb.pro_amt_cal']);

            $number = $this->numbertostring($order_details->or_tot_price);

            $pdf_name = "GPFF_OR_".$Orders->order_id;

            //Invoice
            view()->share('datas',$orders_lists);
            view()->share('order_details',$order_details);
            view()->share('cus_de',$cus_de);
            view()->share('war', $warehouse);
            view()->share('com_details',$com_details);
            view()->share('invoice',$pdf_name);
            view()->share('value',$number);
            view()->share('date',Carbon::now());

            $pdf = \PDF::loadView('or_invoice_sa')
            ->setPaper('a4', 'landscape');
            $pdf->save(public_path('Order_invoice/'.$pdf_name.'.pdf'));
            $file_name = $pdf_name.'.pdf';
            $names = $file_name;
            $filePath = 'Order_invoice/'.$names; 
      
            Storage::disk('s3')->put($filePath, file_get_contents(public_path('Order_invoice')."/".$file_name));
    }else{
        //Order Invoice
        $order_details = DB::table('gpff_order')
                        ->where('order_id',$Orders->order_id)
                        ->First();

        $warehouse = DB::table('gpff_warehouse')
                    ->where('warehouse_id', $order_details->warehouse_id)
                    ->First();

        // $order_list = DB::table('gpff_order_list as gol')
        //                 ->leftjoin('gpff_order_list_batchwise as gprl','gol.order_list_id','gprl.order_list_id')
        //                 ->where('gol.order_id',$Orders->order_id)
        //                 ->get();
        $order_list = DB::table('gpff_order_list as golis')
                            ->join('gpff_order_list_batchwise as gpolb', 'gpolb.order_list_id','golis.order_list_id')
                            // ->
                            ->where('golis.order_id',$Orders->order_id)
                            ->get(['golis.category_name', 'golis.product_genericname','gpolb.scheme','gpolb.batch_no','gpolb.product_netprice','gpolb.product_grossprice','gpolb.product_qty','gpolb.batch_emp_date','gpolb.FOC','gpolb.FOC_amt','gpolb.spl_FOC','gpolb.product_discount','gpolb.product_tot_price','gpolb.comments','gpolb.pro_amt_cal']);

        $cus_de = DB::table('gpff_customer')
                        ->where('customer_id',$order_details->customer_id)
                        ->First();

        $com_details = DB::table('gpff_branch')
                        ->where('branch_id',$order_details->branch_id)
                        ->First();

        $number = $this->numbertostring($order_details->or_tot_price);

        $pdf_name = "GPFF_OR_".$Orders->order_id;
        //Invoice
        view()->share('datas',$order_list);
        view()->share('order_details',$order_details);
        view()->share('cus_de',$cus_de);
        view()->share('war', $warehouse);
        view()->share('com_details',$com_details);
        view()->share('invoice',$pdf_name);
        view()->share('value',$number);
        view()->share('date',Carbon::now());

        $pdf = \PDF::loadView('or_invoice_sa')
        ->setPaper('a4', 'landscape');
        $pdf->save(public_path('Order_invoice/'.$pdf_name.'.pdf'));
        $file_name = $pdf_name.'.pdf';
        $name = $file_name;
        $filePath = 'Order_invoice/'.$name; 
  
        Storage::disk('s3')->put($filePath, file_get_contents(public_path('Order_invoice')."/".$file_name));
    }
        return 1;
    }

    public function custmerOrderDetails($Orders){

        $name = DB::table('gpff_customer')
                ->where('customer_id', $Orders->customer_id)
                ->First(['customer_name','customer_lan','customer_lat','region_id']);
        if($Orders->or_type == 2){

            if($name->customer_lan != '' && $name->customer_lat != '')
            {
                $war = DB::select('SELECT * , (3956 * 2 * ASIN(SQRT( POWER(SIN(( '.$name->customer_lat.' - warehouse_lat) *  pi()/180 / 2), 2) +COS( '.$name->customer_lat.' * pi()/180) * COS(warehouse_lat * pi()/180) * POWER(SIN(( '.$name->customer_lan.' - warehouse_lan) * pi()/180 / 2), 2) ))) as distance  from gpff_warehouse  having  distance <= 10000 and region_id = '.$name->region_id.' order by distance');

            }else{
                return 2;
            }

            DB::table('gpff_cus_cart')
            ->where('customer_id', $Orders->customer_id)
            ->delete();

            $values = array(
                'customer_id'       => $Orders->customer_id ,
                'branch_id'         => $Orders->branch_id ,
                'region_id'         => $Orders->region_id ,
                'area_id'           => $Orders->area_id ,
                'or_tot_price'      => $Orders->or_tot_price ,
                'or_blc_price'      => $Orders->or_tot_price ,
                'customer_name'     => $name->customer_name ,
                'warehouse_id'      => $war[0]->warehouse_id,
                'order_date'        => $Orders->order_date , 
                'order_created_by'  => $Orders->order_created_by,
                'payment_type'      => $Orders->payment_type,
                'payment_img'       => $Orders->payment_img ,
                'net_or_gross_price'=> $Orders->net_or_gross_price,
                'spl_discount'       => $Orders->spl_discount ,
                'or_type'           => 2,
                'created_at'        => date('Y-m-d H:i:s') , 
                'updated_at'        => date('Y-m-d H:i:s')
            );
        }else{
            $values = array(
                'task_id'           => $Orders->task_id , 
                'field_officer_id'  => $Orders->field_officer_id ,
                'field_officer_name'=> $Orders->field_officer_name ,
                'area_manager_id'   => $Orders->area_manager_id ,
                'customer_id'       => $Orders->customer_id ,
                'customer_name'     => $name->customer_name , 
                'warehouse_id'      => $Orders->warehouse_id,
                'payment_type'      => $Orders->payment_type,
                'order_date'        => $Orders->order_date , 
                'or_gross_total'    => $Orders->or_gross_total ,
                'or_tot_price'      => $Orders->or_tot_price ,
                'or_blc_price'      => $Orders->or_tot_price ,
                'order_discount'    => $Orders->order_discount ,
                'order_created_by'  => $Orders->order_created_by,
                'branch_id'         => $Orders->branch_id ,
                'region_id'         => $Orders->region_id ,
                'area_id'           => $Orders->area_id , 
                'telecalling_id'    => $Orders->telecalling_id,
                'telecalling_type'  => $Orders->telecalling_type,               
                'or_type'           => $Orders->or_type,
                // 'net_or_gross_price'=> $Orders->net_or_gross_price,
                'credit_lim'        => $Orders->credit_lim,
                'credit_valid_to'   => $Orders->credit_valid_to,
                'spl_discount'      => $Orders->spl_discount ,
                'created_at'        => date('Y-m-d H:i:s') , 
                'updated_at'        => date('Y-m-d H:i:s')
            );
        }

        $order_id   =   DB::table('gpff_order')
                        ->insertGetId($values);

        if($Orders->telecalling_id != ''){
            $feedback = "Order Taken - Order ID: ".$order_id;
            $tele_value = array(
                // 'events'            => $Orders->events,
                'telecalling_type'  => $Orders->telecalling_type,
                'order_id'          => $order_id,
                'feedback' => $feedback,
                'status'            =>1,
                'updated_at'        => date('Y-m-d H:i:s')
            );

            DB::table('gpff_telecalling')
            ->where('telecalling_id', $Orders->telecalling_id)
            ->update($tele_value);
        }
        if($Orders->task_id != ''){
            $feedback = "Order Taken - Order ID: ".$order_id;
            $tele_value = array(
                'feedback' => $feedback,
                'task_status' =>3,
                'updated_at'        => date('Y-m-d H:i:s')
            );

            DB::table('gpff_task')
            ->where('task_id', $Orders->task_id)
            ->update($tele_value);
        }

        $SP_FOC = 0;
        if($Orders->or_type == 2){
        foreach($Orders->order_list as $order_lists){
            $list_value = array(
                    'order_id'              => $order_id,
                    'category_id'           => $order_lists['category_id'],
                    'category_name'         => $order_lists['category_name'],
                    'product_id'            => $order_lists['product_id'],
                    'product_genericname'   => $order_lists['product_genericname'],
                    'product_qty'           => $order_lists['product_qty'],
                    'product_tot_price'     => $order_lists['product_tot_price'],
                    'product_type'          => $order_lists['product_type'],
                    'created_at'            => date('Y-m-d H:i:s') ,
                    'updated_at'            => date('Y-m-d H:i:s')
                );
                $order_list_id = DB::table('gpff_order_list')
                                ->insert($list_value);
        }
    }else{
        foreach($Orders->order_list as $order_lists){
            $list_value = array(
                    'order_id'              => $order_id,
                    'category_id'           => $order_lists['category_id'],
                    'category_name'         => $order_lists['category_name'],
                    'product_id'            => $order_lists['product_id'],
                    'product_genericname'   => $order_lists['product_genericname'],
                    'product_qty'           => $order_lists['product_qty'],
                    'product_tot_price'     => $order_lists['product_tot_price'],
                    'product_type'          => $order_lists['product_type'],
                    'created_at'            => date('Y-m-d H:i:s') ,
                    'updated_at'            => date('Y-m-d H:i:s')
                );
                    $order_list_id = DB::table('gpff_order_list')
                                    ->insertGetId($list_value);
            $field_list_value = array(
                    'order_list_id'         => $order_list_id,
                    'order_id'              => $order_id,
                    'scheme'                => $order_lists['scheme'],
                    'FOC'                   => $order_lists['FOC'],
                    'FOC_amt'               => $order_lists['FOC_amt'],
                    'spl_FOC'               => $order_lists['spl_FOC'],
                    'pro_amt_cal'           => $order_lists['pro_amt_cal'],
                    'product_netprice'      => $order_lists['product_netprice'],
                    'product_grossprice'    => $order_lists['product_grossprice'],
                    'product_discount'      => $order_lists['product_discount'],
                    'comments'              => $order_lists['comments'],
                    'created_at'            => date('Y-m-d H:i:s') ,
                    'updated_at'            => date('Y-m-d H:i:s')
                );
             $field_officer_list_id = DB::table('gpff_order_foc')
                                    ->insert($field_list_value);
        }
        // foreach ($Orders->field_officer_list as $field_officer_lists){

        //     $field_list_value = array(
        //         'order_id'              => $order_id,
        //         'scheme'                => $field_officer_lists['scheme'],
        //         'FOC'                   => $field_officer_lists['FOC'],
        //         'FOC_amt'               => $field_officer_lists['FOC_amt'],
        //         'spl_FOC'               => $field_officer_lists['spl_FOC'],
        //         'pro_amt_cal'           => $field_officer_lists['pro_amt_cal'],
        //         'product_netprice'      => $field_officer_lists['product_netprice'],
        //         'product_grossprice'    => $field_officer_lists['product_grossprice'],
        //         'product_discount'      => $field_officer_lists['product_discount'],
        //         'comments'              => $field_officer_lists['comments'],
        //         'created_at'            => date('Y-m-d H:i:s'),
        //         'updated_at'            => date('Y-m-d H:i:s')
        //     );
        //     $field_officer_list_id = DB::table('gpff_order_foc')
        //                             ->insert($field_list_value);
        // }
    }
        $event_values = array(
                'order_id'      => $order_id, 
                'event_name'    => "The order has been Placed" ,
                'event_date'    => now() ,
                'order_status'  => 0 ,
                'created_at'    => date('Y-m-d H:i:s') , 
                'updated_at'    => date('Y-m-d H:i:s')
            );

            DB::table('gpff_order_events')
            ->insert($event_values);

        $cmn = new Common();

        if($Orders->or_type == 2){

            $waremessage = "Customer ".$name->customer_name." has placed one Order to your WareHouse.";

            $warename = DB::table('gpff_users')
                    ->where('warehouse_id', $war[0]->warehouse_id)
                    ->First();

            $page_id = 'UNKNOWN';

            $cmn->insertNotification($Orders->customer_id,$name->customer_name,$warename->user_id,$waremessage,$page_id);
        }
        return 1;
}

public function acceptCustomerOrderdetails($Orders){
    $order_id = $Orders->order_id;
    $order_status = $Orders->order_status;
    $balance_check = $this->getStockBalanceCheck($Orders);
    if($balance_check == 2){
        return 3;
    }
    $SP_FOC = 0;

    if($order_status ==1){
        $order_batch = DB::table('gpff_order_list_batchwise')
                        ->where('order_id', $order_id)
                        ->get();
        if(Count($order_batch) > 0){
            return 2;
        }
        foreach($Orders->order_list as $order_lists) { 
            $exit = DB::table('gpff_order_list')
                    ->where('category_id', $order_lists['category_id'])
                    ->where('product_id', $order_lists['product_id'])
                    ->where('order_id', $order_id)
                    ->First();
            $SP_FOC = $SP_FOC + $order_lists['spl_FOC'];

            $value = array(
                'product_qty'        => $order_lists['product_qty'] ,
                'accepted_qty'       => $order_lists['product_qty'] ,
                'product_tot_price'  => $order_lists['product_tot_price'],
                'updated_at'        => date('Y-m-d H:i:s')
            );

            DB::table('gpff_order_list')
            ->where('category_id', $order_lists['category_id'])
            ->where('product_id', $order_lists['product_id'])
            ->where('order_id', $order_id)
            ->update($value);

            $list_value_batch = array(
                'order_id'              => $order_id,
                'order_list_id'         => $exit->order_list_id,
                'category_id'           => $order_lists['category_id'],
                'product_id'            => $order_lists['product_id'],
                'batch_no'              => $order_lists['batch_no'],
                'batch_emp_date'        => $order_lists['batch_emp_date'],    
                'product_qty'           => $order_lists['product_qty'],
                'product_netprice'      => $order_lists['product_netprice'],
                'product_grossprice'    => $order_lists['product_grossprice'],
                'product_discount'      => $order_lists['product_discount'],
                'product_tot_price'     => $order_lists['product_tot_price'],
                'product_type'          => $order_lists['product_type'],
                'scheme'                => $order_lists['scheme'],
                'comments'              => $order_lists['comments'],
                'FOC'                   => $order_lists['FOC'],
                'FOC_amt'               => $order_lists['FOC_amt'],
                'spl_FOC'               => $order_lists['spl_FOC'],
                'pro_amt_cal'           => $order_lists['pro_amt_cal'],
                'created_at'            => date('Y-m-d H:i:s') ,
                'updated_at'            => date('Y-m-d H:i:s')
            );
            DB::table('gpff_order_list_batchwise')
            ->insert($list_value_batch);
        }
        if($Orders->or_type == 2){
            $val_order_details = array(
                'field_officer_id'=>$Orders->field_officer_id,
                'field_officer_name' =>$Orders->field_officer_name,
                'biller_id'=>$Orders->biller_id,
                'biller_name' =>$Orders->biller_name,
                'spl_discount' => $Orders->spl_discount,
                'tot_box'      => $Orders->tot_box,
                'or_tot_price' => $Orders->or_tot_price,
                'or_blc_price' => $Orders->or_tot_price,
                'or_gross_total' => $Orders->or_gross_total,
                'order_discount' => $Orders->order_discount,
                'credit_valid_to' => $Orders->credit_valid_to,
                'credit_lim'    => $Orders->credit_lim
            );
        }else{
            $val_order_details = array(
                'biller_id'=>$Orders->biller_id,
                'biller_name' =>$Orders->biller_name,
                'spl_discount' => $Orders->spl_discount,
                'tot_box'      => $Orders->tot_box,
                'or_tot_price' => $Orders->or_tot_price,
                'or_blc_price' => $Orders->or_tot_price,
                'or_gross_total' => $Orders->or_gross_total,
                'credit_valid_to' => $Orders->credit_valid_to,
                'order_discount' => $Orders->order_discount
            );
        }
        DB::table('gpff_order')
            ->where('order_id', $order_id)
            ->update($val_order_details);
            
        $orders_details = DB::table('gpff_order')
                        ->where('order_id', $order_id)
                        ->First();

        $this->stockReduceBasedOrder($order_id);

            
        if($SP_FOC == 0 && $orders_details->spl_discount == 0){

            $event_values = array(
                'order_id'    => $order_id, 
                'event_name'  => "The order has been in-progress" ,
                'event_date'  => now() ,
                'order_status'  => 1 ,
                'created_at'  => date('Y-m-d H:i:s') , 
                'updated_at'  => date('Y-m-d H:i:s')
            );

            DB::table('gpff_order_events')
            ->insert($event_values);
            $orvalue1 = array(
                    'order_status'       => 1,
                    'tot_box' => $Orders->tot_box,
                    'updated_at'        => date('Y-m-d H:i:s')
                );

             DB::table('gpff_order')
            ->where('order_id', $order_id)
            ->update($orvalue1);

            $pdf_name = "GPFF_OR_".$order_id;
            $file_name = $pdf_name.'.pdf';

            $value = array(
                    'invoice_no'        => $file_name ,
                    'invoice_type'      => 1,
                    'invoice_date'      => date('Y-m-d H:i:s'),
                    'updated_at'        => date('Y-m-d H:i:s')
                );

            DB::table('gpff_order')
            ->where('order_id',$order_id)
            ->update($value);

            $cmn = new Common();
            $invoice_details =  array(
                'order_id' => $order_id,
                'order_type' => 1,
                'invoice_url' => $file_name,
                'invoice_log_status' => 0,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
              );
            $cmn->insertInvoiceQueue($invoice_details);
    

            // $this->stockReduceBasedOrder($order_id);

            // $order_details = DB::table('gpff_order')
            //                 ->where('order_id',$order_id)
            //                 ->First();

            // $cus_de = DB::table('gpff_customer')
            //                 ->where('customer_id',$order_details->customer_id)
            //                 ->First();

            // $com_details = DB::table('gpff_branch')
            //                 ->where('branch_id',$order_details->branch_id)
            //                 ->First();

            // $warehouse = DB::table('gpff_warehouse')
            //             ->where('warehouse_id', $order_details->warehouse_id)
            //             ->First();

            // $orders_lists = DB::table('gpff_order_list as golis')
            //                 ->join('gpff_order_list_batchwise as gpolb', 'gpolb.order_list_id','golis.order_list_id')
            //                 ->where('golis.order_id',$order_id)
            //                 ->get(['golis.category_name', 'golis.product_genericname','gpolb.scheme','gpolb.batch_no','gpolb.product_netprice','gpolb.product_grossprice','gpolb.product_qty','gpolb.batch_emp_date','gpolb.FOC','gpolb.FOC_amt','gpolb.spl_FOC','gpolb.product_discount','gpolb.product_tot_price','gpolb.comments','gpolb.pro_amt_cal']);

            // $number = $this->numbertostring($order_details->or_tot_price);

            // //Invoice
            // view()->share('datas',$orders_lists);
            // view()->share('order_details',$order_details);
            // view()->share('cus_de',$cus_de);
            // view()->share('war', $warehouse);
            // view()->share('com_details',$com_details);
            // view()->share('invoice',$pdf_name);
            // view()->share('value',$number);
            // view()->share('date',Carbon::now());

            // $pdf = \PDF::loadView('or_invoice_2')
            // ->setPaper('a4', 'landscape');
            // $pdf->save(public_path('Order_invoice/'.$pdf_name.'.pdf'));
            // $file_name = $pdf_name.'.pdf';
            // $names = $file_name;
            // $filePath = 'Order_invoice/'.$names; 
      
            // Storage::disk('s3')->put($filePath, file_get_contents(public_path('Order_invoice')."/".$file_name));
        }else{
            $orvalue1 = array(
                    'order_status'       => 5,
                    'updated_at'        => date('Y-m-d H:i:s')
                );

             DB::table('gpff_order')
            ->where('order_id', $order_id)
            ->update($orvalue1);

            $event_values = array(
                    'order_id'    => $order_id, 
                    'event_name'  => "The order send to the admin" ,
                    'event_date'  => now() ,
                    'order_status'  => 5 ,
                    'created_at'  => date('Y-m-d H:i:s') , 
                    'updated_at'  => date('Y-m-d H:i:s')
                );
                
            DB::table('gpff_order_events')
            ->insert($event_values);
        }
    InvoiceGenerate::dispatch();
    return 1;  
    }
}
    public function getOrderBeforeAdminApproval($Orders){
        $order_id = $Orders->order_id;

        $id = DB::table('gpff_order')
            ->where('order_id', $order_id)
            ->First(['customer_id', 'invoice_type','branch_id','warehouse_id']);

            $warehouse =  DB::table('gpff_branch as gb')
            ->join('gpff_warehouse as gw', 'gw.branch_id', 'gb.branch_id')
            ->where('gw.warehouse_id', $id->warehouse_id)
            ->where('gb.branch_id', $id->branch_id)
            ->get(['gw.warehouse_name','gw.warehouse_address','gw.warehouse_contact','gb.branch_name']);

        $customer  = DB::table('gpff_customer')
                        ->where('customer_id', $id->customer_id)
                        ->get();


        $product    =   DB::table('gpff_order_list_batchwise as golb')
                        ->leftjoin('gpff_order_list as gol','golb.order_list_id','gol.order_list_id')
                        ->where('golb.order_id', $order_id)
                        ->orderBy('golb.created_at','ASC')
                        ->get(['golb.order_id','golb.order_list_id','golb.category_id','gol.category_name','golb.product_id','gol.product_genericname','golb.batch_no','golb.batch_emp_date','golb.product_qty','golb.product_netprice','golb.product_grossprice','golb.or_gross_total','golb.product_discount','golb.product_tot_price','golb.product_type','golb.scheme','golb.comments','golb.FOC','golb.FOC_amt','golb.spl_FOC','golb.pro_amt_cal','golb.created_at','golb.updated_at']);

        $status    =    DB::table('gpff_order')
                        ->where('order_id', $order_id)
                        ->get(['order_status','order_date','order_rej_date','reject_reason','acc_or_tot_price','or_tot_price','or_gross_total','order_discount','payment_type','invoice_no','spl_discount', 'or_type']);

        $result = array(
                'customer'  =>  $customer,
                'product'   =>  $product,
                'status'    =>  $status,
                'warehouse'  => $warehouse
            );

        return $result;
    }

    public function getOrderBasedFOCDetails($Orders){

        $id = DB::table('gpff_order')
              ->where('order_id', $Orders->order_id)
              ->First(['customer_id', 'invoice_type','branch_id','warehouse_id']);

        $warehouse =  DB::table('gpff_branch as gb')
              ->join('gpff_warehouse as gw', 'gw.branch_id', 'gb.branch_id')
              ->where('gw.warehouse_id', $id->warehouse_id)
              ->where('gb.branch_id', $id->branch_id)
              ->get(['gw.warehouse_name','gw.warehouse_address','gw.warehouse_contact','gb.branch_name']);

        $customer    =   DB::table('gpff_customer')
                            ->where('customer_id', $id->customer_id)
                            ->get();


        $product    =   DB::table('gpff_order_foc as gporfoc')
                        ->leftjoin('gpff_order_list as gol','gporfoc.order_list_id','gol.order_list_id')
                        // ->join('gpff_order_foc as gporfoc', 'gporfoc.order_list_id','gol.order_list_id')
                        ->where('gporfoc.order_id', $Orders->order_id)
                        ->orderBy('gporfoc.created_at','ASC')
                        ->get(['gporfoc.order_id','gporfoc.order_list_id','gol.category_id','gol.category_name','gol.product_id','gol.product_genericname','gol.product_qty','gporfoc.product_netprice','gporfoc.product_grossprice','gporfoc.product_discount','gol.product_tot_price','gol.product_type','gporfoc.scheme','gporfoc.comments','gporfoc.FOC','gporfoc.FOC_amt','gporfoc.spl_FOC','gporfoc.pro_amt_cal','gporfoc.created_at','gporfoc.updated_at']);

        $status    =    DB::table('gpff_order')
                        ->where('order_id', $Orders->order_id)
                        ->get(['order_status','order_date','order_rej_date','reject_reason','acc_or_tot_price','or_tot_price','or_gross_total','order_discount','payment_type','invoice_type','invoice_no','spl_discount', 'or_type']);

        $result = array(
                'customer'  =>  $customer,
                'product'   =>  $product,
                'status'    =>  $status,
                'warehouse'  => $warehouse
            );

        return $result;
    }

    public function getOrderBatchFOCDetails($Orders){

        $id      = DB::table('gpff_order')
                      ->where('order_id', $Orders->order_id)
                      ->First(['customer_id', 'invoice_type', 'or_type','branch_id','warehouse_id']);
        // console.log()
        // print_r($id);
        $warehouse =  DB::table('gpff_branch as gb')
              ->join('gpff_warehouse as gw', 'gw.branch_id', 'gb.branch_id')
              ->where('gw.warehouse_id', $id->warehouse_id)
              ->where('gb.branch_id', $id->branch_id)
              ->get(['gw.warehouse_name','gw.warehouse_address','gw.warehouse_contact','gb.branch_name']);


        $customer    =   DB::table('gpff_customer')
                            ->where('customer_id', $id->customer_id)
                            ->get();

        if($id->or_type==1){
                // $product =  DB::table('gpff_order_foc as gporfoc')
                //         ->leftjoin('gpff_order_list as gol','gporfoc.order_list_id','gol.order_list_id')
                //         ->leftjoin('gpff_order_list_batchwise as gpolb','gporfoc.order_id','gpolb.order_id')
                //         ->where('gporfoc.order_id', $Orders->order_id)
                //         ->orderBy('gporfoc.created_at','ASC')
                //         ->get(['gporfoc.order_id','gporfoc.order_list_id','gol.category_id','gol.category_name','gol.product_id','gol.product_genericname','gol.product_qty','gporfoc.product_netprice','gporfoc.product_grossprice','gporfoc.product_discount','gol.product_tot_price','gol.product_type','gporfoc.scheme','gporfoc.comments','gporfoc.FOC','gporfoc.FOC_amt','gporfoc.spl_FOC','gporfoc.pro_amt_cal','gporfoc.created_at','gporfoc.updated_at', 'gpolb.batch_no', 'gpolb.batch_emp_date']);
                $product = DB::table('gpff_order_list_batchwise as golb')
                    ->leftjoin('gpff_order_list as gol','golb.order_list_id','gol.order_list_id')
                    ->where('golb.order_id', $Orders->order_id)
                    ->orderBy('golb.created_at','ASC')
                    ->get(['golb.order_id','golb.order_list_id','golb.category_id','gol.category_name','golb.product_id','gol.product_genericname','golb.batch_no','golb.batch_emp_date','golb.product_qty','golb.product_netprice','golb.product_grossprice','golb.or_gross_total','golb.product_discount','golb.product_tot_price','golb.product_type','golb.scheme','golb.comments','golb.FOC','golb.FOC_amt','golb.spl_FOC','golb.pro_amt_cal','golb.created_at','golb.updated_at']);
        }else{
            $product = DB::table('gpff_order_list_batchwise as golb')
                    ->leftjoin('gpff_order_list as gol','golb.order_list_id','gol.order_list_id')
                    ->where('golb.order_id', $Orders->order_id)
                    ->orderBy('golb.created_at','ASC')
                    ->get(['golb.order_id','golb.order_list_id','golb.category_id','gol.category_name','golb.product_id','gol.product_genericname','golb.batch_no','golb.batch_emp_date','golb.product_qty','golb.product_netprice','golb.product_grossprice','golb.or_gross_total','golb.product_discount','golb.product_tot_price','golb.product_type','golb.scheme','golb.comments','golb.FOC','golb.FOC_amt','golb.spl_FOC','golb.pro_amt_cal','golb.created_at','golb.updated_at']);
        }

         $status    =    DB::table('gpff_order')
                        ->where('order_id', $Orders->order_id)
                        ->get(['order_status','order_date','order_rej_date','reject_reason','acc_or_tot_price','or_tot_price','or_gross_total','order_discount','payment_type','invoice_type','invoice_no','spl_discount', 'or_type']);

        $result = array(
                'customer'  =>  $customer,
                'product'   =>  $product,
                'status'    =>  $status,
                'warehouse'  => $warehouse
            );

        return $result;
    }
    // Order Based Order Details
    public function getOrderBasedOrderList($Orders)
    {   
        return  DB::table('gpff_order')
                ->where('order_id', $Orders->order_id)
                ->get();
    }

    private function stockReduceBasedOrder($order_id){

        $ord_de  =  DB::table('gpff_order_list_batchwise')
                    ->where('order_id', $order_id)
                    ->get(['product_id','product_qty','batch_no', 'FOC', 'spl_FOC']);

        $ware = DB::table('gpff_order')
                ->where('order_id', $order_id)
                ->First();

        
        foreach ($ord_de as $ord_value) {
            
            $blc_qty  =  DB::table('gpff_product_batch_stock')
                        ->where('product_id', $ord_value->product_id)
                        ->where('batch_id', $ord_value->batch_no)
                        ->where('warehouse_id', $ware->warehouse_id)
                        ->sum('product_blc_qty');

            $sal_qty  =  DB::table('gpff_product_batch_stock')
                        ->where('product_id', $ord_value->product_id)
                        ->where('batch_id', $ord_value->batch_no)
                        ->where('warehouse_id', $ware->warehouse_id)
                        ->sum('product_sales_qty');
            
            // foc details fetched
            $foc_qty  =  DB::table('gpff_product_batch_stock')
                        ->where('product_id', $ord_value->product_id)
                        ->where('batch_id', $ord_value->batch_no)
                        ->where('warehouse_id', $ware->warehouse_id)
                        ->sum('product_sale_FOC_qty');

            $qty = $ord_value->product_qty;
            $qty1 = $ord_value->FOC;
            $sales  = $sal_qty + $qty;
            $blc    = $blc_qty - $qty;

            if($ord_value->FOC != 0){
                $blc = $blc - $ord_value->FOC;
                $foc = $foc_qty + $qty1;
            }else{
                $sales = $sales;
                $blc = $blc;
                $foc = $foc_qty;
            }

            if($blc <= 0){
                $product_status = 2;
            }else{
                $product_status = 1;
            }

            $value = array(
                'product_blc_qty'       => $blc ,
                'product_sales_qty'     => $sales ,
                'product_sale_FOC_qty'  => $foc,
                'product_status'        => $product_status,
                'updated_at'            => date('Y-m-d H:i:s')
            );

            DB::table('gpff_product_batch_stock')
            ->where('product_id', $ord_value->product_id)
            ->where('batch_id', $ord_value->batch_no)
            ->where('warehouse_id', $ware->warehouse_id)
            ->update($value);
        }
        return 1;
    }


    public function generateOrderPDF($orders){
        $order_id = $orders->order_id;
        $pdf_name = "GPFF_OR_".$order_id;
        $file_name = $pdf_name.'.pdf';
        $order_details = DB::table('gpff_order')
                        ->where('order_id',$order_id)
                        ->First();

        $order_list_detail = DB::table('gpff_order as gor')
                            ->join('gpff_order_list as gol', 'gol.order_id', 'gor.order_id')
                            ->join('gpff_order_list_batchwise as gprl','gol.order_list_id','gprl.order_list_id')
                            ->where('gol.order_id',$order_id)
                            ->get();

        $warehouse = DB::table('gpff_warehouse')
                    ->where('warehouse_id', $order_details->warehouse_id)
                    ->First();

        $cus_de = DB::table('gpff_customer')
                        ->where('customer_id',$order_details->customer_id)
                        ->First();

        $com_details = DB::table('gpff_branch')
                        ->where('branch_id',$order_details->branch_id)
                        ->First();

        $number = $this->numbertostring($order_details->or_tot_price);

        //Invoice
        view()->share('datas',$order_list_detail);
        view()->share('order_details',$order_details);
        view()->share('cus_de',$cus_de);
        view()->share('war', $warehouse);
        view()->share('com_details',$com_details);
        view()->share('invoice',$pdf_name);
        view()->share('value',$number);
        view()->share('date',$orders->invoice_date);
        $customPaper = array(0,0,595,425);
        // $pdf = \PDF::loadView('or_invoice')
        // ->setPaper($customPaper,'portrait');
        $pdf = \PDF::loadView('or_invoice')
        ->setPaper('a4', 'landscape');
        $pdf->save(public_path('Order_invoice/'.$pdf_name.'.pdf'));
        $file_name = $pdf_name.'.pdf';
        $name = $file_name;
        $filePath = 'Order_invoice/'.$name; 
        Storage::disk('s3')->put($filePath, file_get_contents(public_path('Order_invoice')."/".$file_name));
        return 1;
    }

    public function generateVendorOrderPDF($orders){
        $vendor_order_id = $orders->order_id;
        $pdf_name  = "GPFF_VENDOR_OR".$vendor_order_id;
        $filename = $pdf_name.'.pdf';

        $vendor_order_details = DB::table('gpff_vendor_order')
                                ->where('vendor_order_id',$vendor_order_id)
                                ->First();
        $vendor_order_list_detail = DB::table('gpff_vendor_order as gor')
                            ->join('gpff_vendor_order_list as gol', 'gol.vendor_order_id', 'gor.vendor_order_id')
                            ->join('gpff_vendor_order_list_batchwise as gprl','gol.vendor_order_list_id','gprl.vendor_order_list_id')
                            ->where('gol.vendor_order_id',$vendor_order_id)
                            ->get();
        // $order_list = [];
        // foreach($vendor_order_list_detail as $key=> $data){

        //     $order_list[] = array($key => $data->$key);
        // }
        // $vendor_order_list_detail = $order_list;
        // $vendor_order_list_detail = (array) $vendor_order_list_detail;

        $warehouse = DB::table('gpff_warehouse')
                    ->where('warehouse_id', $vendor_order_details->warehouse_id)
                    ->First();

        $area_manager = DB::table('gpff_users')
                        ->where('user_id', $vendor_order_details->area_manager_id)
                        ->First();

        $warehouse = DB::table('gpff_warehouse')
                    ->where('warehouse_id', $vendor_order_details->warehouse_id)
                    ->First();

        $vendor_details = DB::table('gpff_customer')
                        ->where('customer_id', $vendor_order_details->vendor_id)
                        ->First();

        $branch_details = DB::table('gpff_branch')
                        ->where('branch_id', $vendor_order_details->branch_id)
                        ->First();

        $number = $this->numbertostring($vendor_order_details->or_tot_price);

        view()->share('datas', $vendor_order_list_detail);
        view()->share('order_details',$vendor_order_details);
        view()->share('cus_de', $vendor_details);
        view()->share('war', $warehouse);
        view()->share('area_manager_details', $area_manager);
        view()->share('com_details', $branch_details);
        view()->share('invoice',$pdf_name);
        view()->share('value', $number);
        view()->share('date', $orders->invoice_date);

        $pdf = PDF::loadView('custom_vendor_invoice')
                ->setPaper('a4', 'landscape');

        $pdf->save(public_path('Order_invoice/'.$pdf_name.'.pdf'));
        $file_name = $pdf_name.'.pdf';
        $names = $file_name;
        // print_r($names);
        // exit();
        $filePath = 'Order_invoice/'.$names; 
  
        Storage::disk('s3')->put($filePath, file_get_contents(public_path('Order_invoice')."/".$file_name));
        return 1;
    }

    public function getOrderScheme(){
        $scheme = DB::table('gpff_order_scheme')
                    ->where('status',1)
                    ->get();

        return $scheme;
    }
    public function updateOrderInvoiceDate($orders){
        $extension = $orders->file->getClientOriginalExtension();
        if($extension == "xlsx" || $extension == "xls" || $extension == "csv") 
        {
            $path = $orders->filepath;
            $import = new UsersImport;
            Excel::import($import, $orders->file);
            $data = $import->data;
            if(!empty($data) && count($data))
            {
                $values = [];
                foreach ($data as $value) 
                {
                    if($value['order_id'] != '' && $value['invoice_date'] != ''){
                        DB::table('gpff_order')
                        ->where('order_id',$value['order_id'])
                        ->update(array(
                            "invoice_date" => $value['invoice_date']
                        ));
                    }
                }
                return 1;
            }else{
                return 2;
            }
        }

    }

    public function getFilePath($file_name){   
        return $file_name->getClientOriginalName();
    }
}