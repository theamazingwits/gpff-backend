<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
class InventoryValidator {

	function supplierName($supplierName, $min = 3, $max = 50) {
		$len = strlen($supplierName);
		if($len < $min) {
			$response = response()->json(['status' => 'failure' , 'message' => 'Please Check the Supplier Name']); 
			echo json_encode($response->original);
			exit;
		} 
	}

	function supplierID($id) {
		if(empty($id)) {
			$response = response()->json(['status' => 'failure' , 'message' => 'Please Check the Supplier Id']); 
			echo json_encode($response->original);
			exit;
		}
	}

}